from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView

from clients.models import SubfieldLayer
from fieldcalendar.models import FieldEvent
from formulas.models import Formula, FormulaVariable
from orders.models import Order
from orders.soilmap_helper import dependency_sort, duplicate_layer, apply_formulas_gis
from django.core.files import File


class RecGenerator(TemplateView):
    template_name = "recommendations/wizard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sf = SubfieldLayer.objects.get(id=self.kwargs.get("pk"))
        order = Order.objects.get(id=sf.dbf_field_values.get("order"))
        context['object'] = order
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['products'] = order.products.all()
        context['layer'] = sf
        # layers = order.field.layer.filter(name__icontains=order.order_number, editable=False)
        # layers = [l for l in layers if l.features == order.num_samples and l.polygons_only]
        # context['subfieldlayers'] = layers
        yldlayers = order.field.layer.all()
        yldlayers = [y for y in yldlayers if (y.dbf_field_values.get("Yld_Vol_Dr") and y.points_only)
                     or (y.polygons_only and
                         (y.dbf_field_values.get('nccpi2cs') or y.dbf_field_values.get('nccpi3corn')
                          ))]
        context['yield_layers'] = yldlayers
        context['formulas'] = dependency_sort(Formula.objects.filter(inputs__test_parameter__unit__unit="null").distinct())

        return context

    def post(self, request, *args, **kwargs):
        order = request.POST.get("_selected_action")
        name = request.POST.get(f'name_rec_{order}', f"Draft Recommendation {order}")
        sf = SubfieldLayer.objects.get(id=self.kwargs.get("pk"))
        formulas = Formula.objects.filter(id__in=request.POST.getlist(f'formula_{order}'))
        inputs = FormulaVariable.objects.filter(formula_input__in=formulas)
        additional_parameters = [i.test_parameter.name for i in inputs]
        dup = duplicate_layer(request, sf, name, additional_params=additional_parameters)
        dup.dbf_field_names['recommendation'] = True

        # Create Yield feature
        if request.POST.get(f'yld_{order}') == 'yld_fixed':
            val = float(request.POST.get(f'yld_value_{order}'))
            yld_vector = [val] * dup.features
            dup.add_feature('Yield', yld_vector)
            dup.append_log(f"Yield added as a fixed value of {val}", save=True)


        apply_formulas_gis(dup, formulas, request)
        zipdir, zipfile = dup.create_shapefile_from_layers()
        FieldEvent.objects.create_shapefile_upload_event(dup, zipdir, zipfile)
        ## this will save the layer source file name.

        return HttpResponseRedirect(reverse("field_geo_details", kwargs={'pk': dup.field.pk}))
