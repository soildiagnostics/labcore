from django.urls import path

from recommendations import views

urlpatterns = [
    path('<int:pk>/start/', views.RecGenerator.as_view(), name="rec_generator"),
    ]