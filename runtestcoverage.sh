#!/bin/bash
echo "Running tests"
coverage run --source="." --omit=gdal_merge.py manage.py test
echo "Generating report"
coverage html
echo "Removing (fake) uploaded images and files"
rm media/contactimages/anonymous*
rm samples/tests/*.csv

