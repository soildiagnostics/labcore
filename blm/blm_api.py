import re
import requests
import json

def get_trs(lat: float, lon: float):
    """
    Get TRS and details for a given lat long
    >>> res = get_trs(40.063591730433409, -88.242087491960007)
    >>> parse_plss(res['features'][0]['attributes']['landdescription'])
    {'State': 'IL', 'Meridian': '3', 'Township': 'T19N', 'Range': 'R8E', 'Section': '36', 'Additional': None}
    """
    base_url = f"https://gis.blm.gov/arcgis/rest/services/Cadastral/BLM_Natl_PLSS_CadNSDI/MapServer/exts/CadastralSpecialServices/GetTRS?lat={lat}&lon={lon}&units=DD&returnlevel=&returnalllevels=true&f=pjson"
    res = requests.get(base_url)
    results = json.loads(res.content)
    return results



def parse_plss(plss: str):
    """
    Takes a PLSS string and turns it into a legal description dictionary
    containing section, township range
    >>> parse_plss("IL030190N0080E0SN360")
    {'State': 'IL', 'Meridian': '3', 'Township': 'T19N', 'Range': 'R8E', 'Section': '36', 'Additional': None}

    >>> parse_plss("NV210380N0560E0SN100ASESW")
    {'State': 'NV', 'Meridian': '21', 'Township': 'T38N', 'Range': 'R56E', 'Section': '10', 'Additional': 'ASESW'}

    >>> parse_plss("NV210380N0560E0")
    {'State': 'NV', 'Meridian': '21', 'Township': 'T38N', 'Range': 'R56E', 'Additional': None}
    """

    pattern = re.compile(
        "^(?P<st>[A-Z]{2})(?P<m>[0-9]{2})0(?P<t>[0-9]{2})0(?P<td>[N|S])0(?P<r>[0-9]{2})0(?P<rd>[E|W])0(SN(?P<s>[0-9]{2})0(?P<a>[A-Z0-9]+)?)?")

    m = re.match(pattern, plss)
    result = dict()
    result["State"] = m.group("st")
    result["Meridian"] = f'{int(m.group("m"))}'
    result["Township"] = f'T{int(m.group("t"))}{m.group("td")}'
    result["Range"] = f'R{int(m.group("r"))}{m.group("rd")}'
    try:
        result["Section"] = f'{int(m.group("s"))}'
    except TypeError:
        pass
    try:
        result["Additional"] = m.group("a")
    except TypeError:
        pass
    return result


if __name__ == "__main__":
    import doctest

    doctest.testmod()
