parse_plss = (plss) => {
    try {
        let pat = /(?<st>[A-Z]{2})(?<m>[0-9]{2})0(?<t>[0-9]{2})0(?<td>[N|S])0(?<r>[0-9]{2})0(?<rd>[E|W])0(SN(?<s>[0-9]{2})0(?<a>[A-Z0-9]+)?)?/;
        let match = plss.match(pat).groups;
        return {
            "State": match.st,
            "Meridian": `${parseInt(match.m)}`,
            "Section": `${parseInt(match.s)}`,
            "Township": `T${parseInt(match.t)}${match.td}`,
            "Range": `R${parseInt(match.r)}${match.rd}`,
            "Additional": match.a
        }
    } catch (e) {
        console.log(plss);
    }
}

make_polygon = (rings, color, content) => {
    $.each(rings, (jdx, rng) => {
        let rng_t = [];
        $.each(rng, (idx, pt) => {
            let newpt = [pt[1], pt[0]];
            rng_t.push(newpt);
        })
        var newpoly = L.polygon(rng_t, {color: color, fillOpacity: 0.05});
        newpoly.bindPopup(content)
            .addTo(mapsList[0]);
        newpoly.openPopup();
        plss.push(newpoly);
    });
}

get_trs = (lat, lon) => {
    let base_url = `https://gis.blm.gov/arcgis/rest/services/Cadastral/BLM_Natl_PLSS_CadNSDI/MapServer/exts/CadastralSpecialServices/GetTRS?lat=${lat}&lon=${lon}&units=DD&returnlevel=&returnalllevels=true&f=pjson`;
    $.getJSON(base_url, (data) => {
        $.each(plss, (idx, val) => {
            mapsList[0].removeLayer(val);
        });
        var feats;
        if(data.features.length > 0) {
            feats = data.features;
        } else if (data.alllevels.firstdivision.features.length > 0) {
            feats = data.alllevels.firstdivision.features;
        }
        $.each(feats, (idx, val) => {
            var desc = parse_plss(val.attributes.landdescription);
            $("#id_state").val(desc.State);
            $("#id_twpnum").val(desc.Township);
            $("#id_range").val(desc.Range);
            $("#id_section").val(desc.Section);
            let content = `${desc.State}, ${desc.Section}, ${desc.Township} ${desc.Range}`;
            make_polygon(val.geometry.rings, "yellow", content);
        });
        // $.each(data.alllevels.firstdivision.features, (idx, feat) => {
        //     make_polygon(feat.geometry.rings, "blue")
        // });
        // $.each(data.alllevels.township.features, (idx, feat) => {
        //     show_polygon(feat.geometry.rings, "green")
        // });
    });
}
