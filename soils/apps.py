from django.apps import AppConfig


class SoilsConfig(AppConfig):
    name = 'soils'
