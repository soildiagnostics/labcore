import json

from django.test import TestCase, Client
from django.urls import reverse

from .serializers import *


# Create your tests here.
class SoilSeriesTests(TestCase):

    def setUp(self):
        asd = StateCountyASD.objects.create(
            state="IL", countyfp=17, county="Champaign")
        asd.save()

        soil = SoilSeries(
            name="Drummer",
            asd=asd,
            mukey=100010,
            rootznaws=150,
            soc0_100=20,
            nccpi2cs=.9,
            om=0,
            pH=0,
            dbthirdbar=0
        )
        soil.save()

        gss = GSSURGOValueTable(
            mukey=242969,
            soc0_100=350,
            nccpi2cs=0.91,
            rootznaws=135,
            droughty=False)
        gss.save()

    def test_soils_exist(self):

        s = SoilSeries.objects.all()
        self.assertEqual(len(s), 1)
        self.assertEqual(str(s[0]), 'Drummer, (Champaign, IL)')

        s = StateCountyASD.objects.all()
        self.assertNotEqual(len(s), 0)
        self.assertNotEqual(str(s[0]), '')

    def test_soil_details(self):

        c = Client()
        s = SoilSeries.objects.all()[0]
        response = c.get(reverse("soil_details", kwargs={"soil": s.id}))
        resp = json.loads(response.content)
        self.assertTrue(resp['id'], s.id)

    def test_mukey_for_location(self):

        c = Client()
        response = c.get(reverse("mukey_for_location", kwargs={"lat": 40.21233147701793,
                                                               "lng": -88.30461859703064}))
        resp = json.loads(response.content)
        #print(resp)
        self.assertTrue(resp['mukey'], 0)
