from django.contrib import admin

from .models import *


# Register your models here.

class StateCountyASDAdmin(admin.ModelAdmin):
    list_filter = ('state',)
    
class SoilSeriesAdmin(admin.ModelAdmin):
    search_fields = ('mukey',)

class GSSSURGOValueTableAdmin(admin.ModelAdmin):
    search_fields = ('mukey',)
    
admin.site.register(GSSURGOValueTable, GSSSURGOValueTableAdmin)
admin.site.register(StateCountyASD, StateCountyASDAdmin)
admin.site.register(SoilSeries, SoilSeriesAdmin)