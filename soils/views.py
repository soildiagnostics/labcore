import json

from django.shortcuts import HttpResponse

from .serializers import *


# Create your views here.

#@login_required



# def get_states(request):
#     return HttpResponse(
#         json.dumps({'states': StateCountyASD.objects.state_list(),
#                     'last_crop_choices': Crop.objects.last_crop_choices(),
#                     'tillage_choices': CornRecommendation.objects.tillage_choices()
#                     }),
#         content_type="application/json"
#     )


# def get_counties_for_state(request, state):
#     return HttpResponse(
#         json.dumps({
#             'state': state,
#             'counties': StateCountyASD.objects.county_list(state)
#         }),
#         content_type="application/json"
#     )


# def get_soils_for_location(request, state, county):
#     return HttpResponse(
#         json.dumps({
#             'state': state,
#             'county': county,
#             'soils': SoilSeries.objects.soil_list(state, county)
#         }),
#         content_type="application/json"
#     )


# def get_soil_properties_for_series(request, state, county, series):
#     return HttpResponse(
#         json.dumps({
#             'state': state,
#             'county': county,
#             'soils': series,
#             'pH': SoilSeries.objects.get_avg_pH(state, county, series),
#             'om': SoilSeries.objects.get_avg_om(state, county, series),
#             'yield': SoilSeries.objects.get_avg_yield(state, county, series),
#         }),
#         content_type="application/json"
#     )


def get_soil_details(request, soil):
    s = SoilSeries.objects.get(pk=soil)
    soil_serial = SoilSerializer(s).data
    return HttpResponse(
        json.dumps(soil_serial),
        content_type="application/json"
    )


def get_mukey_for_location(request, lat, lng):
    soil = SoilSeries.objects.soil_from_latlng(lat, lng)
    return HttpResponse(
        json.dumps({
            'lat': lat,
            'lng': lng,
            'mukey': soil.mukey,
            'state': soil.asd.state,
            'county': soil.asd.county,
            'soil_series': soil.name,
            'pH': soil.pH,
            'om': soil.om  # ,
            #'yield': soil.get_bushels_from_nccpi2cs()
        }),
        content_type="application/json"
    )
