from rest_framework import serializers

from .models import *


class SoilSerializer(serializers.ModelSerializer):
    class Meta:
        model = SoilSeries
        fields = '__all__'