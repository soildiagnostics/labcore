import re
from urllib.request import urlopen

from django.db import models
from django.db.models import Avg


# Helper function:

# def get_key_for_choice(value, choicelist):
#     key = None
#     for k, v in choicelist:
#         if v == value:
#             key = k
#     return key
#
#
# def get_value_for_key(key, choicelist):
#     value = None
#     for k, v in choicelist:
#         if k == key:
#             value = v
#     return value


def soil_from_coords(lat, lng):
    url = "http://casoilresource.lawr.ucdavis.edu/gmap/get_mapunit_data.php?lat={}&lon={}".format(lat, lng)
    html = urlopen(url)
    f = html.read()
    mukey = int(re.findall(
        '<span class="record">\s*(\d+)\s*</span>', f.decode())[0])
    try:
        return SoilSeries.objects.filter(mukey=mukey)[0]
    except IndexError:
        # There are no soils for that mukey - we will create one anew
        # This exception handling is meant to be a bandaid, not to replace
        # a proper inclusion in the database.
        st, cty = re.findall(
            '<span class="record">\s*([a-zA-Z]{2})([0-9]{1,3})\s*</span>', f.decode())[0]
        name = re.findall(
            'cokey=[0-9]+\">([A-Z-a-z0-9\s]+)</a></span>', f.decode())[0]
        asd, created = StateCountyASD.objects.get_or_create(
            state=st.upper(), countyfp=int(cty))
        if created:
            asd.save()

        try:
            # Fallback to get mukey-specific information from the gridded
            # SSURGO value table
            gssurgo_soil = GSSURGOValueTable.objects.get(mukey=mukey)

            soil = SoilSeries(
                name=name,
                asd=asd,
                mukey=mukey,
                rootznaws=gssurgo_soil.rootznaws,
                soc0_100=gssurgo_soil.soc0_100,
                nccpi2cs=gssurgo_soil.nccpi2cs,
                om=0,
                pH=0,
                dbthirdbar=0
            )
        except Exception:
            # GSSURGO is a fail too, lets get average properties
            # for the series name in the ASD.
            try:
                soil = SoilSeries(
                    name=name,
                    asd=asd,
                    mukey=mukey,
                    rootznaws=SoilSeries.objects.filter(name=name).aggregate(
                        Avg('rootznaws'))['rootznaws__avg'],
                    soc0_100=SoilSeries.objects.filter(name=name).aggregate(
                        Avg('soc0_100'))['soc0_100__avg'],
                    nccpi2cs=SoilSeries.objects.filter(name=name).aggregate(
                        Avg('nccpi2cs'))['nccpi2cs__avg'],
                    om=SoilSeries.objects.filter(
                        name=name).aggregate(Avg('om'))['om__avg'],
                    pH=SoilSeries.objects.filter(
                        name=name).aggregate(Avg('pH'))['pH__avg'],
                    dbthirdbar=SoilSeries.objects.filter(name=name).aggregate(
                        Avg('dbthirdbar'))['dbthirdbar__avg']
                )
            except Exception:
                # Nothing worked - more exception handling may be needed.
                return None

        soil.save()
    return soil


# # Create your models here.
# class ASDManager(models.Manager):
#
#     def state_choices(self):
#         states = [(state, state,) for state in self.state_list()]
#         return states
#
#     def county_choices_for_state(self, state):
#         counties = [(cty, cty,) for cty in self.county_list()]
#         return counties
#
#     def state_list(self):
#         states = self.order_by('state').values_list(
#             'state', flat=True).distinct()
#         return list(states)
#
#     def county_list(self, state):
#         counties = self.filter(state=state).order_by(
#             'county').values_list('county', flat=True).distinct()
#         return list(counties)


class StateCountyASD(models.Model):
    statefp = models.IntegerField(null=True)
    state = models.CharField(max_length=2)
    countyfp = models.IntegerField()
    geoid = models.IntegerField(null=True)
    county = models.CharField(max_length=40, null=True, blank=True)
    asd = models.IntegerField(null=True)
    # objects = ASDManager()

    def __str__(self):
        return('%s, %s' % (self.county, self.state))

    class Meta:
        ordering = ('state', 'county')


class SoilSeriesManager(models.Manager):

    # def soil_list(self, state, county):
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     soil_list = self.filter(asd=asd).order_by(
    #         'name').values_list('name', flat=True).distinct()
    #     return list(soil_list)
    #
    # def soil_list_choices(self, state, county):
    #     soil_list = self.soil_list(state, county)
    #     soil_list = [(soil, soil) for soil in soil_list]
    #     return(soil_list)
    #
    def soil_from_latlng(self, lat, lng):
        soil = soil_from_coords(lat, lng)
        return soil
    #
    # def get_avg_om(self, state, county, series):
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     return self.filter(asd=asd).filter(name=series).aggregate(Avg('om'))
    #
    # def get_avg_dbthirdbar(self, state, county, series):
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     return self.filter(asd=asd).filter(name=series).aggregate(Avg('dbthirdbar'))
    #
    # def get_avg_pH(self, state, county, series):
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     return self.filter(asd=asd).filter(name=series).aggregate(Avg('pH'))
    #
    # def get_avg_yield(self, state, county, series):
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     cpi = self.filter(asd=asd).filter(
    #         name=series).aggregate(Avg('nccpi2cs'))
    #     return int(58.205 + 168 * cpi['nccpi2cs__avg'])  # see model below
    #
    # def make_or_get_generic_soil(self, state, county, series):
    #     # lets try to get first
    #     asd = StateCountyASD.objects.get(state=state, county=county)
    #     try:
    #         soil = self.get(asd=asd, name=series, mukey=None)
    #     except SoilSeries.DoesNotExist:
    #
    #         soil = SoilSeries(
    #             name=series,
    #             asd=asd,
    #             mukey=None,
    #             rootznaws=self.filter(asd=asd).filter(name=series).aggregate(
    #                 Avg('rootznaws'))['rootznaws__avg'],
    #             soc0_100=self.filter(asd=asd).filter(name=series).aggregate(
    #                 Avg('soc0_100'))['soc0_100__avg'],
    #             nccpi2cs=self.filter(asd=asd).filter(name=series).aggregate(
    #                 Avg('nccpi2cs'))['nccpi2cs__avg'],
    #             om=self.get_avg_om(state, county, series)['om__avg'],
    #             pH=self.get_avg_pH(state, county, series)['pH__avg'],
    #             dbthirdbar=self.filter(asd=asd).filter(name=series).aggregate(
    #                 Avg('dbthirdbar'))['dbthirdbar__avg']
    #         )
    #         soil.save()
    #     return soil


class SoilSeries(models.Model):
    name = models.CharField(max_length=40)
    asd = models.ForeignKey(StateCountyASD, on_delete=models.CASCADE)
    mukey = models.IntegerField(null=True)
    objects = SoilSeriesManager()
    rootznaws = models.FloatField()
    soc0_100 = models.FloatField()
    nccpi2cs = models.FloatField()
    om = models.FloatField()
    pH = models.FloatField()
    dbthirdbar = models.FloatField()  # Bulk density

    def __str__(self):
        return "%s, (%s)" % (self.name, self.asd)

    class Meta:
        ordering = ('name',)


class GSSURGOValueTable(models.Model):
    mukey = models.IntegerField()
    soc0_100 = models.FloatField(null=True, blank=True)
    nccpi2cs = models.FloatField(null=True, blank=True)
    rootznaws = models.FloatField(null=True, blank=True)
    droughty = models.BooleanField(blank=True)

    def __str__(self):
        return str(self.mukey)
