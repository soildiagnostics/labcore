# Load FIPS data

from soils.models import StateCountyASD, SoilSeries


with open('soils/fips_data.csv', 'r') as dat:
    lines = dat.readlines()
    for line in lines:
        rec = line.split(',')
        print(rec)
        obj = StateCountyASD(
            statefp=int(rec[0]),
            state=rec[1],
            countyfp=int(rec[2]),
            geoid=int(rec[3]),
            county=rec[4],
            asd=int(rec[5]))
        obj.save()

SoilSeries.objects.all().delete()
with open('soils/valueSeries.csv', 'r') as dat:
    lines = dat.readlines()
    for line in lines:
        rec = line.split(',')
        print(rec)
        try:
            soc0_100 = float(rec[24])
        except:
            soc0_100 = 0
        try:
            rootznaws = float(rec[28])
        except:
            rootznaws = 0
        try:
            nccpi2cs = float(rec[26])
        except:
            nccpi2cs = 0
        try:
            pH = float(rec[44])
        except:
            pH = 6.3
        try:
            om = float(rec[42])
        except:
            om = 2.75
        try:
            dbthirdbar = float(rec[43])
        except:
            dbthirdbar = 1.4
        obj = SoilSeries(
            name=rec[0],
            asd=StateCountyASD.objects.get(geoid=rec[2]),
            soc0_100=soc0_100,
            rootznaws=rootznaws,
            nccpi2cs=nccpi2cs,
            pH=pH,
            om=om,
            dbthirdbar=dbthirdbar,
            mukey=rec[29].strip())
        obj.save()
