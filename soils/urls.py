from django.conf.urls import url

# from django.views.generic import TemplateView
import soils.views

urlpatterns = [
    # url(r'^$', nrec.views.cornrec),
    # url(r'^(?P<fsn>[0-9.\-]+)/$', nrec.views.cornrec),
    # url(r'generate/', nrec.views.generate_recommendation),
    # url(r'load_gss/', nrec.views.load_gss_data),
    # url(r'get_states/', nrec.views.get_states),
    url(r'soil/(?P<soil>[0-9]+)/$',
        soils.views.get_soil_details, name="soil_details"),
    # url(r'get_counties/(?P<state>[A-Z]{2})/$',
    #    nrec.views.get_counties_for_state),
    # url(r'get_soils/(?P<state>[A-Z]{2})/(?P<county>[A-Za-z.,]+)/$',
    #    nrec.views.get_soils_for_location),
    url(r'get_mukey/(?P<lat>[0-9.\-]+)/(?P<lng>[0-9.\-]+)/$',
        soils.views.get_mukey_for_location, name="mukey_for_location"),
    # url(r'get_soils/(?P<state>[A-Z]{2})/(?P<county>[A-Za-z.,]+)/(?P<series>[A-Za-z.,]+)/$',
    #    nrec.views.get_soil_properties_for_series),
]
