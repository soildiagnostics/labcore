FROM soildx/gdalbase:dj3

WORKDIR /code
COPY . /code/

ENV PYTHONUNBUFFERED 1
EXPOSE 8000

COPY docker-entrypoint.sh /usr/bin
ENTRYPOINT ["docker-entrypoint.sh"]