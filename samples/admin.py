import csv
import datetime
import itertools
from io import StringIO
from collections import OrderedDict

from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.models import ContentType
from django.db import models
from django.db.models import Count, Avg, StdDev
from django.db.models import Prefetch, Min, Max, Func
from django.db.models import Q
from django.forms import Textarea, TextInput
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.urls import path
from django.utils.html import format_html
from django.utils.timezone import now
from django.utils.translation import gettext as _

from associates.models import OrganizationUser
from customnotes.models import CustomField
from orders.models import Order
from orders.pdfs import render_to_pdf
from products.models import Product, TestParameter, Category
from rangefilter.filter import DateRangeFilter
from samples.models import (Sample, Basket, File, FileSummary, FileDetails,
                            LabMeasurement, TrackingRecord, OrderSummary, SampleSummary, SampleRegistry,
                            ProcessingAction,
                            FileUploadParser, FileUpload, OrderLayerID, BASKET_MAX_POSITIONS,
                            ProcessingRecord, LabMeasurementSummary,
                            FILE_MAX_BASKETS, SampleTally)
## Pandas and plotly
from .plots import *
from django_admin_listfilter_dropdown.filters import RelatedOnlyDropdownFilter


# Register your models here.
class OrderLayerIDInlineAdmin(admin.TabularInline):
    model = OrderLayerID
    readonly_fields = ('order',)
    fields = ('order', 'point_layer', 'region_layer')
    ordering = ('order__order_number',)
    extra = 0


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    model = File
    date_hierarchy = ('created')
    list_display = [
        'name',
        'status',
        'orders',
        'summary',
        'details',
        # 'details2',
        'dcr',
        'tally',
        'created',
        'num_baskets',
        'samples',
    ]
    ordering = ('-created',)
    list_filter = ('status',)
    list_editable = ('status',)
    inlines = [
        OrderLayerIDInlineAdmin,
    ]
    search_fields = ('name',)
    actions = ['delete_and_release_baskets']

    def delete_and_release_baskets(self, request, queryset):
        for file in queryset:
            if file.num_samples == 0:
                res = file.basket_set.all().delete()
                file.delete()
                messages.info(request, f"Deleted the following: {repr(res)}")
            else:
                messages.info(request, f"File {file} has {file.num_samples} samples: won't delete")

    delete_and_release_baskets.short_description = "Delete empty File and release baskets"

    def orders(self, obj):
        orders = [sample.order for basket in obj.qsbaskets for sample in basket.qssamples]
        orders = list(set(orders))
        orders.sort(key=lambda s: s.order_number)
        return orders

    def samples(self, obj):
        return obj.sample_count

    def summary(self, obj):
        return format_html("<a href={url}>Summary</a>", url=obj.summary)

    def details(self, obj):
        return format_html("<a href={url}>Details</a>", url=obj.details)

    # def details2(self, obj):
    #     return format_html("<a href='{url}/details2/'>Details 2</a>", url=obj.pk)

    def dcr(self, obj):
        return format_html("<a href='{url}/dcr/'>DCR</a>", url=obj.pk)

    def tally(self, obj):
        return format_html("<a href='{url}/tally/'>Tally</a>", url=obj.pk)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('<int:pk>/dcr/', self.dcrview),
            path('<int:pk>/tally/', self.tallyview),
            # path('<int:pk>/details2/', self.details2view)
        ]
        return my_urls + urls

    def _common_context(self, pk):
        context = dict()
        file = File.objects.get(id=pk)
        products = Product.objects.filter(order__samples__basket__file=file).distinct()

        context['file'] = file
        context['products'] = products
        # context['orders'] = Order.objects.filter(products__in=context['products']).filter(
        #     samples__basket__file=context['file']).distinct()
        orders = Order.objects.filter(products__in=context['products']).filter(
            samples__basket__file=context['file'])
        # tricky to get a distinct set of orders (SQL distinct and ordering incompatibilities)
        # and preserve the ordering by sample__id, this does it.
        sample_ordered = orders.order_by('samples__id')
        s = []
        [s.append(o) for o in sample_ordered if o not in s]
        context['orders'] = s

        basket_orgs = {}
        baskets = Basket.objects.filter(file=context['file'])
        for basket in baskets:
            orders = Order.objects.filter(samples__basket=basket)
            orgs = []
            for order in orders:
                order_org = order.field.farm.client.originator.organization
                if order_org.is_primary:
                    orgs.append(f"{order.field.farm.client.name} (GMS)")
                elif order_org:
                    orgs.append(order_org.name)
            orgs = list(set(orgs))
            basket_orgs[basket.id] = orgs
        context['basket_orgs'] = basket_orgs

        gps_products = Product.objects.filter(Q(name="Grid Mapping") | \
                                              Q(package__name="Grid Mapping"))
        loose_products = Product.objects.filter(Q(name__icontains="Non GPS") | \
                                                Q(package__name__icontains="Non GPS") | \
                                                Q(description__icontains='Loose'))
        context['gps_orders'] = Order.objects.filter(products__in=gps_products).filter(
            samples__basket__file=file).distinct().order_by('pk')

        acres = dict()
        for p in products:
            if p in gps_products:
                op = Order.objects.filter(products__in=[p]).filter(
                    samples__basket__file=file).distinct()
                area = sum([o.field.area for o in op])
                acres[p.name] = area
            else:
                acres[p.name] = None
        context['acres'] = acres

        context['loose_orders'] = Order.objects.filter(products__in=loose_products).filter(
            samples__basket__file=file).distinct().order_by('pk')
        return context

    def dcrview(self, request, pk):
        context = self._common_context(pk)
        # return TemplateResponse(request, "admin/dcr.html", context)
        return render_to_pdf(context, template="admin/dcr.html", request=request)

    def tallyview(self, request, pk):
        context = self._common_context(pk)
        fertility_products = Product.objects.filter(Q(category__name='Fertility') | \
                                                    Q(package__category__name='Fertility'))
        context['fertility_orders'] = Order.objects.filter(products__in=fertility_products).filter(
            samples__basket__file=context['file']).distinct()
        # return TemplateResponse(request, "admin/tally.html", context)
        return render_to_pdf(context, template="admin/tally.html", request=request)

    # def details2view(self, request, pk):
    #     context = self._common_context(pk)
    #     return TemplateResponse(request, "admin/file_details_change_list2.html", context)

    def get_queryset(self, request):
        qs = super(FileAdmin, self).get_queryset(request)
        qs = qs.prefetch_related(
            'basket_set',
            Prefetch(
                'basket_set',
                to_attr='qsbaskets',
                queryset=Basket.objects.prefetch_related(
                    Prefetch(
                        'samples',
                        queryset=Sample.objects.select_related('order'),
                        to_attr='qssamples'
                    )
                )
            ),
        ).annotate(sample_count=Count('basket__samples'),
                   #            order_count=Count('basket__samples__order', distinct=True)
                   )
        return qs


@admin.register(Basket)
class BasketAdmin(admin.ModelAdmin):
    model = Basket
    search_fields = ('name',)


# class TrackingRecordInlineAdminForm(forms.ModelForm):
#     class Meta:
#         model = TrackingRecord
#         fields = "__all__"
#
#     def clean(self):
#         if self.has_changed() and self.initial:
#             raise ValidationError (
#                 _("Cannot change an existing record")
#             )
#         return super().clean()


class TrackingRecordInlineAdmin(admin.TabularInline):
    model = TrackingRecord
    extra = 0
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }
    # autocomplete_fields = ('user',)
    #
    readonly_fields = ('user', 'log')

    # form = TrackingRecordInlineAdminForm

    # def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
    #     if db_field.name == "user":
    #         ouser = OrganizationUser.objects.get(user=request.user)
    #         kwargs['queryset'] = OrganizationUser.objects.filter(organization=ouser.organization)
    #         kwargs['initial'] = ouser
    #     return super(TrackingRecordInlineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Sample)
class SampleAdmin(admin.ModelAdmin):
    exclude = ['location',
               ]
    raw_id_fields = ('order',)
    inlines = [TrackingRecordInlineAdmin]
    autocomplete_fields = ('basket',)
    list_display = [
        'id',
        'custom_name',
        'order',
        'order_serial',
        'depth',
        'depth_unit',
        'get_file',
        'basket',
        'basket_position',
    ]
    list_editable = [
        'custom_name',
        'order_serial'
    ]
    search_fields = ('=id', 'order__order_number', 'basket__file__name')
    date_hierarchy = 'created'
    # list_select_related = ('order', 'basket', 'basket__file',)

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '10'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 40})},
    }

    def save_model(self, request, obj, form, change):
        ouser = OrganizationUser.objects.get(user=request.user)
        obj.save(orguser=ouser)

    def get_queryset(self, request):
        qs = super(SampleAdmin, self).get_queryset(request)
        return qs.select_related('basket__file',
                                 'basket',
                                 'order')

    def get_file(self, obj):
        try:
            return obj.basket.file
        except AttributeError:
            return None

    get_file.short_description = 'File'


@admin.register(LabMeasurement)
class LabMeasurementAdmin(admin.ModelAdmin):
    search_fields = ('=sample__id', 'parameter__name', '=sample__order__order_number')
    autocomplete_fields = ('sample', 'parameter',)
    raw_id_fields = ('related_measurement',)
    list_display = ('order', 'sample', 'parameter', 'value', 'measurement_error', 'detection_limit',
                    'void', 'duplicate')
    list_editable = ('value', 'measurement_error', 'detection_limit')
    # list_filter = ('parameter', 'duplicate')
    list_filter = (("sample__order__products__category", RelatedOnlyDropdownFilter),
                   ("sample__order__products", RelatedOnlyDropdownFilter),
                   ("parameter", RelatedOnlyDropdownFilter),
                   ('created', DateRangeFilter),
                   'void', 'duplicate'
                   )
    date_hierarchy = 'created'
    actions = ['export_as_csv']

    def save_model(self, request, obj, form, change):
        ouser = OrganizationUser.objects.get(user=request.user)
        obj.save(orguser=ouser)

    def export_as_csv(self, request, queryset):
        param = TestParameter.objects.get(id=request.GET.get('parameter__id__exact'))
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(str(param))
        writer = csv.writer(response)
        values = queryset.values_list('value', flat=True)
        for v in values:
            writer.writerow([float(v)])
        return response

    export_as_csv.description = "Export values to CSV"


@admin.register(OrderSummary)
class OrderSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/order_summary_change_list.html'
    date_hierarchy = 'created'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': models.Count('id'),
            # 'complete': models.Count('is_processed')
        }

        summary = list(
            qs
                .values('order',
                        'order__order_number',
                        'order__date_created',
                        'order__order_status__name')
                .annotate(**metrics)
                .order_by('-order__created')
        )

        response.context_data['summary'] = summary

        return response


@admin.register(SampleTally)
class SampleTallyAdmin(admin.ModelAdmin):
    change_list_template = "admin/tally_summary_change_list.html"
    date_hierarchy = 'created'

    def get_count(self, annotation, file, product):

        if file == 'None':
            file = None
        else:
            file = file.id

        for item in annotation:
            if item['basket__file'] == file:
                if item['order__products'] == product.id:
                    return item['count']
        return 0

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        files = list(File.objects.filter(basket__samples__in=qs).distinct().order_by('created'))
        products = list(Product.objects.filter(order__samples__in=qs).distinct().order_by('category__id', 'id'))
        categories = list(Category.objects.filter(product__in=products).distinct().order_by('id'))

        sample_summary = dict()
        counts = dict()
        grand_total = dict()
        grand_total['samples'] = 0

        for category in categories:
            prods_in_cat = [p for p in products if p.category == category]
            counts[category] = len(prods_in_cat)

        annotations = qs.values('basket__file', 'order__products').annotate(count=Count('order__products')).order_by('basket__file')
        # create empty dict
        files.append("None")

        for file in files:
            filedict = dict()
            counts[file] = 0
            for product in products:
                numsamples = self.get_count(annotations, file, product)
                filedict[product] = numsamples
                try:
                    grand_total[product] = grand_total[product] + numsamples
                except (KeyError, TypeError):
                    grand_total[product] = numsamples
                counts[file] = counts[file] + numsamples
                grand_total['samples'] = grand_total['samples'] + numsamples

            sample_summary[file] = filedict

        response.context_data['files'] = files
        response.context_data['products'] = products
        response.context_data['categories'] = categories
        response.context_data['summary'] = sample_summary
        response.context_data['counts'] = counts
        response.context_data['grand_total'] = grand_total

        yr = request.GET.get('created__year')
        mo = request.GET.get('created__month')
        day = request.GET.get('created__day')

        filename = 'all'
        if yr:
            filename = f'{yr}'
        if mo:
            filename = f'{filename}-{mo}'
        if day:
            filename = f'{filename}-{day}'

        response.context_data['filename'] = filename


        return response




# ------- HELPER FUNCTIONS -------------


def products_and_test_parameters(product_queryset, include_calculated=False, include_all=True):
    "Fetch products and product test parameters"

    if include_calculated:
        qs = TestParameter.objects.all()
    else:
        qs = TestParameter.objects.filter(calculated=False)

    test_params_prefetch = Prefetch(
        'test_parameters',
        to_attr='test_params',
        queryset=qs.select_related('unit')
    )

    product_type = ContentType.objects.get_for_model(Product)

    custom_fields_prefetch = Prefetch(
        'custom_fields',
        queryset=CustomField.objects.filter(content_type_id=product_type.id)
    )

    qs_products = product_queryset.distinct().prefetch_related(
        test_params_prefetch,
        custom_fields_prefetch,
        Prefetch(
            'package',
            # to_attr='products',
            queryset=Product.objects.prefetch_related(
                test_params_prefetch,
                custom_fields_prefetch
            ).annotate(cnt_params=Count('test_parameters', distinct=True)),
        ),
    ).annotate(cnt_all_params=Count('package__test_parameters', distinct=True),
               cnt_params=Count('test_parameters', distinct=True)
               )

    prod_test_params = dict()
    for p in qs_products:
        test_params = p.test_params
        if p.cnt_params > 0:
            prod_test_params[p.pk] = test_params
        for subprod in p.package.all():
            if subprod.cnt_params > 0:
                prod_test_params[subprod.pk] = subprod.test_params

    qs_p = Product.objects.filter(id__in=product_queryset)
    for p in product_queryset:
        qs_p |= p.package.all()
    unique_parameters = qs.filter(product__in=qs_p)
    if not include_all:
        unique_parameters = unique_parameters.filter(calculated=include_calculated)

    return (qs_products, prod_test_params, unique_parameters.distinct())


def process_samples_dict(samples_queryset, products, prod_test_params, unique_parameters, last_valid=False):
    "Fetch sample data and return dict for templates"
    if last_valid:
        lmqs = LabMeasurement.objects.filter(void=False).select_related('parameter')
    else:
        lmqs = LabMeasurement.objects.select_related('parameter')

    qs_samples = samples_queryset.prefetch_related(
        'order__products',
        Prefetch(
            'labmeasurement_set',
            to_attr='labs',
            queryset=lmqs
        )
    )

    if len(samples_queryset) > 0 and samples_queryset.first().basket_position is not None:
        qs_samples = qs_samples.select_related(
            'basket', 'order', 'basket__file').order_by('basket__name', 'basket_position')

    readings = list()
    for sample in qs_samples:
        sample.sample_is_processed = True if products.count() > 0 else None
        sample.checkbox = admin.helpers.checkbox.render(admin.helpers.ACTION_CHECKBOX_NAME, sample.pk)
        sample.productset = dict()
        sample.parset = OrderedDict()
        # all_products = list(sample.products)
        all_products = sample.order.products.all()
        for product in products:
            if product in all_products:
                all_subproducts_are_complete = True
                if product.is_package:
                    sub_products = product.package.all()
                else:
                    sub_products = [product]
                for prod in sub_products:
                    try:
                        pars = prod_test_params[prod.pk]
                        # pars = unique_parameters
                    except KeyError:
                        continue
                    sample.productset[prod] = dict()
                    product_is_complete = True
                    for par in pars:
                        measures = [l for l in sample.labs if l.parameter == par]
                        all_void = all([m.void for m in measures])
                        par_is_complete = True
                        if not par.calculated and (len(measures) == 0 or all_void):
                            par_is_complete = False
                            product_is_complete = False
                            all_subproducts_are_complete = False
                            sample.sample_is_processed = False
                        sample.productset[prod][par] = {'measures': measures,
                                                        'par_complete': par_is_complete
                                                        }
                        if par in unique_parameters:
                            sample.parset[par] = {'measures': measures,
                                                  'par_complete': par_is_complete
                                                  }
                    sample.productset[prod]['is_complete'] = product_is_complete
                if [product] != sub_products:
                    sample.productset[product] = dict()
                    sample.productset[product]['is_complete'] = all_subproducts_are_complete
            else:
                all_subproducts_are_complete = None
                if product.is_package:
                    sub_products = product.package.all()
                else:
                    sub_products = [product]
                for prod in sub_products:
                    try:
                        pars = prod_test_params[prod.pk]
                        # pars = unique_parameters
                    except KeyError:
                        continue
                    sample.productset[prod] = dict()
                    product_is_complete = None
                    for par in pars:
                        # measures = [l for l in sample.labs if l.parameter == par]
                        # all_void = all([m.void for m in measures])
                        par_is_complete = None
                        sample.productset[prod][par] = {'measures': None,
                                                        'par_complete': par_is_complete
                                                        }
                    sample.productset[prod]['is_complete'] = product_is_complete
                if [product] != sub_products:
                    sample.productset[product] = dict()
                    sample.productset[product]['is_complete'] = all_subproducts_are_complete

        readings.append(sample)
    return readings


@admin.register(SampleSummary)
class SampleSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/sample_summary_change_list.html'
    # list_filter = ('duplicate', 'void',)
    ordering = ('order_serial',)

    def changelist_view(self, request, extra_context=None):
        order = Order.objects.get(pk=request.GET.get('order'))

        context_data = dict()
        context_data['order'] = order

        products = Product.objects.filter(order=order)
        products_qs, prod_test_params, unique_parameters = products_and_test_parameters(products)
        # add by product, par, order lab statistics
        for prod in products:
            products_qs
        context_data['products'] = products_qs

        samples_qs = Sample.objects.filter(order=order.pk)

        readings = process_samples_dict(samples_qs, products_qs, prod_test_params, unique_parameters)
        context_data['readings'] = readings
        context_data['unique_parameters'] = unique_parameters

        return super().changelist_view(request, extra_context=context_data)


@admin.register(FileSummary)
class FileSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/file_summary_change_list.html'
    date_hierarchy = 'created'

    def lookup_allowed(self, lookup, value):
        if lookup == "basket__file":
            return True
        else:
            return super().lookup_allowed(lookup, value)

    def changelist_view(self, request, extra_context=None):
        file = File.objects.get(pk=request.GET.get('basket__file'))

        context_data = dict()
        context_data['file'] = file

        products_qs = Product.objects.filter(
            order__samples__basket__file=file, test_parameters__isnull=False
        )

        products_qs, prod_test_params, uniques = products_and_test_parameters(products_qs)
        context_data['products'] = products_qs

        sample_file = OrderedDict()
        for basket in file.baskets.order_by('name'):
            qs_samples = basket.samples.all()
            samples = process_samples_dict(qs_samples, products_qs, prod_test_params, uniques)
            sample_file[basket] = samples

        context_data['sample_file'] = sample_file

        return super().changelist_view(request, extra_context=context_data)


@admin.register(FileDetails)
class FileDetailsAdmin(admin.ModelAdmin):
    change_list_template = "admin/file_details_change_list.html"
    actions = [
        'recheck',
        'generate_lab_id_csv',
    ]

    def lookup_allowed(self, lookup, value):
        if lookup == "basket__file":
            return True
        else:
            return super().lookup_allowed(lookup, value)

    def get_queryset(self, request):
        qs0 = super().get_queryset(request)
        file = File.objects.get(pk=request.GET.get('basket__file'))
        qs = qs0.filter(basket__file=file).order_by('id')
        return qs

    def recheck(self, request, queryset):
        queryset.update(recheck=True)

    recheck.short_description = "Recheck selected samples"

    def generate_lab_id_csv(self, request, queryset):
        queryset = queryset.order_by('id')
        queryset.filter(basket_position__in=[1, 21]).update(duplicate=True)
        # this is a custom GMS thing - soils in positions 1 and 21 are scooped into a different
        # basket for duplicate testing.
        response = HttpResponse(content_type='text/csv')
        file = File.objects.get(pk=request.GET.get('basket__file'))
        labids = [item.id for item in queryset]
        rechecks = [item.id for item in queryset.filter(recheck=True)]
        duplicates = [item.id for item in queryset.filter(duplicate=True)]
        basket = [item.basket.name for item in queryset]

        ziplist = itertools.zip_longest(basket, labids, duplicates, rechecks, fillvalue="")

        response['Content-Disposition'] = f'''attachment; filename="{file}_LabIDs.csv"'''
        writer = csv.writer(response)

        writer.writerow(['Basket', 'Lab IDs', 'Duplicates', 'Rechecks'])
        for item in ziplist:
            writer.writerow(item)

        return response

    generate_lab_id_csv.short_description = "Generate CSV for Lab IDs"

    def changelist_view(self, request, extra_context=None):

        file = File.objects.get(pk=request.GET.get('basket__file'))

        context_data = dict()
        context_data['file'] = file

        qs_samples = self.get_queryset(request)

        qs_products = Product.objects.filter(
            order__samples__in=qs_samples,
        ).distinct()

        products, prod_test_params, unique_parameters = products_and_test_parameters(qs_products)
        context_data['products'] = products
        for p in unique_parameters:
            print(p)
        readings = process_samples_dict(qs_samples, products, prod_test_params, unique_parameters)
        context_data['readings'] = readings
        context_data['unique_parameters'] = unique_parameters

        return super(FileDetailsAdmin, self).changelist_view(request, extra_context=context_data)


from django.forms import ModelChoiceField


class MyModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return "#{} : {} ".format(obj.order_number, obj.field.farm.client.originator.organization.name)
        # , obj.field.farm.client.contact, obj.field.farm.name, obj.field.name)# % obj.id


class SampleRegistryForm(forms.ModelForm):
    order_qs = Order.objects.select_related(
        'field', 'field__farm', 'field__farm__client',
        'field__farm__client__contact',
        'field__farm__client__originator__organization'
        # ).filter(id__gt=29490),
    ).exclude(order_status__name__in=['Done', 'published', 'Cancel'])
    full_order = MyModelChoiceField(queryset=order_qs,
                                    widget=forms.Select(attrs={'class': "input-lg"}),
                                    )
    order_number = forms.CharField(max_length=50, required=False)
    order = forms.ModelChoiceField(queryset=order_qs, widget=forms.HiddenInput())

    class Meta:
        model = SampleRegistry
        exclude = ('duplicate_positions',)

    def __init__(self, *args, **kwargs):
        super(SampleRegistryForm, self).__init__(*args, **kwargs)
        if self.instance.pk is not None:
            self.fields['full_order'].widget = forms.HiddenInput()
        else:
            self.fields['order_number'].widget = forms.HiddenInput()


@admin.register(SampleRegistry)
class SampleRegistryAdmin(admin.ModelAdmin):
    form = SampleRegistryForm
    date_hierarchy = ('created')
    autocomplete_fields = ('order',)
    exclude = ('duplicate_positions',)
    list_display = ("order", "num_samples", "file", "basket", "basket_position", "gps_acres", "void")
    change_form_template = 'admin/sample_registry_change_form.html'
    actions = ['void_samples']

    fieldsets = (
        (None, {
            'fields': ('full_order', 'order_number', 'order', 'num_samples', 'custom_start_number', 'allocate_basket')
        }),
        ("Field Technician's records", {
            'fields': ('gps_acres', 'sampled', 'comments', 'additional_charges', 'depth', 'depth_unit')
        }),
        ('Autogenerated File, Basket and Position (Do not change)', {
            'fields': ('basket', 'basket_position'),
            'classes': ('collapse',),
        }),
    )

    def void_samples(self, request, queryset):
        for receiving in queryset:
            receiving.samples.update(void=True)
            newcomment = f"Record voided by {request.user} on {now()}"
            receiving.comments = f"{receiving.comments}\n{newcomment}"
            messages.info(request, newcomment)
        queryset.update(void=True)

    def order_number(self, object):
        return object.order.order_number

    def file(self, object):
        try:
            return object.basket.file
        except AttributeError:
            return None

    def get_changeform_initial_data(self, request):
        available_basket, first_position = File.objects.get_available_basket_position()
        return {'basket': available_basket,
                'basket_position': first_position}

    def save_model(self, request, obj, form, change):
        ouser = OrganizationUser.objects.get(user=request.user)
        obj.save(orguser=ouser, request=request)

    def add_view(self, request, form_url='', extra_context=None):
        ModelForm = self.get_form(request, None, change=False)

        f, _created = File.objects.get_or_create_file()
        extra_context = extra_context or {}
        extra_context['currentFile'] = f
        data = request.POST.copy()
        if 'full_order' in data:
            data['order'] = data['full_order']
        form = ModelForm(data, request.FILES)
        form_validated = form.is_valid()
        if request.method == "POST" and '_confirmsave' not in request.POST:
            if form_validated:
                context = dict()
                form.cleaned_data['order'] = form.cleaned_data['full_order']
                context['form'] = form
                context['data'] = form.cleaned_data
                available_basket, first_position = File.objects.get_available_basket_position()
                context['empties'] = available_basket.file.open_slots
                context['basket'] = available_basket
                context['position'] = first_position
                context['file'] = available_basket.file
                context['new_file'] = False
                context['allocate'] = form.cleaned_data['allocate_basket']
                context['site_header'] = self.admin_site.site_header
                if not context['allocate']:
                    messages.add_message(request, messages.WARNING, "No basket will be allocated to these samples")
                else:
                    if form.cleaned_data['num_samples'] > available_basket.file.open_slots:
                        context['new_file'] = True
                        context['basket'] = available_basket
                        context['position'] = first_position
                        messages.add_message(request, messages.WARNING, \
                                             "New File Needed: Only {} slots left in current file".format(
                                                 available_basket.file.open_slots))

                next_sample_id = Sample.objects.latest('id').id if Sample.objects.exists() else getattr(settings,
                                                                                                        'PROCESSOR_INITIAL_SAMPLE_ID',
                                                                                                        0)
                context['first_id'] = next_sample_id + 1
                context['last_id'] = next_sample_id + form.cleaned_data['num_samples']
                left_in_file = available_basket.file.open_slots
                num_samples = form.cleaned_data['num_samples']
                # print(left_in_file, num_samples, available_basket, available_basket.empty_positions, first_position)
                if num_samples > available_basket.empty_positions:
                    context['multiple_baskets'] = True

                    bnames = ['First basket', 'Second basket', 'Third basket', 'Fourth basket',
                              'Fifth basket', 'Sixth basket', 'Seventh basket', "Eighth basket",
                              "Ninth basket", "Tenth basket"]
                    basket_id = 0
                    remaining = num_samples
                    filled = 0
                    baskets = []
                    while remaining > 0:
                        first_position = first_position if first_position != 1 else 1
                        last_position = BASKET_MAX_POSITIONS
                        # print(basket_id, first_position, filled, remaining)
                        if basket_id > (FILE_MAX_BASKETS - 1):
                            raise NotImplementedError('Samples exceed allowable of baskets in File.')
                        if basket_id == 0:
                            first_id = next_sample_id + 1
                            last_id = next_sample_id + (BASKET_MAX_POSITIONS - (first_position - 1))
                            sample_cnt = BASKET_MAX_POSITIONS - (first_position - 1)
                        else:
                            if remaining > BASKET_MAX_POSITIONS:
                                first_id = context['first_id'] + filled
                                last_id = first_id + BASKET_MAX_POSITIONS - 1
                                sample_cnt = BASKET_MAX_POSITIONS
                                last_position = BASKET_MAX_POSITIONS
                            else:
                                first_id = context['first_id'] + filled
                                last_id = first_id + remaining - 1
                                sample_cnt = remaining
                                last_position = remaining
                        baskets.append({
                            "name": int(available_basket.name) + basket_id,
                            "first_id": first_id,
                            "first_position": first_position,
                            "last_id": last_id,
                            "last_position": last_position,
                            "basket": bnames[basket_id],
                            "sample_cnt": sample_cnt
                        })
                        filled += BASKET_MAX_POSITIONS - (first_position - 1)
                        remaining = num_samples - filled
                        basket_id += 1
                        first_position = 1
                    context["baskets"] = baskets
                # print(context)

                return TemplateResponse(request, "admin/confirm_add_registry.html", context)
        return super().add_view(request, form_url, extra_context)


@admin.register(FileUploadParser)
class FileUploadParserAdmin(admin.ModelAdmin):
    list_display = ('name', 'check_test_line')
    search_fields = ('name',)


@admin.register(FileUpload)
class FileUploadAdmin(admin.ModelAdmin):
    autocomplete_fields = ('parser',)
    list_display = ('file', 'parser')

    fieldsets = (
        (None, {
            'fields': ('file', 'parser', 'reading_type')
        }),
        ("DEPRECATED - do not use", {
            'fields': ('duplicates', 'void'),
            'classes': ('collapse',),
        })
    )

    def save_model(self, request, obj, form, change):
        ouser = OrganizationUser.objects.get(user=request.user)
        obj.save(orguser=ouser, request=request)


# @admin.register(SamplingRecord)
# class SamplingRecordAdmin(admin.ModelAdmin):
#     list_display = ('order', 'sampler', 'sampled', 'num_samples', 'gps_acres')
#     autocomplete_fields = ('order', 'sampler',)


class ProcessingRecordAdminForm(forms.ModelForm):
    technician = forms.ModelChoiceField(queryset=None)

    class Meta:
        model = ProcessingRecord
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(ProcessingRecordAdminForm, self).__init__(*args, **kwargs)
        request_orguser = OrganizationUser.objects.filter(user=self.current_user).first()
        qs = OrganizationUser.objects.filter(user__is_staff=True)
        if request_orguser and self.instance.pk is None:
            self.fields['technician'].initial = request_orguser
            qs = qs.filter(organization=request_orguser.organization)
        print(self.fields.keys())
        self.fields['technician'].queryset = qs


@admin.register(ProcessingRecord)
class ProcessingRecordAdmin(admin.ModelAdmin):
    list_display = ('file', 'action', 'technician', 'time')
    form = ProcessingRecordAdminForm
    autocomplete_fields = ('file', 'technician')

    def save_model(self, request, obj, form, change):
        ouser = OrganizationUser.objects.get(user=request.user)
        obj.save(orguser=ouser, request=request)

    def get_form(self, request, *args, **kwargs):
        form = super(ProcessingRecordAdmin, self).get_form(request, **kwargs)
        form.current_user = request.user
        return form


admin.site.register(ProcessingAction)


class QuarterFilter(admin.SimpleListFilter):
    title = _("Date ranges")
    parameter_name = "range"

    def lookups(self, request, model_admin):
        """
        Provides a list of quarters to filter by
        :param request:
        :param model_admin:
        :return:
        """
        return (
            ("ThisQ", "This quarter"),
            ("LastQ", "Last quarter"),
            ("YTD", "Year to date"),
            ("LastYTD", "This time last year"),
        )

    def queryset(self, request, queryset):
        today = now()
        thisq = today.month // 3
        if self.value() == "ThisQ":
            qstart = datetime.date(today.year, thisq * 3, 1)
            qend = qstart + datetime.timedelta(days=91)
            qs = queryset.filter(created__gte=qstart, created__lt=qend)
        elif self.value() == "LastQ":
            qend = datetime.date(today.year, thisq * 3, 1)
            qstart = qend - datetime.timedelta(days=91)
            qs = queryset.filter(created__gte=qstart, created__lt=qend)
        elif self.value() == "YTD":
            qstart = datetime.date(today.year, 1, 1)
            qs = queryset.filter(created__gte=qstart, created__lte=today)
        elif self.value() == "LastYTD":
            qstart = datetime.date(today.year - 1, 1, 1)
            qend = datetime.date(today.year - 1, today.month, today.day)
            qs = queryset.filter(created__gte=qstart, created__lte=qend)
        else:
            qs = queryset
        return qs


class Month(Func):
    function = 'EXTRACT'
    template = '%(function)s(MONTH from %(expressions)s)'
    output_field = models.IntegerField()


def getMY(m):
    today = now()
    if m <= today.month:
        return datetime.date(now().year, m, 1).strftime('%B %Y')
    else:
        return datetime.date(now().year - 1, m, 1).strftime('%B %Y')


class CustomRelatedOnlyFieldListFilter(admin.RelatedOnlyFieldListFilter):
    def field_choices(self, field, request, model_admin):
        pk_qs = model_admin.get_queryset(request).distinct().values_list('%s__pk' % self.field_path, flat=True)
        return field.get_choices(include_blank=False, limit_choices_to={'pk__in': pk_qs})


@admin.register(LabMeasurementSummary)
class LabMeasurementSummaryAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    change_list_template = "admin/measurement_summary_change_list.html"
    # list_filter = ("parameter", QuarterFilter, "sample__order__products")
    list_filter = (("sample__order__products__category", RelatedOnlyDropdownFilter),
                   ("sample__order__products", RelatedOnlyDropdownFilter),
                   ("parameter", RelatedOnlyDropdownFilter),
                   ('created', DateRangeFilter),
                   )

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset.filter(void=False)
            df = pd.DataFrame(list(qs.values()))
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': Count('id'),
            'average': Avg('value'),
            'min': Min('value'),
            'max': Max('value'),
            'stdev': StdDev('value')
        }

        response.context_data['summary'] = list(
            qs
                .values('parameter__significant_digits', 'parameter__name', 'parameter__unit__unit')
                .annotate(**metrics)
                .order_by('-total')
        )

        response.context_data['summary_total'] = dict(
            qs.aggregate(**metrics)
        )

        if "parameter__id__exact" in request.GET.keys():
            # Request is for a single parameter
            bins = pd.cut(df['value'].astype('float'), 20)
            vc = bins.value_counts(sort=False)
            strvc = vc.to_string()
            strvc = strvc.replace('(', "").replace(']', ',')
            tab = pd.read_csv(StringIO(strvc), header=None, names=["From", "To", "Count"])
            response.context_data['table'] = pd.DataFrame(tab).to_html(index=False)
            tp = TestParameter.objects.get(id=request.GET.get('parameter__id__exact'))
            response.context_data['parameter'] = tp
            response.context_data['histogram'] = get_histogram_plot(data=df, value='value',
                                                                    title=f"Histogram")
            response.context_data['time_series'] = get_time_series_plot(data=df, value='value', title=f"Trend",
                                                                        time='created')

        return response
