# Generated by Django 2.1.1 on 2018-10-30 01:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('samples', '0014_auto_20181029_2034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileuploadparser',
            name='pattern',
            field=models.TextField(help_text='Regex magic: Captured number is (?P<name>[-+]?[0-9]*\\.?[0-9]*.)'),
        ),
    ]
