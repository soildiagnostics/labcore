# Generated by Django 2.1.5 on 2020-02-05 12:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('samples', '0046_sampleregistry_custom_start_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='SampleTally',
            fields=[
            ],
            options={
                'verbose_name': 'Sample Processing Tally',
                'verbose_name_plural': 'Sample Processing Tallies',
                'proxy': True,
                'indexes': [],
            },
            bases=('samples.sample',),
        ),
    ]
