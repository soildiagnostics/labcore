# Generated by Django 2.1.4 on 2018-12-30 18:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('samples', '0025_auto_20181227_2149'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileDetails',
            fields=[
            ],
            options={
                'verbose_name': 'Sample Details by File',
                'verbose_name_plural': 'Sample Details by File',
                'proxy': True,
                'indexes': [],
            },
            bases=('samples.sample',),
        ),
    ]
