import codecs
import datetime
import re

from django.contrib import messages
from django.contrib.gis.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.utils import timezone
from django.utils.translation import gettext as _

from associates.models import OrganizationUser
from common.models import ModelDiffMixin
from orders.models import Order
from products.models import Product, TestParameter

# Create your models here.

FILE_MAX_BASKETS = 8
BASKET_MAX_POSITIONS = 50

UNIT_CHOICES = (
    ("I", "Inches"),
    ("C", "Centimeters")
)


class TimeRecord(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created']
        abstract = True


class SampleContainer(TimeRecord):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class FileManager(models.Manager):
    def get_or_create_file(self):
        """
        Creates a file for today using today's day number such that:
            {DOY}.{SEQ}   DOY is the julian day of the year and SEQ is the sequence of files in the day starting from 1
        :return: File, boolean created
        """

        today = timezone.localtime(timezone.now()).replace(hour=0, minute=0, second=0, microsecond=0)
        try:
            created = False
            ## See if there is any open file.
            opens = File.objects.filter(status="O",
                                    created__range=[today, today + datetime.timedelta(1)])
            if opens.exists():
                return (opens.latest("created"), created)
            f = File.objects.filter(created__range=[today, today + datetime.timedelta(1)]).latest('created')
            if f.status == 'C':
                f = f.get_next_file()
                created = True
            return (f, created)
        except (IndexError, File.DoesNotExist):
            f = File.objects.create(name="{}.1".format(today.timetuple().tm_yday))
            return (f, True)

    def get_available_basket_position(self):
        f, created = File.objects.get_or_create_file()
        b = f.get_first_available_basket()
        return (b, b.next_empty_position)


class File(SampleContainer):
    STATUS_CHOICES = (
        ('O', "Open"),
        ('C', "Closed"),
        ('F', "Completed")
    )
    status = models.CharField(max_length=1, default="O", choices=STATUS_CHOICES)
    objects = FileManager()
    max_baskets = models.IntegerField(default=FILE_MAX_BASKETS)

    @property
    def baskets(self):
        return self.basket_set.all()

    @property
    def num_baskets(self):
        return self.basket_set.count()

    @property
    def num_samples(self):
        return Sample.objects.filter(basket__file=self).count()

    @property
    def num_valid_samples(self):
        return Sample.objects.filter(basket__file=self, void=False).count()

    @property
    def sample_range(self):
        samples = Sample.objects.filter(basket__file=self).order_by('pk')
        return (samples.first(), samples.last())

    @property
    def summary(self):
        return "/admin/samples/filesummary/?basket__file={}".format(self.pk)

    @property
    def details(self):
        return "/admin/samples/filedetails/?basket__file={}".format(self.pk)

    def get_next_file(self):
        pattern = "^(\w+)(?:\.(\d+))?$"
        m = re.search(pattern, self.name)
        f = File.objects.create(name="{}.{}".format(m.group(1), int(m.group(2)) + 1))
        self.save()
        return f

    def get_next_basket(self):
        if self.baskets.count() == self.max_baskets:
            f = self.get_next_file()
            return f.get_next_basket()
        else:
            year = datetime.datetime.today().year
            baskets_this_year = Basket.objects.filter(created__year=year)
            if baskets_this_year.count() == 0:
                return Basket.objects.create(file=self, name="1")
            last_basket = baskets_this_year.latest('created')
            new_name = str(1 + int(last_basket.name))
            return Basket.objects.create(file=self, name=new_name)

    def get_first_available_basket(self):
        if self.baskets:
            b = self.baskets.latest('pk')  # TODO - should this be latest by id or created rather than modified?
            if b.has_space:
                return b
        return self.get_next_basket()

    @property
    def open_slots(self):
        b = self.get_first_available_basket()
        pos = b.next_empty_position
        empties_this_basket = b.max_positions - pos + 1
        empty_baskets = self.max_baskets - self.num_baskets
        return (empty_baskets * b.max_positions + empties_this_basket)

    @property
    def orders_in_file(self):
        orders = Order.objects.filter(samples__basket__file=self).distinct().values_list('order_number', flat=True)
        return list(set(orders))


class Basket(SampleContainer):
    file = models.ForeignKey(File, on_delete=models.CASCADE)
    max_positions = models.IntegerField(default=BASKET_MAX_POSITIONS)

    @property
    def due(self):
        return self.created + datetime.timedelta(days=7)

    @property
    def samples(self):
        return self.sample_set.all()

    def sample_in_position(self, pos):
        try:
            return Sample.objects.get(basket=self, basket_position=pos)
        except Sample.DoesNotExist:
            return None

    @property
    def next_empty_position(self):
        # i = 1
        # while self.sample_in_position(i):
        #     i += 1
        # return i
        try:
            next = self.samples.latest('basket_position')
            return next.basket_position + 1
        except ObjectDoesNotExist:
            return 1

    @property
    def empty_positions(self):
        try:
            next = self.samples.latest('basket_position')
            return self.max_positions - next.basket_position
        except ObjectDoesNotExist:
            return self.max_positions

    @property
    def has_space(self):
        return self.max_positions > self.samples.count()

    def __str__(self):
        return self.name


#
# class SampleManager(managers.ModelManager):
#
#     def labmeasurements_summary(self):
#         "Annotate the sample with labmeasurements"
#         # Count lab measurements per product-testparameter
#         self.annotate(total_labs=Count('lab_measurements'))
#
#     def labmeasurements_by_testparameter(self, param):
#         return self.labmeasurements_summary.filter(parameter=param, duplicate=False, void=False)


class Sample(ModelDiffMixin, TimeRecord):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="samples")
    custom_name = models.CharField(max_length=50, blank=True, null=True)
    order_serial = models.IntegerField()
    location = models.MultiPointField(blank=True, null=True,
                                      help_text=_("Single or multiple GPS-tagged cores"))
    depth = models.IntegerField(blank=True, null=True)
    depth_unit = models.CharField(max_length=1, default="I", choices=UNIT_CHOICES)
    basket = models.ForeignKey(Basket, blank=True, null=True, on_delete=models.CASCADE, related_name='samples')
    basket_position = models.IntegerField(blank=True, null=True)
    void = models.BooleanField(default=False, help_text=_("Mark void if this sample is not to be used in results"))
    duplicate = models.BooleanField(default=False, help_text=_("Check if sample is a duplicate"))
    recheck = models.BooleanField(default=False, help_text="Check if sample is to be rechecked")

    # ignore_fields = [modified]
    capture_diff_for_fields = "__all__"

    def __str__(self):
        return str(self.pk)

    @property
    def located(self):
        return (self.location is not None)

    @property
    def get_test_parameters(self):
        return self.order.get_parameters

    def get_lab_measurements_for_parameter(self, param):
        # if not param.calculated:
        #     return LabMeasurement.objects.filter(sample=self, parameter=param, void=False, duplicate=False)
        # else:
        #     return self._calculate(param)
        return LabMeasurement.objects.filter(sample=self, parameter=param, void=False)

    def _calculate(self, param):
        return LabMeasurement.objects.filter(sample=self, parameter=param, void=False)

    def complete_for_product(self, product, products=None):
        try:
            # assert (product in (products or self.order.products.all()))
            for p in product.get_test_parameters():
                if self.get_lab_measurements_for_parameter(p).count() == 0:
                    return False
            return True
        except AssertionError:
            return None
        # measurements = [len(self.get_lab_measurements_for_parameter(p)) > 0 for p in product.get_test_parameters()]
        # return all(measurements)

    @property
    def products(self):
        return self.order.products.all().prefetch_related('products',
                                                          )

    @property
    def is_processed(self):
        for prod in self.products:
            if not self.complete_for_product(prod, self.products):
                return False
        return True
        # measurements = [self.get_lab_measurements_for_parameter(p).count() > 0 for p in test_params]
        # return all(measurements)

    def save(self, *args, **kwargs):
        try:
            orguser = kwargs.pop('orguser')
        except KeyError:
            raise KeyError("No orguser specified")
        if self.has_changed:
            TrackingRecord.objects.create(sample=self,
                                          user=orguser,
                                          log="Sample updated: {}".format(self.diff_text))
            obj = super().save(**kwargs)
            return obj

        else:
            obj = super().save(**kwargs)
            TrackingRecord.objects.create(sample=self,
                                          user=orguser,
                                          log="Sample registered")
            return obj

    class Meta:
        unique_together = ('order', 'basket', 'basket_position')
        ordering = ('order', 'order_serial')
        indexes = [
            models.Index(fields=('order', 'order_serial', 'created'))
        ]


class TrackingRecord(TimeRecord):
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE)
    user = models.ForeignKey(OrganizationUser, on_delete=models.PROTECT)
    log = models.TextField()

    def __str__(self):
        return self.created.strftime("%m-%d-%Y, %I:%m %p")


class LabMeasurement(ModelDiffMixin, models.Model):
    created = models.DateTimeField(auto_now_add=True)
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE)
    parameter = models.ForeignKey(TestParameter, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=4)
    related_measurement = models.ForeignKey('LabMeasurement', blank=True, null=True, on_delete=models.CASCADE)
    void = models.BooleanField(default=False)
    duplicate = models.BooleanField(default=False)
    detection_limit = models.DecimalField(max_digits=10, decimal_places=4, blank=True, null=True)
    measurement_error = models.DecimalField(max_digits=10,
                                            decimal_places=4,
                                            blank=True, null=True,
                                            help_text=_("If present, value will be rendered with a plus/minus sign"))
    capture_diff_for_fields = "__all__"

    class Meta:
        ordering = ['-created', 'parameter']
        indexes = [
            models.Index(fields=('created', 'parameter')),
            models.Index(fields=('sample', 'parameter'))
        ]

    def __str__(self):
        if self.measurement_error:
            return f"{self.value}±{self.measurement_error} {self.parameter} on sample {self.sample}"
        return "{} {} on sample {}".format(self.value, self.parameter, self.sample)

    # def alert(self):
    #     pass

    @property
    def order(self):
        return self.sample.order

    def save(self, *args, **kwargs):
        try:
            orguser = kwargs.pop('orguser')
        except KeyError:
            raise KeyError("No orguser specified")

        if self.has_changed:
            TrackingRecord.objects.create(sample=self.sample,
                                          user=orguser,
                                          log="Updated {}: {}".format(self.parameter, self.diff_text))
            obj = super().save(**kwargs)
            return obj


        else:
            obj = super().save(**kwargs)
            TrackingRecord.objects.create(sample=self.sample,
                                          user=orguser,
                                          log="New measurement: {}".format(self.__str__()))
            return obj


class OrderSummary(Sample):
    class Meta:
        proxy = True
        verbose_name = _("Order Summary")
        verbose_name_plural = _("Order Summaries")


class SampleSummary(Sample):
    class Meta:
        proxy = True
        verbose_name = _("Sample Summary by Order")
        verbose_name_plural = _("Sample Summaries by Order")


class FileSummary(File):
    # ordering = ('basket', 'basket_position',)
    class Meta:
        proxy = True
        verbose_name = _("Sample Summary by File")
        verbose_name_plural = _("Sample Summaries by File")


class SampleTally(Sample):
    class Meta:
        proxy = True
        verbose_name = _("Sample Processing Tally")
        verbose_name_plural = _("Sample Processing Tallies")


class FileDetails(Sample):
    class Meta:
        proxy = True
        verbose_name = _("Sample Details by File")
        verbose_name_plural = _("Sample Details by File")


# class BaseMeasurementFileParser(models.Model):
#     name = models.CharField(max_length=50)
#     file = models.FileField(upload_to="measurements/")
#
#     class Meta:
#         abstract = True
#
#     def save(self, *args, **kwargs):
#         self.handle_file()
#         super().save(*args, **kwargs)
#
#     def handle_file(self):
#         raise NotImplementedError


class SamplingRecord(TimeRecord):
    order = models.OneToOneField(Order, on_delete=models.PROTECT)
    sampler = models.ForeignKey(OrganizationUser, blank=True, null=True, on_delete=models.PROTECT)
    num_samples = models.IntegerField(help_text=_("Number of samples in this order"))
    gps_acres = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)
    sampled = models.DateField(blank=True, null=True)
    comments = models.TextField(blank=True)

    class Meta:
        verbose_name = _("Field Technician's Record")
        verbose_name_plural = _("Field Technicians' Records")

    @property
    def is_sampled(self):
        return self.sampled is not True

    def __str__(self):
        return self.order.order_number

    def save(self, *args, **kwargs):
        self.order.set_status("Sampling complete")
        super().save(*args, **kwargs)


class ProcessingAction(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ProcessingRecord(TimeRecord):
    file = models.ForeignKey(File, on_delete=models.PROTECT)
    technician = models.ForeignKey(OrganizationUser, on_delete=models.PROTECT)
    action = models.ForeignKey(ProcessingAction, on_delete=models.PROTECT)
    time = models.DateTimeField()

    def __str__(self):
        return f"{self.action} by {self.technician} on File {self.file}"

    def save(self, *args, **kwargs):
        try:
            orguser = kwargs.pop('orguser')
        except KeyError:
            raise KeyError("No orguser specified")

        request = kwargs.pop('request')
        samples = Sample.objects.filter(basket__file=self.file)
        for sample in samples:
            TrackingRecord.objects.create(sample=sample,
                                          user=orguser,
                                          log="{} by {} on {}".format(self.action, self.technician, self.time))
        obj = super().save(**kwargs)
        messages.success(request, f"Added {samples.count()} tracking records")
        return obj


class SampleRegistry(TimeRecord):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    num_samples = models.IntegerField(help_text=_("Number of samples in this order"))
    custom_start_number = models.IntegerField(
        help_text=_("Provide the starting number (will be set as the custom name for the samples)"), default=1)
    gps_acres = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True,
                                    help_text=_("Area of field as measured by technician"),
                                    verbose_name="GPS Acres")
    sampled = models.DateField(blank=True, null=True,
                               help_text=_("Date when samples were collected"))
    additional_charges = models.CharField(blank=True, null=True, max_length=32,
                                          help_text=_("Additional charges if any. Specify total or per acre"))
    comments = models.TextField(blank=True,
                                help_text=_("Additional comments with respect to sample collection or receipt"))

    allocate_basket = models.BooleanField(default=True,
                                          help_text=_(
                                              "Should we allocate these samples to a Basket and File? Leave unchecked for special tests. "))
    depth = models.IntegerField(blank=True, null=True)
    depth_unit = models.CharField(max_length=1, default="I", choices=UNIT_CHOICES)
    basket = models.ForeignKey(Basket, blank=True, null=True, on_delete=models.CASCADE,
                               help_text=_("Starting basket: Automatically generated - do not change"))
    basket_position = models.IntegerField(blank=True, null=True,
                                          help_text=_(
                                              "Starting basket position: Automatically generated - do not change"))
    duplicate_positions = models.CharField(max_length=10, default="-1",
                                           help_text=_("Samples in these basket positions will be run as duplicates"))
    void = models.BooleanField(default=False, help_text=_('This receiving record is voided'))

    class Meta:
        verbose_name = _("Sample Receiving Record")
        verbose_name_plural = _("Sample Receiving Records")

    def __str__(self):
        return f"{self.order}, {self.num_samples} sample(s), at {self.basket}:{self.basket_position}"

    @property
    def samples(self):
        starting_sample_id = Sample.objects.get(basket=self.basket, basket_position=self.basket_position).id
        ending_sample_id = starting_sample_id + self.num_samples
        return Sample.objects.filter(pk__gte=starting_sample_id, pk__lt=ending_sample_id)


    def save(self, *args, **kwargs):
        request = None
        if kwargs.get('request'):
            request = kwargs.pop('request')

        try:
            orguser = kwargs.pop('orguser')
        except KeyError:
            raise KeyError("No orguser specified")

        self.order.set_status("samples received")

        if self.allocate_basket:
            f, created = File.objects.get_or_create_file()
            if f.open_slots < FILE_MAX_BASKETS * BASKET_MAX_POSITIONS and f.open_slots < self.num_samples:
                ## this above condition ensures that if an order has more samples than max allowed, it
                ## doesn't unnecessarily create another basket.
                f.status = 'C'
                f.save()

            dup_pos = self.duplicate_positions.split(",")
            i = 1
            basket, position = File.objects.get_available_basket_position()
            # set initial
            self.basket = basket
            self.basket_position = position
            while i <= self.num_samples:
                basket, position = File.objects.get_available_basket_position()
                ## Update this incase the file was bumped.
                # self.basket = basket
                # self.basket_position = position
                while position <= basket.max_positions:
                    if i <= self.num_samples:
                        duplicate = str(position) in dup_pos
                        # if duplicate:
                        #     i -= 1

                        s = Sample(
                            order=self.order,
                            custom_name=f"{self.custom_start_number + i - 1}" if self.custom_start_number != 1 else None,
                            order_serial=i,
                            depth=self.depth,
                            depth_unit=self.depth_unit,
                            basket=basket,
                            basket_position=position,
                            duplicate=duplicate
                        )
                        s.save(orguser=orguser)
                        # print("File {}-{} Pos {}; Serial {}, Duplicate {}".format(b.file, b.name, position, s.order_serial, s.duplicate))
                        i += 1
                    position += 1
                    # if position == 50:
                    #     pdb.set_trace()
            try:
                OrderLayerID.objects.get_or_create(
                    file=basket.file,
                    order=self.order
                )
            except IntegrityError:
                if request:
                    messages.add_message(request, messages.WARNING,
                                         "This order already has been tested in another File ({})".format(
                                             self.order.orderlayerid.file))
        else:
            self.basket = None
            self.basket_position = None
            i = 1
            while i <= self.num_samples:
                s = Sample(
                    order=self.order,
                    custom_name=f"{self.custom_start_number + i - 1}" if self.custom_start_number != 1 else None,
                    order_serial=i,
                    depth=self.depth,
                    depth_unit=self.depth_unit
                )
                i = i + 1

                s.save(orguser=orguser)

        super().save(*args, **kwargs)


class FileUploadParser(models.Model):
    """
    We are building a CSV parser that will automatically create lab measurements from an uploaded file.
    The pattern field requires a regex pattern with named capture groups. Only two things are
    required: The capture group must include `sample` as part of its pattern, and at least one or more
    test parameter names prefixed with `tp_`.

    For instance, the following line contains pH records:
        '436-1818963,6.14\n'
    The corresponding regex is:
    '(?P<basket>[-+]?[0-9]*\.?[0-9]*.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]*.),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]*.)\\n'

    A helpful pattern is `number` which is '[-+]?[0-9]*\.?[0-9]*.'
    With a named capture, this is:
    (?P<name>[-+]?[0-9]*\.?[0-9]*.)

    If a reading for the parameter for the given sample id is found, it will mark the old reading as void.

    If lines in the file are of the format
    "Sample ID, Parameter, Value", the regex will need to be something like
    (?P<sample>[-+]?[0-9]*\.?[0-9]+.),(?P<tp_name>[a-zA-Z]+),(?P<value>[-+]?[0-9]*\.?[0-9]+.)\\n

    """
    name = models.CharField(max_length=50)
    skip_lines = models.IntegerField(default=0, help_text=_("How many lines to ignore at the top of the file?"))
    pattern = models.TextField(help_text=_("Regex magic: Captured number is (?P<name>[-+]?[0-9]*\.?[0-9]*.)"))
    test_line = models.TextField(blank=True, null=True, help_text=_("Test your regex on an example line here"))

    def __str__(self):
        return self.name

    @property
    def check_test_line(self):
        if self.test_line:
            try:
                res = re.match(self.pattern, self.test_line)
                return repr(res.groupdict())
            except Exception:
                pass
        return "{}"

    def _get_lab_measurement(self, sample, parameter, value, ouser, void, drvoid):
        try:
            par = TestParameter.objects.get(name=parameter, calculated=False)
        except TestParameter.DoesNotExist:
            return (sample, parameter, None)

        try:
            oldlm = LabMeasurement.objects.get(
                sample=sample,
                parameter=par
            )
        except LabMeasurement.DoesNotExist:
            oldlm = None
        except LabMeasurement.MultipleObjectsReturned:
            oldlm = LabMeasurement.objects.filter(
                sample=sample,
                parameter=par
            ).latest('pk')
        newlm = LabMeasurement(
            sample=sample,
            parameter=par,
            value=value,
            related_measurement=oldlm,
            void=drvoid
        )
        newlm.save(orguser=ouser)
        if void and oldlm:
            oldlm.void = True
            oldlm.save(orguser=ouser)
            oldlm.refresh_from_db()
        return (sample, newlm, oldlm)

    def parse_line(self, line, ouser, request=None, void=None, drvoid=False):
        """
        :param line: Line to be parsed
        :return: Tuple of new and old lab measurements, either can be None if errors.
        """
        pattern = codecs.decode(self.pattern, 'unicode-escape')
        res = re.match(pattern, line)
        try:
            sample = Sample.objects.get(id=res.groupdict()['sample'])
        except Sample.DoesNotExist:
            if request:
                messages.add_message(request, messages.WARNING, f"Sample not found {res.groupdict()['sample']}")
            return [(res.groupdict()['sample'], None, None)]
        except AttributeError:
            if request:
                messages.add_message(request, messages.ERROR, f"Error on line {line}")
            return [None, None, None]
        if "tp_name" in res.groupdict().keys():
            ## This is a Sample, Parameter, Value type line
            return [self._get_lab_measurement(sample,
                                              res.groupdict()['tp_name'],
                                              float(res.groupdict()["value"]),
                                              ouser, void, drvoid)]
        else:
            pars = [k for k in res.groupdict() if k.startswith("tp_")]
            result = []
            for par in pars:
                sample, newlm, oldlm = self._get_lab_measurement(sample,
                                                                 par[3:].replace("__", " "),
                                                                 float(res.groupdict()[par]),
                                                                 ouser, void, drvoid)

                result.append((sample, newlm, oldlm))
            return result

    def cleanstring(self, string):
        if isinstance(string, bytes):
            return string.decode()
        return string

    def parse_file(self, file, ouser, request=None, void=False, drvoid=False):
        with file.open(mode='r'):
            lines = file.readlines()
            lines = lines[self.skip_lines:]  ## skip some lines..
            lines = [self.cleanstring(l) for l in lines]
        results = [self.parse_line(line.strip(), ouser, request, void=void, drvoid=drvoid) for line in lines]

        return results


class FileUpload(TimeRecord):
    CHOICES = (
        ('F', 'Routine Readings'),
        ('R', 'Rechecks'),
        ('D', 'Duplicates')
    )

    file = models.FileField(upload_to='sample_data/')
    parser = models.ForeignKey(FileUploadParser, on_delete=models.PROTECT)
    duplicates = models.BooleanField(default=False, help_text=_(
        "Check if this file contains duplicate readings. If unchecked, and if samples already exist, we will assume this is a recheck file"))
    void = models.BooleanField(default=False, help_text=_(
        "Check if you want the last readings to be automatically marked as void"))
    reading_type = models.CharField(max_length=1, default='F', choices=CHOICES,
                                    help_text="Duplicate or Recheck uploads will be automatically marked Void")

    def __str__(self):
        return self.file.name

    def save(self, *args, **kwargs):
        try:
            ouser = kwargs.pop('orguser')
        except KeyError:
            raise KeyError("No orguser specified")

        request = kwargs.pop('request')
        if self.reading_type == "D":
            self.duplicates = True
        super().save(*args, **kwargs)
        if self.reading_type != 'F':
            drvoid = True
        else:
            drvoid = False

        self.parser.parse_file(self.file, ouser, request, void=self.void, drvoid=drvoid)


class OrderLayerID(models.Model):
    file = models.ForeignKey(File, on_delete=models.PROTECT)
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    point_layer = models.CharField(max_length=100, help_text=_("Point layer ID"), blank=True)
    region_layer = models.CharField(max_length=100, help_text=_("Regions Layer ID"), blank=True)


class LabMeasurementSummary(LabMeasurement):
    class Meta:
        proxy = True
        verbose_name = "Measurement Summary"
        verbose_name_plural = "Measurement Summary"
