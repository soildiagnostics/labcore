import datetime
import random

from django.conf import settings
from django.contrib.gis.geos import MultiPoint, Point
from django.core.files import File as FileFile
from django.test import TestCase, override_settings
from django.utils.timezone import now

from associates.models import OrganizationUser, Organization
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company
from orders.models import OrderStatus, Order
from products.models import Category, Product, TestParameter, MeasurementUnit, Discount
from samples.models import File, Basket, Sample, TrackingRecord, LabMeasurement, SampleRegistry, \
    FileUploadParser


def create_samples_in_order(order):
    f = File.objects.create(name="123.1")
    b = Basket.objects.create(file=f, name="456")

    location = SampleTestCase.create_multipoint(Point(40.111951, -88.230055), 3)

    for i in range(32):
        s = Sample(
            order=order,
            order_serial=i,
            location=location,
            depth=12,
            basket=b,
            basket_position=i
        )
        s.save(orguser=order.created_by)

def create_order(field, orguser, with_samples=False):
    status, created = OrderStatus.objects.get_or_create(name="new")
    o = Order.objects.create(
        order_number="123ABC@#$",
        field=field,
        created_by=orguser,
        lab_processor=orguser.organization,
        order_status=status
    )

    cat, created = Category.objects.get_or_create(name="Soil Test")
    p = Product.objects.create(name="FertiSaver-N", category=cat, created_by=orguser.organization)
    u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
    par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=2)
    p.test_parameters.add(par)

    p.set_price(100, 'USD')
    # d = Discount.objects.create(dealer=a,
    #                             discount=10,
    #                             content_object=p)
    o.products.add(p)

    p = Product.objects.create(name="LOI", created_by=orguser.organization)
    u, created = MeasurementUnit.objects.get_or_create(unit="%")
    par, created = TestParameter.objects.get_or_create(name="Organic C", unit=u, index=3)
    p.test_parameters.add(par)

    p.set_price(100, 'USD')
    # d = Discount.objects.create(dealer=a,
    #                             discount=10,
    #                             content_object=p)
    o.products.add(p)
    if with_samples:
        create_samples_in_order(o)
    return o

class SampleTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()

        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        cat, created = Category.objects.get_or_create(name="Soil Test")
        p = Product.objects.create(name="FertiSaver-N", category=cat, created_by=ouser.organization)
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=2)

        p.test_parameters.add(par)
        self.assertEqual(len(p.test_parameters.all()), 1)

        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        p = Product.objects.create(name="LOI", created_by=ouser.organization)
        u, created = MeasurementUnit.objects.get_or_create(unit="%")
        par, created = TestParameter.objects.get_or_create(name="Organic C", unit=u, index=3)

        p.test_parameters.add(par)
        self.assertEqual(len(p.test_parameters.all()), 1)

        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        self.urbana = Point(40.111951, -88.230055)

    def create_samples_in_order(self, order):
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        location = SampleTestCase.create_multipoint(Point(40.111951, -88.230055), 3)

        for i in range(32):
            s = Sample(
                order=order,
                order_serial=i,
                location=location,
                depth=12,
                basket=b,
                basket_position=i
            )
            s.save(orguser=order.created_by)

    def create_order(self, field, orguser, with_samples=False):
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=field,
            created_by=orguser,
            lab_processor=orguser.organization,
            order_status=status
        )

        cat, created = Category.objects.get_or_create(name="Soil Test")
        p = Product.objects.create(name="FertiSaver-N", category=cat, created_by=orguser.organization)
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=2)
        p.test_parameters.add(par)

        p.set_price(100, 'USD')
        # d = Discount.objects.create(dealer=a,
        #                             discount=10,
        #                             content_object=p)
        o.products.add(p)

        p = Product.objects.create(name="LOI", created_by=orguser.organization)
        u, created = MeasurementUnit.objects.get_or_create(unit="%")
        par, created = TestParameter.objects.get_or_create(name="Organic C", unit=u, index=3)
        p.test_parameters.add(par)

        p.set_price(100, 'USD')
        # d = Discount.objects.create(dealer=a,
        #                             discount=10,
        #                             content_object=p)
        o.products.add(p)
        if with_samples:
            create_samples_in_order(o)
        return o

    def test_add_file(self):

        f = File.objects.create(name="123.1")
        self.assertNotEqual(f.pk, None)
        self.assertEqual(str(f), "123.1")

    def test_create_file_with_day(self):

        f, created = File.objects.get_or_create_file()
        self.assertTrue(created)
        self.assertEqual(f.name, "{}.1".format(datetime.datetime.now().timetuple().tm_yday))

    def test_get_next_file(self):

        yday = datetime.datetime.now().timetuple().tm_yday
        f, created = File.objects.get_or_create_file()
        self.assertEqual(f.name, "{}.1".format(datetime.datetime.now().timetuple().tm_yday))

        for i in range(20):
            f = f.get_next_file()
            self.assertEqual(f.name, "{}.{}".format(yday, i + 2))

    def test_get_next_file_when_closed(self):
        # yday = datetime.datetime.now().timetuple().tm_yday
        f, created = File.objects.get_or_create_file()
        self.assertEqual(f.name, "{}.1".format(datetime.datetime.now().timetuple().tm_yday))

        f.status = 'C'
        f.save()
        g, created = File.objects.get_or_create_file()
        self.assertTrue(created)
        self.assertEqual(g.name, "{}.2".format(datetime.datetime.now().timetuple().tm_yday))

        self.assertEqual(f.status, 'C')
        self.assertEqual(g.status, 'O')

    def test_get_reopened_file(self):
        # yday = datetime.datetime.now().timetuple().tm_yday
        f, created = File.objects.get_or_create_file()
        self.assertEqual(f.name, "{}.1".format(datetime.datetime.now().timetuple().tm_yday))

        f.status = 'C'
        f.save()
        g, created = File.objects.get_or_create_file()
        self.assertTrue(created)
        self.assertEqual(g.name, "{}.2".format(datetime.datetime.now().timetuple().tm_yday))

        self.assertEqual(f.status, 'C')
        self.assertEqual(g.status, 'O')

        f.status = 'O'
        f.save()
        g.status = 'C'
        g.save()

        f2, created = File.objects.get_or_create_file()
        self.assertFalse(created)
        self.assertEqual(f2, f)

    def test_get_first_available_basket(self):

        f, created = File.objects.get_or_create_file()
        b = f.get_first_available_basket()
        self.assertEqual(b.name, '1')

        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(b), "{}".format(b.name))
        self.assertTrue(b.has_space)

    def test_add_basket(self):
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        self.assertNotEqual(b.pk, None)
        self.assertEqual(b.name, "456")
        self.assertIn(b, f.baskets)
        self.assertEqual(b.next_empty_position, 1)

    def test_empty_positions(self):
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="123")
        order = Order.objects.get(order_number="123ABC@#$")
        self.assertEqual(b.next_empty_position, 1)
        self.assertEqual(b.empty_positions, b.max_positions)

        for i in range(13):
            s = Sample(
                order=order,
                order_serial=i,
                location=self.create_multipoint(self.urbana, 3),
                depth=12,
                basket=b,
                basket_position=i
            )

            s.save(orguser=order.created_by)
        self.assertEqual(b.next_empty_position, 13)
        self.assertEqual(b.empty_positions, b.max_positions-12)

    @staticmethod
    def create_multipoint(point, neighbors):

        pnt_array = []
        for i in range(neighbors):
            pnt = Point(point.x + random.gauss(0, 0.0000001),
                        point.y + random.gauss(0, 0.0000001))
            pnt_array.append(pnt)
        return MultiPoint(pnt_array)

    def test_create_samples(self):

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")

        s = Sample(
            order=order,
            order_serial=3,
            location=self.create_multipoint(self.urbana, 3),
            depth=12,
            basket=b,
            basket_position=4
        )

        self.assertRaises(KeyError, s.save)

        for i in range(32):
            s = Sample(
                order=order,
                order_serial=i,
                location=self.create_multipoint(self.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)

            self.assertEqual(str(s), str(s.pk))
            trk = TrackingRecord.objects.get(sample=s)
            self.assertEqual(trk.log, "Sample registered")

            s.depth = 6
            self.assertRaises(KeyError, s.save)

            s.save(orguser=order.created_by)
            trk = TrackingRecord.objects.filter(sample=s)
            self.assertEqual(len(trk), 2)
            self.assertEqual(trk.latest('created').log, "Sample updated: depth changed from 12 to 6")
            self.assertEqual(str(trk.latest('created')), now().strftime("%m-%d-%Y, %I:%m %p"))
            self.assertTrue(s.located)
            self.assertEqual(b.next_empty_position, i+1+4)

        self.assertEqual(len(order.samples.all()), 32)

    def test_get_test_parameters(self):

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                location=self.create_multipoint(self.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)

            self.assertEqual(str(s), str(s.pk))

        self.assertEqual(len(order.samples.all()), 3)

        prod_list = order.products.all()

        for sample in order.samples.all():
            for prod in prod_list:
                for tp in prod.test_parameters.all():
                    self.assertIn(tp, sample.get_test_parameters)

    def test_not_processed(self):

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                location=self.create_multipoint(self.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)

            self.assertEqual(str(s), str(s.pk))
            self.assertFalse(s.is_processed)

            s.save(orguser=order.created_by)

            self.assertEqual(str(s), str(s.pk))
            self.assertFalse(s.is_processed)

    def test_create_lab_measurements(self):

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                location=self.create_multipoint(self.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)
            self.assertFalse(s.is_processed)

            params = s.get_test_parameters
            for p in params:
                l = LabMeasurement(
                    sample=s,
                    parameter=p,
                    value=random.gauss(10, 1)
                )

                self.assertRaises(KeyError, l.save)
                l.save(orguser=order.created_by)
                l.save(orguser=order.created_by)  # no change!
                l.value += random.gauss(10, 2)
                l.save(orguser=order.created_by)

                self.assertIn(str(p), str(l))

            self.assertTrue(s.is_processed)

    def test_sample_registry(self):

        order = Order.objects.get(order_number="123ABC@#$")
        recv = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=500
        )
        recv.save(orguser=order.created_by)
        self.assertEqual(recv.samples.count(), 500)

        s = Sample.objects.latest('id')
        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(s.basket), "10".format(yday))
        self.assertEqual(s.basket_position, 50)

        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=2
        )

        with self.assertRaises(KeyError):
            s.save()

        s.save(orguser=order.created_by)

        s = Sample.objects.latest('created')
        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(s.basket), "11".format(yday))
        self.assertEqual(s.basket_position, 2)

        self.assertRaises(KeyError, s.save)


    def test_sample_registry_no_basket(self):

        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=10,
            allocate_basket=False
        )
        s.save(orguser=order.created_by)

        s = Sample.objects.latest('id')
        self.assertEqual(s.basket, None)
        self.assertEqual(s.basket_position, None)
        startpk = Sample.objects.earliest("id").id
        endpk = Sample.objects.latest("id").id

        self.assertEqual(endpk-startpk+1, 10)


    def test_empties_in_file(self):
        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=15
        )
        s.save(orguser=order.created_by)
        s = Sample.objects.latest('id')
        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(s.basket), "1".format(yday))
        self.assertEqual(s.basket_position, 15)
        self.assertEqual(s.basket.file.open_slots, 385)

    def test_bump_to_next_file(self):
        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=395
        )
        s.save(orguser=order.created_by)

        s1 = Sample.objects.latest('id')
        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(s1.basket), "8".format(yday))
        self.assertEqual(s1.basket_position, 45)

        order.order_number = "123432"
        order.pk = None
        order.save()
        order = Order.objects.get(order_number="123ABC@#$")
        o2 = Order.objects.get(order_number="123432")

        self.assertNotEqual(order.pk, o2.pk)
        s = SampleRegistry(
            order=o2,
            depth=6,
            depth_unit="I",
            num_samples=20
        )

        s.save(orguser=order.created_by)

        s2 = Sample.objects.latest('id')
        yday = datetime.datetime.now().timetuple().tm_yday
        self.assertEqual(str(s2.basket), "9".format(yday))
        self.assertEqual(s2.basket_position, 20)
        self.assertEqual(s2.basket.file.status, 'O')
        s1.refresh_from_db()
        self.assertEqual(s1.basket.file.status, 'C')

    def test_create_fileupload_parser(self):
        fup = FileUploadParser.objects.create(
            name="Test FUP",
            skip_lines=0,
            pattern='(?P<basket>[-+]?[0-9]*\.?[0-9]+.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]+.),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]+.)\\n',
        )
        self.assertEqual(fup.check_test_line,
                         "{}")
        self.assertEqual(str(fup), "Test FUP")

        fup.test_line = "436-1818963,6.14\n"
        self.assertEqual(fup.check_test_line,
                         "{'basket': '436', 'sample': '1818963', 'tp_pH': '6.14'}")

        fup.test_line = "4361818963,6.14\n"
        self.assertEqual(fup.check_test_line,
                         "{}")

    def test_parse_line(self):
        TestParameter.objects.create(
            name="pH",
            unit=MeasurementUnit.objects.create(unit="units"),
            significant_digits=1,
            index=1
        )

        fup = FileUploadParser.objects.create(
            name="Test FUP",
            skip_lines=0,
            pattern='(?P<basket>[-+]?[0-9]*\.?[0-9]*.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]*.),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]*.)\\n',
        )

        order = Order.objects.get(order_number="123ABC@#$")
        lines = ["1-4651631,4.2\n"]
        for line in lines:
            sample, new, old = fup.parse_line(line, order.created_by)[0]
            self.assertEqual((sample, new, old), ('4651631', None, None))

        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=5
        )
        s.save(orguser=order.created_by)
        samples = Sample.objects.filter(order=order)
        lines = ["{0}-{1},{2}\n".format(s.basket_position, s.pk, random.gauss(6, 0.1)) for s in samples]
        for line in lines:
            # import pdb; pdb.set_trace()
            sample, new, old = fup.parse_line(line, order.created_by)[0]
            self.assertEqual(old, None)
            self.assertNotEqual(new, None)

    def test_parse_line_melt(self):
        TestParameter.objects.create(
            name="pH",
            unit=MeasurementUnit.objects.create(unit="units"),
            significant_digits=1,
            index=1
        )

        fup = FileUploadParser.objects.create(
            name="Test FUP2",
            skip_lines=0,
            pattern='(?P<basket>[-+]?[0-9]*\.?[0-9]*.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]*.),(?P<tp_name>[a-zA-Z]+),(?P<value>[-+]?[0-9]*\.?[0-9]*.)\\n',
            test_line="1-1,pH,6.2\n"
        )

        self.assertTrue(fup.check_test_line)
        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=5
        )
        s.save(orguser=order.created_by)
        samples = Sample.objects.filter(order=order)
        lines = ["{0}-{1},pH,{2}\n".format(s.basket_position, s.pk, random.gauss(6, 0.1)) for s in samples]
        for line in lines:
            sample, new, old = fup.parse_line(line, order.created_by, False)[0]
            self.assertEqual(old, None)
            self.assertNotEqual(new, None)

        lines = ["{0}-{1},FSN,{2}\n".format(s.basket_position, s.pk, random.gauss(6, 0.1)) for s in samples]
        for line in lines:
            sample, new, old = fup.parse_line(line, order.created_by)[0]
            self.assertEqual(old, None)
            self.assertEqual(new, 'FSN')

    @override_settings(MEDIA_ROOT=settings.BASE_DIR)
    def test_parse_file(self):
        TestParameter.objects.create(
            name="pH",
            unit=MeasurementUnit.objects.create(unit="units"),
            significant_digits=1,
            index=1
        )

        fup = FileUploadParser.objects.create(
            name="Test FUP",
            skip_lines=0,
            pattern='(?P<basket>[-+]?[0-9]*\.?[0-9]*.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]*.),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]*.)',
        )

        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=5
        )
        s.save(orguser=order.created_by)
        samples = Sample.objects.filter(order=order)
        lines = ["{0}-{1},{2}\n".format(s.basket_position, s.pk, random.gauss(6, 0.1)) for s in samples]
        f = open("samples/tests/ph_test.csv", "w")
        f.write("".join(lines))
        f.close()

        res = fup.parse_file(FileFile(f), order.created_by)
        self.assertEqual(len(res), 5)

        for r in res:
            _, l, _ = r[0]
            self.assertNotEqual(l.pk, None)

        res = fup.parse_file(FileFile(f), order.created_by, void=True)
        self.assertEqual(len(res), 5)

        for r in res:
            _, _, o = r[0]
            self.assertTrue(o.void)

    # def test_file_upload(self):
    #     fup = FileUploadParser.objects.create(
    #         name="Test FUP",
    #         skip_lines=0,
    #         pattern='(?P<basket>[-+]?[0-9]*\.?[0-9]*.)\\-(?P<sample>[-+]?[0-9]*\.?[0-9]*.),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]*.)\\n',
    #     )
    #
    #     mockfile = SimpleUploadedFile("ph_test.csv", b"Some random contents")
    #     fupload = FileUpload.objects.create(
    #         file=mockfile,
    #         parser=fup
    #     )
    #
    #     self.assertEqual(str(fupload), fupload.file.name)

    @override_settings(MEDIA_ROOT=settings.BASE_DIR)
    def test_void_measurements(self):
        TestParameter.objects.create(
            name="pH",
            unit=MeasurementUnit.objects.create(unit="units"),
            significant_digits=1,
            index=1
        )

        fup = FileUploadParser.objects.create(
            name="Test FUP",
            skip_lines=0,
            pattern='(?P<basket>[0-9]+)\-(?P<sample>[0-9]+),(?P<tp_pH>[-+]?[0-9]*\.?[0-9]*.)',
        )

        order = Order.objects.get(order_number="123ABC@#$")
        s = SampleRegistry(
            order=order,
            depth=6,
            depth_unit="I",
            num_samples=5
        )
        s.save(orguser=order.created_by)
        samples = Sample.objects.filter(order=order)
        lines = ["{0}-{1},{2}\n".format(s.basket_position, s.pk, random.gauss(6, 0.1)) for s in samples]
        f = open("samples/tests/ph_test.csv", "w")
        f.write("".join(lines))
        f.close()

        res = fup.parse_file(FileFile(f), order.created_by)
        self.assertEqual(len(res), 5)

        # res = fup.parse_file(FileFile(f), order.created_by)
        # lm = LabMeasurement.objects.filter(sample__order=order, void=True)
        # self.assertEqual(lm.count(), 5)
