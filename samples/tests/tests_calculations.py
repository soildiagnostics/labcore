import random

from django.contrib.auth.models import User
from django.test import TestCase

from products.models import MeasurementUnit
from samples.models import *
from samples.templatetags.calculated_parameters import *
from samples.tests.tests_models import SampleTestCase


class CalculationsTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        stc = SampleTestCase()
        stc.setUp()

        self.user = User.objects.get(username="testinguser")
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                location=stc.create_multipoint(stc.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)
            self.assertFalse(s.is_processed)

            params = s.get_test_parameters
            for p in params:
                l = LabMeasurement(
                    sample=s,
                    parameter=p,
                    value=random.gauss(10, 1)
                )

                l.save(orguser=order.created_by)

                self.assertIn(str(p), str(l))

            self.assertTrue(s.is_processed)

        p = Product.objects.get(name="FertiSaver-N")
        u, created = MeasurementUnit.objects.get_or_create(unit="lb/ac")
        par, created = TestParameter.objects.get_or_create(name="K", unit=u, index=3)
        p.test_parameters.add(par)

        u, created = MeasurementUnit.objects.get_or_create(unit="lb/ac")
        par, created = TestParameter.objects.get_or_create(name="Mg", unit=u, index=3)
        p.test_parameters.add(par)

        u, created = MeasurementUnit.objects.get_or_create(unit="lb/ac")
        par, created = TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        p.test_parameters.add(par)

        u, created = MeasurementUnit.objects.get_or_create(unit="% base saturation")
        par, created = TestParameter.objects.get_or_create(name="K", unit=u, index=3)
        p.test_parameters.add(par)

        u, created = MeasurementUnit.objects.get_or_create(unit="% base saturation")
        par, created = TestParameter.objects.get_or_create(name="Mg", unit=u, index=3)
        p.test_parameters.add(par)

        u, created = MeasurementUnit.objects.get_or_create(unit="% base saturation")
        par, created = TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        p.test_parameters.add(par)

    def test_add_labmeasurements(self):
        kpar = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        sample = Sample.objects.last()

        l = LabMeasurement(sample=sample,
                           parameter=kpar,
                           value=400)
        l.save(orguser=sample.order.created_by)
        self.assertTrue(l.pk != None)

    def test_calculate_function(self):
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        sample = Sample.objects.last()

        kpar = TestParameter.objects.get(name="Ca", unit__unit="lb/ac")

        l = LabMeasurement(sample=sample,
                           parameter=kpar,
                           value=400)
        l.save(orguser=sample.order.created_by)

        val = calculate(sample, par)
        self.assertEqual(val, 200)

    def test_missing_calculation(self):
        u, created = MeasurementUnit.objects.get_or_create(unit="ppb")
        par, created = TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        sample = Sample.objects.last()

        kpar = TestParameter.objects.get(name="Ca", unit__unit="lb/ac")

        l = LabMeasurement(sample=sample,
                           parameter=kpar,
                           value=400)
        l.save(orguser=sample.order.created_by)

        val = calculate(sample, par)
        self.assertEqual(val, "???")

    def test_record_cec_and_base_saturation(self):
        u, created = MeasurementUnit.objects.get_or_create(unit="ppb")
        v, created = MeasurementUnit.objects.get_or_create(unit="% base saturation")
        w, created = MeasurementUnit.objects.get_or_create(unit="meq/100g")
        x, created = MeasurementUnit.objects.get_or_create(unit="units")
        TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        TestParameter.objects.get_or_create(name="K", unit=u, index=3)
        TestParameter.objects.get_or_create(name="Mg", unit=u, index=3)
        TestParameter.objects.get_or_create(name="Ca", unit=v, index=3)
        TestParameter.objects.get_or_create(name="K", unit=v, index=3)
        TestParameter.objects.get_or_create(name="Mg", unit=v, index=3)
        TestParameter.objects.get_or_create(name="H", unit=v, index=3)
        TestParameter.objects.get_or_create(name="CEC", unit=w, index=3)
        TestParameter.objects.get_or_create(name="pH Water", unit=x, index=3)
        TestParameter.objects.get_or_create(name="pH Buffer", unit=x, index=3)

        sample = Sample.objects.last()

        capar = TestParameter.objects.get(name="Ca", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=8298)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=214)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="Mg", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=1226)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="pH Water", unit__unit="units")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=6.5)
        l.save(orguser=sample.order.created_by)

        self.assertTrue(record_cec_and_base_saturation(sample))

        lm = LabMeasurement.objects.get(
            sample=sample,
            parameter__name="CEC"
        )

        self.assertAlmostEqual(float(lm.value), 26.1277, places=2)

        pcksat = LabMeasurement.objects.get(
            sample=sample,
            parameter__name="K",
            parameter__unit__unit="% base saturation"
        )
        self.assertAlmostEqual(float(pcksat.value), 1.0501, places=2)

        pccasat = LabMeasurement.objects.get(
            sample=sample,
            parameter__name="Ca",
            parameter__unit__unit="% base saturation"
        )
        self.assertAlmostEqual(float(pccasat.value), 79.398, places=2)

        pcmgsat = LabMeasurement.objects.get(
            sample=sample,
            parameter__name="Mg",
            parameter__unit__unit="% base saturation"
        )
        self.assertAlmostEqual(float(pcmgsat.value), 19.55, places=2)

        l = LabMeasurement.objects.filter(sample=sample,
                                          parameter__name="pH Buffer").last()
        self.assertEqual(l.value, 7.0)

    def test_get_parameter(self):

        u, created = MeasurementUnit.objects.get_or_create(unit="ppb")
        v, created = MeasurementUnit.objects.get_or_create(unit="% base saturation")
        w, created = MeasurementUnit.objects.get_or_create(unit="meq/100g")
        TestParameter.objects.get_or_create(name="Ca", unit=u, index=3)
        TestParameter.objects.get_or_create(name="K", unit=u, index=3)
        TestParameter.objects.get_or_create(name="Mg", unit=u, index=3)
        TestParameter.objects.get_or_create(name="Ca", unit=v, index=3)
        TestParameter.objects.get_or_create(name="K", unit=v, index=3)
        TestParameter.objects.get_or_create(name="Mg", unit=v, index=3)
        TestParameter.objects.get_or_create(name="CEC", unit=w, index=3)
        TestParameter.objects.get_or_create(name="H", unit=v, index=3)
        u, created = MeasurementUnit.objects.get_or_create(unit="units")
        TestParameter.objects.get_or_create(name="pH Water", unit=u, index=3)
        TestParameter.objects.get_or_create(name="pH Buffer", unit=u, index=3)

        sample = Sample.objects.last()

        capar = TestParameter.objects.get(name="Ca", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=8298)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=214)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="Mg", unit__unit="lb/ac")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=1226)
        l.save(orguser=sample.order.created_by)

        capar = TestParameter.objects.get(name="pH Water", unit__unit="units")
        l = LabMeasurement(sample=sample,
                           parameter=capar,
                           value=6.5)
        l.save(orguser=sample.order.created_by)

        self.assertAlmostEqual(float(get_cec_and_base_sat(sample, "K").value), 1.05, places=2)
        # first time through the exception path

        self.assertAlmostEqual(float(get_cec_and_base_sat(sample, "K").value), 1.05, places=2)
        # second time through the non-exception path.

    def test_buffer_ph_calculation(self):
        u, created = MeasurementUnit.objects.get_or_create(unit="units")
        phw, created = TestParameter.objects.get_or_create(name="pH Water", unit=u, index=3)
        phb, created = TestParameter.objects.get_or_create(name="pH Buffer", unit=u, index=3)

        sample = Sample.objects.last()

        l = LabMeasurement(sample=sample,
                           parameter=phw,
                           value=6.0)
        l.save(orguser=sample.order.created_by)

        val = calculate(sample, phb)
        self.assertEqual(val.value, 6.6)

        l.value = 6.8
        l.save(orguser=sample.order.created_by)

        val = calculate(sample, phb)
        self.assertEqual(val.value, 7.0)


