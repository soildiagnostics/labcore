import random
from datetime import datetime

from django import forms
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.http import HttpRequest
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from associates.models import OrganizationUser, Organization
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company
from orders.models import Order, OrderStatus
from products.models import Category, Product, MeasurementUnit, TestParameter, Discount
from samples.admin import SampleRegistryForm, SampleRegistryAdmin, products_and_test_parameters
from samples.models import (File, Basket, Sample, LabMeasurement,
                            SampleRegistry, BASKET_MAX_POSITIONS)
from samples.tests.tests_models import SampleTestCase


class FileCentricAdmin(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        stc = SampleTestCase()
        stc.setUp()

        self.user = User.objects.get(username="testinguser")
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                location=stc.create_multipoint(stc.urbana, 3),
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)
            self.assertFalse(s.is_processed)

            params = s.get_test_parameters
            for p in params:
                l = LabMeasurement(
                    sample=s,
                    parameter=p,
                    value=random.gauss(10, 1)
                )

                l.save(orguser=order.created_by)

                self.assertIn(str(p), str(l))

            self.assertTrue(s.is_processed)

    def test_admin_views(self):
        user = self.user
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        client = Client()

        client.force_login(user)

        pages = [
            '/admin/samples/',
            '/admin/samples/basket/',
            '/admin/samples/file/',
            '/admin/samples/labmeasurement/',
            '/admin/samples/ordersummary/',
            # '/admin/samples/filesummary/',
            # '/admin/samples/samplesummary/',
            '/admin/samples/sample/',
        ]

        for page in pages:
            response = client.get(page)
            self.assertEqual(response.status_code, 200)

    def test_void_toggle_post(self):
        user = User.objects.get(username="testinguser")
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        client = Client()

        client.force_login(user)

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="123ABC@#$")
        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)
            self.assertFalse(s.is_processed)

            params = s.get_test_parameters
            for p in params:
                l = LabMeasurement(
                    sample=s,
                    parameter=p,
                    value=random.gauss(10, 1)
                )

                l.save(orguser=order.created_by)

                resp = client.post(reverse("toggle_void", kwargs={'pk': l.pk}))
                self.assertEqual(resp.json()['void'], True)

                resp = client.post(reverse("toggle_void", kwargs={'pk': l.pk + 999}))
                self.assertEqual(resp.json()['error'], 'No such measurement')


class MockRequest:
    pass


class MockSuperUser:
    def has_perm(self, perm):
        return True


request = MockRequest()
request.user = MockSuperUser()


class SampleRegistryAdminTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.is_staff = True
        self.user.is_superuser = True
        self.user.save()

        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        self.field = ClientsTestCase().createField()

        self.ouser = OrganizationUser.objects.get(user__username="testinguser")
        self.order = SampleTestCase().create_order(self.field, self.ouser)

        self.site = AdminSite()

    def test_initial_add_form(self):
        request = HttpRequest()
        request.user = self.user
        self.client.force_login(self.user)
        form = SampleRegistryForm(request)
        self.assertEqual(type(form.fields['order_number'].widget), type(forms.HiddenInput()))
        response = self.client.get("/admin/samples/sampleregistry/add/")
        self.assertEqual(response.status_code, 200)

    def test_initial_change_form(self):
        sreg = SampleRegistry(
            order=self.order,
            num_samples=10,
            gps_acres=40,
            sampled=datetime.now(),
            additional_charges='',
            comments='',
            allocate_basket=True,
        )
        sreg.save(orguser=self.ouser)
        self.client.force_login(self.user)
        response = self.client.get(f"/admin/samples/sampleregistry/{sreg.id}/change/")
        self.assertEqual(response.status_code, 200)

        request = HttpRequest()
        request.user = self.user
        form = SampleRegistryForm(instance=sreg)
        self.assertEqual(type(form.fields['full_order'].widget), type(forms.HiddenInput()))

    def test_add_view_admin(self):
        request = HttpRequest()
        request.user = self.user
        data = {'full_order': self.order.id,
                'num_samples': 10,
                'custom_start_number': 1,
                'gps_acres': 40,
                # 'sampled': datetime.now().date().isoformat(),
                # 'additional_charges': '',
                # 'comments': '',
                'allocate_basket': True,
                # 'depth': None,
                'depth_unit': 'I',
                # 'basket': 37,
                # 'basket_position': 32,
                # 'duplicate_positions': '-1',
                }
        ma = SampleRegistryAdmin(SampleRegistry, self.site)
        self.client.force_login(self.user)
        self.assertEqual(list(ma.get_form(request).base_fields), ['full_order',
                                                                  'order_number',
                                                                  'order', 'num_samples',
                                                                  'custom_start_number',
                                                                  'allocate_basket',
                                                                  'gps_acres', 'sampled', 'comments',
                                                                  'additional_charges', 'depth', 'depth_unit', 'basket',
                                                                  'basket_position'])

        resp = self.client.post(f"/admin/samples/sampleregistry/add/", data=data)
        self.assertEqual(resp.status_code, 200)

        context = resp.context
        # self.assertEqual(context['orguser'], self.user)
        self.assertEqual(context['allocate'], True)
        self.assertEqual(context['empties'], 400)
        self.assertEqual(context['file'], File.objects.last())  # 123.1  doy.1
        self.assertEqual(context['new_file'], False)
        self.assertEqual(context['first_id'], 1)
        self.assertEqual(context['last_id'], 10)
        self.assertEqual(context['basket'], Basket.objects.last())
        self.assertEqual(context['position'], 1)
        self.assertNotIn('_confirmsave', context.keys())

    def test_confirm_view(self):
        request = HttpRequest()
        request.user = self.user
        data = {'full_order': self.order.id,
                'order': self.order.id,
                'num_samples': 10,
                'custom_start_number': 1,
                'gps_acres': 40,
                # 'sampled': datetime.now().date().isoformat(),
                # 'additional_charges': '',
                'comments': 'new registry',
                'allocate_basket': True,
                # 'depth': 12,
                'depth_unit': 'I',
                'basket': Basket.objects.last().id,
                # 'basket_position': 32,
                'duplicate_positions': '-1',
                '_confirmsave': "Yes, I'm sure",
                }
        self.client.force_login(self.user)
        self.assertEqual(SampleRegistry.objects.count(), 0)
        resp = self.client.post(f"/admin/samples/sampleregistry/add/", data=data)
        self.assertEqual(resp.status_code, 302)
        # print(resp.context)
        self.assertEqual(SampleRegistry.objects.count(), 1)
        reg = SampleRegistry.objects.last()
        self.assertEqual(reg.basket, Basket.objects.last())
        self.assertEqual(reg.basket_position, 1)
        self.assertEqual(reg.comments, 'new registry')
        self.assertEqual(reg.depth, None)

    def test_sample_basket_assignment(self):
        request = HttpRequest()
        request.user = self.user
        data = {'full_order': self.order.id,
                'order': self.order.id,
                'num_samples': 505,
                'custom_start_number': 1,
                'gps_acres': 40,
                # 'sampled': datetime.now().date().isoformat(),
                # 'additional_charges': '',
                'comments': 'new registry',
                'allocate_basket': True,
                # 'depth': 12,
                'depth_unit': 'I',
                'basket': Basket.objects.last().id,
                # 'basket_position': 32,
                # 'duplicate_positions': '-1',
                }
        self.client.force_login(self.user)
        self.assertEqual(SampleRegistry.objects.count(), 0)

        with self.assertRaises(NotImplementedError):
            self.client.post(f"/admin/samples/sampleregistry/add/", data=data)

        data['num_samples'] = 345
        resp = self.client.post(f"/admin/samples/sampleregistry/add/", data=data)
        self.assertEqual(resp.status_code, 200)

        # context check for multiple baskets, files
        context = resp.context
        self.assertEqual(context['allocate'], True)
        self.assertEqual(context['empties'], 400)
        self.assertEqual(context['file'], File.objects.last())  # 123.1  doy.1
        self.assertEqual(context['new_file'], False)
        self.assertEqual(context['first_id'], 1)
        self.assertEqual(context['last_id'], 345)
        self.assertEqual(context['basket'], Basket.objects.last())
        self.assertEqual(context['position'], 1)
        self.assertNotIn('_confirmsave', context.keys())
        self.assertEqual(context['multiple_baskets'], True)
        self.assertEqual(context['baskets'][0]['basket'], 'First basket')
        self.assertEqual(str(context['baskets'][0]['name']), Basket.objects.last().name)
        self.assertEqual(context['baskets'][0]['first_id'], 1)
        self.assertEqual(context['baskets'][0]['first_position'], 1)
        self.assertEqual(context['baskets'][0]['last_id'], 50)
        self.assertEqual(context['baskets'][0]['last_position'], 50)
        self.assertEqual(context['baskets'][1]['first_id'], 51)
        self.assertEqual(context['baskets'][1]['first_position'], 1)
        self.assertEqual(context['baskets'][1]['last_id'], 100)
        self.assertEqual(context['baskets'][1]['last_position'], 50)
        self.assertEqual(context['baskets'][2]['first_id'], 101)
        self.assertEqual(context['baskets'][2]['first_position'], 1)
        self.assertEqual(context['baskets'][2]['last_id'], 150)
        self.assertEqual(context['baskets'][2]['last_position'], 50)
        self.assertEqual(context['baskets'][0]['sample_cnt'], BASKET_MAX_POSITIONS)
        self.assertEqual(context['baskets'][6]['basket'], 'Seventh basket')
        self.assertEqual(context['baskets'][6]['name'], int(Basket.objects.last().name) + 6)
        self.assertEqual(context['baskets'][6]['sample_cnt'], data['num_samples'] - BASKET_MAX_POSITIONS * 6)


class SampleProductAdminTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()

        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="TestOrder1",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "TestOrder1")
        cat, created = Category.objects.get_or_create(name="Soil Test")
        p = Product.objects.create(name="FertiSaver-N", category=cat, created_by=ouser.organization)
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=2)

        p.test_parameters.add(par)

        par, created = TestParameter.objects.get_or_create(name="Calculated", unit=u, calculated=True, index=3)
        p.test_parameters.add(par)
        self.assertEqual(len(p.test_parameters.all()), 2)

        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        self.urbana = Point(40.111951, -88.230055)

    def test_sample_products_and_test_parameters(self):
        f = File.objects.create(name="123.1")
        b = Basket.objects.create(file=f, name="456")

        order = Order.objects.get(order_number="TestOrder1")

        for i in range(3):
            s = Sample(
                order=order,
                order_serial=i,
                depth=12,
                basket=b,
                basket_position=4 + i
            )

            s.save(orguser=order.created_by)
            self.assertFalse(s.is_processed)

            params = s.get_test_parameters
            for p in params:
                l = LabMeasurement(
                    sample=s,
                    parameter=p,
                    value=random.gauss(10, 1)
                )

                self.assertRaises(KeyError, l.save)
                l.save(orguser=order.created_by)
                l.save(orguser=order.created_by)  # no change!
                l.value += random.gauss(10, 2)
                l.save(orguser=order.created_by)

                self.assertIn(str(p), str(l))

            self.assertTrue(s.is_processed)

        products = Product.objects.filter(order=order)

        # will include all test paramters including calculated and non calculated
        organic = TestParameter.objects.get(name="Organic N")
        calc = TestParameter.objects.get(name="Calculated")
        products_qs, prod_test_params, unique_parameters = products_and_test_parameters(products,
                                                                                        include_calculated=True)
        self.assertEqual(len(unique_parameters), 2)
        self.assertIn(organic, unique_parameters)
        self.assertIn(calc, unique_parameters)

        # will include only non calculated test params
        products_qs, prod_test_params, unique_parameters = products_and_test_parameters(products,
                                                                                        include_calculated=False)
        self.assertEqual(len(unique_parameters), 1)
        self.assertIn(organic, unique_parameters)
        self.assertNotIn(calc, unique_parameters)
