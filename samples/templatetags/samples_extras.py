from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import Avg, StdDev

from products.models import Product
# import decimal
from samples.admin import process_samples_dict, products_and_test_parameters
from samples.models import LabMeasurement, Sample
from samples.templatetags.calculated_parameters import calculate

register = template.Library()

@register.filter
def measurement(sample, parameter):
    qs = LabMeasurement.objects.filter(sample=sample, parameter=parameter, void=False)
    if qs.exists():
        lm = qs.latest('created')
        return bdl_floatformat(lm)
    return "No data"

@register.filter
def all_measurements(sample, parameter):
    qs = LabMeasurement.objects.filter(sample=sample, parameter=parameter)
    return qs

@register.filter
def bdl_floatformat(measurement):
    #measurement.value = measurement.value + decimal.Decimal(0.0000001) # This is to force the value to look nice...
    ## round(1, 2) = 1, and it should look like 1.00
    parameter = measurement.parameter
    if measurement.detection_limit and measurement.value < measurement.detection_limit:
        return "BDL"
    if parameter.significant_digits == 0:
        retval = intcomma(int(round(measurement.value)))
    else:
        retval = intcomma(round(measurement.value, parameter.significant_digits))

    if measurement.measurement_error:
        retval = f"{retval} ± {intcomma(round(measurement.measurement_error, parameter.significant_digits))}"
    return retval

@register.filter
def calculated_parameter(sample, parameter):
    if parameter.name in ["Lime", "Organic N", "Seeding rate", "Yield Estimate", "Nitrogen"]:
        return measurement(sample, parameter)
    m = calculate(sample, parameter)
    if isinstance(m, str):
        return m
    return bdl_floatformat(m)

@register.filter
def complete_for_product(sample, product):
    try:
        return sample.productset[product]['is_complete']
    except KeyError:
        return None

@register.filter
def average_for_order(par, order):
    stats = LabMeasurement.objects.filter(sample__order=order,
                                          parameter=par,
                                          void=False).aggregate(Avg("value"),
                                                                StdDev("value"))
    try:
        avg = round(stats['value__avg'], par.significant_digits)
    except TypeError:
        avg = None
    try:
        stddev = round(stats['value__stddev'], par.significant_digits)
    except TypeError:
        stddev = None
    return f"{avg}±{stddev}"

@register.filter
def mean_for_order(par, order):
    stats = LabMeasurement.objects.filter(sample__order=order,
                                          parameter=par,
                                          void=False).aggregate(Avg("value"))

    try:
        avg = intcomma(round(stats['value__avg'], par.significant_digits))
        if par.significant_digits==0:
            avg = intcomma(int(round(stats['value__avg'], 0)))
    except TypeError:
        avg = None
    return avg

@register.filter
def stdev_for_order(par, order):
    stats = LabMeasurement.objects.filter(sample__order=order,
                                          parameter=par,
                                          void=False).aggregate(StdDev("value"))
    try:
        stddev = round(stats['value__stddev'], par.significant_digits)
    except TypeError:
        stddev = None
    return f"{stddev}"

@register.filter
def pending_for_parameter(sample, parameter):
    return sample.get_lab_measurements_for_parameter(parameter).count() == 0

@register.filter
def sample_product_items(sample, product):
    return sample.productset[product].items()

# @register.filter
# def sample_product_recent_items(sample, product):
#     out =  sample.productset[product].items()
#     return out

@register.filter
def num_samples_for_file(order, file):
    return order.num_samples_for_file(file)

@register.filter
def sample_range_for_file(order, file):
    return list(order.sample_range_for_file(file))

@register.simple_tag
def to_list(*args):
    return args

@register.filter
def org_list(basket_orgs, basket):
    return ",  ".join(basket_orgs[basket.id])

@register.simple_tag(takes_context=True)
def process_samples_list_for_order(context, order):
    context['count'] = context.get('count', 0) + 1
    samples = Sample.objects.filter(order=order)
    products = Product.objects.filter(order=order)
    products_qs, prod_test_params, unique_parameters = products_and_test_parameters(products, include_calculated=True)
    reads = process_samples_dict(samples, products_qs, prod_test_params, unique_parameters, last_valid=True)

    return reads


@register.filter
def count_for_key(dictionary, key):
    val = dictionary.get(key)
    if not val:
        return ""
    return val