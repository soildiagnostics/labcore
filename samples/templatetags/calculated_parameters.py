from products.models import TestParameter
from samples.models import LabMeasurement, Sample


def get_sample_value(sample: Sample, parameter_name: str, unit: str) -> float:
    try:
        tpar = TestParameter.objects.get(name=parameter_name,
                                         calculated=False,
                                         unit__unit=unit)

        labmeasurement = LabMeasurement.objects.filter(sample=sample,
                                                       parameter=tpar,
                                                       void=False).latest('created')
        return float(labmeasurement.value)
    except TestParameter.DoesNotExist:
        raise
    except LabMeasurement.DoesNotExist:
        raise


def cec_base_sat_update_helper(sample: Sample, parameter: str, value: float) -> LabMeasurement:
    orguser = sample.order.created_by
    if parameter == "CEC":
        unit = "meq/100g"
    elif parameter == "pH Buffer":
        unit = "units"
    else:
        unit = "% base saturation"

    try:
        lm = LabMeasurement.objects.get(
            sample=sample,
            parameter=TestParameter.objects.get(name=parameter,
                                                unit__unit=unit)
        )
    except TestParameter.DoesNotExist:
        print("Test Par does not exist in cec")
        raise

    except LabMeasurement.DoesNotExist:
        lm = LabMeasurement(
            sample=sample,
            parameter=TestParameter.objects.get(name=parameter,
                                                unit__unit=unit)
        )
    lm.value = value
    lm.save(orguser=orguser)
    return lm


def buffer_ph_from_water(sample: Sample) -> LabMeasurement:
    water_ph = get_sample_value(sample, 'pH Water', 'units')
    orguser = sample.order.created_by
    try:
        ph_buffer = LabMeasurement.objects.get(sample=sample,
                                               parameter__name="pH Buffer")
        if ph_buffer.value != min(water_ph + 0.6, 7.0):
            ph_buffer.value = min(water_ph + 0.6, 7.0)
            ph_buffer.save(orguser=orguser)
    except LabMeasurement.DoesNotExist:
        ph_buffer = LabMeasurement(
            sample=sample,
            parameter=TestParameter.objects.get(name="pH Buffer"),
            value=min(water_ph + 0.6, 7.0)
        )
        ph_buffer.save(orguser=orguser)

    return ph_buffer


def record_cec_and_base_saturation(sample: Sample) -> bool:
    # get meqs of ions
    try:
        # Reference: https://www.spectrumanalytic.com/support/library/ff/CEC_BpH_and_percent_sat.htm
        potassium = get_sample_value(sample, 'K', 'lb/ac')
        magnesium = get_sample_value(sample, 'Mg', 'lb/ac')
        calcium = get_sample_value(sample, 'Ca', 'lb/ac')
        # sodium = get_sample_value(sample, 'Na', 'ppm') * 2 ## convert to lb/ac
        buffer_pH = float(buffer_ph_from_water(sample).value)

        H_meq = (7 - buffer_pH) / 20
        K_meq = potassium / 780
        Mg_meq = magnesium / 240
        Ca_meq = calcium / 400
        # Na_meq = sodium / 460 ## Not used in CEC calculations by GMS.

        cec = K_meq + Mg_meq + Ca_meq + H_meq
        K_pc_basesat = K_meq / cec * 100
        Mg_pc_basesat = Mg_meq / cec * 100
        Ca_pc_basesat = Ca_meq / cec * 100
        H_pc_basesat = H_meq / cec * 100

        cec_base_sat_update_helper(sample, 'K', K_pc_basesat)
        cec_base_sat_update_helper(sample, 'Ca', Ca_pc_basesat)
        cec_base_sat_update_helper(sample, 'Mg', Mg_pc_basesat)
        cec_base_sat_update_helper(sample, 'H', H_pc_basesat)
        cec_base_sat_update_helper(sample, 'CEC', cec)
        cec_base_sat_update_helper(sample, 'pH Buffer', buffer_pH)
        return True

    except Exception:
        # print(f"Exception in computing CEC and related values for sample {sample}: {e}")
        raise


def get_cec_and_base_sat(sample: Sample, parameter: str) -> LabMeasurement:
    if parameter == "CEC":
        unit = "meq/100g"
    elif parameter == "pH Buffer":
        unit = "units"
    else:
        unit = "% base saturation"

    ## Check if the last calculation of CEC was earlier than the most recent
    ## data for the order.
    try:
        try:
            val = LabMeasurement.objects.select_related('parameter').get(
                sample=sample,
                parameter__name=parameter,
                parameter__unit__unit=unit
            )
            last_data = LabMeasurement.objects.filter(sample=sample,
                                                      parameter__unit__unit="lb/ac",
                                                      created__gte=val.created).exists()
            assert not last_data
            return val
        except (LabMeasurement.DoesNotExist, AssertionError):

            first_calc = LabMeasurement.objects.filter(sample=sample,
                                                       parameter__unit__unit=unit).earliest('created').created
            last_data = LabMeasurement.objects.filter(sample=sample,
                                                      parameter__unit__unit="lb/ac",
                                                      created__gte=first_calc).exists()

            if last_data:
                record_cec_and_base_saturation(sample)
    except LabMeasurement.DoesNotExist:
        record_cec_and_base_saturation(sample)

    val = LabMeasurement.objects.select_related('parameter').get(
        sample=sample,
        parameter__name=parameter,
        parameter__unit__unit=unit
    )

    return val


def get_psnt_recommendation(sample: Sample) -> str:
    try:
        v = get_sample_value(sample, 'Nitrate', 'ppm')
        if 0 <= v < 8:
            rec = "Very low nitrates, recommend 100-140 lbs N"
        elif 8 <= v < 15:
            rec = "Low nitrates, recommend 60-90 lbs N"
        elif 15 <= v < 23:
            rec = "Moderate nitrates, recommend 30-60 lbs N"
        elif 23 <= v <= 25:
            rec = "Borderline nitrates, recommend optional 10-30 lbs N"
        elif v > 25:
            rec = "Unlikely to respond to additional N"
        else:
            rec = "Invalid"
        return rec
    except Exception:
        return "???"


calculation_dictionary = {
    ("CEC", 'meq/100g'): lambda s: get_cec_and_base_sat(s, "CEC"),
    ("K", "% sat."): lambda s: get_cec_and_base_sat(s, "K"),
    ("Ca", "% sat."): lambda s: get_cec_and_base_sat(s, "Ca"),
    ("Mg", "% sat."): lambda s: get_cec_and_base_sat(s, "Mg"),
    ("H", "% sat."): lambda s: get_cec_and_base_sat(s, "H"),
    ("K", "ppm"): lambda s: get_sample_value(s, 'K', 'lb/ac') / 2,
    ('Mg', "ppm"): lambda s: get_sample_value(s, 'Mg', 'lb/ac') / 2,
    ('Ca', "ppm"): lambda s: get_sample_value(s, 'Ca', 'lb/ac') / 2,
    ('pH Buffer', "units"): lambda s: buffer_ph_from_water(s),
    ('N Recommendation', "lb/ac"): lambda s: get_psnt_recommendation(s),
}


def calculate(sample, parameter):
    try:
        func = calculation_dictionary.get((parameter.name, str(parameter.unit)))
        return func(sample)
    except Exception:
        return "???"
