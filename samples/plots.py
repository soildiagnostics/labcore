import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


def get_histogram_plot(**kwargs):
    try:
        data = kwargs.get('data')
        title = kwargs.get('title')
        value = kwargs.get('value')

        fig = px.histogram(data, x=value,
                           title=title,
                           height=500,
                           color_discrete_sequence=['rgb(65, 118, 144)'])
        fig.update_layout(plot_bgcolor='rgb(246, 246, 246)')
        return fig.to_html()
    except ValueError:
        return "No Data"


def get_time_series_plot(**kwargs):
    try:
        data = kwargs.get('data')
        title = kwargs.get('title')
        value = kwargs.get('value')
        time = kwargs.get('time')

        data['idx'] = pd.to_datetime(data[time])
        data = data.set_index('idx')
        df = data[[value]]
        df = df[value].apply(pd.to_numeric, errors='coerce')
        df = df.resample('1D', axis=0).ohlc()

        fig = go.Figure(go.Ohlc(x=df.index,
                                open=df['open'],
                                high=df['high'],
                                low=df['low'],
                                close=df['close'],
                                increasing_line_color='rgb(65, 118, 144)',
                                decreasing_line_color='rgb(65, 118, 144)'))

        fig.update_layout(title=title, height=500,
                          plot_bgcolor='rgb(246, 246, 246)')

        return fig.to_html()
    except (ValueError, KeyError):
        return "No Data"