from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('toggle/<int:pk>/', csrf_exempt(views.ToggleMeasurementVoid.as_view()), name="toggle_void"),
    ]