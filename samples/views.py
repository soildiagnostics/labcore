from django.http import JsonResponse
from django.views import View

from associates.models import OrganizationUser
from associates.role_privileges import DealershipRequiredMixin
from .models import LabMeasurement


# Create your views here.

class ToggleMeasurementVoid(DealershipRequiredMixin, View):

    def post(self, request, pk, *args, **kwargs):

        try:
            m = LabMeasurement.objects.get(pk=pk)
        except LabMeasurement.DoesNotExist:
            return JsonResponse({'error':'No such measurement'})

        m.void = not m.void
        orguser = OrganizationUser.objects.get(user=request.user)
        m.save(orguser=orguser)
        return JsonResponse({'measurement':m.pk, 'void':m.void})

