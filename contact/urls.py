from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from common.apihelper import apipath
from . import views
from .apiviews import APINewContact, APIEditDeleteContact

urlpatterns = [
    # path('', views.ContactList.as_view(), name="contact_list"),
    path('persons/', views.PersonList.as_view(), name="persons_list"),
    path('person/<int:pk>/',
         views.PersonUpdateView.as_view(), name="person_detail"),
    # path('companies/', views.CompanyList.as_view(), name="companies_list"),
    path('company/<int:pk>/',
         views.CompanyUpdateView.as_view(), name="company_detail"),
    # path('all/', views.ContactsListView.as_view(), name="all_contacts"),
    # path('company/add/', views.CompanyAddView.as_view(), name="add_company"),
    path('person/add/', views.PersonAddView.as_view(), name="add_person"),
    path('persons/<int:pk>/createuser/',
         views.CreateUserView.as_view(), name="create_user_from_person"),
    path('persons/<int:pk>/resetpassword/',
         views.ResendPasswordView.as_view(), name="resend_password")
]

urlpatterns += [
    ## Contacts
    apipath('create/', APINewContact.as_view(), name="api_new_contact"),
    apipath('edit/<int:pk>/', APIEditDeleteContact.as_view(), name="api_edit_delete_contact"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
