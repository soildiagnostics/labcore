# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.forms import PasswordResetForm
from django.core.exceptions import PermissionDenied, ValidationError
from django.http import JsonResponse, Http404
from django.shortcuts import HttpResponseRedirect
from django.urls import reverse
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.views.generic.list import ListView
from django.conf import settings
from rest_framework import generics

from associates.role_privileges import DealershipRequiredMixin, FilteredObjectMixin
from .forms import (CompanyForm, PersonForm, inlineFormHelper,
                    AddressModelFormSet, EmailModelFormSet, PhoneModelFormSet,
                    )
from .models import Person, Company, Contact
from .serializers import PersonSerializer, CompanySerializer, ContactSerializer


class PersonList(generics.ListCreateAPIView):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class PersonDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class CompanyList(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class ContactList(generics.ListCreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class ContactsListView(ListView):
    model = Contact
    paginate_by = 50

    def get_queryset(self):
        return Contact.objects.all()
        # TODO Filter by company


class ContactValidationMixin(FormView):
    def form_valid(self, form):
        context = self.get_context_data()
        phone = context['phone']
        email = context['email']
        address = context['address']

        if phone.is_valid() and email.is_valid() and address.is_valid() and form.is_valid():
            self.object = form.save()
            phone.instance = self.object
            phone.save()
            email.instance = self.object
            email.save()
            address.instance = self.object
            address.save()



            return HttpResponseRedirect(self.get_success_url())
        else:
            form.add_error(None, phone.errors)
            form.add_error(None, email.errors)
            form.add_error(None, address.errors)
            return self.render_to_response(self.get_context_data(form=form))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['helper'] = inlineFormHelper()
        context['formtype'] = "Update Profile"
        args = []
        if self.request.POST:
            args.append(self.request.POST)

        context['phone'] = PhoneModelFormSet(*args, instance=self.object)
        context['email'] = EmailModelFormSet(*args, instance=self.object)
        context['address'] = AddressModelFormSet(*args, instance=self.object)

        return context



class CompanyUpdateView(DealershipRequiredMixin, FilteredObjectMixin,
                        ContactValidationMixin, UpdateView):
    model = Company
    form_class = CompanyForm

    def get_success_url(self):
        return reverse("team_members")

    def get_context_data(self, **kwargs):
        context = super(CompanyUpdateView, self).get_context_data(**kwargs)
        context['formtype'] = "Update Company Profile"
        return context


class PersonUpdateView(DealershipRequiredMixin, FilteredObjectMixin, ContactValidationMixin, UpdateView):
    model = Person
    form_class = PersonForm
    instance_owner_organization = 'client__originator__organization'
    instance_owner_client = 'client'
    client_has_permission = True


class PersonAddView(ContactValidationMixin, CreateView):
    model = Person
    form_class = PersonForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formtype'] = "Add Person to Company"
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        co = self.request.GET.get("company")
        try:
            if co:
                kwargs["company"] = Company.objects.get(id=co)
        except Company.DoesNotExist:
            pass
        return kwargs


class CreateUserView(DealershipRequiredMixin, SingleObjectMixin, View):
    """
    The CreateUser request typically comes from a Client/Contact update page.
    """
    model = Person
    instance_owner_organization = "client__originator__organization"

    def reset_password_helper(self, request, user, extra=None):
        form = PasswordResetForm({'email': user.email})
        if form.is_valid():
            https = request.META['SERVER_PORT'] == 443
            sender = settings.DEFAULT_FROM_EMAIL
            form.save(
                request=request,
                from_email=sender,
                email_template_name="registration/password_reset_email.html",
                use_https=https
            )
            response = {'status': 'link_sent', 'user': user.username, 'email': user.email}
            if extra:
                response['message'] = extra
            return response

    def post(self, request, pk, *args, **kwargs):

        extra = None
        try:
            person = self.get_object()
        except Http404:
            ## maybe the contact is a Company?
            try:
                contact = Contact.objects.get(id=pk)
                person = contact.get_company_contact
                assert contact.is_company and person is not None
                extra = "Sending to company contact"
            except AssertionError as e:
                response = {'status': 'fail', 'message': "Couldn't find the contact"}
                return JsonResponse(response, status=500)
            except Exception as e:
                response = {'status': 'fail', 'message': str(e)}
                return JsonResponse(response, status=500)

        try:
            user = person.get_or_create_user_from_person(email=person.primary_emails[0], random_password=True)
            return JsonResponse(self.reset_password_helper(request, user, extra))
        except ValidationError as e:
            response = {'status': 'fail',
                        'message': str(e)}
            return JsonResponse(response, status=500)
        except IndexError as e:
            response = {'status': 'fail',
                        'message': "Index Error - Did you set an email to Login and mark it as Primary?"}
            return JsonResponse(response, status=500)


class ResendPasswordView(CreateUserView):

    def post(self, request, pk, *args, **kwargs):
        try:
            person = self.get_object()
            return JsonResponse(self.reset_password_helper(request, person.user))
        except Http404:
            try:
                person = Company.objects.get(id=pk).get_company_contact()
                return JsonResponse(self.reset_password_helper(request, person.user))
            except Exception:
                raise
        # return JsonResponse(response)
