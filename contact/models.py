# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from address.models import AddressField
from django.contrib.auth.models import User as django_User
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.db.models.signals import pre_save
from django.dispatch import receiver

PHONE_TYPES = (
    ('M', 'Mobile'),
    ('O', 'Office'),
    ('H', 'Home'),
    ('F', 'Fax'),
    ('X', 'Other'),
)

ADDRESS_TYPES = (
    ('O', 'Office'),
    ('H', 'Home'),
    ('X', 'Other'),
)

EMAIL_TYPES = (
    ('O', 'Office'),
    ('H', 'Home'),
    ('L', 'Login'),
    ('X', 'Other'),
)


class User(django_User):
    "Proxy model of auth.User to add custom functions"

    class Meta:
        proxy = True
        permissions = (
            ('view_myuser', "Can view My User"),
        )

    def save(self, *args, **kwargs):
        "Override of auth.User save to set or create person when user is saved"
        person = kwargs.pop('person', None)
        super(User, self).save(*args, **kwargs)
        # if this user has no person associated, set or create one
        if not getattr(self, 'person', None):
            if person:
                person.user = self
                person.save()
            else:
                person = Person.objects.create(
                    name=self.first_name,
                    last_name=self.last_name,
                    user=self,
                )
            if self.email:
                EmailModel.objects.get_or_create(
                    contact=person,
                    email=self.email,
                    email_type="L",
                    primary=True
                )

    def update_person(self, company=None):
        person = self.person
        person.name = self.first_name
        person.last_name = self.last_name
        if company:
            person.company = company
        person.save()

        if self.email:
            EmailModel.objects.get_or_create(
                contact=person,
                email=self.email,
                email_type="L",
                primary=True
            )

    def __str__(self):
        try:
            return "{} username : {} {} : {}".format(self.username, self.first_name, self.last_name,
                                                     self.person.company)
        except Exception:
            return "{} username : {} {}".format(self.username, self.first_name, self.last_name)


class PhoneModel(models.Model):
    """
    Abstraction on the Phone number. 
    Related to a :model: `contactfields.Contact`
    """
    phone_regex = RegexValidator(
        regex=r'^(\d{3}-\d{3}-\d{4}$)|(^\+?1?\d{9,15})$',
        message=_("Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))
    phone_number = models.CharField(
        validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    phone_type = models.CharField(
        max_length=1, choices=PHONE_TYPES, default='M',
        help_text="Phone may be mobile, office, fax, home or other.")
    extension = models.CharField(max_length=17, blank=True,
                                 help_text=_('Extension number'))
    primary = models.BooleanField(default=False, help_text=_("Primary phone"))
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="phone")

    @property
    def type(self):
        return self.phone_type

    def __str__(self):
        return "{} ({})".format(self.phone_number, self.phone_type)

    class Meta:
        verbose_name = _("Phone")
        verbose_name_plural = _("Phones")


class EmailModel(models.Model):
    """
    Wraps Emailfield with additional information
    """
    email = models.EmailField()
    email_type = models.CharField(
        max_length=1, choices=EMAIL_TYPES, default='O')
    primary = models.BooleanField(default=False)
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="email")

    def __str__(self):
        return "{} ({})".format(self.email, self.email_type)

    @property
    def type(self):
        return self.email_type

    class Meta:
        verbose_name = _("Email")
        verbose_name_plural = _("Emails")


class AddressModel(models.Model):
    """
    Wraps :model: `address.Address` with useful information
    """
    address = AddressField(on_delete=models.CASCADE)
    address_type = models.CharField(
        max_length=1, choices=ADDRESS_TYPES, default='O')
    primary = models.BooleanField(default=False)
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="address")

    def __str__(self):
        return self.address.formatted

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")

    @property
    def type(self):
        return self.address_type

    def save(self, *args, **kwargs):
        if self.address.formatted == "":
            self.address.formatted = self.address.raw
        self.address.save()
        super(AddressModel, self).save(*args, **kwargs)


class Contact(models.Model):
    """ 
    Generic Contact model. Can be marked as Person or Company
    """
    name = models.CharField(max_length=100, help_text=_("Full name for a company, first name for a person"))
    middle_name = models.CharField(max_length=100, blank=True, default='')
    last_name = models.CharField(max_length=100, blank=True)
    company = models.ForeignKey(
        'Company', null=True, blank=True, on_delete=models.CASCADE)
    is_company = models.BooleanField(default=False)
    is_company_contact = models.BooleanField(default=False,
                                             help_text=_("This person is the primary contact for the company"))
    user = models.OneToOneField(
        User, on_delete=models.PROTECT, null=True, blank=True, related_name='person')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    notes = models.TextField(null=True, blank=True, verbose_name=(u'Notes'))
    image = models.ImageField(upload_to='contactimages/', blank=True)
    url = models.URLField(blank=True)
    representation = models.CharField(max_length=200, editable=False, blank=True)

    class Meta:
        verbose_name = _("Contact")
        verbose_name_plural = _("Contacts")
        ordering = ['name']
        indexes = [
            models.Index(fields=['name', 'is_company', 'is_company_contact'])
        ]

    def __str__(self):
        return self.representation

    def clean(self):
        if self.is_company and self.is_company_contact:
            raise ValidationError({"is_company": "is_company and is_company_contact cannot be true simultaneously",
                                   "is_company_contact": "is_company and is_company_contact cannot be true simultaneously"})

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def generate_representation(self):
        if self.is_company:
            try:
                primary = Person.objects.get(company=self, is_company_contact=True)
            except Person.MultipleObjectsReturned:
                primary = Person.objects.filter(company=self, is_company_contact=True).first()
            except Person.DoesNotExist:
                primary = None
            if primary:
                self.representation = "{}, Contact: {}".format(self.name, primary)
            else:
                self.representation = "{}".format(self.name)
        else:
            self.representation = " ".join(filter(None, [self.name, self.middle_name, self.last_name]))

    # def save(self, *args, **kwargs):
    #     self.generate_representation()
    #     super(Contact, self).save(*args, **kwargs)

    def _get_emails(self, primary=True):
        emails = EmailModel.objects.filter(contact=self, primary=primary)
        return [e.email for e in emails]

    @property
    def primary_emails(self):
        return self._get_emails()

    @property
    def login_email(self):
        try:
            return EmailModel.objects.get(contact=self,
                                          primary=True,
                                          email_type="L")
        except EmailModel.DoesNotExist:
            return None
        except EmailModel.MultipleObjectsReturned:
            return "Multiple emails found."

    @property
    def fax(self):
        faxes = PhoneModel.objects.filter(contact=self, phone_type="F")
        return faxes.first() if faxes else None

    @property
    def non_primary_emails(self):
        return self._get_emails(primary=False)

    @property
    def users_with_same_email(self):
        users = User.objects.filter(email__in=self.primary_emails)
        return users

    @property
    def has_company_contact(self):
        try:
            c = Company.objects.get(id=self.id)
            return c.has_company_contact()
        except Company.DoesNotExist:
            return False

    @property
    def get_company_contact(self):
        try:
            c = Company.objects.get(id=self.id)
            return c.get_company_contact()
        except Company.DoesNotExist:
            return None

    def disable_user(self):
        if self.user:
            self.user.set_unusable_password()
            self.user.is_active = False
            self.user.save()

    def remove_user(self):
        if self.user:
            self.user.set_unusable_password()
            self.user = None
            self.save()


@receiver(pre_save, sender=Contact)
def gen_representation(sender, instance, **kwargs):
    instance.generate_representation()

class PersonManager(models.Manager):

    def get_queryset(self):
        return super(PersonManager, self).get_queryset().filter(
            is_company=False)


class Person(Contact):
    """
    Proxy model on :model: `contactfields.Contact` 
    """
    objects = PersonManager()

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        proxy = True
        ordering = ["last_name"]

    @property
    def first_name(self):
        return self.name

    def get_absolute_url(self):
        return reverse("person_detail", kwargs={"pk": self.pk})

    @staticmethod
    def get_unique_user_name(username, first_name, last_name, company=None):
        '''
        Get a unique user_name when creating new users
        :param username:    given username string
        :param first_name:  first_name string
        :param last_name:   last_name string
        :param company:     company name string
        :return:  a unique username validated from the User set
        '''
        if not username:
            username = '{}{}'.format(first_name, last_name).lower()
        if User.objects.filter(username=username).exists():
            if username[-1].isdigit():
                if username[-3:].isdigit():
                    # add 1 to the previous user with similar to this name
                    return '{}{}'.format(username[0:-3], '000{}'.format(int(username[-3:]) + 1)[-3:])
                else:
                    # add 1 to the previous user with similar name
                    return '{}{}'.format(username[0:-1], '{}'.format(int(username[-1]) + 1))
            else:
                return '{}{}'.format(username, '001')
        else:
            return username

    def get_or_create_user_from_person(self, username=None, is_active=True, email=None, random_password=False):
        """
        Helper function to create a :model: `auth.User`
        from a :model: `contactfields.Person` object
        : param username : string username for the auth.User
        : param is_active : boolean whether the User should be set active
        : param email: This is a simple email string, not the email object.
        : param random_password: if set to False, no registration emails will be sent if user clicks on
            "Forgot my password"
        """

        # find an email if available

        if not email and self.email.exists():
            ## this bit extracts the email address from the Email Model.
            try:
                email = self.email.get(primary=True, email_type="L").email
            except EmailModel.DoesNotExist:
                email = self.email.first()
                email.primary = True
                email.email_type = "L"
                email.save()
                email = email.email

        unique_username = self.get_unique_user_name(
            username if username else None, self.name, self.last_name, self.company)
        try:
            u = User.objects.get(
                username=username,
                is_active=is_active,
                first_name=self.name,
                last_name=self.last_name,
            )
        except User.DoesNotExist:
            ## Make sure no other user has this email.
            if email and User.objects.filter(email=email).exists():
                raise ValidationError("Email already used by another user")
            u = User(
                username=unique_username,
                is_active=is_active,
                first_name=self.name,
                last_name=self.last_name,
                email=email if email else '',
            )
            if random_password:
                u.set_password(User.objects.make_random_password())
            else:
                u.set_unusable_password()
            u.save(person=self)

        return (u)

    def save(self, *args, **kwargs):
        ## update representation
        self.generate_representation()

        ## Make sure user gets updated.
        if self.user and self.user.email:
            self.user.first_name = self.first_name
            self.user.last_name = self.last_name
            if self.login_email and self.login_email.email != self.user.email:
                self.user.email = self.login_email.email
            self.user.save()
        return super(Person, self).save(*args, **kwargs)


class CompanyManager(models.Manager):

    def create(self, **kwargs):
        kwargs.update({'is_company': True})
        return super(CompanyManager, self).create(**kwargs)

    def get_queryset(self):
        return super(CompanyManager, self).get_queryset().filter(
            is_company=True)

    def get_right_company(self, company_name, org):

        ## Check if company exists
        companies = Company.objects.filter(name=company_name)
        for company in companies:
            if hasattr(company, 'client'):
                if company.client.originator.organization == org:
                    return company
            elif company.get_company_contact() and hasattr(company.get_company_contact(), 'client'):
                if company.get_company_contact().client.originator.organization == org:
                    return company
        ## No company matches,
        company = Company.objects.create(name=company_name)
        return company



class Company(Contact):
    """
    Proxy model on :model: `contactfields.Contact` 
    """
    objects = CompanyManager()

    def save(self, *args, **kwargs):
        self.generate_representation()
        self.is_company = True
        super(Company, self).save(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _("Company")
        verbose_name_plural = _("Companies")

    def get_company_contact(self):
        try:
            return Person.objects.get(company=self, is_company_contact=True)
        except Person.DoesNotExist:
            return None
        except Person.MultipleObjectsReturned:
            return Person.objects.filter(company=self, is_company_contact=True).latest("id")

    def has_company_contact(self):
        return self.get_company_contact() != None

    def company_members(self):
        return Person.objects.filter(company=self)

