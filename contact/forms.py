from address.forms import AddressField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from django import forms
from django.forms import inlineformset_factory, BaseFormSet
from django.utils.safestring import mark_safe
from django.utils.text import gettext_lazy as _
from sorl.thumbnail import get_thumbnail

from .models import Contact, PhoneModel, AddressModel, EmailModel, Company, Person


class BetterImageWidget(forms.ClearableFileInput):
    """ A ImageField Widget for admin that shows a thumbnail. """

    def __init__(self, attrs={}):
        super(BetterImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and hasattr(value, 'url'):
            thumb = get_thumbnail(value, 'x64', crop='center', format='PNG')
            output.append('<div class="pull-right"><img src="%s" style="height: 64px;" /></div>'
                          % (thumb.url))
        output.append(super(BetterImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class CommonHelper(FormHelper):

    def __init__(self, *args, **kwargs):
        super(CommonHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-inline'
        self.field_template = "contact/inline_field.html"
        self.disable_csrf = True
        self.form_tag = False
        self.label_class = 'col-lg-2'
        self.field_class = 'col-lg-10'
        self.render_hidden_fields = True
        self.render_required_fields = True
        self.layout = Layout(
            Div(
                Div('name', css_class="col-md-4"),
                Div('middle_name', css_class="col-md-4"),
                Div('last_name', css_class="col-md-4"),
                css_class="col-md-7 form-group"
            ),
            Div(
                "image", css_class="col-md-5 form-group"
            ),
            Div(
                Div('is_company', css_class="col-md-3"),
                Div('notes', css_class="col-md-9"),
                css_class="col-md-5 form-group"
            ))


class NewContactForm(forms.ModelForm):
    CHOICES = [('I', _('Individual')),
               ('C', _('Company (With an optional Contact for the Company)'))]
    client_type = forms.ChoiceField(choices=CHOICES,
                                    # widget=forms.RadioSelect,
                                    help_text=_("Please select if the "
                                               "billed client is an Individual "
                                               "or a Company. You can "
                                               "optionally set up a "
                                               "contact person for the Client Company"))

    company_name = forms.CharField(strip=True,
                                   empty_value="",
                                   required=False)
    company_contact = forms.IntegerField(required=False,
                                         widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(NewContactForm, self).__init__(*args, **kwargs)
        if not self.instance.is_company and self.instance.company:
            self.initial["company_name"] = self.instance.company.name
        if self.instance.is_company and not self.instance.has_company_contact:
            self.initial['client_type'] = "C"
            self.initial['company_name'] = self.instance.name
            self.initial['name'] = ""
        if self.instance.is_company and self.instance.has_company_contact:
            self.initial['company_contact'] = self.instance.get_company_contact.id
            self.initial['client_type'] = "C"
            self.initial['company_name'] = self.instance.name
            self.initial['name'] = self.instance.get_company_contact.name
            self.initial['middle_name'] = self.instance.get_company_contact.middle_name
            self.initial['last_name'] = self.instance.get_company_contact.last_name

        self.helper = CommonHelper(self)
        self.helper.layout = Layout(
            Div(
                Div('client_type', css_class="col-md-12"),
                Div('name', css_class="col-md-4"),
                Div('middle_name', css_class="col-md-4"),
                Div('last_name', css_class="col-md-4"),
                #Div('is_company_contact', css_class="col-md-3"),
                Div("company_name", css_class="col-md-12"),
                css_class="col-md-9 form-group"
            ),
            Div(
                Div('notes', css_class="col-md-12"),
                css_class="col-md-3 form-group"
            ))

    def clean(self):
        if self.cleaned_data['client_type'] == "C":
            self.cleaned_data['first_name'] = self.cleaned_data.get('name')
            self.cleaned_data['name'] = self.cleaned_data.get('company_name')
            self.cleaned_data['is_company'] = True
        if self.cleaned_data['client_type'] == "I" and self.cleaned_data['company_name']:
            self.cleaned_data['is_company_contact'] = True
        if self.errors.get('name'):
            self.errors.pop("name")
        return self.cleaned_data

    class Meta:
        model = Contact
        exclude = ['user',
                   'company',
                   'url', 'image']


class inlineFormHelper(FormHelper):

    def __init__(self, *args, **kwargs):
        super(inlineFormHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False
        self.render_hidden_fields = True
        self.render_required_fields = True
        self.form_id = 'whatabigformid'
        self.form_class = 'form-inline'
        self.field_template = "contact/inline_field.html"
        self.form_show_labels = True


class PhoneForm(forms.ModelForm):
    class Meta:
        model = PhoneModel
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(PhoneForm, self).__init__(*args, **kwargs)
        self.helper = inlineFormHelper()
        self.helper.layout = Layout(
            Div(
                Div('phone_number', css_class='col-md-4'),
                Div('phone_type', css_class='col-md-4'),
                Div('extension', css_class='col-md-4'),
                css_class='form-group col-md-7'
            ),
            Div(
                Div('primary', css_class='col-md-9'),
                Div('DELETE', css_class='col-md-3'),
                css_class="form-group col-md-5"
            )
        )


class EmailForm(forms.ModelForm):
    class Meta:
        model = EmailModel
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)
        self.helper = inlineFormHelper()
        self.helper.layout = Layout(
            Div(
                Div('email', css_class='col-md-8'),
                Div('email_type', css_class='col-md-4'),
                css_class='form-group col-md-7'
            ),
            Div(
                Div('primary', css_class='col-md-9'),
                Div('DELETE', css_class='col-md-3'),
                css_class="form-group col-md-5"
            )
        )


class AddressForm(forms.ModelForm):
    address = AddressField()

    class Meta:
        model = AddressModel
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(AddressForm, self).__init__(*args, **kwargs)
        self.helper = inlineFormHelper()
        self.helper.layout = Layout(
            Div(
                Div('address', css_class='col-md-8'),
                Div('address_type', css_class='col-md-4'),
                css_class="form_group col-md-7"
            ),
            Div(
                Div('primary', css_class='col-md-9'),
                Div('DELETE', css_class='col-md-3'),
                css_class='form-group col-md-5'
            )
        )


PhoneModelFormSet = inlineformset_factory(Contact, PhoneModel, form=PhoneForm, extra=1)
class EmailModelFormSet(inlineformset_factory(Contact, EmailModel, form=EmailForm, extra=1)):
    def clean(self):
        super(EmailModelFormSet, self).clean()
        if any(self.errors):
            return
        logins = 0
        for form in self.forms:
            if form.cleaned_data.get('email_type') == "L" and not form.cleaned_data.get("DELETE"):
                logins += 1
        if logins > 0 and self.instance.is_company:
            if hasattr(self.instance, "get_company_contact"):
                if not self.instance.get_company_contact:
                    raise forms.ValidationError("Companies cannot have logins - go into the Person's record to set login details")
        if logins > 1 and not self.instance.is_company:
            raise forms.ValidationError("Only one Login per Person allowed")
AddressModelFormSet = inlineformset_factory(Contact, AddressModel, form=AddressForm, extra=1)


class CompanyForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CompanyForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helperlayout = Layout(
            Div(
                Div('name', css_class="col-md-12"),
                css_class="col-md-7 form-group"
            ),
            Div(
                "image", css_class="col-md-5 form-group"
            ))

    class Meta:
        model = Company
        widgets = {'image': BetterImageWidget()}
        exclude = [
            'middle_name',
            'last_name',
            'notes',
            'user',
            'company',
            'is_company_contact',
            'is_company'
        ]


class PersonForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        try:
            co = kwargs.pop("company")
            super(PersonForm, self).__init__(*args, **kwargs)
        except KeyError:
            super(PersonForm, self).__init__(*args, **kwargs)
            co = self.instance.company
        if co:
            self.initial["company"] = co
            self.fields["company"].queryset = Company.objects.filter(id__in=[co.id])
        elif self.data.get("company"):
            self.initial["company"] = Company.objects.get(id=self.data.get("company"))
            self.fields["company"].queryset = Company.objects.filter(id__in=[self.data.get("company")])
        else:
            self.fields["company"].queryset = Company.objects.none()
            #raise Company.DoesNotExist("A company name should have been available")
        self.helper = CommonHelper(self)
        self.helper.layout = Layout(
            Div(
                Div('name', css_class="col-md-4"),
                Div('middle_name', css_class="col-md-4"),
                Div('last_name', css_class="col-md-4"),
                css_class="col-md-7 form-group"
            ),
            Div(
                "image", css_class="col-md-5 form-group"
            ),
            Div(
                Div("company", css_class="col-md-8"),
                Div("is_company_contact", css_class="col-md-4"),
                css_class="col-md-12 form-group"
            )
        )

    class Meta:
        model = Person
        widgets = {'image': BetterImageWidget()}
        exclude = [
            'notes',
            'user',
            'is_company',
            'url',
        ]


class OrgUserPersonForm(PersonForm):

    def __init__(self, *args, **kwargs):
        self.helper.layout = Layout(
            Div(
                Div('name', css_class='col-md-4'),
                Div('middle_name', css_class='col-md-3'),
                Div('last_name', css_class='col-md-2'),
                Div('image', css_class='col-md-2'),
                Div('is_admin', css_class='col_sm-1'),
                css_class='form-group'
            )
        )
