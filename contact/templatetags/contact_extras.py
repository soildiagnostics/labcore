from django import template
register = template.Library()

def usphone(phone):
    value = phone.phone_number
    phone = '(%s) %s - %s (%s)' %(value[0:3],value[3:6],value[6:10], phone.phone_type)
    return phone

register.filter('usphone', usphone)