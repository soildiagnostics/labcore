from rest_framework import serializers
from rest_framework.serializers import ValidationError

from associates.models import Organization
from .models import Person, Company, PhoneModel, EmailModel, AddressModel, Contact, User


class PhoneModelSerialzer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = PhoneModel
        fields = "__all__"


class AddressModelSerializer(serializers.ModelSerializer):
    address = serializers.SerializerMethodField()

    def get_address(self, obj):
        return str(obj)

    class Meta:
        model = AddressModel
        fields = "__all__"


class AddressModelCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)
    address = serializers.CharField()

    class Meta:
        model = AddressModel
        fields = "__all__"


class EmailModelSerialzer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = EmailModel
        fields = "__all__"


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = "__all__"

    def validate(self, data):
        # print("validate Data is : ", data)
        if data.get('is_company') and data.get('is_company_contact'):
            raise ValidationError({"is_company": "is_company and is_company_contact cannot be true simultaneously",
                                   "is_company_contact": "is_company and is_company_contact cannot be true simultaneously"})
        return data


class CompanySerializer(serializers.ModelSerializer):
    phone = PhoneModelSerialzer(many=True)
    address = AddressModelSerializer(many=True)
    email = EmailModelSerialzer(many=True)

    class Meta:
        model = Company
        fields = ('name', 'address', 'phone', 'email')


class CompanyCreateSerializer(serializers.ModelSerializer):
    phone = PhoneModelSerialzer(many=True, required=False)
    address = AddressModelCreateSerializer(many=True, required=False)
    email = EmailModelSerialzer(many=True, required=False)

    class Meta:
        model = Company
        fields = ('name', 'address', 'phone', 'email')

    def create(self, validated_data):
        company = Company.objects.create(name=validated_data.get('name'))

        if validated_data.get('address'):
            addresses = validated_data.pop('address')
            for addr in addresses:
                AddressModel.objects.create(contact=company, **addr)

        if validated_data.get('phone'):
            phones = validated_data.pop('phone')
            for ph in phones:
                PhoneModel.objects.create(contact=company, **ph)

        if validated_data.get('email'):
            emails = validated_data.pop('email')
            for em in emails:
                EmailModel.objects.create(contact=company, **em)

        return company

    def update(self, instance, validated_data):
        company = instance
        company.name = validated_data['name']
        company.save()

        org = Organization.objects.get(company=company)
        org.name = company.name
        org.save()

        if validated_data.get('address'):
            addresses = validated_data.pop('address')
            for addr in addresses:
                address = AddressModel.objects.get(id=addr['id'])
                address.address = addr['address']
                address.save()
                addr.pop('address')
                AddressModel.objects.filter(id=addr['id']).update(**addr)

        if validated_data.get('phone'):
            phones = validated_data.pop('phone')
            for ph in phones:
                PhoneModel.objects.filter(id=ph['id']).update(**ph)

        if validated_data.get('email'):
            emails = validated_data.pop('email')
            for em in emails:
                EmailModel.objects.filter(id=em['id']).update(**em)

        return company


class PersonSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, max_length=100)
    middle_name = serializers.CharField(required=False, max_length=100)
    last_name = serializers.CharField(required=False, max_length=100)
    phone = serializers.StringRelatedField()
    address = serializers.StringRelatedField()
    company = serializers.StringRelatedField()
    email = serializers.StringRelatedField()

    class Meta:
        model = Person
        fields = "__all__"


class UserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'is_staff')


class UserUpdateSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=150, required=False)
    password = serializers.CharField(max_length=128, required=False)

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'is_staff')
