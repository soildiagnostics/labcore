# Generated by Django 2.1.4 on 2019-01-09 13:12

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0009_auto_20181227_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonemodel',
            name='phone_number',
            field=models.CharField(blank=True, max_length=17, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^(\\d{3}-\\d{3}-\\d{4}$)|(^\\+?1?\\d{9,15})$')]),
        ),
    ]
