# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from unittest import skip

from django.core.files.uploadedfile import SimpleUploadedFile
# from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from associates.models import Organization
from contact.forms import CompanyForm, PhoneForm, AddressForm, PersonForm
from contact.models import Company, User


class ContactViewsTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        c = Company.objects.create(name="SkyData Technologies LLC")
        u = User.objects.create(username="testinguser",
                                first_name="Testing",
                                last_name="User",
                                email="user@test.com",
                                password="nothingsecret",
                                is_active=True)

        u.person.company = c
        u.person.image = SimpleUploadedFile(name='anonymous.jpg',
                                            content=open('contact/anonymous.jpg', 'rb').read(),
                                            content_type='image/jpeg')
        u.person.save()
        # Role.objects.create(role="Dealer")
        Organization.objects.create_dealership(company=c)

    def test_company_update_form(self):
        form = CompanyForm(data={"name": "Sky"})
        self.assertTrue(form.is_valid())

        form = PhoneForm(data={"phone": "2177216032", "phone_type": "O"})
        self.assertFalse(form.is_valid())

        c = Company.objects.get(name="SkyData Technologies LLC")
        form = PhoneForm(data={"phone": "2177216032", "phone_type": "O", "contact": c.pk})
        self.assertTrue(form.is_valid())

    # def test_wrong_organization(self):
    #     c = Company.objects.create(name="Soil Diagnostics, Inc")
    #     u = User.objects.get(username="testinguser")
    #     client = Client()
    #     client.force_login(u)
    #     resp = client.get(reverse("company_detail", args=(c.id,)))
    #     self.assertEqual(resp.status_code, 403)

    @skip
    def test_company_update_view(self):
        client = Client()

        company = Company.objects.get(name="SkyData Technologies LLC")
        update_url = reverse("company_detail", args=(company.id,))
        u = User.objects.get(username="testinguser")
        client.force_login(u)
        resp = client.get(update_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            "name": "Sky",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
            "phone-0-contact": company.pk,
            "phone-0-id": '',
            "email-0-email": "something@test.co",
            "email-0-email_type": "O",
            "email-0-contact": company.pk,
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address": "1234 W Blvd",
            "address-0-address_type": "O",
            "address-0-contact": company.pk,
        }
        resp = client.post(update_url, data=post_data)

        self.assertEqual(resp.status_code, 302)
        company.refresh_from_db()
        self.assertEqual(company.name, "Sky")

    def test_company_update_invalid_form(self):
        client = Client()
        c = Company.objects.get(name="SkyData Technologies LLC")
        update_url = reverse("company_detail", args=(c.id,))
        u = User.objects.get(username="testinguser")
        client.force_login(u)

        post_data = {
            "name": "Sky",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": ''
        }

        form = AddressForm(post_data)
        self.assertFalse(form.is_valid())

        # resp = client.post(update_url, data=post_data)
        # self.assertEqual(resp.status_code, 200)
        # c.refresh_from_db()
        # self.assertEqual(c.name, "SkyData Technologies LLC")

    def test_person_update_view(self):
        client = Client()
        u = User.objects.get(username="testinguser")
        client.force_login(u)
        c = Company.objects.get(name="SkyData Technologies LLC")
        org, _ = Organization.objects.create_dealership(company=c)
        org.add_user(user=u)
        update_url = reverse("person_detail", kwargs={'pk': u.person.pk})
        resp = client.get(update_url)

        self.assertEqual(resp.status_code, 200)

        post_data = {
            "name": "Kaustubh",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "2179999999",
            "phone-0-phone_type": "M",
            "phone-0-contact": u.person.pk,
            "phone-0-id": '',
            "email-0-email": "",
            "email-0-email_type": "O",
            "email-0-contact": u.person.pk,
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": u.person.pk,
            "address-0-address": "",
        }

        resp = client.post(update_url, data=post_data)
        self.assertEqual(resp.status_code, 302)
        u.refresh_from_db()

        self.assertEqual(u.person.phone.first().phone_number, "2179999999")
        self.assertEqual(str(u.person), "Kaustubh Bhalerao")
        # self.assertEqual(u.first_name, "Kaustubh")
        # self.assertEqual(u.last_name, "Bhalerao")

        post_data.update({'email-0-email': 'kdb@bhalerao.com'})
        resp = client.post(update_url, data=post_data)
        u.refresh_from_db()
        self.assertEqual(u.person.email.last().email, "kdb@bhalerao.com")

        post_data.update({'address-0-address': '14 Oak Ct. Champaign IL 61822'})
        resp = client.post(update_url, data=post_data)
        u.refresh_from_db()
        address = u.person.address.first()
        self.assertEqual(u.person.address.first().address.raw, "14 Oak Ct. Champaign IL 61822")


    def test_person_invalid_update(self):
        client = Client()
        u = User.objects.get(username="testinguser")
        client.force_login(u)
        c = Company.objects.get(name="SkyData Technologies LLC")
        org, _ = Organization.objects.create_dealership(company=c)
        org.add_user(user=u)
        update_url = reverse("person_detail", kwargs={'pk': u.person.pk})
        resp = client.get(update_url)

        self.assertEqual(resp.status_code, 200)

        post_data = {
            "name": "Kaustubh",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
        }

        resp = client.post(update_url, data=post_data)
        # print(resp.context)
        self.assertEqual(resp.status_code, 200)
        u.refresh_from_db()

        self.assertEqual(str(u.person), "Testing User")

    def test_invalid_phone_number(self):
        post_data = {
            "phone_number": "abcd",
            "phone_type": "M",
            "contact": User.objects.get(username="testinguser").person.id,
            "id": '',
        }

        form = PhoneForm(post_data)
        self.assertFalse(form.is_valid())
        self.assertIn( "Phone number must be entered in the format", form.errors["phone_number"][0])
        # resp = client.post(update_url, data=post_data)
        # self.assertEqual(resp.status_code, 200)
        # self.assertIn(b"Phone number must be entered in the format", resp.content)
        #
        # post_data.update({'phone-0-phone_number': '(217) 721 6032'})
        # resp = client.post(update_url, data=post_data)
        # self.assertEqual(resp.status_code, 200)
        # self.assertIn(b"Phone number must be entered in the format", resp.content)

    def test_unauthorized_person_update(self):
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="newuser",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)

        client = Client()
        client.force_login(u)
        # c = Company.objects.get(name="SkyData Technologies LLC")
        u2 = User.objects.get(username="testinguser")
        update_url = reverse("person_detail", kwargs={'pk': u2.person.pk})
        resp = client.get(update_url)

        self.assertEqual(resp.status_code, 403)
