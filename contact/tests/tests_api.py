from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from associates.models import Organization
from contact.models import Company, User, Person
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class ContactAPIViewsTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        c = Company.objects.create(name="SkyData Technologies LLC")
        u = User.objects.create(username="testinguser",
                                first_name="Testing",
                                last_name="User",
                                email="user@test.com",
                                password="nothingsecret",
                                is_active=True)

        u.person.company = c
        u.person.image = SimpleUploadedFile(name='anonymous.jpg',
                                            content=open('contact/anonymous.jpg', 'rb').read(),
                                            content_type='image/jpeg')
        u.person.save()
        # Role.objects.create(role="Dealer")
        Organization.objects.create_dealership(company=c)
        self.client = APIClient()
        self.token = Token.objects.create(user=u)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_new_company(self):
        data = {
            "name": "Test Co",
            # "middle_name": "",
            # "last_name": "",
            "is_company": True,
            # "is_company_contact": false,
            # "notes": "",
            # "image": null,
            # "url": "",
            # "company": null,
            # "user": null
        }
        url = reverse("api_new_contact")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['is_company'], True)
        newco = Company.objects.latest("pk")
        self.assertEqual(newco.name, "Test Co")

    def test_create_new_person_with_company(self):
        data = {
            "name": "Firstname",
            # "middle_name": "",
            "last_name": "Lastname",
            # "is_company": True,
            # "is_company_contact": false,
            # "notes": "",
            # "image": null,
            # "url": "",
            "company": "Test co",
            # "user": null
        }
        url = reverse("api_new_contact")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['is_company'], False)
        newco = Company.objects.latest("pk")
        self.assertEqual(newco.name, "Test co")
        person = Person.objects.latest("pk")
        self.assertEqual(person.name, "Firstname")
