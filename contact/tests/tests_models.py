# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.test import Client
from django.test import TestCase

from contact.models import PhoneModel, Contact, Person, Company, AddressModel, EmailModel, User


# Create your tests here.
class PhoneTestCase(TestCase):
    def setUp(self):
        PhoneModel.objects.create(phone_number="+12177216032")

    def test_phone(self):
        phone = PhoneModel.objects.get(phone_number="+12177216032")
        self.assertEqual(phone.phone_type, "M")
        self.assertEqual(str(phone), "+12177216032 (M)")


class EmailTestCase(TestCase):
    def setUp(self):
        EmailModel.objects.create(email="test@example.com")

    def test_email(self):
        email = EmailModel.objects.get(email='test@example.com')
        self.assertEqual(email.email_type, "O")
        self.assertEqual(str(email), "test@example.com (O)")


class ContactTestCase(TestCase):
    def setUp(self):
        PhoneModel.objects.create(phone_number="+12177216032")
        Company.objects.create(name="SkyData Technologies LLC")
        User.objects.create(username="testinguser",
                            first_name="Testing",
                            last_name="User",
                            email="user@test.com",
                            password="nothingsecret",
                            is_active=True)

    def test_create_company(self):
        c = Contact.objects.get(name__startswith="Sky")
        self.assertTrue(c.is_company)
        self.assertEqual(str(c), "SkyData Technologies LLC")

    def test_update_company(self):
        c = Company.objects.get(name__startswith="Sky")
        c.name = "SkyData Technologies Inc."
        c.save()
        c.refresh_from_db()
        self.assertEqual(str(c), "SkyData Technologies Inc.")

    def test_create_person(self):
        p = PhoneModel.objects.get(phone_number="+12177216032")
        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        address2 = AddressModel(
            address="14 Oak Ct, Champaign, IL 61822, USA", contact=m)
        address2.save()

        p.contact = m
        p.save()
        self.assertEqual(p.contact, m)
        self.assertEqual(len(m.phone.all()), 1)

        a2 = m.address.all()[0]
        self.assertEqual(str(a2), "14 Oak Ct, Champaign, IL 61822, USA")

        c = Company.objects.get(name__startswith="Sky")
        m.company = c
        m.save()
        ml = Person.objects.filter(company=c)
        self.assertEqual(len(ml), 1)

    def test_create_person_from_user(self):
        u = User.objects.get(username="testinguser")
        # p = Person.objects.get_or_create_person_from_user(u)
        p = Person.objects.get(user=u)
        e = EmailModel.objects.get(email=u.email)

        self.assertEqual(p.email.all()[0], e)
        self.assertEqual(str(p), "Testing User")

    def test_create_user_from_person(self):
        p = Person.objects.create(
            name='Sachin',
            last_name='Bhalerao'
        )
        e = EmailModel.objects.create(
            contact=p,
            email='bhalersa2026@u4sd.org'
        )

        self.assertEqual(p.user, None)

        u = p.get_or_create_user_from_person('sachinb', False)

        self.assertEqual(u.username, 'sachinb')
        self.assertEqual(u.last_name, 'Bhalerao')
        self.assertEqual(u.email, 'bhalersa2026@u4sd.org')
        self.assertEqual(u.is_active, False)
        self.assertNotEqual(u.password, '')
        p.refresh_from_db()
        self.assertEqual(p.user, u)

        p = Person.objects.create(
            name='Sachin',
            last_name='Bhalerao'
        )
        e = EmailModel.objects.create(
            contact=p,
            email='bhalersa2026@u4sd.org',
            email_type="L",
            primary=True
        )

        with self.assertRaises(ValidationError):
            u = p.get_or_create_user_from_person('sachinb001', False)

        e.email='bhalersa2027@u4sd.org'
        e.save()

        u = p.get_or_create_user_from_person('sachinb001', False)
        self.assertEqual(u.username, 'sachinb001')

    def test_create_user_from_person_with_missing_email(self):
        ## Should this test fail?
        p = Person.objects.create(name='Sam Bhalerao')
        u = p.get_or_create_user_from_person('samb')

        self.assertEqual(u.last_name, '')
        self.assertEqual(u.username, 'samb')
        self.assertEqual(u.is_active, True)

    def test_update_person_to_update_user(self):
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.last_name = "UserChanged"
        p.save()
        u.refresh_from_db()
        p.refresh_from_db()
        self.assertEqual(u.last_name, p.last_name)
        self.assertEqual(str(p), "Testing UserChanged")

    def test_update_person_to_update_user_login(self):
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        e = p.email.filter(email_type='L').get()
        self.assertEqual(e.email, "user@test.com")
        p.last_name = "UserChanged"
        e.email = "user2@test.com"
        e.save()
        p.save()
        u.refresh_from_db()
        p.refresh_from_db()
        self.assertEqual(u.last_name, p.last_name)
        self.assertEqual(str(p), "Testing UserChanged")
        self.assertEqual(u.email, "user2@test.com")

    def test_user_admin(self):
        u = User.objects.get(username="testinguser")
        # store the password to login later
        password = 'mypassword'
        my_admin = User.objects.create_superuser('myuser', 'myemail@test.com', password)
        c = Client()
        # You'll need to log him in before you can send requests through the client
        c.login(username=my_admin.username, password=password)
        resp = c.get('/admin/contact/user/{}/change/'.format(str(u.pk)))
        self.assertEqual(resp.status_code, 200)

    def test_create_unique_usernames_function(self):
        un = Person.get_unique_user_name(None, 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao')

        u = User.objects.create(username='jessiebhalerao',
                                first_name='Jessie', last_name='Bhalerao')
        un = Person.get_unique_user_name(
            'jessiebhalerao', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao001')

        u.username = 'jessiebhalerao001'
        u.save()
        un = Person.get_unique_user_name(
            'jessiebhalerao001', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao002')

        u.username = 'jessiebhalerao1'
        u.save()
        un = Person.get_unique_user_name(
            'jessiebhalerao1', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao2')

# test for when a new user is created and the person has the same name already

    def test_is_company_and_is_company_contact_not_true_simultaneously(self):
        """
        This test confirms that a Contact object is not created with is_company and is_company_contact both set to True
        """

        with self.assertRaises(ValidationError):
            Contact.objects.create(name="test_company", is_company=True, is_company_contact=True)

