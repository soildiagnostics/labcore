from rest_framework import generics, permissions, status
from rest_framework.response import Response

from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin
from clients.models import Client
from contact.models import Company, Contact
from contact.serializers import ContactSerializer


class APINewContact(generics.CreateAPIView, DealershipRequiredMixin):
    """
    Single form API to create a new contact - returns contact.
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = ContactSerializer

    def post(self, request, *args, **kwargs):

        if request.data.get('company'):
            data = request.data.copy()
            company, created = Company.objects.get_or_create(name=data.get('company'))
            data['company'] = company.id
            serializer = ContactSerializer(data=data)

        else:
            serializer = ContactSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIEditDeleteContact(generics.RetrieveUpdateDestroyAPIView,
                          # DestroyMixinIntegrityError,
                          FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Retrieve, Edit and Delete a contact details
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()

    def update(self, request, *args, **kwargs):
        contact = self.get_object()

        if request.data.get('company'):
            data = request.data.copy()
            company, created = Company.objects.get_or_create(name=data.get('company'))
            data['company'] = company.id
            serializer = ContactSerializer(instance=contact, data=data)

        else:
            serializer = ContactSerializer(instance=contact, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
