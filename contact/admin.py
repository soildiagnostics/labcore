# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from address.forms import AddressWidget
from address.models import AddressField
from django import forms
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Permission
from django.db.models import Prefetch
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

# from django.contrib.auth.models import User
from associates.models import OrganizationUser
from .models import PhoneModel, EmailModel, AddressModel, \
    Person, Company, Contact, User


# Register your models here.
# admin.site.register(Contact)
# admin.site.register(PhoneModel)
# admin.site.register(AddressModel)
# admin.site.register(EmailModel)


class PhoneInline(admin.TabularInline):
    model = PhoneModel


class AddressInline(admin.TabularInline):
    model = AddressModel

    formfield_overrides = {
        # models.CharField: {'widget': TextInput(attrs={'size':'40'})},
        # models.TextField: {'widget': Textarea(attrs={'rows':1, 'cols':120})},
        AddressField: {'widget': AddressWidget(attrs={"style": "width: 300px"})}
    }


class EmailInline(admin.TabularInline):
    model = EmailModel


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    exclude = (
        'is_company',
    )
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]

    list_display = ('get_name', 'company', 'get_user', 'get_client', 'get_ao')
    # list_filter = ('company',)
    search_fields = ('last_name', 'name', 'company__name',)
    autocomplete_fields = ('company',
                           'user',
                           )

    # autocomplete_search_fields = {
    #     "company": ['company__name'],
    # }

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related('client', 'client__originator', 'user', 'user__organizations_organization',
                                   'user__organizations_organizationuser').select_related('company')

    def get_name(self, instance):
        return '{} {}'.format(instance.name, instance.last_name)

    get_name.short_description = 'Name'

    def get_client(self, instance):
        if hasattr(instance, 'client'):
            return format_html(
                f"""<a href='{reverse("admin:clients_client_change", args=[instance.client.pk])}'>{instance.client}</a>""")

    get_client.short_description = "Client Record"

    def get_user(self, instance):
        if hasattr(instance, 'user') and instance.user:
            return format_html(
                f"""<a href='{reverse("admin:contact_user_change", args=[instance.user.pk])}'>{instance.user.username}</a>""")

    get_user.short_description = "User"

    def get_ao(self, instance):
        if hasattr(instance, 'user') and instance.user:
            try:
                aoset = instance.user.organizations_organization.all()
                return format_html("<br>".join([str(a) for a in aoset])) if aoset.count() else None
            except AttributeError:
                pass
        return None

    get_ao.short_description = "AO User"

    # suppress add user because the default pop-up is too minimal
    # def get_form(self, request, obj=None, **kwargs):
    #     form = super(PersonAdmin, self).get_form(request, obj, **kwargs)
    #     form.base_fields['user'].widget.can_add_related = False
    #     return form


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    exclude = (
        'middle_name',
        'last_name',
        'company',
        'is_company_contact',
        'is_company',
        'user',
    )
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]
    search_fields = ('name',)
    list_display = ('name', 'company_contact', 'members', 'get_client')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related(Prefetch("contact_set", to_attr="_members")).prefetch_related('client',
                                                                                                 'client__originator')

    def members(self, obj):
        ms = obj._members
        links = [f"""<a href='{reverse("admin:contact_person_change", args=[m.id])}'>{m}</a>""" for m in ms]
        return format_html("<br>".join(links))

    def company_contact(self, obj):
        ms = obj._members
        m = [i for i in ms if i.is_company_contact]
        if m:
            return format_html(f'''<a href='{reverse("admin:contact_person_change", args=[m[0].id])}'>{m[0]}</a>''')
        return None

    def get_client(self, instance):
        if hasattr(instance, 'client'):
            return format_html(
                f"""<a href='{reverse("admin:clients_client_change", args=[instance.client.pk])}'>{instance.client}</a>""")

    get_client.short_description = "Client Record"


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]

    list_display = ('__str__', 'company', 'is_company',)
    list_editable = ('is_company',)
    list_filter = ('is_company',)
    search_fields = ('last_name', 'name', 'company__name', "email__email")
    autocomplete_fields = ('company', 'user')

    # autocomplete_search_fields = {
    #    "company": ['company__name'],
    # }


class PersonInline(admin.StackedInline):
    model = Person
    can_delete = False
    verbose_name_plural = 'Person'
    fk_name = 'user'
    autocomplete_fields = ('company', 'user')
    fields = (('name', 'middle_name', 'last_name'),
              ('company', 'is_company_contact'),
              ('image',),
              ('url',),
              ('notes',),
              )


class CustomUserAdmin(UserAdmin):
    inlines = (PersonInline,)
    list_display = (
    'get_person', 'username', 'email', 'first_name', 'last_name', 'is_active', 'last_login')  # , 'get_location')
    list_select_related = ('person',)
    readonly_fields = ('inline_person',)
    actions = ['make_useremail_as_login_email']

    def make_useremail_as_login_email(self, request, queryset):
        for u in queryset:
            try:
                person = u.person
                ## see if user email exists in the Contact email.
                if not person.login_email == u.email:
                    EmailModel.objects.create(
                        email_type="L",
                        email=u.email,
                        contact=person,
                        primary=True,
                    )
            except Contact.DoesNotExist:
                messages.info(request, f"{u.username} has no Person")

    make_useremail_as_login_email.short_description = "Make user email as login email"

    def inline_person(self, instance):
        return '<span class="replaceme inline_person"></span>'

    inline_person.allow_tags = True

    fieldsets = (
        # ("Person", {
        #     "fields": (
        #         ('inline_person',)
        #     )
        # }),
        ("User", {
            "classes": (),
            "fields": (  # ('first_name', 'last_name'),
                ('username'),
                ("email"),
            )
        }),
        ('Permissions', {
            'classes': ('collapse'),
            'fields': (('is_active', 'is_staff',),
                       ('password'),
                       ('groups',),
                       # ('user_permissions')
                       ),
        }),
    )

    add_fieldsets = (
        # (None, {
        #     "fields": (
        #         ('inline_person',)
        #     )
        # }),
        ("User", {
            "classes": ("grp-collapse grp-open",
                        # 'collapse', 'extrapretty',
                        ),
            "fields": (('first_name', 'last_name'),
                       ("email"),
                       ('username'),

                       )
        }),
        ('Permissions', {
            'classes': ('collapse'),
            'fields': (('is_active', 'is_staff',),
                       ('password1', 'password2',),
                       ('groups',),
                       # 'user_permissions'
                       )
        }),
    )

    def get_person(self, instance):
        if instance.person:
            val = '{} {}'.format(instance.person.name, instance.person.last_name)
            if val == " ":
                return instance.username
            else:
                return val

    get_person.short_description = 'Person'
    get_person.admin_order_field = 'person__name'

    # def get_inline_instances(self, request, obj=None):
    #     if not obj:
    #         return list()
    #     return super(CustomUserAdmin, self).get_inline_instances(request, obj)


class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['email'] = forms.EmailField(label=_("E-mail"), max_length=75)


# CustomUserAdmin.add_form = UserCreationFormExtended

# admin.site.unregister(auth_User)
# admin.site.unregister(auth_Group)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Permission)
