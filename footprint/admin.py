from django.contrib import admin

from footprint.models import UserActivity


# Register your models here.

@admin.register(UserActivity)
class UserActivityAdmin(admin.ModelAdmin):
    list_display = ('user', 'sessions', 'online', 'logout')

    def online(self, obj):
        return obj.is_online
    online.boolean = True

