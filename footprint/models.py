from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


# Create your models here.

class UserActivityManager(models.Manager):

    def set_login(self, user):
        try:
            ua = UserActivity.objects.get(user=user)
            ua.login = timezone.now()
            ua.logout = None
            ua.sessions = ua.sessions + 1
            ua.save()
        except UserActivity.DoesNotExist:
            UserActivity.objects.create(user=user, login=timezone.now())

    def set_logout(self, user):
        try:
            ua, created = UserActivity.objects.get_or_create(user=user)
            ua.sessions = max(0, ua.sessions - 1)
            if ua.sessions == 0:
                ua.logout = timezone.now()
            ua.save()
        except Exception:
            pass

    def get_online(self):
        return UserActivity.objects.filter(sessions=0)

class UserActivity(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    login = models.DateTimeField()
    logout = models.DateTimeField(blank=True, null=True)
    sessions = models.PositiveSmallIntegerField(default=1)
    objects = UserActivityManager()

    @property
    def is_online(self):
        return self.logout == None or self.sessions > 0

    def __str__(self):
        return repr(self.user)

    class Meta:
        ordering = ('-sessions', '-logout')
