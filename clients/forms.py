
from ckeditor.widgets import CKEditorWidget
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field as CrispyField
from crispy_forms.layout import Submit, Layout, HTML, Button, Div
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import Http404
from django.urls import reverse
from django.utils.text import gettext_lazy as _
from leaflet.forms.widgets import LeafletWidget

from associates.models import OrganizationUser
from clients.models import Client, Farm, Field, SubfieldLayer, VariabilityReport
from common.lambda_utils import LambdaEventHandler
from contact.models import Contact
from fieldcalendar.models import CalendarEvent
from uuid import uuid4



class FileUploadForm(forms.Form):
    file = forms.FileField()


class MyLeafletWidget(LeafletWidget):
    geometry_field_class = 'MyGeometryField'


class CommonHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(CommonHelper, self).__init__(*args, **kwargs)
        self.form_class = 'form-inline'
        # self.label_class = 'col-lg-2'
        # self.field_class = 'col-lg-10'
        self.field_template = "contact/inline_field.html"
        self.form_show_errors = True
        self.form_method = 'post'
        self.add_input(Submit('submit', 'Submit'))
        self.add_input(Button(
            'cancel', 'Cancel', css_class='btn-default', onclick="window.history.back()"))


class NewClientForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.org = kwargs.pop('org')
        super(NewClientForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_id = 'id-newClientForm'
        self.fields['originator'].queryset = OrganizationUser.objects.filter(
            organization=self.org)
        self.fields['contact'].queryset = Contact.objects.filter(client__originator__organization=self.org)
        self.helper.form_class = "uniForm"
        self.helper.field_template = "bootstrap3/field.html"
        self.helper.layout = Layout(
            Div(
                Div('contact', 'originator', css_class="col-md-12 form-group"),
                Div('notes', css_class="col-md-8"),
                Div('active', css_class="col-md-4"),
                css_class="col-md-12"
            )

        )

    class Meta:
        model = Client
        fields = "__all__"


class UpdateClientForm(NewClientForm):

    def __init__(self, *args, **kwargs):
        super(UpdateClientForm, self).__init__(*args, **kwargs)
        self.helper.form_id = 'id-updateClientForm'
        objid = kwargs['instance'].id
        client = Client.objects.get(pk=objid)
        self.fields['originator'].queryset = self.fields['originator'].queryset | \
                                             OrganizationUser.objects.filter(
                                                 organization=kwargs['instance'].originator.organization)
        self.fields['contact'].queryset = Contact.objects.filter(pk=client.contact.pk)
        self.helper.form_action = reverse(
            'client_detail', kwargs={"pk": objid})
        self.helper.add_input(Button(
            'update', 'Update Contact Information', css_class='btn-default',
            onclick="location.href='{}';".format(reverse("client_update", kwargs={"pk": client.contact.pk}))))

        self.helper.add_input(Button(
            'delete', 'Delete Client', css_class='btn-danger',
            onclick="location.href='{}';".format(reverse("client_delete", kwargs={"pk": client.pk}))))

    class Meta:
        model = Client
        fields = "__all__"


class NewFarmForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.client = kwargs.pop('client')
        self.org = kwargs.pop('org')
        super(NewFarmForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_class = None
        self.helper.form_id = 'id-newFarmForm'
        self.fields['client'].queryset = Client.objects.filter(pk=self.client)
        self.initial['client'] = self.client
        self.helper.layout = Layout(
            Div(
                Div('name', 'client', css_class="form-group"),
                Div('notes', css_class="form-group"),
                css_class="col-md-12 form-group"
            )
        )

    class Meta:
        model = Farm
        fields = "__all__"

    def clean(self):
        cleaned_data = super().clean()
        try:
            c = Client.objects.get(pk=self.client)
            assert (c.originator.organization == self.org or self.org.is_primary)
            return cleaned_data
        except AssertionError:
            ## This error occurs if you're superuser and can view the page, but aren't the
            ## owner of the client or the client themselves.
            raise ValidationError(
                _("Organizations do not match"),
                code="superusererror"
            )


class UpdateFarmForm(NewFarmForm):

    def __init__(self, *args, **kwargs):
        farm = kwargs.pop('farm')
        super(UpdateFarmForm, self).__init__(*args, **kwargs)
        self.helper.form_id = 'id-updateFarmForm'
        self.helper.form_action = reverse('farm_detail', kwargs={"pk": farm})

    class Meta:
        model = Farm
        fields = "__all__"


class BaseFieldForm(forms.ModelForm):
    event = forms.ModelChoiceField(queryset=CalendarEvent.objects.filter(kind__name="File Upload"),
                                   required=False)
    event_date = forms.CharField(required=False,
                                 widget=forms.TextInput(attrs={'id': 'datepicker'}))
    file = forms.FileField(required=False, widget=forms.ClearableFileInput)

    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm')
        self.org = kwargs.pop('org')
        super(BaseFieldForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_id = 'id-newFieldForm'
        self.helper.form_action = reverse('new_field', kwargs={"pk": self.farm})
        try:
            self.fields['farm'].queryset = Farm.objects.filter(client=Farm.objects.get(id=self.farm).client)
        except Farm.DoesNotExist:
            raise Http404
        self.initial['farm'] = self.farm
        self.helper.form_class = None
        self.helper.layout = Layout(
            Div(
                Div('boundary',
                    Div(
                        HTML("You can zoom to coordinates to quickly if you know them. "
                             "<b>The lat long will not be saved; only the boundary, if entered,"
                             " will be saved to the database.</b>"
                             " Coordinates may be entered as decimals, (e.g. 44.42561111)"
                             " or in degrees separated by commas (e.g. 44, 25, 32.2)")),
                    Div('latitude', css_class="col-md-6"),
                    Div('longitude', css_class="col-md-6"),
                    css_class="col-md-6 form-group"),
                Div(
                    Div('name', css_class="col-md-6"),
                    Div('farm', css_class="col-md-6"),
                    css_class="col-md-6 form-group"),
                Div(
                    Div('state', css_class="col-md-4"),
                    Div('county', css_class="col-md-4"),
                    Div('township', css_class="col-md-4"),
                    Div('section', css_class="col-md-4"),
                    Div('twpnum', css_class="col-md-4"),
                    Div('range', css_class="col-md-4"),
                    Div('plat_name', css_class="col-md-6"),
                    Div('intersection', css_class="col-md-8"),
                    Div('approx_area', css_class="col-md-4"),

                    css_class="form-group col-md-6"),
                Div(
                    Div('plat_file', css_class="col-md-6"),
                    css_class="col-md-6 form-group"
                ),
                # Div(
                #     Div('event', css_class="col-md-4"),
                #     Div('event_date', css_class="col-md-3"),
                #     Div('file', css_class="col-md-5"),
                #     css_class="col-md-6 form-group"),
                Div(
                    Div('notes', css_class="col-md-12"),
                    css_class="col-md-6 form-group"
                ),
                css_class="form-group col-md-12"))

    latitude = forms.CharField(required=False)
    longitude = forms.CharField(required=False)

    class Meta:
        model = Field
        fields = ["name", "farm", "latitude", "longitude",
                  "state", "county", "township", "twpnum", "intersection", "approx_area",
                  "section", "range", "plat_name", "plat_file", "boundary", "notes"]

        widgets = {'boundary': MyLeafletWidget()}

    def clean(self):
        cleaned_data = super().clean()
        try:
            all_present = all([self.cleaned_data['event'],
                               self.cleaned_data['event_date'],
                               self.cleaned_data['file']])
            some_present = any([self.cleaned_data['event'],
                                self.cleaned_data['file']])
            if some_present and not all_present:
                raise forms.ValidationError(
                    _("Event data not complete")
                )
        except KeyError:
            raise forms.ValidationError(_("File data absent or file corrupt"))
        return cleaned_data


    # def save(self, commit=True):
    #     obj = super(BaseFieldForm, self).save(commit)
    #     events = []
    #     if self.cleaned_data.get('boundary'):
    #         if settings.SOILDX_LAMBDA_PROCESSING_ENABLED and settings.SOILDX_PUBLIC_10M_ELEVATION_ENABLED:
    #             lambda_handler = LambdaEventHandler(fieldobj=obj)
    #             event = lambda_handler.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")
    #             logger.log(logging.INFO, event)
    #             events.append(event)
    #     return obj


class NewFieldForm(BaseFieldForm):

    def __init__(self, *args, **kwargs):
        super(NewFieldForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit(
            'another', 'Save and Add Another', css_class='btn-default'))
        self.helper.add_input(Button(
            'flyTo', 'Zoom To Coordinates', css_class='btn-default', onclick="zoomTo()"))



class UpdateFieldForm(NewFieldForm):

    def __init__(self, *args, **kwargs):
        field = kwargs.pop('field')
        super(UpdateFieldForm, self).__init__(*args, **kwargs)
        self.helper.form_id = 'id-updateFieldForm'
        self.helper.form_action = reverse('field_detail', kwargs={"pk": field})
        self.helper.add_input(Button(
            'gis', 'View Geospatial Data', css_class='btn-default',
            onclick=f"location.href='{reverse('field_geo_details', kwargs={'pk': self.instance.pk})}'"))
        if Field.objects.get(id=field).is_located:
            self.helper.add_input(Button(
                'address', 'Get Address', css_class='btn-default'))

        self.helper.layout = Layout(
            Div(
                Div('boundary',
                    Div(
                        HTML("You can zoom to coordinates to quickly if you know them. "
                             "<b>The lat long will not be saved; only the boundary, if entered,"
                             " will be saved to the database.</b>"
                             " Coordinates may be entered as decimals, (e.g. 44.42561111)"
                             " or in degrees separated by commas (e.g. 44, 25, 32.2)")),
                    Div('latitude', css_class="col-md-6"),
                    Div('longitude', css_class="col-md-6"),
                    css_class="col-md-6 form-group"),
                Div(
                    Div('name', css_class="col-md-6"),
                    Div('farm', css_class="col-md-6"),
                    css_class="col-md-6 form-group"),
                Div(
                    Div('state', css_class="col-md-4"),
                    Div('county', css_class="col-md-4"),
                    Div('township', css_class="col-md-4"),
                    Div('section', css_class="col-md-4"),
                    Div('twpnum', css_class="col-md-4"),
                    Div('range', css_class="col-md-4"),
                    Div('plat_name', css_class="col-md-6"),
                    Div('intersection', css_class="col-md-8"),
                    Div('approx_area', css_class="col-md-4"),

                    css_class="form-group col-md-6"),
                Div(
                    Div('plat_file', css_class="col-md-6"),
                    css_class="col-md-6 form-group"
                ),
                Div(
                    Div('event', css_class="col-md-4"),
                    Div('event_date', css_class="col-md-3"),
                    Div('file', css_class="col-md-5"),
                    css_class="col-md-6 form-group"),
                Div(
                    Div('notes', css_class="col-md-12"),
                    css_class="col-md-6 form-group"
                ),
                css_class="form-group col-md-12"))


class DisplayFieldFormBase(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DisplayFieldFormBase, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.disable_csrf = True

    class Meta:
        model = Field
        fields = ["boundary"]
        abstract = True


class DisplayFieldForm(DisplayFieldFormBase):
    # This one is used to display the field on the order form.

    class Meta:
        model = Field
        fields = ["boundary"]
        widgets = {'boundary': MyLeafletWidget(attrs={"id": f"boundary_{str(uuid4())[:8]}"})}
        # widgets = {'boundary': MyLeafletWidget()}


# def field_form_generator(instance):
#
#     class DisplayFormClass(DisplayFieldFormBase):
#         class Meta:
#             model = Field
#             fields = ["boundary"]
#             widgets = {'boundary': MyLeafletWidget()}


class DisplayVariabilityReportForm(DisplayFieldFormBase):
    # This one is used to display the field on the variability report form.

    class Meta:
        model = VariabilityReport
        fields = ["annotations"]
        widgets = {'annotations': MyLeafletWidget()}


class FieldGeoDetailsForm(BaseFieldForm):
    report_data = forms.CharField(required=False,
                                  widget=forms.TextInput(attrs={'type': 'hidden'}))

    def __init__(self, *args, **kwargs):
        field = kwargs.pop('field')
        super().__init__(*args, **kwargs)
        self.helper.form_id = 'id-updateFieldFormGIS'
        self.helper.form_action = reverse('field_geo_details', kwargs={"pk": field})
        self.helper.add_input(Button(
            'updateBbox', 'Update Bounding Box', css_class='btn-default'))
        self.helper.add_input(Button(
            'events', 'View Field Events', css_class='btn-default',
            onclick=f"location.href='{reverse('field_detail', kwargs={'pk': self.instance.pk})}'"))
        self.helper.add_input(Submit(
            'genreport', 'Submit and Generate New Report', css_class='btn-success'))
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            Div(
                Div('boundary', 'report_data', css_class="col-lg-8 col-md-12 col-sm-12 form-group"),
                Div(
                    Div('name', css_class="col-md-6"),
                    Div('farm', css_class="col-md-6"),
                    css_class="col-lg-4 col-md-12 col-sm-12 form-group"
                ),
                Div(
                    Div('event', css_class="col-md-4"),
                    Div('event_date', css_class="col-md-3"),
                    Div('file', css_class="col-md-5"),
                    css_class="col-lg-4 col-md-12 col-sm-12 form-group"),
                Div(
                    Div('notes', css_class="col-md-12 col-sm-12"),
                    css_class="col-lg-4 col-md-12 col-sm-12 form-group"),
                css_class="col-lg-12 col-md-12 col-sm-12"
            )
        )

    class Meta:
        model = Field
        fields = ["name", "farm", "latitude", "longitude",
                  'event', 'event_date', 'file', "boundary",
                  "notes", "report_data"]
        widgets = {'boundary': MyLeafletWidget(),
                   "notes": CKEditorWidget(config_name="zinnia-content")}

    # def clean(self):
    #     cleaned_data = super().clean()
    #
    #     return cleaned_data


# class FieldSamplingMapForm(forms.ModelForm):
#
#     def __init__(self, *args, **kwargs):
#         field = kwargs.pop('field')
#         super().__init__(*args, **kwargs)
#         self.helper = FormHelper(self)
#         self.helper.form_id = 'id-updateFieldFormSampling'
#         self.helper.form_action = reverse('field_sampling_map', kwargs={"pk": field})
#
#     class Meta:
#         model = SubfieldLayer
#         fields = ["polygons", "points"]
#         wid = LeafletWidget()
#         widgets = {'polygons': wid,
#                    'points': wid}

class FieldRiskDetailsForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())
    report = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        field = kwargs.pop('field')
        self.farm = kwargs.pop('farm')
        self.org = kwargs.pop('org')
        # if 'comments' in kwargs.keys():
        #     comments = kwargs.pop('comments')
        #     kwargs.update(initial={
        #         'description': comments
        #     })
        super().__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_class = 'bootstrap3'
        self.helper.field_template = "bootstrap3/field.html"
        self.helper.form_method = "POST"
        self.helper.form_id = 'id-updateFieldVariabilityForm'
        self.helper.add_input(Submit(
            'printview', 'Submit and go to Print View', css_class='btn-success'))
        self.helper.layout = Layout(
            Div('annotations', css_class="col-md-5 col-sm-3 form-group"),
            Div('report', 'description', css_class="col-md-7 col-sm-9 form-group"),
        )

    class Meta:
        model = VariabilityReport
        fields = ["report", "annotations", "description"]
        widgets = {'annotations': MyLeafletWidget()}


class FieldRiskAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = VariabilityReport
        fields = "__all__"


class BaseSamplingMapForm(forms.ModelForm):
    boundary = forms.CharField(required=False,
                               widget=forms.TextInput(attrs={'type': 'hidden'}))
    points = forms.CharField(required=False,
                               widget=forms.Textarea(attrs={'type': 'hidden'}))
    regions = forms.CharField(required=False,
                               widget=forms.Textarea(attrs={'type': 'hidden'}))

    def __init__(self, *args, **kwargs):
        field = kwargs.pop('field')
        image = kwargs.pop('file')
        create = kwargs.pop('create')
        super().__init__(*args, **kwargs)

        fld = Field.objects.get(pk=field)
        self.initial['boundary'] = fld.geojson_boundary()
        self.initial['field'] = fld.pk
        self.initial['source_file'] = image
        if create:
            self.initial['name'] = "Sampling Map"
            self.initial['dbf_field_names'] = {'field_names': ["Sampling Map"]}
        else:
            self.initial['name'] = self.instance.name
            self.initial['dbf_field_names'] = self.instance.dbf_field_names

        self.helper = FormHelper(self)
        self.helper.form_id = 'id-updateFieldFormSampling'
        # self.helper.form_action = reverse('create_sample_map', kwargs={"pk": field})
        self.helper.add_input(Button(
            'gis', 'View Geospatial Data', css_class='btn-default',
            onclick=f"location.href='{reverse('field_geo_details', kwargs={'pk': field})}'"))
        self.helper.add_input(Submit(
            'genmap', 'Submit', css_class='btn-success'))
        self.helper.layout = Layout(
            CrispyField('name'),
            Div('geoms', css_class="form-group"),
            CrispyField('field', type="hidden"),
            CrispyField('source_file', type="hidden"),
            CrispyField('boundary', type="hidden"),
            CrispyField('points', type="hidden"),
            CrispyField('regions', type="hidden"),
            CrispyField('dbf_field_names', type="hidden")
        )

    class Meta:
        model = SubfieldLayer
        fields = ('field', 'geoms', 'source_file', 'name', 'dbf_field_names')
        widgets = {'geoms': MyLeafletWidget()}

class SamplingMapForm(BaseSamplingMapForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.add_input(Button(
            'squaregrid', 'Create Grid', css_class='btn-default'))
        self.helper.add_input(Button(
            'voronoi', 'Voronoi Tesselation', css_class='btn-default'))
        self.initial['dbf_field_names'] = {'field_names': ["Sampling Map"]}

class SamplingPointsForm(BaseSamplingMapForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial['dbf_field_names'] = {'field_names': ["Sampling Map"]}

class SamplingMapUpdateForm(BaseSamplingMapForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.helper.add_input(Button(
        #     'makeshp', 'Make Shapefile', css_class='btn-success'))
        self.helper.form_action = reverse('update_sample_map', kwargs={"pk": self.instance.pk})


class SubFieldForm(forms.ModelForm):
    class Meta:
        model = SubfieldLayer
        fields = ("name",)