import json
import os
from datetime import datetime
from urllib import parse

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.gis.db.models import MultiPolygonField
from django.db import IntegrityError
from django.db.models import Prefetch
from django.db.models.functions import Cast
from django.http import JsonResponse, HttpResponse
from django.template.loader import get_template
from django.utils import timezone
from django.utils.timezone import now
from numpy import mean
from rest_framework import generics, permissions, mixins, status
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin, FilteredObjectMixin
from common.models import ParseTimeMixin
from common.searchquery import SearchQueryAssemblyMixin
from fieldcalendar.event_base import CalendarEvent, CalendarEventKind
from .gis.shapezip_handler import ZippedShapeFile
from .gis.tif_handler import TiffFile
from .models import Client, Field, SubfieldLayer, Farm, RasterFile
from .serializers import ClientSerializer, HallPassRequestSerializer, FieldSerializer, FieldWithEventsSerializer, \
    LayerAdjustSerializer, FarmSerializer, ClientCreateSerializer, FarmCreateSerializer, FieldGeoJSONSerializer, \
    SubfieldSerializer, ClientIdSerializer
from django.contrib.gis.db.models.functions import Distance, Centroid, AsGeoJSON
from django.contrib.gis.geos import Point, GEOSGeometry, MultiPolygon
from django.contrib.gis.measure import D
from fieldcalendar.serializers import FieldEventSerializer
from fieldcalendar.models import FieldEvent
from clients.views_gis import CLIPPING_THRESHOLD, raster_helper, SLOPE_LEGEND, ELEVATION_LEGEND
from django.contrib.postgres.search import TrigramSimilarity
import geopandas as gpd
import pandas as pd
from shapely import wkt
from .views import PRESET_EVENTS
import numpy as np


class DestroyMixinIntegrityError(object):
    def destroy(self, request, *args, **kwargs):
        try:
            return super().destroy(request, *args, **kwargs)
        except IntegrityError as e:
            return Response({"error": repr(e)}, status=status.HTTP_403_FORBIDDEN)


class APIClientList(generics.ListAPIView, SearchQueryAssemblyMixin,
                    FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send list of clients, farms and fields
    """

    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = ClientSerializer
    model = Client
    instance_owner_organization = "originator__organization"
    instance_owner_client = "instance"  ## the model is the client itself.

    def get_queryset(self):
        qs = self.get_initial_queryset()

        client = self.request.query_params.get("search", None)
        if client:
            qs = qs.filter(self.get_q_object('contact__name', client) |
                           self.get_q_object('contact__last_name', client))
            qs = qs.annotate(name_similarity=TrigramSimilarity('contact__name', client))
            qs = qs.annotate(last_name_similarity=TrigramSimilarity('contact__last_name', client)).order_by(
                '-last_name_similarity',
                '-name_similarity',
                '-pk')
        return qs


class APIClientStats(APIView, DealershipRequiredMixin):
    """
    Provide web stats
    """
    permissions_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        fields = Field.objects.filter(boundary__isnull=False).select_related('farm', 'farm__client').values('boundary',
                                                                                                            'farm',
                                                                                                            'farm__client')
        df = pd.DataFrame(fields)

        def area_calc(bdy):
            try:
                geo = bdy['boundary'].transform(2791, clone=True)
                return wkt.loads(geo.wkt)
            except:
                return None

        df['geometry'] = df.apply(area_calc, axis=1)
        gdf = gpd.GeoDataFrame(df, geometry='geometry')
        gdf['area'] = gdf.area
        gdf['area'] = gdf['area'].map(lambda x: round(x * 0.000247105381, 1))
        filtered_gdf = gdf[gdf['area'] > 0]
        stats = dict()
        stats['clients'] = len(filtered_gdf['farm__client'].unique())
        stats['farms'] = len(filtered_gdf['farm'].unique())
        stats['fields'] = filtered_gdf.shape[0]
        stats['avg_client_acres'] = np.round(np.mean(filtered_gdf.groupby('farm__client').sum()['area']), 1)
        stats['avg_farm_acres'] = np.round(np.mean(filtered_gdf.groupby('farm').sum()['area']), 1)
        stats['avg_field_acres'] = np.round(np.mean(filtered_gdf['area']), 1)

        return Response(stats)


class APIGetClient(APIView, DealershipRequiredMixin):
    """
    Retrieve a client ID based on client name
    """
    permissions_classes = (permissions.IsAuthenticated,)

    def get(self, request, name):
        print("client : ", name)
        client = Client.objects.filter(contact__name=name).first()
        if client:
            serializer = ClientIdSerializer(client)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'client with given name does not exist'}, status=status.HTTP_404_NOT_FOUND)


class APIEditDeleteClient(DestroyMixinIntegrityError,
                          generics.RetrieveUpdateDestroyAPIView,
                          FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Retrieve, Edit and Delete a client details
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = ClientCreateSerializer
    model = Client
    instance_owner_organization = "originator__organization"
    instance_owner_client = "instance"  ## the model is the client itself.

    def get_queryset(self):
        qs = self.get_initial_queryset()
        return qs

    def update(self, request, *args, **kwargs):
        client = self.get_object()
        contact = client.contact

        serializer = ClientCreateSerializer(contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            client.refresh_from_db()
            newser = ClientSerializer(client)
            return Response(newser.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APINewClient(generics.CreateAPIView, DealershipRequiredMixin):
    """
    Single form API to create a new client - returns client ID.
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = ClientCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = ClientCreateSerializer(data=request.data)
        if serializer.is_valid():
            new_contact = serializer.save()
            new_client = Client.objects.create(contact=new_contact, originator=self.orguser)
            newser = ClientSerializer(new_client)
            return Response(newser.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIFarmsForClient(generics.ListCreateAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send list of farms only per client
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FarmSerializer
    model = Farm
    instance_owner_organization = "client__originator__organization"
    instance_owner_client = "client"
    lookup_field = "client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        qs = qs.filter(client=self.kwargs.get("client"))

        return qs

    def post(self, request, *args, **kwargs):
        data = {
            "client": Client.objects.get(id=self.kwargs.get('client')),
            "name": request.data.get("name")
        }
        serializer = FarmSerializer(data=data)
        if serializer.is_valid():
            new_farm = Farm.objects.create(**data)
            newser = FarmSerializer(new_farm)
            return Response(newser.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIEditFarm(DestroyMixinIntegrityError,
                  generics.RetrieveUpdateDestroyAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FarmSerializer
    model = Farm
    instance_owner_organization = "client__originator__organization"
    instance_owner_client = "client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        return qs


class APIListFieldLocations(generics.ListAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send locations of fields
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldGeoJSONSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        qs = qs.annotate(bdy=Cast('boundary', MultiPolygonField())).filter(bdy__isvalid=True).annotate(
            centroid=AsGeoJSON(Centroid('bdy'))).select_related('farm__client',
                                                                'farm__client__contact',
                                                                'farm__client__originator__organization',
                                                                'farm__client__originator__user__person')
        return qs


class APIFieldsForFarm(generics.ListCreateAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send list of farms only per client
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        qs = qs.filter(farm=self.kwargs.get("farm"))
        return qs

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        data['farm'] = self.kwargs.get('farm')
        # data = {
        #     "farm": self.kwargs.get('farm'),
        #     "name": request.data.get("name")
        # }
        # Check if boundary is a geojson
        bdy = None
        try:
            if type(data['boundary']) == type(dict()):
                bdy = data.pop('boundary')
            elif(type(json.loads(data['boundary'])) == type(dict())):
                bdy = json.loads(data.pop('boundary'))
        except:
            pass
        serializer = FieldSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
            if bdy:
                srid = self.request.data.get('srid', 4326)
                instance.boundary = GEOSGeometry(json.dumps(bdy), srid=srid)
                instance.save()
                instance.refresh_from_db()
                serializer = self.get_serializer(instance)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIEditField(DestroyMixinIntegrityError,
                   generics.RetrieveUpdateDestroyAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send list of farms only per client
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        return qs


class APIEditFieldGeoJSON(APIEditField):
    serializer_class = FieldGeoJSONSerializer

    def update(self, request, *args, **kwargs):
        """Overrriding default update method"""
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        try:
            srid = self.request.data.get('srid', 4326)
            bdy_gj = self.request.data.get("boundary")
            instance.boundary = GEOSGeometry(json.dumps(bdy_gj), srid=srid)
            instance.save()
            instance.refresh_from_db()
            serializer = self.get_serializer(instance)
        except Exception:
            raise

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class APISubfieldLayers(generics.ListAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    List of subfield layers for any given field.
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = SubfieldSerializer
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    instance_owner_client = "field__farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        qs = qs.filter(field=self.kwargs.get("field"))
        polys = self.request.query_params.get('polygons_only')
        if polys:
            qs = [q for q in qs if q.polygons_only]
        return qs


class APILayerStubView(APIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def get_queryset(self):
        return self.get_initial_queryset()

    def get(self, request, pk, *args, **kwargs):
        layer = self.get_initial_queryset().get(pk=pk)
        template = get_template("clients/gis_layer_stub.html")
        context = {"l": layer, 'field': layer.field, 'layers': layer.field.layer.all()}
        string = template.render(context, request=request)
        return JsonResponse({layer.id: string})


class APISubfieldLayerEdit(generics.RetrieveUpdateDestroyAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = SubfieldSerializer
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    instance_owner_client = "field__farm__client"

    def get_queryset(self):
        return self.get_initial_queryset()


class APISubfieldRaster(generics.RetrieveAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    model = RasterFile
    instance_owner_organization = "layer__field__farm__client__originator__organization"
    instance_owner_client = "layer__field__farm__client"

    def get_queryset(self):
        return self.get_initial_queryset()

    def retrieve(self, request, *args, **kwargs):
        subfieldlayer = SubfieldLayer.objects.get(id=self.kwargs.get("pk"))
        parameter = self.kwargs.get("parameter")
        result = raster_helper("file", subfieldlayer, parameter, self.request.user.username)
        if isinstance(result, RasterFile):
            with result.file.open("rb") as f:
                response = HttpResponse(f.read(), content_type="image/png")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(result.file.name)
                return response
        ## Otherwise the result is a JSONResponse
        return result



class APISubfieldLayerAddFeature(generics.UpdateAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = SubfieldSerializer
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    instance_owner_client = "field__farm__client"

    def get_queryset(self):
        return self.get_initial_queryset().filter(editable=True)

    def update(self, request, *args, **kwargs):
        """Overrriding default update method"""
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        instance.dbf_field_names['field_names'].extend(request.data.keys())
        instance.dbf_field_values.update(request.data)
        for key in request.data.keys():
            assert len(instance.dbf_field_values[key]) == instance.features, \
                f"Length mismatch: {instance.dbf_field_values[key]} != {instance.features}"
        instance.save()
        serializer = self.get_serializer(instance)
        # serializer.is_valid(raise_exception=True)
        # self.perform_update(serializer)
        # try:
        #     srid = self.request.data.get('srid', 4326)
        #     bdy_gj = self.request.data.get("boundary")
        #     instance.boundary = GEOSGeometry(json.dumps(bdy_gj), srid=srid)
        #     instance.save()
        #     instance.refresh_from_db()
        #     serializer = self.get_serializer(instance)
        # except Exception:
        #     raise

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class APISubfieldLayerDuplicate(APIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = SubfieldSerializer
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    instance_owner_client = "field__farm__client"

    def get_queryset(self):
        return self.get_initial_queryset()

    def post(self, request, pk, format=None):
        try:
            sf = self.get_queryset().get(pk=self.kwargs.get('pk'))
            newname = self.request.query_params.get('name', f"Copy of {sf.name}")
            pars = self.request.query_params.get('parameters', list())
            pars = pars.split(",") if len(pars) else pars
            field_vals = {k: v for (k, v) in sf.dbf_field_values.items() if k in pars}
            newsf = SubfieldLayer.objects.create(
                geoms=sf.geoms,
                name=newname,
                field_id=sf.field.id,
                editable=True,
                dbf_field_names={'field_names': pars},
                dbf_field_values=field_vals
            )
            serializer = SubfieldSerializer(instance=newsf)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except SubfieldLayer.DoesNotExist:
            return Response({'error': 'no such subfield'}, status=status.HTTP_404_NOT_FOUND)


class APICreateHallPass(generics.CreateAPIView, DealershipRequiredMixin):
    """
    Create a hall pass
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = HallPassRequestSerializer

    # def perform_create(self, serializer):
    #     try:
    #         serializer.save()
    #     except Exception:
    #         return


class APIPostZipfile(APIView, DealershipRequiredMixin):
    """
    A simple zipfile upload api
    """
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = (FileUploadParser,)

    def put(self, request, field, format=None):
        try:
            if 'file' not in request.data:
                raise ParseError("Empty Content")
            field = Field.objects.get(id=field)
            file = request.data.get('file')

            try:
                evt = request.query_params.get("process_type", "Shapefile Upload")
                evt_kind, _ = CalendarEventKind.objects.get_or_create(name="File Upload")
                event_type, _ = CalendarEvent.objects.get_or_create(name=evt, kind=evt_kind)
            except (CalendarEvent.DoesNotExist, CalendarEvent.MultipleObjectsReturned):
                event_type = None

            date = timezone.now()

            ## create an event for the file upload.
            fe = FieldEvent.objects.create_system_event(
                field=field,
                title=event_type.name,
                start=date,
                end=date,
                allDay=False
            )
            fe.event_type = event_type
            # fe.file.save(f"shapefiles/{field.id}/{file.name}", file)
            # fe.save()

            if event_type.name in PRESET_EVENTS:
                fe.file.save(f"shapefiles/{field.id}/{file.name}", file)
                fe.save()
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.send)("process-uploaded-shapefile", {
                    "type": "process",
                    "user": self.request.user.username,
                    "fieldevent": fe.id,
                    "field": field.id
                })
            else:
                fe.file.save(f"uploads/{field.id}/{file.name}", file)
                fe.save()

            serializer = FieldEventSerializer(instance=fe)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)
            return Response({'error': repr(e)}, status=status.HTTP_400_BAD_REQUEST)


class APIFieldsByDistance(generics.ListAPIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        lat = self.request.query_params.get("lat", 0)
        lon = self.request.query_params.get("lon", 0)
        pnt = Point(float(lon), float(lat), srid=4326)
        res = qs.filter(boundary__distance_lte=(pnt, D(m=1600).m)).annotate(
            distance=Distance("boundary", pnt)).order_by("distance")
        return res


class APIFieldEventStatusView(generics.ListAPIView, ParseTimeMixin, FilteredQuerySetMixin, DealershipRequiredMixin):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldWithEventsSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        extent = self.request.query_params.get("extent", None)
        if extent:
            qs = Field.objects.filter_queryset_with(qs, extent)
        qs = qs.filter(boundary__isnull=False).annotate(
            bdy=AsGeoJSON('boundary'))

        feqs = FieldEvent.objects.filter(field__in=qs).select_related('event_type__kind')
        cat = self.request.query_params.get("category")
        if cat != "---":
            feqs = feqs.filter(event_type__kind=cat)
        try:
            feqs = feqs.filter(start__gte=self.parse_with_tz(self.request.query_params.get('start')))
        except TypeError:
            pass
        try:
            feqs = feqs.filter(start__lte=self.parse_with_tz(self.request.query_params.get('end')))
        except TypeError:
            pass

        return qs.prefetch_related(Prefetch("fieldevent_set", queryset=feqs, to_attr="events"))

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['queryset'] = self.get_queryset()
        return context


class APISetLayerAsBoundary(FilteredObjectMixin, FilteredQuerySetMixin,
                            DealershipRequiredMixin, generics.RetrieveAPIView):
    model = SubfieldLayer
    permission_classes = (permissions.IsAuthenticated,)
    instance_owner_organization = "field__farm__client__originator__organization"

    def get_queryset(self):
        return self.get_initial_queryset()

    def post(self, request, *args, **kwargs):
        try:
            subfield = self.get_object()
            layer = SubfieldLayer.objects.get(id=subfield.id)
            layer.field.boundary = layer.simplify_geometry()
            layer.field.save()
            return Response({'status': 'success'})
        except Exception as e:
            return Response({'error': repr(e)})


class APIAdjustLayerValue(generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = LayerAdjustSerializer
    queryset = SubfieldLayer.objects.all()

    def update(self, request, *args, **kwargs):
        """This method is almost exactly the same as in the UpdateAPIView"""
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        assert instance.editable, "Layer is not editable"
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # We are only returning serializer validated_data here, because the serializer doesn't fit the model
        return Response(serializer.validated_data)


class APISubfieldParameters(FilteredObjectMixin, FilteredQuerySetMixin,
                            DealershipRequiredMixin, APIView):
    permission_classes = (permissions.IsAuthenticated,)
    instance_owner_organization = "field__farm__client__originator__organization"
    model = SubfieldLayer
    ## A conflict with DealershipRequired mixin exists. The LoginRequiredMixin and IsAutheticated permission
    ## Are at odds with each other. This may exist for all API calls.
    def get(self, request, pk, *args, **kwargs):
        subfield = SubfieldLayer.objects.get(id=pk)
        return Response(subfield.dbf_field_names)


class APISubfieldGeoJSONView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        subfield = kwargs.get("pk")
        parameter = kwargs.get("parameter")
        ## Check if this layer already exists. If not, then recreate it from the file
        subfieldlayer = SubfieldLayer.objects.get(id=subfield)
        if subfieldlayer.name == "USGS 10m Elevation":
            # Short circuit
            response = json.loads(subfieldlayer.field.boundary.geojson)
            response['parameter'] = parse.unquote(parameter)
            if parameter == "slope_public_10m":
                response['legend'] = SLOPE_LEGEND
                response['min'] = 0
                response['max'] = 20
            else:
                response['legend'] = ELEVATION_LEGEND
                response['min'] = 0
                response['max'] = 100
            response['usgs'] = True
            return JsonResponse(response)
        values = subfieldlayer.get_values(parse.unquote(parameter))
        if not values:
            fe = FieldEvent.objects.filter(field=subfieldlayer.field,
                                           file=subfieldlayer.source_file).last()
            if not fe:
                return Response({"error": "No data in the subfield layer."})
            handler = ZippedShapeFile(fe.file, subfieldlayer.field)
            try:
                handler.attach_to_field(shapefile=parse.unquote(subfieldlayer.name), parameter=parse.unquote(parameter))
            except Exception as e:
                print(e)
            finally:
                handler.cleanup()
            subfieldlayer.refresh_from_db()

        clip = False if not values or len(values) <= CLIPPING_THRESHOLD else True

        try:
            response = subfieldlayer.geojson_features(parameter=parse.unquote(parameter), clip=clip)
            response['parameter'] = parse.unquote(parameter)
            return Response(response)
        except TypeError:
            return Response({'status': 'type error'}, status=500)
        except KeyError:
            return Response({'status': 'key error'}, status=404)


class APISearchClientsFarmsFields(generics.ListAPIView, SearchQueryAssemblyMixin,
                                  FilteredQuerySetMixin, DealershipRequiredMixin):
    """
    Send list of farms only per client
    """
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()

        client = self.request.query_params.get("client", None)
        if client:
            qs = qs.filter(self.get_q_object('farm__client__contact__name', client))
        farm = self.request.query_params.get("farm", None)
        if farm:
            qs = qs.filter(self.get_q_object('farm__name', farm))
        field = self.request.query_params.get("field", None)
        if field:
            qs = qs.filter(self.get_q_object('name', field))
            qs = qs.annotate(similarity=TrigramSimilarity('name', field)).order_by('-similarity', '-pk')
        return qs

class APIPolygonizeRaster(APIView, FilteredQuerySetMixin, DealershipRequiredMixin):
    permissions_classes = (permissions.IsAuthenticated,)
    serializer_class = SubfieldSerializer
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    instance_owner_client = "field__farm__client"

    def get_queryset(self):
        return self.get_initial_queryset()

    def post(self, request, pk, parameter, format=None):
        try:
            sf = self.get_queryset().get(pk=pk)
            newname = f"{sf.name}:{parameter}"
            try:
                rf = RasterFile.objects.filter(layer=sf, parameter=parameter, file__icontains=".tif").latest('pk')
            except RasterFile.DoesNotExist:
                sf.make_raster(None, parameter, save_tif=True)
                rf = RasterFile.objects.filter(layer=sf, parameter=parameter, file__icontains=".tif").latest('pk')

            thandler = TiffFile(rf.file, field=sf.field)

            process =  self.request.query_params.get('process', 'contour')
            if process == 'contour':
                intervals = request.query_params.get('intervals', 7)
                values = sf.get_values(parameter)
                minval = min(values)
                maxval = max(values)
                interval = (maxval-minval)/intervals
                dir, file = thandler.contour(interval, parameter)
                zhandler = ZippedShapeFile(zipfile=rf.file, field=sf.field, tmpdir=dir)
                zhandler.extracted = True
                zhandler.folder = dir
                newsf, _ = zhandler.make_subfield_layer(file, parameters=['ID', parameter])
                newsf.dbf_field_values[parameter] = [minval + i*interval for i in range(newsf.features)]
                newsf.name = newname
                #make fieldevent
                fe = FieldEvent.objects.create_shapefile_upload_event(newsf, dir, 'contour.zip')
                newsf.source_file = fe.file.name
                newsf.editable = True
                newsf.save()
                serializer = SubfieldSerializer(instance=newsf)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            elif process == "polygonize":
                dir, file = thandler.polygonize()
                zhandler = ZippedShapeFile(zipfile=rf.file, field=sf.field, tmpdir=dir)
                zhandler.extracted = True
                zhandler.folder = dir
                newsf, _ = zhandler.make_subfield_layer(file, parameters=['DN'])
                newsf.name = "Zones"
                newsf.editable = True
                # make fieldevent
                fe = FieldEvent.objects.create_shapefile_upload_event(newsf, dir, 'zones.zip')
                newsf.source_file = fe.file.name
                newsf.merge_polygons_of_similar_values()
                newsf.refresh_from_db()
                serializer = SubfieldSerializer(instance=newsf)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except SubfieldLayer.DoesNotExist:
            return Response({'error': 'no such subfield'}, status=status.HTTP_404_NOT_FOUND)
        finally:
            try:
                thandler.cleanup()
            except:
                pass
            try:
                zhandler.cleanup()
            except:
                pass

