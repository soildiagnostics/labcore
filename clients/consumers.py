import asyncio
import datetime
import json
import os
import sys
import tempfile
import time
import traceback
import uuid
from asyncio import sleep
from urllib import parse

from channels.consumer import SyncConsumer
from channels.exceptions import StopConsumer
from channels.generic.websocket import JsonWebsocketConsumer, AsyncJsonWebsocketConsumer
from django.conf import settings
from django.core.files import File
from django.template.loader import get_template
from django.utils import timezone

from clients.gis.gishelper import SDXShapefile
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.gis.tif_handler import TiffFile
from clients.models import Field, SubfieldLayer, RasterFile
from clients.serializers import SubfieldSerializer
from common.consumers import send_ws_message
from fieldcalendar.models import FieldEvent
from asgiref.sync import sync_to_async, async_to_sync
from django.core.cache import cache, InvalidCacheBackendError


class GISProcessConsumer(JsonWebsocketConsumer):

    def cleanup(self):
        try:
            for h in self.handlers:
                h.cleanup()
        except Exception as e:
            print(e)

    def connect(self):
        try:
            self.handlers = list()
            self.accept()
        except Exception as e:
            print(e)

    def disconnect(self, code):
        self.cleanup()
        raise StopConsumer

    def receive_json(self, content, **kwargs):
        if content.get('request') == "variability":
            self.proc_gis(content)
        elif content.get('request') == "nrec":
            self.proc_nrec(content)
        elif content.get('request') == "layerstub":
            self.proc_stub(content)
        else:
            self.send_json({'error': 'Not sure what to do with this request'})

    def _get_field_and_subfield(self, scope):
        field = Field.objects.get(id=scope['url_route']['kwargs']['pk'])
        sf, created = SubfieldLayer.objects.get_or_create(
            field=field,
            name="Variability Calculation"
        )
        sf.dbf_field_names["Regressions"] = list()
        sf.dbf_field_names['field_names'] = ['Variability']
        sf.dbf_field_names['raster'] = True
        return (field, sf)

    def _save_or_update_raster(self, sf, path, ext='png'):
        with open(path, 'rb') as fh:
            ## Reuse an existing rasterfile object not currently associated with a
            # variability report, delete redundant ones, or make a new one
            if RasterFile.objects.filter(layer=sf, parameter="Variability",
                                         file__icontains=ext,
                                         variabilityreport__isnull=True).count() > 1:
                rf = RasterFile.objects.filter(layer=sf, parameter="Variability",
                                               file__icontains=ext,
                                               variabilityreport__isnull=True).latest('id')
                rf.file.delete()
                rf.file.save(f"variability_{str(uuid.uuid4())[:8]}.{ext}", File(fh), save=True)
                rflist = RasterFile.objects.filter(layer=sf,
                                                   parameter="Variability",
                                                   file__icontains=ext,
                                                   variabilityreport__isnull=True).exclude(id=rf.id)
                for r in rflist:
                    r.file.delete()
                rflist.delete()
            else:
                rf = RasterFile.objects.create(layer=sf, parameter="Variability")
                rf.file.save(f"variability_{str(uuid.uuid4())[:8]}.{ext}", File(fh), save=True)
        return rf

    def _clip_to_boundary(self, results, field, sf):
        if field.is_located and field.area > 0:
            gj = json.loads(field.geojson_boundary())['features'][0]['geometry']
            h = SDXShapefile()
            self.handlers.append(h)
            epsg = int(json.loads(field.geojson_boundary())['crs']['properties']['name'][5:])
            results['image'] = h.clip_to_json(results['image'], json.dumps(gj), epsg)

        rf_png = self._save_or_update_raster(sf, results['image'])
        rf_tif = self._save_or_update_raster(sf, results['tiff'], 'tif')
        return rf_png, rf_tif

    @async_to_sync
    async def _get_raster_values(self, *args):
        return_values = list()
        for arg in args:
            try:
                return_values.append([asyncio.ensure_future(self._parse_param(p)) for p in arg])
            except TypeError:
                return_values.append(None)
        for corolist in return_values:
            await asyncio.gather(*corolist)
        for i, val in enumerate(return_values):
            return_values[i] = [coro.result() for coro in val]
        return tuple(return_values)

    def proc_gis(self, content, zones=False):
        try:
            field, sf = self._get_field_and_subfield(self.scope)
            self.send_json({
                'message': "Received: Takes a while for large files"
            })
            risk_data = content
            yield_parameters = risk_data.get("yield[]", list())
            other_parameters = risk_data.get("others[]", list())
            plotonly_parameters = risk_data.get("plotonly[]", list())
            yield_values, other_values, plotonly_values = self._get_raster_values(yield_parameters,
                                                                                  other_parameters,
                                                                                  plotonly_parameters)

            sdxhelper = SDXShapefile()
            self.handlers.append(sdxhelper)

            ## Computing results for yield as a function of elevation terms

            res = list()
            for yldpar in range(len(yield_parameters)):
                response_var = yield_parameters[yldpar]
                for otherpar in range(len(other_parameters)):
                    indep_var = other_parameters[otherpar]
                    result, warn = sdxhelper.regress(yield_values[yldpar], False,
                                                     other_values[otherpar], False)
                    sf.dbf_field_names["Regressions"].append(f"{response_var}~{indep_var}")
                    sf.dbf_field_values[f"{response_var}~{indep_var}"] = str(result.summary())
                    res.append((response_var, indep_var, result, warn))
                ## Also do a multiple regression
                if len(other_values) > 1:
                    try:
                        result, warn = sdxhelper.multiple_regression(yield_values[yldpar], other_values)
                        sf.dbf_field_names["Regressions"].append(f"{response_var}~{'+'.join(other_parameters)}")
                        sf.dbf_field_values[f"{response_var}~{'+'.join(other_parameters)}"] = str(result.summary())
                        res.append((response_var, ' + '.join(other_parameters), result, warn))
                    except Exception as e:
                        self.send_json({
                            'message': f"Multiple Regression failed with {repr(e)}"
                        })

            risk = [(y, x, r.conf_int().values.tolist(), r.rsquared, repr(w), str(r.summary())) for y, x, r, w in res]
            rf = None
            tif = None
            if (len(risk) > 0):
                self.send_json({
                    'message': "Computed regressions"
                })
            try:
                yield_values.extend(other_values)
                nclusters = int((field.area + 2) / 10)
                division = "equal intervals"
                if zones:
                    nclusters = int((field.area + 2) / 10)  ## round above 2-tenths acre to next higher.
                    division = "equal areas"
                results = sdxhelper.cluster_params(yield_values, nclusters=nclusters)  # edge bias
                self.send_json({
                    'message': f"Computed {nclusters} clusters on {division}"
                })
                rf, tif = self._clip_to_boundary(results, field, sf)
            except Exception as e:
                print(e)
                results = dict()

            sf.save()

            results["yield"] = yield_parameters
            results["others"] = other_parameters
            results["plotsonly"] = plotonly_parameters
            results["risk"] = risk
            results['area'] = field.area
            results["file"] = rf.file.url if rf else None
            results["raster"] = rf.id if rf else None
            results['tiff'] = tif.file.url if tif else None

            self.send_json({
                'message': "done",
                'response': "variability",
                'results': results
            })

        except Exception as e:
            print(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_tb(exc_traceback, limit=3, file=sys.stdout)
            self.send_json({
                'message': "error",
                'description': str(e)
            })

    def _get_raster(self, field, file, subfield, parameter, slope=False):
        """
        Existing rasters are not useful since they may be colorized. We need to
        recalculate rasters as GTiffs again.
        :param field: Field pk
        :param file: Shapefile
        :param subfield: SubfieldLayer pk
        :param parameter: Parameter
        :param slope: Bool (set to true for Elevation_)
        :return: raster GTiff file
        """
        file = parse.unquote(file).replace("/", "\/")
        parameter_fixed = parse.unquote(parameter).replace("/", "")

        subfieldlayer = SubfieldLayer.objects.get(pk=subfield, field=field, name=file)
        ### Level 1 Cache - is the tif file already on the drive?
        ## Look to see if a handler exists with the file in it.
        handler = None
        for h in self.handlers:
            for f in os.listdir(h.folder):
                if parameter_fixed + ".tif" in f:
                    handler = h
                    break

        if handler:
            self.send_json({
                'message': f"reusing {parameter}"
            })
            tifffile = f"{handler.folder}/{parameter_fixed}.tif"
            # if parameter == "dummy":
            #     tifffile = f"{handler.folder}/{parameter_fixed}.tif"
            return tifffile

        # Level 2 Cache - Handler is not present, but perhaps the Rasterfile already exists.
        # If so, make a new handler, download the shapefile and use it.
        # Lambda slope and elevation files should not need to come farther than here.

        assert handler == None, "Handler shouldn't exist here. "

        cached_rasters = RasterFile.objects.filter(layer=subfieldlayer,
                                                   parameter=parameter,
                                                   file__icontains=".tif").count()
        if cached_rasters >= 1:
            rf = RasterFile.objects.filter(layer=subfieldlayer,
                                           parameter=parameter,
                                           file__icontains=".tif").latest('pk')
            RasterFile.objects.filter(layer=subfieldlayer,
                                      parameter=parameter,
                                      file__icontains=".tif").exclude(id=rf.id).delete()
            handler = SDXShapefile()
            self.handlers.append(handler)
            if not os.path.isdir(f"{handler.folder}/{file}"):
                os.mkdir(f"{handler.folder}/{file}")

            with open(f"{handler.folder}/{file}/{parameter_fixed}.tif", 'wb') as f:
                f.write(rf.file.read())
            return f"{handler.folder}/{file}/{parameter_fixed}.tif"

        ## No cache exists, we need to build the tif file from the data sources.

        assert cached_rasters == 0, "No cached rasters should be available here. "

        fe = None
        if subfieldlayer.source_file != "":
            """SoilDx-provided Elevation data sublayers, 
            soil survey data layers or copied layers will not have a source file"""
            fe = FieldEvent.objects.filter(field=field,
                                           file=subfieldlayer.source_file).latest('pk')

        if subfieldlayer.source_file == "" or file == "Soil Survey Data":
            handler = SDXShapefile()
            if parameter == "dummy":
                gj = subfieldlayer.geojson_features(parameter="nccpi2cs", clip=False)
            else:
                gj = subfieldlayer.geojson_features(parameter=parameter, clip=False)
            shp = handler.make_shapefile_for_feature_data("Polygon", gj, parameter)
            handler2 = SDXShapefile(shp)
            handler2.field = field
            raster = handler2.make_raster(parameter, color=None)
            self.handlers.append(handler2)
            handler.cleanup()
        else:
            ## check if fe.file exists in existing handlers.

            existinghandler = None
            for hdl in self.handlers:
                if hdl.file == fe.file:
                    self.send_json({
                        'message': "Reusing handler for {}".format(hdl.file)
                    })
                    # use the local file
                    existinghandler = hdl
                break
            if not existinghandler:
                handler = ZippedShapeFile(fe.file, field)
                self.handlers.append(handler)
            else:
                handler = existinghandler
            # processing = handler.processing
            # while processing:
            #     time.sleep(0.1)
            #     processing = handler.processing
            handler.attach_to_field(shapefile=parse.unquote(file), parameter=parse.unquote(parameter))

            # if "Recommendation" in file:
            #     file = 'rec'
            ## This was part of the nrec processing.

            self.send_json({
                'message': "Computing raster for {}, {}".format(file, parameter)
            })

            if subfieldlayer.field.is_located:
                extent = subfieldlayer.field.boundary.extent
            else:
                extent = None
            raster = handler.make_raster(file, parameter, slope, extent)

        tiffilename = f"{parameter_fixed}.tif"
        with open(raster, 'rb') as fh:
            rf = RasterFile(
                layer=subfieldlayer,
                parameter=parameter,
            )
            rf.file.save(tiffilename, File(fh), save=True)
        return raster

    async def _parse_param(self, str, slope=False):
        ret = str.split(":")
        field, file, subfield, parameter = (ret[1], ret[2], ret[3], ret[4])
        get_raster = sync_to_async(self._get_raster, thread_sensitive=True)
        return await get_raster(field, file, subfield, parameter, slope)

    async def build_stub(self, content, layer):
        template = get_template("clients/gis_layer_stub.html")
        context = {"l": layer, 'field': layer.field, 'layers': layer.field.layer.all()}
        string = template.render(context)
        return string

    async def _cache_get(self, content):
        return await sync_to_async(cache.get)(f"stubs_{content['lyrid']}")

    async def _cache_set(self, content, string, timeout):
        return await sync_to_async(cache.set)(f"stubs_{content['lyrid']}", string, timeout=timeout)

    def _get_or_make_stub(self, content):
        string = async_to_sync(self._cache_get)(content)
        if string is None:
            layer = SubfieldLayer.objects.select_related('field').prefetch_related('field__layer').get(
                id=content['lyrid'])
            string = async_to_sync(self.build_stub)(content, layer)
            try:
                async_to_sync(cache.set)(f"stubs_{content['lyrid']}", string, timeout=None)
            except Exception:
                pass
        layer = SubfieldLayer.objects.get(id=content['lyrid'])
        ser = SubfieldSerializer(layer)
        return {
            'message': content['request'],
            'lyrid': content['lyrid'],
            'div': ser.data
        }

    def proc_stub(self, content):
        self.send_json({
            'message': 'Received ' + content['lyrid'],
        })
        try:
            message = self._get_or_make_stub(content)
            self.send_json(message)
        except Exception as e:
            print(content)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=4, file=sys.stdout)

    def proc_nrec(self, content):
        self.send_json({
            'message': "Received"
        })

        try:
            field, sf = self._get_field_and_subfield(self.scope)
            rasters = list()
            factors = list()
            shapes = list()
            for k, v in content['shapes'].items():
                shapes.append(v)
                try:
                    rasters.append(self._parse_param(v))
                except Exception as e:
                    try:
                        rasters.append(self._get_raster(field, "Soil Survey Data", "dummy"))
                    except Exception as e:
                        print("error", e)
            for k, v in content['factors'].items():
                try:
                    factors.append(float(v))
                except Exception as e:
                    factors.append(None)
            handler = SDXShapefile()
            self.handlers.append(handler)
            multiraster = handler.combine_rasters(rasters)
            gj = json.loads(field.geojson_boundary())['features'][0]['geometry']
            epsg = int(json.loads(field.geojson_boundary())['crs']['properties']['name'][5:])
            nrec = handler.generate_nrec(multiraster, shapes, factors, json.dumps(gj), epsg)

            recsf = SubfieldLayer.objects.create(
                name="Recommendation",
                field=field,
                dbf_field_names={'field_names': list()},
            )

            recsf.name = f"{recsf.name}_{recsf.pk}"

            with open(nrec['shapefile'], 'rb') as recfile:
                fe = FieldEvent.objects.create_file_upload_event(field,
                                                                 "Recommendation",
                                                                 datetime.datetime.now(),
                                                                 datetime.datetime.now())
                fe.file.save(f"{recsf.field.pk}/{recsf.name}.zip", File(recfile), save=True)

            nrec['shapefile'] = fe.file.name
            recsf.source_file = fe.file.name
            recsf.save()
            unzip = zip(*nrec['seeding_rate'])
            recsf.add_points(unzip.__next__())
            self._add_feature(recsf, 'Seeding Rate', unzip.__next__(), nrec.pop('seeding_rate_img'))
            nrec.pop('seeding_rate')

            unzip = zip(*nrec['nitrogen'])
            unzip.__next__()
            self._add_feature(recsf, 'Nitrogen', unzip.__next__(), nrec.pop('nitrogen_img'))
            nrec.pop('nitrogen')

            unzip = zip(*nrec['yield_goal'])
            unzip.__next__()
            self._add_feature(recsf, 'Yield Goal', unzip.__next__(), nrec.pop('yield_goal_img'))
            nrec.pop('yield_goal')

            unzip = zip(*nrec['lime'])
            unzip.__next__()
            self._add_feature(recsf, 'Lime', unzip.__next__(), nrec.pop('lime_img'))
            nrec.pop('lime')

            unzip = zip(*nrec['responsiveness'])
            unzip.__next__()
            self._add_feature(recsf, 'Responsiveness', unzip.__next__(), nrec.pop('responsiveness_img'))
            nrec.pop('responsiveness')

            unzip = zip(*nrec['slope_loss'])
            unzip.__next__()
            self._add_feature(recsf, 'Slope Loss', unzip.__next__(), nrec.pop('slope_loss_img'))
            nrec.pop('slope_loss')

            unzip = zip(*nrec['supply'])
            unzip.__next__()
            self._add_feature(recsf, 'Supply', unzip.__next__(), nrec.pop('supply_img'))
            nrec.pop('supply')

            recsf.dbf_field_values['nrec_parameters'] = nrec
            recsf.save()

            self.send_json({
                'message': "done",
                'response': "nrec",
                'results': nrec
            })

        except Exception as e:
            self.send_json({
                'message': "error",
                'description': str(e)
            })

    def _add_feature(self, subfield, name, values, rasterfile):
        subfield.add_feature(name, values)
        with open(rasterfile, 'rb') as fh:
            rf = RasterFile(
                layer=subfield,
                parameter=name,
            )
            rf.file.save(f"{subfield.name}/{name}.png", File(fh), save=True)
            rf.save()


class ShapefileProcessConsumer(SyncConsumer):
    def _process_shapefile(self, fe, field):
        res = None
        handler = ZippedShapeFile(fe.file, field)
        try:
            res = handler.attach_to_field(process_type=fe.event_type.name)
            return res
        except Exception as e:
            print(e)
        finally:
            handler.cleanup()

    def _process_tifffile(self, fe, field):
        ## Copy the file to local
        tmp = tempfile.mkdtemp(prefix="tif")
        file = os.path.basename(fe.file.name)
        with open(f"{tmp}/{file}", "wb+") as dest:
            for chunk in fe.file.chunks():
                dest.write(chunk)

        handler = TiffFile(dest.name, field, tmp)
        try:
            assert handler.valid_file(), f"{handler.file} is not a valid tiff file"
            l, r, c = handler.create_layers_and_rasterfiles(field, file, fe.title)
            handler.process_tiff()
            time = timezone.now()
            return [(l, time, time)]

        except Exception as e:
            raise
        finally:
            handler.cleanup()

    def process(self, message):
        try:
            res = None
            if 'layer' in message.keys():
                ## This is a background raster processing command
                layer = SubfieldLayer.objects.get(id=message['layer'])
                file = message['file']
                parameter = message['parameter']
                msg = f"Received background rasterization task for layer {layer}, parameter {parameter}"
                start = time.time()
                send_ws_message(message["user"], msg)
                res = layer.make_raster(file, parameter, save_tif=True)
                elapsed = time.time() - start
                send_ws_message(message["user"],
                                f"Raster ready: layer {layer}, parameter {parameter}, took {elapsed} secs")
            else:
                field = Field.objects.get(id=message["field"])
                fe = FieldEvent.objects.get(id=message["fieldevent"])
                if fe.file.name[-3:] == 'zip':
                    message_text = f"recieved zipfile, field {field}, event {fe}, file {fe.file}"
                    send_ws_message(message["user"], message_text)
                    res = self._process_shapefile(fe, field)
                elif fe.file.name[-3:] == 'tif':
                    message_text = f"recieved zipfile, field {field}, event {fe}, file {fe.file}"
                    send_ws_message(message["user"], message_text)
                    res = self._process_tifffile(fe, field)
                if res:
                    for layer, start, end in res:
                        layer.source_file = fe.file.name
                        layer.save()
                        send_ws_message(message["user"], f"Created Layer {layer.name}")
                        print(f"Created Layer {layer.name}")

                        if layer.dbf_field_values.get("process_logs"):
                            if layer.dbf_field_values.get("process_logs").get("boundary_assumed"):
                                field.boundary = layer.polygons
                                field.save()
                        if start:
                            FieldEvent.objects.create_journal_entry(
                                field=field,
                                title="New layer: " + layer.name,
                                entry="Layer created",
                                start=timezone.make_aware(datetime.datetime(start.year, start.month, start.day)),
                                end=timezone.make_aware(datetime.datetime(end.year, end.month, end.day))
                            )
            return res

        except Exception as e:
            # print("Exception", repr(e), message)
            # print(settings.DATABASES)
            exc_type, exc_value, exc_traceback = sys.exc_info()

            # traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            # print("*** print_exception:")

            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=2, file=sys.stdout)
            # print("*** print_exc:")
            # traceback.print_exc()
            # print("*** format_exc, first and last line:")
            # formatted_lines = traceback.format_exc().splitlines()
            # print(formatted_lines[0])
            # print(formatted_lines[-1])
            # print("*** format_exception:")
            # print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            # print("*** extract_tb:")
            # print(repr(traceback.extract_tb(exc_traceback)))
            # print("*** format_tb:")
            # print(repr(traceback.format_tb(exc_traceback)))
            # print("*** tb_lineno:", exc_traceback.tb_lineno)
