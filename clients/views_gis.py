import datetime
import json
import os
import tempfile
from urllib import parse
from zipfile import ZipFile

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.contrib.gis.geos import MultiPoint, GEOSGeometry, GeometryCollection
from django.core.exceptions import PermissionDenied
from django.db.models import Prefetch
from django.http import HttpResponse, Http404
from django.http import JsonResponse
from django.shortcuts import HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.timezone import now
from django.views import View
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import permissions, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from associates.role_privileges import DealershipRequiredMixin, FilteredObjectMixin, FilteredQuerySetMixin
from clients.gis.gishelper import SDXShapefile
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.views import FieldDetail
from common.apihelper import api_group_permission_classfactory
from common.consumers import send_ws_message
from fieldcalendar.event_base import CalendarEventKind
from fieldcalendar.models import FieldEvent
from formulas.models import Formula
from orders.serializers import FieldOrderSerializer
from products.models import Category, TestParameter
from .forms import (FieldGeoDetailsForm, FieldRiskDetailsForm, DisplayVariabilityReportForm,
                    SamplingMapForm, SamplingMapUpdateForm, DisplayFieldForm, SubFieldForm, SamplingPointsForm)
from .gis.tif_handler import TiffFile
from .gis.utils import get_all_file_paths
from .models import Field, SubfieldLayer, RasterFile, VariabilityReport
from .pdfs import render_to_pdf
from clients.soilsmixin import make_geometrycollection_from_featurecollection

SLOPE_LEGEND = [
    ["#0D0887", "Flat", 0],
    # ["#41049D", "Very Low", 0.25],
    ["#6900A8", "Low", 0.75],
    # ["#8E0CA4", "Mild", 1],
    ["#CD4A76", "Gentle", 2],
    # ["#E26660", "Moderate", 3],
    ["#F3854B", "Medium", 4],
    ["#FCA835", "High", 5],
    ["#FCCE25", "Very High", 10],
    # ["#F0F921", "Black Diamond", 20]
]

ELEVATION_LEGEND = [
    ["#253494", "Valley", 0],
    ["#330EBC", "Lower", 33],
    ["#41B6C4", "Middle", 50],
    ["#81CEBA", "Higher", 66],
    ["#FFFFCC", "Peak", 100]
]
CLIPPING_THRESHOLD = 120


class FieldGeoDetails(FieldDetail):
    template_name = "clients/field_geo_details.html"
    form_class = FieldGeoDetailsForm

    def get_context_data(self, **kwargs):
        context = super(FieldGeoDetails, self).get_context_data(**kwargs)
        context['formtype'] = "Geospatial Tools"
        context['layers'] = self.get_object().layer.all()
        return context

    def get_success_url(self):
        field = Field.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('field_geo_details', kwargs={'pk': field.pk})

    def form_valid(self, form):
        if form.cleaned_data['report_data'] and form.data.get('genreport'):
            data = json.loads(form.cleaned_data['report_data'])
            raster = RasterFile.objects.get(id=int(data["raster"]))
            report = VariabilityReport.objects.create(
                analyst=self.orguser,
                image=raster,
                data=data,
                annotations=form.cleaned_data['boundary']
            )
            field = Field.objects.get(pk=self.kwargs["pk"])

            FieldEvent.objects.create_journal_entry(
                field=field,
                title="Variability Report",
                entry=f"Report ID {report.pk}",
                start=timezone.now(),
                end=timezone.now()
            )

            report.description = form.cleaned_data['notes']
            report.save()

            return HttpResponseRedirect(reverse_lazy('field_variability_report', kwargs={'pk': report.pk}))

        return super().form_valid(form)


class SubfieldLayerDetails(FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        object = self.get_object()
        context['field'] = object.field
        context['farm'] = object.field.farm
        context['client'] = object.field.farm.client
        context['formtype'] = "Subfield Details"
        return context


class SubfieldLayerUpdate(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"
    form_class = SubFieldForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        object = self.get_object()
        context['subfield'] = object
        context['field'] = object.field
        context['farm'] = object.field.farm
        context['client'] = object.field.farm.client
        context['formtype'] = "Subfield Update"
        return context


class SubfieldLayerDownloadShape(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def get(self, request, pk, *args, **kwargs):
        subfield = self.get_object()
        zipdir, zipfile = subfield.create_shapefile_from_layers()
        with open(f"{zipdir}/{zipfile}", 'rb') as f:
            response = HttpResponse(f.read(), content_type="application/zip")
        response['Content-Disposition'] = 'inline; filename=' + os.path.basename(zipfile)
        return response


class FieldGeoLayer(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'farm__client'
    hallpass_field = ""

    def get(self, request, pk, subfield, file, parameter, *args, **kwargs):
        """
        :param request:
        :param pk: Field ID
        :param subfield: Subfield ID
        :param file: file name
        :param parameter: Parameter
        :param args:
        :param kwargs:
        :return: GeoJSON object with a few additional parameters:
            "soils": True if the parameter requested is SSURGO,
            "few_points": False if the number of points is decimated (typical for yield maps and drone images),
                          True for soil test data,
            "legend": Custom legend if available for parameter.
            "usgs": True if the requested param is USGS 10m Elevation or slope. There is no GeoJSON Feature data
            in this case.
            "subfield_type":    points_only (yield, soil tests),
                                polygons_only (ssurgo, region boundaries) or
                                polygons_to_points (dense polygons converted to centroids)
        """
        field = self.get_object()

        ## if the parameter is USGS 10m Elevation, we skip this response
        if file == 'USGS%2010m%20Elevation' or file == 'USGS 10m Elevation':
            response = json.loads(field.boundary.geojson)
            response['parameter'] = parse.unquote(parameter)
            if parameter == "slope_public_10m":
                response['legend'] = SLOPE_LEGEND
                response['min'] = 0
                response['max'] = 20
            else:
                response['legend'] = ELEVATION_LEGEND
                response['min'] = 0
                response['max'] = 100
            response['usgs'] = True
            return JsonResponse(response)

        ## Check if this layer already exists. If not, then recreate it from the file
        subfieldlayer = SubfieldLayer.objects.get(id=subfield, field=field, name=parse.unquote(file))
        values = subfieldlayer.get_values(parse.unquote(parameter))

        raster = subfieldlayer.dbf_field_names.get('raster', False)
        if not values and raster:
            ## This is a raster layer.
            response = json.loads(field.boundary.geojson)
            response['parameter'] = parse.unquote(parameter)
            try:
                par = TestParameter.objects.get_parameter_by_string(parameter)
                legend = par.renderer.legend
            except:
                legend = None
            response['legend'] = legend
            response['numeric'] = True
            return JsonResponse(response)

        if not values:
            fe = FieldEvent.objects.filter(field=field,
                                           file=subfieldlayer.source_file).last()
            if not fe:
                return JsonResponse({"error": "No data in the subfield layer."})
            handler = ZippedShapeFile(fe.file, field)
            try:
                handler.attach_to_field(shapefile=parse.unquote(file), parameter=parse.unquote(parameter))
            except Exception as e:
                print(e)
            finally:
                handler.cleanup()
            subfieldlayer.refresh_from_db()

        clip = False if not values or len(values) <= CLIPPING_THRESHOLD else True

        try:
            response = subfieldlayer.geojson_features(parameter=parse.unquote(parameter), clip=clip)
            response['parameter'] = parse.unquote(parameter)
            return JsonResponse(response)
        except TypeError:
            return JsonResponse({'status': 'type error'}, status=500)


def raster_helper(file, subfieldlayer, parameter, username):
    try:
        png = RasterFile.objects.get(parameter=parse.unquote(parameter),
                                     layer=subfieldlayer,
                                     file__icontains=".png")
    except RasterFile.MultipleObjectsReturned:
        pngs = RasterFile.objects.filter(parameter=parse.unquote(parameter),
                                         layer=subfieldlayer,
                                         file__icontains=".png")
        png = pngs.latest("id")
        pngs_to_delete = pngs.exclude(id__exact=png.id)
        pngs_to_delete.delete()
    except RasterFile.DoesNotExist:
        ## This may be a Risk raster:
        if subfieldlayer.features > 5000:
            ## Let's construct the raster on a background task
            print("Sending to background task")
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.send)("process-uploaded-shapefile", {
                "type": "process",
                "user": username,
                "layer": subfieldlayer.id,
                "file": parse.unquote(file),
                "parameter": parse.unquote(parameter)
            })
            return JsonResponse({'task': 'backgrounded'})
        else:
            try:
                png = subfieldlayer.make_raster(file=parse.unquote(file), parameter=parse.unquote(parameter))
            except AttributeError as e:
                print(repr(e))
                raise Http404(repr(e))
    return png


class FieldGeoRaster(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'farm__client'
    hallpass_field = ""

    permissions_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk, subfield, file, parameter, *args, **kwargs):
        ## Check if this layer already exists. If not, then recreate it from the file
        field = self.get_object()
        subfieldlayer = SubfieldLayer.objects.get(id=subfield, field=field, name=parse.unquote(file))
        result = raster_helper(file, subfieldlayer, parameter, self.request.user.username)
        if isinstance(result, RasterFile):
            with result.file.open("rb") as f:
                response = HttpResponse(f.read(), content_type="image/png")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(result.file.name)
                return response
        return result

    def post(self, request, pk, file, parameter, *args, **kwargs):
        field = self.get_object()
        try:
            subfieldlayer = SubfieldLayer.objects.get(field=field, name="soildx")
        except SubfieldLayer.DoesNotExist:
            subfieldlayer = SubfieldLayer.objects.create(field=field, name="soildx")
        try:
            raster = RasterFile.objects.get(parameter=parameter,
                                            layer=subfieldlayer)
            raster.file = file
            raster.save()
            response = HttpResponse("Update complete", status=200)
            return response
        except RasterFile.DoesNotExist:
            new = RasterFile.objects.create(
                parameter=parameter,
                layer=subfieldlayer,
                file=self.request.FILES['file']
            )
            response = HttpResponse("Upload complete", status=201)
            return response
        except:
            return HttpResponse('Error occurred', status=500)


def delete_rasters(subfield):
    rfs = RasterFile.objects.filter(layer=subfield)
    # if subfield.dbf_field_names.get('raster'):
    #     # Leave the raw file for the raster layer.
    #     rfs = rfs.exclude(parameter=subfield.name)
    try:
        files = [r.deletefile(r.file) for r in rfs]
        rfs.delete()
    except Exception as e:
        print(e)
        files = None
    return files


class DuplicateSubfieldLayer(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def post(self, request, pk, *args, **kwargs):
        subfield = self.get_object()
        try:
            # assert subfield.polygons_only, "Copied layer must be a polygons-only layer"
            newsf = SubfieldLayer.objects.create(
                name=f"Copy of {subfield.name}",
                field=subfield.field,
                geoms=subfield.geoms,
            )
            return JsonResponse(newsf.name, safe=False)
        except AssertionError as e:
            return JsonResponse(repr(e), safe=True)


class BringInSubfieldLayer(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def post(self, request, pk, target, param, agg, *args, **kwargs):
        source_subfield = self.get_object()
        target_subfield = SubfieldLayer.objects.get(id=target)
        param = parse.unquote(param)
        try:
            target_subfield.bring_in(source_subfield, parameter=param, aggfun=agg)
            return JsonResponse({"status": "success",
                                 "message": f"Merged {agg} of parameter {param} of {source_subfield.name} into {target_subfield.name}",
                                 "target": target_subfield.id},
                                safe=False)
        except Exception as e:
            return JsonResponse({'message': repr(e)}, safe=True)


class ApplyFormulaOnSubfieldLayer(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def post(self, request, pk, formula, *args, **kwargs):
        subfield = self.get_object()
        formula = Formula.objects.get(id=formula)
        user = request.user
        try:
            formula.evaluate(subfield, user)
            return JsonResponse({"status": "success",
                                 "message": f"Formula applied. Refreshing for results",
                                 "target": subfield.id},
                                safe=False)
        except Exception as e:
            return JsonResponse(repr(e), safe=True)


class DeleteSubfieldView(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'field__farm__client'

    def get_success_url(self):
        field = self.get_object().field
        return reverse_lazy('field_geo_details', kwargs={'pk': field.pk})

    def delete(self, request, *args, **kwargs):
        subfield = self.get_object()
        delete_rasters(subfield)
        return super().delete(request, *args, **kwargs)


class RemoveRasterCache(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'field__farm__client'
    hallpass_field = "field"

    def post(self, request, pk, *args, **kwargs):
        subfield = self.get_object()
        files = delete_rasters(subfield)
        return JsonResponse(files, safe=False)


LambdaGroupPermissions = api_group_permission_classfactory("LambdaGroupPermissions", "GISLambda")


class FieldGeoRasterPost(APIView, SingleObjectMixin):
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'farm__client'

    permissions_classes = (permissions.IsAuthenticated, LambdaGroupPermissions)

    def post(self, request, pk, file, parameter, layer, *args, **kwargs):
        field = self.get_object()
        try:
            tempdir = tempfile.mkdtemp(prefix="tif")
            with open(f"{tempdir}/{file}", 'wb+') as dest:
                for chunk in request.FILES['file'].chunks():
                    dest.write(chunk)

            self.rasterhandler = TiffFile(tiffile=f"{tempdir}/{file}", field=field, tmpdir=tempdir)
            sf, raster, created = self.rasterhandler.create_layers_and_rasterfiles(field, layer, parameter)
            if created:
                response = Response("Upload complete", status=201)
            else:
                response = Response("Upload complete", status=200)
            # FieldEvent.objects.create_raster_upload_event(layer, raster)
            send_ws_message(field.farm.client.originator.username, f"{parameter} image available")
            return response
        except Exception as e:
            raise
        finally:
            try:
                self.rasterhandler.cleanup()
            except:
                pass


class FieldRiskReport(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    model = Field
    template_name = "clients/field_risk_report.html"
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'farm__client'
    form_class = FieldRiskDetailsForm
    hallpass_field = ""

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        field = Field.objects.get(pk=self.kwargs['pk'])
        kwargs['farm'] = field.farm.pk
        kwargs['field'] = field.pk
        kwargs['org'] = self.org
        kwargs['comments'] = field.notes
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        field = Field.objects.get(pk=self.kwargs['pk'])
        context['formtype'] = "Field Variability Evaluation"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['files'] = FieldEvent.objects.filter(field=field).filter(file__isnull=True)
        context['layers'] = SubfieldLayer.objects.filter(field=field)
        data = json.loads(self.request.GET.dict()["data"])
        pixels = sum(data["counts"]) - data["counts"][0]
        areas = [c / pixels * 100 for c in data["counts"]]

        t = zip(data["colors"], areas, data["centers"])

        raster = RasterFile.objects.get(id=int(data["raster"]))
        v = VariabilityReport.objects.create(
            analyst=self.orguser,
            image=raster,
            data=data
        )
        context["data"] = data
        context['table'] = t
        context["report"] = v
        context['client'] = field.farm.client
        return context

    def form_valid(self, form):
        if form.cleaned_data['report'] and form.cleaned_data['comments']:
            report = VariabilityReport.objects.get(pk=form.cleaned_data['report'])
            field = Field.objects.get(pk=self.kwargs["pk"])

            fe = FieldEvent.objects.create(
                field=field,
                event_id="risk_report",
                start=timezone.now(),
                editable=False
            )

            report.description = form.cleaned_data['comments']
            report.save()

        return HttpResponseRedirect(reverse_lazy('field_variability_report', kwargs={'pk': report.pk}))


class FieldRiskReportUpdate(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    model = VariabilityReport
    template_name = "clients/field_risk_report.html"
    instance_owner_organization = "image__layer__field__farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'image__layer__field__farm__client'
    form_class = FieldRiskDetailsForm
    hallpass_field = "image__layer__field"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        report = VariabilityReport.objects.get(pk=self.kwargs['pk'])
        kwargs['farm'] = report.image.layer.field.farm.pk
        kwargs['field'] = report.image.layer.field.pk
        kwargs['org'] = self.org
        # kwargs['comments'] = report.description
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        report = VariabilityReport.objects.get(pk=self.kwargs['pk'])
        field = report.image.layer.field
        data = report.data
        context['formtype'] = "Edit Field Variability Evaluation"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['files'] = FieldEvent.objects.filter(field=field).filter(file__isnull=True)

        context['layers'] = SubfieldLayer.objects.filter(field=field)
        pixels = sum(data["counts"]) - data["counts"][0]
        areas = [c / pixels * 100 for c in data["counts"]]

        t = zip(data["colors"], areas, data["centers"])
        context['field'] = field
        context["data"] = data
        context['table'] = t
        context["report"] = report
        context['client'] = field.farm.client
        return context

    def get_success_url(self):
        report = VariabilityReport.objects.get(pk=self.kwargs['pk'])
        if "printview" in self.request.POST:
            return reverse_lazy('field_variability_report', kwargs={'pk': report.pk})
        else:
            return reverse('field_risk_update', kwargs={'pk': report.pk})

    # def form_valid(self, form):
    #     if form.cleaned_data['report'] and form.cleaned_data['description']:
    #         import pdb; pdb.set_trace()
    #         report = VariabilityReport.objects.get(pk=self.kwargs['pk'])
    #
    #         # fe = FieldEvent.objects.create(
    #         #     field=field,
    #         #     event_id="risk_report",
    #         #     start=timezone.now(),
    #         #     editable=False
    #         # )#
    #         report.generated = timezone.now()
    #         report.description = form.cleaned_data['description']
    #         report.save()
    #
    #     return HttpResponseRedirect(self.get_success_url())


class PDFResponseMixin:
    """
    Mixin for Django class based views.
    Switch normal and pdf template based on request.

    The switch is made when the request has a particular querydict, per
    class attributes, `pdf_querydict_keys` and `pdf_querydict_value`
    example:

        http://www.example.com?[pdf_querydict_key]=[pdf_querydict_value]

    Example with values::

        http://www.example.com?format=pdf

    Simplified version of snippet here:
    http://djangosnippets.org/snippets/2540/
    """
    pdf_querydict_key = 'format'
    pdf_querydict_value = 'pdf'

    def is_pdf(self):
        value = self.request.GET.get(self.pdf_querydict_key, '')
        return value.lower() == self.pdf_querydict_value.lower()

    def get_pdf_response(self, context, **response_kwargs):
        return render_to_pdf(context, request=self.request, **response_kwargs)

    def render_to_response(self, context, **response_kwargs):
        if self.is_pdf():
            return self.get_pdf_response(context, **response_kwargs)
        return super(PDFResponseMixin, self).render_to_response(
            context, **response_kwargs)


class FieldVariabilityReport(PDFResponseMixin, FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    model = VariabilityReport
    instance_owner_organization = "analyst__organization"
    client_has_permission = False
    instance_owner_client = ''

    def get_context_data(self, **kwargs):
        context = super(FieldVariabilityReport, self).get_context_data(**kwargs)
        context['field'] = self.object.field
        context['report'] = VariabilityReport.objects.get(id=self.kwargs["pk"])
        data = context['report'].data
        pixels = sum(data["counts"]) - data["counts"][0]
        areas = [c / pixels * 100 for c in data["counts"]]
        context['table'] = zip(data["colors"], areas, data["centers"])
        # context['yield_plots'] = [(par, DisplayFieldForm(instance=self.object.field)) for par in data.get("yield")]
        # context['other_plots'] = [(par, DisplayFieldForm(instance=self.object.field)) for par in data.get("others")]
        # context['just_plots'] = [(par, DisplayFieldForm(instance=self.object.field)) for par in data.get("plotsonly")]
        context['yield_plots'] = [(par, None) for par in data.get("yield")]
        context['other_plots'] = [(par, None) for par in data.get("others")]
        context['just_plots'] = [(par, None) for par in data.get("plotsonly")]

        context['fielddisplay'] = DisplayVariabilityReportForm(instance=context['report'])
        context['formtype'] = "Field Variability Report"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['org'] = self.org
        return context


class FieldVariabilityReportDelete(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    model = VariabilityReport
    instance_owner_organization = "analyst__organization"
    client_has_permission = False

    def get_success_url(self):
        vr = VariabilityReport.objects.get(pk=self.kwargs['pk'])
        field = vr.image.layer.field
        return reverse_lazy('field_detail', kwargs={'pk': field.pk})


class GenerateSampleGridFromPoints(DealershipRequiredMixin, UpdateView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    template_name = "clients/field_sampling_map.html"
    form_class = SamplingMapForm

    def get_object(self, queryset=None):
        object = super().get_object(queryset)
        self.rasterfile = object.rasterfile_set.filter(file__icontains=".png").first().file.url
        return object

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['field'] = self.object.field.pk
        kwargs['file'] = self.rasterfile
        kwargs['create'] = self.request.GET.get('create', False)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lllng, lllat, urlng, urlat = list(self.object.field.boundary.extent)
        context['extent'] = [lllat, lllng, urlat, urlng]
        context['file'] = self.rasterfile
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['formtype'] = "Generate Grid from Sample Points"
        context['field'] = self.object.field
        context['client'] = self.object.field.farm.client
        context['farm'] = self.object.field.farm
        return context

    def form_valid(self, form):
        lyr = self.get_object()
        dbf_field_names = lyr.dbf_field_names
        dbf_field_values = lyr.dbf_field_values
        if "Serial" not in dbf_field_names['field_names']:
            dbf_field_names['field_names'].append("Serial")
            dbf_field_values['Serial'] = list(range(1, lyr.features + 1))
        if self.request.GET.get('create', False):
            ## this is a new subfield based on another one
            geoms = make_geometrycollection_from_featurecollection(form.cleaned_data['regions'])
            newlyr = SubfieldLayer.objects.create(name=form.cleaned_data['name'],
                                                  field=form.cleaned_data['field'],
                                                  geoms=geoms,
                                                  editable=True,
                                                  dbf_field_names=dbf_field_names,
                                                  dbf_field_values=dbf_field_values)
        else:
            ## This is an update of an existing form - nothing other than name and features should be changed.
            assert form.instance.editable, "Subfield layer not editable"
            geoms = form.cleaned_data['geoms']
            form.instance.name = form.cleaned_data['name']
            form.instance.geoms = geoms
            form.instance.dbf_field_values['Serial'] = list(range(1, len(geoms) + 1))
            form.instance.save()
        return HttpResponseRedirect(reverse_lazy("field_geo_details", kwargs={'pk': form.instance.field.pk}))


class GenerateSamplePoints(DealershipRequiredMixin, CreateView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False
    template_name = "clients/field_sampling_map.html"
    form_class = SamplingPointsForm

    def get_success_url(self):
        object = Field.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('field_geo_details', kwargs={'pk': object.pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['field'] = self.kwargs['pk']
        kwargs['file'] = self.request.GET.get('file')
        kwargs['create'] = 1
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extent'] = self.request.GET.get('extent')
        context['file'] = self.request.GET.get('file')
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['formtype'] = "Generate Sample Points"
        field = Field.objects.get(pk=self.kwargs.get('pk'))
        context['field'] = field
        context['client'] = field.farm.client
        context['farm'] = field.farm
        return context

    def form_valid(self, form):
        filename = form.cleaned_data['source_file'].split("/")[-3:]
        filename = "/".join(filename)
        rf = RasterFile.objects.filter(file=filename).first()
        newsf = form.save(commit=False)
        newsf.editable = True
        newsf.save()
        rf.pk = None
        rf.layer = newsf
        rf.save()
        return HttpResponseRedirect(reverse_lazy("field_geo_details", kwargs={'pk': newsf.field.pk}))


class UpdateSamplePoints(DealershipRequiredMixin, UpdateView):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = True
    template_name = "clients/field_sampling_map.html"
    form_class = SamplingMapUpdateForm

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        return obj

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['field'] = self.get_object().field.pk
        kwargs['file'] = self.get_object().source_file
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        a, b, c, d = self.get_object().field.boundary.extent
        context['extent'] = [b, a, d, c]
        context['file'] = self.get_object().source_file
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['formtype'] = "Update Sample Points and Region Boundaries"
        field = self.get_object().field
        context['field'] = field
        context['client'] = field.farm.client
        context['farm'] = field.farm
        return context


class GenerateSampleShapeFile(FilteredObjectMixin, DealershipRequiredMixin, SingleObjectMixin, View):
    model = SubfieldLayer
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if obj.name == "Sampling Map":
            return obj
        raise PermissionDenied()

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        handler = SDXShapefile()

        ## Make points file from geojson layer
        gj = obj.geojson_features(parameter="serial", clip=False)
        epsg = 4326

        pts = handler.make_shapefile_for_samplepoints(gj, "points", epsg)

        rf = RasterFile.objects.get(file=obj.source_file.split('media/')[1])
        ## Make zone shapefile from raster
        with open(f"{handler.folder}/zones.png", 'wb') as f:
            f.write(rf.file.read())
        handler.polygonize(f"{handler.folder}/zones.png", obj.field.boundary.extent)
        filepaths = get_all_file_paths(pts)
        with ZipFile(f"{pts}/{obj.field.name}_samplepoints.zip", 'w') as f:
            for file in filepaths:
                f.write(file, os.path.basename(file))
        fe = FieldEvent.objects.create_journal_entry(obj.field, "Sample point creation",
                                                     "Created sample points shapefile",
                                                     now(), now(), False, False)

        with open(f"{pts}/{obj.field.name}_samplepoints.zip", 'rb') as f:
            fe.file.save(f"shapefiles/{obj.field.id}/samplepoints.zip", f)
            fe.save()
        with open(f"{pts}/{obj.field.name}_samplepoints.zip", 'rb') as f:
            response = HttpResponse(f.read(), content_type='application/zip')
            response['Content-Disposition'] = 'inline; filename=' + f"{obj.field.name}_samplepoints.zip"
        handler.cleanup()
        return response


class BirdsEyeView(DealershipRequiredMixin, TemplateView):
    template_name = "clients/birdseye.html"

    def get_context_data(self, **kwargs):
        context = super(BirdsEyeView, self).get_context_data(**kwargs)
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['event_categories'] = CalendarEventKind.objects.all()
        context['product_categories'] = Category.objects.all()
        return context
