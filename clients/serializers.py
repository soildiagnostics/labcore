import base64

from django.contrib.gis.geos import GEOSGeometry
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import APIException

from contact.models import User
from fieldcalendar.serializers import FieldEventSerializer
from formulas.models import Formula, FormulaVariable
from formulas.serializers import FormulaSerializer
from .models import Client, Farm, Field, HallPass, SubfieldLayer
from contact.serializers import ContactSerializer
import json


class ZipFileSerializer(serializers.Serializer):
    zipfile = serializers.FileField()
    event = serializers.CharField()
    event_date = serializers.DateTimeField(required=False)


class FieldSerializer(serializers.ModelSerializer):
    area = serializers.SerializerMethodField()

    def get_area(self, obj):
        return obj.area

    class Meta:
        model = Field
        fields = "__all__"


class FieldGeoJSONSerializer(serializers.ModelSerializer):
    boundary = serializers.SerializerMethodField()
    centroid = serializers.SerializerMethodField()
    farm = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    area = serializers.SerializerMethodField()


    def get_area(self, obj):
        return obj.area

    def get_farm(self, obj):
        return obj.farm.name

    def get_client(self, obj):
        return str(obj.farm.client)

    def get_centroid(self, obj):
        if self.context['request'].query_params.get("centroid", 0):
            try:
                return json.loads(obj.centroid)
            except Exception:
                try:
                    return json.loads(obj.boundary.centroid.geojson)
                except Exception:
                    return None
        return None

    def get_boundary(self, obj):
        if not self.context['request'].query_params.get("centroid", 0):
            try:
                return json.loads(obj.geojson_boundary())
            except Exception:
                return None
        return None

    class Meta:
        model = Field
        fields = "__all__"


class FieldLocationGeoJSONSerializer(serializers.ModelSerializer):
    centroid = serializers.SerializerMethodField()

    def get_centroid(self, obj):
        try:
            return json.loads(obj.geojson_boundary())
        except TypeError:
            return None

    class Meta:
        model = Field
        fields = "__all__"


class FieldBoundarySerializer(serializers.ModelSerializer):
    area = serializers.SerializerMethodField()

    def get_area(self, obj):
        return obj.area
    class Meta:
        model = Field
        fields = ("name", "boundary", "area")


class FarmSerializer(serializers.ModelSerializer):
    fields = FieldSerializer(many=True, required=False)
    area = serializers.SerializerMethodField()
    def get_area(self, obj):
        return obj.area
    class Meta:
        model = Farm
        fields = ("id", "name", "fields", "area")


class FarmCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Farm
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    area = serializers.SerializerMethodField()
    def get_area(self, obj):
        return obj.area

    def get_name(self, obj):
        return str(obj.contact)

    farms = FarmSerializer(many=True)

    class Meta:
        model = Client
        fields = ("id", "name", "area", "farms")


class ClientIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ("id",)


class ClientCreateSerializer(ContactSerializer):
    """
    This serializer always creates a new contact
    """
    pass

class ClientStatsSerializer(serializers.Serializer):
    clients = serializers.IntegerField()
    farms = serializers.IntegerField()
    fields = serializers.IntegerField()
    avg_farm = serializers.FloatField()
    avg_field = serializers.FloatField()
    avg_client = serializers.FloatField()

class FieldSerializerBinaryFiles(serializers.ModelSerializer):
    plat_file = serializers.SerializerMethodField()

    def get_plat_file(self, obj):
        if obj.plat_file:
            with obj.plat_file.open(mode="rb") as f:
                return base64.b64encode(f.read())
        return b''

    class Meta:
        model = Field
        fields = "__all__"


class HallPassRequestSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    field = serializers.SerializerMethodField()

    def get_username(self, obj):
        return obj.user.username

    def get_field(self, obj):
        return obj.pk

    def create(self, validated_data):
        data = validated_data.copy()
        data['created_by'] = self.context['request'].user

        try:
            data['user'] = User.objects.get(username=self.initial_data.get('username'))
        except User.DoesNotExist:
            raise UserNotFound()

        try:
            data['field'] = Field.objects.get(id=int(self.initial_data.get('field')))
        except Field.DoesNotExist:
            raise FieldNotFound()

        hp = HallPass.objects.create(**data)
        return hp

    class Meta:
        model = HallPass
        fields = ['expiry', 'username', 'field']


class UserNotFound(APIException):
    status_code = 404
    default_detail = "User does not exist"
    default_code = "no such user"


class FieldNotFound(APIException):
    status_code = 404
    default_code = "no such field"
    default_detail = "Field does not exist"


class FieldWithEventsSerializer(serializers.ModelSerializer):
    # fieldevent_set = FieldEventSerializer(many=True)
    events = serializers.SerializerMethodField()
    boundary = serializers.SerializerMethodField()

    def get_boundary(self, obj: Field):
        try:
            return obj.bdy
        except:
            return GEOSGeometry(obj.boundary).geojson

    def get_events(self, obj: Field):
        if 'queryset' in self.context.keys():
            return FieldEventSerializer(obj.events, many=True, read_only=True).data
        return FieldEventSerializer(obj.fieldevent_set.all(), many=True, read_only=True).data

    class Meta:
        model = Field
        fields = ("id", "name", "boundary", "events")


class LayerAdjustSerializer(serializers.Serializer):
    parameter = serializers.CharField(max_length=10)
    sequence = serializers.IntegerField()
    value = serializers.FloatField()

    def update(self, instance, validated_data):
        parameter = validated_data["parameter"]
        sequence = validated_data["sequence"]
        new_val = validated_data["value"]

        instance.adjust_value(parameter, sequence, new_val)
        return instance


class SubfieldSerializer(serializers.ModelSerializer):
    features = serializers.SerializerMethodField()
    layer_fields = serializers.SerializerMethodField()
    log = serializers.SerializerMethodField()
    formulas = serializers.SerializerMethodField()
    value_keys = serializers.SerializerMethodField()
    polygons_only = serializers.SerializerMethodField()
    points_only = serializers.SerializerMethodField()
    source_file_event = serializers.SerializerMethodField()
    raster = serializers.SerializerMethodField()

    def get_raster(self, obj):
        return obj.dbf_field_names.get('raster')

    def get_features(self, obj):
        return obj.features

    def get_layer_fields(self, obj):
        return obj.dbf_field_names.get('field_names')

    def get_log(self, obj):
        return obj.log

    def get_formulas(self, obj):
        formulas = dict()
        try:
            for field in obj.dbf_field_names.get('field_names'):
                field_formulas = Formula.objects.filter(inputs__test_parameter__name=field)
                if field_formulas.count():
                    formulas[field] = FormulaSerializer(field_formulas, many=True).data
        except Exception:
            pass
        return formulas

    def get_value_keys(self, obj):
        return list(obj.dbf_field_values.keys())

    def get_polygons_only(self, obj):
        return obj.polygons_only

    def get_points_only(self, obj):
        return obj.points_only

    def get_source_file_event(self, obj):
        return obj.source_file_event.pk if obj.source_file_event else None

    class Meta:
        model = SubfieldLayer
        exclude = ['dbf_field_values', 'geoms', 'dbf_field_names']
        read_only_fields = ('field', 'source_file')
