from address.forms import AddressWidget
from address.models import AddressField
from django.conf import settings
from django.contrib.gis import admin
from django.urls import reverse
from django.utils.html import format_html
from leaflet.admin import LeafletGeoAdmin, LeafletGeoAdminMixin

from .forms import FieldRiskAdminForm
from .models import Client, Farm, Field, SubfieldLayer, VariabilityReport, RasterFile


# Register your models here.


class FarmInline(admin.TabularInline):
    model = Farm
    extra = 0


class FieldInline(LeafletGeoAdminMixin, admin.StackedInline):
    model = Field
    extra = 0
    exclude = ["boundary"]
    show_change_link = True


# class SubFieldInline(LeafletGeoAdminMixin, admin.StackedInline):
#     model = Subfield
#     extra = 0


class ClientFormAdmin(admin.ModelAdmin):
    model = Client
    list_display = ('contact', 'active', 'originator', 'is_company', 'user')
    list_editable = ('active',)
    list_select_related = ('originator', 'contact')
    search_fields = ('contact__name', 'contact__last_name',
                     'originator__organization__company__name',
                     'originator__organization__name',
                     'originator__user__person__name',
                     'originator__user__person__last_name',
                     )
    list_filter = ('active', 'originator__organization__name'
                   )
    # raw_id_fields = ('contact',)
    autocomplete_fields = ('contact', 'originator',)
    inlines = [
        FarmInline
    ]

    def user(self, obj):
        try:
            str = format_html(
                f"""<a href="{reverse('admin:contact_user_change', args=[obj.contact.user.pk])}">{obj.contact.user.username}</a>""")
            return str
        except Exception as e:
            return None

    def get_queryset(self, request, organization=None):
        # qs = super(ClientFormAdmin, self).get_queryset(request)
        qs = Client.all_objects.all()
        # filter the set for a given organization in the request
        if organization:
            qs = qs.filter(organization=organization)
        # for f in self.list_select_related:
        #     qs.select_related(f)
        qs = qs.prefetch_related('contact',
                                 'originator__user__person',
                                 'originator__organization__company')

        return qs

    def changelist_view(self, request, extra_context=None, organization=None):
        response = super(ClientFormAdmin, self).changelist_view(
            request,
            extra_context=extra_context,
        )

        # check for organization context
        # organization = extra_context.get('organization')
        if organization:
            try:
                cl = response.context_data['cl']
            except:
                cl = self.get_changelist_instance(request)
                cl.formset = None
                if not hasattr(response, 'context_data'):
                    response.context_data = {}
                response.context_data['opts'] = self.model._meta

            cl.queryset = self.get_queryset(request, organization)
            response.context_data['cl'] = cl
        return response



class FarmFormAdmin(LeafletGeoAdminMixin, admin.ModelAdmin):
    model = Farm
    list_display = ('client', 'name',)
    search_fields = ('client__contact__name', 'name',)
    list_filter = ('client__contact__name',)
    autocomplete_fields = ('client',)

    inlines = [
        FieldInline
    ]
    raw_id_fields = ('client',)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['GOOGLE_API_KEY'] = settings.GOOGLE_API_KEY
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


class FieldFormAdmin(LeafletGeoAdmin):
    model = Field
    change_form_template = "change_form_default.html"
    list_display = ('client', 'farm', 'name', 'area',)
    search_fields = ('farm__client__contact__name', 'farm__name', 'name',)
    list_filter = ('farm__client__contact__name', 'farm__name', 'name')
    formfield_overrides = {
        AddressField: {'widget': AddressWidget(attrs={"style": "width: 300px"})}
    }
    raw_id_fields = ('farm',)
    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['GOOGLE_API_KEY'] = settings.GOOGLE_API_KEY
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )


class SubfieldLayerAdmin(admin.ModelAdmin):
    model = SubfieldLayer
    date_hierarchy = 'date_created'
    list_display = ('field', 'name', 'date_created', 'date_modified')
    readonly_fields = list_display
    search_fields = ('field__name', 'name')

    def queryset(self, request):
        return super(SubfieldLayerAdmin, self).queryset(request).select_related('field')


class VariabilityReportAdmin(admin.ModelAdmin):
    model = VariabilityReport
    date_hierarchy = "generated"
    list_display = ("field", "image", "analyst", "generated")
    search_fields = ("field__name", "analyst__name")
    autocomplete_fields = ("analyst",)
    raw_id_fields = ('image',)
    form = FieldRiskAdminForm

admin.site.register(Client, ClientFormAdmin)
admin.site.register(Farm, FarmFormAdmin)
admin.site.register(Field, FieldFormAdmin)
#admin.site.register(Subfield, SubfieldAdmin)
admin.site.register(SubfieldLayer, SubfieldLayerAdmin)

admin.site.register(VariabilityReport, VariabilityReportAdmin)
admin.site.register(RasterFile)