import logging

from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.template.loader import get_template
from weasyprint import HTML

from common.admincontext import dashboard_context


def render_to_pdf(context, **kwargs):
    """ https://docs.djangoproject.com/en/dev/howto/outputting-pdf/ """

    logger = logging.getLogger('weasyprint')
    logger.addHandler(logging.FileHandler('/tmp/weasyprint.log'))

    context_data = context
    request = kwargs.pop('request')

    context_data = {**context_data, **dashboard_context(request)}
    context_data['pdf'] = True

    template = get_template("clients/variabilityreport_detail.html")
    rendered_template = template.render(context_data)
    pdf_file = HTML(string=rendered_template, base_url=request.build_absolute_uri()).write_pdf(stylesheets=[
        #CSS(settings.STATIC_ROOT + '/assets/plugins/bootstrap/bootstrap.min.css'),
        #CSS(settings.STATIC_ROOT + '/assets/css/seipkon_print.css'),
    ])

    f = ContentFile(pdf_file, name=f"{context_data['object'].field.name}_report.pdf")
    context_data["report"].file = f
    context_data["report"].save()

    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'filename=f"{f.name}"'

    # response = PDFTemplateResponse(
    #     request=request,
    #     template=template,
    #     filename="report.pdf",
    #     context=context,
    #     show_content_in_browser=True,
    #     cmd_options= {
    #         'margin-top': '50mm'
    #     }
    # )

    #response = HttpResponse(content_type='application/pdf')
    # uncomment to toggle: downloading | display (moz browser)
    # response['Content-Disposition'] = 'attachment; filename="myfilename.pdf"'
    #
    # c = canvas.Canvas(response)
    # c.drawString(100, 600, order.order_number)
    # c.showPage
    # c.save()
    return response
