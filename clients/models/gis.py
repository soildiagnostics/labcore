import json
import os
import shutil
import tempfile
import urllib
from collections import defaultdict
from urllib import parse
from zipfile import ZipFile

import geopandas as gpd
import numpy as np
import pandas as pd
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point, MultiPoint, Polygon, MultiPolygon, GeometryCollection
# from django.contrib.postgres.fields import JSONField
from django.core.files import File
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property
from shapely import wkt
from shapely.geometry import MultiPoint

from associates.models import OrganizationUser
from clients.gis.gishelper import SDXShapefile
from clients.gis.utils import get_all_file_paths
from clients.models import Field, update_soils
from clients.soilsmixin import SoilsAPIMixin
from common.lambda_utils import update_usgs
from common.models import DeleteFieldFileMixin
from products.models import TestParameterRenderer, TestParameter
from django.core.cache import cache

PRESET_EVENTS = [
    'Zones',
    'Harvest',
    'Planting',
    'As Applied',
    'Grid Sampling',
    'Imagery',
    'Shapefile Upload',
    'Sampling Locations',
    'Boundary',
    'NDVI',
]


class SubfieldLayer(SoilsAPIMixin, models.Model):
    ''' SubfieldLayers are associated with Fields
    '''
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=256)
    field = models.ForeignKey(
        Field, on_delete=models.PROTECT, related_name="layer")
    dbf_field_names = models.JSONField(default=dict)
    # polygons = models.MultiPolygonField(null=True, blank=True)
    # points = models.MultiPointField(null=True, blank=True)
    geoms = models.GeometryCollectionField(null=True, blank=True)
    dbf_field_values = models.JSONField(default=dict)
    source_file = models.CharField(max_length=512, default="", blank=True)
    editable = models.BooleanField(default=False)

    # def save(self, *args, **kwargs):
    #     ## Invalidate gis template stub cache if exists
    #     # print("Cache: ", cache.delete(f'stubs_{self.id}'))
    #     return super().save(*args, **kwargs)

    @cached_property
    def points(self):
        if self.geoms is None:
            return list()
        return [g for g in list(self.geoms) if g.geom_type == "Point"]

    @cached_property
    def polygons(self):
        if self.geoms is None:
            return list()
        return [g for g in list(self.geoms) if g.geom_type == "Polygon"]

    @cached_property
    def area_vector(self):
        geos = [g.transform(2791, clone=True) for g in list(self.geoms)]
        areas = [g.area * 0.000247105381 for g in geos]
        return areas

    @cached_property
    def avg_area(self):
        return sum(self.area_vector) / self.features

    def area_parameter_vector(self, parameter):
        area_vec = self.area_vector
        par_vec = self.dbf_field_values.get(parameter, [1] * self.features)
        return zip(area_vec, par_vec)

    def area_weighted_sum(self, parameter):
        return sum([a * p for a, p in self.area_parameter_vector(parameter)])

    def area_weighted_mean(self, parameter):
        return self.area_weighted_sum(parameter) / sum(self.area_vector)

    @property
    def source_file_event(self):
        try:
            assert self.source_file != "" and self.source_file != None
            return self.field.fieldevent_set.filter(file__icontains=self.source_file).latest('pk')
        except:
            return None

    def __str__(self):
        return "{} ({})".format(self.field.name, self.name)

    def get_absolute_url(self):
        if self.name == "Sample Points":
            return reverse("update_sample_map", kwargs={'pk': self.id})
        return reverse("subfield_details", kwargs={"pk": self.id})

    def get_values(self, parameter, filter=None):
        """
        :param: Filter should be a GEOSGeometry Polygon
        """
        try:
            assert parameter in self.dbf_field_names['field_names']
        except AssertionError:
            return None
        try:
            if filter:
                geoms = self.points if self.points_only else self.polygons
                zipped = zip(geoms, self.dbf_field_values[parameter])
                zipped_fltr = [val for (g, val) in zipped if filter.contains(g)]
                return zipped_fltr
            return self.dbf_field_values[parameter]
        except KeyError:
            return None

    def make_geodataframe(self, parameters="__all__"):
        """
        Make a geodataframe with a list of parameters
        """
        # assert self.polygons_only or self.points_only, "Must be either polygons or points"
        # feats = self.points if self.points_only else [p for p in self.polygons]
        feats = list(self.geoms)
        feats = [i.wkt for i in feats]
        df = pd.DataFrame({"geometry": feats})
        try:
            flds = self.dbf_field_names['field_names']
            ## Also throw an if parameter is Serial and Serial in not in keys
            if parameters[0] == 'Serial' and 'Serial' not in flds:
                raise KeyError
        except KeyError:
            ## This is a new layer
            self.dbf_field_names['field_names'] = ['Serial']
            self.dbf_field_values['Serial'] = [i + 1 for i in range(self.features)]
            self.save()
            flds = self.dbf_field_names['field_names']

        vals = self.dbf_field_values.keys()
        flds = [f for f in flds if f in vals]
        if not parameters == "__all__":
            assert all([p in flds for p in parameters]), f"Requested parameters {parameters} are not in the layer"
            flds = [f for f in flds if f in parameters]
        for f in flds:
            df[f] = self.dbf_field_values[f]

        # if self.points_only:
        #     gsr = [shpPoint(p) for p in df.geometry]
        # else:
        #     gsr = [shpPolygon(p.coords[0]) for p in df.geometry]
        df['geometry'] = df['geometry'].apply(wkt.loads)

        gdf = gpd.GeoDataFrame(df, geometry='geometry')
        return gdf

    def append_log(self, string, save=True):
        if "updates" not in self.dbf_field_values.keys():
            self.dbf_field_values["updates"] = list()
        now = timezone.now()
        message = f"At {now}, {string}"
        self.dbf_field_values['updates'].append(message)
        if save:
            return self.save()

    @property
    def log(self):
        return self.dbf_field_values.get('updates')

    def bring_in(self, finelayer, parameter, aggfun="mean"):
        """
        Use geometry from the current layer to aggregate over features from another layer
        For instance: average yield data to regional boundaries present in the object features.

        The current layer must be polygons_only

        :param finelayer: A layer containing geometries at a much finer granularity than the current layer
        :param parameter: A parameter - should be available in dbf_field_values of the other layer
        :param aggfun: A function name to aggregate the shapefile over, e.g. "mean"
        """
        parameter = urllib.parse.unquote(parameter)
        assert self.points_only == False, "Cannot use this function on layers with points. Aggregation layer must constitute region boundaries"
        big_gdf = finelayer.make_geodataframe(parameters=[parameter])
        mygdf = self.make_geodataframe(parameters=["Serial"])
        big_gdf_join = gpd.sjoin(big_gdf, mygdf, how="inner", op="intersects")
        big_gdf_dissolve = big_gdf_join.dissolve(by="Serial", aggfunc=aggfun)
        self.dbf_field_names['field_names'].append(parameter)
        self.dbf_field_values[parameter] = big_gdf_dissolve[parameter].tolist()
        return self.append_log(f"Brought in layer {repr(finelayer)} parameter {parameter}")

    def adjust_value(self, parameter, sequence, new_value):
        """
        Manually set a value to a new value on a parameter.
        """
        assert parameter in self.dbf_field_names['field_names'], f"Parameter {parameter} not found"
        assert self.editable, "This layer is not editable"
        old_val = self.dbf_field_values[parameter][sequence]
        self.dbf_field_values[parameter][sequence] = new_value
        self.append_log(f"Parameter {parameter} at pos {sequence} from {old_val} to {new_value}",
                        save=False)
        self.save()
        ## Remove any cached rasters
        self.rasterfile_set.filter(parameter=parameter).delete()

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)

    def geojson_features(self, parameter=None, clip=True):
        """
        Wrapper for geojson_points and geojson_polygons
        :param parameter: The parameter to be rendered
        :param clip: Boolean, whether to normalize and clip the readings to +/-3 standard deviations
        :return: Geojson
        """
        if parameter == "serial":
            parameter_values = [str(i) for i in range(self.features)]
        else:
            parameter_values = self.dbf_field_values[parameter]

        is_numeric = False
        try:
            parameter_values = np.array(parameter_values)
            parameter_values = parameter_values.astype(float)
            is_numeric = True
        except Exception:
            # categorical values
            pass

        try:
            par = TestParameter.objects.get_parameter_by_string(parameter)
            legend = par.renderer.legend
            if parameter == "Zone_ID":
                ## Only send as many as there are zone_ids
                legend = par.renderer.legend[:self.features]
        except Exception:
            legend = None

        gj = {
            "type": "FeatureCollection",
            "features": list(),
            "soils": False,
            "few_points": False,
            'legend': legend,
            "clip": clip,
            "numeric": is_numeric,
            "editable": self.editable
        }

        if clip and is_numeric:
            parameter_values = np.array(parameter_values)
            mean = np.mean(parameter_values)
            sd = np.std(parameter_values)
            parameter_values = np.clip(parameter_values, mean - 2 * sd, mean + 2 * sd)

        if is_numeric:
            gj["min"] = np.nanmin(parameter_values),
            gj["max"] = np.nanmax(parameter_values),

        if self.polygons_only and len(self.polygons) == len(list(self.geoms)):
            gj['subfield_type'] = "polygons_only"
            return self._geojson_polygons(parameter=parameter,
                                          parameter_values=parameter_values,
                                          gj=gj)
        elif self.points_only:
            gj['subfield_type'] = "points_only"
            return self._geojson_points(parameter=parameter,
                                        parameter_values=parameter_values,
                                        gj=gj)
        else:
            ## What to do when both polygons and points are present?
            gj['subfield_type'] = "geometry_collection"
            return self._geojson_geomcollection(parameter=parameter,
                                                parameter_values=parameter_values,
                                                gj=gj)

    def _geojson_geomcollection(self, parameter=None, parameter_values=None, gj=None):
        """
        Returns a featurecollection geojson of all features
        :param parameter:
        :param gj: Geojson object
        :return: geojson
        """
        gj['bbox'] = [[x for x in z] for z in self.geoms.convex_hull.coords[0]]
        for i in range(len(list(self.geoms))):
            geom = self.geoms[i]
            pt = self._create_feature(geom, parameter, parameter_values[i], i)
            gj["features"].append(pt)
        return gj

    def _geojson_polygons(self, parameter=None, parameter_values=None, gj=None):
        """
        Returns a geojson of polygon features
        :param parameter:
        :param gj: Geojson object
        :return:
        """
        gj['bbox'] = [[x for x in z] for z in self.geoms.convex_hull.coords[0]]

        if self.name == "Soil Survey Data":
            gj['soils'] = True
            for i in range(len(self.polygons)):
                areasymbol = self.dbf_field_values['areasymbol'][i]
                mukey = self.dbf_field_values['mukey'][i]
                musym = self.dbf_field_values['musym'][i]
                gj["features"].append(self._create_feature(self.polygons[i],
                                                           parameter,
                                                           parameter_values[i],
                                                           f"{areasymbol}, {musym}, MapUnit {mukey}, <br>{parameter}:{parameter_values[i]}")
                                      )
            return gj


        else:
            ## these are polygons of something else, like drone images
            ## so we convert them back to points
            if len(self.polygons) > 1000:
                moddiv = 40
                decimate = True
                gj['subfield_type'] = "polygons_to_points"
            else:
                moddiv = 1
                gj['few_points'] = True
                decimate = False
            for i in range(len(self.polygons)):
                ## For very dense polygons, we will treat them like points
                if i % moddiv == 0:
                    geom = self.polygons[i].centroid if decimate else self.polygons[i]
                    try:
                        pt = self._create_feature(geom, parameter, parameter_values[i], None if decimate else i)
                    except IndexError:
                        pt = self._create_feature(geom, parameter, np.mean(parameter_values), None if decimate else i)
                    gj["features"].append(pt)

            return gj

    def _geojson_points(self, parameter=None, parameter_values=None, gj=None):
        """
        :param parameter: Feature to display
        :param gj: Geojson
        :return: Altered geojson
        """
        gj["bbox"] = self.geoms.convex_hull.coords[0]
        if len(self.points) > 1000:
            moddiv = 40
            decimate = True
        else:
            moddiv = 1
            gj['few_points'] = True
            decimate = False
        for i in range(len(self.points)):
            if i % moddiv == 0:
                try:
                    pt = self._create_feature(self.points[i], parameter, parameter_values[i], None if decimate else i)
                except IndexError:
                    pt = self._create_feature(self.points[i], parameter, np.mean(parameter_values),
                                              None if decimate else i)
                gj["features"].append(pt)

        return gj

    def _create_feature(self, geom, parameter, value, content=None):
        if str(value) == 'nan' and isinstance(value, np.float64):
            value = "NaN"
        if not isinstance(value, str):
            try:
                par = TestParameter.objects.get_parameter_by_string(parameter)
                sigdig = par.significant_digits
                if sigdig == 0:
                    value = int(value)
                else:
                    value = round(value, sigdig)
            except Exception as e:
                value = round(value, 1)
        feature = {
            'type': 'Feature',
            'geometry': json.loads(geom.geojson),
            'properties': {
                parameter: value,
                'popupContent': f"{parameter}: {value}",
                'tooltipContent': value
            },
        }
        if content is not None:
            feature['properties']['popupContent'] = f"{feature['properties']['popupContent']}<br>#{content}"
        return feature

    @cached_property
    def features(self):
        if self.geoms:
            return self.geoms.num_geom
        return None
        # if self.inferred_boundary:
        #     return 1
        # try:
        #     return self.geoms.num_geom
        # except TypeError:
        #     return len(self.polygons) if self.polygons else 0 + len(self.points) if self.points else 0

    @cached_property
    def points_only(self):
        return self.geoms and self.geoms.dims == 0

    @cached_property
    def polygons_only(self):
        return self.geoms and self.geoms.dims == 2

    @cached_property
    def inferred_boundary(self):
        """True if the geoms is a single multipolygon"""
        return self.geoms is not None and len(list(self.geoms)) == 1 and \
               (isinstance(self.geoms[0], Polygon) or isinstance(self.geoms[0], MultiPolygon))

    def simplify_geometry(self):
        """
        Takes a set of Multipolygons and polygons and simplifies them to return one multipolygon
        """
        polys = list()
        for geom in list(self.geoms):
            if isinstance(geom, Polygon):
                polys.append(geom)
            else:
                simple_poly = geom.convex_hull
                polys.append(simple_poly)
        mp = MultiPolygon(polys).unary_union
        if isinstance(mp, MultiPolygon):
            return mp
        return MultiPolygon(mp)

    def add_points(self, pts):
        """
        :param pts: list of tuples with (long, lat)
        :return: None
        """
        pointlist = list()

        for lng, lat in pts:
            pointlist.append(Point(lng, lat))

        self.points = MultiPoint(pointlist)
        self.save()

    def add_feature(self, name, values):
        """
        :param name: name of feature
        :param values: list of points to add
        :return: None
        """
        self.dbf_field_names['field_names'].append(name)
        self.dbf_field_values[name] = values
        self.save()

    def create_shapefile_from_layers(self, parameters="__all__", clip=False, zip=True, delete_tmp=True):
        """
        Turns the entire layer into a downloadable shapefile and stores the shapefile as the source file.
        :return: filepath of zipfile

        If you set delete_tmp to False, you must clean up after yourself.
        """
        gdf = self.make_geodataframe(parameters=parameters)
        if clip:
            if parameters == "__all__":
                parameters = self.dbf_field_names['field_names']
            for parameter in parameters:
                mean = np.mean(gdf[parameter])
                sd = np.std(gdf[parameter])
                gdf[parameter] = np.clip(gdf[parameter], mean - 3 * sd, mean + 3 * sd)
        tempdir = tempfile.mkdtemp(prefix="gis")
        zipdir = tempfile.mkdtemp(prefix="zip")
        try:
            gdf.crs = "epsg:4326"  ## Are we sure this is good enough?
            gdf.to_file(f"{tempdir}/{self.name}.shp")

            filepaths = get_all_file_paths(tempdir)
            if zip:
                with ZipFile(f"{zipdir}/{self.name}.zip", 'w') as f:
                    for file in filepaths:
                        f.write(file, os.path.basename(file))

                return (zipdir, f"{self.name}.zip")
            return (tempdir, f"{self.name}.shp")
        except Exception as e:
            raise
        finally:
            if delete_tmp:
                try:
                    # print("cleaning up " + self.tempdir)
                    shutil.rmtree(tempdir)
                except OSError as e:
                    print(f"{self.__class__} Error {e.filename}-{e.strerror}")

    def make_raster(self, file, parameter, save_tif=True):
        """
        Just sets up the SDXShapefile handlers to generate the raster images.
        :param parameter:
        :return:
        """
        # check if required data is available in the subfield model.
        assert parameter in self.dbf_field_values.keys(), "Cannot create raster without first creating the geolayer"
        colormap = TestParameterRenderer.objects.get_colorfile_for_string(parameter)
        try:
            self.field.boundary.extent
        except AttributeError:
            raise

        clip = False
        if self.features > 1000:
            clip = True

        # gdf = self.make_geodataframe([parameter], clip=clip)
        tfile, shp = self.create_shapefile_from_layers([parameter], clip=clip, zip=False, delete_tmp=False)

        # gj = self.geojson_features(parameter=parameter, clip=clip)
        # handler = SDXShapefile()
        try:

            pngfilename = f"{self.field.pk}/{parameter}.png"
            tiffilename = f"{self.field.pk}/{parameter}.tif"
            handler2 = SDXShapefile(f"{tfile}/{shp}", tempdir=tfile)

            try:
                tiff, png = handler2.make_raster(parameter, color=colormap, extent=self.field.boundary.extent,
                                                 clip=clip, sendtif=True)
                gj = json.loads(self.field.geojson_boundary())['features'][0]['geometry']
                epsg = int(json.loads(self.field.geojson_boundary())['crs']['properties']['name'][5:])
                png = handler2.clip_to_json(png, json.dumps(gj), epsg)

                with open(png, 'rb') as fh:
                    rf = RasterFile(
                        layer=self,
                        parameter=parameter,
                    )
                    rf.file.save(pngfilename, File(fh), save=True)
                if save_tif:
                    tiff = handler2.clip_to_json(tiff, json.dumps(gj), epsg, make_png=False)
                    with open(tiff, 'rb') as fh:
                        rf = RasterFile(
                            layer=self,
                            parameter=parameter,
                        )
                        rf.file.save(tiffilename, File(fh), save=True)

            finally:
                handler2.cleanup()

            self.refresh_from_db()
            png = RasterFile.objects.filter(parameter=parse.unquote(parameter),
                                            layer=self,
                                            file__icontains=".png").latest('pk')
            return png
        except Exception:
            raise

    def merge_polygons_of_similar_values(self):
        """
        Use as a post processing technique to reduce the number of features
        """
        vals = self.get_values('DN')
        geom_vals = zip(vals, self.geoms)
        mpdict = defaultdict(list)
        for k, v in geom_vals:
            mpdict[k].append(v)

        gc_dict = {k: MultiPolygon(v) for (k, v) in mpdict.items()}
        ordered_vals = list(set(vals))
        ordered_vals.sort()

        self.geoms = GeometryCollection([gc_dict[v] for v in ordered_vals])
        self.add_feature('Zone_ID', list(range(1, len(ordered_vals) + 1)))
        self.append_log(f"{len(vals)} condensed to {len(self.geoms)}", save=True)

    class Meta:
        ordering = ['-date_created', 'name']

    def save(self, *args, **kwargs):
        try:
            cache.delete(f"stubs_{self.pk}")
        except Exception as e:
            print(e)
        return super().save(*args, **kwargs)


def update_soil_layer(field, usgs=True):
    sf, created = SubfieldLayer.objects.get_or_create(field=field,
                                                      name="Soil Survey Data")
    if field.boundary:
        field.validate_boundary(save=True)
        sf.make_layer()
        sf.save()
    rasters = RasterFile.objects.filter(layer=sf)
    for r in rasters:
        r.file.delete()
        r.delete()
    if not field.boundary:
        sf.delete()
    if usgs:
        if settings.SOILDX_LAMBDA_PROCESSING_ENABLED and settings.SOILDX_PUBLIC_10M_ELEVATION_ENABLED and field.pk:
            update_usgs(field)


@receiver(update_soils)
def update_callback(sender, field, **kwargs):
    update_soil_layer(field)


########### End of SubfieldLayer ###############

def get_raster_filename(instance, filename):
    return f"rasters/{instance.layer.pk}/{filename}"


class RasterFile(DeleteFieldFileMixin, models.Model):
    created = models.DateTimeField(auto_now_add=True)
    layer = models.ForeignKey(SubfieldLayer, on_delete=models.CASCADE)
    parameter = models.CharField(max_length=100)
    file = models.ImageField(upload_to=get_raster_filename)

    def __str__(self):
        return self.file.name


class VariabilityReport(models.Model):
    generated = models.DateTimeField(auto_now_add=True)
    analyst = models.ForeignKey(OrganizationUser, on_delete=models.PROTECT)
    image = models.ForeignKey(RasterFile, on_delete=models.PROTECT)
    data = models.JSONField(default=dict)
    description = models.TextField(default="This text is to be customized by the analyst.")
    file = models.FileField(upload_to="variability_reports/", blank=True, null=True)
    annotations = models.MultiPolygonField(blank=True, null=True)

    @property
    def field(self):
        return self.image.layer.field

    @property
    def jsondata(self):
        return json.dumps(self.data)

    def __str__(self):
        return "Variability report for " + self.image.layer.field.name
