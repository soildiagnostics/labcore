import json
import urllib.request
import uuid
from datetime import timedelta

from address.models import AddressField
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.gis.geos import Polygon
from django.core.serializers import serialize
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.utils.translation import gettext as _
from tagging.fields import TagField

from associates.models import OrganizationUser
from common.lambda_utils import LambdaEventHandler, update_usgs
from common.models import ModelDiffMixin
from contact.models import Contact, User
import django.dispatch
update_soils = django.dispatch.Signal()
from shapely import wkt
from shapely.geometry import MultiPolygon as shpMultiPolygon


# domain = Site.objects.get_current().domain


class ClientManager(models.Manager):

    def __init__(self, *args, **kwargs):
        self.active_only = kwargs.pop('active_only', True)
        super(ClientManager, self).__init__(*args, **kwargs)

    def get_or_create_default_client(self, orguser):

        try:
            contact = Contact.objects.get(
                name="Anonymous Entity",
                notes=f"Client created by {orguser}. Do not delete or modify.",
            )

        except Contact.DoesNotExist:
            contact = Contact.objects.create(
                name="Anonymous Entity",
                middle_name=str(uuid.uuid4())[:8],
                notes=f"Client created by {orguser}. Do not delete or modify.",
            )
        return Client.all_objects.get_or_create(contact=contact,
                                                originator=orguser,
                                                active=False)

    def get_queryset(self):
        qs = super(ClientManager, self).get_queryset()
        if self.active_only:
            return qs.filter(active=True)
        return qs


class Client(models.Model):
    """
    Client / Farmer contact information. This client belongs to
    an originator, who in turn is an organization user.
    """
    contact = models.OneToOneField(
        Contact, on_delete=models.PROTECT,
        help_text=_("Could be Company or Person"))
    originator = models.ForeignKey(OrganizationUser, on_delete=models.PROTECT,
                                   help_text=_("Person originating the client"),
                                   related_name="clients")
    notes = models.TextField(null=True, blank=True,
                             help_text=_("Additional information"))
    active = models.BooleanField(default=True, help_text=_("Mark as inactive - don't delete"))
    tags = TagField()
    objects = ClientManager()
    all_objects = ClientManager(active_only=False)

    def get_absolute_url(self):
        return reverse('client_detail', kwargs={'pk': self.id})

    def __str__(self):
        return '{} > {}'.format(str(self.originator), str(self.contact))

    @property
    def name(self):
        return self.contact

    @property
    def company(self):
        return self.contact.name if self.contact.is_company else self.contact.company

    @property
    def is_user(self):
        return self.contact.user is not None

    @property
    def is_company(self):
        return self.contact.is_company

    @cached_property
    def area(self):
        return sum([f.area for f in self.farms.all().prefetch_related('fields')])

    class Meta:
        ordering = ['contact']


class FarmManager(models.Manager):

    def get_or_create_default_farm(self, orguser):
        c, created = Client.objects.get_or_create_default_client(orguser)
        return Farm.objects.get_or_create(client=c,
                                          name="Anonymous farm",
                                          notes=f"Anonymous farm created by {orguser}. Do not delete or modify")


class Farm(models.Model):
    """
    Many farms to a farmer
    """
    name = models.CharField(max_length=100)
    client = models.ForeignKey(
        Client, on_delete=models.PROTECT, related_name="farms")
    notes = models.TextField(null=True, blank=True,
                             help_text=_("Additional information"))
    objects = FarmManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    def get_absolute_url(self):
        return reverse('farm_detail', kwargs={'pk': self.id})

    @cached_property
    def area(self):
        return sum([f.area for f in self.fields.filter(boundary__isnull=False)])


class FieldManager(models.Manager):

    def get_new_field_on_anonymous_client(self, orguser):
        f, created = Farm.objects.get_or_create_default_farm(orguser)
        return Field.objects.create(farm=f,
                                    notes=f"Anonymous field created by {orguser}. Do not modify or delete")

    def filter_queryset_with(self, qs, extent):
        """
        qs is a Field queryset.
        extent is a pair of coordinates representing the NE and SW corners of a rectangle,
        e.g. "40.46889024168825,-87.53219604492189,39.86969567045658,-88.9281463623047"
        """
        coords = extent.split(",")
        nelat = float(coords[0])
        nelng = float(coords[1])
        swlat = float(coords[2])
        swlng = float(coords[3])

        aoi = Polygon.from_bbox((swlng, swlat, nelng, nelat))

        return qs.filter(boundary__bboverlaps=aoi)


class Field(ModelDiffMixin, models.Model):
    """
    abstraction for field level
    """
    name = models.CharField(max_length=100, help_text=_("Name of Field or Facility"))
    farm = models.ForeignKey(
        Farm, on_delete=models.PROTECT, related_name="fields")
    boundary = models.MultiPolygonField(null=True, blank=True, geography=True)
    address = AddressField(blank=True, null=True, on_delete=models.CASCADE)
    projection = models.IntegerField(default=4326, help_text=_(
        "Geographic projection"))
    state = models.CharField(max_length=50, blank=True, default='')
    county = models.CharField(max_length=50, blank=True, default='')
    section = models.CharField(max_length=50, blank=True, default='')
    twpnum = models.CharField(max_length=50, blank=True,
                              default='', help_text="Township Number", verbose_name="Township Number")
    township = models.CharField(max_length=50, blank=True, default='',
                                help_text="Township Name", verbose_name="Township Name")
    intersection = models.CharField(max_length=100, blank=True, default='',
                                    verbose_name="Nearest Intersection")
    approx_area = models.CharField(max_length=32, blank=True, default='', help_text="Approx Area")
    range = models.CharField(max_length=50, blank=True, default='')
    plat_name = models.CharField(max_length=100, blank=True, default='')
    plat_file = models.FileField(upload_to='platfiles/', blank=True, null=True)
    notes = models.TextField(null=True, blank=True,
                             help_text=_("Additional information"))
    objects = FieldManager()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    ###
    ## ModelDiffMixin capture
    capture_diff_for_fields = [boundary]

    @property
    def area(self):
        try:
            proj = self.projection if self.projection != 4326 else 2791
            geo = self.boundary.transform(proj, clone=True)
            return round(geo.area * 0.000247105381, 1)
        except Exception as e:
            try:
                return int(self.approx_area)
            except Exception as e:
                return 0

    def get_address(self):
        if self.is_located:
            lng, lat = self.boundary.centroid.coords
            base = f"https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lng}&key={settings.GOOGLE_API_KEY}"
            with urllib.request.urlopen(base) as response:
                result = json.loads(response.read())
                locations = [r['formatted_address'] for r in result['results']]
                return locations

    # def save(self, *args, **kwargs):
    #     self.get_address()
    #     super().save(*args, **kwargs)

    @property
    def client(self):
        return str(self.farm.client)

    class Meta:
        ordering = ['name']
        verbose_name = "Field or Facility"
        verbose_name_plural = "Fields or Facilties"

    def __str__(self):
        return ('%s > %s > %s') % (self.farm.client, self.farm, self.name)

    def get_absolute_url(self):
        return reverse('field_detail', kwargs={'pk': self.id})

    @property
    def is_located(self):
        return self.boundary is not None

    @property
    def google_path(self):
        try:
            poly = self.boundary.unary_union
            polystrings = list()
            for p in poly:
                coords = p.coords[0]
                polystrings.append("|".join([f"{lat},{lng}" for lng, lat in coords]))
            return "&path=".join(polystrings)
        except Exception:
            try:
                coords = self.boundary.convex_hull.coords[0]
                return "|".join([f"{lat},{lng}" for lng, lat in coords])
            except Exception:
                return None

    @property
    def geom(self):
        return self.boundary

    @property
    def valid_hallpasses(self):
        return self.hallpass.filter(expiry__gte=now())

    def latitude(self):
        if self.boundary:
            return round(self.boundary.centroid[1], 6)

    def longitude(self):
        if self.boundary:
            return round(self.boundary.centroid[0], 6)

    def validate_boundary(self, save=False):
        if self.boundary.valid:
            return True
        bdy = shpMultiPolygon(wkt.loads(self.boundary.wkt))
        clean = bdy.buffer(0)
        if isinstance(clean, shpMultiPolygon):
            self.boundary = clean.wkt
        else:
            cleanm = shpMultiPolygon([clean])
            self.boundary = cleanm.wkt
        if save:
            self.save()
            self.refresh_from_db()
        return self.boundary.valid

    def geojson_boundary(self):
        if self.boundary:
            assert self.validate_boundary(save=True)
            return serialize('geojson', [self], geometry_field="boundary", fields="name")

    def sample_points_url(self):
        try:
            fe = self.fieldevent_set.filter(event_type__name="Sampling Locations").latest('created')
            return reverse('fieldevent_file', args=[fe.pk])
        except Exception:
            return None

    def save(self, *args, **kwargs):
        update_layer = False

        if self.has_changed and 'boundary' in self.changed_fields:
            if settings.SOILDX_SSURGO_SOILS_ENABLED:
                update_layer = True


        super().save(*args, **kwargs)
        if update_layer:
            update_soils.send(sender=self.__class__, field=self)




def default_expiry():
    return now() + timedelta(days=7)


class HallPassManager(models.Manager):
    def active_passes(self, user):
        return HallPass.objects.filter(user=user,
                                       expiry__gte=now())


class HallPass(models.Model):
    """
    A class that allows certain users to have the ability to access Field objects that they
    otherwise will not be able to.

    The hall pass should only be granted by the owner of the field, and should be
    grantable to another client or orguser.

    The access rights are determined by the role_privileges module.
    """
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="given_hallpass")
    field = models.ForeignKey(Field, related_name="hallpass", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="hallpass", on_delete=models.CASCADE)
    expiry = models.DateTimeField(help_text=_("When should this hall pass become inactive"),
                                  default=default_expiry)
    objects = HallPassManager()

    @property
    def expired(self):
        return now() > self.expiry
