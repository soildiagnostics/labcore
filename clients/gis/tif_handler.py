import os
import shutil
import tempfile
from subprocess import call
from zipfile import ZipFile

from django.conf import settings
from django.core.files import File
from osgeo import gdal
from osgeo import osr
from clients.gis.gishelper import SDXShapefile
from clients.models import SubfieldLayer, RasterFile, get_all_file_paths
from fieldcalendar.models import FieldEvent

gdal.UseExceptions()


class TiffFile:
    """
    Handler for uploaded tif files. Wrapper around gishelper.py
    """

    def __init__(self, tiffile, field, tmpdir=None):
        """
        Set up the Zipped File handler
        :param zipfile: Containing shapefile and associated files
        :param field: clients.models.Field
        """
        self.field = field
        self.file = tiffile
        self.tempdir = tmpdir
        if not self.tempdir:
            self.tempdir = tempfile.mkdtemp(prefix="tif")
        self.folder = None
        self.tifffile_handlers = []
        self.tif_is_valid = False
        self.extracted = False
        self.processing = False
        self.data = None
        self.layer = None
        super().__init__()

    def copy_file(self):
        filebase = os.path.basename(self.file.name)
        with open(f"{self.tempdir}/{filebase}", "wb+") as dest:
            for chunk in self.file.chunks():
                dest.write(chunk)
        return f"{self.tempdir}/{filebase}"

    def valid_file(self):
        """
        Determine if the file is a valid geotiff.
        """
        if not self.tif_is_valid:
            try:
                self.processing = True
                self.data = gdal.Open(f"{self.file}")
                self.tif_is_valid = len(self.data.GetMetadata().keys()) > 0
                return self.tif_is_valid
            except Exception as e:
                print(e, self.file, self.tempdir)
                return False
            finally:
                self.processing = False
        return True

    def clip(self, src):
        """
        Single call to SDXHandler.clip_to_json that will reproject and clip to field boundary.
        Be sure not to convert to png
        """

        gj = self.field.boundary.geojson
        cliphandler = SDXShapefile()
        self.tifffile_handlers.append(cliphandler)
        clipped = cliphandler.clip_to_json(src, gj, make_png=False)
        print("Clipped to", clipped)
        return clipped

    def reproject(self, clip=True, save=False):
        """
        This function will reproject and scale the layer to 0-255. If you pass clip=True
        it will directly use the SDXHandler's clip_to_json which will also allow you to warp and
        clip in the same operation. Be sure to not convert to png.
        """
        if self.valid_file():
            # Define target SRS
            dst_srs = osr.SpatialReference()
            dst_srs.ImportFromEPSG(4326)
            dst_wkt = dst_srs.ExportToWkt()
            resampling = gdal.GRA_NearestNeighbour
            outfile = f"{self.tempdir}/reprojected.tif"
            # Call AutoCreateWarpedVRT() to fetch default values for target raster dimensions and geotransform
            tmp_ds = gdal.AutoCreateWarpedVRT(self.data,
                                              None,  # src_wkt : left to default value --> will use the one from source
                                              dst_wkt,
                                              resampling)

            dst_ds = gdal.GetDriverByName('GTiff').CreateCopy(outfile, tmp_ds)
            dst_ds = None
            print("Reprojected to", outfile)
            result = outfile
            if clip:
                result = self.clip(outfile)
            if save and self.layer:
                self.add_rasterfile(self.layer, "Reprojected", result, type="tif")
            return result
        return None

    def scale(self, src):
        scaled = f"{self.tempdir}/scaled.tif"
        cmd = f"gdal_translate -ot Byte -scale {src} {scaled}"
        call(cmd, shell=True)
        print("Scaled to", scaled)
        return scaled

    def colorize(self, src, save=False):
        """
        Delegate to gishandler.py
        """
        scaled = self.scale(src)
        gishandler = SDXShapefile()
        self.tifffile_handlers.append(gishandler)
        colored = gishandler.colorize(scaled, band=1, suffix='', colormap=None)
        if save and self.layer:
            self.add_rasterfile(self.layer, "Reprojected", colored, type="png")
        return colored

    def process_tiff(self):
        clipped_and_scaled = self.reproject(clip=True, save=True)
        colored = self.colorize(clipped_and_scaled, save=True)
        return colored

    def cleanup(self):
        """
        Remove temp directory
        :return: None
        """
        try:
            for h in self.tifffile_handlers:
                h.cleanup()
            shutil.rmtree(self.tempdir)
        except OSError as e:
            print(f"{self.__class__} Error {e.filename}-{e.strerror}")

    def create_layers_and_rasterfiles(self, field, layer, parameter):
        created = False
        try:
            subfieldlayer, created = SubfieldLayer.objects.get_or_create(field=field, name=layer)
        except SubfieldLayer.MultipleObjectsReturned:
            subfieldlayer = SubfieldLayer.objects.filter(field=field, name=layer).latest('pk')
            SubfieldLayer.objects.filter(field=field, name=layer).exclude(id=subfieldlayer.id).delete()

        fnames = subfieldlayer.dbf_field_names.get('field_names', list())
        if isinstance(parameter, bytes):
            parameter = parameter.decode()
        if parameter not in fnames:
            fnames.append(parameter)
            subfieldlayer.dbf_field_names['field_names'] = fnames
        subfieldlayer.dbf_field_names['raster'] = True
        subfieldlayer.save()
        self.layer = subfieldlayer

        try:
            ftype = os.path.splitext(self.file)[1][1:]
        except:
            ftype = "tif"
        raster, rcreated = self.add_rasterfile(subfieldlayer, parameter, self.file, type=ftype)
        return (self.layer, raster, rcreated)

    def add_rasterfile(self, layer, parameter, file, type="tif"):
        filename = f"{parameter}.{type}"
        created = False
        try:
            raster = RasterFile.objects.get(layer=layer, parameter=parameter, file__contains=filename)
        except RasterFile.DoesNotExist:
            with open(file, "rb") as f:
                raster = RasterFile.objects.create(
                    parameter=parameter,
                    layer=layer,
                    file=File(f, name=filename),
                )
                created = True
        except RasterFile.MultipleObjectsReturned:
            raster = RasterFile.objects.filter(layer=layer, parameter=parameter).latest('pk')
            RasterFile.objects.filter(layer=layer, parameter=parameter).exclude(id=raster.id).delete()
            with open(file, "rb") as f:
                raster.file.save(File(f, name=filename))

        if parameter not in layer.dbf_field_names['field_names']:
            layer.dbf_field_names['field_names'].append(parameter)

        print("Raster saved", raster, raster.parameter)
        return raster, created

    def contour(self, interval, parameter):
        filebase = self.copy_file()
        outfile = f"{self.tempdir}/contour.shp"
        cmd = f"""gdal_contour -p -q -a {parameter} {filebase} {outfile} -i {interval}"""
        call(cmd, shell=True)
        filepaths = get_all_file_paths(self.tempdir)
        with ZipFile(f"{self.tempdir}/contour.zip", 'w') as f:
            for file in filepaths:
                f.write(file, os.path.basename(file))
        return self.tempdir, 'contour.shp'

    def polygonize(self):
        filebase = self.copy_file()
        outfile = f"{self.tempdir}/zones.shp"
        cmd = f"gdal_translate -q -ot Byte -a_nodata 0 -b 1 -scale 0 1 0 255 {filebase} {self.tempdir}/scaled.tif"
        call(cmd, shell=True)

        ## Clip to GeoJSON
        clipper = SDXShapefile()
        self.tifffile_handlers.append(clipper)
        gj = self.field.boundary.geojson
        clipped = clipper.clip_to_json(f"{self.tempdir}/scaled.tif", gj, make_png=False)

        cmd = f"""gdal_polygonize.py -q -b 1 {clipped} {outfile} """
        call(cmd, shell=True)
        filepaths = get_all_file_paths(self.tempdir)
        with ZipFile(f"{self.tempdir}/zones.zip", 'w') as f:
            for file in filepaths:
                f.write(file, os.path.basename(file))
        return self.tempdir, 'zones.shp'