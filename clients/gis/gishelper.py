import glob
import json
import math
import os
import re
import shutil
import tempfile
import uuid
from zipfile import ZipFile

import fiona
import numpy as np
import pandas as pd
from skimage import io
import statsmodels.formula.api as sm
from fiona.crs import from_epsg
from matplotlib.cm import get_cmap
from osgeo import gdal, gdal_array, osr
from osgeo import ogr
from shapely.geometry import Point, mapping
from skimage.filters import gaussian, sobel
from sklearn import cluster
from subprocess import call
import warnings
import scipy.stats


class SDXShapefile:
    file = None
    xPixel = 0.00005
    yPixel = 0.00005
    folder = None
    tempdir = None
    projection = None

    def __init__(self, file=None, tempdir=None):
        """
        Set up Shapefile Object
        :param file: File name (String)
        """
        self.tempdir = tempdir if tempdir else tempfile.mkdtemp(prefix="gis")

        if file:
            self.file = file
            driver = ogr.GetDriverByName('ESRI Shapefile')
            self.dataSource = driver.Open(self.file, 0)
            try:
                ref = self.layer.GetSpatialRef()
                ref.AutoIdentifyEPSG()
                self.projection = ref.GetAuthorityCode(None)
            except Exception as e:
                raise
            if self.dataSource is None:
                return None
            self.folder = "{}/{}".format(self.tempdir, self.layer.GetDescription())
            os.mkdir(self.folder)
        else:
            self.folder = "{}/nofile".format(self.tempdir)
            os.mkdir(self.folder)

        gdal.UseExceptions()
        gdal.AllRegister()
        return super().__init__()

    @property
    def layer(self):
        """
        :return: Layer name
        """
        return self.dataSource.GetLayer()

    @property
    def featureCount(self):
        """
        :return: Number of features
        """
        return self.layer.GetFeatureCount()

    def geoms(self):
        """
        :return: Iterator giving geoms from the layer
        """
        for i in range(self.layer.GetFeatureCount()):
            yield self.layer[i]

    @property
    def fields(self):
        """
        :return: Provides a list of parameter names in the shapefile
        """
        layerdef = self.layer.GetLayerDefn()
        return [layerdef.GetFieldDefn(i).GetName() for i in range(layerdef.GetFieldCount())]

    def get_parameter(self, parameter):
        """
        Gets values for a given parameter in a shapefile
        :param parameter: Name of parameter
        :return: List of values
        """

        for f in self.geoms():
            if not parameter == "dummy":
                yield f.GetField(parameter[:10])
            else:
                yield 1

    def aggregate_over_area(self, aoi, parameter, func):
        """
        :param aoi: An OGRGeometry string
        :param parameter: A string
        :param func: A function that aggregates over a vector
        :return: The result of the function
        """
        fltr_geom = ogr.CreateGeometryFromWkt(aoi)
        self.layer.SetSpatialFilter(fltr_geom)
        vals = self.get_parameter(parameter)
        result = func(vals)
        self.layer.SetSpatialFilter(None)
        return result

    def _interpolate(self, vector, outmin=0, outmax=255):
        """
        Interpolate a given vector to between outmin and outmax values;
        default 8-bit grayscale. Not useful for non-integer interpolations

        outval = round((val-vmin)/(vmax-vmin) * (outmax-outmin) + outmin)
        :param vector:
        :param outmin:
        :param outmax:
        :return:
        """
        vector = list(vector)
        vmin = min(vector)
        vmax = max(vector)
        return [round((val - vmin) / (vmax - vmin) * (outmax - outmin) + outmin) for val in vector]

    def make_raster(self, parameter, color=None, slope=False, extent=None, interpolate=False, clip=False,
                    sendtif=False):
        """
        Create a raster file
        :param parameter: Name of parameter
        :param color: Color scheme, default None to colorize the raster w.
            This is provided by the TestParameterRenderer model
        :param slope: Bool, default False, to compute slope
        :param interpolate: Bool, default false, to interpolate to 8-bit grayscale
        :param extent: 4-tuple, default None, format (lngmin, lngmax, latmin, latmax)
        :param sendtif: Sends a tuple of the tif and png names when true, otherwise just sends the png name
        :return: filename for raster
        """
        parameter_fix = re.sub("/", "", parameter)
        raster = f"{self.folder}/{parameter_fix}.tif"
        if not color and not slope:
            if os.path.exists(raster):
                return raster
        if not color and slope:
            if os.path.exists(raster + "_slp.tif"):
                return raster + "_slp.tif"
        try:
            if extent:
                lngmin, latmin, lngmax, latmax = extent
                ## Note the tuple order is not the same for the output of .extent in geodjango, vs gdals GetExtent
            else:
                lngmin, lngmax, latmin, latmax = self.layer.GetExtent()
            gridy = int((latmax - latmin) / self.xPixel)
            gridx = int((lngmax - lngmin) / self.yPixel)
            values = self.get_parameter(parameter)
            if interpolate:
                mapped_values = list(self._interpolate(values))
            else:
                mapped_values = list(values)

            if clip or len(mapped_values) > 1000:
                sd = np.std(np.array(mapped_values))
                mean = np.mean(mapped_values)
                mapped_values = np.clip(mapped_values, mean - 2 * sd, mean + 2 * sd)
            geoms = self.geoms()
            if not self.layer.GetGeomType() == 3:  ## check to see if this is a polygon layer or point layer
                points = ((f.GetGeometryRef().GetX(), f.GetGeometryRef().GetY()) for f in geoms)
                outvalues = zip(points, mapped_values)
                vrt = """<OGRVRTDataSource>
        <OGRVRTLayer name="dem">
            <SrcDataSource>{}/dem.csv</SrcDataSource>
            <GeometryType>wkbPoint</GeometryType>
            <GeometryField encoding="PointFromColumns" x="Longitude" y="Latitude" z="Value"/>
        </OGRVRTLayer>
    </OGRVRTDataSource>""".format(self.folder)
                with open(self.folder + "/dem.vrt", "w") as f:
                    f.write(vrt)

                with open(self.folder + "/dem.csv", "w") as f:
                    f.write("Longitude, Latitude, Value\n")
                    for (x, y), z in outvalues:
                        f.write("{},{},{}\n".format(x, y, z))

                r1 = self.xPixel * 2
                r2 = self.yPixel * 2
                # alg = f"average:radius1={r1}:radius2={r2}"
                alg = f"invdistnn:radius1={r1}:radius2={r2}:max_points=20:smoothing=.5:power=1.0:nodata=-9999"

                if self.featureCount < 1000:
                    ## This is a soil test map usually
                    area = gridx * gridy / self.featureCount
                    factor = math.sqrt(area)
                    r1 = r1 * factor
                    r2 = r2 * factor
                    alg = f"invdistnn:radius1={r1}:radius2={r2}:max_points=12:smoothing=0:power=2.0:nodata=-9999"
                    # alg = "nearest"
                    # alg = f"linear:radius={r1}"

                dst_srs = osr.SpatialReference()
                res = dst_srs.ImportFromProj4(
                    '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
                )

                gdal.UseExceptions()
                gridoptions = gdal.GridOptions(
                    format="GTiff",
                    outputType=gdal.GDT_Float32,
                    width=gridx,
                    height=gridy,
                    outputBounds=[lngmin, latmax, lngmax, latmin],
                    outputSRS=dst_srs,
                    algorithm=alg,
                    layers="dem",
                    options=['-q']
                )

                gdal.Grid(
                    destName=raster,
                    srcDS=f"{self.folder}/dem.vrt",
                    options=gridoptions
                )
                # call(f'''gdal_grid -l dem -a invdistnn:power=2.0:smothing=1.0:radius=2.0:max_points=12:min_points=0:nodata=-999.0 -ot Float32 -of GTiff "{self.folder}/dem.vrt" "{self.folder}/{parameter_fix}.tif"''')
            else:
                # This is a polygon geometry coming from the SoilDx layer, or it could be from
                # the drone image.
                ## Note of May 2020 - Column names are limited to 10 characters, even if layer names are not.
                src = self.file
                lyr = self.layer.GetName()
                # determine if projection is required:
                if self.projection and self.projection != '4326':
                    src = f'{self.folder}/reprojected.shp'
                    cmd = f'''ogr2ogr -f "ESRI Shapefile" -t_srs EPSG:4326 {src} {self.file}'''
                    call(cmd, shell=True)
                    parameter_fix = parameter_fix[:10]  ## names are normalized!
                    lyr = 'reprojected'

                # First call with the non-normalized parameter name
                cmd = f'''gdal_rasterize -q -te {lngmin} {latmin} {lngmax} {latmax} -tr {self.xPixel} {self.yPixel} -a "{parameter[:10]}" -l "{lyr}" "{src}" "{raster}" -a_nodata -9999'''
                res = call(cmd, shell=True)
                try:
                    assert res == 0, "Didn't work with unfixed parameter name"
                except AssertionError:
                    # Try with parameter_fix[:9]
                    cmd = f'''gdal_rasterize -q -te {lngmin} {latmin} {lngmax} {latmax} -tr {self.xPixel} {self.yPixel} -a "{parameter_fix[:10]}" -l "{lyr}" "{src}" "{raster}" -a_nodata -9999'''
                    res = call(cmd, shell=True)
                    try:
                        assert res == 0, "Command wasn't successful with parameter_fix[:9]"
                    except AssertionError:
                        ## Last attempt
                        cmd = f'''gdal_rasterize -q -te {lngmin} {latmin} {lngmax} {latmax} -tr {self.xPixel} {self.yPixel} -a "{parameter_fix[:9]}" -l "{lyr}" "{src}" "{raster}" -a_nodata -9999'''
                        res = call(cmd, shell=True)
                        assert res == 0, "Failed rendering altogether"
        except Exception:
            raise

        tiff = raster
        if slope:
            raster = self.slope(raster)
        if color:
            raster = self.colorize(raster, colormap=color)

        if sendtif:
            return (tiff, raster)
        return raster

    def cmap_translator(self, cmap, percent=True):
        """
        Takes a matplotlib cmap and converts it into a colormap string used by the colorize function
        :param: cmap - a Matplotlib cmap
        returns: dictionary with string and hex color vector.
        """
        # stringlist = ["nv 0 0 0 0"]
        stringlist = list()
        hexes = list()
        ncols = cmap.N
        for i in range(0, ncols):
            r, g, b, a = cmap.__call__(i, bytes=True)
            if percent:
                pc = int(100 * (i + 1) / ncols)
                stringlist.append(f"{pc}% {r} {g} {b} {a}")
            else:
                stringlist.append(f"{i} {r} {g} {b} {a}")
            hexes.append(f"#{format(r, '02x')}{format(g, '02x')}{format(b, '02x')}{format(a, '02x')}")
        return {
            "colormap": "\n".join(stringlist),
            "colors": hexes
        }

    def colorize(self, src, band=1, suffix='', colormap=None):
        """
        Colorize a given DEM file. The default color scheme is Yellow to Green ("YG").
        If the source filename contains '_src', then it will use the scheme with a different
        set of cutoff points (goes from 0 to 10% slopes).
        Additional color schemes to be developed.
        :param src: Source file (DEM) to colorize.
        :param band: Band for the source file
        :param suffix: a suffix added to the output filename
        :param colormap: a string representing the colormap to be used
        :return: Filename for the colored png file.
        """
        # print(f'Coloring "{src}"')
        try:
            # The color map obtained from ColorBrewer
            if not colormap:
                if "_slp" in src:
                    with open(self.folder + "/color.txt", "w") as f:
                        f.write("""nv 0 0 0 0
    0% 255 255 229 255 
    1% 247 252 185 255
    2% 217 240 163 255
    3% 173 221 142 255
    5% 120 198 121 255
    10% 65 171 93 255
    50% 35 132 67 255
    75% 0 104 55 255
    100% 0 69 41 255""")
                elif "cluster" in src:
                    with open(self.folder + "/color.txt", "w") as f:
                        f.write("""nv 254 237 222 255
    0% 254 237 222 255 
    16% 253 208 162 255
    33% 253 174 107 255
    50% 253 141 60 255
    66% 230 85 13 255
    83% 166 54 3 255
    100% 140 45 4 255""")

                else:
                    with open(self.folder + "/color.txt", "w") as f:
                        f.write("""nv 0 0 0 0
    0% 0 0 0 0                     
    5.5% 255 255 229 255 
    16.66% 247 252 185 255
    27.77% 217 240 163 255
    38.88% 173 221 142 255
    50% 120 198 121 255
    61.11% 65 171 93 255
    72.22% 35 132 67 255
    83.33% 0 104 55 255
    94.44% 0 69 41 255""")
            else:
                with open(self.folder + "/color.txt", "w") as f:
                    f.write(colormap)

            colorize = f'''gdaldem color-relief -q -of PNG -b {band} "{src}" "{self.folder}/color.txt" "{src}_col{suffix}.png" -nearest_color_entry -alpha'''
            res = call(colorize, shell=True)
            assert res == 0, "Colorize failed"

        except Exception:
            raise

        return f"{src}_col{suffix}.png"

    def slope(self, src):
        """
        Calculate slope from DEM file
        :param src: Source filename for DEM
        :return: Slope raster (appends _src to the filename, useful in rescaling the
            color scheme)
        """
        # print(f"Slope calculation on {src}")
        try:
            ## Calculate the slope of the DEM
            gradient = f'''gdaldem slope "{src}" "{src}_slp.tif" -p -of GTiff -q -scale 370400'''
            call(gradient, shell=True)
        except Exception:
            raise

        return f"{src}_slp.tif"

    def cleanup(self):
        """
        Delete temporary files
        :return: None
        """
        try:
            # print("cleaning up " + self.tempdir)
            shutil.rmtree(self.tempdir)
        except OSError as e:
            print(f"{self.__class__} Error {e.filename}-{e.strerror}")

    def regress(self, yraster, yslope, xraster, xslope):
        """
        Run a single parameter regression of yield as a function of other stuff.
        Provides a Pearson correlation between two parameters.
        :param yraster: Y parameter GTiff raster
        :param yslope: is the Y parameter a slope?
        :param xraster: X parameter GTiff raster
        :param xslope: Run regression against slope(X)
        :return: Result summary
        """
        ## combine rasters to register them on the same lat longs first.
        combined = self.combine_rasters([yraster, xraster])
        ypar = "Response" + ("_slope" if yslope else '')
        xpar = "Variable" + ("_slope" if xslope else '')

        call(f'''gdal_translate -of XYZ -q -b 1 "{combined}" "{yraster}.csv"''', shell=True)
        ycsv = pd.read_csv(f"{yraster}.csv", sep=" ", names=["Longitude", "Latitude", ypar])

        call(f'''gdal_translate -of XYZ -q -b 2 "{combined}" "{xraster}.csv"''', shell=True)
        xcsv = pd.read_csv(f"{xraster}.csv", sep=" ", names=["Longitude", "Latitude", xpar])

        dat = pd.merge(ycsv, xcsv, on=["Longitude", "Latitude"])
        if xslope:
            try:
                dat = dat.loc[dat[xpar] < 20]
                dat = dat.loc[dat[xpar] >= 0]
                df = dat.loc[dat[ypar] > 0]
            except Exception as e:
                df = dat
        else:
            try:
                df = dat.loc[dat[ypar] > 0]
            except Exception as e:
                df = dat

        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            result = sm.ols(formula=f"{ypar} ~ {xpar}", data=df, missing="drop").fit()
            return result, w

    def multiple_regression(self, yraster, xrasters):
        """
        Run a single parameter regression of yield as a function of other stuff.
        Provides a Pearson correlation between two parameters.
        :param yraster: Y parameter GTiff raster
        :param xrasters: List of X parameter GTiff rasters
        :return: Result summary
        """
        ## combine rasters to register them on the same lat longs first.
        num_influencers = len(xrasters)
        xrasters.insert(0, yraster)
        combined = self.combine_rasters(xrasters)
        base = os.path.basename(yraster)
        ypar = os.path.splitext(base)[0]

        call(f'''gdal_translate -of XYZ -q -b 1 "{combined}" "{yraster}.csv"''', shell=True)
        ycsv = pd.read_csv(f"{yraster}.csv", sep=" ", names=["Longitude", "Latitude", ypar])

        xpars = list()
        for i in range(num_influencers):
            call(f'''gdal_translate -of XYZ -q -b {i+2} "{combined}" "{xrasters[i+1]}.csv"''', shell=True)
            base = os.path.basename(xrasters[i+1])
            xpar = os.path.splitext(base)[0]
            xpars.append(xpar)
            xcsv = pd.read_csv(f"{xrasters[i+1]}.csv", sep=" ", names=["Longitude", "Latitude", xpar])
            ycsv  = pd.merge(ycsv, xcsv, on=["Longitude", "Latitude"])

        try:
            df = ycsv.loc[ycsv["Response"] > 0]
        except Exception as e:
            df = ycsv

        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            result = sm.ols(formula=f"{ypar} ~ {'+'.join(xpars)}", data=df, missing="drop").fit()
            return result, w

    ########## Shapefile Producers.

    def clip_to_json(self, pngfile, geojson, epsg=4326, src_epsg=4326, make_png=True):
        try:
            jsonshape = self.get_shapefile_from_geojson(geojson, epsg)
            outfile = pngfile[:-4] + "_clip.tif"
            # outfile = pngfile[:-4] + "_clip.png"
            cmd = f'''gdalwarp -q -s_srs EPSG:{src_epsg} -t_srs EPSG:{epsg} -srcnodata -9999 -dstnodata -9999 -crop_to_cutline -cutline "{jsonshape}" "{pngfile}" "{outfile}"'''
            call(cmd, shell=True)
            if make_png:
                pngout = pngfile[:-4] + '_clip.png'
                cmd = f'''gdal_translate -q -of PNG "{outfile}" "{pngout}"'''
                call(cmd, shell=True)
                outfile = pngout
            assert os.path.isfile(outfile), "File doesn't exist - possibly bad cutline"
            return outfile
        except AssertionError as e:
            print(e)
            return outfile

    def get_shapefile_from_geojson(self, geojson, epsg):
        """
        This is used by rasters to clip the image to the size specified by the field boundary
        :param geojson:
        :param epsg:
        :return:
        """
        schema = {'geometry': 'MultiPolygon', 'properties': {'field': 'str:50'}}
        with fiona.open(self.folder + '/polygon.shp', 'w',
                        crs=from_epsg(epsg), driver='ESRI Shapefile', schema=schema) as layer:
            layer.write({'geometry': json.loads(geojson), 'properties': {'field': ''}})

        return self.folder + '/polygon.shp'

    def make_shapefile_for_recommendation(self, nrec, epsg):
        """
        This creates a shapefile out of the recommendation
        :param nrec:
        :param epsg:
        :return:
        """
        schema = {'geometry': 'Point',
                  'properties': {'Yield Goal': 'float',
                                 'Seeding Rate': 'float',
                                 'Nitrogen': 'float',
                                 'Lime': 'float',
                                 'Slope Loss': 'float',
                                 'Responsiveness': 'float',
                                 'Supply': 'float'}}

        points = [f[0] for f in nrec['yield_goal']]
        l = len(points)
        yld = [f[1] for f in nrec['yield_goal']]
        assert len(yld) == l, "Length not equal"
        seed = [f[1] for f in nrec['seeding_rate']]
        assert len(seed) == l, "Length not equal"
        lime = [f[1] for f in nrec['lime']]
        assert len(lime) == l, "Length not equal"
        nitrogen = [f[1] for f in nrec['nitrogen']]
        assert len(nitrogen) == l, "Length not equal"
        slope = [f[1] for f in nrec['slope_loss']]
        resp = [f[1] for f in nrec['responsiveness']]
        supply = [f[1] for f in nrec['supply']]

        props = zip(yld, seed, nitrogen, lime, slope, resp, supply)
        rows = zip(points, props)

        with fiona.open(self.folder + '/rec.shp', 'w',
                        crs=from_epsg(epsg), driver='ESRI Shapefile', schema=schema) as layer:
            for (lng, lat), (y, s, n, l, slp, r, g) in rows:
                layer.write({'geometry': mapping(Point(lng, lat)),
                             'properties': {
                                 'Yield Goal': y,
                                 'Seeding Rate': s,
                                 'Nitrogen': n,
                                 'Lime': l,
                                 'Slope Loss': slp,
                                 'Responsiveness': r,
                                 'Supply': g
                             }})

        return self.folder + '/rec.shp'

    def make_shapefile_from_geodataframe(self, gdf, parname):
        """
        Wrapper around the gdf.to_file function.
        """
        filename = self.folder + f"/{parname}.shp"
        gdf.to_file(filename)
        return filename

    def make_shapefile_for_feature_data(self, feature, geojson, parameter, epsg=4326):
        """
        Takes a soil survey data geojson and produces a shapefile from it
        :param ftr: "Point", "Polygon" or "MultiPolygon"
        :param geojson:
        :param parameter:
        :return:
        """
        schema = {'geometry': feature, 'properties': {parameter: 'float'}}
        parname = parameter.replace("/", "")
        with fiona.open(self.folder + f"/{parname}.shp", 'w',
                        crs=from_epsg(epsg), driver='ESRI Shapefile', schema=schema) as layer:
            for ftr in geojson.get('features'):
                layer.write({'geometry': ftr.get('geometry'),
                             'properties': {parameter: ftr.get('properties').get(parameter)}
                             })
        return self.folder + f"/{parname}.shp"

    def make_shapefile_for_samplepoints(self, geojson, filename, epsg=4326):
        """
        This is called by the Sample Points creator
        :param geojson:
        :param epsg:
        :return:
        """
        schema = {'geometry': 'Point', 'properties': {'serial': 'str:6'}}
        with fiona.open(self.folder + f'/{filename}.shp', 'w',
                        crs=from_epsg(epsg), driver='ESRI Shapefile', schema=schema) as layer:
            for gj in geojson['features']:
                for key in [k for k in gj['properties'].keys() if k != "serial"]:
                    gj['properties'].pop(key)
                layer.write(gj)
        return self.folder

    def add_attribute(self, attribute, data):
        """
        Add an attribute to the existing shapefile
        :param attribute: String - name of the attribute (layer) to be added
        :param data: Vector of float data
        :return: name of the new shapefile - it should create a new output shapefile and return
        """

        ptype = 'float' if type(data[0]) is float else "str"

        outfile = self.file[:-4] + "_out.shp"
        with fiona.open(self.file, 'r') as source:
            sink_schema = source.schema.copy()
            sink_schema['properties'][attribute] = ptype

            with fiona.open(outfile, 'w',
                            crs=source.crs,
                            driver=source.driver,
                            schema=sink_schema) as sink:
                for i in range(len(source)):
                    try:
                        source[i]['properties'].update(
                            attribute=data[i]
                        )
                        sink.write(source[i])
                    except Exception:
                        raise

        return outfile

    def combine_rasters(self, rasters):
        """
        Combine multiple DEM rasters into a single file. Each raster will be put in a separate file
        :param rasters: List of rasters
        :return: Multiband raster file
        """
        rasters = [f'''"{r}"''' for r in rasters]
        inputs = " ".join(rasters)
        outfile = f"{self.folder}/merged{str(uuid.uuid4())}.tif"
        command = f'''gdal_merge.py -separate -q -ps {self.xPixel} {self.yPixel} -o "{outfile}" {inputs}'''
        call(command, shell=True)
        return outfile

    def cluster_params(self, rasters, nclusters=8, method="equaldivs"):
        """
        Cluster a list of parameters. Wrapper to self.cluster
        :param params: List of raster files
        :param nclusters: number of clusters
        :param method: "equaldivs" - the default, which divides k-means in equal divisions.
                        "equalareas" - produces uniform-sized clusters
        :return: clustered png file
        """
        multiraster = self.combine_rasters(rasters)
        return self.cluster(multiraster, nclusters, method=method)

    def polish(self, data):
        """
        Scales the array to z-scores and clips the ends to -3 / +3
        :param array:
        :return: polished array
        """
        data = np.where(data == -9999, np.nan, data)
        data = np.where(data == -999, np.nan, data)
        data = np.where(data == 0, np.nan, data) ## This is a potential issue
        mean = np.nanmean(data)
        data = np.where(np.isnan(data), mean, data)
        data = gaussian(data, sigma=1, mode='constant', cval=mean, preserve_range=True)
        res = scipy.stats.zscore(data, axis=None)
        mean = np.nanmean(data)
        std = np.nanstd(data)
        return (mean, std, res)

    def rescale(self, centers, stats):
        for i in range(centers.shape[1]):
            # iterate over columns
            mean, std = (stats[i][0], stats[i][1])
            centers[:, i] = std * centers[:, i] + mean
        return centers

    def cluster(self, multiraster_file, nclusters, method="equaldivs"):
        """
        Runs K-means clustering on a multiraster file.
        :param multiraster_file: File generated by self.combine_rasters
        :param nclusters: Number of clusters to compute
        :return: png for clustered file showing variously colored clusters
        """

        stats = list()
        gdal.UseExceptions()
        img_ds = gdal.Open(multiraster_file, gdal.GA_ReadOnly)
        img_depth = img_ds.RasterCount
        img = np.zeros((img_ds.RasterYSize, img_ds.RasterXSize, img_depth),
                       gdal_array.GDALTypeCodeToNumericTypeCode(img_ds.GetRasterBand(1).DataType))

        for b in range(img.shape[2]):
            mean, std, img[:, :, b] = self.polish(img_ds.GetRasterBand(b + 1).ReadAsArray())
            stats.append([mean, std])
        # m, s, polished_rows = self.polish(np.indices((img_ds.RasterYSize, img_ds.RasterXSize))[0].astype(float))
        # stats.append([m, s])
        # m, s, polished_cols = self.polish(np.indices((img_ds.RasterYSize, img_ds.RasterXSize))[1].astype(float))
        # stats.append([m, s])
        #
        # img[:, :, img_depth-2] = polished_rows
        # img[:, :, img_depth-1] = polished_cols

        ## Scale the data
        # img = gaussian(img, sigma=3, multichannel=True)
        # k_means = cluster.KMeans(n_clusters=nclusters)
        k_means = cluster.MiniBatchKMeans(n_clusters=nclusters)
        # k_means = cluster.MeanShift(bandwidth=2)
        new_shape = (img.shape[0] * img.shape[1], img.shape[2])
        X = img[:, :, :10].reshape(new_shape)  # 10 is some suitable large number.
        k_means.fit(X)
        # if method == "equaldivs":
        #     ## We will apply a smoothing functon
        #     # Not yet working

        X_cluster = k_means.labels_
        centers = k_means.cluster_centers_
        X_cluster = X_cluster.reshape(img[:, :, 0].shape)

        # We are going to replace the labels with the values of the first dimension (yield)
        idx = np.argsort(centers[:, 0])  # sort by first term in yield list
        lut = np.zeros_like(idx)
        lut[idx] = np.arange(nclusters)
        X_cluster = lut[X_cluster]
        centers = centers[idx]

        centers = self.rescale(centers, stats)
        counts = np.unique(X_cluster, return_counts=True)

        X_cluster = X_cluster / nclusters

        geoTransform = img_ds.GetGeoTransform()
        ulx, xres, xskew, uly, yskew, yres = geoTransform
        lrx = ulx + (img_ds.RasterXSize * xres)
        lry = uly + (img_ds.RasterYSize * yres)
        extent = [[uly, ulx], [lry, lrx]]

        outfile = f"{self.folder}/clusters.tif"

        output_raster = gdal.GetDriverByName("GTiff").Create(outfile,
                                                             img_ds.RasterXSize,
                                                             img_ds.RasterYSize,
                                                             1, gdal.GDT_Float32)
        output_raster.SetGeoTransform(geoTransform)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)
        output_raster.SetProjection(srs.ExportToWkt())  # Exports the coordinate system
        # to the file
        output_raster.GetRasterBand(1).WriteArray(X_cluster)  # Writes my array to the raster
        output_raster.GetRasterBand(1).ComputeStatistics(0)
        output_raster.FlushCache()
        output_raster = None
        ## Scale
        cmd = f"gdal_translate -q -ot Byte -b 1 -scale 0 1 10 255  {outfile} {self.folder}/cl_bytes.tif"
        call(cmd, shell=True)
        ## Sieve
        minpx = int(img.shape[0] * img.shape[1] / 1000)
        cmd = f"gdal_sieve.py -st {minpx} -q -8 {self.folder}/cl_bytes.tif {self.folder}/cl_sievd_bytes.tif"
        call(cmd, shell=True)

        ## Rescale
        cmd = f"gdal_translate -q -ot Float32 -b 1 -scale 0 255 0 1 {self.folder}/cl_sievd_bytes.tif {outfile}"
        call(cmd, shell=True)

        # Reread:
        img_polished = io.imread(outfile)
        edge = sobel(img_polished)
        complexity = np.sum(edge) / np.sum(img_polished)
        arr = np.random.choice(nclusters, (img_ds.RasterXSize, img_ds.RasterYSize))
        arr = arr / nclusters
        arr_edge = sobel(arr)
        asymptotic_complexity = np.sum(arr_edge) / np.sum(arr)

        ## Build colormap
        cmap = get_cmap("terrain", lut=nclusters).reversed()
        cmap_ = self.cmap_translator(cmap)

        return ({"centers": centers.tolist(),
                 "counts": counts[1].tolist(),
                 "extent": extent,
                 'complexity': complexity / asymptotic_complexity,
                 "tiff": outfile,
                 "image": self.colorize(outfile, colormap=cmap_['colormap']),
                 # "colors": ['#ffffff00', '#feedde', '#fdd0a2', '#fdae6b', '#fd8d3c', '#f16913', '#d94801', '#8c2d04']})
                 "colors": cmap_['colors']})

    def polygonize(self, raster, extent):

        ## translate and project the png file to the extent
        llx, lly, urx, ury = extent

        cmd = f"""gdal_translate -a_nodata 0 -q -of GTiff gdal -a_ullr {llx} {ury} {urx} {lly} {raster} {self.folder}/zones.tif"""
        call(cmd, shell=True)

        gdal.UseExceptions()
        src_ds = gdal.Open(f"""{self.folder}/zones.tif""")
        if src_ds is None:
            print('Unable to open %s' % raster)
            return None

        try:
            srcband = src_ds.GetRasterBand(2)
        except RuntimeError as e:
            print("RuntimeError", e)
            return None

        dst_layername = "zones"
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource(self.folder + "/zones.shp")
        dst_layer = dst_ds.CreateLayer(dst_layername, srs=None)

        gdal.Polygonize(srcband, None, dst_layer, -1, [], callback=None)

        return self.folder

    def generate_nrec(self, multiraster, shapes, factors, boundary, epsg):
        ## Open the raster image:
        gdal.UseExceptions()
        img_ds = gdal.Open(multiraster, gdal.GA_ReadOnly)

        ## Create a zeros image for two channels more than the multiraster.
        ## The additional two channels will be used for lime and nitrogen recommendations

        img = np.zeros((img_ds.RasterYSize, img_ds.RasterXSize, img_ds.RasterCount + 4),
                       gdal_array.GDALTypeCodeToNumericTypeCode(img_ds.GetRasterBand(1).DataType))

        nrec_factors = dict()

        ## Determine if yield or PI is used.
        if "nccpi" in shapes[0]:
            nrec_factors['yield_goal'] = "Productivity Index"
            nrec_factors['yield_goal_factor'] = factors[0] if factors[0] else 250
        else:
            nrec_factors['yield_goal'] = f"Yield Map: {shapes[0]}"
            nrec_factors['yield_goal_factor'] = factors[0] if factors[0] else 1

        current_band = 0
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray() * nrec_factors[
            'yield_goal_factor']

        ## Determine if variable rate seeding is to be used.
        current_band = 1
        if shapes[1] == None and factors[1] == None:
            nrec_factors['Plant Population'] = "Generate variable rate"
            nrec_factors['seed_rate'] = 6.02  ## bushels per 1000 plants
            img[:, :, current_band] = np.round(img[:, :, 0] / nrec_factors[
                'seed_rate'])
        if shapes[1] == None and factors[1] != None:
            nrec_factors['Plant Population'] = f"Use fixed rate of {factors[1]} thousand plants"
            nrec_factors['seed_rate'] = factors[1]
            img[:, :, current_band] = np.round(img_ds.GetRasterBand(current_band + 1).ReadAsArray() + nrec_factors[
                'seed_rate'])
        if shapes[1] != None:
            nrec_factors['Plant Population'] = f"Seeding rate map: {shapes[1]}"
            img[:, :, current_band] = np.round(img_ds.GetRasterBand(current_band + 1).ReadAsArray())

        ## Determine C:N ratio factor
        nrec_factors['C:N factor'] = factors[2] if factors[2] else 0.5
        current_band = 2
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray() * nrec_factors[
            'C:N factor']

        current_band = 3
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray()

        nrec_factors['Available water storage factor'] = factors[3] if factors[3] else 1
        current_band = 4
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray() * nrec_factors[
            'Available water storage factor']

        nrec_factors['pH correction'] = factors[4] if factors[4] else 30
        current_band = 5

        lime = lambda ph: max(6.8 - ph, 0) * 0.2 + 1.2
        lime_vec = np.vectorize(lime)

        img[:, :, current_band] = lime_vec(img_ds.GetRasterBand(current_band + 1).ReadAsArray())

        nrec_factors['slope loss'] = factors[5] if factors[5] else 5
        current_band = 6
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray() * nrec_factors[
            'slope loss']

        nrec_factors['fixed offset'] = factors[6] if factors[6] else 50
        current_band = 7
        img[:, :, current_band] = img_ds.GetRasterBand(current_band + 1).ReadAsArray() + nrec_factors[
            'fixed offset']

        phcorrection = lambda ph: max(6.8 - ph, 0) * nrec_factors['pH correction']
        phcorr_vec = np.vectorize(phcorrection)

        current_band = 8
        img[:, :, current_band] = phcorr_vec(img_ds.GetRasterBand(5 + 1).ReadAsArray())

        ## Calculate demand

        demand = img[:, :, 0] * 1.1

        ## Calculate supply

        """
        Call:
        lm(formula = CheckYield ~ I(PlantPopulation/2.5) + rootznaws:ISNT - 
            1, data = dat)
        
        Residuals:
            Min      1Q  Median      3Q     Max 
        -88.012 -21.306   2.815  24.003 116.035 
        
        Coefficients:
                                Estimate Std. Error t value Pr(>|t|)    
        I(PlantPopulation/2.5) 0.0031741  0.0004325   7.338 5.84e-11 ***
        rootznaws:ISNT         0.0010103  0.0001783   5.667 1.40e-07 ***
        ---
        Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
        
        Residual standard error: 36.71 on 100 degrees of freedom
        Multiple R-squared:  0.9411,	Adjusted R-squared:  0.9399 
        F-statistic: 798.8 on 2 and 100 DF,  p-value: < 2.2e-16
        
        """

        supply = 1.1 * (0.0031741 * img[:, :, 1] + 0.0010103 * img[:, :, 3] * img[:, :, 4])

        ## calculate losses

        losses = img[:, :, 8] + img[:, :, 6]

        img[:, :, 9] = (demand - supply + losses) // 5 * 5

        ## Calculate response probability

        """
        Call:
        glm(formula = Responsive ~ I(orgC/ISNT) + ISNT:rootznaws - 1, 
            family = "binomial", data = dat)
        
        Deviance Residuals: 
            Min       1Q   Median       3Q      Max  
        -2.1864  -0.5087   0.2380   0.5599   2.1763  
        
        Coefficients:
                         Estimate Std. Error z value Pr(>|z|)    
        I(orgC/ISNT)    8.723e+01  1.746e+01   4.995 5.90e-07 ***
        ISNT:rootznaws -8.483e-05  1.813e-05  -4.680 2.87e-06 ***
        ---
        Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
        
        (Dispersion parameter for binomial family taken to be 1)
        
            Null deviance: 141.402  on 102  degrees of freedom
        Residual deviance:  76.223  on 100  degrees of freedom
        AIC: 80.223
        
        Number of Fisher Scoring iterations: 6
        """

        odds = np.exp(img[:, :, 2] / img[:, :, 3] * 87.23 - 8.483e-05 * img[:, :, 3] * img[:, :, 4])
        img[:, :, 10] = odds / (1 + odds)
        img[:, :, 11] = supply

        ## Store recommendation image

        geoTransform = img_ds.GetGeoTransform()
        ulx, xres, xskew, uly, yskew, yres = geoTransform
        lrx = ulx + (img_ds.RasterXSize * xres)
        lry = uly + (img_ds.RasterYSize * yres)
        extent = [[uly, ulx], [lry, lrx]]

        outfile = f"{self.folder}/recommendation.tif"

        output_raster = gdal.GetDriverByName("GTiff").Create(outfile,
                                                             img_ds.RasterXSize,
                                                             img_ds.RasterYSize,
                                                             img_ds.RasterCount + 4, gdal.GDT_Float32)
        output_raster.SetGeoTransform(geoTransform)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)
        output_raster.SetProjection(srs.ExportToWkt())  # Exports the coordinate system
        # to the file
        for i in range(img_ds.RasterCount + 4):
            output_raster.GetRasterBand(i + 1).WriteArray(img[:, :, i])  # Writes my array to the raster
            output_raster.GetRasterBand(i + 1).ComputeStatistics(0)
            output_raster.GetRasterBand(i + 1).FlushCache()
        output_raster.FlushCache()
        output_raster = None

        nrec_factors['yield_goal'], nrec_factors['yield_goal_img'] = self.raster_to_points(outfile, 1, 0, boundary,
                                                                                           epsg, 8)
        nrec_factors['seeding_rate'], nrec_factors['seeding_rate_img'] = self.raster_to_points(outfile, 2, 1, boundary,
                                                                                               epsg, 8)
        nrec_factors['nitrogen'], nrec_factors['nitrogen_img'] = self.raster_to_points(outfile, 10, 0, boundary, epsg,
                                                                                       8)
        nrec_factors['lime'], nrec_factors['lime_img'] = self.raster_to_points(outfile, 6, 1, boundary, epsg, 8)
        nrec_factors['slope_loss'], nrec_factors['slope_loss_img'] = self.raster_to_points(outfile, 7, 0, boundary,
                                                                                           epsg, 8)
        nrec_factors['responsiveness'], nrec_factors['responsiveness_img'] = self.raster_to_points(outfile, 11, 2,
                                                                                                   boundary, epsg, 8)
        nrec_factors['supply'], nrec_factors['supply_img'] = self.raster_to_points(outfile, 12, 0, boundary, epsg, 8)
        self.make_shapefile_for_recommendation(nrec_factors, epsg)

        recfiles = glob.glob(f"{self.folder}/rec.*")

        with ZipFile(f"{self.folder}/recommendations.zip", 'w') as f:
            for file in recfiles:
                f.write(file, os.path.basename(file))

        nrec_factors['shapefile'] = f"{self.folder}/recommendations.zip"

        return nrec_factors

    def raster_to_points(self, raster, band, significant_digits, boundary, epsg, maskband=None, colormap=None):
        """
        :param raster: Single band raster
        :param significant_digits: Number of digits to round to
        :return: list of points and values at the point
        """
        jsonshape = self.get_shapefile_from_geojson(boundary, epsg)
        clipfile = raster[:-4] + f"{band}_clip"
        cmd = f'''gdalwarp -q -dstnodata 0 -cutline "{jsonshape}" -crop_to_cutline "{raster}" "{clipfile}"'''
        call(cmd, shell=True)

        colfile = self.colorize(clipfile, color="YG", band=band, suffix=f"{band}", colormap=colormap)

        outfile = clipfile[:-4] + ".csv"
        mask = f"-mask {maskband}" if maskband else ""
        cmd = f"""gdal_translate -q -of XYZ -b {band} {mask} {clipfile} {outfile}"""
        call(cmd, shell=True)
        with open(outfile, 'r') as f:
            lines = f.readlines()
        lines = [l.strip().split(" ") for l in lines]
        lines = [[(float(l[0]), float(l[1])), round(float(l[2]), significant_digits)] for l in lines if
                 not float(l[2]) == 0]
        return lines, colfile


if __name__ == "__main__":
    file = "../Home1442018/Home1442018Harvest.shp"
    # file = "../GarysEast2018/GaryEast2018Harvest.shp"
    sf = SDXShapefile(file)
    # sf.regress('Yld_Vol_Dr', False, 'Elevation_', True)
    # call(f"display {sf.make_raster('Yld_Vol_Dr', slope=False)} &")
    # call(f"display {sf.make_raster('Elevation_', slope=True)}")

    mutliraster = sf.multiraster(
        [
            ("Yld_Vol_Dr", False),
            ("Elevation_", True),
            # ("Elevation_", False),
        ]
    )
    # Scall(f"display {mutliraster}")
    sf.cluster(mutliraster, 8)

    # with fiona.Env():
    #     f = gpd.read_file("../29606/29606/29606.shp")
    #
    # g = gpd.read_file("../29606/export_order_29606_1541428760.csv")
    # df = pd.merge(f, g, left_on="SampleID", right_on="Sample #")
    # df = df.rename(index=str, columns={"geometry_x": "geometry"})
    # df = df.drop("geometry_y", axis=1)
    # df = gpd.GeoDataFrame(df, geometry="geometry")
    # df.to_file("../29606/29606_tests/29606_tests.shp")

    sf.cleanup()
