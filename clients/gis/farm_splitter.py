import os
import tempfile
import zipfile

from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import MultiPolygon, Polygon

from clients.models import Field


class ZippedMultifieldShapeFile:
    """
    Class based handler for zipped shape files. This handler will
    create the requisite events and delegate shapefile processing tasks to
    the auxilliary gis helper.
    """

    def __init__(self, zipfile, farm, tmpdir=None):
        """
        Set up the Zipped File handler
        :param zipfile: Containing shapefile and associated files
        :param field: clients.models.Field
        """
        self.farm = farm
        self.file = zipfile
        self.tempdir = tmpdir
        if not self.tempdir:
            self.tempdir = tempfile.mkdtemp(prefix="sdx")
        self.folder = None
        self.shapefile_handlers = []
        self.zip_is_valid = False
        self.extracted = False
        self.processing = False
        super().__init__()

    def valid_zip(self):
        """
        Extracts zipfile
        :return: True if folder in which shapefile is extracted is correctly setup
        """
        if not self.zip_is_valid:
            try:
                self.processing = True
                unzipped = zipfile.ZipFile(self.file)
                unzipped.extractall(f'{self.tempdir}/')
                # filename = "{}/{}".format(self.tempdir, unzipped.infolist()[0].filename)
                filename = os.path.join(self.tempdir, unzipped.infolist()[0].filename)
                if os.path.isdir(filename):
                    self.folder = filename
                elif os.path.isfile(filename):
                    self.folder = os.path.dirname(filename)
                self.zip_is_valid = True
                self.extracted = True
                return True
            except Exception as e:
                print(e, self.file, self.tempdir)
                raise
            finally:
                self.processing = False
        return True

    def shapefile_list(self):
        """
        :return: List of shapefiles, each containing a layer in the zipfile
        """
        files = os.listdir(self.folder)
        shapefiles = [f for f in files if f[-4:] == ".shp"]
        return shapefiles

    def analyze(self, shapefile):
        """
        Prints a summary of the contents of a shapefile in a given zipfile
        :param: shapefile : String - name of shapefile
        :return: list() an Array of layers within the shapefile
        """
        try:
            # file = "{}/{}".format(self.folder, shapefile)
            file = os.path.join(self.folder, shapefile)
            ds = DataSource(file)
            for lyr in ds:
                print(f"Layer: {lyr.name}, geometry: {lyr.geom_type}, features: {len(lyr.get_geoms())}, fields: {lyr.fields}")
            return ds
        except Exception as e:
            raise
            print(repr(e))

    def create_fields(self, shapefile, lyrname, attributename):
        """
        :param: shapefile : str - filename
        :param: lyrname : str -  name of layer to extract from the shapefile
        :param: attributename : str - use this attribute name as the name of the field created
        """

        try:
            file = os.path.join(self.folder, shapefile)
            ds = DataSource(file)
            for lyr in ds:
                if lyr.name == lyrname:
                    for feature, attribute in zip(lyr.get_geoms(geos=True), lyr.get_fields(attributename)):
                        try:
                            if isinstance(feature, Polygon):
                                bdy = MultiPolygon(feature)
                            elif isinstance(feature, MultiPolygon):
                                bdy = feature
                            else:
                                print(f"Feature {feature} isn't a Poly or MultyPoly")
                                bdy = None
                            fld = Field.objects.create(
                                name=str(attribute),
                                farm=self.farm,
                                boundary=bdy
                            )
                            print(f"{fld} created")
                        except Exception as e:
                            print(f"Feature {feature} had an exception {repr(e)})")
        except Exception as e:
            print(repr(e))
