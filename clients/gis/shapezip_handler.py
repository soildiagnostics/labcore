import os
import shutil
import tempfile
import zipfile

from django.contrib.gis.gdal import DataSource, SpatialReference, CoordTransform
from django.contrib.gis.geos import MultiPoint, MultiPolygon, Point, Polygon, GeometryCollection

from clients.gis.gishelper import SDXShapefile
from clients.models import SubfieldLayer, PRESET_EVENTS


class ZippedShapeFile:
    """
    Class based handler for zipped shape files. This handler will
    create the requisite events and delegate shapefile processing tasks to
    the auxilliary gis helper.
    """

    def __init__(self, zipfile, field, tmpdir=None):
        """
        Set up the Zipped File handler
        :param zipfile: Containing shapefile and associated files
        :param field: clients.models.Field
        """
        self.field = field
        self.file = zipfile
        self.tempdir = tmpdir
        if not self.tempdir:
            self.tempdir = tempfile.mkdtemp(prefix="sdx")
        self.folder = None
        self.shapefile_handlers = []
        self.zip_is_valid = False
        self.extracted = False
        self.processing = False
        super().__init__()

    def valid_zip(self):
        """
        Extracts zipfile
        :return: True if folder in which shapefile is extracted is correctly setup
        """
        if not self.zip_is_valid:
            try:
                self.processing = True
                unzipped = zipfile.ZipFile(self.file)
                unzipped.extractall(f'{self.tempdir}/')
                # filename = "{}/{}".format(self.tempdir, unzipped.infolist()[0].filename)
                filename = os.path.join(self.tempdir, unzipped.infolist()[0].filename)
                if os.path.isdir(filename):
                    self.folder = filename
                elif os.path.isfile(filename):
                    self.folder = os.path.dirname(filename)
                self.zip_is_valid = True
                self.extracted = True
                return True
            except Exception as e:
                print(e, self.file, self.tempdir)
                raise
            finally:
                self.processing = False
        return True

    def shapefile_list(self):
        """
        :return: List of shapefiles, each containing a layer in the zipfile
        """
        files = os.listdir(self.folder)
        shapefiles = [f for f in files if f[-4:] == ".shp"]
        return shapefiles

    def extract_event_date(self, layer):
        """
        Extracts the start and end time from the timestamp in the shapefile
        :param layer: Layer from which event is to be extracted
        :return: tuple (start, end) for min and max dates in the shapefile
        """
        if "Date" in layer.fields:
            drange = layer.get_fields("Date")
            start = min(drange)
            end = max(drange)
            return start, end
        else:
            return None, None



    def make_subfield_layer(self, shapefile, parameters=[], process_type="Shapefile Upload"):
        """
        Creates a client.models.SubfieldLayer for a shapefile and a given list of parameters
        :param shapefile: Shapefile from which to extract parameters
        :param parameters: List of parameters (default = [])
        :param process_type: one of the following:
            'Shapefile Upload':  use the following logic:
                        If there is a single (multi)polygon field, set that as the field boundary
                        If there is a list of points, treat that as yield maps or soil tests, use decimation for large number of features
                        If there is a list of polygons, treat that as polygons, use decimation for large number of features
            'Boundary': Treat the polygon / multipolygon as the field boundary
            'Management Zones': Treat the polygons or multipolygons as management zones
        :return: tuple (subfieldlayer, (start_event, end_event))
        """
        try:
            # file = "{}/{}".format(self.folder, shapefile)
            file = os.path.join(self.folder, shapefile)
            ds = DataSource(file)
            lyr = ds[0]
            subfieldlayer, created = SubfieldLayer.objects.get_or_create(
                field=self.field,
                name=lyr.name,
                source_file="{}".format(
                    self.file.name) if not 'shapefiles/' in self.file.name else self.file.name
            )

            if "process_logs" not in subfieldlayer.dbf_field_values.keys():
                subfieldlayer.dbf_field_values['process_logs'] = dict()

            if created:
                subfieldlayer.dbf_field_names['field_names'] = lyr.fields
                geoms = lyr.get_geoms(geos=True)
                ## Reproject here
                coord_trans = False
                src_srid = lyr[0].geom.srid
                if not src_srid:
                    subfieldlayer.dbf_field_values['process_logs']['original_projection_assumed'] = 4326
                    src_srid = 4326

                subfieldlayer.dbf_field_values['process_logs']['original_projection'] = lyr[0].geom.srid
                if src_srid != 4326:
                    src = SpatialReference(src_srid)
                    tgt = SpatialReference(4326)
                    coord_trans = CoordTransform(src, tgt)
                    subfieldlayer.dbf_field_values['process_logs']['reprojected_to'] = 4326
                ## Code to decimate the file if there are too many geoms
                ## we limit ourselves to about 30,000 geoms.
                # divint = len(geoms) // 30000 + 1
                # subfieldlayer.dbf_field_values['process_logs']['filesize_limited'] = divint != 1
                divint = 1
                small_geoms = list()
                for i in range(len(geoms)):
                    if i % divint == 0:
                        if coord_trans:
                            small_geoms.append(geoms[i].transform(coord_trans, clone=True))
                        else:
                            small_geoms.append(geoms[i])

                subfieldlayer.polygons = MultiPolygon([g for g in small_geoms if isinstance(g, Polygon)])
                subfieldlayer.points = MultiPoint([g for g in small_geoms if isinstance(g, Point)])
                if process_type in PRESET_EVENTS:
                    """Assume defaults for inferred boundary"""
                    if len(small_geoms) == 1 and (isinstance(small_geoms[0], MultiPolygon) or isinstance(small_geoms[0], Polygon)):
                        # Treat this as a boundary file
                        if isinstance(small_geoms[0], MultiPolygon):
                            subfieldlayer.polygons = small_geoms[0]
                        else:
                            subfieldlayer.polygons = MultiPolygon(small_geoms[0])
                        fld = subfieldlayer.field
                        fld.boundary = subfieldlayer.polygons
                        fld.save()

                        subfieldlayer.dbf_field_values['process_logs']['boundary_assumed'] = True
                if process_type == "Boundary" and not subfieldlayer.dbf_field_values['process_logs']['boundary_assumed']:
                    """ We force-set the boundary from polygon features. """
                    fld = subfieldlayer.field
                    fld.boundary = subfieldlayer.simplify_geometry()
                    fld.save()


                ## Always store the geometry collection
                subfieldlayer.geoms = GeometryCollection(small_geoms)
                subfieldlayer.save()

            if parameters:
                for p in parameters:
                    if p in lyr.fields:
                        try:
                            values = lyr.get_fields(p)
                            # # divint = len(values) // 30000 + 1
                            # small_values = list()
                            # for i in range(len(values)):
                            #     if i % divint == 0:
                            #         small_values.append(values[i])
                            subfieldlayer.dbf_field_values[p] = values
                        except Exception as e:
                            subfieldlayer.dbf_field_values[p] = repr(e)
                subfieldlayer.save()

            return subfieldlayer, self.extract_event_date(lyr)
        except Exception as e:
            raise

    def attach_to_field(self, shapefile=None, parameter=None, slope=False, process_type="Shapefile Upload"):
        """
        Creates a subfield layer and a raster for a given parameter
        :param shapefile: shapefile. If shapefile is not specified,
            all shapefiles in the zip are processed and corresponding subfieldlayers are
            created.
        :param parameter: parameter. If parameter is specified, a raster file is also created.
        :param slope: DEPRECATED
        :param process_type: This is a field event name that indicates how a shapefile is to be processed
        :return: subfieldlayer list
        """
        if not self.extracted:
            if not self.valid_zip():
                return None

        layers = []
        shapefiles_list = self.shapefile_list()
        if shapefile:
            shapefiles_list = [s for s in shapefiles_list if shapefile in s]

        for sf in shapefiles_list:
            subfield, (start, end) = self.make_subfield_layer(sf, parameters=[parameter],
                                                              process_type=process_type)
            layers.append((subfield, start, end))

        return layers


    def make_raster(self, shapefile, parameter, slope=False, extent=None):
        """
        Create raster file from shapefile for a given parameter
        :param shapefile: Shapefile
        :param parameter: Parameter
        :param slope: Bool, default False
        :param color: Default "YG" for yellow to green. See other options
        :return: filename for raster.
        """

        if not self.folder:
            if not self.valid_zip():
                return None

        try:
            shObject = None
            for h in self.shapefile_handlers:
                # if h.file == "{}/{}.shp".format(self.folder, shapefile):
                if h.file == os.path.join(self.folder, f"{shapefile}.shp"):
                    shObject = h
            if not shObject:
                shObject = SDXShapefile(file=os.path.join(self.folder, f"{shapefile}.shp"))
                self.shapefile_handlers.append(shObject)

            raster = shObject.make_raster(parameter, color=None, slope=slope, extent=extent)
            return raster

        except Exception:
            raise

    def cleanup(self):
        """
        Remove temp directory
        :return: None
        """
        try:
            for s in self.shapefile_handlers:
                s.cleanup()
            shutil.rmtree(self.tempdir)
        except OSError as e:
            print(f"{self.__class__} Error {e.filename}-{e.strerror}")
