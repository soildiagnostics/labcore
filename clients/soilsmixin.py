import ast
import json

import requests
from django.conf import settings
from django.contrib.gis.geos import MultiPolygon, Polygon, GeometryCollection, GEOSGeometry


class SoilsAPIMixin(object):
    base = settings.SOILDX_SOILS

    def soils(self):

        data = {
            'boundary': self.field.boundary.wkt,
            'valu1': 1,
            'muaggatt': 1
        }
        response = requests.post(f"{self.base}intersection/feature", json=data)
        if response.status_code == 200:
            return response.content
        else:
            print(response.content)
            return b''

    def make_layer(self):
        try:
            self.name = "Soil Survey Data"
            features = json.loads(self.soils()).get('features')

            if features:
                field_names = {
                    'field_names': [
                        'areasymbol',
                        'musym',
                        'mukey',
                        # 'nccpi2cs',
                        'nccpi3corn',
                        # 'nccpi2sg',
                        "nccpi3soy",
                        "nccpi3cot",
                        'nccpi3sg',
                        # 'nccpi2co',
                        # 'nccpi2all',
                        'nccpi3all',
                        'rootznemc',
                        'rootznaws',
                        'droughty'
                    ]
                }

                self.dbf_field_names = field_names

                properties = dict()
                for f in field_names['field_names']:
                    properties[f] = list()

                flat_geometries = list()
                for i in range(len(features)):
                    g = features[i].get('geometry')
                    if g.get('type') == 'Polygon':
                        flat_geometries.append(Polygon(g.get('coordinates')[0]))
                        for fn in field_names['field_names']:
                            properties[fn].append(features[i].get('properties').get(fn))
                    else:
                        assert g.get('type') == "MultiPolygon"
                        for j in range(len(g.get('coordinates'))):
                            flat_geometries.append(Polygon(g.get('coordinates')[j][0]))
                            for fn in field_names['field_names']:
                                properties[fn].append(features[i].get('properties').get(fn))

                self.dbf_field_values = properties
                self.geoms = GeometryCollection(flat_geometries)

                self.save()
        except Exception as e:
            print(e)


def make_geometrycollection_from_featurecollection(feature_collection):
    geoms = []
    features = ast.literal_eval(feature_collection)
    for feature in features['features']:
        feature_geom = feature['geometry']
        geoms.append(GEOSGeometry(json.dumps(feature_geom)))
    return GeometryCollection(tuple(geoms))
