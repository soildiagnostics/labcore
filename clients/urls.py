from django.urls import path

from clients.views import (ClientList, ClientListJSON, ClientAdd, ClientDelete, ClientUpdate, ClientDetail,
                           FarmAdd, FarmDelete, FarmDetail, FieldAdd, FieldDelete, FieldDetail, FieldList,
                           FieldListJSON, FieldAddress, FieldFileDropzone
                           )
from clients.views_gis import (FieldGeoDetails, FieldGeoLayer, FieldGeoRaster, FieldGeoRasterPost,
                               FieldRiskReport, FieldRiskReportUpdate, FieldVariabilityReport,
                               FieldVariabilityReportDelete, GenerateSamplePoints, GenerateSampleShapeFile,
                               UpdateSamplePoints, RemoveRasterCache, SubfieldLayerDetails,
                               SubfieldLayerUpdate, DeleteSubfieldView, DuplicateSubfieldLayer,
                               BringInSubfieldLayer, ApplyFormulaOnSubfieldLayer, SubfieldLayerDownloadShape,
                               BirdsEyeView, GenerateSampleGridFromPoints)
from common.apihelper import apipath
from .apiviews import (APIClientList, APICreateHallPass, APIFieldsByDistance,
                       APIFieldEventStatusView, APIAdjustLayerValue, APIFarmsForClient, APIFieldsForFarm,
                       APISubfieldGeoJSONView, APISubfieldParameters, APINewClient, APIPostZipfile, APIEditDeleteClient,
                       APIEditFarm, APIEditField, APIEditFieldGeoJSON, APISubfieldLayers, APISubfieldLayerEdit,
                       APISearchClientsFarmsFields, APIListFieldLocations, APISubfieldLayerDuplicate,
                       APISubfieldLayerAddFeature, APIGetClient, APISetLayerAsBoundary, APIClientStats,
                       APILayerStubView, APIPolygonizeRaster, APISubfieldRaster)

urlpatterns = [
    path('', ClientList.as_view(), name="client_list"),
    path("spatial/", BirdsEyeView.as_view(), name="client_list_spatial"),
    path('data/', ClientListJSON.as_view(), name="client_list_json"),
    path('add/', ClientAdd.as_view(), name="client_add"),
    path('<int:pk>/', ClientDetail.as_view(), name="client_detail"),
    path('<int:pk>/update', ClientUpdate.as_view(), name="client_update"),
    path('<int:pk>/delete/', ClientDelete.as_view(), name="client_delete"),
    ##
    path('<int:pk>/add/', FarmAdd.as_view(), name="new_farm"),
    path('farm/<int:pk>/', FarmDetail.as_view(), name="farm_detail"),
    path('farm/<int:pk>/delete/', FarmDelete.as_view(), name="farm_delete"),
    ##
    path('farm/<int:pk>/add/', FieldAdd.as_view(), name="new_field"),
    path('fields/', FieldList.as_view(), name="field_list"),
    path('fields/data/', FieldListJSON.as_view(), name="field_list_json"),
    path('field/<int:pk>/', FieldDetail.as_view(), name="field_detail"),
    path('field/<int:pk>/address/', FieldAddress.as_view(), name="field_address"),
    path('field/<int:pk>/fileupload/', FieldFileDropzone.as_view(), name="field_file_dropzone"),
    path('field/<int:pk>/delete/', FieldDelete.as_view(), name="field_delete"),
    path('field/<int:pk>/gis/', FieldGeoDetails.as_view(), name="field_geo_details"),
    path('field/<int:pk>/gis/<int:subfield>/<str:file>/<str:parameter>/', FieldGeoLayer.as_view(),
         name="field_geo_layer"),
    path('field/<int:pk>/gis/<int:subfield>/<str:file>/<str:parameter>/raster/', FieldGeoRaster.as_view(),
         name="field_geo_raster"),
    path('field/<int:pk>/gis/<file>/<parameter>/<layer>/raster/post/',
         FieldGeoRasterPost.as_view(), name="field_geo_raster_post"),
    path('field/<int:pk>/riskreport/', FieldRiskReport.as_view(), name="field_risk_report"),
    path('field/<int:pk>/riskupdate/', FieldRiskReportUpdate.as_view(), name="field_risk_update"),
    path('field/<int:pk>/variability/', FieldVariabilityReport.as_view(), name="field_variability_report"),
    path('field/<int:pk>/variability/delete',
         FieldVariabilityReportDelete.as_view(), name="field_variability_report_delete"),
    path('field/<int:pk>/samplingmap/', GenerateSamplePoints.as_view(), name="create_sample_map"),
    path('layer/<int:pk>/samplingmap/', GenerateSampleGridFromPoints.as_view(), name="create_sample_grid_from_points"),
    path('layer/<int:pk>/update/', UpdateSamplePoints.as_view(), name="update_sample_map"),
    path('layer/<int:pk>/details/', SubfieldLayerDetails.as_view(), name="subfield_details"),
    path('layer/<int:pk>/edit/', SubfieldLayerUpdate.as_view(), name="subfield_update"),
    path('layer/<int:pk>/download/', SubfieldLayerDownloadShape.as_view(), name="subfield_download"),
    path('layer/<int:pk>/shapefile/', GenerateSampleShapeFile.as_view(), name="sample_map_shapefile"),
    path('layer/<int:pk>/removerasters/', RemoveRasterCache.as_view(), name="remove_raster_cache"),
    path('layer/<int:pk>/duplicate/', DuplicateSubfieldLayer.as_view(), name="duplicate_subfield_layer"),
    path('layer/<int:pk>/bringinto/<int:target>/<str:param>/<str:agg>/',
         BringInSubfieldLayer.as_view(), name="bring_in_subfield_layer"),
    path('layer/<int:pk>/apply_formula/<int:formula>/', ApplyFormulaOnSubfieldLayer.as_view(),
         name="apply_formula_on_subfield"),
    path('layer/<int:pk>/deletelayer/', DeleteSubfieldView.as_view(), name="delete_subfield_layer"),
]

urlpatterns += [
    ## Clients
    apipath('list/', APIClientList.as_view(), name="api_clients_list"),
    apipath("stats/", APIClientStats.as_view(), name="api_client_stats"),
    apipath('retrieve/<str:name>/', APIGetClient.as_view(), name="api_client_retrieve"),
    apipath('create/', APINewClient.as_view(), name="api_new_client"),
    apipath('edit/<int:pk>/', APIEditDeleteClient.as_view(), name="api_edit_delete_client"),
    ## Farms
    apipath("<int:client>/farms/", APIFarmsForClient.as_view(), name="api_client_farms"),
    apipath("farm/<int:pk>/edit/", APIEditFarm.as_view(), name="api_edit_farm"),
    ## Fields
    apipath("fields/list/", APIListFieldLocations.as_view(), name="api_field_locations"),
    apipath("farm/<int:farm>/fields/", APIFieldsForFarm.as_view(), name="api_farm_fields"),
    apipath("field/<int:pk>/edit/", APIEditField.as_view(), name="api_edit_field"),
    apipath("field/<int:pk>/edit/gj/", APIEditFieldGeoJSON.as_view(), name="api_edit_field_geojson"),
    ## Layers
    apipath("field/<int:field>/layers/", APISubfieldLayers.as_view(), name="api_subfield_layers"),
    apipath("field/<int:field>/zipfile/", APIPostZipfile.as_view(), name="api_post_zipfile"),
    apipath("layer/<int:pk>/edit/", APISubfieldLayerEdit.as_view(), name="api_subfield_edit"),
    apipath('layer/<int:pk>/stub/', APILayerStubView.as_view(), name="api_layer_stub_view"),
    apipath("layer/<int:pk>/duplicate/", APISubfieldLayerDuplicate.as_view(), name="api_subfield_duplicate"),
    apipath("layer/<int:pk>/addfeature/", APISubfieldLayerAddFeature.as_view(), name="api_subfield_addfeature"),
    apipath("layer/<int:pk>/parameters/", APISubfieldParameters.as_view(), name="api_layer_parameters"),
    apipath("layer/<int:pk>/adjust/", APIAdjustLayerValue.as_view(), name="api_adjust_layer_value"),
    apipath("layer/<int:pk>/setboundary/", APISetLayerAsBoundary.as_view(), name="api_set_boundary"),
    apipath("layer/<int:pk>/<str:parameter>/", APISubfieldGeoJSONView.as_view(), name="api_layer_geojson"),
    apipath("raster/<int:pk>/<str:parameter>/", APISubfieldRaster.as_view(), name="api_raster"),
    apipath("layer/<int:pk>/<str:parameter>/polygonize/", APIPolygonizeRaster.as_view(), name="api_raster_polygonize"),
    ## Other APIs
    apipath('hallpass/', APICreateHallPass.as_view(), name="api_create_hall_pass"),
    apipath("field/bylocation/", APIFieldsByDistance.as_view(), name="api_fields_by_location"),
    apipath("list/spatial/", APIFieldEventStatusView.as_view(), name="api_field_events_birdseye"),
    ### Search APIs
    apipath('search/', APISearchClientsFarmsFields.as_view(), name="api_search_clients_farms_fields")
]
