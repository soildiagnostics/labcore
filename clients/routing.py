from django.urls import path

from clients import consumers

websocket_urlpatterns = [
    path('clients/field/<int:pk>/gis/ws/', consumers.GISProcessConsumer),
]