from django import template
from django.db.models import Q
from clients.models import SubfieldLayer
from formulas.models import Formula

register = template.Library()

@register.filter
def formulas(string):
    return Formula.objects.filter(inputs__test_parameter__name__contains=string)
    #return Formula.objects.filter(Q(input_parameters__contains=f"('{string}', None)")|
                                  # Q(input_parameters__contains=f"('{string}',None)"))

@register.filter
def area_avg(layer: SubfieldLayer, parameter):
    try:
        return layer.area_weighted_mean(parameter)
    except Exception:
        return "Parameter not cached"

@register.filter
def area_sum(layer: SubfieldLayer, parameter):
    try:
        return layer.area_weighted_sum(parameter)
    except Exception:
        return "Parameter not cached"