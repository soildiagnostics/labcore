from django import template
register = template.Library()
from django.urls import reverse
from urllib.parse import quote

@register.filter
def parameter_url(string):
    comps = string.split(":")
    # return quote(f"/clients/field/{int(comps[1])}/gis/{int(comps[3])}/{comps[2]}/{comps[4]}/")
    return reverse("field_geo_layer", kwargs={
        "pk": int(comps[1]),
        "subfield": int(comps[3]),
        "file": quote(comps[2], safe=''),
        "parameter": quote(comps[4], safe='')
    })

@register.filter
def format_parameter(string):
    comps = string.split(":")
    return f"{comps[2]} ({comps[3]}): {comps[4]}"