# Generated by Django 3.1.3 on 2020-11-21 17:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0054_auto_20200825_1638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subfieldlayer',
            name='dbf_field_names',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='subfieldlayer',
            name='dbf_field_values',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='variabilityreport',
            name='data',
            field=models.JSONField(default=dict),
        ),
    ]
