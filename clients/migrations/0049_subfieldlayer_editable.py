# Generated by Django 3.0.5 on 2020-08-04 18:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0048_subfieldlayer_geoms'),
    ]

    operations = [
        migrations.AddField(
            model_name='subfieldlayer',
            name='editable',
            field=models.BooleanField(default=False),
        ),
    ]
