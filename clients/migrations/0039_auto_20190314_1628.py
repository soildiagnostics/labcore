# Generated by Django 2.1.5 on 2019-03-14 21:28

from django.db import migrations, models

import clients.models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0038_client_tags'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rasterfile',
            name='file',
            field=models.ImageField(upload_to=clients.models.get_raster_filename),
        ),
    ]
