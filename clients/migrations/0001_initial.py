# Generated by Django 2.0.7 on 2018-08-07 16:19

import django.contrib.gis.db.models.fields
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('organizations', '0003_field_fix_and_editable'),
        ('contact', '0006_auto_20180806_1558'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.TextField(blank=True, help_text='Additional information', null=True)),
                ('contact', models.OneToOneField(help_text='Could be Company or Person', on_delete=django.db.models.deletion.PROTECT, to='contact.Contact')),
                ('originator', models.ForeignKey(help_text='Person originating the order', on_delete=django.db.models.deletion.CASCADE, related_name='client', to='organizations.OrganizationUser')),
            ],
        ),
        migrations.CreateModel(
            name='Farm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='farms', to='clients.Client')),
            ],
        ),
        migrations.CreateModel(
            name='Field',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('bounding_box', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=4326)),
                ('farm', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fields', to='clients.Farm')),
            ],
        ),
        migrations.CreateModel(
            name='Subfield',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('bounding_box', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326)),
            ],
        ),
        migrations.CreateModel(
            name='SubfieldLayer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
                ('field', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='layer', to='clients.Field')),
            ],
        ),
        migrations.AddField(
            model_name='subfield',
            name='layer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subfield', to='clients.SubfieldLayer'),
        ),
    ]
