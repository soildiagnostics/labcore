from statistics import mean
from unittest import skip

from django.contrib.gis.geos import GeometryCollection
from django.test import TestCase, tag, override_settings
from django.urls import reverse
from shapely.geometry import Polygon

from clients.gis.shapezip_handler import ZippedShapeFile
from clients.tests.tests_models import ClientsTestCase
from clients.models import SubfieldLayer
import os, tempfile, shutil
from clients.gis.gishelper import SDXShapefile
import os
import shutil
import tempfile

from django.test import TestCase

from clients.gis.gishelper import SDXShapefile
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.models import SubfieldLayer
from clients.tests.tests_models import ClientsTestCase


class ZipFileTest(TestCase):
    fixtures = ['roles']

    def setUp(self):

        self.file = open("./clients/tests/GarysEast2018.zip", 'rb')
        ClientsTestCase().setUp()
        self.field = ClientsTestCase().createField()

    def test_cleanup(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertIsNotNone(self.handler.tempdir)
        folder = self.handler.tempdir
        self.handler.cleanup()
        import os
        with(self.assertRaises(FileNotFoundError)):
            os.listdir(folder)

    def test_unzip_file(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        self.handler.cleanup()

    def test_layer_list(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        self.assertEqual(len(self.handler.shapefile_list()), 4)
        self.handler.cleanup()

    @tag("single")
    def test_open_shapefile(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        files = self.handler.shapefile_list()
        for f in files[0:2]:
            subfieldlayer, date = self.handler.make_subfield_layer(f)
            subfieldlayer, date = self.handler.make_subfield_layer(f,
                                                                   subfieldlayer.dbf_field_names['field_names'][0:1])
            self.assertNotEqual(subfieldlayer, None)
        self.assertEqual(SubfieldLayer.objects.filter(field=self.field).count(), 2)

        lyr = self.field.layer.get(name="GarysEast2018Planting")
        self.assertEqual(lyr.features, 29995)
        self.handler.cleanup()

    @tag("single")
    def test_get_subfield_parameter(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        files = self.handler.shapefile_list()
        subfieldlayer, date = self.handler.make_subfield_layer(files[0])
        subfieldlayer, date = self.handler.make_subfield_layer(files[0],
                                                               subfieldlayer.dbf_field_names['field_names'][3:4])
        self.assertNotEqual(subfieldlayer, None)
        # print(subfieldlayer.name, subfieldlayer.dbf_field_names['field_names'])

        values = subfieldlayer.get_values(subfieldlayer.dbf_field_names['field_names'][3])

        self.assertEqual(len(values), 29995)
        self.handler.cleanup()

    @tag("single")
    def test_autocreate_subfields(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        subfields = self.handler.attach_to_field()
        self.assertEqual(len(subfields), 4)
        for subfield, start, end in subfields:
            self.assertIsNotNone(subfield.dbf_field_values['process_logs'])
            self.assertTrue(subfield.points_only)
            self.assertEqual(subfield.dbf_field_values["process_logs"].get('original_projection'), 4326)
            self.assertIsNone(subfield.dbf_field_values["process_logs"].get('reprojected_to'))
        self.handler.cleanup()

    def test_autocreate_subfields_generic_geoms(self):
        file = open("./clients/tests/Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip", "rb")
        self.handler = ZippedShapeFile(file, self.field)
        subfields = self.handler.attach_to_field(shapefile='Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly',
                                                 parameter='Zone_ID')
        self.assertEqual(len(subfields), 1)
        self.assertEqual(subfields[0][0].features, 8)
        for subfield, start, end in subfields:
            self.assertIsNotNone(subfield.dbf_field_values['process_logs'])
            self.assertFalse(subfield.points_only)
            self.assertTrue(subfield.polygons_only)
            self.assertEqual(subfield.dbf_field_values["process_logs"].get('original_projection'), 4326)
            self.assertIsNone(subfield.dbf_field_values["process_logs"].get('reprojected_to'))
        self.handler.cleanup()

    def test_adjust_layer_value(self):
        file = open("./clients/tests/LordaleBobWest-Retest.zip", 'rb')
        self.handler = ZippedShapeFile(file, self.field)
        subfields = self.handler.attach_to_field()
        self.assertEqual(len(subfields), 2)
        sf, _, _ = subfields[0]
        self.assertEqual(sf.name, "Lordale Farms - Bob's - West - 2019 Soil Test Sites")
        sf.dbf_field_names['field_names'].append("testPar")
        sf.dbf_field_values["testPar"] = [0] * 5
        sf.editable = True
        sf.save()
        sf.refresh_from_db()
        sf.adjust_value("testPar", 0, 1)
        sf.refresh_from_db()
        self.assertEqual(sf.get_values("testPar"), [1, 0, 0, 0, 0])

        ## Let's now test the API way of changing it.

        data = {
            "parameter": "testPar",
            "sequence": 1,
            "value": 1
        }

        from clients.serializers import LayerAdjustSerializer
        ser = LayerAdjustSerializer(data=data)
        self.assertTrue(ser.is_valid())

        url = reverse("api_adjust_layer_value", kwargs={'pk': sf.id})
        from django.test.client import Client as C
        from contact.models import User
        clt = C()
        clt.force_login(user=User.objects.get(username="myuser"))

        import json
        resp = clt.put(url, json.dumps(data), content_type="application/json")
        self.assertEqual(resp.status_code, 200)
        sf.refresh_from_db()
        self.assertEqual(sf.get_values("testPar"), [1, 1, 0, 0, 0])
        self.assertEqual(len(sf.dbf_field_values.get("updates")), 2)

        self.handler.cleanup()

    def test_unzip_boundary_file_multipoly(self):
        file = open("./clients/tests/Wubben_DunsheeW150.zip", 'rb')
        handler = ZippedShapeFile(file, self.field)
        self.assertEqual(self.field.area, 76.6)
        subfields = handler.attach_to_field()
        self.assertEqual(len(subfields), 2)
        ## This file has a multipolygon boundary
        self.assertTrue(subfields[0][0].polygons_only)
        self.assertTrue(subfields[1][0].points_only)
        self.assertTrue(subfields[0][0].inferred_boundary)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 151.2)
        handler.cleanup()

    def test_unzip_boundary_file_polygon(self):
        file = open("./clients/tests/N40.zip", 'rb')
        handler = ZippedShapeFile(file, self.field)
        self.assertEqual(self.field.area, 76.6)
        subfields = handler.attach_to_field(process_type="Boundary")
        self.assertEqual(len(subfields), 1)
        self.assertTrue(subfields[0][0].polygons_only)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 38.3)
        handler.cleanup()

    def test_unzip_sampling_locations_file_polygon(self):
        file = open("./clients/tests/N40.zip", 'rb')
        handler = ZippedShapeFile(file, self.field)
        self.assertEqual(self.field.area, 76.6)
        subfields = handler.attach_to_field(process_type="Sampling Locations")
        self.assertEqual(len(subfields), 1)
        self.assertTrue(subfields[0][0].polygons_only)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 38.3)
        handler.cleanup()


# for fname in os.listdir(tempfile.gettempdir()):
#     if fname.startswith("sdx") or fname.startswith("gis"):
#         try:
#             print(fname)
#             shutil.rmtree(os.path.join(tempfile.gettempdir(), fname))
#         except OSError as e:
#             print(f"Teardown {self.__class__} Error {e.filename}-{e.strerror}")


# @override_settings(GDAL_LIBRARY_PATH = '/usr/local/lib/libgdal.so')
class GISTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        self.file = open("./clients/tests/GarysEast2018.zip", 'rb')

        ClientsTestCase().setUp()
        self.field = ClientsTestCase().createField()

    def test_extract_dbf_values_anticipation(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='GarysEast2018Planting', parameter='Elevation_')
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0].name, "GarysEast2018Planting")
        self.assertEqual(res[0][0].features, 29995)
        self.handler.cleanup()

    def test_spatial_filter(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='GarysEast2018Planting', parameter='Elevation_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "GarysEast2018Planting")
        self.assertEqual(sf.features, 29995)
        aoi = sf.points.centroid.buffer(0.001)
        self.assertTrue(sf.points.intersects(aoi))
        vals = sf.get_values("Elevation_")
        self.assertEqual(len(vals), 29995)
        vals = sf.get_values("Elevation_", aoi)
        self.assertEqual(len(vals), 1518)
        self.handler.cleanup()

    def test_create_geodataframe_from_points(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='GarysEast2018Planting', parameter='Elevation_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "GarysEast2018Planting")
        self.assertEqual(sf.features, 29995)
        gdf = sf.make_geodataframe()
        self.assertEqual(gdf.shape, (29995, 2))

    def test_create_geodataframe_from_polygons(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='GarysEast2018Planting', parameter='Elevation_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "GarysEast2018Planting")
        self.assertEqual(sf.features, 29995)
        avg_elev = mean(sf.get_values("Elevation_"))
        ## Create new subfieldlayer
        from django.contrib.gis.geos import MultiPolygon
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.points.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))

    def test_create_geodataframe_from_heterogeneousfile(self):
        file = open("./clients/tests/Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip", "rb")
        self.handler = ZippedShapeFile(file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly',
                                           parameter='Zone_ID')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly")
        self.assertEqual(sf.features, 8)
        ## Create new subfieldlayer
        from django.contrib.gis.geos import MultiPolygon
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.geoms.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))

    def test_create_shapefile_from_geodataframe(self):

        file = open("./clients/tests/Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip", "rb")
        self.handler = ZippedShapeFile(file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly',
                                           parameter='Zone_ID')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly")
        self.assertEqual(sf.features, 8)
        ## Create new subfieldlayer
        from django.contrib.gis.geos import MultiPolygon
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.geoms.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))
        gdf.to_file("gdf_test_write.shp")
        import os
        os.remove("gdf_test_write.shp")
        os.remove("gdf_test_write.cpg")
        os.remove("gdf_test_write.dbf")
        os.remove("gdf_test_write.shx")

    def test_create_raster_from_heterogenous_file(self):

        file = open("./clients/tests/Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip", "rb")
        self.handler = ZippedShapeFile(file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly',
                                           parameter='K_ppm_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        from django.contrib.gis.geos import MultiPolygon
        sf.field.boundary = MultiPolygon(sf.geoms.convex_hull)
        sf.field.save()
        self.assertEqual(sf.name, "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly")
        self.assertEqual(sf.features, 8)
        ## Create new subfieldlayer
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.geoms.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))
        png = sf.make_raster(None, 'K_ppm_')
        import os
        self.assertTrue(os.path.exists(png.file.path))
        os.remove(png.file.path)


    def test_bring_in_geodataframe(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(shapefile='GarysEast2018Planting', parameter='Elevation_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        self.assertEqual(sf.name, "GarysEast2018Planting")
        self.assertEqual(sf.features, 29995)
        avg_elev = mean(sf.get_values("Elevation_"))
        ## Create new subfieldlayer
        from django.contrib.gis.geos import MultiPolygon
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.points.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))

        small_sf.bring_in(sf, "Elevation_")
        self.assertEqual(len(small_sf.dbf_field_values['updates']), 1)
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 3))
        self.assertAlmostEqual(gdf["Elevation_"][0], avg_elev, 2)

    @skip
    def test_make_slope_raster(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        raster = self.handler.make_raster(shapefile='GarysEast2018Planting', parameter='Elevation_', slope=True)
        self.assertIn("/tmp", raster)
        self.handler.cleanup()

    @tag("single")
    def test_regression_and_clustering(self):
        self.handler = ZippedShapeFile(self.file, self.field)
        self.assertTrue(self.handler.valid_zip())
        yraster = self.handler.make_raster('GaryEast2018Harvest', 'Yld_Vol_Dr', slope=False)
        xraster = self.handler.make_raster('GaryEast2018Harvest', 'Elevation_', slope=True)

        self.sdxhelper = SDXShapefile()
        result, warn = self.sdxhelper.regress(yraster, False, xraster, True)
        self.assertNotEqual(result, None)
        clusterresults = self.sdxhelper.cluster_params([yraster, xraster])
        self.assertIn("/tmp", clusterresults.get('image'))
        self.sdxhelper.cleanup()
        self.handler.cleanup()

    def test_cleanup(self):
        another_handler = ZippedShapeFile(self.file, self.field)
        tmp = another_handler.tempdir
        another_handler.cleanup()
        for fname in os.listdir(tempfile.gettempdir()):
            self.assertNotIn(tmp, fname)
