import pytest
from channels.routing import URLRouter
from channels.testing import WebsocketCommunicator
from django.contrib.gis.geos import GEOSGeometry

from clients.consumers import GISProcessConsumer
from asgiref.sync import async_to_sync, sync_to_async
from django.test import Client as TestClient, tag
from django.test import TransactionTestCase
from django.urls import reverse, path

from associates.models import Organization
from clients.models import Client, Field, SubfieldLayer, RasterFile
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company, Person, User
from fieldcalendar.models import CalendarEvent




class GISPageTests(TransactionTestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase().createField()

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)

    def test_view_geo_details(self):
        resp = self.client.get(reverse("field_geo_details", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_ws_connect(self):

        @pytest.mark.asyncio
        async def test_gisprocess_consumer_connect():
            communicator = WebsocketCommunicator(GISProcessConsumer, "/testws/")
            connected, subprotocol = await communicator.connect()
            assert connected

        async_to_sync(test_gisprocess_consumer_connect)()

    @tag("single")
    def test_variability_analysis(self):

        wsurl = reverse("field_geo_details", kwargs={'pk': self.field.pk}) + "ws/"
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-88.242459 40.063451, -88.242538 40.063707, -88.241821 40.063764, -88.241592 40.063457, -88.242459 40.063451 )))')
        self.field.boundary = mp
        self.field.save()
        self.assertEqual(self.field.layer.first().name, "Soil Survey Data")
        yld = f"id:{self.field.id}:Soil Survey Data:{self.field.layer.first().pk}:nccpi3corn"
        @pytest.mark.asyncio
        async def test_gisconsumer_variability_analysis():
            application = URLRouter([
                path('clients/field/<int:pk>/gis/ws/', GISProcessConsumer),
            ])

            communicator = WebsocketCommunicator(application, wsurl)
            connected, subprotocol = await communicator.connect()
            assert connected

            packet = {
                "request": "variability",
                "yield[]": [yld],
                "others[]": [],
                "plotonly[]": []
            }

            await communicator.send_json_to(packet)
            message = None
            while message != "done":
                response = await communicator.receive_json_from()
                message = response["message"]
            await communicator.disconnect()

        async_to_sync(test_gisconsumer_variability_analysis)()
        self.assertEqual(RasterFile.objects.filter(layer=self.field.layer.filter(name="Soil Survey Data").first(),
                                                   parameter="nccpi3corn").count(), 1)
        ## Check cacheing

        async_to_sync(test_gisconsumer_variability_analysis)()

    @tag("single")
    def test_regression_analysis(self):

        wsurl = reverse("field_geo_details", kwargs={'pk': self.field.pk}) + "ws/"
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-88.242459 40.063451, -88.242538 40.063707, -88.241821 40.063764, -88.241592 40.063457, -88.242459 40.063451 )))')
        self.field.boundary = mp
        self.field.save()
        self.assertEqual(self.field.layer.latest('pk').name, "Soil Survey Data")
        yld = f"id:{self.field.id}:Soil Survey Data:{self.field.layer.first().pk}:nccpi3corn"
        other = f"id:{self.field.id}:Soil Survey Data:{self.field.layer.first().pk}:nccpi3soy"
        plt = f"id:{self.field.id}:Soil Survey Data:{self.field.layer.first().pk}:rootznemc"

        @pytest.mark.asyncio
        async def test_gisconsumer_regression_analysis():
            application = URLRouter([
                path('clients/field/<int:pk>/gis/ws/', GISProcessConsumer),
            ])

            communicator = WebsocketCommunicator(application, wsurl)
            connected, subprotocol = await communicator.connect()
            assert connected

            packet = {
                "request": "variability",
                "yield[]": [yld],
                "others[]": [other],
                "plotonly[]": [plt]
            }

            await communicator.send_json_to(packet)

            message = None
            while message != "done":
                response = await communicator.receive_json_from()
                message = response["message"]

            packet = {
                "request": "variability",
                "yield[]": [other],
                "others[]": [plt],
                "plotonly[]": [yld]
            }

            ## Let's test the cacheing
            await communicator.send_json_to(packet)

            message = None
            while message != "done":
                response = await communicator.receive_json_from()
                message = response["message"]

            await communicator.disconnect()

        async_to_sync(test_gisconsumer_regression_analysis)()

    @tag("single")
    def test_async_shapefile_reuse_in_consumer(self):

        with open("clients/tests/Jasper_Co.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }

        from clients.tests.tests_asyncgeoprocesses import background_worker
        async_to_sync(background_worker)(message)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 298.6)

        soil_test_layer = self.field.layer.get(name="2016 Soil Test Sites")
        yld = f"id:{self.field.id}:2016 Soil Test Sites:{soil_test_layer.pk}:LOI"
        other = f"id:{self.field.id}:2016 Soil Test Sites:{soil_test_layer.pk}:Ca"
        plt = f"id:{self.field.id}:2016 Soil Test Sites:{soil_test_layer.pk}:pH"
        wsurl = reverse("field_geo_details", kwargs={'pk': self.field.pk}) + "ws/"

        @pytest.mark.asyncio
        async def test_gisconsumer_regression_analysis():
            application = URLRouter([
                path('clients/field/<int:pk>/gis/ws/', GISProcessConsumer),
            ])

            communicator = WebsocketCommunicator(application, wsurl)
            connected, subprotocol = await communicator.connect(timeout=5)
            assert connected

            packet = {
                "request": "variability",
                "yield[]": [yld],
                "others[]": [other],
                "plotonly[]": [plt]
            }

            await communicator.send_json_to(packet)

            message = None
            while message != "done":
                response = await communicator.receive_json_from(timeout=10)
                message = response["message"]
                # print(message)

            packet = {
                "request": "variability",
                "yield[]": [other],
                "others[]": [plt],
                "plotonly[]": [yld]
            }

            ## Let's test the cacheing
            await communicator.send_json_to(packet)

            message = None
            while message != "done":
                response = await communicator.receive_json_from(timeout=5)
                message = response["message"]
                # print(message)

            await communicator.disconnect()

        async_to_sync(test_gisconsumer_regression_analysis)()


    # def tearDown(self) -> None:
    #     import os
    #     import tempfile
    #     for fname in os.listdir(tempfile.gettempdir()):
    #         self.assertNotIn("gis", fname)
    #         self.assertNotIn("sdx", fname)

