from asgiref.sync import async_to_sync
from django.contrib.gis.geos import Point, GeometryCollection
from django.test import TestCase, LiveServerTestCase
# from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from clients.gis.shapezip_handler import ZippedShapeFile
from clients.models import Field, Client, SubfieldLayer
from clients.tests.tests_asyncgeoprocesses import background_worker
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Person
from fieldcalendar.event_base import CalendarEvent


class GISAPITests(LiveServerTestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = APIClient()
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        # self.client.force_authenticate(user=self.user)
        self.field = ClientsTestCase.createField(ClientsTestCase())

        # Retrieve
        url = reverse("api_edit_field_geojson", kwargs={'pk': self.field.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = response.json()


        # Update
        data = {
            "name": "Test Field",
            "boundary": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                -88.224559,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.102634
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.720092,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936089
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.82287,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.805679
                            ]
                        ]
                    ]
                ]
            },
            "srid": 4326,
            "farm": content['farm']
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Field.objects.latest('pk').area, 3.9)

    def test_fields_list(self):
        url = reverse("api_field_locations")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.json()[0]['boundary'], None)
        self.assertEqual(response.json()[0]['centroid'], None)

        # Centroid only
        with self.assertNumQueries(5):
            response = self.client.get(f"{url}?centroid=1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]['boundary'], None)
        self.assertNotEqual(response.json()[0]['centroid'], None)

    def test_sublayer_lists(self):
        url = reverse('api_subfield_layers', kwargs={'field': self.field.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['features'], 5)

    def test_sublayer_lists_poly_only(self):
        url = reverse('api_subfield_layers', kwargs={'field': self.field.pk}) + "?polygons_only=1"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['features'], 5)

    def test_sublayer_api_rendered(self):
        for layer in SubfieldLayer.objects.all():
            url = reverse('api_layer_stub_view', kwargs={'pk': layer.pk})
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()), 1)

    def test_get_layer_parameters(self):
        subfieldlayer = SubfieldLayer.objects.latest('pk')
        self.assertEqual(len(subfieldlayer.dbf_field_names['field_names']), 11)
        url = reverse("api_layer_parameters", kwargs={'pk': subfieldlayer.id})
        response = self.client.get(url)
        self.assertEqual(len(response.json()['field_names']), 11)

    def test_retrieve_edit_delete_sublayers(self):
        subfieldlayer = SubfieldLayer.objects.latest('pk')
        url = reverse("api_subfield_edit", kwargs={'pk': subfieldlayer.id})
        response = self.client.get(url)
        self.assertEqual(response.json()['features'], 5)

        ## Update
        data = {
            "geoms": 0,
            "name": "Risk Calculation",
            "field": subfieldlayer.field.id
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['name'], "Risk Calculation")

        ## Destroy
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

    def test_retrieve_raster(self):
        file = open("./clients/tests/Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip", "rb")
        self.handler = ZippedShapeFile(file, self.field)
        self.assertTrue(self.handler.valid_zip())
        res = self.handler.attach_to_field(
            shapefile='Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly',
            parameter='K_ppm_')
        self.assertEqual(len(res), 1)
        sf = res[0][0]
        from django.contrib.gis.geos import MultiPolygon
        sf.field.boundary = MultiPolygon(sf.geoms.convex_hull)
        sf.field.save()
        self.assertEqual(sf.name, "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly")
        self.assertEqual(sf.features, 8)
        ## Create new subfieldlayer
        small_sf = SubfieldLayer.objects.create(field=sf.field,
                                                geoms=GeometryCollection(MultiPolygon(sf.geoms.convex_hull)))
        small_sf.dbf_field_names["field_names"] = ["Serial"]
        small_sf.dbf_field_values["Serial"] = [1]
        gdf = small_sf.make_geodataframe()
        self.assertEqual(gdf.shape, (1, 2))
        png = sf.make_raster(None, 'K_ppm_')
        import os
        self.assertTrue(os.path.exists(png.file.path))

        url = reverse("api_raster", kwargs={'pk': sf.id, 'parameter': 'K_ppm_'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        os.remove(png.file.path)

    def test_post_zipfile(self):
        field = Field.objects.latest('pk')
        filename = "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip"
        file = open(f"./clients/tests/{filename}", "rb")

        url = reverse("api_post_zipfile", kwargs={'field': field.pk})

        headers = {
            f'HTTP_CONTENT_DISPOSITION': f'attachment; filename={filename}'
        }
        resp = self.client.put(url, data={'file': file}, **headers)
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.json()['title'], "Shapefile Upload")

        url = reverse("api_post_zipfile", kwargs={'field': field.pk}) + "?process_type=Boundary%20Upload"

        headers = {
            f'HTTP_CONTENT_DISPOSITION': f'attachment; filename={filename}'
        }
        resp = self.client.put(url, data={'file': file}, **headers)
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.json()['title'], "Boundary Upload")

        url = reverse("api_post_zipfile", kwargs={'field': field.pk}) + "?process_type=As Applied"

        headers = {
            f'HTTP_CONTENT_DISPOSITION': f'attachment; filename={filename}'
        }
        resp = self.client.put(url, data={'file': file}, **headers)
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.json()['title'], "As Applied")

    def test_duplicate_subfield(self):
        field = Field.objects.latest('pk')
        filename = "Brian Hora_Hora Home West_Hora Home West_2019_NO Product_1_poly.zip"
        file = open(f"./clients/tests/{filename}", "rb")

        url = reverse("api_post_zipfile", kwargs={'field': field.pk})

        headers = {
            f'HTTP_CONTENT_DISPOSITION': f'attachment; filename={filename}'
        }
        resp = self.client.put(url, data={'file': file}, **headers)
        self.assertEqual(resp.status_code, 201)

        sf = field.layer.latest('pk')

        url = reverse('api_subfield_duplicate', kwargs={'pk': sf.pk})

        resp = self.client.post(f'{url}?name=duplicate&parameters=nccpi3corn,nccpi3soy')
        self.assertEqual(resp.status_code, 201)

        newsf = field.layer.latest('pk')
        self.assertEqual(newsf.name, 'duplicate')

        self.assertEqual(sf.features, newsf.features)
        self.assertIn('nccpi3corn', newsf.dbf_field_names.get('field_names'))
        self.assertIn('nccpi3soy', newsf.dbf_field_values.keys())

    def test_add_feature(self):
        field = Field.objects.latest('pk')
        lyr = SubfieldLayer.objects.create(field=field,
                                           name="Test Layer",
                                           geoms=GeometryCollection(list(field.boundary)),
                                           dbf_field_names={'field_names': list()})
        url = reverse("api_subfield_addfeature", kwargs={'pk': lyr.pk})
        data = {
            "area": [4.9, 5.0, 6.0],
            "custom": [{'red': 1}, {'blue': (1, 2)}, {'green': [1, 2, 3]}]
        }

        resp = self.client.put(url, data=data)
        self.assertEqual(resp.status_code, 404)

        lyr.editable = True
        lyr.save()

        resp = self.client.put(url, data=data)
        self.assertEqual(resp.status_code, 200)

        url = reverse("api_layer_geojson", kwargs={'pk': lyr.pk, 'parameter': 'custom'})
        resp = self.client.get(url)
        # print(resp.json())
        self.assertEqual(resp.status_code, 200)
