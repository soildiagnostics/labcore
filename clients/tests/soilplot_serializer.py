from django.core import serializers
from django.core.serializers import sort_dependencies

from products.models import Product, Category
from samples.models import LabMeasurement, Sample
from orders.models import Order, OrderStatus, OrderStatusType

app_list = {}

order = Order.objects.filter(id__in=[40305, 15630])
app_list['Order'] = order
app_list['status'] = OrderStatus.objects.all()
app_list['status_type'] = OrderStatusType.objects.all()
app_list['products'] = Product.objects.all()
app_list['categories'] = Category.objects.all()
app_list['Samples'] = Sample.objects.filter(order__in=order)
lm = LabMeasurement.objects.filter(sample__order__in=order)
app_list["LabMeasurements"] = lm
app_list['parameters'] = set([l.parameter for l in lm])
app_list['measurements'] = set([p.unit for p in app_list['parameters']])
app_list['field'] = [order[0].field]
app_list['farm'] = [order[0].field.farm]
app_list['client'] = [order[0].field.farm.client]
app_list['orgusers'] = [order[0].created_by, order[0].field.farm.client.originator, order[0].sampler]
app_list['orgs'] = {order[0].created_by.organization,
                    order[0].sampler.organization,
                    order[0].field.farm.client.originator.organization,
                    order[0].lab_processor}

# The sort_dependencies will ensure that the models are sorted so that
# those with foreign keys are taken care. If SecondModel has a fk to FirstModel,
# then sort_dependencies will take care of the ordering in the json file so that
# FirstModel comes first in the fixture thus preventing ambiguity when reloading
data = serializers.serialize("json", sort_dependencies(app_list.items()))
f = open('output.json', 'w')
f.write(data)
f.close()
