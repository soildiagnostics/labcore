import json

from django.contrib.gis.geos import Point
from django.test import TestCase, LiveServerTestCase
# from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from clients.models import Field, Client
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Person


class ClientViewTests(LiveServerTestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = APIClient()
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        # self.client.force_authenticate(user=self.user)
        self.field = ClientsTestCase.createField(ClientsTestCase())

    def test_get_client_list_by_dealer(self):
        url = reverse("api_clients_list")

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], str(self.field.farm.client.contact))
        self.assertEqual(resp_json[0]['farms'][0]['name'], self.field.farm.name)
        self.assertEqual(resp_json[0]['farms'][0]['fields'][0]['name'], self.field.name)

    def test_client_stats(self):
        url = reverse("api_client_stats")
        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(resp_json['avg_client_acres'], 76.6)


    def test_search_client_list_by_dealer(self):
        url = reverse("api_clients_list") + "?search=Jones"

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 0)

        url = reverse("api_clients_list") + "?search=Bha"

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], str(self.field.farm.client.contact))
        self.assertEqual(resp_json[0]['farms'][0]['name'], self.field.farm.name)
        self.assertEqual(resp_json[0]['farms'][0]['fields'][0]['name'], self.field.name)

        url = reverse("api_clients_list") + "?search=Kaust"

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], str(self.field.farm.client.contact))
        self.assertEqual(resp_json[0]['farms'][0]['name'], self.field.farm.name)
        self.assertEqual(resp_json[0]['farms'][0]['fields'][0]['name'], self.field.name)

    def test_get_farm_list_by_client(self):
        url = reverse("api_client_farms", kwargs={'client': self.field.farm.client_id})

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], self.field.farm.name)
        self.assertEqual(resp_json[0]['fields'][0]['name'], self.field.name)

    def test_get_field_list_by_farm(self):
        url = reverse("api_farm_fields", kwargs={'farm': self.field.farm_id})

        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], self.field.name)

    def test_get_client_list_by_client(self):
        url = reverse("api_clients_list")
        self.assertEqual(Field.objects.count(), 1)
        self.field2 = ClientsTestCase.createField(ClientsTestCase())
        self.assertEqual(Field.objects.count(), 2)
        self.assertNotEqual(self.field.farm.client, self.field2.farm.client)

        p = Person.objects.get(id=self.field.farm.client.contact.id)
        u = p.get_or_create_user_from_person()
        self.token = Token.objects.create(user=u)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.client.login()
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(resp_json), 1)
        self.assertEqual(resp_json[0]['name'], str(self.field.farm.client.contact))
        self.assertEqual(resp_json[0]['farms'][0]['name'], self.field.farm.name)
        self.assertEqual(resp_json[0]['farms'][0]['fields'][0]['name'], self.field.name)

    def test_get_field_list_by_point(self):
        lat = 39.33
        lon = -91.29
        url = f'{reverse("api_fields_by_location")}?lat={lat}&lon={lon}'
        response = self.client.get(url)
        resp_json = response.json()
        self.assertEqual(len(resp_json), 1)

    def test_create_client(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Brian Hora")

    def test_retrive_update_delete_client(self):
        data = {
            "name": "Brain",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Brain Hora")

        brian = Client.objects.latest('pk')
        url = reverse("api_edit_delete_client", kwargs={'pk': brian.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['name'], "Brain Hora")
        ## make an update
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['name'], "Brian Hora")
        self.assertEqual(Client.objects.get(id=content['id']), brian)
        self.assertEqual(Client.objects.get(id=content['id']).contact, brian.contact)

        # Delete client
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        with self.assertRaises(Client.DoesNotExist):
            Client.objects.get(id=brian.id)

    def test_create_farm(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")
        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

    def test_retrieve_update_destroy_farm(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")
        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        ## Retrieve
        url = reverse("api_edit_farm", kwargs={'pk': content.get('id')})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        ## Update
        data = {
            "name": "Mitchell's Farm",
        }

        url = reverse("api_edit_farm", kwargs={'pk': content.get('id')})
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's Farm")

        ## Delete
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

    def test_create_field(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")
        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        data = {
            "name": "Test Field",
            "boundary": "SRID=4326;MULTIPOLYGON (((-88.224559 40.102634, -88.224559 40.103107, -88.223905 40.103107, -88.223905 40.102634, -88.224559 40.102634)), ((-90.72009199999999 40.936089, -90.72009199999999 40.936327, -90.71994599999999 40.936327, -90.71994599999999 40.936089, -90.72009199999999 40.936089)), ((-90.82286999999999 39.805679, -90.82286999999999 39.806745, -90.82162599999999 39.806745, -90.82162599999999 39.805679, -90.82286999999999 39.805679)))",
            "projection": 2791,
            "state": "IL",
            "county": "",
            "section": "19",
            "twpnum": "T19N",
            "township": "",
            "intersection": "",
            "approx_area": "",
            "range": "R9E",
            "plat_name": "",
            "plat_file": "",
            "notes": "",
            "address": ""
        }

        url = reverse("api_farm_fields", kwargs={'farm': content['id']})
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Test Field")
        self.assertEqual(Field.objects.latest('pk').area, 3.9)

    def test_create_field_gj(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")

        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        data = {
            "name": "Test Field",
            "boundary": json.dumps({
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                -88.224559,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.102634
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.720092,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936089
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.82287,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.805679
                            ]
                        ]
                    ]
                ]
            }),
            "projection": 2791,
            "state": "IL",
            "county": "",
            "section": "19",
            "twpnum": "T19N",
            "township": "",
            "intersection": "",
            "approx_area": "",
            "range": "R9E",
            "plat_name": "",
            "notes": "",
            "address": ""
        }

        url = reverse("api_farm_fields", kwargs={'farm': content['id']})
        response = self.client.post(url, data=data, format='json')

        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Test Field")
        self.assertEqual(Field.objects.latest('pk').area, 3.9)


    def test_retrieve_update_destroy_field(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")
        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        data = {
            "name": "Test Field"
        }

        url = reverse("api_farm_fields", kwargs={'farm': content['id']})
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Test Field")
        new = Field.objects.latest('pk')
        self.assertEqual(new.area, 0)
        self.assertEqual(new.layer.count(), 0)
        # Retrieve
        url = reverse("api_edit_field", kwargs={'pk': content['id']})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Update
        data = {
            "name": "Test Field",
            "boundary": "SRID=4326;MULTIPOLYGON (((-88.224559 40.102634, -88.224559 40.103107, -88.223905 40.103107, -88.223905 40.102634, -88.224559 40.102634)), ((-90.72009199999999 40.936089, -90.72009199999999 40.936327, -90.71994599999999 40.936327, -90.71994599999999 40.936089, -90.72009199999999 40.936089)), ((-90.82286999999999 39.805679, -90.82286999999999 39.806745, -90.82162599999999 39.806745, -90.82162599999999 39.805679, -90.82286999999999 39.805679)))",
            "projection": 2791,
            "state": "IL",
            "county": "",
            "section": "19",
            "twpnum": "T19N",
            "township": "",
            "intersection": "",
            "approx_area": "",
            "range": "R9E",
            "plat_name": "",
            "plat_file": "",
            "notes": "",
            "address": "",
            "farm": content.get('farm')
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, 200)
        #make sure soil layers are created
        latest = Field.objects.latest('pk')
        self.assertEqual(latest.area, 3.9)
        self.assertEqual(latest.layer.filter(name__contains="Survey").count(), 1)

        ## Destroy
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)

    def test_retrieve_update_destroy_field_gj(self):
        data = {
            "name": "Brian",
            "middle_name": "",
            "last_name": "Hora",
            "is_company": False,
            "is_company_contact": False,
            "notes": "",
            "image": "",
            "url": "",
            "company": "",
            "user": ""
        }
        url = reverse("api_new_client")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        clt = Client.objects.latest("pk")
        content = response.json()
        self.assertEqual(content['id'], clt.id)
        self.assertEqual(content['name'], "Brian Hora")
        data = {
            "name": "Mitchell's",
        }

        url = reverse("api_client_farms", kwargs={'client': clt.id})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Mitchell's")

        data = {
            "name": "Test Field"
        }

        url = reverse("api_farm_fields", kwargs={'farm': content['id']})
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 201)
        content = response.json()
        self.assertEqual(content['name'], "Test Field")
        latest_field = Field.objects.latest('pk')
        self.assertEqual(latest_field.area, 0)
        self.assertEqual(latest_field.id, content['id'])

        sf = latest_field.layer.filter(name="Soil Survey Data")
        self.assertFalse(sf.exists())

        # Retrieve
        url = reverse("api_edit_field_geojson", kwargs={'pk': content['id']})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Update
        data = {
            #"name": "Test Field",
            "boundary": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                -88.224559,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.102634
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.720092,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936089
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.82287,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.805679
                            ]
                        ]
                    ]
                ]
            },
            #"srid": 4326,
            #"farm": content['farm']
            "notes": "test_notes"
        }
        response = self.client.patch(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        latest_field.refresh_from_db()
        self.assertEqual(latest_field.area, 3.9)
        self.assertEqual(latest_field.notes, "test_notes")

        ## Also make sure soil layers were created
        sf = latest_field.layer.filter(name="Soil Survey Data")
        self.assertTrue(sf.exists())
        self.assertNotEqual(sf.first().features, 0)


class HallPassTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientViewTests.setUp(self)

    def test_create_hall_pass(self):
        newuser = User.objects.create_user("new")
        url = reverse('api_create_hall_pass')

        field = Field.objects.last()

        cltuser = User.objects.create_user("client")
        field.farm.client.person = cltuser.person
        field.farm.client.save()

        self.client.force_login(cltuser)

        data = {
            'username': 'new',
            'field': field.pk,
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)

    def test_fail_create_hall_pass(self):
        url = reverse('api_create_hall_pass')

        field = Field.objects.last()

        cltuser = User.objects.create_user("client")
        field.farm.client.person = cltuser.person
        field.farm.client.save()

        self.client.force_login(cltuser)

        data = {
            'username': 'new',
            'field': field.pk,
        }

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 404)
