from datetime import timedelta

from django.contrib.gis.geos import GEOSGeometry
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now

from associates.models import Organization, OrganizationUser, Role
from clients.models import Client, Farm, Field, HallPass
from contact.models import Person, Company
from contact.models import User, EmailModel
from contact.tests.tests_models import ContactTestCase


class FieldViewPermissionsTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.superuser = User.objects.create_superuser('mysuperuser', 'myemail@test.com', 'password')
        self.staffuser = User.objects.create_user('mystaffuser')
        self.staffuser.is_staff = True
        self.staffuser.is_active = True
        self.staffuser.save()

        cprimary = Company.objects.create(name="Primary company")
        b, created = Organization.objects.create_dealership(
            company=cprimary
        )
        b.roles.add(Role.objects.get(role="Primary"))

        p = Person.objects.get(user=self.superuser)
        p.company = cprimary
        p.is_company_contact = True
        p.save()

        p = Person.objects.get(user=self.staffuser)
        p.company = cprimary
        p.save()
        l = b.add_company_members_to_organization()
        self.assertEqual(len(l), 2)

        ## Create two companies

        ## Company 1
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)
        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        e = EmailModel.objects.create(email="test@example.com")
        e.contact = m
        e.save()
        m.save()
        ### Create clients farms fields for each company
        self.orguser1 = OrganizationUser.objects.get(user__username="testinguser")
        self.client1 = Client.objects.create(contact=m,
                                             originator=self.orguser1)
        self.farm1 = Farm.objects.create(name="Farm1", client=self.client1)
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.field1 = Field.objects.create(name="Field1",
                                           farm=self.farm1,
                                           boundary=mp)

        ## For company 2
        c2 = Company.objects.create(name="Another company")
        b, created = Organization.objects.create_dealership(
            company=c2
        )
        u2 = User.objects.create_user(username="seconduser")
        p = Person.objects.get(user=u2)
        p.company = c2
        p.is_company_contact = True
        p.save()
        l = b.add_company_members_to_organization()
        self.assertEqual(len(l), 1)

        ### Create clients farms fields for each company
        self.orguser2 = OrganizationUser.objects.get(user__username="seconduser")
        m2 = Person.objects.create(name="Jessie",
                                   last_name="Bhalerao")
        self.client2 = Client.objects.create(contact=m2,
                                             originator=self.orguser2)
        self.farm2 = Farm.objects.create(name="Farm2", client=self.client2)
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.field2 = Field.objects.create(name="Field2",
                                           farm=self.farm2,
                                           boundary=mp)

        self.client1pages = [
            reverse('client_detail', kwargs={'pk': self.client1.pk}),
            reverse('farm_delete', kwargs={'pk': self.farm1.pk}),
            reverse('field_detail', kwargs={'pk': self.field1.pk}),
            reverse('field_geo_details', kwargs={'pk': self.field1.pk})
        ]

        self.client2pages = [
            reverse('client_detail', kwargs={'pk': self.client2.pk}),
            reverse('farm_delete', kwargs={'pk': self.farm2.pk}),
            reverse('field_detail', kwargs={'pk': self.field2.pk}),
            reverse('field_geo_details', kwargs={'pk': self.field2.pk})
        ]



    def test_views_by_staffuser(self):
        tc = TestClient()
        tc.force_login(self.staffuser)

        for view in self.client1pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

    def test_views_by_orguser(self):
        tc = TestClient()
        tc.force_login(self.orguser1.user)

        for view in self.client1pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

        tc.force_login(self.orguser2.user)

        for view in self.client2pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

    def test_views_by_orguser_multiple(self):
        tc = TestClient()
        tc.force_login(self.orguser1.user)

        for view in self.client1pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

        neworguser = OrganizationUser.objects.create(user=self.orguser1.user,
                                                     organization=self.orguser2.organization)
        tc.force_login(neworguser.user)

        for view in self.client1pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

    def test_views_unreachable_by_wrong_org(self):
        tc = TestClient()
        tc.force_login(self.orguser1.user)

        for view in self.client2pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

        tc.force_login(self.orguser2.user)

        for view in self.client1pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

    def test_create_hallpass_for_orguser(self):

        # allow orguser1 to view field2
        hp = HallPass.objects.create(field=self.field2, user=self.orguser1.user, created_by=self.staffuser)

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        for view in self.client2pages[0:2]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

        for view in self.client2pages[2:]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

        hp.delete()
        for view in self.client2pages[2:]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

    def test_create_hallpass_for_another_client(self):

        # allow client1 to view field2
        ## First we create a user for the contact.
        user = self.client1.contact.get_or_create_user_from_person(username='client1', is_active=True)

        hp = HallPass.objects.create(field=self.field2, user=user, created_by=self.staffuser)

        tc = TestClient()
        tc.force_login(user)

        for view in self.client2pages[0:2]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

        for view in self.client2pages[2:]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

        hp.delete()
        for view in self.client2pages[2:]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

    def test_company_client_views(self):

        # Create company as client
        self.client3 = Client.objects.create(contact=Company.objects.get(name="Another company"),
                                             originator=self.orguser2)
        self.farm3 = Farm.objects.create(name="Farm3", client=self.client3)
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.field3 = Field.objects.create(name="Field3",
                                           farm=self.farm3,
                                           boundary=mp)
        self.client3pages = [
            reverse('client_detail', kwargs={'pk': self.client3.pk}),
            reverse('farm_delete', kwargs={'pk': self.farm3.pk}),
            reverse('field_detail', kwargs={'pk': self.field3.pk}),
            reverse('field_geo_details', kwargs={'pk': self.field3.pk})
        ]

        tc = TestClient()
        tc.force_login(self.orguser2.user)

        for view in self.client3pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

    def test_expired_hallpass_for_orguser(self):

        # allow orguser1 to view field2
        hp = HallPass.objects.create(field=self.field2, user=self.orguser1.user, created_by=self.staffuser,
                                     expiry=now()-timedelta(days=1))

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        for view in self.client2pages:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)




