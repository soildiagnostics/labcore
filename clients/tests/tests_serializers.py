import json
from datetime import timedelta

from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from clients.models import Field, SubfieldLayer
from clients.tests.tests_models import ClientsTestCase
from contact.models import User
from fieldcalendar.models import FieldEvent, CalendarEventKind, CalendarEvent
from clients.serializers import FieldWithEventsSerializer, SubfieldSerializer
from formulas.models import FormulaVariable, Formula
from products.models import MeasurementUnit, TestParameter
from samples.tests.tests_calculations import CalculationsTestCase, LabMeasurement, Sample


class FieldBirdsEyeSerializerTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        kind, _ = CalendarEventKind.objects.get_or_create(name="Planting")
        c, _ = CalendarEvent.objects.get_or_create(kind=kind, name="Corn")

    def test_get_events(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        details = {
            "_id": "_fc8",
            "color": "green",
            "start": now().isoformat(),
            "end": now().isoformat(),
            "title": "Corn",
            "allDay": True,
            "editable": False,
            "field_id": f.pk,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        fe = FieldEvent.objects.create_system_event(field=f, title="System Event", start=now(), end=now())
        serdat = FieldWithEventsSerializer(f).data
        self.assertEqual(len(serdat['events']), 1)
        self.assertEqual(json.loads(serdat['boundary'])["type"], "MultiPolygon")


class SubfieldLayerWithFormulaSerializerTest(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        CalculationsTestCase.setUp(self)

        self.user = User.objects.get(username="testinguser")
        self.client = APIClient()
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        # self.client.force_authenticate(user=self.user)
        self.field = ClientsTestCase.createField(ClientsTestCase())

        # Retrieve
        url = reverse("api_edit_field_geojson", kwargs={'pk': self.field.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = response.json()

        # Update
        data = {
            "name": "Test Field",
            "boundary": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                -88.224559,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.103107
                            ],
                            [
                                -88.223905,
                                40.102634
                            ],
                            [
                                -88.224559,
                                40.102634
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.720092,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936327
                            ],
                            [
                                -90.719946,
                                40.936089
                            ],
                            [
                                -90.720092,
                                40.936089
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                -90.82287,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.806745
                            ],
                            [
                                -90.821626,
                                39.805679
                            ],
                            [
                                -90.82287,
                                39.805679
                            ]
                        ]
                    ]
                ]
            },
            "srid": 4326,
            "farm": content['farm']
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Field.objects.latest('pk').area, 3.9)

        self.nullunit, _ = MeasurementUnit.objects.get_or_create(unit="null")
        pcbs = MeasurementUnit.objects.create(unit="% base saturation")
        input_par = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        self.input = FormulaVariable.objects.create(test_parameter=input_par)
        output_par = TestParameter.objects.create(name="K", unit=pcbs)
        self.output = FormulaVariable.objects.create(test_parameter=output_par)

        self.formula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=self.output,
            code="sqrt(x[0]/2)"
        )
        self.formula.inputs.add(self.input)

        l = LabMeasurement(sample=Sample.objects.last(),
                           parameter=input_par,
                           value=400)
        l.save(orguser=Sample.objects.last().order.created_by)

        self.sf = SubfieldLayer.objects.first()
        self.sf.dbf_field_names.get('field_names').append("K")
        self.sf.save()
        self.sf.refresh_from_db()

    def test_formula_findable(self):
        vars = FormulaVariable.objects.filter(test_parameter__name__in=self.sf.dbf_field_names.get('field_names'))
        self.assertEqual(vars.count(), 2)
        formulas = Formula.objects.filter(inputs__in=vars).distinct()
        self.assertEqual(formulas.count(), 1)

    def test_formula_in_serializer(self):

        ser = SubfieldSerializer(self.sf).data
        self.assertEqual(len(list(ser['formulas'].keys())), 1)
