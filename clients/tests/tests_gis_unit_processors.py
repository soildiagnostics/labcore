from django.core.files import File
from django.test import SimpleTestCase
from clients.gis.unit_processors import managed_tempfolder, tempfile_copy, gis_operator, file_copier, gdal_translate
import os


class UnitProcTestCase(SimpleTestCase):

    def setUp(self):
        self.files = os.listdir("/tmp/")

    def test_managed_directory(self):
        with managed_tempfolder() as folder:
            self.assertTrue(os.path.isdir(folder))
            with open(f"{folder}/tempfile.txt", "w") as wfile:
                wfile.write("Test")
            with open(f"{folder}/tempfile.txt", "r") as rfile:
                self.assertEqual(rfile.read(), "Test")
        self.assertFalse(os.path.isdir(folder))

    def test_managed_directory_exception(self):
        try:
            with managed_tempfolder() as folder:
                self.assertTrue(os.path.isdir(folder))
                with open(f"{folder}/tempfile.txt", "w") as wfile:
                    wfile.write("Test")
                    raise AssertionError
        except AssertionError:
            pass
        self.assertFalse(os.path.isdir(folder))

    def test_managed_copy(self):
        with open("clients/tests/gardencity.zip", "rb") as mediafile:
            djFile = File(mediafile)
            with tempfile_copy(djFile) as copy:
                self.assertEqual(os.path.getsize("clients/tests/gardencity.zip"), os.path.getsize(copy))
                self.assertEqual(os.path.basename(copy), "gardencity.zip")
            self.assertFalse(os.path.exists(copy))

    def test_managed_copy_exception(self):
        try:
            with open("clients/tests/gardencity.zip", "rb") as mediafile:
                djFile = File(mediafile)
                with tempfile_copy(djFile) as copy:
                    self.assertEqual(os.path.getsize("clients/tests/gardencity.zip"), os.path.getsize(copy))
                    self.assertEqual(os.path.basename(copy), "gardencity.zip")
                    raise AssertionError
        except AssertionError:
            pass
        self.assertFalse(os.path.exists(copy))

    def test_file_copier(self):
        with file_copier("clients/tests/gardencity.zip") as fc:
            self.assertEqual(os.path.getsize("clients/tests/gardencity.zip"), os.path.getsize(fc.output))
        self.assertFalse(os.path.exists(fc.output))

    def test_gis_operator(self):

        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            'q': ""
        }
        destfile = "scaled.tif"
        with gis_operator(cmd, src=inputfile, dst=destfile, **options) as translator:
            res = translator.call()
            outfile = translator.output
            self.assertEqual(res.output, outfile)
        self.assertFalse(os.path.exists(outfile))

    def test_gis_operator_anonymous_dest(self):

        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            'q': ""
        }
        with self.assertRaises(AssertionError), \
             gis_operator(cmd, inputfile, **options) as translator:
            res = translator.call()
            outfile = translator.output
            self.assertEqual(res, outfile)
            self.assertFalse(os.path.exists(outfile))

        options = {
            "ot": "Byte",
            "b": 1,
            'q': "",
            "of": "GTiff"
        }
        with gis_operator(cmd, src=inputfile, **options) as translator:
            res = translator.call()
            outfile = translator.output
            self.assertEqual(res.output, outfile)
        self.assertFalse(os.path.exists(outfile))

    def test_deferred_input_call(self):
        class MockFile(object):
            def __init__(self, file):
                self.output = file
                self.failed = False

        inputfile = MockFile("clients/tests/hora_ndvi.tif")
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            'q': "",
            "of": "GTiff"
        }
        with self.assertRaises(AssertionError):
            with gis_operator(cmd, **options) as translator:
                res = translator.call()
                outfile = translator.output
                self.assertEqual(res, outfile)
            self.assertFalse(os.path.exists(outfile))

        with gis_operator(cmd, **options) as translator:
            res = translator.deferred_input_call(inputfile)
            result = translator.output
            self.assertEqual(res.output, result)
        self.assertFalse(os.path.exists(result))

    def test_rshift_override(self):
        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }
        options2 = {
            "ot": "UInt16",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }

        with gis_operator(cmd, src=inputfile, **options) as translate_to_Byte:
            with gis_operator(cmd, **options2) as curried_translate_to_UInt16:
                outfile = translate_to_Byte >> curried_translate_to_UInt16

        self.assertFalse(os.path.exists(outfile.output))

    def test_rshift_with_downloader(self):

        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }

        with file_copier(inputfile) as fop:
            with gis_operator(cmd, **options) as translate_to_Byte:
                outfile = fop >> translate_to_Byte
        self.assertFalse(os.path.exists(outfile.output))

    def test_rshift_with_downloader_3ops(self):

        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }
        options2 = {
            "ot": "UInt16",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }
        with file_copier(inputfile) as fop, \
                gis_operator(cmd, **options) as translate_to_Byte, \
                gis_operator(cmd, **options2) as curried_translate_to_UInt16:
            ## WooHoo!
            outfile1 = fop >> translate_to_Byte >> curried_translate_to_UInt16
            outfile2 = fop >> curried_translate_to_UInt16 >> translate_to_Byte >> curried_translate_to_UInt16

        self.assertFalse(os.path.exists(outfile1.output))
        self.assertFalse(os.path.exists(outfile2.output))

    def test_rshift_with_failure(self):

        inputfile = "clients/tests/hora_ndvi.tif"
        cmd = "gdal_translate"
        options = {
            "ot": "Byte",
            "b": 1,
            "of": "Gobbbledegook",
            "q": ""
        }
        options2 = {
            "ot": "UInt16",
            "b": 1,
            "of": "GTiff",
            "q": ""
        }
        with file_copier(inputfile) as fop, \
                gis_operator(cmd, **options) as translate_to_Byte, \
                gis_operator(cmd, **options2) as curried_translate_to_UInt16:
            outfile1 = fop >> translate_to_Byte >> curried_translate_to_UInt16
            self.assertTrue(outfile1.failed)

    def test_closures(self):
        inputfile = "clients/tests/hora_ndvi.tif"
        options = {
            "ot": "Byte",
            "b": 1,
            'q': ""
        }
        destfile = "scaled.tif"
        with gdal_translate(src=inputfile, dst=destfile, **options) as translator:
            res = translator.call()
            outfile = translator.output
            self.assertEqual(res.output, outfile)
        self.assertFalse(os.path.exists(outfile))


    def tearDown(self):
        files = os.listdir("/tmp/")
        self.assertEqual(len(files), len(self.files))
