import os
import shutil
import tempfile

from django.test import TestCase

from clients.gis.gishelper import SDXShapefile
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.models import SubfieldLayer
from clients.tests.tests_models import ClientsTestCase
from django.core import serializers

from orders.models import Order


class SoilMapTest(TestCase):
    fixtures = ['roles']

    def setUp(self):

        self.file = open("./clients/tests/GarysEast2018.zip", 'rb')

        ClientsTestCase().setUp()
        self.field = ClientsTestCase().createField()

        for obj in serializers.deserialize('json', open('output.json').read()):
            obj.save()

    def test_samples_loaded(self):
        order = Order.objects.get(id=40305)
        self.assertEqual(order.samples.count(), 36)