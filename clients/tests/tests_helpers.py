from django.contrib.gis.geos import GEOSGeometry

from associates.models import Organization, OrganizationUser
from clients.models import Client, Farm, Field
from contact.models import Person, EmailModel, PhoneModel, Company, User


def createTestOrgUser(existing_user = None):
    try:
        ou = OrganizationUser.objects.get(user=existing_user)
        return ou
    except:
        pass
    phone = PhoneModel.objects.create(phone_number="+12177216032")
    company = Company.objects.create(name="SkyData Technologies LLC")
    user, _ = User.objects.get_or_create(username="testinguser",
                        first_name="Testing",
                        last_name="User",
                        email="user@test.com",
                        password="nothingsecret",
                        is_active=True)


    org, created = Organization.objects.create_dealership(
        company=company)
    p = Person.objects.get(user_id=user.id)
    p.company = company
    p.phone.add(phone)
    p.is_company_contact = True
    p.save()
    l = org.add_company_members_to_organization()
    return OrganizationUser.objects.get(user=user)


def createClient(for_org_user=None, existing_user=None):
    m = Person.objects.create(name="Kaustubh",
                              last_name="Bhalerao")
    e = EmailModel.objects.create(email="test@example.com")
    e.contact = m
    e.save()
    m.save()

    if not for_org_user:
        for_org_user = createTestOrgUser(existing_user=existing_user)

    # Create client for contact
    return Client.objects.create(contact=m,
                                 originator=for_org_user)


def createFarm(for_org_user=None, existing_user=None):
    client = createClient(for_org_user, existing_user)
    return Farm.objects.create(name="Test farm",
                               client=client)


def createField(for_org_user=None, existing_user=None):
    farm = createFarm(for_org_user, existing_user)
    mp = GEOSGeometry(
        'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
    boundary = mp
    return Field.objects.create(name="Test field",
                                farm=farm,
                                boundary=mp)