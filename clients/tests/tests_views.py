import datetime
import json
from unittest import skip

from django.db.models import ProtectedError
from django.test import Client as TestClient
from django.test import TestCase
# from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.timezone import now

from associates.models import Organization, OrganizationUser, Role
from clients.models import Client, Field, SubfieldLayer, Farm
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company, Person, User
from contact.models import PhoneModel
from fieldcalendar.models import CalendarEvent

## AsyncIO tests
import pytest
from channels.testing import ApplicationCommunicator
from clients.consumers import ShapefileProcessConsumer


class ClientViewTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase().createField()

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)

    def test_client_list_view_no_dealership(self):

        # ClientsTestCase.createClient(self)
        client = ClientsTestCase.createClient(self)
        c = TestClient()
        p = c.get(reverse('client_list'))
        self.assertEqual(p.status_code, 302)

    def test_client_list_view_as_dealer(self):
        PhoneModel.objects.create(phone_number="+12177216032")
        c, created = Company.objects.get_or_create(name="SkyData Technologies LLC")
        a, created = Organization.objects.create_dealership(company=c)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        a.add_company_members_to_organization()

        self.assertEqual(a.is_dealer, True)
        resp = self.client.get(reverse('client_list'))
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(a.is_dealer, True)
        resp = self.client.get(reverse('client_list_json'))
        self.assertEqual(resp.status_code, 200)
        resp_json = resp.json()
        self.assertEqual(resp_json['recordsTotal'], 1)

    def test_client_views(self):
        u = User.objects.get(username="testinguser")
        c = TestClient()
        view_list = ["client_list",
                     "client_add",
                     "client_list_json",
                     ]
        for vu in view_list:
            p = c.get(reverse(vu))
            self.assertEqual(p.status_code, 302)

        c.force_login(u)
        for vu in view_list:
            p = c.get(reverse(vu))
            self.assertEqual(p.status_code, 200)

        json_search = "/clients/data/?draw=3&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=contact&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=originator&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=1&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=j&search%5Bregex%5D=false&_=1536541924118"
        p = c.get(json_search)
        self.assertEqual(p.status_code, 200)

    def test_client_individual_add_post_view(self):

        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-987-6543",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)

        self.assertTrue(resp.status_code, 302)
        new_client = Client.objects.get(contact__name=post_data['name'])
        self.assertEqual(new_client.contact.last_name, post_data['last_name'])
        self.assertEqual(new_client.contact.email.first().email, post_data['email-0-email'])
        self.assertEqual(new_client.contact.phone.first().phone_number, post_data['phone-0-phone_number'])

        ## test if this created a new person:
        self.assertIsNotNone(Person.objects.get(id=new_client.contact.id))

    def test_client_individual_add_post_view_continue(self):

        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-987-6543",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
            "_continue": True
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)
        new_client = Client.objects.get(contact__name=post_data['name'])
        self.assertTrue(resp.status_code, 302)
        self.assertEqual(resp.url, reverse("client_update", args=[new_client.contact.pk]))

        self.assertEqual(new_client.contact.last_name, post_data['last_name'])
        self.assertEqual(new_client.contact.email.first().email, post_data['email-0-email'])
        self.assertEqual(new_client.contact.phone.first().phone_number, post_data['phone-0-phone_number'])

    def test_client_individual_with_company_add_post_view(self):

        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-987-6543",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
            "company_name": "SoilDX",
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)

        self.assertTrue(resp.status_code, 302)
        new_client = Client.objects.get(contact__name=post_data['name'])
        self.assertEqual(new_client.contact.last_name, post_data['last_name'])
        self.assertEqual(new_client.contact.email.first().email, post_data['email-0-email'])
        self.assertEqual(new_client.contact.phone.first().phone_number, post_data['phone-0-phone_number'])
        self.assertEqual(new_client.contact.company.name, post_data["company_name"])
        self.assertEqual(new_client.originator.organization, self.org)

    def test_client_company_add_post_view(self):

        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "C",
            "company_name": "SoilDX",
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)

        self.assertEqual(resp.status_code, 302)
        new_client = Client.objects.get(contact__name="SoilDX")
        self.assertEqual(new_client.contact.name, post_data['company_name'])
        self.assertTrue(new_client.contact.is_company)
        # check company contact person
        cp = Person.objects.get(name=post_data["name"])
        self.assertEqual(cp.company.name, post_data['company_name'])
        self.assertTrue(cp.is_company_contact)

        # post with no company contact
        post_data.pop("name")
        post_data.pop("last_name")
        post_data.update({"company_name": "AnotherCO"})
        resp = self.client.post(reverse("client_add"), data=post_data)
        self.assertEqual(resp.status_code, 302)
        co_client = Client.objects.get(contact__name="AnotherCO")
        self.assertEqual(co_client.contact.name, post_data['company_name'])
        self.assertTrue(co_client.contact.is_company)
        co = Company.objects.get(id=co_client.contact.id)
        self.assertEqual(co.contact_set.count(), 0)

    def test_client_update_individual_view(self):

        existing_client = self.field.farm.client
        response = self.client.get(reverse("client_update", kwargs={'pk': existing_client.contact.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(existing_client.contact.is_company)
        self.assertEqual(existing_client.contact.name, "Kaustubh")
        self.assertEqual(existing_client.contact.email.first().email, "test@example.com")
        self.assertEqual(existing_client.contact.phone.count(), 0)

        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "1",
            "phone-0-phone_number": "217-555-1212",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newclient@update.com",
            "email-0-email_type": "O",
            "email-0-contact": existing_client.contact.id,
            "email-0-id": existing_client.contact.email.first().id,
            "email-0-primary": True,
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
        }

        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        self.assertTrue(response.status_code, 302)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.name, "NewClient-Update")
        self.assertIsNone(existing_client.contact.company)
        self.assertEqual(existing_client.contact.email.last().email, "newclient@update.com")
        self.assertEqual(existing_client.contact.email.count(), 1)
        self.assertEqual(existing_client.contact.phone.count(), 1)

        post_data["phone-INITIAL_FORMS"] = "1"
        post_data["phone-0-id"] = existing_client.contact.phone.first().id
        post_data["phone-0-contact"] = existing_client.contact.id

        # add company name
        post_data["company_name"] = "SoilDX"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.company.name, "SoilDX")

        # update company
        post_data["company_name"] = "SoilDX-V2"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.company.name, "SoilDX-V2")

        # delete company
        post_data.pop("company_name")
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertIsNone(existing_client.contact.company)

        # change to company - keeping contact person
        post_data["company_name"] = "SoilDX-V2"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.company.name, "SoilDX-V2")
        self.assertFalse(existing_client.contact.is_company)
        orig_person = Person.objects.get(id=existing_client.contact.id)
        cp_user = orig_person.get_or_create_user_from_person()
        existing_client.refresh_from_db()
        self.assertIsNotNone(existing_client.contact.user)

        post_data["client_type"] = "C"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.name, "SoilDX-V2")
        self.assertTrue(existing_client.is_company)

        # be sure user is retained and data transferred to the new Person record
        new_person = Person.objects.get(name="NewClient-Update")
        self.assertEqual(new_person.user, cp_user)
        self.assertIsNone(existing_client.contact.user)
        self.assertIsNone(existing_client.contact.company)
        orig_person.refresh_from_db()
        self.assertEqual(orig_person.id, existing_client.contact.id)
        self.assertNotEqual(orig_person.id, new_person.id)
        self.assertFalse(orig_person.is_company_contact)
        self.assertTrue(new_person.is_company_contact)
        self.assertEqual(new_person.email.last().email, "newclient@update.com")
        self.assertEqual(new_person.email.count(), 1)
        self.assertEqual(new_person.phone.count(), 1)

        # if changed to company with no individual, disable the contact's user account

        # TODO Not sure if this is a good test.
        post_data["client_type"] = "I"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertEqual(existing_client.contact.name, "NewClient-Update")
        self.assertFalse(existing_client.is_company)
        person = Person.objects.get(id=existing_client.contact.id)

        post_data.pop("name")
        post_data.pop("last_name")
        post_data["client_type"] = "C"
        post_data["company_contact"] = existing_client.contact.id
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertTrue(existing_client.is_company)
        self.assertRaises(TypeError, person.user)

    def test_client_update_company_view(self):

        existing_client = self.field.farm.client
        response = self.client.get(reverse("client_update", kwargs={'pk': existing_client.contact.pk}))
        self.assertEqual(response.status_code, 200)

        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "C",
            "company_name": "SoilDX-V2",
        }

        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        self.assertTrue(response.status_code, 302)
        existing_client.refresh_from_db()
        self.assertTrue(existing_client.contact.is_company)
        self.assertIsNone(existing_client.contact.company)
        self.assertEqual(existing_client.contact.name, "SoilDX-V2")

        # update company contact
        post_data["name"] = "Jane"
        post_data["last_name"] = "Doe"
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertIsNone(existing_client.contact.company)
        self.assertEqual(existing_client.contact.name, "SoilDX-V2")
        co_person = Person.objects.get(last_name="Doe", company=existing_client.contact)
        self.assertTrue(co_person.is_company_contact)

        # remove this person, a valid user
        janeuser = co_person.get_or_create_user_from_person()

        post_data.pop("name")
        post_data.pop("last_name")
        post_data['company_contact'] = co_person.id
        self.assertEqual(Person.objects.filter(company=existing_client.contact).count(), 2)
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        self.assertEqual(response.status_code, 302)
        existing_client.refresh_from_db()
        co_person.refresh_from_db()
        self.assertEqual(Person.objects.filter(company=existing_client.contact).count(), 2)
        self.assertFalse(co_person.is_company_contact)

        # remove this person, when not a user
        co_person.user = None
        co_person.save()
        janeuser.delete()
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        self.assertEqual(Person.objects.filter(company=existing_client.contact).count(), 1)

        # start as company and remove company and move to individual
        post_data.update({"client_type": "I",
                          "name": "NewClient-Update",
                          "last_name": "Bhalerao",
                          })
        post_data.pop("company_name")
        post_data.pop("company_contact")
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertFalse(existing_client.contact.is_company)
        self.assertEqual(existing_client.contact.name, "NewClient-Update")
        self.assertFalse(existing_client.contact.is_company_contact)
        self.assertIsNone(existing_client.contact.company)

        # start as company and keep company and move to individual
        post_data.update({"client_type": "C", "company_name": "Keep Business"})
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertTrue(existing_client.contact.is_company)
        post_data.update({"client_type": "I",
                          "name": "Individual",
                          "last_name": "Bhalerao",
                          "company_name": "Keep Business",
                          })
        existing_co = existing_client.contact
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertFalse(existing_client.contact.is_company)
        self.assertEqual(existing_client.contact.name, "Individual")
        self.assertEqual(existing_client.contact.company.name, "Keep Business")
        self.assertTrue(existing_client.contact.is_company_contact)

        # change to individual with no company -- had company contact
        post_data.update({"client_type": "C", "company_name": "Remove Business",
                          "name": "Connie", "last_name": "Contact"})
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        co_person = Person.objects.get(name="Connie")
        post_data.pop("company_name")
        post_data.update({"client_type": "I",
                          "name": "Individual",
                          "last_name": "Bhalerao",
                          "company_contact": co_person.id})
        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        existing_client.refresh_from_db()
        self.assertFalse(existing_client.contact.is_company)
        self.assertEqual(existing_client.contact.name, "Individual")
        self.assertIsNone(existing_client.contact.company)
        self.assertFalse(existing_client.contact.is_company_contact)
        self.assertFalse(Person.objects.filter(name="Connie").exists())

    def test_company_cannot_have_logins(self):
        existing_client = self.field.farm.client
        existing_client.contact.is_company = True
        existing_client.contact.save()
        existing_client.contact.refresh_from_db()
        response = self.client.get(reverse("client_update", kwargs={'pk': existing_client.contact.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(existing_client.contact.is_company, True)

        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "email1@gmail.com",
            "email-0-email_type": "L",
            "email-0-contact": existing_client.contact.id,
            "email-0-id": existing_client.contact.email.first().id,
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "C",
            "company_name": "SoilDX-V2",
        }

        from contact.forms import EmailModelFormSet
        form = EmailModelFormSet(post_data, instance=existing_client.contact)
        self.assertFalse(form.is_valid())
        self.assertEqual(form._non_form_errors[0],
                         "Companies cannot have logins - go into the Person's record to set login details")

    def test_individual_cannot_have_multiple_logins(self):
        existing_client = self.field.farm.client
        response = self.client.get(reverse("client_update", kwargs={'pk': existing_client.contact.pk}))
        self.assertEqual(response.status_code, 200)
        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-555-1212",
            "phone-0-phone_type": "M",
            "phone-0-contact": existing_client.contact.id,
            "phone-0-id": '',
            "email-TOTAL_FORMS": "2",
            "email-INITIAL_FORMS": "1",
            "email-0-email": "email1@gmail.com",
            "email-0-email_type": "L",
            "email-0-contact": existing_client.contact.id,
            "email-0-id": existing_client.contact.email.first().id,
            "email-1-email": "email2@gmail.com",
            "email-1-email_type": "L",
            "email-1-contact": existing_client.contact.id,
            "email-1-id": "",
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "C",
            "company_name": "SoilDX-V2",
        }

        from contact.forms import EmailModelFormSet
        form = EmailModelFormSet(post_data, instance=existing_client.contact)
        self.assertFalse(form.is_valid())
        self.assertEqual(form._non_form_errors[0], "Only one Login per Person allowed")

    def test_client_add_post_view_invalid_form(self):
        u = User.objects.get(username="testinguser")
        c = TestClient()

        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "client_type": "I",
        }

        c.force_login(u)
        resp = c.post(reverse("client_add"), data=post_data)

        self.assertTrue(resp.status_code, 302)
        self.assertRaises(Client.DoesNotExist, Client.objects.get, contact__name="NewClient")

    def test_client_update_invalid_view(self):

        existing_client = self.field.farm.client
        response = self.client.get(reverse("client_update", kwargs={'pk': existing_client.contact.pk}))
        self.assertEqual(response.status_code, 200)

        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "client_type": "I"
        }

        response = self.client.post(reverse("client_update", kwargs={'pk': existing_client.contact.pk}),
                                    data=post_data)
        self.assertTrue(response.status_code, 302)
        existing_client.refresh_from_db()
        self.assertNotEqual(existing_client.contact.name, "NewClient-Update")

    def test_client_detail_view(self):

        last_client = Client.objects.last()
        resp = self.client.get(reverse("client_detail", kwargs={'pk': last_client.pk + 10}))
        self.assertEqual(resp.status_code, 404)

        existing_client = self.field.farm.client
        user = existing_client.originator.user
        self.client.force_login(user)

        response = self.client.get(reverse("client_detail", kwargs={'pk': existing_client.pk}))
        self.assertEqual(response.status_code, 200)

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        self.assertEqual(u.pk, User.objects.last().pk)
        self.assertNotEqual(u.person, None)

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("client_detail", kwargs={'pk': existing_client.pk}))
        self.assertEqual(resp.status_code, 403)

    def test_client_delete_view(self):
        existing_client = self.field.farm.client

        # try client-user delete
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        self.assertEqual(u.pk, User.objects.last().pk)
        self.assertNotEqual(u.person, None)

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("client_delete", kwargs={'pk': existing_client.pk}))
        self.assertEqual(resp.status_code, 403)

        originator = existing_client.originator
        field = self.field
        farm = self.field.farm
        client_person = existing_client.contact
        user = existing_client.originator.user
        self.client.force_login(user)

        self.assertEqual(existing_client.originator.clients.count(), 1)
        response = self.client.post(reverse("client_delete", kwargs={'pk': existing_client.pk}))
        self.assertEqual(originator.clients.count(), 0)
        existing_client.refresh_from_db()
        self.assertFalse(existing_client.active)

        # TODO test what should happen with farms and fields of an inactive client?

    def test_person_setup_user(self):
        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-987-6543",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "O",
            "email-0-contact": '',
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)
        self.assertTrue(resp.status_code, 302)
        newclt = Client.objects.latest("pk")

        resp = self.client.post(reverse("create_user_from_person", args=[newclt.contact.pk + 1]))
        self.assertEqual(resp.status_code, 500)
        self.assertEqual(json.loads(resp.content)["message"], "Contact matching query does not exist.")

        resp = self.client.post(reverse("create_user_from_person", args=[newclt.contact.pk]))
        self.assertEqual(resp.status_code, 500)
        self.assertEqual(json.loads(resp.content)["message"],
                         'Index Error - Did you set an email to Login and mark it as Primary?')

        User.objects.create_user("someuser", "newguy@bhalerao.com")

        post_data = {
            "name": "NewClient-Update",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "1",
            "phone-0-phone_number": "217-555-1212",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "O",
            "email-0-contact": newclt.contact.id,
            "email-0-id": newclt.contact.email.first().id,
            "email-0-primary": True,
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
        }

        response = self.client.post(reverse("client_update", kwargs={'pk': newclt.contact.pk}),
                                    data=post_data)
        self.assertTrue(response.status_code, 302)
        resp = self.client.post(reverse("create_user_from_person", args=[newclt.contact.pk]))
        self.assertEqual(resp.status_code, 500)
        self.assertEqual(json.loads(resp.content)["message"],
                         "['Email already used by another user']")

    def test_person_setup_user_update_user(self):
        post_data = {
            "name": "NewClient",
            "last_name": "Bhalerao",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "phone-0-phone_number": "217-987-6543",
            "phone-0-phone_type": "M",
            "phone-0-contact": '',
            "phone-0-id": '',
            "email-0-email": "newguy@bhalerao.com",
            "email-0-email_type": "L",
            "email-0-contact": '',
            "email-0-primary": True,
            "email-0-id": '',
            "address-0-id": "",
            "address-0-address_type": "O",
            "address-0-contact": '',
            "client_type": "I",
        }

        self.client.force_login(self.user)
        resp = self.client.post(reverse("client_add"), data=post_data)
        self.assertTrue(resp.status_code, 302)
        newclt = Client.objects.latest("pk")

        resp = self.client.post(reverse("create_user_from_person", args=[newclt.contact.pk]))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)["status"], "link_sent")

        newclt.refresh_from_db()
        newuser = User.objects.get(person__pk=newclt.contact.pk)
        self.assertEqual(newuser.email, "newguy@bhalerao.com")

        ## Now we update the login email
        post_data = {
            'client_type': ['I'],
            'name': ['Jessie'],
            'middle_name': ['and Kaustubh'],
            'last_name': ['Bhalerao'],
            'company_name': [''],
            'notes': [''],
            'company_contact': [''],
            'phone-TOTAL_FORMS': ['1'],
            'phone-INITIAL_FORMS': ['0'],
            'phone-MIN_NUM_FORMS': ['0'],
            'phone-MAX_NUM_FORMS': ['1000'],
            'phone-0-phone_number': [''],
            'phone-0-phone_type': ['M'],
            'phone-0-extension': [''],
            'phone-0-id': [''],
            'phone-0-contact': newclt.contact.id,
            'phone-1-phone_number': [''],
            'phone-1-phone_type': ['M'],
            'phone-1-extension': [''],
            'phone-1-id': [''],
            'phone-1-contact': newclt.contact.id,
            'email-TOTAL_FORMS': ['2'],
            'email-INITIAL_FORMS': ['1'],
            'email-MIN_NUM_FORMS': ['0'],
            'email-MAX_NUM_FORMS': ['1000'],
            'email-0-email': ['newguyemail@bhalerao.com'],
            'email-0-email_type': ['L'],
            'email-0-primary': True,
            'email-0-id': newclt.contact.email.first().id,
            'email-0-contact': newclt.contact.id,
            'email-1-email': [''],
            'email-1-email_type': ['O'],
            'email-1-id': [''],
            'email-1-contact': newclt.contact.id,
            'address-TOTAL_FORMS': ['1'],
            'address-INITIAL_FORMS': ['0'],
            'address-MIN_NUM_FORMS': ['0'],
            'address-MAX_NUM_FORMS': ['1000'],
            'address-0-contact': newclt.contact.id,
            'address-0-address_type': "O",
            'address-0-address': "",
            'submit': ['Submit']}

        resp = self.client.post(reverse("client_update", kwargs={'pk': newclt.contact.pk}), data=post_data)
        self.assertTrue(resp.status_code, 302)
        newuser.refresh_from_db()
        self.assertEqual(newuser.email, "newguyemail@bhalerao.com")


class ClientFarmsFieldsTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase().createField()

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)

    def test_farm_add_view(self):
        c = self.field.farm.client
        resp = self.client.get(reverse("new_farm", kwargs={'pk': c.pk}))
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'name': 'New farm on client',
            'client': c.pk
        }

        self.assertEqual(len(c.farms.all()), 1)
        resp = self.client.post(reverse("new_farm", kwargs={'pk': c.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(len(c.farms.all()), 2)

    def test_farm_add_invalid_org(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        org, _ = Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        post_data = {
            'name': 'New farm on client',
            'client': self.field.farm.client.pk
        }

        self.assertEqual(len(self.field.farm.client.farms.all()), 1)
        resp = self.client.post(reverse("new_farm", kwargs={'pk': self.field.farm.client.pk}), data=post_data)

        ouser, c = OrganizationUser.objects.get_or_create(user=u, organization=org)
        self.assertNotEqual(self.field.farm.client.originator.organization, ouser.organization)
        self.assertEqual(resp.status_code, 403)

        self.assertEqual(len(self.field.farm.client.farms.all()), 1)

        # add field should work for the Primary org
        u.is_staff = True
        u.save()
        self.client.force_login(u)
        self.assertEqual(len(self.field.farm.client.farms.all()), 1)
        org.roles.add(Role.objects.get(role='Primary'))
        resp = self.client.post(reverse("new_farm", kwargs={'pk': self.field.farm.client.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(self.field.farm.client.farms.all().count(), 2)

        # try update with Primary
        new_farm = self.field.farm.client.farms.last()
        post_data = {
            'name': 'Change the farm name',
            'client': self.field.farm.client.pk
        }
        resp = self.client.post(reverse("farm_detail", kwargs={'pk': new_farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)
        new_farm.refresh_from_db()
        self.assertEqual(new_farm.name, 'Change the farm name')

    def test_farm_add_invalid_form(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        org, _ = Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        post_data = {
            'name': 'New farm on client',
        }

        self.assertEqual(len(self.field.farm.client.farms.all()), 1)
        resp = self.client.post(reverse("new_farm", kwargs={'pk': self.field.farm.client.pk}), data=post_data)

        ouser, c = OrganizationUser.objects.get_or_create(user=u, organization=org)
        self.assertNotEqual(self.field.farm.client.originator.organization, ouser.organization)
        self.assertEqual(resp.status_code, 403)

    def test_farm_delete_bad_user(self):
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.post(reverse("farm_delete", kwargs={"pk": self.field.farm.pk}))
        self.assertEqual(resp.status_code, 403)

    def test_delete_farm_protected_when_field_exists(self):
        self.client.force_login(self.user)
        with self.assertRaises(ProtectedError):
            resp = self.client.post(reverse("farm_delete", kwargs={"pk": self.field.farm.pk}))
        # url = reverse("farm_delete", kwargs={"pk": self.field.farm.pk})
        # resp = self.client.post(url)
        self.assertEqual(len(self.field.farm.client.farms.all()), 1)

    def test_farm_detail(self):
        resp = self.client.get(reverse("farm_detail", kwargs={'pk': self.field.farm.pk}))
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'name': 'New name on farm',
            'client': self.field.farm.client.pk
        }

        resp = self.client.post(reverse("farm_detail", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        self.field.farm.refresh_from_db()

        self.assertEqual(self.field.farm.name, "New name on farm")

    def test_farm_detail_invalid_org(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("farm_detail", kwargs={'pk': self.field.farm.pk}))
        self.assertEqual(resp.status_code, 403)

        post_data = {
            'name': 'New name on farm',
            'client': self.field.farm.client.pk
        }

        resp = self.client.post(reverse("farm_detail", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 403)

    def test_field_views_json(self):
        u = User.objects.get(username="testinguser")
        c = TestClient()
        view_list = ["field_list"]

        for vu in view_list:
            p = c.get(reverse(vu))
            self.assertEqual(p.status_code, 302)

        c.force_login(u)
        for vu in view_list:
            p = c.get(reverse(vu))
            self.assertEqual(p.status_code, 200)

        json_search = "/clients/fields/data/?draw=4&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=contact&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=farm&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=field&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=contact&start=0&length=10&search%5Bvalue%5D=field&search%5Bregex%5D=false&_=1537208028575"
        p = c.get(json_search)
        self.assertEqual(p.status_code, 200)

        json_search = "/clients/fields/data/"
        p = c.get(json_search)
        response = p.json()
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(response['data']), 1)

        # test search request
        search_term = 'Kau'
        json_search = "/clients/fields/data/?draw=4&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=contact&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D={}&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=farm&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=field&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=contact&start=0&length=10&search%5Bvalue%5D=field&search%5Bregex%5D=false&_=1537208028575".format(
            search_term)
        p = c.get(json_search)
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(p.json()['data']), 1)

        search_term = 'Kau helerao'
        json_search = "/clients/fields/data/?draw=4&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=contact&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D={}&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=farm&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=field&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=contact&start=0&length=10&search%5Bvalue%5D=field&search%5Bregex%5D=false&_=1537208028575".format(
            search_term)
        p = c.get(json_search)
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(p.json()['data']), 1)

        search_term = 'K '
        json_search = "/clients/fields/data/?draw=4&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=contact&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D={}&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=farm&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=field&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=contact&start=0&length=10&search%5Bvalue%5D=field&search%5Bregex%5D=false&_=1537208028575".format(
            search_term)
        p = c.get(json_search)
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(p.json()['data']), 1)

        # client can view his field, have to create a user for him first
        our_client = Client.objects.get(contact__name='Kaustubh', contact__last_name="Bhalerao")
        our_client_person = Person.objects.get(id=our_client.contact.id)
        our_client_person.get_or_create_user_from_person(email=our_client.contact.email.first().email,
                                                         random_password=True)
        our_client_person.refresh_from_db()
        uc = our_client_person.user
        c.force_login(uc)
        p = c.get(reverse('field_list_json'))
        self.assertEqual(p.status_code, 200)

        # add fields for a different org
        # test only the orguser's clients fields come back
        c.force_login(self.alt_org_user)
        p = c.get(reverse('field_list_json'))
        self.assertEqual(p.status_code, 200)

        json_search = "/clients/fields/data/"
        p = c.get(json_search)
        response = p.json()
        self.assertEqual(len(response['data']), 0)

        # test if the test_user is set to is_staff they still don't get anything since the org isn't primary
        self.alt_org_user.is_staff = True
        self.alt_org_user.save()

        json_search = "/clients/fields/data/"
        p = c.get(json_search)
        response = p.json()
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(response['data']), 0)

        # test if the test_user is set to is_staff and org is set to primary, then they get all
        self.alt_org.roles.add(Role.objects.get(role="Primary"))
        self.alt_org_user.save()

        p = c.get(json_search)
        response = p.json()
        self.assertEqual(p.status_code, 200)
        self.assertEqual(len(response['data']), 1)

    def test_add_field_view(self):

        resp = self.client.get(reverse("new_field", kwargs={'pk': self.field.farm.pk}))
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'name': 'New field on farm',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk + 9999}), data=post_data)
        self.assertEqual(resp.status_code, 404)

        self.assertEqual(len(self.field.farm.fields.all()), 2)

    # def test_add_field__with_shapefile_view(self):
    #
    #     resp = self.client.get(reverse("new_field", kwargs={'pk': self.field.farm.pk}))
    #     self.assertEqual(resp.status_code, 200)
    #     evt, created = CalendarEvent.objects.get_or_create(name="Shapefile Upload")
    #
    #     with open("./clients/tests/Blum 80.zip", 'rb') as fp:
    #         post_data = {
    #             'name': 'New field on farm',
    #             'farm': self.field.farm.pk,
    #             "event": evt.pk,
    #             "event_date": now(),
    #             "file": fp,
    #         }
    #
    #         resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk}), data=post_data)
    #         self.assertEqual(resp.status_code, 302)
    #
    #     fld = Field.objects.latest("pk")
    #     self.assertEqual(fld.name, "New field on farm")
    #     self.assertEqual(fld.area, 20)

    def test_add_field_invalid_org(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        org, created = Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("new_field", kwargs={'pk': self.field.farm.pk}))
        self.assertEqual(resp.status_code, 403)

        post_data = {
            'name': 'New field on farm',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 403)

    def test_add_field_as_primary_org(self):
        # add field should work for the Primary org
        self.assertEqual(self.field.farm.fields.all().count(), 1)
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        org, created = Organization.objects.create_dealership(company=c)
        #org.roles.add(Role.objects.get(role='Primary'))
        Organization.objects.set_primary(org)
        org.add_user(user=u)
        post_data = {
            'name': 'New field on farm',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(self.field.farm.fields.all().count(), 2)

        # try update with Primary
        u.is_staff = True
        u.save()
        self.client.force_login(u)
        new_field = Field.objects.last()
        post_data = {
            'name': 'Change the field name',
            'farm': self.field.farm.pk
        }
        resp = self.client.post(reverse("field_detail", kwargs={'pk': new_field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)
        new_field.refresh_from_db()
        self.assertEqual(new_field.name, 'Change the field name')

    def test_field_detail_view(self):
        resp = self.client.get(reverse("field_detail", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'name': 'New name on field',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("field_detail", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        self.field.refresh_from_db()

        self.assertEqual(self.field.name, "New name on field")

    def test_field_detail_invalid_org(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("field_detail", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 403)

        post_data = {
            'name': 'New name on field',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("field_detail", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 403)

    def test_field_detail_with_hallpass(self):

        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        org, _ = Organization.objects.create_dealership(company=c)
        org.add_user(user=u)
        self.client.force_login(u)

        resp = self.client.get(reverse("field_detail", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 403)

        from clients.models import HallPass
        hp = HallPass.objects.create(field=self.field, user=u, created_by=u)

        resp = self.client.get(reverse("field_detail", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

        hp.expiry = now() - datetime.timedelta(days=1)
        hp.save()

        self.assertTrue(hp.expired)
        resp = self.client.get(reverse("field_detail", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 403)

    def test_field_delete(self):

        post_data = {
            'name': 'New field on farm',
            'farm': self.field.farm.pk
        }

        resp = self.client.post(reverse("new_field", kwargs={'pk': self.field.farm.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        self.assertEqual(len(self.field.farm.fields.all()), 2)

        f = Field.objects.get(name="New field on farm")
        resp = self.client.post(reverse("field_delete", kwargs={'pk': f.pk}))
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(len(self.field.farm.fields.all()), 1)


    def test_field_delete_invalid_org(self):
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)
        resp = self.client.post(reverse("field_delete", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 403)
