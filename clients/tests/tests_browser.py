import time
from unittest import skip

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.cache import cache
from django.test import override_settings

from clients.models import Client
from contact.models import User
from associates.tests.tests_browser import BrowserTests

from associates.models import Organization
from contact.models import Person, Company
from clients.tests.tests_models import ClientsTestCase
from django.urls import reverse

from contact.tests.tests_models import ContactTestCase
from django.test.client import Client as TestClient


class DealerTests(BrowserTests):
    fixtures = ['roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.set_password("buckeyes")
        self.user.save()
        self.field = ClientsTestCase().createField()

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)

    def test_create_client(self):
        self._login(self.user.username, "buckeyes")
        self.wd.find_css("a.btn").click()
        self.wd.wait_for_css("#id_client_type")
        ## Add individual as Client
        self.wd.fill_input("#id_name", "Roger")
        self.wd.fill_input("#id_last_name", "Wilco")
        self.wd.find_css("#submit-id-continue").click()
        self.wd.wait_for_css("#id_client_type")
        newClient = Client.objects.latest("pk")
        self.assertEqual(newClient.originator.user, self.user)

        ## Update client
        self.wd.fill_input("#id_phone-0-phone_number", "2177216032")
        self.wd.fill_input("#id_email-0-email", "test@testco.com")
        self.wd.find_css("#id_email-0-email_type").click()
        self.wd.find_css("#id_email-0-email_type > option[value='L']").click()
        self.wd.find_css("#id_email-0-primary").click()
        self.wd.find_css("#submit-id-continue").click()
        self.wd.wait_for_css("#id_client_type")
        newClient = Client.objects.latest("pk")
        self.assertEqual(newClient.originator.user, self.user)

    def test_view_client(self):
        self._login(self.user.username, "buckeyes")
        self.wd.find_css("#sidebar > div.menu-section > ul > li:nth-child(2) > a").click()
        # time.sleep(2)
        self.wd.get('%s%s' % (self.live_server_url,
                              reverse("client_detail", kwargs={'pk': self.field.farm.client.pk})))
        # time.sleep(2)
        self.wd.wait_for_css("#button-id-update").click()
        self.wd.wait_for_css("#submit-id-submit")

    def test_change_client_from_individual_to_company(self):
        self.assertFalse(self.field.farm.client.contact.is_company)
        currentclient = self.field.farm.client
        individual = self.field.farm.client.contact
        self._login(self.user.username, "buckeyes")
        self.wd.find_css("#sidebar > div.menu-section > ul > li:nth-child(2) > a").click()
        self.wd.get('%s%s' % (self.live_server_url,
                              reverse("client_detail", kwargs={'pk': self.field.farm.client.pk})))
        self.wd.wait_for_css("#button-id-update").click()
        self.wd.wait_for_css("#submit-id-submit")

        ## Add company and switch client to company.
        self.wd.fill_input("#id_company_name", "ClientCompany")
        self.wd.find_css("#id_client_type").click()
        self.wd.find_css("#id_client_type > option[value='C']").click()
        self.wd.find_css("#submit-id-continue").click()

        self.wd.wait_for_css("#add_person_button")
        currentclient.refresh_from_db()
        self.assertTrue(currentclient.contact.is_company)
        company = Company.objects.get(id=currentclient.contact.id)
        self.assertTrue(company.get_company_contact().id, individual.id)

        ## Add a person to the company
        self.wd.wait_for_css("#add_person_button").click()
        self.wd.fill_input("#id_name", "Peter")
        self.wd.fill_input("#id_last_name", "Rabbit")
        self.wd.fill_input("#id_phone-0-phone_number", "2177216032")
        self.wd.find_css("#submit-id-submit").click()
        self.wd.wait_for_css("#submit-id-submit")
        company.refresh_from_db()
        self.assertEqual(company.company_members().count(), 2)

        # Now change client back to the Individual.
        self.wd.get('%s%s' % (self.live_server_url,
                              reverse("client_detail", kwargs={'pk': self.field.farm.client.pk})))
        self.wd.wait_for_css("#button-id-update").click()
        self.wd.wait_for_css("#submit-id-submit")
        self.wd.find_css("#id_client_type").click()
        self.wd.find_css("#id_client_type > option[value='I']").click()
        self.wd.find_css("#submit-id-continue").click()
        self.wd.wait_for_css("#submit-id-submit")
        currentclient.refresh_from_db()

        self.assertFalse(currentclient.contact.is_company)
        self.assertEqual(currentclient.contact.name, "Kaustubh")


class ClientTests(BrowserTests):
    fixtures = ['roles']

    def setUp(self):
        DealerTests.setUp(self)
        p = Person.objects.get(id=self.field.farm.client.contact.id)
        self.cltuser = p.get_or_create_user_from_person(username="kbhalerao", is_active=True)
        self.cltuser.set_password("buckeyes")
        self.cltuser.save()

    def test_dashboard_view(self):
        self._login(self.cltuser.username, "buckeyes")
        self.wd.find_css("#profile_button").click()
        self.wd.find_css("#profile_link").click()
        self.wd.fill_input("#id_middle_name", "Deepak")
        self.wd.find_css("#submit-id-submit").click()
        self.cltuser.person.refresh_from_db()
        self.assertEqual(self.cltuser.person.middle_name, "Deepak")

    def test_person_of_client_company_login(self):
        self.assertFalse(self.field.farm.client.contact.is_company)
        currentclient = self.field.farm.client
        individual = self.field.farm.client.contact
        self._login(self.user.username, "buckeyes")
        self.wd.find_css("#sidebar > div.menu-section > ul > li:nth-child(2) > a").click()
        self.wd.get('%s%s' % (self.live_server_url,
                              reverse("client_detail", kwargs={'pk': self.field.farm.client.pk})))
        self.wd.wait_for_css("#button-id-update").click()
        self.wd.wait_for_css("#submit-id-submit")

        ## Add company and switch client to company.
        self.wd.fill_input("#id_company_name", "ClientCompany")
        self.wd.find_css("#id_client_type").click()
        self.wd.find_css("#id_client_type > option[value='C']").click()
        self.wd.find_css("#id_email-0-email_type").click()
        self.wd.find_css("#id_email-0-email_type > option[value='O']").click()

        self.wd.find_css("#submit-id-continue").click()

        currentclient.refresh_from_db()
        self.assertTrue(currentclient.contact.is_company)
        company = Company.objects.get(id=currentclient.contact.id)
        self.assertTrue(company.get_company_contact().id, individual.id)

        self._login(self.cltuser.username, "buckeyes")
        self.wd.find_css("#profile_button").click()
        self.wd.find_css("#profile_link").click()
        self.wd.fill_input("#id_middle_name", "Deepak")
        self.wd.find_css("#submit-id-submit").click()
        self.cltuser.refresh_from_db()
        self.assertEqual(self.cltuser.person.middle_name, "Deepak")

    def tearDown(self):
        cache.clear()
