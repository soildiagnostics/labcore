from statistics import mean
from unittest import skip

from django.contrib.gis.geos import GeometryCollection
from django.test import TestCase, tag, override_settings
from django.urls import reverse
from shapely.geometry import Polygon

from clients.gis.shapezip_handler import ZippedShapeFile
from clients.tests.tests_models import ClientsTestCase
from clients.models import SubfieldLayer
import os, tempfile, shutil
from clients.gis.gishelper import SDXShapefile
from clients.gis.farm_splitter import ZippedMultifieldShapeFile
import os
import shutil
import tempfile

from django.test import TestCase

from clients.gis.gishelper import SDXShapefile
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.models import SubfieldLayer
from clients.tests.tests_models import ClientsTestCase


class ZipFileTest(TestCase):
    fixtures = ['roles']

    def setUp(self):

        self.file = open("./clients/tests/Pivots.zip", 'rb')
        ClientsTestCase().setUp()
        self.field = ClientsTestCase().createField()
        self.fshandler = ZippedMultifieldShapeFile(self.file, self.field.farm)

    def test_shapefile_check(self):
        self.assertTrue(self.fshandler.valid_zip())
        self.assertEqual(len(self.fshandler.shapefile_list()), 1)

    def test_shapefile_analyze(self):
        self.assertTrue(self.fshandler.valid_zip())
        shfiles = self.fshandler.shapefile_list()
        self.assertEqual(len(self.fshandler.analyze(shapefile=shfiles[0])), 1)

    def test_create_fields(self):
        self.assertTrue(self.fshandler.valid_zip())
        shfiles = self.fshandler.shapefile_list()
        lyrs = self.fshandler.analyze(shfiles[0])
        self.fshandler.create_fields(shfiles[0], lyrs[0].name, 'Name')
        self.assertEqual(self.field.farm.fields.count(), 19)