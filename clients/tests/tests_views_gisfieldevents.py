import json
from datetime import timedelta

from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now

from clients.tests.tests_models import ClientsTestCase
from contact.models import User
from fieldcalendar.models import FieldEvent, CalendarEventKind, CalendarEvent
from clients.serializers import FieldWithEventsSerializer

class FieldBirdsEyeViewTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        kind, _ = CalendarEventKind.objects.get_or_create(name="Planting")
        c, _ = CalendarEvent.objects.get_or_create(kind=kind, name="Corn")

    def test_events_time_bound(self):

        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        details = {
            "_id": "_fc8",
            "color": "green",
            "start": now().isoformat(),
            "end": now().isoformat(),
            "title": "Corn",
            "allDay": True,
            "editable": False,
            "field_id": f.pk,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        fe = FieldEvent.objects.create_system_event(field=f, title="System Event", start=now(), end=now())
        serdat = FieldWithEventsSerializer(f).data
        self.assertEqual(len(serdat['events']), 1)
        self.assertEqual(json.loads(serdat['boundary'])["type"], "MultiPolygon")

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_birdseye') + '?' + urlencode(time)

        with self.assertNumQueries(10):
            response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['events'][0]['extendedProps']['field_id'], f.pk)
        self.assertTrue('color' in resp[0]['events'][0].keys())

        ### Get no events
        time = {
            'start': now() - timedelta(days=2),
            'end': now() - timedelta(days=1)
        }
        url = reverse('api_field_events_birdseye') + '?' + urlencode(time)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        resp = json.loads(response.content)
        self.assertEqual(len(resp[0]['events']), 0)