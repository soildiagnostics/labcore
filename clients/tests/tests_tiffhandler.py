import os

from django.contrib.gis.geos import MultiPolygon, Polygon
from django.test import TestCase
from clients.gis.tif_handler import TiffFile
from clients.tests.tests_models import ClientsTestCase


class TiffHandlerTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase().setUp()
        self.field = ClientsTestCase().createField()
        self.handler = None

        bdy = MultiPolygon([Polygon([[-91.608318, 41.374071], [-91.604177, 41.374004], [-91.604294, 41.366938],
                                     [-91.606788, 41.36695], [-91.606784, 41.36707], [-91.606832, 41.367166],
                                     [-91.606816, 41.3673], [-91.606814, 41.367366], [-91.606825, 41.367424],
                                     [-91.606912, 41.367482], [-91.607012, 41.367522], [-91.607125, 41.367562],
                                     [-91.607238, 41.367593], [-91.6073, 41.367632], [-91.607375, 41.36769],
                                     [-91.607398, 41.367758], [-91.60746, 41.367816], [-91.607535, 41.367846],
                                     [-91.607646, 41.367934], [-91.607772, 41.367964], [-91.607987, 41.367977],
                                     [-91.608975, 41.367984], [-91.609063, 41.367995], [-91.609126, 41.368025],
                                     [-91.609175, 41.368092], [-91.60921, 41.368198], [-91.609119, 41.373531],
                                     [-91.60909, 41.373655], [-91.608976, 41.373662], [-91.608773, 41.373659],
                                     [-91.608571, 41.373656], [-91.608508, 41.373656], [-91.608446, 41.373657],
                                     [-91.608333, 41.373693], [-91.608274, 41.373823], [-91.608308, 41.373985],
                                     [-91.608318, 41.374071]])])
        self.field.boundary = bdy
        self.field.save()

    def test_invalid_tif(self):
        png = "./clients/tests/test_elev_raster.png"
        self.handler = TiffFile(png, field=self.field)
        self.assertFalse(self.handler.valid_file())

    def test_valid_tif(self):
        tiff = "./clients/tests/hora_ndvi.tif"
        self.handler = TiffFile(tiff, field=self.field)
        self.assertTrue(self.handler.valid_file())

    def test_reproject(self):
        tiff = "./clients/tests/hora_ndvi.tif"
        self.handler = TiffFile(tiff, field=self.field)
        self.assertTrue(self.handler.valid_file())
        layer, r, c = self.handler.create_layers_and_rasterfiles(self.field, "Rasterfile", "NDVI")
        self.handler.reproject(clip=True, save=True)
        self.assertIn('NDVI', layer.dbf_field_names['field_names'])
        self.assertTrue(layer.dbf_field_names['raster'])
        self.assertIn('Reprojected', layer.dbf_field_names['field_names'])
        self.assertEqual(layer.rasterfile_set.count(), 2)

    def test_colorize(self):
        tiff = "./clients/tests/hora_ndvi.tif"
        self.handler = TiffFile(tiff, field=self.field)
        self.assertTrue(self.handler.valid_file())
        layer, r, c = self.handler.create_layers_and_rasterfiles(self.field, "Rasterfile", "NDVI")
        self.handler.colorize(self.handler.reproject(clip=True, save=True),
                              save=True)
        self.assertIn('NDVI', layer.dbf_field_names['field_names'])
        self.assertTrue(layer.dbf_field_names['raster'])
        self.assertIn('Reprojected', layer.dbf_field_names['field_names'])
        self.assertEqual(layer.rasterfile_set.count(), 3)

    def test_create_layers_events(self):
        tiff = "./clients/tests/hora_ndvi.tif"
        self.handler = TiffFile(tiff, field=self.field)
        self.assertTrue(self.handler.valid_file())
        l, r, c = self.handler.create_layers_and_rasterfiles(self.field, "Rasterfile", "NDVI")
        self.assertEqual(l.field, self.field)
        self.assertEqual(r.layer, l)
        self.assertEqual(r.file.name, f"rasters/{l.pk}/NDVI.tif")
        self.assertTrue(c)
        l, r, c = self.handler.create_layers_and_rasterfiles(self.field, "Rasterfile", "NDVI")
        self.assertFalse(c)

    def test_contour(self):
        tiff = "./clients/tests/hora_ndvi.tif"
        self.handler = TiffFile(tiff, field=self.field)
        self.assertTrue(self.handler.valid_file())
        l, r, c = self.handler.create_layers_and_rasterfiles(self.field, "Rasterfile", "NDVI")
        self.assertEqual(l.field, self.field)
        self.assertEqual(r.layer, l)
        self.assertEqual(r.file.name, f"rasters/{l.pk}/NDVI.tif")
        self.assertTrue(c)
        self.handler.file = r.file
        res = self.handler.contour(1, "NDVI")
        self.assertEqual((self.handler.tempdir, 'contour.shp'), res)

    def tearDown(self):
        self.handler.cleanup()
