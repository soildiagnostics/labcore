# from django.contrib.auth.models import User
from django.contrib.gis.geos import Polygon, MultiPolygon, GEOSGeometry, GeometryCollection
from django.db.models import ProtectedError
from django.test import Client as TestClient
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse

from associates.models import OrganizationUser, Organization, Role
from clients.forms import BaseFieldForm
from clients.models import Client, Farm, Field, SubfieldLayer
from contact.models import Company, Person, EmailModel, User
from contact.tests.tests_models import ContactTestCase


# Create your tests here.
class ClientsTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.org = a
        # a.roles.add(Role.objects.get(role="Primary"))
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)
        User.objects.create_superuser('myuser', 'myemail@test.com', 'password')

    def createClient(self):
        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        e = EmailModel.objects.create(email="test@example.com")
        e.contact = m
        e.save()
        m.save()

        # Create client for contact
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        return Client.objects.create(contact=m,
                                     originator=orguser)

    def createFarm(self):
        client = self.createClient()
        return Farm.objects.create(name="Test farm",
                                   client=client)

    def createField(self):
        farm = self.createFarm()
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.boundary = mp
        return Field.objects.create(name="Test field",
                                    farm=farm,
                                    boundary=mp)

    def test_add_client(self):
        client = self.createClient()
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        m = Person.objects.get(name="Kaustubh",
                               last_name="Bhalerao")
        self.assertEqual(str(client), "{} > {}".format(str(orguser), str(m)))
        self.assertEqual(client.get_absolute_url(), '/clients/{}/'.format(client.pk))
        self.assertEqual(str(client.name), "Kaustubh Bhalerao")

    def test_delete_client_contact(self):
        client = self.createClient()
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        m = Person.objects.get(name="Kaustubh",
                               last_name="Bhalerao")
        self.assertEqual(str(client), "{} > {}".format(str(orguser), str(m)))
        self.assertEqual(client.get_absolute_url(), '/clients/{}/'.format(client.pk))
        self.assertEqual(str(client.name), "Kaustubh Bhalerao")

        from django.db.models import ProtectedError
        with self.assertRaises(ProtectedError):
            client.contact.delete()

        client.delete()
        client.contact.delete()

        with self.assertRaises(Client.DoesNotExist):
            client.refresh_from_db()

    def test_delete_orguser(self):
        client = self.createClient()
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        m = Person.objects.get(name="Kaustubh",
                               last_name="Bhalerao")
        self.assertEqual(str(client), "{} > {}".format(str(orguser), str(m)))
        self.assertEqual(client.get_absolute_url(), '/clients/{}/'.format(client.pk))
        self.assertEqual(str(client.name), "Kaustubh Bhalerao")

        from django.db.models import ProtectedError
        with self.assertRaises(ProtectedError):
            orguser.delete()

        newuser = User.objects.create_user("newguy")

        newguy = OrganizationUser.objects.create(user=newuser,
                                                 organization=orguser.organization)
        self.assertIsNone(newguy.delete())

    def test_add_farm(self):
        farm = self.createFarm()
        self.assertEqual(str(farm), "Test farm")

    def test_farm_admin_change(self):
        farm = self.createFarm()
        c = TestClient()
        c.force_login(User.objects.get(username="myuser"))
        change_url = reverse('admin:clients_farm_change', args=(farm.id,))
        response = c.get(change_url)
        self.assertEqual(response.status_code, 200)

    def test_add_field(self):
        field = self.createField()

        self.assertEqual(
            str(field), "Testing User (SkyData Technologies LLC) > Kaustubh Bhalerao > Test farm > Test field")
        self.assertEqual(field.client, "Testing User (SkyData Technologies LLC) > Kaustubh Bhalerao")
        self.assertEqual(field.area, 76.6)

        field.boundary = None
        field.save()
        field.refresh_from_db()
        self.assertEqual(field.area, 0)
        self.assertEqual(field.get_absolute_url(), '/clients/field/{}/'.format(field.pk))

    def test_field_admin_change(self):
        field = self.createField()
        c = TestClient()
        c.force_login(User.objects.get(username="myuser"))
        change_url = reverse('admin:clients_field_change', args=(field.id,))
        response = c.get(change_url)
        self.assertEqual(response.status_code, 200)

    def test_add_subfields(self):
        field = self.createField()
        layer1 = SubfieldLayer.objects.create(name="layer1", field=field)

        p1 = Polygon(((0, 0), (0, 1), (1, 1), (0, 0)))
        p2 = Polygon(((1, 1), (1, 2), (2, 2), (1, 1)))
        mp = MultiPolygon(p1, p2)
        layer1.geoms = GeometryCollection(mp)
        layer1.save()

        self.assertEqual(str(layer1), "Test field (layer1)")

        SubfieldLayer.objects.create(name="layer2", field=field, geoms=GeometryCollection(mp))

        self.assertEqual(len(field.layer.all()), 2)

    def test_field_form_data_save(self):
        field = self.createField()
        form_data = {'name': field.name,
                     'farm': field.farm.id,
                     'boundary': self.boundary,
                     }
        kwargs = {'farm': field.farm.pk,
                  # 'field': field.pk,
                  'org': self.org}

        form = BaseFieldForm(data=form_data, **kwargs)
        self.assertTrue(form.is_valid())
        resp = form.save()
        self.assertIsNotNone(resp.id)

    @override_settings(IN_TEST=True, SOILDX_LAMBDA_PROCESSING_ENABLED=True, SOILDX_PUBLIC_10M_ELEVATION_ENABLED=True)
    def test_form_data_save_to_lambda(self):
        """This will create a new field based on the contents of the existing field"""
        field = self.createField()
        form_data = {'name': field.name,
                     'farm': field.farm.id,
                     'boundary': self.boundary,
                     }
        kwargs = {'farm': field.farm.pk,
                  # 'field': field.pk,
                  'org': self.org}

        form = BaseFieldForm(data=form_data, **kwargs)
        self.assertTrue(form.is_valid())
        resp = form.save()
        self.assertEqual(resp.name, field.name)

    def test_create_anonymous_client(self):
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        c, created = Client.objects.get_or_create_default_client(orguser)
        self.assertTrue(created)

        c, created = Client.objects.get_or_create_default_client(orguser)
        self.assertFalse(created)

    def test_create_anonymous_farm(self):
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        f, created = Farm.objects.get_or_create_default_farm(orguser)
        self.assertTrue(created)

        f, created = Farm.objects.get_or_create_default_farm(orguser)
        self.assertFalse(created)

    def test_create_anonymous_field(self):
        orguser = OrganizationUser.objects.get(user__username="testinguser")
        f = Field.objects.get_new_field_on_anonymous_client(orguser)
        f2 = Field.objects.get_new_field_on_anonymous_client(orguser)
        self.assertNotEqual(f.id, f2.id)

    def test_delete_field(self):
        field = self.createField()
        self.assertEqual(field.area, 76.6)
        from clients.models import update_soil_layer
        update_soil_layer(field)
        self.assertTrue(field.layer.count() > 0)
        with self.assertRaises(ProtectedError):
            field.delete()
