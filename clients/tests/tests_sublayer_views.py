import os
import shutil
import tempfile

import requests
from django.conf import settings
from django.test import Client as TestClient
from django.test import TestCase, override_settings, LiveServerTestCase
# from django.contrib.auth.models import User
from django.urls import reverse

from associates.models import Organization
from clients.models import SubfieldLayer, RasterFile
from clients.tests.tests_models import ClientsTestCase
from clients.tests.tests_views import ClientViewTests
from contact.models import Company, Person, User


class SublayerViewTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase().createField()

        #add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)


    @override_settings(SOILDX_LAMBDA_PROCESSING_ENABLED = True)
    def test_post_field_geo_raster(self):

        raster_file = 'test_elev_raster.tif'
        raster_path = os.path.join('clients/tests/', raster_file)
        url = reverse('field_geo_raster_post', kwargs={'pk': self.field.pk,
                                                 'file': raster_file,
                                                 'parameter': '13_DEM',
                                                 'layer': 'soildx'})
        self.client.force_login(self.user)
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(url,
                   {"file": fp},
                )
        self.assertEqual(resp.status_code, 201)
        subfields = self.field.layer.all()
        self.assertEqual(subfields.count(), 1)
        self.assertEqual(subfields.first().name, "soildx")
        raster = RasterFile.objects.first()
        self.assertEqual(raster.parameter, '13_DEM')
        self.assertEqual(raster.layer, subfields.first())
        img = raster.file.open()
        self.assertEqual(len(img), 52533)

        # try update
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(reverse('field_geo_raster_post',
                                            kwargs={'pk': self.field.pk,
                                                     'file': raster_file,
                                                     'parameter': '13_DEM',
                                                     'layer': 'soildx'}),
                   {"file": fp},
                )
        self.assertEqual(resp.status_code, 200)
        raster = RasterFile.objects.first()
        self.assertEqual(raster.parameter, '13_DEM')
        self.assertEqual(subfields.count(), 1)
        self.assertEqual(subfields.first().name, "soildx")

        # try a different extension - should be new record
        raster_file = 'elev_raster_new.png'
        url = reverse('field_geo_raster_post', kwargs={'pk': self.field.pk,
                                                 'file': raster_file,
                                                 'parameter': '13_DEM',
                                                 'layer': 'soildx'})
        self.client.force_login(self.user)
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(url,
                   {"file": fp},
                )
        # print(resp.content)
        self.assertEqual(resp.status_code, 201)
        raster = RasterFile.objects.last()
        self.assertEqual(raster.parameter, '13_DEM')
        self.assertIsNotNone(raster.file)
        self.assertEqual(raster.layer, subfields.first())
        self.assertEqual(self.field.layer.count(), 1)
        layer = raster.layer
        self.assertEqual(layer.dbf_field_names, {'field_names': ['13_DEM'], 'raster': True})
        self.assertEqual(layer.rasterfile_set.count(), 2)

        # layer with space in the name
        raster_file = 'test_elev_raster_new.tif'
        url = reverse('field_geo_raster_post', kwargs={'pk': self.field.pk,
                                                 'file': raster_file,
                                                 'parameter': '13_DEM',
                                                 'layer': 'public elevation'})
        self.client.force_login(self.user)
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(url,
                   {"file": fp},
                )
        self.assertEqual(resp.status_code, 201)
        layer = SubfieldLayer.objects.get(name='public elevation')
        self.assertEqual(layer.name, 'public elevation')
        self.assertEqual(layer.dbf_field_names, {'field_names': ['13_DEM'], 'raster': True})

        # layer with a different parameter
        raster_file = 'elev_raster_new.tif'
        url = reverse('field_geo_raster_post', kwargs={'pk': self.field.pk,
                                                 'file': raster_file,
                                                 'parameter': 'NEW_PARAM_SAME_LAYER',
                                                 'layer': 'public elevation'})
        self.client.force_login(self.user)
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(url,
                   {"file": fp},
                )
        self.assertEqual(resp.status_code, 201)
        layer = SubfieldLayer.objects.get(name='public elevation')
        self.assertEqual(layer.rasterfile_set.count(), 2)
        self.assertEqual(layer.dbf_field_names, {'field_names': ['13_DEM', 'NEW_PARAM_SAME_LAYER'], 'raster': True})


    def tearDown(self):
        for fname in os.listdir(tempfile.gettempdir()):
            if fname.startswith("sdx") or fname.startswith("gis"):
                try:
                    shutil.rmtree(os.path.join(tempfile.gettempdir(), fname))
                except OSError as e:
                    print(f"Teardown {self.__class__} Error {e.filename}-{e.strerror}")

        for fname in os.listdir('clients/tests/media'):
            if fname[0]=='.':
                continue
            try:
                shutil.rmtree(os.path.join(settings.BASE_DIR, 'clients/tests/media', fname))
            except OSError as e:
                print(f"Teardown {self.__class__} Error {e.filename}-{e.strerror}")


class OutsideClientAPITests(LiveServerTestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientViewTests.setUp(self)

    @override_settings(SOILDX_LAMBDA_PROCESSING_ENABLED=True)
    def test_post_field_geo_requests(self):

        raster_file = 'test_elev_raster.tif'
        raster_path = os.path.join(settings.BASE_DIR, 'clients/tests/', raster_file)

        TOKEN_URL = '{}/api-token-auth/'.format(self.live_server_url)
        LOGIN_URL = '{}/api/login/'.format(self.live_server_url)
        LOGIN_URL2 = '{}/auth/login/?next=/dashboard/'.format(self.live_server_url)


        client = requests.session()
        client.get(LOGIN_URL)
        csrftoken = client.cookies['csrftoken']

        self.user.set_password('newpass')
        self.user.save()
        self.assertTrue(self.user.check_password('newpass'))

        login_data = {
            'username': self.user.username,
            'password': 'newpass',
        }
        r0 = client.post(TOKEN_URL, data=login_data)
        token = r0.json()['token']

        # r1 = client.post(LOGIN_URL2, data=login_data,
        #                  # headers={'Authorization': "Token {}".format(token),
        #                  #          'Content-Type': 'application/json'},
        #                  )
        # csrftoken = client.cookies['csrftoken']
        # print('\ntoken 2:  ', csrftoken)

        ENDPOINT_URL = '{url_prefix}/clients/field/{field_pk}/gis/{file}/{parameter}/{layer}/raster/post/'.format(
            url_prefix=self.live_server_url,
            field_pk=self.field.pk,
            file=raster_file,
            parameter='13_DEM',
            layer='soildx'
        )
        with open(raster_path, 'rb') as fp:
            response = client.post(ENDPOINT_URL,
                             files={"file": fp,
                                   },
                             headers = {'Authorization':  "Token {}".format(token),
                                        },
                             allow_redirects=False,
                            )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(RasterFile.objects.count(), 1)
        raster = RasterFile.objects.first()
        self.assertEqual(raster.parameter, '13_DEM')
        self.assertIsNotNone(raster.file)
        layer = raster.layer
        self.assertEqual(layer.dbf_field_names, {'field_names': ['13_DEM'], 'raster': True})

        # try update
        ENDPOINT_URL = '{url_prefix}/clients/field/{field_pk}/gis/{file}/{parameter}/{layer}/raster/post/'.format(
            url_prefix=self.live_server_url,
            field_pk=self.field.pk,
            file=raster_file,
            parameter='13_DEM',
            layer='soildx'
        )
        with open(raster_path, 'rb') as fp:
            response = client.post(ENDPOINT_URL,
                                   files={"file": fp,
                                          },
                                   headers={'Authorization': "Token {}".format(token),
                                            },
                                   allow_redirects=False,
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(RasterFile.objects.count(), 1)
        layer = raster.layer
        self.assertEqual(layer.dbf_field_names, {'field_names': ['13_DEM'], 'raster': True})


