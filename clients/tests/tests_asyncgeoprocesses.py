from asgiref.sync import async_to_sync, sync_to_async
from django.contrib.gis.geos import MultiPolygon, Polygon
from django.test import Client as TestClient, tag
from django.test import TransactionTestCase
from django.urls import reverse

from associates.models import Organization
from clients.models import Client, Field, SubfieldLayer
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company, Person, User
from fieldcalendar.models import CalendarEvent

## AsyncIO tests
import pytest
from channels.testing import ApplicationCommunicator
from clients.consumers import ShapefileProcessConsumer

@pytest.mark.asyncio
@pytest.mark.django_db(transaction=True)
async def background_worker(message):
    communicator = ApplicationCommunicator(ShapefileProcessConsumer, {"type": "channel"})
    assert await communicator.receive_nothing(timeout=0.1, interval=0.01) is True
    assert (await sync_to_async(Field.objects.get)(id=message.get("field"))).id is not None
    await communicator.send_input(message)
    assert await communicator.receive_nothing(timeout=5, interval=0.1) is True
    # return


class GeoLayersTests(TransactionTestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase().createField()

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague', email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)

    @tag("single")
    def test_view_geo_details(self):
        resp = self.client.get(reverse("field_geo_details", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_detail_page(self):
        with open("clients/tests/GarysEast2018.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_detail", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})
        self.assertEqual(resp.status_code, 302)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_geodetails_page(self):
        with open("clients/tests/Wubben_DunsheeW150.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }

        async_to_sync(background_worker)(message)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 151.2)

        sf = self.field.layer.get(name="Wubben, Julian - Dunshee West 150 - Dunshee West 150 - 2015 Soil Test Sites")

        lyr = reverse('field_geo_layer', kwargs={'pk': sf.field.pk,
                                                 'file': sf.name,
                                                 "subfield": sf.pk,
                                                 'parameter': "pH"})
        resp = self.client.get(lyr)
        self.assertEqual(resp.status_code, 200)

        sf.refresh_from_db()
        self.assertEqual(len(sf.dbf_field_values['pH']), sf.features)

        raster = reverse('field_geo_raster', kwargs={'pk': sf.field.pk,
                                                     'file': sf.name,
                                                     "subfield": sf.pk,
                                                     'parameter': "pH"})
        resp = self.client.get(raster)
        self.assertEqual(resp.status_code, 200)

        from django.contrib.gis.geos import MultiPolygon
        sf.field.boundary = MultiPolygon([sf.geoms.convex_hull])
        sf.field.save()

        raster = reverse('field_geo_raster', kwargs={'pk': sf.field.pk,
                                                     'file': sf.name,
                                                     "subfield": sf.pk,
                                                     'parameter': "pH"})
        resp = self.client.get(raster)
        self.assertEqual(resp.status_code, 200)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_geodetails_page2(self):
        """ This file contains no projection information"""

        with open("clients/tests/Redeker_WarnersSofCemetery.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)

        fe = self.field.fieldevent_set.latest("pk")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }
        async_to_sync(background_worker)(message)

        sf = SubfieldLayer.objects.filter(field=self.field)
        self.assertEqual(sf.count(), 4)

        for s in sf:
            if "process_logs" in s.dbf_field_values.keys():
                self.assertEqual(s.dbf_field_values['process_logs']['original_projection'], None)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_geodetails_page3(self):
        """ This file contains no projection information"""
        self.field.boundary = None
        self.field.save()
        self.assertIsNone(self.field.boundary)
        with open("clients/tests/Blum 80.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_detail", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)

        fe = self.field.fieldevent_set.latest("pk")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }
        async_to_sync(background_worker)(message)

        sf = self.field.layer.all()
        self.assertEqual(sf.count(), 2)
        self.field.refresh_from_db()
        self.assertIsNotNone(self.field.boundary)
        self.assertEqual(len(self.field.boundary.coords), 2)

        for s in sf:
            if "process_logs" in s.dbf_field_values.keys():
                self.assertEqual(s.dbf_field_values['process_logs']['original_projection'], 4326)
                self.assertEqual(s.dbf_field_values['process_logs']['boundary_assumed'], True)

        resp = self.client.post(reverse("field_detail", kwargs={'pk': self.field.pk}),
                                {"name": self.field.name + "changed",
                                 "state": "IL",
                                 "county": "Champaign",
                                 "farm": self.field.farm.pk,
                                 })

        self.assertEqual(resp.status_code, 302)
        self.field.refresh_from_db()

        self.assertEqual(self.field.county, "Champaign")
        self.assertIn("changed", self.field.name)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_geodetails_page4(self):
        """ This file contains a single polygon field boundary"""

        with open("clients/tests/N40.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        self.assertEqual(fe.details.get("title"), "Shapefile Upload")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }
        async_to_sync(background_worker)(message)

        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 38.3)

        sf = SubfieldLayer.objects.filter(field=self.field)
        self.assertEqual(sf.count(), 2)

        for s in sf:
            if "process_logs" in s.dbf_field_values.keys():
                self.assertEqual(s.dbf_field_values['process_logs']['original_projection'], 4326)
        self.assertCleanup()

    @tag("single")
    def test_post_shape_zip_file_field_geodetails_page5(self):
        """ This file contains a single polygon field boundary and is uploaded to sample points
        We test that this does not create or force a boundary
        """

        with open("clients/tests/N40.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Sampling Locations")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        self.assertEqual(fe.details.get("title"), "Sampling Locations")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }
        async_to_sync(background_worker)(message)

        self.field.refresh_from_db()
        # self.assertEqual(self.field.area, 38.3)

        sf = SubfieldLayer.objects.filter(field=self.field)
        self.assertEqual(sf.count(), 2)

        for s in sf:
            if "process_logs" in s.dbf_field_values.keys():
                self.assertEqual(s.dbf_field_values['process_logs']['original_projection'], 4326)
        self.assertCleanup()

    def test_post_tiff_file_field_geodetails_page(self):
        """
        This file contains a tiff file
        """

        bdy = MultiPolygon([Polygon([[-91.608318, 41.374071], [-91.604177, 41.374004], [-91.604294, 41.366938],
                              [-91.606788, 41.36695], [-91.606784, 41.36707], [-91.606832, 41.367166],
                              [-91.606816, 41.3673], [-91.606814, 41.367366], [-91.606825, 41.367424],
                              [-91.606912, 41.367482], [-91.607012, 41.367522], [-91.607125, 41.367562],
                              [-91.607238, 41.367593], [-91.6073, 41.367632], [-91.607375, 41.36769],
                              [-91.607398, 41.367758], [-91.60746, 41.367816], [-91.607535, 41.367846],
                              [-91.607646, 41.367934], [-91.607772, 41.367964], [-91.607987, 41.367977],
                              [-91.608975, 41.367984], [-91.609063, 41.367995], [-91.609126, 41.368025],
                              [-91.609175, 41.368092], [-91.60921, 41.368198], [-91.609119, 41.373531],
                              [-91.60909, 41.373655], [-91.608976, 41.373662], [-91.608773, 41.373659],
                              [-91.608571, 41.373656], [-91.608508, 41.373656], [-91.608446, 41.373657],
                              [-91.608333, 41.373693], [-91.608274, 41.373823], [-91.608308, 41.373985],
                              [-91.608318, 41.374071]])])
        self.field.boundary = bdy
        self.field.save()

        with open("clients/tests/hora_ndvi.tif", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        self.assertEqual(fe.details.get("title"), "Shapefile Upload")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }
        self.assertIsNotNone(self.field.geojson_boundary())
        async_to_sync(background_worker)(message)

        self.field.refresh_from_db()

        sf = SubfieldLayer.objects.filter(field=self.field)
        self.assertEqual(sf.count(), 1)
        self.assertTrue(sf.first().rasterfile_set.count() > 0)

        self.assertCleanup()

    def test_contour_layer(self):
        with open("clients/tests/Wubben_DunsheeW150.zip", 'rb') as fp:
            evt = CalendarEvent.objects.get(name="Shapefile Upload")
            resp = self.client.post(reverse("field_geo_details", kwargs={'pk': self.field.pk}),
                                    {"name": self.field.name,
                                     "farm": self.field.farm.pk,
                                     "event": evt.pk,
                                     "event_date": "10/23/2018",
                                     "file": fp})

        self.assertEqual(resp.status_code, 302)
        fe = self.field.fieldevent_set.latest("pk")
        message = {
            "type": "process",
            "user": self.user.username,
            "fieldevent": fe.id,
            "field": self.field.id
        }

        async_to_sync(background_worker)(message)
        self.field.refresh_from_db()
        self.assertEqual(self.field.area, 151.2)

        sf = self.field.layer.get(
            name="Wubben, Julian - Dunshee West 150 - Dunshee West 150 - 2015 Soil Test Sites")

        lyr = reverse('field_geo_layer', kwargs={'pk': sf.field.pk,
                                                 'file': sf.name,
                                                 "subfield": sf.pk,
                                                 'parameter': "pH"})
        resp = self.client.get(lyr)
        self.assertEqual(resp.status_code, 200)

        sf.refresh_from_db()
        self.assertEqual(len(sf.dbf_field_values['pH']), sf.features)

        raster = reverse('field_geo_raster', kwargs={'pk': sf.field.pk,
                                                     'file': sf.name,
                                                     "subfield": sf.pk,
                                                     'parameter': "pH"})
        resp = self.client.get(raster)
        self.assertEqual(resp.status_code, 200)

        url = reverse('api_raster_polygonize', kwargs={'pk': sf.pk,
                                                       'parameter': 'pH'})
        resp = self.client.post(url)
        self.assertEqual(resp.status_code, 201)
        newsf = SubfieldLayer.objects.latest('pk')
        self.assertEqual(newsf.features, 7)
        self.assertTrue(newsf.polygons_only)
        self.assertEqual(len(newsf.get_values('pH')), 7)
        self.assertIn('pH', newsf.dbf_field_names['field_names'])
        print(newsf.get_values('pH'))
        print(newsf.get_values('ID'))
        self.assertTrue(all(i>0 for i in newsf.get_values('pH')))
        self.assertIsNotNone(newsf.source_file_event.id)




    def assertCleanup(self):
        pass
