from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management.base import BaseCommand
from tqdm import tqdm

from clients.models import Field, SubfieldLayer
from fieldcalendar.models import FieldEvent


class Command(BaseCommand):
    help = "Runs background shapefile processing for shapefile and location uploads without corresponding layers"

    def handle(self, *args, **options):

        fes = FieldEvent.objects.filter(event_type__name__in=["Shapefile Upload",
                                                              "Sampling Locations"])

        for f in tqdm(fes):
            print(f"Event {f.id}, file {f.file}")
            sf = SubfieldLayer.objects.filter(source_file=f.file)
            if sf.count() == 0:
                channel_layer = get_channel_layer()
                res = async_to_sync(channel_layer.send)("process-uploaded-shapefile", {
                    "type": "process",
                    "user": "kbhalerao",
                    "fieldevent": f.id,
                    "field": f.field.id
                })
                print(res)
            else:
                print(f"{sf.count()} layers found, {sf.values_list('id', 'name')}")






