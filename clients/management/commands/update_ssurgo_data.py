from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management.base import BaseCommand
from tqdm import tqdm

from clients.models import Field, SubfieldLayer, update_soil_layer
from fieldcalendar.models import FieldEvent
from orders.models import Order
from django.contrib.gis.geos import MultiPolygon

class Command(BaseCommand):
    help = "Update fields to latest SSURGO and USGS data"

    def handle(self, *args, **options):
        print("Updating fields with older nccpi2 data")
        nccpi2cs_fields = Field.objects.filter(layer__dbf_field_names__field_names__contains='nccpi2cs')
        for field in nccpi2cs_fields:
            update_soil_layer(field)
            print("Updated ", field)

        print("Looking for bounded fields with missing SSURGO data")
        completes = SubfieldLayer.objects.filter(name="Soil Survey Data").values_list('field_id', flat=True)
        missing = Field.objects.filter(boundary__isnull=False).exclude(id__in=completes)
        for field in missing:
            update_soil_layer(field)
            print("Updated ", field)