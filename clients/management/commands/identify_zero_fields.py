from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management.base import BaseCommand
from tqdm import tqdm

from clients.models import Field, SubfieldLayer
from fieldcalendar.models import FieldEvent
from orders.models import Order
from django.contrib.gis.geos import MultiPolygon

class Command(BaseCommand):
    help = "Identifies fields with zero boundaries"

    def handle(self, *args, **options):

        fes = Field.objects.filter(boundary__isnull=False)
        buf = 30 / 40000000.0 * 360.0
        for f in tqdm(fes):
            if f.area == 0:
                os = Order.objects.filter(field=f, id__gte=40000).values_list("order_number", flat=True)
                f.boundary = MultiPolygon([f.boundary.buffer(buf)])
                f.save()
                if os.count() > 0:
                    print(f"Field {f} with orders {os}")
                else:
                    print(f"Older field {f}")







