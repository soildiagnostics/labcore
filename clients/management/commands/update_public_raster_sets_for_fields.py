import time

from django.core.management.base import BaseCommand
from django.db.models import Q
from tqdm import tqdm

from clients.models import Field, update_soil_layer, SubfieldLayer
from common.lambda_utils import LambdaEventHandler


class Command(BaseCommand):
    help = "Runs update for soil rasters and/or public elevation rasters on all located Fields in this database"

    def add_arguments(self, parser):

        parser.add_argument('--soils',
                            action='store_true',
                            dest='soils_update',
                            # type=str,
                            help='Update the soils rasters only.')
        parser.add_argument('--elevation',
                            action='store_true',
                            dest='elevation_update',
                            # type=str,
                            help='Update the 10m public elevation and slope rasters only.')
        parser.add_argument('--field',
                            dest='field_pk',
                            action='store',
                            type=int,
                            help='Update a specific field by PK.')
        parser.add_argument('--fake',
                            action="store_true",
                            dest="fake",
                            help='Fake updates')

    def handle(self, *args, **options):

        soils_update = options.get('soils_update', False)
        elevation_update = options.get('elevation_update', False)
        update_both = not (soils_update or elevation_update)
        fake = options.get('fake', False)
        if options.get('field_pk', None):
            ssurgo_fields = Field.objects.filter(pk=options['field_pk'])
            usgs_fields = ssurgo_fields
        else:
            completes = SubfieldLayer.objects.filter(name="Soil Survey Data").values_list('field_id', flat=True)
            is_complete = Q(id__in=completes)
            has_boundary = Q(boundary__isnull=False)
            nccpi2fields = Q(layer__dbf_field_names__field_names__contains='nccpi2cs') & Q(layer__name="Soil Survey Data")
            ssurgo_fields = Field.objects.filter(nccpi2fields | (has_boundary & ~is_complete)).distinct()

            completes = SubfieldLayer.objects.filter(name="USGS 10m Elevation",
                                                     dbf_field_names__field_names__contains='elevation_public_10m').filter(dbf_field_names__field_names__contains='slope_public_10m').values_list('field_id', flat=True)
            is_complete = Q(id__in=completes)
            has_boundary = Q(boundary__isnull=False)
            usgs_fields = Field.objects.filter(has_boundary & ~is_complete).distinct()

        print(f"Total number of fields: {Field.objects.count()}")
        soils_num = 0
        elev_num = 0
        if soils_update or update_both:
            print(f"Beginning update of SSURGO layers for {ssurgo_fields.count()} field(s).")
            for f in tqdm(ssurgo_fields):
                if f.area and f.area > 0 and f.area < 1000:  ## omit some toy fields
                    if not fake:
                        time.sleep(0.5)
                        update_soil_layer(f, usgs=False)
                        soils_num += 1
        if elevation_update or update_both:
            print(f"Beginning update of USGS layers for {usgs_fields.count()} field(s).")
            for f in tqdm(usgs_fields):
                if f.area and f.area > 0 and f.area < 1000:  ## omit some toy fields
                    if not fake:
                        time.sleep(0.5)
                        gis = LambdaEventHandler(f)
                        resp = gis.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")
                        elev_num += 1
        print(f"Completed update of public data layers: SSURGO {soils_num}, USGS {elev_num}")
