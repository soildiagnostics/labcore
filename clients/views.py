import datetime

from asgiref.sync import async_to_sync
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404
from django.http import JsonResponse
from django.shortcuts import HttpResponseRedirect, redirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django_datatables_view.base_datatable_view import BaseDatatableView

from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin, FilteredObjectMixin
from clients.forms import (NewFarmForm, NewFieldForm, UpdateClientForm, UpdateFarmForm, UpdateFieldForm)
from clients.gis.shapezip_handler import ZippedShapeFile
from clients.models import Client, Farm, Field, VariabilityReport, PRESET_EVENTS
from common.consumers import send_ws_message
from common.searchquery import SearchQueryAssemblyMixin
from contact.forms import NewContactForm, PhoneModelFormSet, \
    EmailModelFormSet, AddressModelFormSet, inlineFormHelper
from contact.models import Company, Person, Contact
from fieldcalendar.models import CalendarEventKind, FieldEvent, CalendarEvent
from orders.models import Order
from products.models import EasyOrder
from products.models import Product
from channels.layers import get_channel_layer


class ClientList(FilteredQuerySetMixin, DealershipRequiredMixin, ListView):
    template_name = 'clients/client_list.html'
    model = Client

class ClientListJSON(FilteredQuerySetMixin, DealershipRequiredMixin,
                     SearchQueryAssemblyMixin, BaseDatatableView):
    model = Client
    columns = ['contact',
               'originator',
               'orders',
               'products']
    order_columns = ['contact',
                     'originator',
                     'orders',
                     'products']
    max_display_length = 500
    instance_owner_organization = "originator__organization"
    instance_owner_client = 'client'
    client_has_permission = False
    search_fields = [
        'contact__name',
        'contact__last_name',
        'originator__organization__name',
        'originator__user__person__last_name',
        'originator__user__person__name',
        'contact__contact__name',
        'contact__contact__last_name'
    ]
    common_queries = [
        'farms__fields__orders__products__name__icontains'
    ]

    def render_column(self, row, column):

        if column == 'contact':
            link = "<a href='{}'>{}</a> <a href='{}'>{}</a> {} {}".format(
                reverse("client_detail", kwargs={'pk': row.pk}),
                row.contact if row.active else "<s>{}</s>".format(row.contact),
                reverse("client_update", kwargs={'pk': row.contact.pk}),
                '<span class="pull-right"><i title="Edit contact information" class="fa fa-edit">&nbsp</i></span>',
                '<span class="pull-right"><i title="Can login" class="fa fa-user">&nbsp</i></span>' if row.is_user else '',
                '<span class="pull-right"><i title="Company" class="fa fa-building">&nbsp</i></span>' if row.contact.is_company else '',
            )
            return link
        if column == "orders":
            orders = Order.objects.filter(field__farm__client=row)
            return ", ".join([
                f'''<a href="{reverse('order_detail',
                                      kwargs={
                                          'pk': o.id})}" title="{o.date_created.date()}">{o.order_number}</a>'''
                for o in orders])
            # return orders.count()
        if column == "products":
            products = Product.objects.filter(order__field__farm__client=row).distinct()
            return ", ".join([p.name for p in products])
            # return orders.count()
        else:
            return super(ClientListJSON, self).render_column(row, column)


class FieldListJSON(DealershipRequiredMixin, FilteredQuerySetMixin,
                    SearchQueryAssemblyMixin, BaseDatatableView):
    model = Field
    columns = ['farm.client.contact',
               'farm',
               'name']
    order_columns = ['farm.client.contact',
                     'farm.name',
                     'name']
    max_display_length = 500
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = 'farm__client'
    client_has_permission = True

    search_fields = [
        'farm__client__contact__name',
        'farm__client__contact__last_name',
        'farm__client__originator__organization__name',
        'farm__client__originator__user__person__last_name',
        'farm__client__originator__user__person__name',
        'farm__name',
        'name'
    ]

    def get_initial_queryset(self):
        qs = super(FieldListJSON, self).get_initial_queryset()
        qs = qs.filter(farm__client__active=True)

        my_passes = self.request.user.hallpass.all()
        pass_fields = Field.objects.filter(hallpass__in=my_passes)

        qs.union(pass_fields)

        qs = qs.select_related(
            'farm', 'farm__client', 'farm__client__contact',
            'farm__client__originator__organization')

        return qs

    def render_column(self, row, column):
        if column == 'farm.client.contact':
            if self.is_client:
                link = "<a href='{}' title='{}'>{}</a>".format(
                    "#",
                    row.farm.client.originator,
                    row.farm.client.contact if row.farm.client.active else "<s>{}</s>".format(row.farm.client.contact),
                )
            else:
                link = "<a href='{}' title='{}'>{}</a> <a href='{}'>{}</a> {}".format(
                    reverse("client_detail", kwargs={'pk': row.farm.client.pk}),
                    row.farm.client.originator,
                    row.farm.client.contact if row.farm.client.active else "<s>{}</s>".format(row.farm.client.contact),
                    reverse("client_update", kwargs={'pk': row.farm.client.contact.pk}),
                    '<span class="pull-right"><i title="Edit contact information" class="fa fa-edit">&nbsp</i></span>',
                    '<span class="pull-right"><i title="Can login" class="fa fa-user">&nbsp</i></span>' if row.farm.client.is_user else ''
                )
            return link
        if column == 'farm':
            if self.is_client:
                link = "<a href='{}'>{}</a>".format(
                    "#",
                    row.farm.name
                )
            else:
                link = "<a href='{}'>{}</a>".format(
                    reverse("farm_detail", kwargs={'pk': row.farm.pk}),
                    row.farm.name
                )
            return link
        if column == 'name':
            if self.is_client:
                link = "<a href='{0}'>{1} {2}</a>".format(
                    reverse("field_detail", kwargs={'pk': row.pk}),
                    row.name, "({} acres)".format(row.area) if row.area else ""
                )
            else:
                link = "<a href='{0}'>{1} {2}</a>{3}".format(
                    reverse("field_detail", kwargs={'pk': row.pk}),
                    row.name, "({} acres)".format(row.area) if row.area else "",
                    EasyOrder.objects.get_easy_links(reverse("new_order", kwargs={'pk': row.pk}))
                )
            return link
        else:
            return super(FieldListJSON, self).render_column(row, column)


class ClientAdd(DealershipRequiredMixin, CreateView):
    form_class = NewContactForm
    model = Client
    template_name = "clients/client_form.html"
    success_url = reverse_lazy('client_list')
    instance_owner_client = None
    client_has_permission = False

    def get_context_data(self, **kwargs):
        context = super(ClientAdd, self).get_context_data(**kwargs)
        context['helper'] = inlineFormHelper()
        context['formtype'] = "Add a new Client"
        if self.request.POST:
            context['phone'] = PhoneModelFormSet(self.request.POST, instance=self.object)
            context['email'] = EmailModelFormSet(self.request.POST, instance=self.object)
            context['address'] = AddressModelFormSet(self.request.POST, instance=self.object)
        else:
            context['phone'] = PhoneModelFormSet(instance=self.object)
            context['email'] = EmailModelFormSet(instance=self.object)
            context['address'] = AddressModelFormSet(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        phone = context['phone']
        email = context['email']
        address = context['address']

        with transaction.atomic():
            self.object = form.save()
            if form.cleaned_data['client_type'] == "I":
                if form.cleaned_data["company_name"]:
                    ## if there is a company name, check if the company exists, and if it is related to
                    ## the current client through the originator
                    self.object.company = Company.objects.get_right_company(form.cleaned_data['company_name'],
                                                                            self.org)
                else:
                    self.object.company = None
                self.object.save()
            if form.cleaned_data['client_type'] == "C" and form.cleaned_data["first_name"]:
                if form.cleaned_data["company_contact"]:
                    person = Person.objects.get(id=form.cleaned_data["company_contact"])
                else:
                    person = Person.objects.create(name=form.cleaned_data.get("first_name"))
                person.company = self.object
                person.middle_name = form.cleaned_data.get("middle_name")
                person.last_name = form.cleaned_data.get("last_name")
                person.is_company_contact = True
                person.save()
            if phone.is_valid() and email.is_valid() and address.is_valid():
                phone.instance = self.object
                phone.save()
                email.instance = self.object
                email.save()
                address.instance = self.object
                address.save()

                c = Client.objects.create(contact=self.object,
                                          originator=self.orguser)
                # return super(ClientAdd, self).form_valid(form)
                if "_continue" in self.request.POST:
                    return HttpResponseRedirect(reverse_lazy('client_update', kwargs={'pk': c.contact.pk}))
                return HttpResponseRedirect(reverse_lazy('client_detail', kwargs={'pk': c.pk}))

            else:
                return self.render_to_response(self.get_context_data(form=form))


class ClientUpdate(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    form_class = NewContactForm
    model = Contact
    template_name = "clients/client_form.html"
    success_url = reverse_lazy('client_list')
    instance_owner_organization = "client__originator__organization"
    client_has_permission = True

    @staticmethod
    def remove_contact_person(form):
        person = Person.objects.get(id=form.cleaned_data["company_contact"])
        user = person.user
        if person.user:
            person.is_company_contact = False
            person.save()
            person.disable_user()
            # TODO  Add a notification to this person that their role has changed, and privileges
        else:
            person.delete()

    @staticmethod
    def copy_person_details(form, obj, new):
        if obj.user:
            user = obj.user
            obj.user = None
            obj.save()
            new.user = user
        new.company = obj
        new.middle_name = form.cleaned_data.get("middle_name")
        new.last_name = form.cleaned_data.get("last_name")
        new.is_company_contact = True
        new.save()

        for rec in obj.address.all():
            rec.pk = None
            rec.contact = new
            rec.save()
        for rec in obj.phone.all():
            rec.pk = None
            rec.contact = new
            rec.save()
        for rec in obj.email.all():
            rec.pk = None
            rec.contact = new
            rec.save()
        return obj

    def get_success_url(self, **kwargs):
        if "_continue" in self.request.POST:
            return reverse_lazy('client_update', args=[self.get_object().id])
        return reverse_lazy('client_detail', args=[self.get_object().client.id])

    # def get_form_kwargs(self):
    #     kwargs = super(ClientUpdate, self).get_form_kwargs()
    #     if self.object.is_company and self.object.has_company_contact:
    #         kwargs['company_contact'] = self.object.get_company_contact()
    #     return kwargs

    def get_context_data(self, **kwargs):
        context = super(ClientUpdate, self).get_context_data(**kwargs)
        context['helper'] = inlineFormHelper()
        context['formtype'] = "Update Client"
        if self.object.is_company:
            context['company'] = Company.objects.get(pk=self.object.pk)
        else:
            context['person'] = Person.objects.get(pk=self.object.pk)

        if self.request.POST:
            context['phone'] = PhoneModelFormSet(self.request.POST, instance=self.object)
            context['email'] = EmailModelFormSet(self.request.POST, instance=self.object)
            context['address'] = AddressModelFormSet(self.request.POST, instance=self.object)
        else:
            context['phone'] = PhoneModelFormSet(instance=self.object)
            context['email'] = EmailModelFormSet(instance=self.object)
            context['address'] = AddressModelFormSet(instance=self.object)
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data()
        phone_form = context['phone']
        email_form = context['email']
        address_form = context['address']

        if (form.is_valid() and phone_form.is_valid() and email_form.is_valid() and address_form.is_valid()):
            return self.form_valid(form, phone_form, email_form, address_form)
        # print('form invalid: ', form.errors, 'phone: ', phone_form.errors, 'email:  ', email_form.errors, 'address:  ', address_form.errors)
        return self.form_invalid(form)

    # def form_invalid(self, form, phone_form, email_form, address_form):
    #     import pdb; pdb.set_trace()
    #     response = super().form_invalid(form)
    #     return response

    def form_valid(self, form, phone_formset, email_formset, address_formset):

        self.object = form.save()
        phone_formset.instance = self.object
        phone_formset.save()
        email_formset.instance = self.object
        email_formset.save()
        address_formset.instance = self.object
        address_formset.save()

        if form.cleaned_data['client_type'] == "I":
            p = Person.objects.get(id=self.object.id)
            if hasattr(p, "user") and p.user:
                try:
                    p.user.email = p.login_email.email
                    p.user.save()
                except Exception as e:
                    print(e)

            if form.cleaned_data["company_name"]:
                ## if there is a company name, check if the company exists, and if it is related to
                ## the current client through the originator
                self.object.company = Company.objects.get_right_company(form.cleaned_data["company_name"],
                                                                        self.org)
            else:
                self.object.company = None
            co_contact = form.cleaned_data.get("company_contact")
            if co_contact and co_contact != self.object.id:
                self.remove_contact_person(form)
        if form.cleaned_data['client_type'] == "C" and form.cleaned_data.get("first_name"):
            # case individual client changed to company with the same or contact details given
            self.object.last_name = ""
            self.object.middle_name = ""
            if form.cleaned_data["company_contact"]:
                person = Person.objects.get(id=form.cleaned_data["company_contact"])
            else:
                person = Person.objects.create(name=form.cleaned_data.get("first_name"))
                self.copy_person_details(form, self.object, person)
            self.object.company = None
        elif form.cleaned_data['client_type'] == "C" and not form.cleaned_data.get("first_name"):
            co_contact = form.cleaned_data.get("company_contact")
            if co_contact and co_contact != self.object.id:
                self.remove_contact_person(form)
            self.object.company = None
        self.object.save()
        if self.object.is_company:
            self.object.disable_user()

        #     # c = Client.objects.get(contact=self.object)
        #     # return HttpResponseRedirect(reverse_lazy('client_detail', kwargs={'pk': c.pk}))
        return HttpResponseRedirect(self.get_success_url())
        # else:
        #     return self.render_to_response(self.get_context_data(form=form))


class ClientDetail(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    form_class = UpdateClientForm
    model = Client
    template_name = "clients/client_update.html"
    success_url = reverse_lazy('client_list')
    instance_owner_organization = "originator__organization"
    instance_owner_client = ""
    client_has_permission = True

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.prefetch_related("farms", "farms__fields")
        return qs

    def get_form_kwargs(self):
        kwargs = super(ClientDetail, self).get_form_kwargs()
        kwargs['org'] = self.org
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ClientDetail, self).get_context_data(**kwargs)
        context['client'] = self.object
        context['formtype'] = "Edit existing Client"
        context['fields'] = Field.objects.filter(farm__client=self.object)
        context['google_api_key'] = settings.GOOGLE_API_KEY
        return context


class ClientDelete(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    # permission_required = 'clients.delete_client'
    model = Client
    success_url = reverse_lazy('client_list')
    instance_owner_organization = "originator__organization"
    client_has_permission = False

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.active = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class FarmAdd(DealershipRequiredMixin, CreateView):
    model = Farm
    form_class = NewFarmForm
    template_name = "clients/new_farm.html"
    client_has_permission = True
    instance_owner_client = 'client'

    def get_form_kwargs(self):
        kwargs = super(FarmAdd, self).get_form_kwargs()
        kwargs['client'] = self.kwargs['pk']
        kwargs['org'] = self.org
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(FarmAdd, self).get_context_data(**kwargs)
        try:
            context['client'] = Client.objects.get(pk=self.kwargs['pk'])
            if not self.org.is_primary:
                assert (context['client'].originator.organization == self.org)
            context['farms'] = Farm.objects.filter(client=self.kwargs['pk'])
            context['fields'] = Field.objects.filter(
                farm__client=self.kwargs['pk'])
            # context['orders'] = OrderForm.objects.filter(
            #    field__farm__client=self.kwargs['pk'])
            context['formtype'] = "Add a new Farm"
            context['google_api_key'] = settings.GOOGLE_API_KEY
            return context
        except AssertionError:
            raise Http404("Organizations do not match!")

    # def get_success_url(self):
    #     return reverse_lazy('client_detail', kwargs={'pk': self.object.client.pk})


class FarmDelete(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    permission_required = 'clients.delete_farm'
    model = Farm
    instance_owner_organization = "client__originator__organization"
    client_has_permission = False
    instance_owner_client = 'client'

    def get_success_url(self):
        return reverse_lazy('client_detail', kwargs={'pk': self.object.client.pk})


class FarmDetail(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    # permission_required = "clients.add_farm"
    model = Farm
    form_class = UpdateFarmForm
    template_name = "clients/farm_detail.html"
    instance_owner_organization = "client__originator__organization"
    instance_owner_client = 'client'
    client_has_permission = True

    def get_success_url(self):
        farm = Farm.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('client_detail', kwargs={'pk': farm.client.pk})

    def get_form_kwargs(self):
        kwargs = super(FarmDetail, self).get_form_kwargs()
        farm = Farm.objects.get(pk=self.kwargs['pk'])
        kwargs['client'] = farm.client.pk
        kwargs['farm'] = farm.pk
        kwargs['org'] = self.org
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(FarmDetail, self).get_context_data(**kwargs)
        farm = Farm.objects.get(pk=self.object.pk)
        context['farm'] = farm
        context['client'] = farm.client
        context['fields'] = Field.objects.filter(farm=self.object.pk)
        context['formtype'] = "Edit existing Farm"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        return context


class FieldList(FilteredQuerySetMixin, DealershipRequiredMixin, ListView):
    model = Field
    template_name = "clients/field_list.html"
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = False


class FieldAdd(DealershipRequiredMixin, CreateView):
    permission_required = 'clients.add_field'
    model = Field
    form_class = NewFieldForm
    template_name = "clients/new_field.html"
    client_has_permission = True
    instance_owner_client = 'farm__client'

    # def get_success_url(self):
    #     farm = Farm.objects.get(pk=self.kwargs['pk'])
    #     return reverse_lazy('client_detail', kwargs={'pk': farm.client.pk})

    def get_form_kwargs(self):
        kwargs = super(FieldAdd, self).get_form_kwargs()
        kwargs['farm'] = self.kwargs['pk']
        kwargs['org'] = self.org
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(FieldAdd, self).get_context_data(**kwargs)
        try:
            farm = Farm.objects.get(pk=self.kwargs['pk'])
            if not self.org.is_primary:
                assert (farm.client.originator.organization == self.org)
            context['formtype'] = "Add a new Field or Facility"
            context['google_api_key'] = settings.GOOGLE_API_KEY
            context['farm'] = farm
            context['client'] = farm.client
            return context
        except AssertionError:
            raise PermissionDenied()
        except Farm.DoesNotExist:
            raise Http404("Farm does not exist")

    def post(self, request, *args, **kwargs):
        result = super().post(request, *args, **kwargs)
        if "another" in request.POST:
            return HttpResponseRedirect(request.path)
        return result


class FieldFileDropzone(DealershipRequiredMixin, DetailView):

    def post(self, request, pk):
        field = Field.objects.get(pk=pk)
        file = self.request.FILES['file']

        fe = FieldEvent.objects.create_journal_entry(
            field=field,
            title="Dropped File",
            entry=f"File {file.name} added",
            start=timezone.now(),
            end=timezone.now()
        )

        fe.file.save(f"{field.id}/{file.name}", file)
        fe.save()

        return JsonResponse({'status': 'success'})




class FieldDetail(FilteredObjectMixin, DealershipRequiredMixin, UpdateView):
    model = Field
    form_class = UpdateFieldForm
    template_name = "clients/field_detail.html"
    instance_owner_organization = "farm__client__originator__organization"
    client_has_permission = True
    instance_owner_client = 'farm__client'
    hallpass_field = ''

    def post(self, request, *args, **kwargs):
        result = super().post(request, *args, **kwargs)
        if "another" in request.POST:
            return HttpResponseRedirect(reverse_lazy("new_field", kwargs={'pk': self.get_object().farm.pk}))
        return result

    def get_queryset(self):
        qs = super().get_queryset()
        if self.__class__.__name__ == "FieldDetail":
            qs = qs.prefetch_related('fieldevent_set')
        return qs

    def get_form_kwargs(self):
        kwargs = super(FieldDetail, self).get_form_kwargs()
        # field = Field.objects.get(pk=self.kwargs['pk'])
        field = self.get_object()
        kwargs['farm'] = field.farm.pk
        kwargs['field'] = field.pk
        kwargs['org'] = self.org
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(FieldDetail, self).get_context_data(**kwargs)
        field = self.get_object()
        context['farm'] = field.farm
        context['client'] = field.farm.client
        context['formtype'] = "Edit existing Field or Facility"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        if self.__class__.__name__ == "FieldDetail":
            ## Don't add to this context if FieldGeoDetails or another subclass is using this context
            context['events'] = CalendarEventKind.objects.user_editable(self.org)
            context['variability_reports'] = VariabilityReport.objects.filter(image__layer__field=field).order_by(
                '-generated')
            files = field.fieldevent_set.exclude(file="").order_by("-modified")
            filtered_files = list()
            for f in files:
                if self.request.user.is_staff:
                    filtered_files.append(f)
                else:
                    if not f.details.get('order'):
                        ## Files created in the course of an order may have the keyword "order" in the details field.
                        filtered_files.append(f)
                    else:
                        try:
                            o = Order.objects.get(id=f.details['order'])
                            if o.order_status.name == "published":
                                filtered_files.append(f)
                        except Exception:
                            pass

            context['files'] = filtered_files
        context['is_client'] = self.is_client

        return context

    # def get_success_url(self):
    #     field = Field.objects.get(pk=self.kwargs['pk'])
    #     return reverse_lazy('field_detail', kwargs={'pk': field.pk})

    def form_valid(self, form):
        new_boundary = None
        field = self.get_object()

        if form.cleaned_data['event'] and form.cleaned_data['event_date'] and form.cleaned_data['file']:

            date = datetime.datetime.strptime(form.cleaned_data['event_date'], "%m/%d/%Y")
            file = self.request.FILES['file']
            try:
                event_type = CalendarEvent.objects.get(name=form.cleaned_data['event'].name)
            except (CalendarEvent.DoesNotExist, CalendarEvent.MultipleObjectsReturned):
                event_type = None

            ## create an event for the file upload.
            fe = FieldEvent.objects.create_system_event(
                field=field,
                title=event_type.name,
                start=timezone.make_aware(date),
                end=timezone.make_aware(date),
                allDay=False
            )
            fe.event_type = event_type

            # if form.cleaned_data['event'].name in ["Shapefile Upload", "Sampling Locations"]:
            if form.cleaned_data['event'].name in PRESET_EVENTS:
                fe.file.save(f"shapefiles/{field.id}/{file.name}", file)
                fe.save()
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.send)("process-uploaded-shapefile", {
                    "type": "process",
                    "user": self.request.user.username,
                    "fieldevent": fe.id,
                    "field": field.id,
                })
            else:
                fe.file.save(f"uploads/{field.id}/{file.name}", file)
                fe.save()

        form.save(commit=False)

        return super().form_valid(form)


class FieldAddress(FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = 'farm__client'
    client_has_permission = True
    hallpass_field = ""

    def get(self, request, pk):
        field = Field.objects.get(pk=pk)
        if field.farm.client.originator.organization == self.org or self.request.user.is_staff:
            address = field.get_address()
            return JsonResponse({'address': address}, safe=False)
        else:
            return JsonResponse({'status': 'fail',
                                 'message': "Organizations do not match!"})


class FieldDelete(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    permission_required = 'clients.delete_field'
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"
    client_has_permission = False

    def get_success_url(self):
        field = Field.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('client_detail', kwargs={'pk': field.farm.client.pk})
