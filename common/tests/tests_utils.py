import json
import os
from unittest import skip

import boto3
from django.conf import settings
from django.contrib.sites.models import Site
from django.test import TestCase
from django.test.utils import override_settings
from six.moves.urllib.parse import quote

# from clients.tests.tests_models import ClientsTestCase
from clients.tests import tests_helpers
from common.lambda_utils import LambdaEventHandler


# from contact.tests.tests_models import ContactTestCase

@override_settings(IN_TEST=True)
class TestLambdaHandler(TestCase):
    fixtures = ['roles']

    def setUp(self):

        self.maxDiff = None

        Site.objects.create(domain="localhost", name="localhost")
        self.field = tests_helpers.createField()
        self.boundary = self.field.boundary
        self.geojson = {'type': 'FeatureCollection',
                        'features': [{'type': 'Feature',
                                      'geometry': {'type': 'MultiPolygon',
                                                   'coordinates': [[[[-88.305955, 40.20368],
                                                                     [-88.306068, 40.2053],
                                                                     [-88.305983, 40.207821],
                                                                     [-88.301247, 40.207823],
                                                                     [-88.30119, 40.200722],
                                                                     [-88.30582, 40.20075],
                                                                     [-88.305955, 40.20368]]]]}}]
                        }

        self.example_config = [{'bucket': 'soildx-clip-13dem-to-field',
                                'jobs': [{'lambda_function': 'elevation_public_10m',
                                          'post_back': {'file_extensions': ('tif', 'png'),
                                                        '            parameter': 'elevation_public_10m',
                                                        'layer': "USGS 10m Elevation"
                                                        }},
                                         {'lambda_function': 'slope_public_10m',
                                          'post_back': {'file_extensions': ('tif', 'png'),
                                                        'parameter': 'slope_public_10m',
                                                        'layer': "USGS 10m Elevation"
                                                        }}]}
                               ]
        if settings.AWS_LAMBDA_EVENT_CONFIGURATION.keys():
            self.config = settings.AWS_LAMBDA_EVENT_CONFIGURATION[
                list(settings.AWS_LAMBDA_EVENT_CONFIGURATION.keys())[0]]
        else:
            self.config = None

    def test_lambda_init(self):

        sheep = LambdaEventHandler(self.field)
        self.assertEqual(sheep.field, self.field)
        # self.assertEqual(sheep.lambda_function, lambda_func)
        # self.assertEqual(sheep.lambda_bucket, bucket)
        # self.assertEqual(sheep.output_extension, output_extension)
        self.assertListEqual(sheep.lambda_event_file_key.split('_')[0:4],
                             [settings.SOILDX_CHILD_WEBSITE_PREFIX,
                              'field',
                              str(self.field.pk),
                              'boundary'],
                             )
        self.assertNotIn(":", sheep.lambda_event_file_key)
        self.assertNotIn("%", quote(sheep.lambda_event_file_key))
        # self.assertEqual(sheep.output_file_key, '{}_{}.{}'.format(self.field.pk, lambda_func, output_extension))
        # self.assertEqual(sheep.post_url, '/clients/field/{}/gis/{}/{}/raster/post/'.format(self.field.pk,
        #                                                                                     sheep.output_file_key,
        #                                                                                     lambda_func))

    def test_lambda_geojson(self):
        sheep = LambdaEventHandler(self.field)

        geojson = sheep.geojson_boundary(self.field)
        self.assertEqual(geojson['type'], 'FeatureCollection')
        self.assertEqual(len(geojson['features']), 1)
        self.assertEqual(geojson['features'][0]['geometry']['type'], 'MultiPolygon')
        self.assertEqual(geojson['features'][0]['geometry']['coordinates'][0][0][0][0], -91.297717)

    def test_full_post_uri(self):
        l = LambdaEventHandler(self.field)
        url = l.full_post_uri('http://example.com', 'test.tif', 'parameter', 'a layer')
        self.assertEqual(url,
                         f'http://example.com/clients/field/{self.field.id}/gis/test.tif/parameter/a%20layer/raster/post/')

    def test_lambda_job_json(self):
        l = LambdaEventHandler(self.field)
        post_back = {
            "file_extensions": ("tif", "png"),
            "parameter": "slope_public_10m",
            "layer": "USGS 10m Elevation"
        }

        job_resp = {'metadata':
                        {'function_name': 'lambda_func',
                         'field_boundary':
                             {'type': 'FeatureCollection',
                              'features': [{'type': 'Feature',
                                            'geometry': {'type': 'MultiPolygon',
                                                         'coordinates': [[[[-88.305955, 40.20368],
                                                                           [-88.306068, 40.2053],
                                                                           [-88.305983, 40.207821],
                                                                           [-88.301247, 40.207823],
                                                                           [-88.30119, 40.200722],
                                                                           [-88.30582, 40.20075],
                                                                           [-88.305955, 40.20368]]]]}}]},
                         'input_data': None,
                         'field_id': self.field.id,
                         'soildx_prefix': 'test'},
                    'post': {'domain': 'http://www.example.com',
                             'parameter': 'slope_public_10m',
                             'layer': 'USGS 10m Elevation',
                             'output': [{'filename': 'slope_public_10m.tif',
                                         'extension': 'tif',
                                         'post_url': f'http://www.example.com/clients/field/{self.field.id}/gis/slope_public_10m.tif/slope_public_10m/USGS%2010m%20Elevation/raster/post/'},
                                        {'filename': 'slope_public_10m.png',
                                         'extension': 'png',
                                         'post_url': f'http://www.example.com/clients/field/{self.field.id}/gis/slope_public_10m.png/slope_public_10m/USGS%2010m%20Elevation/raster/post/'}]}}

        job = l.lambda_job_json(geojson=self.geojson,
                                lambda_function='lambda_func',
                                domain='http://www.example.com',
                                soildx_prefix='test',
                                post_back=post_back)

        self.assertDictEqual(job, job_resp)

    def test_lambda_event_json(self):
        sheep = LambdaEventHandler(self.field)

        if self.config:
            config = self.config
            event = sheep.lambda_event_json(config, self.geojson)
            self.assertEqual(len(event), len(config['jobs']))
            job1 = event[0]
            self.assertEqual(job1['metadata']['function_name'], config['jobs'][0]['lambda_function'])
            self.assertDictEqual(job1['metadata']['field_boundary'], self.geojson)
            self.assertEqual(job1['metadata']['field_id'], self.field.id)
            self.assertEqual(job1['metadata']['soildx_prefix'], settings.SOILDX_CHILD_WEBSITE_PREFIX)
            self.assertEqual(job1['post']['domain'], "http://example.com")
            self.assertEqual(job1['post']['parameter'], config['jobs'][0]['post_back']['parameter'])
            self.assertEqual(job1['post']['output'][0]['extension'],
                             config['jobs'][0]['post_back']['file_extensions'][0])
            expected_fn = f"{config['jobs'][0]['post_back']['parameter']}.{config['jobs'][0]['post_back']['file_extensions'][0]}"
            self.assertEqual(job1['post']['output'][0]['filename'], expected_fn)
            expected_url = f"http://example.com/clients/field/{self.field.id}/gis/{expected_fn}/{config['jobs'][0]['post_back']['parameter']}/{quote(config['jobs'][0]['post_back']['layer'])}/raster/post/"
            self.assertEqual(job1['post']['output'][0]['post_url'], expected_url)

    def test_lambda_event_json_no_boundary(self):
        self.field.boundary = None
        self.field.save()

        sheep = LambdaEventHandler(self.field)
        resp = sheep.write_lambda_events('public_elev_10m')
        self.assertFalse(resp['status'])

        self.field.boundary = self.boundary
        self.field.save()

    def test_bad_lambda_config(self):
        sheep = LambdaEventHandler(self.field)
        with self.assertRaisesMessage(NotImplementedError,
                                      'AWS_LAMBDA_EVENT_CONFIGURATION settings for <KEY> are not set.'):
            sheep.write_lambda_events('KEY')

    def test_lambda_event_to_local_file(self):

        sheep = LambdaEventHandler(self.field)

        if self.config:
            json_data = sheep.lambda_event_json(self.config, self.geojson)
            temp_file = sheep.lambda_event_to_temp_file(json_data)

            with open(temp_file, 'rb') as f:
                data = json.load(f)
            self.assertIn('metadata', data[0])
            self.assertIn('post', data[0])

            # remove the temp file
            os.unlink(temp_file)

    def test_lambda_event_to_s3(self):

        sheep = LambdaEventHandler(self.field)

        if self.config:
            json_data = sheep.lambda_event_json(self.config, self.geojson)
            temp_file = sheep.lambda_event_to_temp_file(json_data)
            self.assertEqual(temp_file.split('.')[1], 'json')
            resp = sheep.write_field_to_lambda_bucket(json_data, 'file_key.tif', 'receiving-bucket')
            self.assertTrue(resp['status'])
            self.assertEqual(resp['message'], 'Field boundary upload success')

            os.unlink(temp_file)

    def test_full_upload_function(self):

        sheep = LambdaEventHandler(self.field)
        resp = sheep.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")
        self.assertTrue(resp['status'])
        self.assertEqual(resp['message'], 'Field boundary upload success')

        self.field.boundary = None
        self.field.save()
        sheep = LambdaEventHandler(self.field)
        resp = sheep.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")
        self.assertFalse(resp['status'])
        self.assertEqual(resp['message'], 'No boundary to upload')

        # reset the field boundary for further tests
        self.field.boundary = self.boundary
        self.field.save()


@skip('Skip test of true s3 resources')
@override_settings(IN_TEST=False)
class LiveLambdaTest(TestCase):

    def setUp(self):
        TestLambdaHandler.setUp(self)

    def test_boto3_conn_for_lambda(self):
        input_file_key = '/test_elev_raster.tif'
        file_to_upload = '/Users/kdbhome/VMGMS/src/soildx-labcore/clients/tests/test_elev_raster.tif'
        lambda_bucket = 'soildx-clip-13dem-to-field'

        s3_client = boto3.client('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        resp = s3_client.upload_file(file_to_upload, lambda_bucket, input_file_key)

    def test_upload_event_to_lambda_bucket(self):
        sheep = LambdaEventHandler(self.field)
        resp = sheep.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")
        self.assertTrue(resp['status'])
        self.assertTrue(resp, sheep.upload_success_msg)
