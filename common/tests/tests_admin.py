from io import StringIO

from django.contrib.sites.models import Site
from django.core.management import call_command
from django.test import TestCase, Client

from contact.models import User
from labcore.wsgi import application


class TestAdminPanel(TestCase):
    def setUp(self):
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
        Site.objects.create(domain="localhost", name="localhost")

    def test_spider_admin(self):
        client = Client()
        client.login(username=self.username, password=self.password)
        admin_pages = [
            "/admin/",
            # put all the admin pages for your models in here.
            # "/admin/auth/",
            # "/admin/auth/group/",
            # "/admin/auth/group/add/",
            # "/admin/auth/user/",
            # "/admin/auth/user/add/",
            "/admin/password_change/",
            "/admin/contact/",
            "/admin/contact/contact/",
            "/admin/contact/contact/add/",
            "/admin/contact/user/",
            "/admin/contact/user/add/",
            "/admin/contact/person/",
            "/admin/contact/person/add/",
            "/admin/associates/organization/",
            "/admin/associates/organization/add/"
        ]
        for page in admin_pages:
            resp = client.get(page)

            self.assertEqual(resp.status_code, 200)


    def test_common_views(self):
        client = Client()
        client.login(username=self.username, password=self.password)
        common_pages = [
            "",
        ]
        for page in common_pages:
            resp = client.get(page)
            self.assertEqual(resp.status_code, 200)

    def test_login_views(self):
        # Pages that you will be redirected if you aren't logged in
        client = Client()
        common_pages = [
            '/auth/logout/',
            '/clients/'
        ]
        for page in common_pages:
            resp = client.get(page)
            self.assertEqual(resp.status_code, 302)
        client.login(username=self.username, password=self.password)
        common_pages = [
            "/auth/login/",
            "",
        ]
        # Pages that will be visible when you're logged in 
        for page in common_pages:
            resp = client.get(page)
            self.assertEqual(resp.status_code, 200)


class TestManagementCommands(TestCase):
    def test_createsu(self):
        out = StringIO()
        call_command('createsu', stdout=out)
        self.assertEqual('', out.getvalue())

class TestWSGIapplication(TestCase):
    def test_wsgi_application(self):
        self.assertNotEqual(application, None)
