from channels.routing import ChannelNameRouter
from django.urls import path

from clients.consumers import GISProcessConsumer, ShapefileProcessConsumer
from common.consumers import ChatConsumer

urlpatterns = [
    path('clients/field/<int:pk>/gis/ws/', GISProcessConsumer),
    path('chat/ws/<str:username>/', ChatConsumer),
]

channel_consumer = ChannelNameRouter({
    "process-uploaded-shapefile": ShapefileProcessConsumer
})
