import json
import os
import tempfile
from datetime import datetime
import logging

import boto3
from django.conf import settings
from django.urls import reverse

from common.apihelper import current_site_url
logger = logging.getLogger(__name__)

def update_usgs(field):
    lambda_handler = LambdaEventHandler(fieldobj=field)
    lambda_handler.write_lambda_events(PROCESS_CONFIG_KEY="public_elev_10m")

class LambdaEventHandler(object):

    def __init__(self, fieldobj):
        self.field = fieldobj
        self.lambda_event_file_key = self._input_file_key(fieldobj)
        self.upload_success_msg = {"status": True, "message": 'Field boundary upload success'}
        self.upload_fail_msg = {"status": False, "message": 'Unsuccessful upload to lambda_processor'}
        self.no_data_to_upload_msg = {"status": False, "message": 'No boundary to upload'}

    @staticmethod
    def _input_file_key(fieldobj):
        return '{site_prefix}_field_{field_pk}_boundary_{datetime}.json'.format(
            site_prefix=settings.SOILDX_CHILD_WEBSITE_PREFIX,
            # aws_prefix=settings.AWS_STORAGE_BUCKET_NAME,
            field_pk=fieldobj.pk,
            field_name=fieldobj.name,
            datetime=datetime.now().isoformat().split('.')[0].replace(":", "_")
        )

    @staticmethod
    def _output_file_key(lambda_function, output_extension):
        return '{lambda_function}.{file_extension}'.format(
            lambda_function=lambda_function,
            file_extension=output_extension
        )

    def check_settings(self):
        if settings.SOILDX_CHILD_WEBSITE_PREFIX is None:
            raise NotImplementedError('SOILDX_CHILD_WEBSITE_PREFIX must be set for each child website.')
        if settings.AWS_LAMBDA_EVENT_CONFIGURATION is None:
            raise NotImplementedError('AWS_LAMBDA_EVENT_CONFIGURATION must be setup for the website.')

    def post_url(self, postback_filename, parameter, layer):
        return reverse('field_geo_raster_post',
                       kwargs={'pk': self.field.pk,
                               'file': postback_filename,
                               "parameter": parameter,
                               "layer": layer})

    def full_post_uri(self, domain, postback_filename, parameter, layer):
        "Compose a fully qualified uri for posting back the processed files into the django database"
        return '{}{}'.format(domain, self.post_url(postback_filename, parameter, layer))

    @staticmethod
    def featurecollection_helper(geojson):
        if geojson['type'] == 'FeatureCollection':
            return geojson
        return {"type": "FeatureCollection",
                "features": [{"type": "Feature",
                              "geometry": geojson}
                             ]}

    def geojson_boundary(self, fieldobj):
        if not fieldobj.boundary:
            return None
        if fieldobj.boundary.area == 0.0:
            return None
        return self.featurecollection_helper(json.loads(fieldobj.boundary.geojson))

    def lambda_job_json(self, geojson, lambda_function, domain, soildx_prefix, post_back, other_input_data=None):
        "Fill in the lambda processing event metadata"
        try:
            output_specs = []
            for ext in post_back['file_extensions']:
                output_file_key = self._output_file_key(post_back['parameter'], ext)
                out_dict = {"filename": output_file_key,
                            "extension": ext,
                            "post_url": self.full_post_uri(domain, output_file_key, post_back['parameter'],
                                                           post_back['layer']),
                            }
                output_specs.append(out_dict)

            return {"metadata": {
                "function_name": lambda_function,
                "field_boundary": geojson,
                "input_data": other_input_data,
                "field_id": self.field.id,
                "soildx_prefix": soildx_prefix,
            },
                "post": {
                    "domain": domain,
                    "parameter": post_back['parameter'],
                    "layer": post_back['layer'],
                    "output": output_specs
                }
            }
        except KeyError:
            raise Exception('Improperly configured AWS_LAMBDA_EVENT_CONFIGURATION.')

    def lambda_event_json(self, config, geojson):
        "Compose the jobs for an event into a single json dictionary"
        events = []
        for job in config['jobs']:
            job['geojson'] = geojson
            job['domain'] = config['domain']
            job['soildx_prefix'] = config['soildx_prefix']
            event = self.lambda_job_json(**job)
            events.append(event)
        return events

    def lambda_event_to_temp_file(self, event_json):
        "Dump the event json to a temporary file to upload to S3"
        try:
            with tempfile.NamedTemporaryFile(suffix='.json', delete=False) as fp:
                fp.write(json.dumps(event_json).encode('utf-8'))
            return fp.name
        except Exception as e:
            print('EXCEPTION IN TEMP FILE CREATION:  ', e)
            return None

    def write_field_to_lambda_bucket(self, event_json, receiver_bucket, file_key):
        "S3 Write function for the event, with file key and bucket from the config"
        if getattr(settings, 'IN_TEST', False):
            return self.upload_success_msg

        file_to_upload = self.lambda_event_to_temp_file(event_json)
        try:
            s3_client = boto3.client('s3',
                                     aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
            s3_client.upload_file(file_to_upload, receiver_bucket, file_key)
            # remove the intermediate file
            os.unlink(file_to_upload)
            return self.upload_success_msg
        except Exception as e:
            print('\nException in S3 upload:  ', e)
            return self.upload_fail_msg

    def write_lambda_events(self, PROCESS_CONFIG_KEY):
        "Publish the lambda event(s) to the lambda receiver bucket(s)"
        if not settings.AWS_LAMBDA_EVENT_CONFIGURATION.get(PROCESS_CONFIG_KEY):
            raise NotImplementedError(
                f'AWS_LAMBDA_EVENT_CONFIGURATION settings for <{PROCESS_CONFIG_KEY}> are not set.')

        geojson = self.geojson_boundary(self.field)
        #geojson = self.field.geojson_boundary()

        if geojson:
            config = settings.AWS_LAMBDA_EVENT_CONFIGURATION[PROCESS_CONFIG_KEY]
            config['geojson'] = geojson
            config['domain'] = current_site_url()
            config['soildx_prefix'] = settings.SOILDX_CHILD_WEBSITE_PREFIX
            event = self.lambda_event_json(config, geojson)
            resp = self.write_field_to_lambda_bucket(event, config['bucket'], self.lambda_event_file_key)
            logger.log(logging.INFO, resp)
            return resp
        logger.log(logging.INFO, self.no_data_to_upload_msg)
        return self.no_data_to_upload_msg
