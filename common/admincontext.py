from associates.models import OrganizationUser
from django.conf import settings
from common.models import Announcement


def dashboard_context(request):
    context = {}
    perms = request.user.get_all_permissions()
    perms = [p for p in perms if p in ["fertisaverN.add_filledsample"]]
    context['permissions'] = perms
    context['GATrackerID'] = settings.GOOGLE_TRACKER
    context['announcements'] = Announcement.objects.get_current()
    try:
        try:
            ouser = OrganizationUser.objects.select_related(
                "organization", "organization__company").prefetch_related('organization__roles').get(user=request.user)
        except OrganizationUser.MultipleObjectsReturned:
            ouser = OrganizationUser.objects.select_related(
                "organization", "organization__company").prefetch_related('organization__roles').filter(user=request.user).first()
        context['orguser'] = ouser
        context['roles'] = [r.role for r in ouser.organization.roles.all()]
        context['orgimage'] = ouser.organization.company.image.url if ouser.organization.company.image else None
    except OrganizationUser.DoesNotExist:
        try:
            if hasattr(request.user.person, 'client'):
                context['orgimage'] = request.user.person.client.originator.organization.company.image.url
            elif hasattr(request.user.person.company, 'client'):
                context['orgimage'] = request.user.person.company.client.originator.organization.company.image.url
            else:
                context['orgimage'] = None
        except Exception:
            pass
    except (TypeError, ValueError):
        pass
    return context
