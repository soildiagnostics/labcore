import boto3
from botocore.client import Config
from dateutil.parser import parse
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db import connection
from django.forms.models import model_to_dict
from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now
from django.utils.functional import cached_property



def prefetch_id(instance):
    """ Fetch the next value in a django id autofield postgresql sequence """
    cursor = connection.cursor()
    cursor.execute(
        "SELECT nextval('{0}_{1}_id_seq'::regclass)".format(
            instance._meta.app_label.lower(),
            instance._meta.object_name.lower(),
        )
    )
    row = cursor.fetchone()
    cursor.close()
    return int(row[0])


class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """
    ignore_fields = list()
    capture_diff_for_fields = list()

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def diff_text(self):
        diff = self.diff
        text = []
        for k, (ini, fin) in diff.items():
            text.append("{} changed from {} to {}".format(k, ini, fin))
        return ",".join(text)

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        if not self.ignore_fields and not self.capture_diff_for_fields:
            self.capture_diff_for_fields = "__all__"
            # raise ImproperlyConfigured(
            #     f"ModelDiffMixin subclass {type(self)} must define either capture_diff_for_fields or ignore_fields)")
        if self.capture_diff_for_fields == "__all__" or self.capture_diff_for_fields == []:
            ## when explicitly set, or not set at all. The second condition implies that ignored field is set.
            self.capture_diff_for_fields = self._meta.fields
        return model_to_dict(self, fields=[field.name for field in self._meta.fields if
                                           (field in self.capture_diff_for_fields) and (
                                                   field not in self.ignore_fields)])


class DeleteFieldFileMixin(object):

    def deletefile(self, file):
        try:
            s3_client = boto3.client('s3', settings.AWS_REGION,
                                     aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                                     config=Config(signature_version="s3v4"))
            filename = '/media' + file.name
            s3_client.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                                    Key=filename)
            return file.name
        except AttributeError:
            file.delete()
            return file.name
        except Exception as e:
            return e


class ParseTimeMixin(object):

    def parse_with_tz(self, string):
        ts = parse(string)
        if timezone.is_naive(ts):
            return timezone.make_aware(ts)
        else:
            return ts


class AnnouncementManager(models.Manager):
    def get_current(self):
        current_time = now()
        return Announcement.objects.filter(start__lte=current_time, end__gt=current_time)


class Announcement(models.Model):
    CSS_CLASSES = (
        ("I", "info"),
        ("W", "warning"),
        ("D", "danger"),
        ("S", "success")
    )
    start = models.DateTimeField(help_text=_("Display from this time"))
    end = models.DateTimeField(help_text=_("Display until this time"))
    message = models.CharField(max_length=200)
    url = models.URLField(blank=True, null=True)
    objects = AnnouncementManager()
    css = models.CharField(max_length=1, choices=CSS_CLASSES, default="I")

    def __str__(self):
        return self.message



class RefreshFromDbInvalidatesCachedPropertiesMixin():

    def refresh_from_db(self, *args, **kwargs):
        self.invalidate_cached_properties()
        return super().refresh_from_db(*args, **kwargs)

    def invalidate_cached_properties(self):
        for key, value in self.__class__.__dict__.items():
            if isinstance(value, cached_property):
                self.__dict__.pop(key, None)


class SingletonValue(models.Model):
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=256)

    def __str__(self):
        return f"{self.key}: {self.value}"