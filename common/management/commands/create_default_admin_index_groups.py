import sys

from django.conf import settings
from django.core.management.base import BaseCommand
from django_admin_index.models import AppGroup, ContentTypeProxy


def flush_admin_index():
    "Flush all model instances in the Admin Index"
    for app_group in AppGroup.objects.all():
        app_group.models.clear()
        app_group.delete()


def create_admin_index(admin_default_groups):

    # Clear existing App Groups to reset
    flush_admin_index()

    for app_group in admin_default_groups:
        ag = AppGroup.objects.create(
                name = app_group["name"],
                order = app_group["order"],
                slug = app_group["slug"]
            )

        for (app_label, app_model) in app_group["models"]:
            ctype = ContentTypeProxy.objects.get(
                app_label=app_label.lower(),
                model=app_model.lower()
            )
            ag.models.add(ctype)

        sys.stdout.write('Created Admin Index App Group: {}.\n'.format(ag.name))

def yes_or_no(question):
    while "The answer is invalid":
        reply = str(input(question+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            return True
        if reply[0] == 'n':
            return False

class Command(BaseCommand):
    help = "Add django_admin_index ADMIN_INDEX_APP_GROUP_DEFAULT_CONFIG "\
            "application groups to organize the Admin site.\n\n" \
            "WARNING: existing ADMIN_INDEX records will be deleted and replaced.\n"

    def add_arguments(self, parser):

        parser.add_argument(
            '--noinput', '--no-input', action='store_false', dest='interactive',
            help='Tells Django to NOT prompt the user for input of any kind.',
        )

    def handle(self, *args, **options):

        admin_default_groups = getattr(settings, 'ADMIN_INDEX_APP_GROUP_DEFAULT_CONFIG', [])

        if not admin_default_groups:
            sys.stdout.write('\nSetting ADMIN_INDEX_APP_GROUP_DEFAULT_CONFIG to organize the Admin site is not set, ' \
                             'ADMIN_INDEX initialization abortedn.\n')
            sys.exit(1)

        # Raise warning that the existing App Groups will be overwritten
        continue_with_initialize = True
        if options.get('interactive', True):
            continue_with_initialize = yes_or_no("\nExisting ADMIN_INDEX records will be deleted and replaced. " \
                                                 "Continue? "
                                                 )
        if continue_with_initialize:
            create_admin_index(admin_default_groups)
            msg = '\nCompleted ADMIN_INDEX App Groups Initialization.\n'
        else:
            msg = '\nAborted ADMIN_INDEX initialization command.\n'
        sys.stdout.write(msg)
