import boto3
from botocore.client import Config
from django.conf import settings
from django.core.management.base import BaseCommand

from clients.models import RasterFile
from fieldcalendar.models import FieldEvent


class Command(BaseCommand):

    def handle(self, *args, **options):
        config = Config(connect_timeout=5, retries={'max_attempts': 0})
        s3 = boto3.client('s3',
                          config=config,
                          aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
                          )
        all_objects = s3.list_objects(Bucket=settings.AWS_STORAGE_BUCKET_NAME)

        for o in all_objects['Contents']:
            filename = o['Key']
            #print(filename)
            if filename.startswith('media/rasters'):
                fname = filename[6:]
                try:
                    RasterFile.objects.get(file=fname)
                except RasterFile.DoesNotExist:
                    print("deleting: ", fname)
                    s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                                     Key=filename)
                except RasterFile.MultipleObjectsReturned:
                    rf = RasterFile.objects.filter(file=fname).latest('pk')
                    rfset = RasterFile.objects.filter(file=fname).exclude(id=rf.id)
                    print("Raster cleanup: ", fname)
                    rfset.delete()
            elif filename.startswith('media/shapefiles') or filename.startswith('media/files/shapefiles'):
                fname = filename[6:]
                try:
                    FieldEvent.objects.get(file=fname)
                except FieldEvent.DoesNotExist:
                    print("deleting: ", fname)
                    s3.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                                     Key=filename)
                except FieldEvent.MultipleObjectsReturned:
                    rf = FieldEvent.objects.filter(file=fname).latest('pk')
                    rfset = FieldEvent.objects.filter(file=fname).exclude(id=rf.id)
                    print("FieldEvent cleanup: ", fname)
                    rfset.delete()
            else:
                pass

