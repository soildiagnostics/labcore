import sys

from django.apps import apps
from django.conf import settings
from django.contrib.auth.management import _get_all_permissions
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Fix permissions for proxy models."

    def add_arguments(self, parser):

        parser.add_argument('--app',
                            type=str,
                            dest='app_name',
            help='Select a specific app in which to add any missing permissions.')


    def handle(self, *args, **options):

        app_name = options.get('app_name')
        if app_name:
            if app_name not in settings.INSTALLED_APPS:
                raise ValueError('App label not in INSTALLED APPS')

            app_models = apps.get_app_config(app_name).get_models()
        else:
            app_models = apps.get_models()

        for model in app_models:
            opts = model._meta
            sys.stdout.write('{}-{}\n'.format(opts.app_label, opts.object_name.lower()))
            ctype, created = ContentType.objects.get_or_create(
                app_label=opts.app_label,
                model=opts.object_name.lower())

            for codename, name in _get_all_permissions(opts):
                sys.stdout.write('  --{}\n'.format(codename))
                p, created = Permission.objects.get_or_create(
                    codename=codename,
                    content_type=ctype,
                    defaults={'name': name})
                if created:
                    sys.stdout.write('Adding permission {}\n'.format(p))