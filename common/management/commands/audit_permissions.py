import asyncio
import sys

from django.conf import settings
from django.core.management.base import BaseCommand
from django.test.client import Client as TestClient
from django.urls import reverse

from associates.models import Organization
from clients.models import Client, Farm, Field
from contact.models import Person
from orders.models import Order


class Command(BaseCommand):
    client = TestClient()
    loop = asyncio.get_event_loop()

    class Command(BaseCommand):
        settings.ALLOWED_HOSTS.append('testserver')
        if hasattr(settings, 'DEBUG_TOOLBAR_PANELS'):
            settings.DEBUG_TOOLBAR_PANELS = []
        self.loop.run_until_complete(self._check_dealership_permissions())
        self.loop.run_until_complete(self._test_client_login())

        settings.ALLOWED_HOSTS.remove('testserver')

    async def _check_dealership_permissions(self):
        """
        Outer loop - just iterates over all the Dealers and members
        :return:
        """
        print("Dealers!")
        for dlr in Organization.objects.filter(roles__role="Dealer"):

            futures = [
                self.loop.run_in_executor(
                    None,
                    self._test_login,
                    member
                )
                for member in dlr.organization_users.all()[0:1] if member.user.is_active
            ]
            for response in await asyncio.gather(*futures):
                print(await response, end="\r")
                sys.stdout.flush()

    async def _test_login(self, member, client=False):

        sys.stdout.write("\033[K")  # clear line
        print(f"#### {member} ####", end="\r", flush=True)

        self.client.force_login(member.user)
        if not client:
            orders = Order.objects.filter(created_by__organization=member.organization)
        else:
            orders = Order.objects.filter(field__farm__client=client)
        try:
            resp = self.client.get("/dashboard/")
            assert resp.status_code == 200, f"{member} Unable to log in {resp.status_code}"
            futures = [
                self.loop.run_in_executor(
                    None,
                    self._test_view_orders,
                    order,
                    member,
                    client
                )
                for order in orders[0:1]
            ]
            for response in await asyncio.gather(*futures):
                print("Order: ", await response, end="\r")
                sys.stdout.flush()
        except Exception as e:
            print(f"test login: {member}", e)
        return member


    async def _test_view_orders(self, order, member, client=False):

        try:
            url = reverse("order_detail", args=[order.pk])
            resp = self.client.get(url)
            assert resp.status_code == 200, f"{member} Unable to view order {order}"
            if client:
                await self._test_view_farms_fields(order.field.farm.client)
            return order
        except Exception as e:
            print(f"test view orders: {member}, {order}", e)

    async def _test_view_farms_fields(self, client):
        try:
            for farm in Farm.objects.filter(client=client)[0:1]:
                resp = self.client.get(reverse("farm_detail", args=[farm.pk]))
                assert resp.status_code == 200, f"{client} Unable to view farm {farm}"
                # print(f"{client} viewed farm {farm}")

            for field in Field.objects.filter(farm__client=client)[0:1]:
                resp = self.client.get(reverse("field_detail", args=[field.pk]))
                assert resp.status_code == 200, f"{client} Unable to view field {field}"
                # print(f"{client} viewed field {field}")
            return True
        except Exception as e:
            print(f"test farms/fields:", e)


    async def _test_client_login(self):
        print("\n## Active Clients ##\n")
        clients = Client.objects.filter(contact__user__isnull=False)
        for client in clients:
            print(f"        Client: {client}", end="\n")
            sys.stdout.flush()
            try:
                person = Person.objects.get(id=client.contact.id)
                return await self._test_login(person, client=client)
            except Exception as e:
                print(f"test_client_login: {client}", e)