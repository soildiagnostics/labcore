from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from contact.models import *
from orders.models import *


def printlogin(l):
    if l.last_login:
        return l.last_login.strftime('%Y/%m/%d')
    return None


class Command(BaseCommand):
    exclude_list = list()

    def handle(self, *args, **options):
        for c in Contact.objects.all():
            emails = c.email.all()
            emails = [e.email for e in emails]
            self.exclude_list.append(c.id)
            rest = Contact.objects.exclude(id__in=self.exclude_list)
            dupes = rest.filter(email__email__in=emails, email__email_type="L")
            login = User.objects.filter(email__in=emails, is_active=True)
            if any([hasattr(d, 'client') for d in dupes]) and login:
                print(f"\nContact: {c.id}, {c}")
                print(f"Orders under this client: {[o.order_number for o in Order.objects.filter(field__farm__client__contact__id=c.id)]}")
                print(f"Username(s): {[(l.username, l.email, l.person.id, printlogin(l)) for l in login]},")
                print("Duplicates: ")
                all_orders = Order.objects.filter(field__farm__client__contact__in=dupes)
                for d in dupes:
                    d_orders = Order.objects.filter(field__farm__client__contact__id=d.id)
                    self.exclude_list.append(d.id)
                    is_client = hasattr(d, 'client')
                    if is_client:
                        print(f"\t{d.id}, {d} is marked as client")
                        print(
                            f"\tOrders under {d.id}: {[o.order_number for o in d_orders]}")
                    else:
                        print(f"\t{d.id}, {d} not marked as client")
                        print(
                            f"\tOrders under {d.id}: {[o.order_number for o in d_orders]}")
                    if d_orders.count() == all_orders.count():
                        print(f"\tContact {d}, {d.id} can see all orders for this group of duplicates")

                for u in login:
                    print(f"\tTesting login permissions for {u} (contact {u.person.id})")
                    clt = None
                    if hasattr(u, 'organizationuser'):
                        print(f"\t\t{u} is associated with AO {u.organizationuser.organization}")
                        if u.organizationuser.organization.is_dealer:
                            dlr_orders = Order.objects.filter(
                                field__farm__client__originator__organization=u.organizationuser.organization).values('order_number')
                            print(f"\t\t{u.organizationuser.organization} has dealer permissions and can see: {dlr_orders}")
                    elif hasattr(u.person, 'client'):
                        clt = u.person.client
                    elif hasattr(u.person.company, 'client'):
                        ## Either user or their company is the client.
                        clt = u.person.company.client
                    else:
                        clt = None
                    if clt is not None:
                        visible_orders = Order.objects.filter(field__farm__client=clt)
                        if all_orders.count() == visible_orders.count():
                            print(f"\t\t{u} can see all orders for these contacts - no action needed")
                        else:
                            print(f"\t\tContact {u.person.id} can see {visible_orders.count()} of {all_orders.count()} orders, which are:")
                            print(f"{[o.order_number for o in visible_orders]}")
                    else:
                        print(f"\t\tLogin {u.username} has no permissions and cannot view any order")