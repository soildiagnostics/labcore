# chat/consumers.py
import json
import time

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer

from footprint.models import UserActivity
from common.templatetags.template_async import process_fragment


def send_ws_message(username, message):
    try:
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("chat_" + username, {
            'message': message,
            'type': "chat.message",
        })
    except Exception as e:
        print(repr(e))


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['username']
        self.room_group_name = 'chat_%s' % self.room_name
        try:
            UserActivity.objects.set_login(self.scope['user'])
        except Exception as e:
            print("Exception", e)

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        self.send(text_data=json.dumps({
            'message': f'Connected to {self.room_group_name}'
        }))

    def disconnect(self, close_code):
        UserActivity.objects.set_logout(self.scope['user'])

        # Leave room group
        self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        try:
            print("Recd type: " + text_data_json['type'])
            response = process_fragment(text_data_json)
            self.send(json.dumps(response))
        except KeyError:
            message = text_data_json['message']
            # Send message to room group
            self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': "Received message: " + message
                }
            )

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))