from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

from contact.models import User as UserModel


# Class to permit the authentication using email or username
class EmailOrUsernameLoginBackend(ModelBackend):  # requires to define two functions authenticate and get_user

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = UserModel.objects.filter(is_active=True).get(Q(username__iexact=username) |
                                                                Q(email__iexact=username))
        except (UserModel.DoesNotExist, UserModel.MultipleObjectsReturned):
            UserModel().set_password(password)
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user

    def get_user(self, user_id):
        try:
            user = UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

        return user if self.user_can_authenticate(user) else None
