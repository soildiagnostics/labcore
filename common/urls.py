from django.conf import settings
from django.contrib import admin, auth
from django.urls import path, include
from django.views.generic.base import RedirectView
from organizations.backends import invitation_backend
from rest_framework.authtoken import views as drf_views

from common import views as common_views

admin.site.site_header = settings.LABCORE_ADMIN_TITLE

urlpatterns = [
    #path('login/', auth_views.login, name="login"),
    #path('logout/', auth_views.logout, name="logout"),
    path('accounts/profile/', RedirectView.as_view(url='/dashboard/', permanent=False), name="profile"),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('dashboard/', common_views.DashboardView.as_view(), name="dashboard"),
    #path('auth/', include('django.contrib.auth.urls')),
    #path('grappelli/', include('grappelli.urls')),  # grappelli URLS
    path('admin/', admin.site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('articles/', include('zinnia.urls')),
    path('comments/', include('django_comments.urls')),
    path('api/', include('rest_framework.urls')),
    #path('accounts/', include('organizations.urls')),
    path('invitations/', include(invitation_backend().get_urls())),
    path('contacts/', include('contact.urls')),
    path("clients/", include("clients.urls")),
    path("fieldevents/", include("fieldcalendar.urls")),
    path("orders/", include("orders.urls")),
    path("products/", include("products.urls")),
    path("associates/", include("associates.urls")),
    path("samples/", include("samples.urls")),
    path("meetings/", include("meetings.urls")),
    path("soils/", include("soils.urls")),
    # path("fertisaverN/", include("fertisaverN.urls")),
    path("svx/", include("svx.urls")),
    path("awis/", include("awis.urls")),
    path("bugs/", include("bugs.urls")),
    path("permissions/<int:pk>/", common_views.PermissionsView.as_view(), name="permissions"),
    path("permissions/<str:username>/", common_views.CheckPermissions.as_view()),
    path("inventory/", include("inventory.urls")),
    path("recommendations/", include("recommendations.urls")),
]

urlpatterns += [
    path('auth/login/', auth.views.LoginView.as_view(),
         {'template_name': 'registration/login.html'}, name='login'),
    path('auth/logout/', auth.views.LogoutView.as_view(),
         {'template_name': 'registration/login.html'}, name='logout'),
    path('auth/password_change/', auth.views.PasswordChangeView.as_view(),
         {'template_name': 'registration/login.html'},name='password_change'),
    path('auth/password_change/done/', auth.views.PasswordChangeDoneView.as_view(),
         {'template_name': 'registration/login.html'},name='password_change_done'),

    path('auth/password_reset/', auth.views.PasswordResetView.as_view(),
         {'template_name': 'registration/login.html'},name='password_reset'),
    path('auth/password_reset/done/', auth.views.PasswordResetDoneView.as_view(),
         {'template_name': 'registration/login.html'},name='password_reset_done'),
    path('auth/reset/<uidb64>/<token>/', auth.views.PasswordResetConfirmView.as_view(),
         {'template_name': 'registration/login.html'},name='password_reset_confirm'),
    path('auth/reset/done/', auth.views.PasswordResetCompleteView.as_view(),
         {'template_name': 'registration/login.html'},name='password_reset_complete'),
    path(r'api-token-auth/', drf_views.obtain_auth_token)
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static
    from django.views.defaults import server_error, page_not_found, permission_denied

    urlpatterns += [
        path('500/', server_error),
        path('403/', permission_denied, kwargs={'exception': Exception("Permission Denied")}),
        path('404/', page_not_found, kwargs={'exception': Exception("Page not Found")}),
    ]

    urlpatterns = [
                      path('__debug__/', include(debug_toolbar.urls))
                  ] + urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



