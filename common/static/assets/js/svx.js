$(document).ready(function () {
    var oTable = $('.datatable').dataTable({
        //order: [[0,"asc"], [1,"asc"]],
        paging: true,
        dom: 'lrtip',
        columnDefs: [
            {
                name: "category",
                orderable: false,
                searchable: true,
                targets: [0]
            },
            {
                name: "product",
                orderable: false,
                searchable: true,
                targets: [1]
            },
            {
                name: "vendor",
                orderable: false,
                searchable: true,
                targets: [2]
            },
            {
                name: "prices",
                orderable: true,
                searchable: true,
                targets: [3]
            },
            {
                name: "actions",
                orderable: false,
                searchable: false,
                targets: [4]
            }
        ],
        "processing": true,
        "serverSide": false,
    });

    $("#searchsvx").keypress(function (evt) {
        if (evt.which == 13) {
            var search = "&search=" + $("#searchsvx").val();
            $.each(network, function (i, url) {
                $.getJSON(url + search)
                    .then(function (data) {
                        make_rows(data);
                    })
                    .fail(function () {
                        console.log("Failed: " + url);
                    });
            })
        }
    });
});

var make_rows = function (data) {
    var table = $(".datatable").DataTable();
    table.clear();
    $(data).each(function (idx, val) {
        table.row.add([
            val.category.name,
            val.name,
            val.created_by,
            val.prices[0].string,
            "<a href='#' class='svxproduct' id='" + val.vendor_product_id + "'>Copy</a>"
        ]).draw();
        $("a#" + val.vendor_product_id).data(val);
        $("a#" + val.vendor_product_id).click(function (e) {
            e.preventDefault();
            var pdata = $(e.target).data();
            console.log(pdata);
            $.ajax("{% url 'api_create_product' %}", {
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(pdata),
                success: function (data) {
                    console.log(data);
                }
            });
        });
    });
}