"use strict";

function get_bounds(proplayer) {
    try {
        var fb = L.geoJson(JSON.parse($("#id_annotations").val()));
        bounds = fb.getBounds();
    } catch (e) {
        console.log(e);
        bounds = proplayer.getBounds();
    }
    return bounds
}

function add_legend(data, map, div) {
    var render_color;
    var grades = [];

    if (data.legend) {
        color_array = [];
        var abundance = [];
        $.each(data.legend, function (idx, val) {
            color_array.push(val[0]);
            grades.push(val[2]);
            abundance.push(val[1]);
        });

        if ("min" in data) {
            if ($.trim(div.html()) === "") {
                let ret = make_premade_legend("{0}<br>{1} to {2}".formatUnicorn(data.parameter,
                    rounding(data.min), rounding(data.max)),
                    grades, abundance, color_array);
                div.append(ret);
            }
        }

        render_color = function (val) {
            var mycol = color_array[0];
            var i = 0;
            while (val > grades[i]) {
                mycol = color_array[i];
                i += 1;
            }
            return color_array[i];
        };
    } else {
        var range = data.max - data.min;
        var color_array = default_color_array;
        var unit = range / color_array.length;
        grades = [];
        $.each(color_array, function (idx, val) {
            grades.push(rounding(parseFloat(data.min) + idx * unit));
        });
        try {
            if ($.trim(div.html()) === "") {
                if (data.min) {
                    div.append(make_legend("{0}<br>{1} to {2}".formatUnicorn(data.parameter,
                        rounding(data.min), rounding(data.max)),
                        grades, color_array))
                }
            }
        } catch (err) {
            console.log(err);
        }
        render_color = function (val) {
            var interp = (val - data.min) / unit;
            return color_array[Math.min(color_array.length - 1, Math.round(interp))];
        };
    }
    return render_color
}

function make_legend(title, grades, color_array) {
    var div = document.createElement("div");

    div.innerHTML = '<h4>{0}</h4>'.formatUnicorn(title);
    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<span><div class="legendtile" style="background:' + color_array[i] + ' !important">&nbsp;&nbsp;&nbsp;</div><div class="legendtext">' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '</div><br/>' : '+</div></span>');
    }
    return div;
}

function make_premade_legend(title, grades, abundance, color_array) {
    var div = document.createElement('div');
    div.innerHTML = '<h4>{0}</h4>'.formatUnicorn(title);
    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        var key = abundance[i] + ": ";
        if (i === 0) {
            key += "<" + grades[i];
        } else if (i === grades.length - 1) {
            key += grades[i - 1] + "+";
        } else {
            key += grades[i - 1] + "&ndash;" + grades[i];
        }
        div.innerHTML +=
            '<div><div class="legendtile" style="background:' + color_array[i] + ' !important">&nbsp;&nbsp;&nbsp;</div><div class="legendtext">' + key + "<br></div>";
    }
    return div;
}

var opts = {
    angle: 0.15, // The span of the gauge arc
    lineWidth: 0.44, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
        length: 0.6, // // Relative to gauge radius
        strokeWidth: 0.035, // The thickness
        color: '#000000' // Fill color
    },
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    colorStart: '#6FADCF',   // Colors
    colorStop: '#8FC0DA',    // just experiment with them
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support

};

