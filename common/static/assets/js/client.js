$(document).ready(function () {
    var oTable = $('#clienttable').DataTable({
        order: [[0, "asc"]],
        paging: true,
        stateSave: true,
        pagingType: "numbers",
        columnDefs: [
            {
                name: "contact",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "originator",
                orderable: true,
                searchable: true,
                targets: [1]
            },
            {
                name: "orders",
                orderable: false,
                searchable: false,
                targets: [2]
            },
            {
                name: "products",
                orderable: false,
                searchable: false,
                targets: [3]
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": client_action_url
    });
    // ...
});