var calendar;

$(document).ready(function () {
    try {

        $("#datepicker").daterangepicker({
            singleDatePicker: true,
        });

        $("#button-id-address").click(address);

        $(".share_button").click(function (event) {
            event.preventDefault();
            $("#modal-error-message").hide();
            console.log($(event.target));
            var url = $(event.target).prop('href');
            var field = $(event.target).attr('field');
            var name = $(event.target).attr('field_name');
            var expiry;
            $("#sharemodal-title").text("Share {0} with".formatUnicorn(name));
            $('#duration').daterangepicker({
                ranges: {
                    'In 7 Days (default)': [moment(), moment().add(7, 'days')],
                    'In 30 Days': [moment(), moment().add(30, 'days')],
                    'In 3 Months': [moment(), moment().add(3, 'months')],
                    'In 1 Year': [moment(), moment().add(1, 'years')],
                },
                "showCustomRangeLabel": true,
                "startDate": moment(),
                "endDate": moment().add(7, 'days'),
            }, function (start, end, label) {
                expiry = end.toISOString();
            });
            $("#schdmodal-save").click(function () {
                $.post({
                    url: url,
                    data: {
                        'field': field,
                        'username': $('input#username').val(),
                        'expiry': expiry
                    },
                    success: function (ret) {
                        $("#schdmodal-save").off("click");
                        $("#modal-error-message").hide();
                        $("#shareModal").modal('hide');
                    },
                    error: function (response, status, error) {
                        $("#modal-error-message").text("Status: {0}, Details: {1}".formatUnicorn(response.status,
                            response.responseJSON.detail));
                        $("#modal-error-message").show();
                    }
                });

            });

            $("#schdmodal-close").click(function () {
                $("#schdmodal-save").off("click");
            });
            $("#shareModal").modal('show');
        });
    } catch (e) {

    }


    // $(".event_select").click(add_to_today);

    /* initialize the external events
    -----------------------------------------------------------------*/

    $('#external-events .fc-event').each(function () {
        new FullCalendarInteraction.Draggable(this, {
            eventData: {
                title: $.trim($(this).attr("name")), // use the element's text as the event title
                extendedProps: (() => {
                    var obj = {
                        event_type: $.trim($(this).attr("id")),
                        icon: $.trim($($($(this).children("img"))).attr("icon")),
                        custom_fields: get_custom_fields(this)
                    };
                    if (typeof (fieldpk) === "undefined") {
                        obj.item_id = itempk;
                    } else {
                        obj.field_id = fieldpk;
                    }
                    return obj
                })(),
                color: $.trim($(this).attr("rendercolor")),
            }
        });
    });

    /* initialize the calendar
    -----------------------------------------------------------------*/
    calendar = new FullCalendar.Calendar(document.getElementById('calendar'), {
        plugins: ['dayGrid', 'timeGrid', 'list', 'interaction'],
        defaultView: 'dayGridMonth',
        editable: true,
        droppable: true,
        header: {
            left: 'prevYear, prev,next, nextYear, today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listYear'
        },
        events: {
            url: fieldcalendarurl,
            type: 'GET',
            error: function () {
                alert('there was an error while fetching events!');
            },
            success: function (reply) {
                //console.log(reply);
            },
        },
        eventReceive: function (event) {
            var calEvent = event.event;
            if (calEvent.extendedProps.custom_fields.length !== 0) {
                modal_div(calEvent).modal('show');
            } else {
                modal_post(calEvent);
            }
        },

        eventDrop: function (event) {
            //var mdiv = modal_div(calEvent);
            modal_post(event.event);
        },

        eventClick: function (event) {
            modal_div(event.event).modal('show');
        },

        eventResize: function (event) {
            modal_post(event.event);
        },

        eventRender: function (info) {
            var title = $(info.el).find(".fc-title").html();
            if (title) {
                $(info.el).find(".fc-title").html(render_title(title, info.event));
            } else {
                title = $(info.el).find(".fc-list-item-title").find("a").html();
                if (title) {
                    $(info.el).find(".fc-list-item-title").find("a").html(render_title(title, info.event));
                }
            }
            var tooltip = new Tooltip(info.el, {
                title: () => {
                    var name = info.event.extendedProps.field_name ? info.event.extendedProps.field_name : info.event.extendedProps.item_name;
                    var tip = "<strong>{0}</strong>".formatUnicorn(name);
                    tip += "<table>";
                    $(info.event.extendedProps.custom_fields).each(function (i, p) {
                        tip += render_tooltip(p.name, p.value);
                    });
                    tip += "</table>";
                    return tip
                },
                placement: 'top',
                trigger: 'hover',
                container: 'body',
                html: true
            });
            if (info.event.extendedProps.geotags || info.event.extendedProps.geolocation) {
                geotagged_events.push(info.event);
                console.log(info.event);
            }
        }
    });

    try {
        calendar.render();
    } catch (e) {

    }
});

rerender_leaflet_markers = function () {
    if (typeof (geotagged_events) !== 'undefined') {
        var markers = [];
        $(geotagged_events).each(function (idx, val) {
            var loc = val.extendedProps.geotags ? val.extendedProps.geotags : val.extendedProps.geolocation;
            var mkr = L.marker([loc.latitude, loc.longitude],
                {icon: photo_icon}).bindPopup(() => {
                    var tip = "<table>";
                    $(val.extendedProps.custom_fields).each(function (i, p) {
                        tip += render_tooltip(p.name, p.value);
                    });
                    tip += "</table>";
                    if(val.extendedProps.file) {
                        tip += val.extendedProps.file;
                    }
                    return tip
                });
            markers.push(mkr);
        });
        if (geotag_markers) {
            mapsList[0].removeLayer(geotag_markers);
        }
        try {
            geotag_markers = L.layerGroup(markers);
            geotag_markers.addTo(mapsList[0]);
        } catch (e) {
        }
    }
}

render_title = function (string, event) {
    string = string.replace("T:", '<i class="wi wi-thermometer">&nbsp;</i>');
    string = string.replace(", Precip:", '<br><i class="wi wi-rain-mix">&nbsp;</i>');
    if (event.extendedProps.icon) {
        string = `<img class="calimage" src="${event.extendedProps.icon}" alt="event_icon" style="background-color: ${event.backgroundColor}"> ${string}`;
    }
    if (event.startEditable === false) {
        string = '<i class="fa fa-lock">&nbsp;</i> ' + string;
    }
    if (event.extendedProps.file) {
        string = '<i class="fa fa-download">&nbsp;</i> ' + string;
    }
    if (event.extendedProps.file_type === "image") {
        string = '<i class="fa fa-camera">&nbsp;</i> ' + string;
    }
    if (event.extendedProps.geotags || event.extendedProps.geolocation) {
        string = '<i class="fa fa-location-arrow">&nbsp;</i> ' + string;
    }
    return string
};

render_name = function (name) {
    if (name === "Temp (F)") {
        name = '<i class="wi wi-thermometer">&nbsp;</i>';
    }
    if (name === "Precip (in)") {
        name = '<i class="wi wi-rain-mix">&nbsp;</i>';
    }
    if (name === "RH (%)") {
        name = '<i class="wi wi-humidity">&nbsp;</i>';
    }
    if (name === "Solar (Wh/m^2)") {
        name = '<i class="wi wi-hot">&nbsp;</i> ';
    }
    if (name === "Wind (mph)") {
        name = '<i class="wi wi-strong-wind">&nbsp;</i> ';
    }
    if (name === "Dew Point (F)") {
        name = 'Dew <i class="wi wi-raindrops">&nbsp;</i> ';
    }
    if (name === "Soil Temp (F)") {
        name = 'Soil <i class="wi wi-thermometer">&nbsp;</i> ';
    }
    if (name === "Soil Moisture (%)") {
        name = 'Soil <i class="wi wi-humidity">&nbsp;</i> ';
    }
    return name
};

render_tooltip = function (name, value) {
    var rendered_name = render_name(name);

    if (name === "Precip (in)") {
        if (value.indexOf("in") == -1) {
            value += " in";
        }
    }
    if (name === "Solar (Wh/m^2)") {
        value += " Wh/m<sup>2</sup>"
    }
    if (name === "Wind (mph)") {
        value = value.replace(",", " mph,");
    }
    if (name === "Dew Point (F)") {
        value += "F";
    }
    if (name === "Soil Temp (F)") {
        value = value.replace(/,/g, "<br>");
    }
    if (name === "Soil Moisture (%)") {
        value = value.replace(/,/g, "<br>");
    }
    return "<tr><td valign='top'>{0}</td><td valign='top' style='padding-left: 4px;'>{1}</td></tr>".formatUnicorn(rendered_name, value);
};

get_custom_fields = function (obj) {
    var cf = [];
    var kids = $(obj).find("p");
    kids.each(function (p) {
        cf.push({
            'id': $(kids[p]).attr('id'),
            'name': $(kids[p]).attr('name'),
            'help_text': $(kids[p]).attr('help_text'),
            'value': $(kids[p]).text()
        });
    });
    return cf;
}

modal_div = function (event) {
    var title = render_title(event.title, event);
    var name = event.extendedProps.field_name ? event.extendedProps.field_name : event.extendedProps.item_name
    var icon = event.icon;
    $("div#image_file").empty();
    $("h4.modal-title").html(title);
    if (event.id) {
        $("h4.modal-title").attr('id', event.id);
        $("span#event-id").text("Field: {0}, Event ID {1}".formatUnicorn(name, event.id));
    } else {
        $("span#event-id").text("Post new event");
    }
    console.log(event);
    var edit = event.startEditable === false ? "readonly" : "";
    if (edit === "readonly") {
        $("#modal-save").attr('disabled', true);
        $("#modal-delete").attr('disabled', true);
    } else {
        $("#modal-save").attr('disabled', false);
        $("#modal-delete").attr('disabled', false);
    }

    $("div.modal-form-group").text("");
    $(event.extendedProps.custom_fields).each(function (index, value) {
        var name = value.name;
        var help_text = value.help_text;
        var cfield_id = value.id;
        var cfield_value = value.value;
        var rendered_name = render_name(name);
        var icon = "";
        if (name !== rendered_name) {
            icon = rendered_name + "&nbsp;&nbsp;";
        }
        var label = `<label for="${name}">${icon}${name}: ${help_text ? help_text : ""}</label>`;
        var input = `<input type='text' class='form-control event_field' name="${name}" id="${cfield_id}" value="${cfield_value}" ${edit}>`;
        $("div.modal-form-group").append([label, input]);
    });
    if (event.extendedProps.file) {
        var dld_div = $("div.file_download");
        dld_div.html("Attached File: <a href='{0}'><i class='fa fa-download'>&nbsp;</i>Download</a></div>".formatUnicorn(event.extendedProps.file));
        if (event.extendedProps.file_type) {
            if (event.extendedProps.file_type === "image") {
                var img = $('<img class="event_image">');
                img.attr('src', event.extendedProps.file);
                $("div#image_file").html(img);
            }
        }
    }
    $("#modal-save").click(function () {
        var cfields = [];
        $("div.modal-body").find("input.event_field").each(function (index, value) {
            try {
                var cfield_id = $(value).attr('id');
                var event_field = seek_field(event, cfield_id);
                event_field.value = $(value).val();
                cfields.push(event_field);
                event.setExtendedProp('custom_fields', cfields);
            } catch (e) {
                // This is when we have a geonote
                event.extendedProps.memo = $(value).val();
            }
        });
        modal_post(event);
    });
    $("#modal-delete").click(function () {
        event_delete(event);
        $("#modal-delete").off("click");
    });
    $("#modal-close").click(function () {
        $("#modal-save").off("click");
        $("#modal-delete").off("click");
    });
    $(this).on('shown.bs.modal', function () {
        $("#modal-save").focus();
    })
    return $("#myModal");
}


event_delete = function (event) {
    var url;
    try {
        if (fieldpk === 0) {
            // this occurs on the client calendar page. We grab the value from the event itself.
            fieldpk = event.extendedProps.field_id;
        }
        url = event.id === "" ? fieldcalendarurl : "/fieldevents/api/v1/field/{0}/events/update_event/{1}/".formatUnicorn(fieldpk, event.id);
    } catch (e) {
        url = event.id === "" ? fieldcalendarurl : "/inventory/api/v1/item/{0}/events/update_event/{1}/".formatUnicorn(itempk, event.id);
    }
    $.ajax({
        url: url,
        type: 'DELETE',
        success: function (data) {
            event.remove();
            //$("#myModal").modal('hide');
        }
    });


};
seek_field = function (event, id) {
    var ret_val = {};

    $(event.extendedProps.custom_fields).each(function (index, value) {
        if (parseInt(value.id) === parseInt(id)) {
            // Ok to allow string and int comparisons.
            ret_val = value;
        }
    });
    return ret_val;
};

function modal_post(event) {
    var datafields = ["id", "allDay", "start", "end", "title", 'extendedProps',
        "editable", "backgroundColor"];

    var data = {};
    $(datafields).each(function (idx, value) {
        data[value] = event[value];
    });

    let type = event.id === "" ? 'POST' : 'PUT';
    var url;
    try {
        if (fieldpk === 0) {
            // this occurs on the client calendar page. We grab the value from the event itself.
            fieldpk = event.extendedProps.field_id;
        }
        url = event.id === "" ? fieldcalendarurl : "/fieldevents/api/v1/field/{0}/events/update_event/{1}/".formatUnicorn(fieldpk, event.id);
    } catch (e) {
        url = event.id === "" ? fieldcalendarurl : "/inventory/api/v1/item/{0}/events/update_event/{1}/".formatUnicorn(itempk, event.id);
    }

    $.ajax({
        url: url,
        type: type,
        contentType: "application/json",
        data: JSON.stringify(data),
        dataType: 'json',
        success: function (ret, textStatus, jqXHR) {

            $("#myModal").modal('hide');
            $("#modal-save").off("click");
            $("#modal-delete").off("click");
            try {
                event.remove();
            } catch (e) {
            }
            calendar.refetchEvents();
        },
        error: function (err) {
            $("#myModal").modal('hide');
            $("#modal-save").off("click");
            $("#modal-delete").off("click");
            alert("Sorry - couldn't do that");
        }

    });
}

function eventSearch() {
    // Declare variables
    var input, filter, ul, i, j, cat, catul, catli, item;
    input = $('#eventInput');
    filter = input.val().toUpperCase();
    ul = $("#fieldevents_list").children("li");

    for (i = 0; i < ul.length; i++) {
        cat = $(ul[i]).children("strong")[0].innerText;
        catul = $(ul[i]).children("ul");
        catli = $(catul).children("li");

        if (cat.toUpperCase().indexOf(filter) > -1) {
            // if category name is filtered, display whole category
            ul[i].style.display = "";
            for (j = 0; j < catli.length; j++) {
                catli[j].style.display = "";
            }

        } else {
            // if cat name is not filtered, check if subitems are filtered
            ul[i].style.display = "none";

            for (j = 0; j < catli.length; j++) {
                item = catli[j].innerText;
                if (item.toUpperCase().indexOf(filter) > -1) {
                    // if subitems are filtered, set parent display
                    catli[j].style.display = "";
                    ul[i].style.display = "";
                } else {
                    // otherwise unset parent display.
                    catli[j].style.display = "none";
                }
            }
        }
    }
}

function make_geonote_div() {
    var target = $("a#geonote");
    var lat = target.attr('lat');
    var lon = target.attr('lon');
    var now = moment().format();
    var event = {
        "id": "",
        "start": now,
        "end": now,
        "title": "Scouting Note",
        "editable": true,
        "className": [],
        "extendedProps": {
            "field_id": fieldpk,
            "custom_fields": [],
            "geolocation": {
                "latitude": lat,
                "longitude": lon
            },
            "memo": 'This is a custom memo about this location'
        }
    }
    var name = "Memo";
    var help_text = "Add a note on this location"
    var modal = modal_div(event);
    var rendered_name = render_name(name);
    var icon = "";
    if (name !== rendered_name) {
        icon = rendered_name + "&nbsp;&nbsp;";
    }
    var label = `<label for="${name}">${icon}${name}: ${help_text ? help_text : ""}</label>`;
    var input = `<input type='text' class='form-control event_field' value=''>`;
    modal.find("div.modal-form-group").append([label, input]);
    modal.modal('show');
}