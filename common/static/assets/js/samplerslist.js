document.addEventListener("DOMContentLoaded", function () {
    var oTable = $('#samplertable').dataTable({
        order: [[5, "desc"]],
        paging: true,
        stateSave: true,
        pagingType: "numbers",
        responsive: true,
        drawCallback: printlinks,
        columnDefs: [
            {
                name: "order_number",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "order_status",
                orderable: true,
                searchable: true,
                targets: [1]
            },
            {
                name: "client",
                orderable: true,
                searchable: true,
                targets: [2]
            },
            {
                name: "farm",
                orderable: true,
                searchable: true,
                targets: [3]
            },
            {
                name: "field",
                orderable: true,
                searchable: true,
                targets: [4]
            }, {
                name: "date_created",
                orderable: true,
                searchable: true,
                targets: [5],
                render: $.fn.dataTable.render.moment(moment.ISO_8601, 'MM/DD/YYYY')
            },
            {
                name: "date_modified",
                orderable: true,
                searchable: true,
                targets: [6],
                render: $.fn.dataTable.render.moment(moment.ISO_8601, 'MM/DD/YYYY')
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": samplerurl
    });
    // ...
});