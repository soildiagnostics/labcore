"use strict";
String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
    function () {
        "use strict";
        var str = this.toString();
        if (arguments.length) {
            var t = typeof arguments[0];
            var key;
            var args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };

var mapsList = [];
var proplayer = false;
var propdata = {};
var markers = null;
var raster = null;
var control = L.control.layers();
var calculated_risk_data = null;
var legend = L.control({position: 'bottomright'});
var default_color_array = ['#ffffe5', '#f7fcb9', '#d9f0a3', '#addd8e', '#78c679', '#41ab5d', '#238443', '#006837', '#004529'];


function makeFeature(feature, layer, adjlink) {
    if (feature.properties && feature.properties.popupContent) {
        let edit_link = `<br><button class="btn btn-warning btn-xs" href=${adjlink}>Edit</button>`
        layer.bindPopup(feature.properties.popupContent + (adjlink ? edit_link : ""));
        layer.bindTooltip(String(feature.properties.tooltipContent), {
            permanent: true,
            direction: 'center',
            className: "maptooltip",
            opacity: 0.7
        });
        if (adjlink) {
            layer.on("popupopen", (pevent) => {
                let content = pevent.target.feature.properties.popupContent;
                let groups = content.match(/^(.+):\s(.+)<br>#(\d+)$/i);
                let node = $(pevent.target._popup._contentNode);
                let edit_link = node.find("button");
                edit_link.on("click", (event) => {
                    event.preventDefault();
                    if (edit_link.siblings("input").length === 0) {
                        edit_link.before(`<input type='text' value="${groups[2]}" class="form-control">`);
                        edit_link.html("Save");
                        edit_link.on("click", (event) => {
                            $.ajax({
                                url: edit_link.attr("href"),
                                type: 'PUT',
                                data: {
                                    parameter: groups[1],
                                    sequence: groups[3],
                                    value: $(edit_link.siblings("input")[0]).val()
                                },
                                success: (data) => {
                                    edit_link.html("Success");
                                    setTimeout(() => {
                                        edit_link.html("Save");
                                    }, 500);
                                }
                            });
                        });
                    }
                });
            });
        }
    }
}

function adjust_layer(event) {
    // let groups = feature.properties.popupContent.match(/^([\w|\d|\s]+):\s(.+)<br>#(\d+)$/i);
    // var adjustModel =
    console.log(event.target)
}

function rounding(label) {
    try {
        var fdigits = 0;
        if (label !== 0) {
            fdigits = Math.max(0, Math.round(Math.log10(100 / label)));
        }
        return parseFloat(label).toFixed(fdigits);
    } catch (e) {
        return label
    }
}

function generate_points_proplayer(render_color, data, adj_layer) {
    try {
        return L.geoJson(data, {
            onEachFeature: (feature, layer) => {
                makeFeature(feature, layer, adj_layer);
            },
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, {
                    radius: 8,
                    fillColor: render_color(feature.properties[$(data).attr("parameter")]),
                    color: render_color(feature.properties[$(data).attr("parameter")]),
                    weight: 1,
                    opacity: 0.7,
                    fillOpacity: 0.5
                })
            }
        });
    } catch (e) {
        return null
    }
}

function generate_polygon_proplayer(render_color, data, adj_layer) {
    try {
        return L.geoJson(data, {
            onEachFeature: (feature, layer) => {
                makeFeature(feature, layer, adj_layer);
            },
            style: function (feature) {
                return {
                    color: render_color(feature.properties[$(data).attr("parameter")]),
                    fillColor: render_color(feature.properties[$(data).attr("parameter")]),
                    fillOpacity: 1
                };
            }
        });
    } catch (e) {
        console.log(e);
    }
}

function getMarkerClusterGroup(render_color, data) {
    return L.markerClusterGroup({
        maxClusterRadius: 40,
        iconCreateFunction: function (cluster) {
            var children = cluster.getAllChildMarkers();
            var value = 0;
            children.forEach(function (kid) {
                value += kid.feature.properties[$(data).attr("parameter")];
            });
            var label = value / cluster.getChildCount();
            var color = render_color(label);
            label = rounding(label);
            return L.divIcon({
                html: '<div style="background-color:' + color +
                    ';color:black;padding:10px;"><span><b>' +
                    label + '</b></span></div>',
                iconSize: L.point(40, 30)
            });
        }
    });
}

function generate_map(url, imageurl, map, div) {
    var bounds;
    var render_color;
    map.spin(true);
    $.getJSON(url, function (data) {
        let url_parts = url.split("/");
        let adj_link = data.editable ? `/clients/api/v1/layer/${url_parts[5]}/adjust/` : false;
        propdata = data;
        render_color = add_legend(data, map, div);
        $("#button-id-updatebbox").prop("disabled", false);
        let items = [proplayer, markers, raster];
        items.push(...control._layers);
        $.each(items, function (index, item) {
            // cleanup
            try {
                map.removeLayer(item);
            } catch (err) {
            }
        });
        /* Different data types:
            USGS: data.usgs === true - no GeoJSON features to plot. Just show the raster
            SSURGO: data.soils === true - SSURGO data, contains polygons
            Yield data: data.usgs === false, data.soils === false,
                        subfield_type === points_only, few_points === false
            Soil test data: data.usgs === false, data.soils === false,
                            subfield_type === points_only, few_points === true
            Drone polygon data:  data.usgs === false, data.soils === false,
                                 subfield_type === polygons_to_points, few_points === false

            Plotting tasks:
                A) Show Layer data for:
                    1) few_points === true: show Point cloud and Marker clusters.
                    2) few_points === false: Show only Points
                    3) subfield_type === polygons_only: show only polygons with centered values
                B) Show Raster data for all.
                c) Create Legend.

         */

        if (data.soils === false && data.subfield_type !== "polygons_only" && data.subfield_type !== "geometry_collection") {
            // USGS and non-polygon data, but USGS has no features.
            proplayer = generate_points_proplayer(render_color, data, adj_link);
            if (data.few_points === false) {
                let vectorLayer = getMarkerClusterGroup(render_color, data);
                vectorLayer.addLayer(proplayer);
                proplayer = vectorLayer;
            }
        } else {
            proplayer = generate_polygon_proplayer(render_color, data, adj_link);
        }
        //.addTo(mapsList[0]);

        if (!data.usgs) {
            map.addLayer(proplayer);
            control.addOverlay(proplayer, propdata.parameter + " values");
        }

        // The soil test data has few points, and so it needs the field boundaries.
        // The other layers need the proplayer boundaries, so the default could be proplayer boundaries

        bounds = get_bounds(proplayer);

        if (data.numeric || data.usgs) {
            raster = L.imageOverlay(imageurl, bounds).addTo(map);;
            raster.on("load", function (e) {
                control.addOverlay(raster, propdata.parameter + " image");
            });
            raster.on("error", function (e) {
                alert("Processing raster. Check back in a while.");
            })

        }
        map.fitBounds(bounds);
        map.spin(false);
    }).fail(function (j,t,e) {
        console.log(e);
        alert(t);
        map.spin(false);
    });
}

function get_bounds(proplayer) {
    var bounds;
    try {
        var fb = L.geoJson(JSON.parse($("#id_boundary").val()));
        bounds = fb.getBounds();
    } catch (e) {
        console.log(e);
        bounds = proplayer.getBounds();
    }
    return bounds
}
