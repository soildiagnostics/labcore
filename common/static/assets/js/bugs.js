$.fn.dataTable.render.ellipsis = function (cutoff) {
    return function (data, type, row) {
        return type === 'display' && data.length > cutoff ?
            data.substr(0, cutoff) + '…' :
            data;
    }
};
$(document).ready(function () {
    var oTable = $('.datatable').DataTable({
        order: [[3, "asc"], [2, "asc"]],
        paging: true,
        stateSave: true,
        pagingType: "numbers",
        columnDefs: [
            {
                name: "pk",
                orderable: true,
                searchable: true,
                targets: [0],
            },
            {
                name: "category",
                orderable: true,
                searchable: true,
                targets: [1],
            },
            {
                name: "bug_name",
                orderable: true,
                searchable: true,
                targets: [2],
            },
            {
                name: "modified",
                orderable: true,
                searchable: false,
                targets: [3],
                render: $.fn.dataTable.render.moment(moment.ISO_8601, 'YYYY/MM/DD')
            },
            {
                name: "state",
                orderable: true,
                searchable: true,
                targets: [4]
            },
            {
                name: "bug_description",
                orderable: false,
                searchable: true,
                targets: [5],
                render: $.fn.dataTable.render.ellipsis(100)
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": actionurl
    });
    // ...
});