L.Map.addInitHook(function () {
    mapsList.push(this); // Use whatever global scope variable you like.
    MyGeometryField = L.GeometryField.extend({
        addTo: function (map) {
            L.GeometryField.prototype.addTo.call(this, map);
            // Customize map for field
        },
        onEachFeature: onEachFeature
    });

    var serial = 1;

    function onEachFeature(feature, layer) {
        layer.bindPopup("#{0}".formatUnicorn(serial));
        serial++;
    }

    this.on('click', function () {
        var hyb = L.gridLayer.googleMutant({
            type: 'hybrid' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        }).addTo(this);
        this.panTo(this.getCenter());
        this._onResize();
        this.invalidateSize();
    });
    var a = this;
    setTimeout(function () {
        a.fireEvent("click");
    }, 2000);
    control.addTo(mapsList[0]);
    if (extent.length === 1) {
        extent = extent[0];
    }
    var ext = [
        [extent[0], extent[1]],
        [extent[2], extent[3]]
    ];

    var zones = L.imageOverlay(raster, ext).addTo(mapsList[0]);
    control.addOverlay(zones, "Reference");
    var bdy = L.geoJSON(JSON.parse($("#id_boundary").val()));
    bdy.setStyle({
        fillOpacity: 0.05,
        color: '#eab700'
    })
    bdy.addTo(mapsList[0]);
    control.addOverlay(bdy, "Boundary");
    mapsList[0].fitBounds(ext);
    addNonGroupLayers(precise_points);
});
(function ($) {
    $(document).ready(function () {
        $('.add-another').click(function (e) {
            e.preventDefault();
            showAddAnotherPopup(this);
        });
        $('.related-lookup').click(function (e) {
            e.preventDefault();
            showRelatedObjectLookupPopup(this);
        });
    });
})(jQuery);

$("#button-id-squaregrid").click(function (e) {
    e.preventDefault();
    var area = $("input#gridarea").val();
    squaregrid(area);
    // $("#modal-save").click(function (e) {
    //     // e.preventDefault();
    //     var area = $("input#area").val();
    //     $("#myModal").modal('hide');
    //     squaregrid(area);
    // });
    // $("#myModal").modal('show');
});


// Would benefit from https://github.com/Leaflet/Leaflet/issues/4461
function addNonGroupLayers(sourceLayer) {
    if (sourceLayer instanceof L.LayerGroup) {
        sourceLayer.eachLayer(function (layer) {
            addNonGroupLayers(layer);
        });
    } else {
        mapsList[0].drawControlid_geoms.options.edit.featureGroup.addLayer(sourceLayer);
    }
}


function removeNonGroupLayers(layers) {
    layers.eachLayer(function (layer) {
        mapsList[0].drawControlid_geoms.options.edit.featureGroup.removeLayer(layer);
    })
}

$("#button-id-voronoi").click(function (e) {
    e.preventDefault();
    if (regionlayer) {
        removeNonGroupLayers(regionlayer)
        regionlayer.clearLayers();
        control.removeLayer(regionlayer);
    }

    var pointsgrid = {
        type: "FeatureCollection",
        features: []
    }
    var geomcollection = JSON.parse($("#id_geoms").val());
    $(geomcollection.geometries).each(function (idx, val) {
        if (val.type === "Point") {
            var feature = {
                type: "Feature",
                geometry: val
            }
            pointsgrid.features.push(feature);
        }
    });
    var poly = JSON.parse($("#id_boundary").val());
    var mpoly = turf.multiPolygon(poly);

    var intersections = [];
    var chunk = mpoly.geometry.coordinates.features[0].geometry.coordinates;
    chunk.forEach(function (mpoly_f, arr) {
        var psubgrid = [];
        var mpoly_ft = turf.polygon(mpoly_f);
        pointsgrid.features.forEach(function (pt, arr) {
            if (turf.booleanContains(mpoly_ft, pt)) {
                psubgrid.push(pt)
            }
        });
        var buffered_envelope = turf.buffer(turf.bboxPolygon(turf.bbox(mpoly_ft)),
            0.0005, {units: 'miles'});
        var cutout = turf.difference(buffered_envelope, mpoly_ft)
        var psubgrid_ft = {
            "type": "FeatureCollection",
            "features": psubgrid
        }
        var voronoiPolys = turf.voronoi(psubgrid_ft, {bbox: turf.bbox(buffered_envelope)});
        voronoiPolys.features.forEach(function (f, idx, arr) {
            var newf = turf.difference(f, cutout);
            intersections.push(newf)
        });
    });

    var voronoiPolys = {
        "type": "FeatureCollection",
        "features": intersections
    }
    regionlayer = L.geoJSON(voronoiPolys);

    regionlayer.setStyle({
        color: 'brown',
        fillOpacity: 0.2,
        weight: 2
    })

    addNonGroupLayers(regionlayer);
    control.addOverlay(regionlayer, "Voronoi");
});

$("#submit-id-genmap").click(function (e) {
    e.preventDefault();
    try {
        $("#id_regions").val(JSON.stringify(regionlayer.toGeoJSON()));
    } catch (e) {
    }
    $("#id-updateFieldFormSampling").submit();
});

function squaregrid(area) {
    if (samplelayer) {
        samplelayer.clearLayers();
        $("#id_points").val('');
    }
    if (regionlayer) {
        removeNonGroupLayers(regionlayer);
        regionlayer.clearLayers();
        control.removeLayer(regionlayer);
        $("#id_regions").val("");
    }
    if (buffered_boundary) {
        buffered_boundary.clearLayers();
    }
    var sqm_per_ac = 0.0015625;
    var radius = Math.sqrt((area * sqm_per_ac));

    var poly = JSON.parse($("#id_boundary").val());
    var options = {units: 'miles'};
    var buffered_poly = turf.buffer(poly, radius, options);
    buffered_boundary = L.geoJSON(buffered_poly);
    buffered_boundary.setStyle({
        color: 'orange',
        fillOpacity: 0,
    });
    buffered_boundary.addTo(mapsList[0]);
    var ext = turf.bbox(buffered_poly);
    var mask = poly.features[0].geometry;
    var grid = turf.squareGrid(ext, radius, options);
    var pts = [];
    var regions = [];
    var nsoffset = parseFloat($("#nsoffset").val()) * 0.000189394;
    var ewoffset = parseFloat($("#ewoffset").val()) * 0.000189394;
    var rotation = parseFloat($("#roffset").val());

    var start = turf.centroid(grid);
    var easting;
    if (ewoffset >= 0) {
        easting = turf.rhumbDestination(start, ewoffset, 90);
    } else {
        easting = turf.rhumbDestination(start, -ewoffset, -90);
    }

    var northing;
    if (nsoffset >= 0) {
        northing = turf.rhumbDestination(easting, nsoffset, 0);
    } else {
        northing = turf.rhumbDestination(easting, -nsoffset, 180);
    }

    var rhbearing = turf.rhumbBearing(start, northing);
    var rhdistance = turf.rhumbDistance(start, northing, options);
    grid = turf.transformTranslate(grid, rhdistance, rhbearing, options);
    grid = turf.transformRotate(grid, rotation);

    grid.features.forEach(function (f) {
        var intersection = turf.intersect(f, turf.polygon(mask.coordinates[0]));
        if (intersection) {
            if (turf.inside(turf.centerOfMass(intersection), mask)) {
                var pt = turf.centerOfMass(intersection);
                pt.properties['serial'] = grid.features.indexOf(f) + 1;
                pts.push(pt);
            }
            regions.push(intersection);
        }
    });
    var pointsgrid = {
        type: "FeatureCollection",
        features: pts
    }


    samplelayer = L.geoJSON(pointsgrid, {
        onEachFeature: function (feature, layer) {
            layer.bindTooltip(feature.properties['serial'].toString());
        },
    });
    $("#id_points").val(JSON.stringify(pointsgrid));

    var regiongrid = {
        type: "FeatureCollection",
        features: regions
    }


    regionlayer = L.geoJSON(regiongrid);
    regionlayer.setStyle({
        fillOpacity: 0
    });

    addNonGroupLayers(regionlayer);
    control.addOverlay(regionlayer, "Regions");
}


function update_field(selector, increment) {
    var myval = parseFloat($(selector).val());
    $(selector).val((myval + increment).toString());
}

function reset_field(selector) {
    $(selector).val("0");
}

$(document).ready(function () {

    $(".btn-rounded").click(function (event) {
        var tgt = $(event.target).closest("button");
        var button_id = tgt.attr("id");
        if (button_id == 'grid_counterclock') {
            update_field("#roffset", -1);
        }
        if (button_id == 'grid_clock') {
            update_field("#roffset", 1);
        }
        if (button_id == 'grid_north') {
            update_field("#nsoffset", 10);
        }
        if (button_id == 'grid_south') {
            update_field("#nsoffset", -10);
        }
        if (button_id == 'grid_east') {
            update_field("#ewoffset", 10);
        }
        if (button_id == 'grid_west') {
            update_field("#ewoffset", -10);
        }
        if (button_id == 'grid_reset') {
            reset_field("#roffset");
            reset_field("#nsoffset");
            reset_field("#ewoffset");
        }
        var area = $("input#gridarea").val();
        squaregrid(area);
    });

    $(".grid_text").change(function () {
        var area = $("input#gridarea").val();
        squaregrid(area);
    })
})

function add_accurate_sample_point(samplingmap) {
    var pin = $("#precise_pin");
    if (pin.length === 0) {
        var precisePin = L.control({position: "topleft"});
        precisePin.onAdd = function (map) {
            var div = L.DomUtil.create('div');
            L.DomUtil.addClass(div, "leaflet-bar");
            div.innerHTML = `
    <a class="leaflet-control-layers leaflet-control-layers-precise-pin leaflet-bar-part" style="outline: none;"
        href="#" id="precise_pin" title="Record this precise location"><i class="fa fa-crosshairs"></i></a>`;
            return div;
        };
        samplingmap.addControl(precisePin);
    }
    $("#precise_pin").off('click').on('click', function (evt) {
        evt.preventDefault();
        var lyr = new L.FeatureGroup()
        L.marker(location_marker._latlng).addTo(lyr);
        addNonGroupLayers(lyr);
        mapsList[0].drawControlid_geoms._toolbars.edit._modes.edit.handler.enable();
        mapsList[0].drawControlid_geoms._toolbars.edit._save();
        alert("Layer added at (" + location_marker._latlng.lat + ", " + location_marker._latlng.lng + ")");
        // mapsList[0].drawControlid_geoms._toolbars.edit._modes.edit.handler.disable();
    });
}
