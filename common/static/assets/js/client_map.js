var field_map;
var layer;

function map_init(map, options) {
    layer = L.geoJson(collection, {
        onEachFeature: function (feature, layer) {
            layer.bindPopup(
                "<a href='/clients/field/" + feature.properties.id + "/gis/'>" +
                "<h4>" + feature.properties.name + "</h4></a>");
        }
    });
    layer.addTo(map);
    field_map = map;
    field_map.scrollWheelZoom.disable();
    setTimeout(function () {
        try {
            field_map.fitBounds(layer.getBounds());
            field_map._onResize();
            field_map.invalidateSize();
        } catch (err) {
        }
    }, 2000);
}