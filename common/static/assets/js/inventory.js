$(document).ready(function () {
    var assetsTable = $('#assettable').DataTable({
        order: [[0, "asc"]],
        paging: true,
        stateSave: true,
        responsive: true,
        pagingType: "numbers",
        columnDefs: [
            {
                name: "name",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "template",
                orderable: true,
                searchable: true,
                targets: [1]
            },
            {
                name: "location",
                orderable: false,
                searchable: true,
                targets: [2]
            },
            {
                name: "contact",
                orderable: false,
                searchable: true,
                targets: [3]
            },
            {
                name: "notes",
                orderable: false,
                searchable: true,
                targets: [3]
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": assets_action_url
    });

    var templateTable = $('#templatetable').DataTable({
        order: [[0, "asc"]],
        paging: true,
        stateSave: true,
        responsive: true,
        pagingType: "numbers",
        columnDefs: [
            {
                name: "description",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "brand",
                orderable: true,
                searchable: true,
                targets: [1]
            },
            {
                name: "model",
                orderable: false,
                searchable: true,
                targets: [2]
            },
            {
                name: "part_number",
                orderable: false,
                searchable: true,
                targets: [3]
            },
            {
                name: "notes",
                orderable: false,
                searchable: true,
                targets: [4]
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": templates_action_url
    });

    var consumablesTable = $('#consumablestable').DataTable({
        order: [[0, "asc"]],
        paging: true,
        stateSave: true,
        responsive: true,
        pagingType: "numbers",
        columnDefs: [
            {
                name: "name",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "quantity",
                orderable: false,
                searchable: false,
                targets: [1]
            },
            {
                name: "template",
                orderable: true,
                searchable: true,
                targets: [2]
            },
            {
                name: "location",
                orderable: false,
                searchable: true,
                targets: [3]
            },
            {
                name: "contact",
                orderable: false,
                searchable: true,
                targets: [4]
            },
            {
                name: "notes",
                orderable: false,
                searchable: true,
                targets: [5]
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": consumables_action_url
    });
    if(typeof (search) != "undefined") {
        assetsTable.search(search).draw();
    }
});