var featureStyle = function (feature) {
    if ("orders" in feature.geometry.properties) {
        if (feature.geometry.properties.orders.length === 0) {
            return {color: "gray"}
        }
        try {
            return {color: feature.geometry.properties.orders[0].products[0].color};
        } catch (e) {
            return {color: "gray"}
        }
    } else {
        if (feature.geometry.properties.events.length === 0) {
            return {color: "gray"}
        }
        return {color: feature.geometry.properties.events[0].color};
    }
}

var format_tooltip = function (object) {
    var iconlist = "";
    if ("orders" in object) {
        $.each(object.orders, (idx, order) => {
            $.each(order.products, (idx, prod) => {
                iconlist += prod.image ? `<img src="${prod.image}" class="prodicon">` : "";
                $.each(prod.image_set, (idx, image) => {
                    iconlist += `<img src="${image}" class="pkgicon">`;
                });
            });
        });
    } else {
        $.each(object.events, (idx, event) => {
            iconlist += event.extendedProps.icon ? `<img src="${event.extendedProps.icon}" class="prodicon">` : "";
        });
    }
    return iconlist
}

var format_popup = function (layer) {
    var popupDiv = $(".popup_template").clone();
    popupDiv.removeClass("popup_template");
    popupDiv.find(".popup_title").html(layer.feature.geometry.properties.name);
    popupDiv.find("a.popup_link").attr("href", `/clients/field/${layer.feature.geometry.properties.id}/`);
    var itemdiv = popupDiv.find("div.item_data");
    if ("orders" in layer.feature.geometry.properties) {
        $.each(layer.feature.geometry.properties.orders, (idx, order) => {
            var orddiv = $(".order_template").clone();
            orddiv.removeClass("order_template");
            orddiv.find("a.order_link").attr("href", `/orders/${order.id}`);
            orddiv.find(".order_date").html(moment(order.date_modified).format("MM/DD/YYYY"));
            orddiv.find('span.order').html(order.order_number);
            orddiv.find('span.order_status').html(order.order_status.name);
            var prods = "";
            $.each(order.products, (idx, prod) => {
                let prod_image = prod.image ? `<img src="${prod.image}" class="prodicon">` : "";
                let imgset = "";
                if (prod.is_package) {
                    imgset += "<br>";
                    $.each(prod.image_set, (idx, image) => {
                        imgset += `<img src="${image}" class="pkgicon">`;
                    });
                }
                prods += `<li class="li-popup">${prod_image} <span style="color:${prod.color};">${prod.name}</span>${imgset}</li>`;
            });
            orddiv.find('ul.products').html(prods);
            itemdiv.append(orddiv);
        });
    } else {
        // must be events
        $.each(layer.feature.geometry.properties.events, (idx, event) => {
            var evtdiv = $(".event_template").clone();
            evtdiv.removeClass("event_template");
            if (idx > 4) {
                evtdiv.addClass("overage");
            }
            var title = evtdiv.find(".event_title");
            let icon = event.extendedProps.icon ? `<img src="${event.extendedProps.icon}" class="prodicon">` : "";
            title.html(`${icon} ${event.title}`);
            title.css("background-color", event.color ? event.color : "white");
            title.css("color", event.color ? event.textColor : "black");
            evtdiv.find(".event_date").html(moment(event.start).format("MM/DD/YYYY"));
            var events = "";
            $.each(event.extendedProps.custom_fields, (idx, cf) => {
                let cf_item = `<li class="li-popup">${cf.name}: ${cf.value}</li>`;
                events += cf_item;
            })
            evtdiv.find('ul.event_data').html(events);
            itemdiv.append(evtdiv);
        });
    }
    $("#popup_details").html(popupDiv.html());
    $(popupDiv).find("div.overage").remove();
    return popupDiv.html();
}

function create_feature(object, active) {
    var poly = JSON.parse(object.boundary);
    poly.properties = object;
    let layer = L.geoJSON(poly, {
        style: featureStyle
    }).bindPopup(format_popup);

    //let tt = L.tooltip().setContent(`<div class="ttrow clearfix">${format_popup(layer)}</div>`);
    //layer.bindTooltip(tt, {
    //    permanent: true,
    //    className: "leaflet-tooltip-custom"
    //});
    //layer.on("mouseover", () => {
    //    layer.openPopup();
    // });
    //layer.on("mouseout", () => {
    //    layer.closePopup();
    // });
    return layer
}

function get_fields(extent) {
    let active = $("ul#fieldsform li.active").find("a").attr("href");
    var urlbase, datepicker, category;
    if (active === "#events") {
        urlbase = events_url_base;
        datepicker = "#datetimepicker1";
        category = "#event_categories"
    } else {
        urlbase = order_url_base;
        datepicker = "#datetimepicker2";
        category = "#products_categories"
    }

    var time = $(datepicker).val();
    var start = moment(time.split(" - ")[0], 'M/DD/YYYY hh:mm A');
    var end = moment(time.split(" - ")[1], 'M/DD/YYYY hh:mm A');
    var timestring = `&start=${start.toISOString()}&end=${end.toISOString()}`;
    var catstring = `&category=${$(category).val()}`;
    $.getJSON(urlbase + extent + timestring + catstring, (data) => {
        ftgroup = L.featureGroup();
        $.each(data, (idx, val) => {
            let feature = create_feature(val, active);
            ftgroup.addLayer(feature);
        });
        ftgroup.addTo(mapsList[0])
        // mapsList[0].fitBounds(ftgroup.getBounds());
        mapready = true;
        mapsList[0].spin(false);
    });
}

refresh_map = function (queued) {
    if (mapready) {
        mapready = false;
        mapsList[0].spin(true);
        $("#popup_details").html("<h4>Click on a field for more details</h4>")
        let ext = mapsList[0].getBounds();
        let extent = `?extent=${ext._northEast.lat},${ext._northEast.lng},${ext._southWest.lat},${ext._southWest.lng}`;
        try {
            if ("_leaflet_id" in ftgroup) {
                mapsList[0].removeLayer(ftgroup);
            }
        } catch (e) {
            console.log(e);
        }
        get_fields(extent);
    } else {
        if (queued) {
            console.log("queued, skipping");
        } else {
            console.log("waiting");
            setTimeout(() => {
                refresh_map(true)
            }, 500);
        }
    }
}

reset_datetime_picker = function (picker) {
    $(picker).daterangepicker({
        timePicker: true,
        autoApply: true,
        startDate: moment().startOf('hour').subtract(1, 'year'),
        endDate: moment(),
        locale: {
            format: 'M/DD/YYYY hh:mm A'
        }
    });
    $(picker).on("apply.daterangepicker", (ev, pkr) => {
        refresh_map();
    });
}

$("#products_categories").on("change", refresh_map);
$("#event_categories").on("change", refresh_map);

$(document).ready(function () {
    reset_datetime_picker("#datetimepicker1");
    reset_datetime_picker("#datetimepicker2");
    setTimeout(() => {
        mapsList[0].on("zoomend moveend", (evt) => {
            refresh_map();
        });
    }, 2000);
    $(document).on('shown.bs.tab', function (e) {
        refresh_map();
    });
});