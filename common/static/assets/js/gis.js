"use strict";
var MyGeometryField;
var procSocket = null;

L.Map.addInitHook(function () {
    mapsList.push(this);
    var boundary_layer;
    MyGeometryField = L.GeometryField.extend({
        addTo: function (map) {
            L.GeometryField.prototype.addTo.call(this, map);
            // Customize map for field
            boundary_layer = this;
            boundary_layer.drawnItems.setStyle({fillOpacity: 0.05});
            control.addOverlay(boundary_layer.drawnItems, "Boundary");
        }
    });
    this.on('click', function () {
        L.gridLayer.googleMutant({
            type: 'hybrid' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        }).addTo(this);
        this.panTo(this.getCenter());
        this._onResize();
        this.invalidateSize();
    });
    this.scrollWheelZoom.disable();
    let a = this;
    setTimeout(function () {
        a.fireEvent("click");
    }, 1000);
    control.addTo(this);
});

$("#button-id-updatebbox").prop("disabled", true);


function setup_buttons() {
    $('.add-another').off('click').on('click', function (e) {
        e.preventDefault();
        showAddAnotherPopup(this);
    });
    $('.related-lookup').off('click').on('click', function (e) {
        e.preventDefault();
        showRelatedObjectLookupPopup(this);
    });
    $("#button-id-updatebbox").off('click').on('click', function (e) {
        e.preventDefault();
        updateBbox();
    });
    $(".btn-remove-cache").off('click').on('click', function (e) {
        e.preventDefault();
        if (confirm("OK to remove all cached images?")) {
            remove_cache(e);
        }
    });
    $(".btn-duplicate-layer").off('click').on('click', function (e) {
        e.preventDefault();
        if (confirm("Create a new layer with the geometry from this layer?")) {
            duplicate_layer(e);
        }
    });
    $(".btn-set-boundary").off('click').on('click', function (e) {
        e.preventDefault();
        if (confirm("Simplify and recreate field boundary with the geometry from this layer?")) {
            set_boundary(e);
        }
    });
    $(".bring-in").off('click').on('click', function (e) {
        e.preventDefault();
        if (confirm("Merge the layer?")) {
            merge_layer(e);
        }
    });
    $(".apply-formula").off('click').on('click', function (e) {
        e.preventDefault();
        if (confirm("Apply Formula?")) {
            apply_formula(e);
        }
    });
    $("a.set_response").off('click').on('click', function (e) {
        e.preventDefault();
        move_to_div($(e.target).closest("div.btn-group"), $("#table_yield"));
    });
    $("a.set_influencer").off('click').on('click', function (e) {
        e.preventDefault();
        move_to_div($(e.target).closest("div.btn-group"), $("#table_others"));
    });
    $("a.just_plot").off('click').on('click', function (e) {
        e.preventDefault();
        move_to_div($(e.target).closest("div.btn-group"), $("#table_plotonly"));
    });

    $(".btn-geo").off('click').on('click', function (e) {
        e.preventDefault();
        var tgt = $(e.target);
        generate_map(tgt.attr("href"), tgt.attr("href") + "raster/",
            mapsList[0], L.DomUtil.create('div', 'info legend'));
    });
    $(".btn-refresh").off('click').on('click', function (e) {
        e.preventDefault();
        var tgt = $(e.target);
        get_layer_div_ws(tgt.attr('href'));
    });
    $('button.dropdown-toggle').each(function (idx, val) {
        // $(val).draggable({
        //     addClasses: false
        // });
        $(val).attr({'draggable': true});
        $(val).bind("dragstart", function (ev) {
            drag(ev.originalEvent)
        });
    })
    $("a.polygonize").off('click').on('click', function (e) {
        e.preventDefault();
        var tgt = $(e.target);
        $.post(tgt.attr("href"), function (data) {
            alert("Done. New layer brought in")
            bring_in_new_layer(data);
        });
    })
}

(function ($) {
    $(document).ready(function () {
        wsconnect();

        $.getJSON("/clients/api/v1/field/{0}/layers/?polygons_only=1".formatUnicorn(fieldpk),
            function (data) {
                // console.log("Got targets");
                targets = data;
                waitForSocketConnection(procSocket, function () {
                    $(".gisstub").each(function (idx, val) {
                        var lyrid = $(val).attr('lyrid');
                        get_layer_div_ws(lyrid);
                    });
                });
            });

        $("#submit-id-genreport").prop('disabled', true);
        setup_buttons();

        $("#gis_layer_search").on('keyup', function () {
            var search = $("#gis_layer_search").val();
            $(".gisstub").each(function (idx, div) {
                var lyrname = $(div).attr('name');
                lyrname.toLowerCase().includes(search.toLowerCase()) ? $(div).show() : $(div).hide();
            });
        });

        // Added from GoogleMutant API
        $("#datepicker").daterangepicker({
            singleDatePicker: true,
        });


    });

})(jQuery);


function wsconnect() {
    $("#risk_calculator").prop("disabled", true);
    $("button#sampling_map").prop('disabled', true);

    procSocket = new WebSocket(
        (location.protocol == 'http:' ? 'ws://' : 'wss://') +
        window.location.host + window.location.pathname + 'ws/'
    );
    $("#procmessages").html("Ready!<br>");

    procSocket.onmessage = function (e) {
        var data = JSON5.parse(e.data);
        $("#procmessages").append(data['message'] + "<br>");
        if (data['message'] === "done" & data['response'] === "variability") {
            $("#procmessages").html("Ready!<br>");
            var dat = data['results'];
            console.log(dat);
            var tiff = dat['tiff'].split("/").slice(-2);
            latest_raster = {
                layer: tiff[0],
                image: tiff[1]
            }
            $("#id_report_data").val(JSON.stringify(dat));
            var imageurl = dat.file;
            try {
                var fb = L.geoJson(JSON.parse($("#id_boundary").val()));
                raster = L.imageOverlay(imageurl, fb.getBounds()).addTo(mapsList[0]);
                control.addOverlay(raster, "Variability");
            } catch (e) {
                console.log(e);
                try {
                    raster = L.imageOverlay(imageurl, dat.extent).addTo(mapsList[0]);
                    control.addOverlay(raster, "Variability");
                } catch (e) {

                }
            }
            try {
                mapsList[0].fitBounds(dat.extent);
                $("#risk-report").collapse("show");
                make_analytics_table(dat);
            } catch (e) {
                console.log(e);
            }

            calculated_risk_data = dat;
            $("#submit-id-genreport").prop("disabled", false);

            $("#risk_calculator").prop("disabled", false);
            $("#sampling_map").prop("disabled", false);
            //$("#fsn_zone").html("<img src='" + imageurl + "' width=100>");
        }
        if (data['message'] === "done" & data['response'] === "nrec") {
            $("#procmessages").html("Ready!");
            var dat = data['results'];
            console.log(dat);

        }
        if (data['message'] === 'error') {
            var error = data['description'];
            $("#procmessages").append(error);
            console.log(error);
        }
        if (data['message'] === "layerstub") {
            receive_lyr_div(data);
        }
    };

    procSocket.onclose = function (e) {
        $("#procmessages").html("Waiting for GIS calculator");
        console.log("Proc socket closed unexpectedly; Will reconnect in 3s");
        setTimeout(function () {
            wsconnect();
        }, 3000);
    };

}

function get_layer_div(lyrid) {
    var url = "/clients/api/v1/layer/" + lyrid + "/stub/";
    $.getJSON(url, function (data) {
        $("#layer_" + lyrid).html(data[lyrid]);
        setup_buttons();
    });
}

function get_layer_div_ws(lyrid) {
    procSocket.send(JSON.stringify({
        'request': 'layerstub',
        'lyrid': String(lyrid)
    }));
}

function receive_lyr_div(content) {
    var lyrid = content.lyrid;
    $("#layer_" + lyrid).html(render_stub(content.div));
    setup_buttons();
    setTimeout(function () {
        $("#procmessages").html("Ready!");
    }, 2000);
}

// Make the function wait until the connection is made...
function waitForSocketConnection(socket, callback) {
    setTimeout(
        function () {
            if (socket.readyState === 1) {
                if (callback != null) {
                    callback();
                }
            } else {
                waitForSocketConnection(socket, callback);
            }
        }, 5); // wait 5 milisecond for the connection...
}

function updateBbox() {
    if (confirm("Are you sure you want to use the " + propdata.parameter +
        " parameter to update field boundaries?")) {
        var boundary = {
            "type": "MultiPolygon",
            "coordinates": []
        }
        boundary.coordinates = [[propdata.bbox]];
        $("#id_boundary").val(JSON.stringify(boundary));
        $("#submit-id-submit").click();
    } else {
        //
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data).parentElement);
    $("#risk_calculator").prop("disabled", false);
}

function recdrop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
    $("#rec_calculator").prop("disabled", false);
    var recfields = [
        'yld',
        'orgc',
        'orgn',
        'aws',
        'ph',
        'slp'
    ];
    recfields.forEach(function (f) {
        var dat = $("td#" + f);
        if (dat.children().length != 1) {
            $("#rec_calculator").prop("disabled", true);
        }
    });
}

function calculate_rec() {
    var rec_input = {};

    var recfields = [
        'yld',
        'seed',
        'orgc',
        'orgn',
        'aws',
        'ph',
        'slp',
        'offset'
    ];

    var shps = {};
    var factors = {};

    recfields.forEach(function (f) {
        var dat = $("td#" + f);
        shps[f] = dat.children('a')[0] ? dat.children('a')[0].id : null;
        var inp = $("input[name='{0}_factor']".formatUnicorn(f));
        factors[f] = inp ? inp.val() : null;
    });

    rec_input['shapes'] = shps;
    rec_input['factors'] = factors;
    rec_input['request'] = 'nrec';
    console.log(rec_input);
    procSocket.send(JSON.stringify(rec_input));
}

function get_sampling_map() {
    var data = JSON.parse($("#id_report_data").val());
    loc += "?extent=" + encodeURI(data['extent']);
    loc += "&file=" + encodeURI(data['file']);
    window.location.href = loc;
}

function make_zones() {
    var url = "/clients/api/v1/layer/{0}/Variability/polygonize/?process=polygonize".formatUnicorn(latest_raster.layer);
    $.post(url, function (data) {
        console.log(data);
        bring_in_new_layer(data);
        alert("Zones created on most recent variability analysis")
    });
}

function calculate_risk() {
    $("#risk_calculator").prop("disabled", true);
    $("#button-id-genreport").prop("disabled", true);
    $("button#sampling_map").prop("disabled", true);
    mapsList[0].removeControl(legend);
    var yield_parameters = $.map($("#table_yield").find("a.btn-geo"),
        function (e) {
            return $(e).attr("id")
        });

    var other_parameters = $.map($("#table_others").find("a.btn-geo"),
        function (e) {
            return $(e).attr("id")
        });

    var plotonly_parameters = $.map($("#table_plotonly").find("a.btn-geo"),
        function (e) {
            return $(e).attr("id")
        });


    var risk_data = {
        'request': 'variability',
        "yield[]": yield_parameters,
        "others[]": other_parameters,
        "plotonly[]": plotonly_parameters
    };

    create_default_table(risk_data);
    procSocket.send(JSON.stringify(risk_data));
}

function create_default_table(risk_data) {
    var html = "<h4>Results</h4>\n" +
        "<p>Results arranged in increasing order of the first term. " +
        "Values near the edges have greater computational errors and may be ignored</p>" +
        "<table class=\"table table-striped table-bordered\", id=\"calculation_table\">" +
        "                        <thead>" +
        "                        <tr id=\"result_columns\">" +
        "                            <th>Color</th>" +
        "                            <th>% Area</th>" +
        "                            <th>Acres</th>" +
        "                        </tr>" +
        "                        </thead>" +
        "                        <tbody>" +
        "                        </tbody>" +
        "                    </table>"
    $("#calculation_table_div").html(html);
    $("#variability_div").html("<h4>Field Variability Index<h4>");
    $("#regression_div").html("<h4>Computed Influences</h4>");
    risk_data["yield[]"].forEach(function (e) {
        console.log(e);
        $("#result_columns").append("<th>" + parameter_format(e) + "</th>");
    });
    risk_data["others[]"].forEach(function (e) {
        console.log(e);
        $("#result_columns").append("<th>" + parameter_format(e) + "</th>");
    });
}

function parameter_format(e) {
    var comps = e.split(":");
    return `${comps[2]} (${comps[3]}): ${comps[4]}`
}

function make_analytics_table(data) {
    var cols = $("#result_columns").children().length;
    var targets = [];
    for (var i = 1; i < cols; i++) {
        targets.push(i);
    }
    var table = $("#calculation_table").DataTable({
        paging: false,
        searching: false,
        info: false,
        "columnDefs": [
            {
                "render": $.fn.dataTable.render.number("", ".", 1),
                "targets": targets
            }
        ]
    });
    var pixels = data.counts.reduce(function (a, b) {
        return a + b;
    }, 0);

    data.counts.forEach(function (count, idx) {
        var l = "<span class='fa fa-2x fa-square' style='color:" + data.colors[idx] + "'></span>";
        var r = [l];
        var pc_area = count / pixels * 100
        r.push(pc_area);
        var area = count / pixels * data.area;
        r.push(area);
        let r2 = r.concat(data.centers[idx]);
        table.row.add(r2);
    });
    table.draw();
    $("#variability_div").append(
        'On a scale of 0 (perfectly uniform) to 1 (perfectly random): ' +
        data['complexity'].toFixed(3));
    data.risk.forEach(function (reg) {
        var yld = reg[0];
        var influencer = reg[1];
        var confint_yld = reg[2][0];
        var confint_inf = reg[2][1];
        var rsq_adj = reg[3];
        var summary = reg[5];
        var html = '<div class="col-md-6 col-lg-4">\n' +
            '                                    <div class="panel panel-default">\n' +
            '                                       <div class="panel-heading">' +
            '<b>Response: ' + yld + '</b><br>' +
            '<b>Influencer: ' + influencer + '</b>' +
            '                                       <div class="panel-body">\n' +
            'Intercept between ' +
            confint_yld[0].toFixed(1) + ' and ' + confint_yld[1].toFixed(1) + ' units <br>' +
            'Impact on Response between ' +
            confint_inf[0].toFixed(3) + ' and ' + confint_inf[1].toFixed(3) + ' units per unit of influence <br>' +
            'Adjusted R-squared: ' + rsq_adj.toFixed(2) + '<br>' +
            '                                       </div>\n' +
            '                                    </div>\n' +
            '<div class="panel-body"><pre style="font-size: 12px">{0}</pre></div>'.formatUnicorn(summary) +
            '                                 </div>';
        $("#regression_div").append(html);
    })
}

function remove_cache(event) {
    var url = $(event.target).prop('href');
    $.post({
        url: url,
        data: {},
        success: function (data) {
            alert("Removed {0} items".formatUnicorn(data.length));
        },
        dataType: 'json'
    })
}

function bring_in_new_layer(data) {
    var lyrid = data.id;
    var div = '<div id="layer_{0}" class="gisstub" lyrid={0} name="{1}"></div>'.formatUnicorn(lyrid, data.name);
    $("#gisstubs").prepend(div);
    $("#stubcount").html(parseInt($("#stubcount").html()) + 1);
    get_layer_div_ws(lyrid);
}

function duplicate_layer(event) {
    var url = $(event.target).prop('href');
    $.post({
        url: url,
        data: {},
        success: function (data) {
            // alert("Duplicated! ({0}). Refresh the page to see it".formatUnicorn(data.length));
            bring_in_new_layer(data);
        },
        dataType: 'json'
    })
}

function merge_layer(event) {
    var url = $(event.target).prop('href');
    $.post({
        url: url,
        data: {},
        success: function (data) {
            alert(data.message);
            get_layer_div_ws(data.target);
        },
        dataType: 'json'
    })
}

function apply_formula(event) {
    var url = $(event.target).prop('href');
    $.post({
        url: url,
        data: {},
        success: function (data) {
            alert(data.message);
            get_layer_div_ws(data.target);
        },
        dataType: 'json'
    })
}

function set_boundary(event) {
    var url = $(event.target).prop('href');
    $.post({
        url: url,
        data: {},
        success: function (data) {
            alert(data.status);
        },
        dataType: 'json'
    })
}

function move_to_div(elem, div) {
    div.append(elem);
    $("#risk_calculator").prop("disabled", false);
}

function add_legend(data, map, div) {
    var render_color;
    var grades = [];

    if (data.legend) {
        color_array = [];
        var abundance = [];
        $.each(data.legend, function (idx, val) {
            color_array.push(val[0]);
            grades.push(val[2]);
            abundance.push(val[1]);
        });

        if ("min" in data) {
            legend.onAdd = function () {
                return make_premade_legend("{0}<br>{1} to {2}".formatUnicorn(data.parameter,
                    rounding(data.min), rounding(data.max)),
                    grades, abundance, color_array, div);
            };
            legend.addTo(map);
        }

        render_color = function (val) {
            var mycol = color_array[0];
            var i = 0;
            while (val > grades[i]) {
                mycol = color_array[i];
                i += 1;
            }
            return color_array[i];
        };
    } else {
        var range = data.max - data.min;
        var color_array = default_color_array;
        var unit = range / color_array.length;
        grades = [];
        $.each(color_array, function (idx, val) {
            grades.push(rounding(parseFloat(data.min) + idx * unit));
        });
        try {
            if (data.min) {
                legend.onAdd = function () {
                    return make_legend("{0}<br>{1} to {2}".formatUnicorn(data.parameter,
                        rounding(data.min), rounding(data.max)),
                        grades, color_array, div);
                };
                legend.addTo(map);
            }
        } catch (err) {
            console.log(err);
        }
        render_color = function (val) {
            var interp = (val - data.min) / unit;
            return color_array[Math.min(color_array.length - 1, Math.round(interp))];
        };
    }
    return render_color
}

function make_legend(title, grades, color_array, div) {
    div.innerHTML = '<h4>{0}</h4>'.formatUnicorn(title);
    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + color_array[i] + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br/>' : '+');
    }
    return div;
}

function make_premade_legend(title, grades, abundance, color_array, div) {
    div.innerHTML = '<h4>{0}</h4>'.formatUnicorn(title);
    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        var key = abundance[i] + ": ";
        if (i === 0) {
            key += "<" + grades[i];
        } else if (i === grades.length - 1) {
            key += grades[i - 1] + "+";
        } else {
            key += grades[i - 1] + "&ndash;" + grades[i];
        }
        div.innerHTML +=
            '<i style="background:' + color_array[i] + '"></i> ' + key + "<br>";
    }
    return div;
}