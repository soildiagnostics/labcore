function render_stub(object) {
    var panel_template = $("#panel-template").clone();
    var heading = panel_template.find("#heading");
    heading.attr("id", "heading" + object.id);
    heading.find('.panel-title').append(render_panel_heading(panel_template, object));
    panel_template.removeAttr("style");
    panel_template.attr("id", "panel_" + object.id);
    var params = panel_template.find("div#field_names-template");
    params.attr('id', "parameters_" + object.id)
    params.append(generate_dbf_field(object));
    params.removeAttr("style");
    return panel_template;
}

function render_panel_heading(panel_template, object) {
    var panel_top = panel_template.find('a[href="#collapse_l_id"]');
    panel_top.attr('href', "#collapse" + object.id);
    var panel_buttons = [];
    if (object.editable) {
        panel_buttons.push('<i class="fa fa-lock-open">&nbsp;</i>');
    } else {
        panel_buttons.push('<i class="fa fa-lock">&nbsp;</i>');
    }
    if (object.value_keys.order && object.polygons_only) {
        panel_buttons.push('<i class="fa fa-calculator" style="color: green">&nbsp;</i>');
    }
    if (object.points_only) {
        panel_buttons.push('<i class="fa fa-map-pin" title="Point layer">&nbsp;</i>');
    } else if (object.polygons_only) {
        panel_buttons.push('<i class="fa fa-vector-square" title="Polygon layer">&nbsp;</i>');
    } else if (object.features > 0) {
        panel_buttons.push('<i class="fa fa-shapes">&nbsp;</i>');
    }
    if (object.raster === true) {
        panel_buttons.push('<i class="fa fa-images">&nbsp;</i>');
    }
    if (object.recommendation === true) {
        panel_buttons.push('<i class="fa fa-prescription">&nbsp;</i>');
    }
    if (object.value_keys.includes('order')) {
        panel_buttons.push('<i class="fa fa-calculator" style="color: green">&nbsp;</i>');
    }
    panel_top.append(panel_buttons);
    panel_top.append(object.name);
    var panel_collapse = panel_template.find('div#collapse_l_id');
    panel_collapse.attr('id', "collapse" + object.id);
    panel_collapse.attr('aria-labelledby', "heading" + object.id);
    var panel_body = panel_template.find('div.layer-actions');
    panel_body.append('<div>{0} features, created {1}, modified {2} </div>'.formatUnicorn(object.features !== null ? object.features : 0,
        moment(object.date_created).fromNow(),
        moment(object.date_modified).fromNow()));
    panel_body.append("<div>");
    panel_body.append(('<a href="{0}"' +
        '                   class="btn btn-xs pull-right btn-refresh" title="Refresh">' +
        '                    <i class="fa fa-sync" style="color: saddlebrown"></i>' +
        '                </a>').formatUnicorn(object.id));
    panel_body.append(('<a href="/clients/layer/{0}/details/"' +
        '                   class="btn btn-xs" title="View details">' +
        '                    <i class="fa fa-eye" style="color: blue"></i>' +
        '                </a>').formatUnicorn(object.id));
    if (object.editable) {
        panel_body.append(('<a href="/clients/layer/{0}/edit/"' +
            '                       class="btn btn-xs" title="Edit name">' +
            '                        <i class="fa fa-edit" style="color: blue"></i>' +
            '                    </a>').formatUnicorn(object.id));
    }
    panel_body.append(('<a href="/clients/layer/{0}/deletelayer/"' +
        '                       class="btn btn-xs" title="Delete layer">' +
        '                        <i class="fa fa-trash" style="color: red"></i>' +
        '                    </a>').formatUnicorn(object.id));
    panel_body.append(('<a href="/clients/layer/{0}/removerasters/"' +
        '                       class="btn btn-remove-cache btn-xs" title="Remove cached rasters">' +
        '                        <i class="fa fa-eraser" style="color: red"></i>' +
        '                    </a>').formatUnicorn(object.id));
    if (object.source_file_event !== null) {
        panel_body.append(('<a href="/fieldevents/{0}/download/"' +
            '              class="btn btn-xs" title="Download related file">' +
            '              <i class="fa fa-download" style="color: blue"></i>' +
            '                    </a>').formatUnicorn(object.source_file_event))
    } else {
        panel_body.append(('<a href="/clients/layer/{0}/download/"' +
            '            class="btn btn-xs" title="Get shapefile">' +
            '            <i class="fa fa-download" style="color: blue"></i>\n' +
            '        </a>').formatUnicorn(object.id));
    }
    panel_body.append(('<a href="/clients/api/v1/layer/{0}/duplicate/"' +
        '                   class="btn-duplicate-layer btn btn-xs"' +
        '                   title="Create another Layer with polygons from this one">' +
        '                    <i class="fa fa-copy" style="color: blue"></i>' +
        '                </a>').formatUnicorn(object.id));
    panel_body.append(('<a href="/clients/api/v1/layer/{0}/setboundary/"' +
        '                   class="btn-set-boundary btn btn-xs"' +
        '                   title="Simplify and set as field boundary">' +
        '                    <i class="fa fa-draw-polygon" style="color: blue"></i>' +
        '                </a>').formatUnicorn(object.id));
    if (object.value_keys.includes('order')) {
        panel_body.append(('<a href="/recommendations/{0}/start/"' +
            '                       class="btn-rec btn btn-xs"' +
            '                       title="Generate Recommendation from this layer">' +
            '                        <i class="fa fa-calculator" style="color: green">&nbsp;</i>' +
            '                    </a>').formatUnicorn(object.id))
    }
    return panel_top;
}

function bring_into_targets(object, val, agg) {
    // Need to double encode urls for "/" in nam
    var items = "";
    $(targets).each(function (idx, target) {
        if (target.polygons_only && target.name !== "Soil Survey Data") {
            var link = encodeURI("/clients/layer/{0}/bringinto/{1}/{2}/{3}/".formatUnicorn(object.id,
                target.id, encodeURIComponent(val), agg));
            var liitem = ('<li><a tabindex="-1" class="bring-in"' +
                ' href="{0}">{1}</a></li>').formatUnicorn(link, target.name);
            items += liitem;
        }
    });
    return items;
}

function generate_formula_items(object, val) {
    var items = "";
    $(object.formulas[val]).each(function (idx, formula) {
        var item = ('<li><a tabindex="-1" class="apply-formula"' +
            '   title="{0}"' +
            '   href="/clients/layer/{1}/apply_formula/{2}/">' +
            '   {3}</a></li>').formatUnicorn(formula.description, object.id, formula.id, formula.name)
        items += item;
    });
    return items
}

function generate_dbf_field(object) {
    var button_list = "";
    var btngroup;
    $(object.layer_fields).each(function (idx, val) {
        if (val == "Sampling Map") {
            var link = ('<a href="/clients/layer/{0}/samplingmap/" ' +
                // var link = ('<a href="#" ' +
                'role="button"  ' +
                'class="btn btn-xs btn-bordered-default">{1}</a>').formatUnicorn(object.id, val);
            button_list += link;
        } else {
            btngroup = $(".btn-group.fieldname").clone();
            btngroup.removeClass('fieldname');
            var btnxs = btngroup.find("button.btn.btn-xs.dropdown-toggle");
            btnxs.attr('id', 'btnid:{0}:{1}:{2}:{3}'.formatUnicorn(object.field,
                object.name, object.id, val));
            btnxs.html('{0} <span class="caret"></span>'.formatUnicorn(val))
            if (object.value_keys.includes(val)) {
                btnxs.addClass('btn-bordered-info');
            } else {
                btnxs.addClass('btn-bordered-default');
            }
            var dllist = btngroup.find("ul.mainlist");
            var viewlink = '/clients/field/{0}/gis/{1}/{2}/{3}/'.formatUnicorn(object.field,
                object.id, encodeURIComponent(object.name), encodeURIComponent(val));
            var viewli = ('<li>' +
                ' <a id="id:{0}:{1}:{2}:{3}" class="btn-geo"' +
                ' href="{4}">' +
                ' View on Map</a></li>').formatUnicorn(object.field, object.name, object.id, val, encodeURI(viewlink));
            dllist.append(viewli);
            dllist.append('<li role="separator" class="divider"></li>' +
                '        <li><a class="set_response" href="#">Set as Response</a></li>' +
                '        <li><a class="set_influencer" href="#">Set as Influencer</a></li>' +
                '        <li><a class="just_plot" href="#">Just Plot</a></li>' +
                '        <li role="separator" class="divider"></li>');
            if (object.value_keys.includes(val)) {
                dllist.append('<li class="dropdown-submenu">' +
                    '           <a tabindex="-1" class="merge_layer" href="#">' +
                    '                Bring this data into</a>' +
                    '                <ul class="dropdown-menu">' +
                    bring_into_targets(object, val, 'mean') +
                    '                </ul>\n' +
                    '            </li>');
                if (object.formulas[val]) {
                    dllist.append('<li role="separator" class="divider"></li>' +
                        '                    <li class="dropdown-submenu">' +
                        '                        <a tabindex="-1" class="apply_formula" href="#">' +
                        '                            Apply formula</a>' +
                        '                        <ul class="dropdown-menu">' +
                        generate_formula_items(object, val) +
                        '</ul></li>');
                }
                if (object.points_only) {
                    dllist.append(('<li role="separator" class="divider"></li>' +
                        '                <li><a tabindex="-1" title="Experimental!"' +
                        '   href="/clients/layer/{0}/samplingmap/?create=1">Create Sampling Map</a></li>' +
                        '                <li><a tabindex="-1" title="Experimental!" class="polygonize"' +
                        '   href="/clients/api/v1/layer/{0}/{1}/polygonize/"}>Polygonize</a></li>'
                    ).formatUnicorn(object.id, val));
                }
            } else {
                if (object.raster === true) {
                    dllist.append(('<li role="separator" class="divider"></li>' +
                        '                <li><a tabindex="-1" title="Experimental!" class="polygonize"' +
                        '   href="/clients/api/v1/layer/{0}/Variability/polygonize/?process=polygonize">Polygonize</a></li>'
                    ).formatUnicorn(object.id));
                } else {
                    dllist.append('<li><a href="#">View first to import into other layers</a></li>');
                }
            }
            button_list += btngroup[0].outerHTML;
        }
    });
    return button_list
}
