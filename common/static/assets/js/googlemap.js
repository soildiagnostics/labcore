var mapsList = [];
var boundary_layer;
var location_marker;

L.Map.addInitHook(function () {
    mapsList.push(this); // Use whatever global scope variable you like.
    this.on('init', function (evt) {
        if (window.location.pathname !== "/clients/spatial/") {
            L.gridLayer.googleMutant({
                type: 'hybrid' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
            }).addTo(this);
        }
        this.panTo(this.getCenter());
        this._onResize();
        this.invalidateSize();
        this.locate({
            watch: true,
            enableHighAccuracy: true
        }) /* This will return map so you can do chaining */
            .on('locationfound', function (e) {
                if (typeof (location_marker) !== "object") {
                    location_marker = L.circleMarker([e.latitude, e.longitude]).bindPopup(
                        "<div><a id='geonote' href='#' lat='{0}' lon='{1}' onclick='make_geonote_div()'>Make GeoNote</a></div>".formatUnicorn(e.latitude, e.longitude));
                    this.addLayer(location_marker);
                } else {
                    location_marker.setLatLng(L.latLng(e.latitude, e.longitude));
                    location_marker.redraw();
                }
                try {
                    add_accurate_sample_point(this);
                } catch (e) {}
            })
            .on('locationerror', function (e) {
                console.log(e);
                // alert("Location access denied.");
            });
    });
    var zoomtoggle = L.control({position: "topleft"});
    zoomtoggle.onAdd = function (map) {
        var div = L.DomUtil.create('div');
        L.DomUtil.addClass(div, "leaflet-bar");
        div.innerHTML = `
    <a class="leaflet-control-layers leaflet-control-layers-scrollzoom leaflet-bar-part" style="outline: none;"
        href="#" attr="off" title="Click to toggle scroll wheel zoom"><i class="fa fa-binoculars"></i></a>`;
        return div;
    };
    this.addControl(zoomtoggle);
    this.addControl(new L.Control.Fullscreen());
    this.on("click", (evt) => {
        try {
            get_trs(evt.latlng.lat, evt.latlng.lng);
        } catch (e) {
        }
    })
    this.scrollWheelZoom.disable();
    var a = this;
    setTimeout(function () {
        a.fireEvent("init");
        $(".leaflet-control-layers-scrollzoom").on("click", (evt) => {
            evt.preventDefault();
            let state = $(evt.target).attr("scroll");
            if (state === "on") {
                $(evt.target).attr("scroll", "off");
                alert("Scroll wheel zoom is now off");
                a.scrollWheelZoom.disable();
            } else {
                $(evt.target).attr("scroll", "on");
                a.scrollWheelZoom.enable();
                alert("Scroll wheel zoom is turned on");
            }
        });
        rerender_leaflet_markers();
    }, 2000);
});

(function ($) {
    $(document).ready(function () {
        $('.add-another').click(function (e) {
            e.preventDefault();
            showAddAnotherPopup(this);
        });
        $('.related-lookup').click(function (e) {
            e.preventDefault();
            showRelatedObjectLookupPopup(this);
        });
        // Added from GoogleMutant API
    });
})(jQuery);

$(document).ready(function () {

});

function zoomTo() {
    var lat = document.getElementById("id_latitude").value;
    var lng = document.getElementById("id_longitude").value;
    mapsList[0].panTo(new L.LatLng(deg_to_dec(lat), deg_to_dec(lng)));
}


function deg_to_dec(string) {
    if (string.includes(",")) {
        var arr = string.split(",");
        var conv = [1, 60, 3600];
        var val = 0;
        arr.forEach(function (currentValue, index) {
            val = val + currentValue / conv[index];
        });
        return val;
    } else {
        return string;
    }
}

function address() {
    $.get("address/",
        function (data) {
            $("#id_notes").append("\nAttempted to look for address based on location. \n");
            data['address'].forEach(function (i, index) {
                i = i + "\n"
                $("#id_notes").append(i);
                if (i.includes("Township")) {
                    $("#id_township").val(i.split(",")[0].replace(" Township", ""));
                } else if (i.includes("County")) {
                    $("#id_county").val(i.split(',')[0].replace(" County", ''));
                } else if (index === data['address'].length - 2) {
                    $("#id_state").val(i.split(',')[0]);
                } else if (index === 0) {
                    // May be wrong:
                }
            });
        });
}


