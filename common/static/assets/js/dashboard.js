/*=========================================================================================
[Dashboard Javascript]

Project	     : GMSLab 

Only Use For Dashboard (index.html) Page.

==========================================================================================*/


(function ($) {
    "use strict";
    jQuery(document).ready(function ($) {
    });
}(jQuery));

String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
    function () {
        "use strict";
        var str = this.toString();
        if (arguments.length) {
            var t = typeof arguments[0];
            var key;
            var args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function setupAjax() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", $("#id_labcorecsrf").val());
            }
        }
    });
}

setupAjax();


$("#dismiss-notifications").click(function () {
    $("#notification_messages").html("0");
    $('#message-list').html("");
});


var chatconnect = function (roomname) {
    chatSocket = new WebSocket(
        (location.protocol == 'http:' ? 'ws://' : 'wss://') +
        window.location.host + '/chat/ws/' + roomname + "/"
    );
    chatSocket.onmessage = function (e) {
        var data = JSON.parse(e.data);
        if (data.type === "async_include") {
            var divid = $(data.container);
            console.log(data);
            divid.append(data.html);
        } else {
            var message = data.message;
            var msg = $("#notification_messages").html();
            $("#notification_messages").html(parseInt(msg) + 1);

            var item = '<li><a href="#" class="single-message">\
                        <div class="message-img">\
                            <img src={0} alt="message" />\
                            <span class="online-message"></span>\
                        </div>\
                        <div class="message-txt">\
                            <h4>SoilDx LabCore</h4>\
                                <p>{1}</p>\
                        </div>\
	                    </a>\
                    </li>'.formatUnicorn($("#id_labcoreicon").val(), message);
            $('#message-list').append(item);
        }
    };
    chatSocket.onclose = function (e) {
        console.log("chat socket closed unexpectedly; Will reconnect in 3s");
        setTimeout(function () {
            chatconnect(roomname);
        }, 3000);
    };
    chatSocket.onopen = function (e) {
        $.each(message_queue, function (idx, val) {
            console.log("Sending" + val);
            chatSocket.send(val);
        })
    }
};


var printlinks = function () {
    $.each($("a[title='Print Labels']"), function (idx, val) {
        $(val).click(function (e) {
            e.preventDefault();
            var action = $(e.target).parent('a').prop('href');
            $("p#order").html($(e.target).prop('id'));
            $("#modal-save").click(function (e) {
                e.preventDefault();
                window.location.href = "{0}?samples={1}&start={2}&format=pdf".formatUnicorn(
                    action, $("#num_samples").val(), $("#start").val()
                )
            });
            $("#myModal").modal('show');
        });
    });
    $.each($("a.order-status"), function (idx, val) {
        $(val).click(function (e) {
            e.preventDefault();
            var action = $(e.target).parent('a').prop('href');
            $.ajax(action, {
                type: "POST",
                success: function (data) {
                    if (data.status == "success") {
                        var table = $($(e.target).parents('table')).DataTable();
                        table.ajax.reload(null, false);
                    }
                }
            });
        });
    });
    $.each($("a.scheduleit"), function (idx, val) {
        $(val).click(function (event) {
            event.preventDefault();
            schedule_div(event).modal('show');
        });
    });
};
