// $(function() {
//   var previewNode = document.querySelector("#template");
//   previewNode.id = "";
//   var previewTemplate = previewNode.parentNode.innerHTML;
//   previewNode.parentNode.removeChild(previewNode);
//
//   var myDropzone = new Dropzone(document.querySelector("#container-dropzone") , {
//     url: "fileupload/", //url to make the request to.
//     thumbnailWidth: 80,
//     thumbnailHeight: 80,
//     parallelUploads: 20,
//     previewTemplate: previewTemplate,
//     autoQueue: false,
//     previewsContainer: "#previews",
//     clickable: ".file-input-button",
//     headers: { // Tried to apply the token this way but no success.
//       'X-CSRFToken': csrftoken
//     }
//   });
//
//   myDropzone.on("addedfile", function(file){
//     file.previewElement.querySelector(".start").onclick = function(){
//       myDropzone.enqueueFile(file);
//       };
//   });
//
//   myDropzone.on("totaluploadprogress", function(progress){
//     document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
//   });
//
//   myDropzone.on("sending", function(file){
//     // Show total progress on start and disable START button.
//     document.querySelector("#total-progress").style.opacity = "1";
//     file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
//   });
//
//   // Hide progress bar when complete.
//   myDropzone.on("queuecomplete", function(progress){
//     document.querySelector("#total-progress").style.opacity = "0";
//   });
//
//   // Setup buttons for every file.
//   document.querySelector("#actions .start").onclick = function(){
//     myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
//   };
//   document.querySelector("#actions .cancel").onclick = function(){
//     myDropzone.removeAllFiles(true);
//   };
// });

$(function() {
  var myDropZone = new Dropzone("div#container-dropzone", {
    url: "fileupload/",
    headers: { // Tried to apply the token this way but no success.
      'X-CSRFToken': csrftoken
    }
  });
});
