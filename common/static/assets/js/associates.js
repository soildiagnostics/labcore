$(document).ready(function () {
    $("input:checkbox").click(function (e) {
        var id = $(e.target).prop("id");
        id = id.split("_")[1];
        var par = $(e.target).prop("name");
        $.post("change/" + par + "/" + id + "/",
            function (data) {
                $("#result_" + data.pk).html("Permissions changed");
                setTimeout(function () {
                    $("#result_" + data.pk).html("");
                }, 1000);
            }
        )
    });
});