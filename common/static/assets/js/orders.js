document.addEventListener("DOMContentLoaded", function () {
    var oTable = $('#ordertable').DataTable({
        order: [[5, "desc"]],
        dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        paging: true,
        stateSave: true,
        pagingType: "numbers",
        responsive: true,
        buttons: [
            {
                extend: 'copy',
                text: 'Copy to Clipboard'
            },
            {
                extend: 'csv',
                text: 'Download CSV'
            }
        ],
        drawCallback: printlinks,
        columnDefs: [
            {
                name: "order_number",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "order_status",
                orderable: true,
                searchable: true,
                targets: [1]
            },
            {
                name: "client",
                orderable: true,
                searchable: true,
                targets: [2]
            },
            {
                name: "farm",
                orderable: true,
                searchable: true,
                targets: [3]
            },
            {
                name: "field",
                orderable: true,
                searchable: true,
                targets: [4]
            }, {
                name: "date_created",
                orderable: true,
                searchable: true,
                targets: [5],
                render: $.fn.dataTable.render.moment(moment.ISO_8601, 'MM/DD/YYYY')
            },
            {
                name: "date_modified",
                orderable: true,
                searchable: true,
                targets: [6],
                render: $.fn.dataTable.render.moment(moment.ISO_8601, 'MM/DD/YYYY')
            },
            {
                name: "products",
                orderable: false,
                searchable: false,
                targets: [7]
            }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": actionurl
    });
    reset_datetime_picker();
});

reset_datetime_picker = function () {
    $("#datetimepicker1").daterangepicker({
        timePicker: true,
        autoApply: true,
        startDate: moment().startOf('hour').add(24, 'hour'),
        endDate: moment().startOf('hour').add(25, 'hour'),
        locale: {
            format: 'M/DD/YYYY hh:mm A'
        }
    });
}

reset_schedule_modal = function() {
    reset_datetime_picker();
    $("#done").prop("checked", true);
    $("#task").val("");
}

schedule_div = function (event) {
    reset_schedule_modal();
    var field_id = $($(event.target).parent('a')[0]).attr('field_id');
    var field_name = $($(event.target).parent('a')[0]).attr('field_name');
    var product_id = $($(event.target).parent('a')[0]).attr('product_id');
    var product_name = $($(event.target).parent('a')[0]).attr('product_name');
    var order_number = $($(event.target).parent('a')[0]).attr('order_number');

    var title = "Mark {0} as Done on Field {1} for Order {2}?".formatUnicorn(product_name, field_name, order_number);
    var url = "/fieldevents/api/v1/field/{0}/events/".formatUnicorn(field_id);

    $("#schdmodal-title").text(title);
    $("#schdmodal-save").click(function () {
        $("#schdmodal-save").off("click");
        modal_post(url, field_id, "{0} on Order {1}".formatUnicorn(product_name, order_number));
    });

    $("#done").on("click", function () {
        var done = $("#done").prop("checked");
        var title;
        if (done) {
            title = "Mark {0} as Done on Field {1} for Order {2}?".formatUnicorn(product_name, field_name, order_number);
        } else {
            title = "Schedule {0} Field {1} for Order {2}?".formatUnicorn(product_name, field_name, order_number);
        }
        $("#schdmodal-title").text(title);
    });

    $("#schdmodal-close").click(function () {
        $("#schdmodal-save").off("click");
    });
    return $("#scheduleModal");
};

function modal_post(url, field, title) {
    var time = $("#datetimepicker1").val();
    var task = $("#task").val();
    var start = moment(time.split(" - ")[0], 'M/DD/YYYY hh:mm A');
    var end = moment(time.split(" - ")[1], 'M/DD/YYYY hh:mm A');
    var done = $("#done").prop("checked");
    if (done) {
        task = "Completed: " + task;
    }

    event_data = {
        id: '',
        allDay: false,
        editable: !done,
        start: start.toISOString(),
        end: end.toISOString(),
        title: title,
        extendedProps: {
            event_type: "Task",
            field_id: field,
            custom_fields: [],
            task: task
        },
    };

    console.log(event_data);

    $.ajax({
        type: 'POST',
        url: url,
        contentType: "application/json",
        data: JSON.stringify(event_data),
        dataType: 'json',
        success: function (data) {
            $("#scheduleModal").modal('hide');
        },
        error: function (data) {
            console.log(data);
        }
    });
}