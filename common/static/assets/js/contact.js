$(document).ready(function () {
    var ph = $("#id_phone-TOTAL_FORMS").val();
    for (i = 0; i < ph; i++) {
        $("input#id_phone-" + i + "-phone_number").mask("999-999-9999");
    }
    $("#div_id_is_company").hide();
    if ($("#id_client_type").val() == "C") {
        $("#id_name").prop("required", false);
    }
    $("label[for='id_name']").html("First name");
    $("#id_client_type").change(function () {
        if ($("#id_client_type").val() == "C") {
            $("#id_company_name").prop("required", true);
            $("label[for='id_company_name']").html("Company name (required)");
            $("#id_name").prop("required", false);
            $("label[for='id_name']").html("First name");
            $("#id_is_company").prop("checked", true);
        } else {
            $("#id_is_company").prop("checked", false);
            $("#id_company_name").prop("required", false);
            $("label[for='id_company_name']").html("Company name");
            $("#id_name").prop("required", true);
            $("label[for='id_name']").html("First name (required)");
        }

    })
});



