from django.conf import settings
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import TemplateView, RedirectView

from associates.models import OrganizationUser
from associates.role_privileges import DealershipRequiredMixin
from clients.models import Client, Farm, Field, HallPass
from django.contrib.auth.decorators import user_passes_test

from contact.models import User
from django.test import Client as TestClient, RequestFactory
from django.urls import reverse

## Views to Test
from contact.views import PersonUpdateView
from associates.views import TeamMembers
from clients.views import ClientUpdate, ClientDetail, FarmDetail, FieldDetail
from orders.models import Order
from orders.views import OrderDetail, CreateOrder, UpdateOrder, WorkOrderDetail


class DashboardView(DealershipRequiredMixin, TemplateView):
    template_name = "dashboard/dashboard.html"
    client_has_permission = True

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['org'] = self.org
        context['client_count'] = Client.objects.filter(originator__organization=self.org).count()
        # TODO wire up additional dashboard context items
        return context


def superuser_required():
    def wrapper(wrapped):
        class WrappedClass(UserPassesTestMixin, wrapped):
            def test_func(self):
                return self.request.user.is_superuser

        return WrappedClass

    return wrapper


@superuser_required()
class PermissionsView(TemplateView):
    template_name = "dashboard/permissions.html"

    def get_context_data(self, pk, **kwargs):
        context = super().get_context_data(**kwargs)
        user = User.objects.get(pk=pk)
        context['object'] = user
        client_list = list()
        ## create a viewlist with the following terms:
        ## ("view_name", ViewClass, url_kwargs, "Friendly Name")
        viewList = [
            ('dashboard', DashboardView, dict(), "View Dashboard"),
        ]
        try:
            context['organizationuser'] = OrganizationUser.objects.get(user=user)
            created_orders = Order.objects.filter(created_by=context['organizationuser'])
            if created_orders.count():
                viewList.extend(
                    [('order_detail', OrderDetail, {"pk": created_orders.first().pk}, "Create Orders as Dealer"),
                     ]
                )
        except Exception as e:
            context['organizationuser'] = repr(e)
        try:
            context['orgclientfield'] = Field.objects.filter(
                farm__client__originator__organization=context['organizationuser'].organization).first()
            viewList.extend(
                [('field_detail', FieldDetail, {"pk": context['orgclientfield'].pk}, "View Client's Field"),
                 ('new_order', CreateOrder, {"pk": context['orgclientfield'].pk}, "Create Order on Client's Fields")
                 ]
            )
        except Exception as e:
            context['orgclientfield'] = repr(e)

        try:
            context['clientrec'] = user.person.client
            client_list.append(user.person.client)
            viewList.append(('client_detail',
                             ClientDetail,
                             {"pk": user.person.client.pk},
                             "View Client's Company Update Page"))
        except Exception as e:
            context['clientrec'] = repr(e)
        try:
            context['company_client'] = user.person.company.client
            client_list.append(user.person.company.client)
        except Exception as e:
            context['company_client'] = repr(e)

        if client_list:
            try:
                context['farms'] = Farm.objects.filter(client__in=client_list)
                viewList.append(('farm_detail', FarmDetail, {'pk': context['farms'].first().pk}, "View Client's Farm"))
            except Exception as e:
                context['farms'] = repr(e)
            try:
                fields = Field.objects.filter(farm__client__in=client_list)[0:3]
                for field in fields:
                    viewList.append(('field_detail', FieldDetail, {'pk': field.pk}, "View Client's Field"))
                    viewList.append(("new_order", CreateOrder, {'pk': field.pk}, "New Order on Own Field"))
            except Exception as e:
                context['fields'] = repr(e)

            try:
                context['orders'] = Order.objects.filter(field__farm__client__in=client_list).first()
                viewList.extend(
                    [('order_detail', OrderDetail, {"pk": context['orders'].pk}, "Client's Order"),
                     ('work_order_detail', WorkOrderDetail, {"pk": context['orders'].pk}, "Client's WorkOrder"),
                     ]
                )
            except Exception as e:
                context['orders'] = repr(e)

        context['hallpasses'] = HallPass.objects.active_passes(user)
        if context['hallpasses']:
            for hp in context["hallpasses"].all():
                viewList.append(('field_detail', FieldDetail, {'pk': hp.field.pk}, "Hallpass Field"))
                viewList.append(("new_order", CreateOrder, {'pk': hp.field.pk}, "New Order on Hallpass field"))
                try:
                    hporders = Order.objects.filter(field__in=[hp.field for hp in context['hallpasses']])
                    context['hallpassOrder']= hporders.values_list("order_number", flat=True)
                    viewList.extend(
                        [('order_detail', OrderDetail, {"pk": hporders.first().pk}, "HallPass Field Order"),
                         ('work_order_detail', WorkOrderDetail, {"pk": hporders.first().pk}, "HallPass Field WorkOrder"),
                         ]
                    )
                except Exception as e:
                    context['hallpassOrder'] = repr(e)
        try:
            context['draftorders'] = Order.objects.filter(draftorder__user=user)
        except Exception as e:
            context['draftorders'] = None
            print(e)
        if context['draftorders']:
            draftorder = context['draftorders'].first()

            viewList.extend(
                [('order_detail', OrderDetail, {"pk": draftorder.pk}, "Draft Order"),
                 ('work_order_detail', WorkOrderDetail, {"pk": draftorder.pk}, "Draft Order WorkOrder")
                 ])

        tc = TestClient()
        tc.force_login(user)
        rqf = RequestFactory()

        context['view_permissions'] = dict()

        for url, view, kwargs, note in viewList:
            fullurl = reverse(url, kwargs=kwargs)
            rq = rqf.get(reverse(url, kwargs=kwargs), SERVER_NAME=settings.DOMAIN.get('domain'))
            rq.user = user
            try:
                response = view.as_view()(rq, **kwargs)
                context['view_permissions'][f"{note} {fullurl}"] = response.status_code
            except Exception as e:
                context['view_permissions'][f"{note} {fullurl}"] = repr(e)

        return context


class CheckPermissions(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        usr = User.objects.get(username=kwargs.get("username"))
        return reverse("permissions", kwargs={"pk": usr.pk})
