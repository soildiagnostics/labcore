from django.db.models import Q

class SearchQueryAssemblyMixin(object):
    """
    Mixin to build search queries for views that work with datatables
    Common queries are used in an OR fashion as is, whereas
    __icontains or __trigram_similar queries are constructed for
    terms in the search_fields list.
    Look at the ClientsListJSON view for usage.
    """
    common_queries = list()
    search_fields = list()

    def get_kw(self, term, key):
        """
        :param term: Something like contact__name
        :param key: search key
        :return: {'contact__name__icontains': key}
        """
        if len(key) <= 6:
            return {term+'__icontains': key}
        return {term+"__trigram_similar": key}

    def get_q_object(self, term, key):
        return Q(**self.get_kw(term, key))

    def build_query(self, search):
        searchkeys = search.split(' ')
        searchkeys = [s for s in searchkeys if s != '']
        all_q_params = Q()
        for key in searchkeys:
            q_params = Q()
            for q in self.common_queries:
                q_params |= Q(**{q: key})
            for term in self.search_fields:
                q_params |= self.get_q_object(term, key)
            all_q_params &= q_params
        return all_q_params

    def filter_queryset(self, qs):
        search = self.request.GET.get('search[value]', '')
        if search:
            qs = qs.filter(self.build_query(search)).distinct()
        return qs
