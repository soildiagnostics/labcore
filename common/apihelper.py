from django.conf import settings
from django.contrib.auth.models import Group
from django.urls import path
from rest_framework.permissions import BasePermission


def apipath(pathname, view, name):
    pathname = "api/{}/{}".format(settings.SOILDX_API_VERSION, pathname)
    return path(pathname, view, name=name)

def current_site_url():
    """Returns fully qualified URL (no trailing slash) for the current site."""
    #from:  https://fragmentsofcode.wordpress.com/2009/02/24/django-fully-qualified-url/

    from django.contrib.sites.models import Site
    Site.objects.clear_cache()
    current_site = Site.objects.get_current()
    protocol = getattr(settings, 'SITE_PROTOCOL', 'http')
    port     = getattr(settings, 'SITE_PORT', '')
    url = '%s://%s' % (protocol, current_site.domain)
    if port:
        url += ':%s' % port
    return url



# Create your views here.


class APIGroupPermission(BasePermission):
    """
    Set up a generic group permission system.
    A group wishing to have API access must have this permission in place.
    """

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        try:
            return Group.objects.get(name=self._group).user_set.filter(id=request.user.id).exists()
        except Group.DoesNotExist:
            return None


def api_group_permission_classfactory(name, group):
    newclass = type(name, (APIGroupPermission,),{"_group": group})
    return newclass

