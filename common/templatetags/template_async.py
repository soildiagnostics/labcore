from django import template
from django.urls import resolve

register = template.Library()

@register.inclusion_tag("common/async_include.html", takes_context=True)
def async_include(context, include_fragment, selector):
    context['fragment'] = include_fragment
    context['container_selector'] = selector
    return context

def process_fragment(packet):

    print(packet)
    matched_view = resolve(packet["location"])
    # mymodule = myfunc.__module__

    packet["html"] = "Here is some data"
    return packet
