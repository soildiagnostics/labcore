#!/bin/bash
#python3 manage.py makemigrations --dry-run
python3 manage.py migrate
python3 manage.py collectstatic --no-input
python3 manage.py clearsessions
#echo vm.overcommit_memory = 1 >> /etc/sysctl.conf
#echo never > /sys/kernel/mm/transparent_hugepage/enabled
redis-server &
python3 manage.py runworker default process-uploaded-shapefile &
daphne -b 0.0.0.0 -p 8000 labcore.asgi:application