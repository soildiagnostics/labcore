"""
Django settings for labcore project.

Generated by 'django-admin startproject' using Django 2.0.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

from django.contrib.messages import constants as messages

from labcore.base import *

DEBUG = True
# DEBUG_TOOLBAR_PANELS = [
#     'debug_toolbar.panels.versions.VersionsPanel',
#     'debug_toolbar.panels.timer.TimerPanel',
#     'debug_toolbar.panels.settings.SettingsPanel',
#     'debug_toolbar.panels.headers.HeadersPanel',
#     'debug_toolbar.panels.request.RequestPanel',
#     'debug_toolbar.panels.sql.SQLPanel',
#     'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#     'debug_toolbar.panels.templates.TemplatesPanel',
#     'debug_toolbar.panels.cache.CachePanel',
#     'debug_toolbar.panels.signals.SignalsPanel',
#     'debug_toolbar.panels.logging.LoggingPanel',
#     'debug_toolbar.panels.redirects.RedirectsPanel',
# ]
#
# DEBUG_TOOLBAR_PANELS += [
#     'debug_toolbar.panels.timer.TimerDebugPanel',
#     #'pympler.panels.MemoryPanel',
#     ]
WSGI_APPLICATION = 'labcore.wsgi.application'
ASGI_APPLICATION = "labcore.routing.application"

SECRET_KEY = 'm*3)_3(gr%@^ly5-@$a^xapi#j8)e-@ui_&reg^^h#eq@f07y+'

INSTALLED_APPS += [
    # 'pympler',
    'frontpage',
]

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'gms_postmess',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/Chicago'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Grappelli settings
LABCORE_ADMIN_TITLE = 'SoilDx LabCore Admin'
if 'DEVELOPMENT' in os.environ:
    LABCORE_ADMIN_TITLE = LABCORE_ADMIN_TITLE + ' [[DEVELOPMENT VERSION]]'

# Django-registration settings
ACCOUNT_ACTIVATION_DAYS = 7
DEFAULT_FROM_EMAIL = 'support@soildiagnostics.com'
EMAIL_HOST_USER = "admin@soildiagnostics.com"
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django_ses.SESBackend'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

STATICFILES_DIRS = [
    # os.path.join(BASE_DIR, "staticfiles"),
    os.path.join(BASE_DIR, "media"),
]

# Mapbox and Leaflet config

# MAPBOX_KEY = 'pk.eyJ1Ijoic29pbGR4aW5jIiwiYSI6ImNpajkzN2lxODAwMjF1Ym00dmxwb2owZXMifQ.vg7xdzuENvYJw1ETKbexYw'


# LOGIN_URL = 'login'
# LOGOUT_URL = 'logout'
# LOGIN_REDIRECT_URL = 'dashboard'
# LOGOUT_REDIRECT_URL = 'home'
#

DOMAIN = {
    "domain": "www.soildiagnostics.com",
    "name": "soildiagnostics.com"
}

# INTERNAL_IPS = ('127.0.0.1',)
# if the Django toolbar isn't visible, try to look for the gateway ip
# docker inspect web_container_id | grep -e '"Gateway"'
# "Gateway": "172.18.0.1",


# CORS_ORIGIN_WHITELIST = (
#     'elasticbeanstalk.com',
#     'localhost:8000',
# )


# RQ_QUEUES = {
#     'default': {
#         'HOST': 'localhost',
#         'PORT': '6379',
#         # If you're
#         'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
#         'DB': 0,
#         'DEFAULT_TIMEOUT': 480,
#     },
#     'high': {
#         'HOST': 'localhost',
#         'PORT': '6379',
#         # If you're
#         'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
#         'DB': 0,
#         'DEFAULT_TIMEOUT': 480,
#     },
#     'low': {
#         'HOST': 'localhost',
#         'PORT': '6379',
#         # If you're
#         'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'),
#         'DB': 0,
#         'DEFAULT_TIMEOUT': 480,
#     }
# }

#
# CRISPY_TEMPLATE_PACK = 'bootstrap3'
# CRISPY_FAIL_SILENTLY = not DEBUG

# MESSAGE_TAGS = {
#     messages.DEBUG: 'alert-info',
#     messages.INFO: 'alert-info',
#     messages.SUCCESS: 'alert-success',
#     messages.WARNING: 'alert-warning',
#     messages.ERROR: 'alert-danger',
# }

GOOGLE_API_KEY = "AIzaSyBKLhTqk1vvsnovGTaeJ8mpGaPB5ZX3wfA"
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '861557868086-qmlg8a1mr9rc56eh6nud8fkiq9u89pti.apps.googleusercontent.com'  # Paste CLient Key
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'tp8-lpknboZkXgbpFoYY9oPt'  # Paste Secret Key

# django-money settings
# Do not use CURRENCIES setting
# it affects the choices on a Money model field (products.Price.amount)
# and migrations are generated by differences 
# between dependent project and labcore
# CURRENCIES = ('USD', 'EUR', 'INR')
ALLOWED_CURRENCIES = ('USD', 'EUR')
DEFAULT_CURRENCY = 'USD'

## Default Lab Processor in orders App

DEFAULT_LAB_PROCESSOR = "Soil Diagnostics, Inc"

SOILDX_SSURGO_SOILS_ENABLED = True

# GDAL_LIBRARY_PATH = '/usr/local/lib/libgdal.so'
CLIENT_CAN_VIEW_PRODUCTS = True
CLIENT_CAN_PLACE_ORDERS = True
