import datetime

from django.contrib import admin
from django.contrib import messages
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.html import format_html
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from associates.models import OrganizationUser, Organization
from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin
from formulas.models import Formula
from orders.soildx_helper import get_soildx_rec, create_lab_measurements
from products.models import TestParameter, Product
from rangefilter.filter import DateRangeFilter
from samples.models import File, LabMeasurement, SampleRegistry
from .models import OrderStatus, Order, OrderSummary, SamplerSummary, OrderStatusType
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter, RelatedOnlyDropdownFilter
from .soilmap_helper import make_shapefile, apply_formulas, duplicate_layer, apply_formulas_gis


# admin.site.register(OrderStatus)

@admin.register(OrderStatus)
class OrderStatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'verbose_name', 'status_type',
                    'preceded_by', 'succeeded_by',
                    'alert', 'allow_edits', 'include_on_dashboard',
                    'allow_third_party_view',)
    list_editable = ('verbose_name', 'status_type',
                     'preceded_by', 'succeeded_by',
                     'alert', 'allow_edits', 'include_on_dashboard',
                     'allow_third_party_view',)
    search_fields = ('name', "verbose_name")


class RelatedFileFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('File')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'file'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        end_date = datetime.datetime.utcnow()
        start_date = end_date - datetime.timedelta(days=365)

        return tuple(list(File.objects.filter(created__range=(start_date, end_date),
                                              status__in=['O', 'C'],
                                              basket__samples__isnull=False).distinct(
            'pk').order_by('-pk').values_list(
            'pk', 'name')))

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() is None:
            return queryset.all()
        return queryset.filter(samples__basket__file__pk=self.value()).distinct()


@admin.register(Order)
class OrderAdmin(FilteredQuerySetMixin, DealershipRequiredMixin, admin.ModelAdmin):
    model = Order
    list_display = ('order_number', 'report', 'get_products', 'order_status', 'sampler',
                    'check_created_by', 'date_created',
                    'field_name')
    # list_select_related = True
    search_fields = ('order_number', 'field__name', 'created_by__user__person__name',
                     'created_by__user__person__last_name', 'products__name',
                     'samples__basket__file__name')
    list_editable = ('sampler', "order_status")
    list_per_page = 25
    list_filter = (('order_status__status_type', RelatedOnlyDropdownFilter),
                   ('order_status', RelatedOnlyDropdownFilter),
                   ("sampler", RelatedOnlyDropdownFilter),
                   RelatedFileFilter,
                   ("products__category", RelatedOnlyDropdownFilter),
                   ("products", RelatedOnlyDropdownFilter))
    date_hierarchy = ('date_created')
    raw_id_fields = ('created_by', 'field', 'parent_order',)
    # autocomplete_fields = ("order_status")
    instance_owner_organization = "created_by__organization"

    exclude = ('content_type', 'object_id')

    def check_created_by(self, obj):
        try:
            client_originator = obj.field.farm.client.originator
            if client_originator.organization != obj.created_by.organization:
                return format_html(
                    f"<span style='background-color: lightcoral;'>Dealer: {client_originator}<br>Order by: {obj.created_by}</span>")
            return obj.created_by
        except AttributeError:
            return format_html(
                f"<span style='background-color: lightcoral;'>Dealer: {client_originator}<br>Order created by Client</span>")

    def report(self, obj):
        return format_html("<a href='/orders/{order}/?format=pdf&cached=1&download=1'>Get</a>", order=obj.pk)

    actions = ['mark_not_ready', 'mark_ready', 'mark_hold', 'mark_done', 'mark_canceled',
               'mark_published',
               'set_order_created',
               'generate_blank_labmeasurements',
               'pull_data_from_soildx',
               'make_layers_from_soildata']

    def mark_done(self, request, queryset):
        status = OrderStatus.objects.get(name="Done")
        queryset.update(order_status=status)

    mark_done.short_description = "Mark selected orders as Completed"

    def mark_published(self, request, queryset):
        status = OrderStatus.objects.get(name="published")
        queryset.update(order_status=status)

    mark_published.short_description = "Mark selected orders as Published"

    def mark_hold(self, request, queryset):
        status, _ = OrderStatus.objects.get_or_create(name="Hold")
        queryset.update(order_status=status)

    mark_hold.short_description = "Mark selected orders as on Hold"

    def mark_canceled(self, request, queryset):
        status = OrderStatus.objects.get(name="Cancel")
        queryset.update(order_status=status)

    mark_canceled.short_description = "Mark selected orders as Canceled"

    def mark_ready(self, request, queryset):
        status = OrderStatus.objects.get(name__istartswith="field ready")
        queryset.update(order_status=status)

    mark_ready.short_description = "Mark selected orders as Ready to Sample"

    def mark_not_ready(self, request, queryset):
        status = OrderStatus.objects.get(name__istartswith="field not ready")
        queryset.update(order_status=status)

    mark_not_ready.short_description = "Mark selected orders as NOT Ready to Sample"

    def set_order_created(self, request, queryset):
        for order in queryset:
            order.created_by = order.field.farm.client.originator
            order.save()
            messages.info(request, f"Order {order.order_number} originator set to {order.created_by}")

    set_order_created.short_description = "Set order creator to client originator"

    def generate_blank_labmeasurements(self, request, queryset):
        orguser = OrganizationUser.objects.get(user=request.user)
        for obj in queryset:
            try:
                assert obj.num_samples > 0, "No samples registered!"
            except AssertionError as e:
                messages.error(request, e)
            else:
                samples = obj.samples.all()
                products = obj.products.all()
                parameters = TestParameter.objects.none()
                for prod in products:
                    if prod.is_package:
                        for p in prod.package.all():
                            parameters |= p.test_parameters.filter(calculated=False)
                    else:
                        parameters |= prod.test_parameters.filter(calculated=False)
                lm = 0
                for s in samples:
                    for par in parameters:
                        if par and LabMeasurement.objects.filter(sample=s, parameter=par).count() == 0:
                            labm = LabMeasurement(sample=s,
                                                  parameter=par,
                                                  value=-1)
                            labm.save(orguser=orguser)
                            lm = lm + 1
                messages.info(request, f"Order {obj}: Created {lm} samples with value -1 for {samples.count()} "
                                       f"samples and {len(parameters)} parameters")

    generate_blank_labmeasurements.short_description = "Generate placeholder lab measurements for all measured parameters, " \
                                                       "when none exist"

    def pull_data_from_soildx(self, request, queryset):
        orguser = OrganizationUser.objects.get(user=request.user)
        for order in queryset:
            rec = get_soildx_rec(order.order_number)
            if not rec:
                messages.info(request, f"Order {order} has no data")
            else:
                try:
                    create_lab_measurements(order, rec, orguser)
                    messages.info(request, f"Success for {order}")
                except Exception as e:
                    messages.info(request, f"Failed for {order}, {e}")

    pull_data_from_soildx.short_description = "Pull data and recs from SoilDx.com"

    def make_layers_from_soildata(self, request, queryset):

        # All requests here will actually be of type POST
        # so we will need to check for our special key 'apply'
        # rather than the actual request type
        if 'apply' in request.POST:
            # The user clicked submit on the intermediate form.
            # Perform our update action:
            orders = request.POST.getlist('_selected_action')
            for order in orders:
                sfid = request.POST.get(order)
                newsf = make_shapefile(order, sfid, request)
                if request.POST.get(f"create_rec_{order}") == 'on':
                    # Create a new layer with recommendation
                    name = request.POST.get(f'name_rec_{order}', f"Draft Recommendation {order}")
                    dup = duplicate_layer(request, newsf, name)

                    # Create Yield feature
                    if request.POST.get(f'yld_{order}') == 'yld_fixed':
                        val = float(request.POST.get(f'yld_value_{order}'))
                        yld_vector = [val]*dup.features
                        dup.add_feature('Yield', yld_vector)

                    formulas = Formula.objects.filter(id__in=request.POST.getlist(f'formula_{order}'))
                    apply_formulas_gis(dup, formulas, request)
            return HttpResponseRedirect(request.get_full_path())

        orders = list()
        for obj in queryset:
            measurements = LabMeasurement.objects.filter(sample__order=obj, void=False)
            pars = TestParameter.objects.filter(id__in=measurements.values_list('parameter', flat=True))
            obj.all_parameters = [p.name for p in pars]
            orders.append(obj)
        context = {'orders': orders,
                   'formulas': Formula.objects.all()}
        return render(request,
                      'admin/soildata_intermediate.html',
                      context=context)

    make_layers_from_soildata.short_description = "Make GIS layers and recommendations from soil data"

    # def apply_formula(self, request, queryset):
    #
    #     # All requests here will actually be of type POST
    #     # so we will need to check for our special key 'apply'
    #     # rather than the actual request type
    #     if 'apply' in request.POST:
    #         # The user clicked submit on the intermediate form.
    #         # Perform our update action:
    #         orders = request.POST.getlist('_selected_action')
    #         for order in orders:
    #             formulas = request.POST.getlist(order)
    #             apply_formulas(order, formulas, request)
    #         # Redirect to our admin view after our update has
    #         # completed with a nice little info message saying
    #         # our models have been updated:
    #         return HttpResponseRedirect(request.get_full_path())
    #
    #     orders = list()
    #     for obj in queryset:
    #         measurements = LabMeasurement.objects.filter(sample__order=obj, void=False)
    #         pars = TestParameter.objects.filter(id__in=measurements.values_list('parameter', flat=True))
    #         obj.all_parameters = [p.name for p in pars]
    #         orders.append(obj)
    #     context = {
    #         'orders': orders,
    #         'formulas': Formula.objects.all()
    #     }
    #     return render(request,
    #                   'admin/formula_intermediate.html',
    #                   context=context)
    #
    # apply_formula.short_description = "Apply Recommendation Formulas"

    def get_queryset(self, request):
        self.request = request
        qs = self.get_initial_queryset()
        self._samplers = OrganizationUser.objects.filter(organization__roles__role="Sampler",
                                                         user__is_active=True).prefetch_related("user", "organization")
        return qs.prefetch_related('field', 'created_by', 'created_by__user__person',
                                   'created_by__organization__company', "sampler", "sampler__organization",
                                   "sampler__user", 'order_status', 'products', 'created_by__user')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "sampler":
            kwargs["queryset"] = self._samplers
        return super(OrderAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


## DEPRECATED
# @admin.register(ReportFile)
# class ReportFileAdmin(admin.ModelAdmin):
#     model = ReportFile
#     list_display = ('order', 'created', 'file')


### Analytical summaries
class QuarterFilter(admin.SimpleListFilter):
    title = _("Date ranges")
    parameter_name = "range"

    def lookups(self, request, model_admin):
        """
        Provides a list of quarters to filter by
        :param request:
        :param model_admin:
        :return:
        """
        return (
            ("ThisQ", "This quarter"),
            ("LastQ", "Last quarter"),
            ("YTD", "Year to date"),
            ("LastYTD", "This time last year"),
        )

    def queryset(self, request, queryset):
        today = now()
        thisq = today.month // 3
        if self.value() == "ThisQ":
            qstart = datetime.date(today.year, thisq * 3, 1)
            qend = qstart + datetime.timedelta(days=91)
            qs = queryset.filter(date_created__gte=qstart, date_created__lt=qend)
        elif self.value() == "LastQ":
            qend = datetime.date(today.year, thisq * 3, 1)
            qstart = qend - datetime.timedelta(days=91)
            qs = queryset.filter(date_created__gte=qstart, date_created__lt=qend)
        elif self.value() == "YTD":
            qstart = datetime.date(today.year, 1, 1)
            qs = queryset.filter(date_created__gte=qstart, date_created__lte=today)
        elif self.value() == "LastYTD":
            qstart = datetime.date(today.year - 1, 1, 1)
            qend = datetime.date(today.year - 1, today.month, today.day)
            qs = queryset.filter(date_created__gte=qstart, date_created__lte=qend)
        else:
            qs = queryset
        return qs


class OrderFilter(admin.SimpleListFilter):
    title = "Filter by Order Status Types"
    parameter_name = "order_status__name"

    def lookups(self, request, model_admin):
        """
        Provides a list of order_status to filter by
        :param request:
        :param model_admin:
        :return:
        """
        return (
            ("Completed", "Completed and Published"),
            ("Incomplete", "Active orders"),
            ("Suspended", "Cancel and Hold"),
        )

    def queryset(self, request, queryset):
        if self.value() == "Completed":
            qs = queryset.filter(order_status__name__in=['Done', 'published'])
        elif self.value() == "Incomplete":
            qs = queryset.exclude(order_status__name__in=["Done", 'published', 'Cancel', 'Hold'])
        elif self.value() == "Suspended":
            qs = queryset.filter(order_status__name__in=["Cancel", "Hold"])
        else:
            qs = queryset
        return qs


class RegistryCreated(DateRangeFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super(RegistryCreated, self).__init__(field, request, params, model, model_admin, field_path)
        self.title = "samples received"


class SamplesTested(DateRangeFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super(SamplesTested, self).__init__(field, request, params, model, model_admin, field_path)
        self.title = "samples analyzed"


@admin.register(OrderSummary)
class OrderSummaryAdmin(admin.ModelAdmin):
    change_list_template = "admin/order_summary_change_list.html"
    date_hierarchy = "date_modified"
    list_filter = (('sampleregistry__created', RegistryCreated),
                   ('samples__labmeasurement__created', SamplesTested),
                   ('date_modified', DateRangeFilter),
                   ("products__category", RelatedOnlyDropdownFilter),
                   ("products", RelatedOnlyDropdownFilter),
                   ('order_status__status_type', RelatedOnlyDropdownFilter),
                   ('order_status', RelatedOnlyDropdownFilter)
                   )

    def get_area(self, order):
        ## Try first to see if there is a sample receiving record for this order
        sample_recv_recs = order.sampleregistry_set.all()
        if sample_recv_recs:
            acres = sum([float(s.gps_acres) for s in sample_recv_recs if s.gps_acres is not None])
            return acres
        return float(order.field.area)

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset.prefetch_related('field')
        except (AttributeError, KeyError):
            return response
        if "products__id__exact" in request.GET.keys():
            prod = Product.objects.filter(id=request.GET.get("products__id__exact")).distinct()
            if prod.count() == 1:
                response.context_data['product'] = prod.first()

        metrics = {
            'total': Count('id'),
            # 'total_acres': self.Area('field'),
        }

        response.context_data['summary'] = list(
            qs
                .values('created_by__organization__id', 'created_by__organization__name')
                .annotate(**metrics)
                .order_by('-total')
        )

        response.context_data['summary_total'] = dict(
            qs.aggregate(**metrics)
        )
        response.context_data['summary_total']['total_acres'] = 0
        response.context_data['summary_total']['total_samples'] = 0
        for row in response.context_data['summary']:
            orders = qs.filter(created_by__organization__id=row['created_by__organization__id'])
            row['total_acres'] = sum([self.get_area(o) for o in orders]) if len(
                response.context_data['summary']) < 25 else 0
            row['total_samples'] = sum([o.num_samples for o in orders]) if len(
                response.context_data['summary']) < 25 else 0
            response.context_data['summary_total']['total_acres'] += row['total_acres']
            response.context_data['summary_total']['total_samples'] += row['total_samples']

        return response


@admin.register(SamplerSummary)
class SamplerSummaryAdmin(admin.ModelAdmin):
    change_list_template = "admin/sampler_summary_change_list.html"
    date_hierarchy = "date_created"
    list_filter = (('date_created', DateRangeFilter),
                   ("sampler", RelatedOnlyDropdownFilter),
                   ('order_status__status_type', RelatedOnlyDropdownFilter),
                   ('order_status', RelatedOnlyDropdownFilter)
                   )

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset.prefetch_related('field')
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': Count('id'),
            # 'total_acres': self.Area('field'),
        }

        response.context_data['summary'] = list(
            qs
                .filter(sampler__isnull=False)
                .prefetch_related("samples")
                .values('sampler__id', 'sampler')
                .annotate(**metrics)
                .order_by('-total')
        )

        response.context_data['summary_total'] = dict(
            qs.filter(sampler__isnull=False).aggregate(**metrics)
        )
        response.context_data['summary_total']['total_acres'] = 0
        response.context_data['summary_total']['total_gpsacres'] = 0
        response.context_data['summary_total']['total_samples'] = 0
        for row in response.context_data['summary']:
            orders = qs.filter(sampler__id=row['sampler__id']).prefetch_related("field", "samples")
            try:
                row['sampler'] = OrganizationUser.objects.get(id=row['sampler__id'])
            except:
                row['sampler'] = None
            row['total_acres'] = sum([o.field.area for o in orders]) if len(
                response.context_data['summary']) < 25 else 0
            row['total_samples'] = sum([o.num_samples for o in orders]) if len(
                response.context_data['summary']) < 25 else 0
            receiving_records = SampleRegistry.objects.filter(order__in=orders,
                                                              gps_acres__isnull=False).values_list('gps_acres',
                                                                                                   flat=True)
            row['gps_acres'] = sum(receiving_records) if len(receiving_records) < 25 else 0
            response.context_data['summary_total']['total_acres'] += row['total_acres']
            response.context_data['summary_total']['total_gpsacres'] += row['gps_acres']
            response.context_data['summary_total']['total_samples'] += row['total_samples']

        if "sampler__id__exact" in request.GET.keys():
            # Request is for a single parameter
            response.context_data['sampler'] = OrganizationUser.objects.get(id=request.GET.get("sampler__id__exact"))
            response.context_data['samplers_orders'] = qs.filter(sampler__id=request.GET.get("sampler__id__exact"))

        return response


admin.site.register(OrderStatusType)
