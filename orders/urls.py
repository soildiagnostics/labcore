from django.urls import path

from common.apihelper import apipath
from .apiviews import APIOrderList, APIOrderTransmit, APIOrderStatusUpdate, APIFieldOrderStatusView
from .views import OrderListJSON, OrderDetail, CreateOrder, \
    UpdateOrder, CreateReOrder, WorkOrderDetail, SamplingListJson, PrintLabels

urlpatterns = [
    # path('', OrderList.as_view(), name="order_list"),
    path('data/', OrderListJSON.as_view(), name="order_list_json"),
    path('sampling/', SamplingListJson.as_view(), name="sampler_list_json"),
    path('<pk>/', OrderDetail.as_view(), name="order_detail"),
    path('<pk>/wo/', WorkOrderDetail.as_view(), name="work_order_detail"),
    path('<pk>/reorder/', CreateReOrder.as_view(), name="order_reorder"),
    path('field/<pk>/neworder/', CreateOrder.as_view(), name="new_order"),
    path('<pk>/update/', UpdateOrder.as_view(), name="update_order"),
    path('<pk>/labels/', PrintLabels.as_view(), name="print_labels"),
    ]

urlpatterns += [
    apipath('list/', APIOrderList.as_view(), name="api_order_list"),
    apipath('transmit/', APIOrderTransmit.as_view(), name="api_order_transmit"),
    apipath('<int:pk>/statusupdate/', APIOrderStatusUpdate.as_view(), name="api_order_status_update"),
    apipath("list/spatial/", APIFieldOrderStatusView.as_view(), name="api_order_list_birdseye")
    # apipath('<int:pk>/task_done/<int:product>/', APIOrderTaskMarkDone.as_view(), name="api_order_task_done")
]