import json

from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings
from django.db.models import Q
from django.forms import widgets

from associates.models import OrganizationUser, Organization
from clients.models import Field
from products.models import Product
from .models import Order, OrderStatus


class NewOrderForm(forms.ModelForm):
    easy_product = forms.CharField(required=False)
    notes = forms.CharField(required=False, widget=forms.Textarea,
                            help_text="Order specific instructions or notes only.")
    order_status = forms.ModelChoiceField(
        queryset=OrderStatus.objects.filter(name__in=["new", "field ready", "field not ready"]),
        widget=widgets.RadioSelect,
        help_text="Set field status if order requires soil sampling",
        empty_label=None)

    class Meta:
        model = Order
        widgets = {'additional_data': forms.HiddenInput()}
        exclude = [
            'field',
            'parent_order',
            'legacy_order',
            'legacy_id',
            'products',
            'sampler',
            'content_type',
            'object_id'
        ]

    def __init__(self, *args, **kwargs):

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.org = kwargs.pop('org')
        self.user = kwargs.pop('user')
        self.field = kwargs.pop('field')
        try:
            prod = kwargs.pop('product')
        except KeyError:
            prod = None
        try:
            self.parent_order = kwargs.pop('parent_order')
        except Exception as e:
            pass
        super(NewOrderForm, self).__init__(*args, **kwargs)
        try:
            self.initial['additional_data'] = self.parent_order.additional_data
        except AttributeError:
            pass

        self.fields['created_by'].queryset = OrganizationUser.objects.filter(
            organization=self.org, user__is_active=True)
        self.initial['easy_product'] = prod
        self.fields['easy_product'].widget = forms.HiddenInput()
        samplers = Organization.objects.get_sampler_organizations()
        # self.fields['sampler'].queryset = OrganizationUser.objects.filter(organization__in=samplers)
        self.fields['lab_processor'].queryset = Organization.objects.get_lab_processor_organizations()
        try:
            lab = Organization.objects.get(name=settings.DEFAULT_LAB_PROCESSOR)
            assert (lab.is_lab_processor)
            if 'lab_processor' not in self.initial:
                self.initial['lab_processor'] = lab
            self.fields['lab_processor'].widget = forms.HiddenInput()
        except Organization.DoesNotExist:
            # print("No such lab")
            pass
        except AssertionError:
            pass

        if self.instance.pk is None:
            try:
                self.initial['created_by'] = OrganizationUser.objects.get(user=self.user)
            except OrganizationUser.DoesNotExist:
                self.initial['created_by'] = Organization.objects.me.organization_users.first()
                self.initial['notes'] = f"Order created by client {self.user}"
            os, created = OrderStatus.objects.get_or_create(name="new")
            self.fields['order_status'].initial = os
        if self.user.is_staff:
            self.fields['order_status'].queryset = OrderStatus.objects.all()
            client = Field.objects.get(pk=self.field).farm.client
            self.fields['created_by'].queryset = OrganizationUser.objects.filter(Q(user=self.user) |
                                                                                 Q(organization=client.originator.organization)) \
                .filter(user__is_active=True).order_by('user__last_name')
            # self.fields['created_by'].queryset = OrganizationUser.objects.filter(
            #     #organization=self.instance.created_by.organization, user__is_active=True)
            #     user__is_active=True)

        # self.helper.layout = self.helper.layout = Layout(
        #     Div(
        #         Div('order_number', 'order_status', css_class="col-md-12 form-group"),
        #         Div('created_by', css_class="col-md-8"),
        #         Div('sampler', css_class="col-md-4"),
        #         css_class="col-md-12"
        #     )
        #
        # )

    # def is_valid(self):
    #     import pdb; pdb.set_trace()
    #     v = super().is_valid()
    #     return v

    def save(self, commit=True):
        instance = super(NewOrderForm, self).save(commit=False)
        instance.field = Field.objects.get(pk=self.field)
        try:
            instance.parent_order = self.parent_order
        except Exception as e:
            # print("No parent order")
            pass

        if commit:
            instance.save()
            instance.order_number = str(instance.pk)
            instance.save()
            # update product lists
            try:
                instance.products.clear()
                proddict = json.loads(self.data['additional_data'])
                for k in proddict.keys():
                    p = Product.objects.get(pk=int(k))
                    instance.products.add(p)
            except KeyError:
                # print("No product list")
                pass

        return instance
