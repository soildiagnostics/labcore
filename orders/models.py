from decimal import Decimal

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.mail import send_mail
from django.db import models
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from djmoney.money import Money

from associates.models import Organization, OrganizationUser
from clients.models import Field
from common.models import ModelDiffMixin
from customnotes.models import CustomField
from fieldcalendar.mixins import FieldJournalMixin
from products.models import Product, TestParameter, Price


class OrderStatusType(models.Model):
    name = models.CharField(max_length=50, help_text=_("Category of Order Status"))

    def __str__(self):
        return self.name


class OrderStatus(models.Model):
    name = models.CharField(max_length=100, help_text=_("Do not change. If you need to change "
                                                        "the label of the order status, use "
                                                        "the verbose name field below. "))
    alert = models.BooleanField(default=False, help_text=_("Send email when the order status updates to this"))
    allow_edits = models.BooleanField(default=True, help_text=_("Allows Dealers to edit if the order is in this state"))
    include_on_dashboard = models.BooleanField(default=True,
                                               help_text=_("Include in search results presented on Dashboard"))
    verbose_name = models.CharField(max_length=100, default='',
                                    help_text=_("If not blank, this name is used to render the status"))
    allow_third_party_view = models.BooleanField(default=False,
                                                 help_text=_(
                                                     "Allows third party assignees to view if the order is in this state"))
    status_type = models.ForeignKey(OrderStatusType, blank=True, null=True, on_delete=models.PROTECT)
    preceded_by = models.ForeignKey("OrderStatus", related_name="next", blank=True,
                                    null=True, on_delete=models.PROTECT)
    succeeded_by = models.ForeignKey("OrderStatus", related_name="prev", blank=True,
                                     null=True, on_delete=models.PROTECT)

    def __str__(self):
        if self.verbose_name != '':
            return self.verbose_name
        return self.name

    class Meta:
        verbose_name = "Order Status Label"
        verbose_name_plural = "Order Status Labels"


class Order(FieldJournalMixin, ModelDiffMixin, models.Model):
    order_number = models.CharField(max_length=50,
                                    help_text=_("Can contain up to \
                                        50 alphanumeric characters"))
    field = models.ForeignKey(Field, null=True, on_delete=models.CASCADE,
                              related_name='orders', blank=True,
                              help_text=_("Please confirm this \
                                    is the correct field"))
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        OrganizationUser, on_delete=models.PROTECT,
        blank=True, null=True,
        help_text=_("This should be the user entering the order, typically"))
    lab_processor = models.ForeignKey(Organization, blank=True, null=True,
                                      on_delete=models.CASCADE,
                                      help_text=_(
                                          "This should be the lab that you will send this order to for analysis"))
    legacy_order = models.BooleanField(default=False, help_text=_(
        "Is this order imported from somewhere else"))
    legacy_id = models.CharField(max_length=200, help_text=_(
        "Location of legacy order"), blank=True)

    notes = models.TextField(blank=True, null=True,
                             help_text="Additional notes regarding this order")
    order_status = models.ForeignKey(OrderStatus, on_delete=models.PROTECT,
                                     help_text=_("Indicate field status if order requires sampling"))
    parent_order = models.ForeignKey("self", help_text=_("Orders related to this one"),
                                     blank=True, on_delete=models.CASCADE, null=True)
    products = models.ManyToManyField(Product, blank=True)
    additional_data = models.JSONField(blank=True, null=True, help_text=_("Do not change this field manually!"))
    sampler = models.ForeignKey(OrganizationUser, blank=True, null=True,
                                on_delete=models.PROTECT,
                                help_text=_("Assign a sampler"), related_name="sampler")

    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT, blank=True, null=True,
                                     help_text=_("Do not edit!"))
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    capture_diff_for_fields = [sampler, order_status]

    def __str__(self):
        return self.order_number

    def get_absolute_url(self):
        return reverse("order_detail", kwargs={'pk': self.pk})

    def get_quantity_for_product(self, product):
        assert product in self.products.all(), f"Product {product} not in Order {self}"
        try:
            prod_details = self.additional_data.get(str(product.id))
            for question_id, answer in prod_details.items():
                cf = CustomField.objects.get(pk=int(question_id))
                if cf.name == "Quantity":
                    return Decimal(answer)
            return Decimal(1)
        except Exception:
            return Decimal(1)

    def subtotal_for_product(self, product):
        qty = self.get_quantity_for_product(product)
        try:
            try:
                price = product.discounted_price(product.prices.get(), self.created_by.organization).amount
            except AttributeError:
                price = product.discounted_price(product.prices.get(),
                                                 self.field.farm.client.originator.organization).amount
        except Price.DoesNotExist:
            return None
        except Price.MultipleObjectsReturned:
            return "Multiple prices"
        return qty * price

    def total(self, currency="USD"):
        cost = Money(0, currency)
        error = False
        for prod in self.products.all():
            try:
                subtotal = self.subtotal_for_product(prod)
                cost = cost + self.subtotal_for_product(prod)
            except Exception:
                error = True
        if error:
            return (cost, "Errors")
        return cost

    @property
    def is_editable(self):
        return self.sampler is None and self.order_status.allow_edits

    @property
    def render_additional_data(self):
        strdict = dict()
        try:
            if self.additional_data.keys():
                for pid, qdict in self.additional_data.items():
                    prod = Product.objects.get(id=int(pid))
                    qstrdict = dict()
                    for cfid, answer in qdict.items():
                        cf = CustomField.objects.get(pk=int(cfid))
                        qstrdict[cf.name] = answer
                    strdict[prod.name] = qstrdict
        except Exception:
            return {'error': 'Malformed Data'}
        return strdict

    def additional_data_for_product(self, product):
        data = self.render_additional_data.get(product.name)
        try:
            return [f"{k}: {v}" for k, v in data.items()]
        except AttributeError:
            return None

    @property
    def totalUSD(self):
        return self.total('USD')

    @property
    def field_name(self):
        return self.field.name

    @cached_property
    def get_products(self):
        return ', '.join([p.name for p in self.products.all()])

    @cached_property
    def get_parameters(self):
        prods = self.products.all()
        tp = TestParameter.objects.filter(product__in=prods)
        return tp

    @cached_property
    def get_tested_parameters(self):
        return self.samples.all().labmeasurement_set.all()

    @cached_property
    def sample_range(self):
        samples = self.samples.order_by('pk').all()
        if samples.count() > 0:
            return (samples.first().id, samples.last().id)
        return (None, None)

    def sample_range_for_file(self, file):
        samples = self.samples.filter(basket__file=file, void=False).order_by('pk').all()
        if samples.count() > 0:
            return (samples.first().id, samples.last().id)
        return (None, None)

    @cached_property
    def sampled(self):
        try:
            return self.sampleregistry_set.first().sampled
        except Exception:
            return None

    @cached_property
    def received_at_lab(self):
        try:
            return self.sampleregistry_set.first().created
        except Exception:
            return None

    @cached_property
    def analyzed(self):
        try:
            return self.samples.last().labmeasurement_set.last().created
        except Exception:
            return None

    @cached_property
    def num_samples(self):
        return self.samples.filter(void=False).count()

    def num_samples_for_file(self, file):
        return self.samples.filter(basket__file=file, void=False).count()

    def email_alert(self):
        if hasattr(self, "created_by") and self.created_by:
            if self.created_by.organization.is_primary:
                to = self.field.farm.client.contact.primary_emails
            else:
                to = self.created_by.user.person.primary_emails
        elif hasattr(self, "draftorder") and self.draftorder:
            to = self.draftorder.user.person.primary_emails
        else:
            to = None

        if to is not None:
            email_from = settings.DEFAULT_FROM_EMAIL
            subject = f"Order {self.order_number} updated: {self.order_status.name}"
            try:
                template = get_template("orders/email/status_update.txt")
                body = template.render({'order': self})
            except TemplateDoesNotExist:
                body = f"Order {self.order_number} was updated to {self.order_status.name}"
            send_mail(
                subject,
                body,
                email_from,
                to
            )

    def eligible_layers(self):
        """
        Returns shapefile layers that have the same number of features as samples in this order,
        so we can attach sample data to them to make shapefiles
        """
        layers = self.field.layer.filter(dbf_field_names__in=['SampleID', 'SAMPLEID', 'Zone_ID'])
        filtered = [l for l in layers if l.features == self.num_samples]
        return filtered

    def sampler_alert(self):
        subject = f"Order {self.order_number} updated: {self.order_status.name}"
        try:
            template = get_template("orders/email/sampler_notification.txt")
            body = template.render({'order': self})
        except TemplateDoesNotExist:
            body = f"Order {self.order_number} was assigned to you for sampling"
        to = self.sampler.user.person.primary_emails
        email_from = settings.DEFAULT_FROM_EMAIL

        send_mail(
            subject,
            body,
            email_from,
            to
        )

    def save(self, *args, **kwargs):
        # if 'sampler' in self.changed_fields and self.order_status.name == "New":
        #     s, _ = OrderStatus.objects.get_or_create(name="Sampler Assigned")
        #     self.order_status = s
        if self.pk == None:
            new_order = True
            # new order
        else:
            new_order = False

        if ('sampler' in self.changed_fields or new_order) and self.sampler is not None:
            o = super(Order, self).save(*args, **kwargs)
            self.create_journal_entry(self.field,
                                      f"{self.order_number} Sampler Assigned",
                                      entry=self.get_absolute_url())
            self.sampler_alert()
            return o

        if 'order_status' in self.changed_fields or new_order:
            o = super(Order, self).save(*args, **kwargs)
            self.create_journal_entry(self.field,
                                      f"{self.order_number} {self.order_status}",
                                      entry=self.get_absolute_url())
            if self.order_status.alert:
                self.email_alert()
            return o

        return super(Order, self).save(*args, **kwargs)

    def set_status(self, status):
        s, created = OrderStatus.objects.get_or_create(name=status)
        self.order_status = s
        self.save()

    class Meta:
        ordering = ["-date_created"]
        indexes = [
            models.Index(fields=['-date_created', '-id']),
            models.Index(fields=['date_created']),
            models.Index(fields=['order_status',
                                 'field',
                                 'created_by',
                                 ])
        ]

    def event_trail(self):
        return self.field.fieldevent_set.filter(details__title__icontains=self.order_number,
                                                event_type__name__in=["Journal Entry", "To Do item"]
                                                ).order_by('pk')

    def completed_tasks(self):
        events = self.event_trail()
        return events.filter(editable=False)

    def completed_for_product(self, product):
        events = self.completed_tasks()
        completed = events.filter(details__title__icontains=product.name)
        return completed


class ReportFile(models.Model):
    """
    Deprecated. Reportfiles are now set up under FieldEvents
    Look at order/pdf.py for usage.
    """
    created = models.DateTimeField(auto_now_add=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    file = models.FileField(upload_to="order_reports/")

    class Meta:
        ordering = ['created']

    def __str__(self):
        return self.file.name


#### Order analytics models

class OrderSummary(Order):
    class Meta:
        proxy = True
        verbose_name = "Order Analytics"
        verbose_name_plural = "Order Analytics"


class SamplerSummary(Order):
    class Meta:
        proxy = True
        verbose_name = "Sampler Analytics"
        verbose_name_plural = "Sampler Analytics"
