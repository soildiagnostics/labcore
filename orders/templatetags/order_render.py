from django import template

from samples.models import SampleRegistry

register = template.Library()

@register.filter
def answer(question, order):
    dat = order.additional_data
    answer = None
    for prod, customfield in dat.items():
        try:
            answer = customfield[str(question.pk)]
            return answer
        except KeyError:
            pass

    return answer

@register.filter
def billing_org(order, org):
    order_client_org = order.field.farm.client.originator.organization
    if order_client_org.is_primary:
        return order.field.farm.client
    return order_client_org

@register.filter
def format_address(billing_entity):
    address = None
    is_org = False
    try:
        address = billing_entity.company.address.filter(primary=True).first()
        is_org=True
    except AttributeError:
        address = billing_entity.contact.address.filter(primary=True).first()
    if not address:
        if is_org:
            address = billing_entity.company.address.first()
        else:
            address = billing_entity.contact.address.first()
    if address:
        # there is a related record with the address in it
        adict = address.address.as_dict()
        if adict.get('locality'):
            street = adict['formatted'].split(',')[0]
            city_zip = "{locality}, {state_code}  {postal_code}".format(**adict)
            return {'street': street, 'city': city_zip}
        else:
            return {'street': adict['formatted'], 'city': ""}
    return None

@register.filter
def primary_email(billing_entity):
    email = None
    is_org = False
    try:
        email = billing_entity.company.email.filter(primary=True).first()
        is_org=True
    except AttributeError:
        email = billing_entity.contact.email.filter(primary=True).first()
    if not email:
        if is_org:
            email = billing_entity.company.email.first()
        else:
            email = billing_entity.contact.email.first()
    if email:
        # there is a related record with the data in it
        return email.email.format()
    return None

@register.filter
def primary_phone(billing_entity):
    phone = None
    is_org = False
    try:
        phone = billing_entity.company.phone.filter(primary=True).first()
        is_org=True
    except AttributeError:
        phone = billing_entity.contact.phone.filter(primary=True).first()
    if not phone:
        if is_org:
            phone = billing_entity.company.phone.first()
        else:
            phone = billing_entity.contact.phone.first()
    if phone:
        # there is a related record with the data in it
        return str(phone)
    return None


@register.filter
def get_samplingrecord(order):
    try:
        samplerec = order.sampleregistry_set.first()
        return samplerec
    except SampleRegistry.DoesNotExist:
        return {}

@register.filter
def line_range(start, end):
    return range(start, end)

@register.filter
def remaining_line_range(context_line_count, total_lines):
    if context_line_count<total_lines:
        return range(0, (total_lines - context_line_count))
    return range(0,0)

def get_task_value(event):
    try:
        note = event.details.get('custom_fields')[0].get('value')
        note = note.replace("Completed: ", f"Completed on {event.start.strftime('%m/%d/%Y')}: ")
        return note
    except Exception as e:
        return repr(e)

@register.filter
def events_for_product(order, product):
    events = order.completed_for_product(product)
    strings =[get_task_value(fe) for fe in events]
    ## this filter is fragile
    return strings

