import json

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django_datatables_view.base_datatable_view import BaseDatatableView

from associates.role_privileges import DealershipRequiredMixin, FilteredObjectMixin, FilteredQuerySetMixin
from clients.forms import DisplayFieldForm
from clients.models import Field
from common.searchquery import SearchQueryAssemblyMixin
from fieldcalendar.models import FieldEvent
from orders.soildx_helper import get_soildx_rec
from products.models import Product
from samples.admin import process_samples_dict, products_and_test_parameters
from samples.models import File
from samples.models import Sample
from .forms import NewOrderForm
from .models import Order
from .pdfs import render_to_pdf


# Create your views here.


# class OrderList(DealershipRequiredMixin, ListView):
#     template_name = "orders/order_list.html"
#     model = Order
#
#     def get_context_data(self, **kwargs):
#         context = super(OrderList, self).get_context_data(**kwargs)
#         context['google_api_key'] = settings.GOOGLE_API_KEY
#         return context
    #
    # def get_queryset(self):
    #     return Order.objects.filter(field__farm__client__originator__organization=self.org)


class OrderListJSON(DealershipRequiredMixin, FilteredQuerySetMixin,
                    SearchQueryAssemblyMixin, BaseDatatableView):
    model = Order
    instance_owner_organization = "created_by__organization"
    instance_owner_client = "field__farm__client"
    client_has_permission = True
    columns = ['order_number',
               'order_status',
               'client',
               'farm',
               'field',
               'date_created',
               'date_modified',
               'products']

    order_columns = ['order_number',
                     'order_status',
                     'field__farm__client',
                     'field__farm',
                     'field',
                     'date_created',
                     'date_modified']
    max_display_length = 500
    common_queries = [
        'order_number__iexact',
        'date_created__year__iexact'
    ]
    search_fields = [
        'order_status__name',
        'order_status__verbose_name',
        'field__name',
        'field__farm__name',
        'field__farm__client__contact__name',
        'field__farm__client__contact__last_name',
        'field__farm__client__originator__user__person__name',
        'field__farm__client__originator__user__person__last_name',
        'field__farm__client__originator__organization__name',
        'products__name',
        'created_by__user__person__name',
        'created_by__user__person__last_name',
        'created_by__organization__name',
        'notes',
        'additional_data'
    ]

    def get_initial_queryset(self):
        return super().get_initial_queryset().select_related(
            'field', 'field__farm', 'field__farm__client', 'field__farm__client__contact',
            'created_by', 'created_by__user__person',
            'lab_processor', 'lab_processor__company',
            'order_status', 'parent_order', 'sampler'
        ).prefetch_related("products").filter(order_status__include_on_dashboard=True)

    def render_column(self, row, column):
        order = Order.objects.get(id=row.pk)
        if column == "order_number":
            editlink = ""
            if order.is_editable or self.org.is_primary:
                editlink = f'''<a class='pull-right' href='{reverse("update_order", kwargs={
                    'pk': row.pk})}' title='Edit'><i class='fa fa-edit' ></i>&nbsp</a>'''
            sample_points_link = ""
            if order.field.sample_points_url():
                sample_points_link = f'''<a class='pull-right' href='{order.field.sample_points_url()}' title='Download Sampling Map'><i class='fa fa-map-pin' ></i>&nbsp</a>'''
            viewtitle = "View"
            if self.org.is_primary:
                viewtitle = f"Originator: {row.field.farm.client.originator.organization}"
            link = f'''<a href='{reverse("order_detail",
                                         kwargs={'pk': row.pk})}' title='{viewtitle}'>{row.order_number}</a> 
                   <a class='pull-right' href='{reverse("order_reorder", kwargs={
                'pk': row.pk})}' title='Reorder'><i class='fa fa-redo'></i>&nbsp</a>
                   {editlink}
                   {sample_points_link} 
                   <a class='pull-right' href='{reverse('print_labels', kwargs={'pk': row.pk})}' 
                   title='Print Labels'><i class='fa fa-tag' ></i>&nbsp</a>
                   <a class='pull-right' href='{reverse("work_order_detail", kwargs={'pk': row.pk})}'
                   title='Print Work Order'><i class='fa fa-print' ></i>&nbsp</a>'''

            return link
        if column == "order_status":
            # if row.order_status.name == "field not ready":
            #     title = f"Sampler {row.sampler}"
            #     return f"""<a href="#" title="{title}">{row.order_status.__str__()}</a>
            #         <a href="{reverse('api_order_status_update', kwargs={'pk': row.pk})}?status=ready"
            #         class="pull-right order-status"
            #         title="Field ready to sample"><i class="fa fa-clipboard-check"></i></a>"""
            if row.order_status.succeeded_by:
                title = f"Sampler {row.sampler}"
                return f"""<a href="#" title="{title}">{row.order_status.__str__()}</a>
                                    <a href="{reverse('api_order_status_update', kwargs={'pk': row.pk})}?status={row.order_status.succeeded_by.pk}" 
                                    class="pull-right order-status" 
                                    title="Mark {row.order_status.succeeded_by}"><i class="fa fa-clipboard-check"></i></a>"""
            if row.sampler:
                title = f"Assigned to {row.sampler}"
                return f"""<a href="#" title="{title}">{row.order_status.__str__()}</a>"""
            return f"""<a href="#" title="No sampler assigned">{row.order_status.__str__()}</a>"""
        if column == "client":
            link = "<a href='{}'>{}</a>".format(
                reverse("client_detail", kwargs={'pk': row.field.farm.client.pk}),
                row.field.farm.client.contact
            )
            return link
        elif column == "farm":
            link = "<a href='{}'>{}</a>".format(
                reverse("farm_detail", kwargs={'pk': row.field.farm.pk}),
                row.field.farm.name
            )
            return link
        elif column == "field":
            link = "<a href='{}'>{} {}</a>".format(
                reverse("field_detail", kwargs={'pk': row.field.pk}),
                row.field.name,
                f"({row.field.area} acres)" if row.field.area else ""
            )
            gis_link = "<a href='{}' class='pull-right' title='GIS'><i class='fa fa-map-pin'>&nbsp;</i></a>".format(
                reverse("field_geo_details", kwargs={'pk': row.field.pk})
            )
            return link + gis_link
        elif column == "parent_order":
            if row.parent_order:
                link = "<a href='{}'>{}</a>".format(
                    reverse("order_detail", kwargs={'pk': row.parent_order.pk}),
                    row.parent_order.order_number
                )
                return link
        elif column == "date_created":
            return row.date_created
        elif column == "date_modified":
            return row.date_modified
        elif column == "products":
            prods = row.products.all()
            if prods:
                schedule_links = list()
                for prod in prods:
                    prodinfo = ""
                    if prod.custom_fields.exists():
                        desc = order.additional_data_for_product(prod)
                        prodinfo = "\n".join(desc if desc else list())
                        prodinfo = f"<a href='#' title='{prodinfo}' class='pull-left'><i class='fa fa-info'></i>&nbsp;&nbsp;</a>"
                    imgtag=""
                    if prod.image:
                        imgtag = f"<img src={prod.image.url} width='1em'>"
                    completed = row.completed_for_product(prod)
                    if completed.count() == 0:
                        link = f"""{prodinfo} {imgtag} {prod.name} <a href="#" 
                        class="pull-right scheduleit" field_id="{row.field.pk}"
                        field_name="{row.field.name}"
                        product_name="{prod.name}"
                        order_number="{row.pk}"
                        product_id="{prod.pk}"
                        title="Mark Done or Schedule it!"><i class="fa fa-calendar-plus"></i>&nbsp;</a>"""
                        # link2 = f"""<a href="{reverse("api_order_task_done", kwargs={"pk": row.pk, "product": prod.pk})}"
                        # class="pull-right task_done" field_id="{row.field.pk}"
                        # field_name="{row.field.name}"
                        # order_number="{row.pk}"
                        # product_name="{prod.name}"
                        # product_id="{prod.pk}"
                        # title="Mark as done!"><i class="fa fa-calendar-check"></i>&nbsp;</a>"""
                    else:
                        evt = completed.latest('pk')
                        link = f"""{prod.name} 
                        <span class=pull-right 
                        title="Completed on {evt.start.strftime('%m/%d/%Y')}">Done!</span>"""
                    schedule_links.append(link)
                return "<hr>".join(schedule_links)
            else:
                return "None"
        else:
            return super(OrderListJSON, self).render_column(row, column)


class SamplingListJson(DealershipRequiredMixin, SearchQueryAssemblyMixin, BaseDatatableView):
    model = Order
    columns = ['order_number',
               'order_status',
               'client',
               'farm',
               'field',
               'date_created',
               'date_modified']

    common_queries = [
        'order_number__iexact',
        'date_created__year__iexact'
    ]
    search_fields = [
        'order_status__name',
        'field__name',
        'field__county',
        'field__farm__name',
        'field__farm__client__contact__name',
        'field__farm__client__contact__last_name',
        'sampler__user__person__name',
        'sampler__user__person__last_name'
    ]

    def get_initial_queryset(self):
        return Order.objects.filter(sampler__organization=self.org,
                                    order_status__allow_third_party_view=True
                                    ).select_related(
            'field', 'field__farm', 'field__farm__client', 'field__farm__client__contact',
            'created_by', 'created_by__user__person',
            'lab_processor', 'lab_processor__company',
            'order_status', 'parent_order', 'sampler'
        )

    def render_column(self, row, column):
        if column == "order_number":
            order = Order.objects.get(id=row.pk)
            sample_points_link = ""
            if order.field.sample_points_url():
                sample_points_link = f'''<a class='pull-right' href='{order.field.sample_points_url()}' 
                title='Download Boundary & Points'><i class='fa fa-map-pin' ></i>&nbsp</a>'''
            viewtitle = "View"
            if self.org.is_primary:
                viewtitle = f"Originator: {row.field.farm.client.originator.organization}"
            link = f'''{row.order_number} 
                   {sample_points_link} 
                   <a class='pull-right' href='{reverse('print_labels', kwargs={'pk': row.pk})}' 
                   title='Print Labels'><i class='fa fa-tag' ></i>&nbsp</a>
                   <a class='pull-right' href='{reverse("work_order_detail", kwargs={'pk': row.pk})}'
                   title='Print Work Order'><i class='fa fa-print' ></i>&nbsp</a>'''

            return link

        if column == "order_status":
            go_icon = "<i class='fa fa-check-circle' style='color: limegreen; font-size: 1.5em;'></i><b>&nbsp;Ready to Sample&nbsp;&nbsp;</b>"
            order_status_icon = go_icon if row.order_status.name[
                                           0:11].lower() == 'field ready' else row.order_status.name
            if row.order_status.succeeded_by:
                next_link = f"""<a href="{reverse('api_order_status_update', kwargs={'pk': row.pk})}?status={row.order_status.succeeded_by.pk}" 
                                    class="pull-right order-status" 
                                    title="Mark {row.order_status.succeeded_by}"><i class="fa fa-clipboard-check"></i></a>"""
            else:
                next_link = None
            status_render = "{}&nbsp;&nbsp;" \
                            "{}{}".format(order_status_icon,
                                          f"assigned to {str(row.sampler.user.person)}",
                                          next_link)
            return status_render
        if column == "client":
            link = "{}<br>Dealer: {}".format(
                row.field.farm.client.contact,
                row.field.farm.client.originator
            )
            return link
        elif column == "farm":
            link = "{}".format(
                row.field.farm.name
            )
            return link
        elif column == "field":
            link = "{} {}<br>County: {}".format(
                row.field.name,
                f"({row.field.area} acres)" if row.field.area else "",
                row.field.county if row.field.county else "??"
            )
            return link
        elif column == "date_created":
            return row.date_created
        elif column == "date_modified":
            return row.date_modified
        else:
            return super(SamplingListJson, self).render_column(row, column)


class OrderDetail(FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    queryset = Order.objects.select_related(
        'field', 'field__farm', 'field__farm__client', 'field__farm__client__contact',
        'created_by', 'created_by__user__person',
        'lab_processor', 'lab_processor__company',
        'order_status', 'parent_order', 'sampler'
    )
    # model=Order
    instance_owner_organization = "created_by__organization"
    template_name = "orders/order_detail.html"
    hallpass_field = "field"
    client_has_permission = True
    instance_owner_client = 'field__farm__client'

    def get_context_data(self, **kwargs):
        order = self.get_object()
        context = super(OrderDetail, self).get_context_data(**kwargs)
        context['view_type'] = self.request.GET.get('view', 'report')
        context['field'] = self.object.field
        context['fielddisplay'] = DisplayFieldForm(instance=context['field'])
        context['formtype'] = "View Order"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['org'] = self.org
        context['reports'] = FieldEvent.objects.filter(field=order.field,
                                                       file__icontains=f"{order.order_number}_report").order_by('-pk')
        context['allow_edit'] = self.object.is_editable or self.org.is_primary
        context['file'] = File.objects.filter(basket__samples__in=order.samples.all()).distinct().first()
        fsnrec = get_soildx_rec(order.order_number)
        if fsnrec:
            context['fertisavern'] = json.loads(fsnrec['recommendation_text'])

        ## Section for wiring up information from custom fields ##
        split = Product.objects.filter(name="Split Billing").first()
        if split in self.object.products.all():
            customfield = self.object.additional_data[str(split.pk)]
            customfield_pk = str(split.custom_fields.get(name="Split details").pk)
            data = customfield[customfield_pk].split(";")
            context['split'] = [d.split(",") for d in data]

        products = Product.objects.filter(order=order)
        products_qs, prod_test_params, unique_parameters = products_and_test_parameters(products,
                                                                                        include_calculated=True)
        context['products'] = products_qs

        samples_qs = Sample.objects.filter(order=order, void=False)
        readings = process_samples_dict(samples_qs, products_qs, prod_test_params, unique_parameters, last_valid=True)
        context['samples_list'] = readings

        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'pdf':
            response_kwargs.update({'template': "orders/order_detail_pdf.html"})
            return render_to_pdf(context, request=self.request, **response_kwargs)
        return super().render_to_response(
            context, **response_kwargs)


class WorkOrderDetail(FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    model = Order
    instance_owner_organization = "created_by__organization"
    instance_owner_client = "field__farm__client"
    template_name = "orders/work_order_detail_db.html"
    client_has_permission = True
    hallpass_field = "field"

    def get_object(self, *args, **kwargs):
        """
        Normally, the link to the work order shouldn't be rendered in the table.
        This prevents the view from working at all, if the permissions aren't correct.
        :param args:
        :param kwargs:
        :return:
        """
        obj = super().get_object(*args, **kwargs)
        if not self.org.is_primary and not self.org.is_dealer and self.org.is_sampler:
            try:
                assert obj.order_status.allow_third_party_view == True, "Not available"
            except AssertionError:
                raise PermissionDenied
        return obj

    def get_context_data(self, **kwargs):
        context = super(WorkOrderDetail, self).get_context_data(**kwargs)
        context['view_type'] = self.request.GET.get('view', 'report')
        context['field'] = self.object.field
        context['fielddisplay'] = DisplayFieldForm(instance=context['field'])
        context['formtype'] = "View Order"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['org'] = self.org
        context['reports'] = FieldEvent.objects.filter(field=self.object.field,
                                                       file__icontains=f"{self.object.order_number}_report").order_by(
            '-pk')
        context['allow_edit'] = self.object.is_editable or self.org.is_primary

        products = self.object.products.all()
        prod_count = 0
        question_count = 0
        for prod in products:
            if prod.is_package:
                prod_count += prod.package.count() + 1
                question_count += prod.custom_fields.count()
                for subprod in prod.package.all():
                    question_count += subprod.custom_fields.count()
            else:
                prod_count += 1
                question_count += prod.custom_fields.count()

        context['product_line_count'] = prod_count + question_count

        ## Section for wiring up information from custom fields ##
        split = Product.objects.filter(name="Split Billing").first()
        if split in self.object.products.all():
            customfield = self.object.additional_data[str(split.pk)]
            customfield_pk = str(split.custom_fields.get(name="Split details").pk)
            data = customfield[customfield_pk].split(";")
            context['split'] = [d.split(",") for d in data]
            context['residue'] = range(0, 3 - len(context['split']))
        else:
            context['split'] = None
            context['residue'] = range(0, 3)

        context['plat_file'] = self.object.field.plat_file
        return context

    def render_to_response(self, context, **response_kwargs):
        response_kwargs["template"] = self.template_name
        return render_to_pdf(context, request=self.request, **response_kwargs)


class CreateOrder(DealershipRequiredMixin, CreateView):
    model = Order
    form_class = NewOrderForm
    template = "orders/new_order.html"

    client_has_permission = settings.CLIENT_CAN_PLACE_ORDERS

    def get_context_data(self, **kwargs):
        context = super(CreateOrder, self).get_context_data(**kwargs)
        context['field'] = Field.objects.get(pk=self.kwargs['pk'])
        context['fielddisplay'] = DisplayFieldForm(instance=context['field'])
        context['formtype'] = "Create new Order"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['products'] = Product.objects.get_available_products(self.org)
        context['org'] = self.org
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateOrder, self).get_form_kwargs()
        kwargs['org'] = self.org
        kwargs['user'] = self.request.user
        kwargs['field'] = self.kwargs['pk']
        kwargs['product'] = self.request.GET.get('product') or None
        return kwargs


class CreateReOrder(DealershipRequiredMixin, CreateView):
    model = Order
    form_class = NewOrderForm
    template = "orders/new_order.html"
    client_has_permission = settings.CLIENT_CAN_PLACE_ORDERS

    def get_context_data(self, **kwargs):
        context = super(CreateReOrder, self).get_context_data(**kwargs)
        parent_order = Order.objects.get(pk=self.kwargs['pk'])
        context['parent_order'] = parent_order
        context['field'] = parent_order.field
        context['fielddisplay'] = DisplayFieldForm(instance=context['field'])
        context['formtype'] = "Repeat Order based on {}".format(parent_order.order_number)
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['products'] = Product.objects.get_available_products(self.org)
        context['org'] = self.org
        return context

    def get_form_kwargs(self):
        kwargs = super(CreateReOrder, self).get_form_kwargs()
        parent_order = Order.objects.get(pk=self.kwargs['pk'])
        kwargs['field'] = parent_order.field.pk
        kwargs['org'] = self.org
        kwargs['user'] = self.request.user
        kwargs['parent_order'] = parent_order
        return kwargs


class UpdateOrder(DealershipRequiredMixin, UpdateView):
    model = Order
    form_class = NewOrderForm
    template = "orders/new_order.html"
    client_has_permission = settings.CLIENT_CAN_PLACE_ORDERS

    def get_context_data(self, **kwargs):
        context = super(UpdateOrder, self).get_context_data(**kwargs)
        context['field'] = self.object.field
        context['fielddisplay'] = DisplayFieldForm(instance=context['field'])
        context['formtype'] = "Update Existing Order"
        context['google_api_key'] = settings.GOOGLE_API_KEY
        context['products'] = Product.objects.get_available_products(self.org)
        context['org'] = self.org
        context['allow_edit'] = self.object.is_editable or self.org.is_primary
        return context

    def get_form_kwargs(self):
        kwargs = super(UpdateOrder, self).get_form_kwargs()
        kwargs['org'] = self.org
        kwargs['user'] = self.request.user
        kwargs['field'] = self.object.field.pk
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.is_editable and not self.org.is_primary:
            return HttpResponseRedirect(self.object.get_absolute_url())
        else:
            return super().get(request, *args, **kwargs)


# class PDFResponseMixin:
#     """
#     Mixin for Django class based views.
#     Switch normal and pdf template based on request.
#
#     The switch is made when the request has a particular querydict, per
#     class attributes, `pdf_querydict_keys` and `pdf_querydict_value`
#     example:
#
#         http://www.example.com?[pdf_querydict_key]=[pdf_querydict_value]
#
#     Example with values::
#
#         http://www.example.com?format=pdf
#
#     Simplified version of snippet here:
#     http://djangosnippets.org/snippets/2540/
#     """
#     pdf_querydict_key = 'format'
#     pdf_querydict_value = 'pdf'
#
#     def is_workorder(self):
#         value = self.request.GET.get("workorder", '')
#         return value.lower() == 'workorder'
#
#     def is_pdf(self):
#         value = self.request.GET.get(self.pdf_querydict_key, '')
#         return value.lower() == self.pdf_querydict_value.lower()
#
#     def get_pdf_response(self, context, **response_kwargs):
#         return render_to_pdf(context, request=self.request, **response_kwargs)
#
#     def render_to_response(self, context, **response_kwargs):
#         if self.is_pdf():
#             # from django.conf import settings
#             # context['STATIC_ROOT'] = settings.STATIC_ROOT
#             return self.get_pdf_response(context, **response_kwargs)
#         # context[self.pdf_url_varname] = self.get_pdf_url()
#         return super(PDFResponseMixin, self).render_to_response(
#             context, **response_kwargs)

class PrintLabels(DealershipRequiredMixin, TemplateView):
    template_name = "orders/avery5160.html"

    def groupby(self, arr, by):
        for i in range(0, len(arr), by):
            yield arr[i:i + by]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        num_samples = range(int(self.request.GET.get("samples", 30)))
        start = int(self.request.GET.get('start', 1))
        samples = [s + start for s in num_samples]
        context['samples_grp'] = self.groupby(samples, 3)
        context['order'] = Order.objects.get(**kwargs)
        return context

    def render_to_response(self, context, **response_kwargs):
        response_kwargs.update({'template': self.template_name})
        return render_to_pdf(context, request=self.request, **response_kwargs)
