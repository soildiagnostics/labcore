from rest_framework import serializers

from clients.models import Field
from clients.serializers import FieldSerializer, FieldSerializerBinaryFiles, FieldBoundarySerializer
from products.serializers import NestedProductSerializer, ProductSummarySerializer
from .models import Order, OrderStatus
from django.contrib.gis.geos import GEOSGeometry
from django.core.serializers import serialize


class OrderStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatus
        fields = ('id', 'name')


class OrderSerializer(serializers.ModelSerializer):
    field = FieldSerializer(many=False)
    products = NestedProductSerializer(many=True)
    order_status = OrderStatusSerializer(read_only=True)
    lab_processor = serializers.SerializerMethodField()
    additional_data_string = serializers.SerializerMethodField()

    def get_additional_data_string(self, obj):
        return obj.render_additional_data

    def get_lab_processor(self, obj):
        return str(obj.lab_processor)

    class Meta:
        model = Order
        exclude = ("legacy_order", "legacy_id", "parent_order", "created_by")


class OrderTransmitSerializer(serializers.ModelSerializer):
    # field = serializers.RelatedField(read_only=True)
    # sampler = serializers.RelatedField(read_only=True)
    initiator = serializers.SerializerMethodField()

    def get_initiator(self, obj):
        return str(obj.created_by.organization.svxparticipant.id)

    client = serializers.SerializerMethodField()

    def get_client(self, obj):
        return str(obj.field.farm.client.contact)

    farm = serializers.SerializerMethodField()

    def get_farm(self, obj):
        return str(obj.field.farm)

    field = FieldSerializerBinaryFiles()
    product_list = serializers.SerializerMethodField()

    def get_product_list(self, obj):
        return [p.id for p in obj.products.all()]

    class Meta:
        model = Order
        fields = "__all__"


class OrderSummarySerializer(serializers.ModelSerializer):
    products = ProductSummarySerializer(many=True)
    order_status = OrderStatusSerializer(read_only=True)

    class Meta:
        model = Order
        exclude = ("legacy_order", "legacy_id", "parent_order", "created_by")


class FieldOrderSerializer(serializers.ModelSerializer):
    orders = serializers.SerializerMethodField()
    boundary = serializers.SerializerMethodField()

    def get_boundary(self, obj: Field):
        return GEOSGeometry(obj.boundary).geojson

    def get_orders(self, obj):
        return OrderSummarySerializer(obj.recent_orders, many=True).data

    class Meta:
        model = Field
        fields = ("id", "name", "boundary", "orders")


def field_order_serializer(fieldqs):
    return serialize("geojson", fieldqs,
                     geometry_field="boundary",
                     fields=('id', 'name'))
