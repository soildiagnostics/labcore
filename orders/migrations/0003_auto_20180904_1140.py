# Generated by Django 2.0.7 on 2018-09-04 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0015_auto_20180904_1005'),
        ('orders', '0002_cart'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cart',
            name='order_number',
        ),
        migrations.RemoveField(
            model_name='cart',
            name='products',
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(to='products.Product'),
        ),
        migrations.DeleteModel(
            name='Cart',
        ),
    ]
