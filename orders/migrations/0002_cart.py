# Generated by Django 2.0.7 on 2018-09-04 14:51

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_auto_20180904_0951'),
        ('orders', '0001_squashed_0003_auto_20180828_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_number', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='orders.Order')),
                ('products', models.ManyToManyField(to='products.Product')),
            ],
        ),
    ]
