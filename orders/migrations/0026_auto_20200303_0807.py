# Generated by Django 2.1.5 on 2020-03-03 14:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0025_auto_20200128_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='created_by',
            field=models.ForeignKey(blank=True, help_text='This should be the user entering the order, typically', null=True, on_delete=django.db.models.deletion.PROTECT, to='associates.OrganizationUser'),
        ),
    ]
