import base64
import logging
from io import BytesIO

import gc
import requests
from django.core.files.base import ContentFile
from django.http import HttpResponse, FileResponse
from django.template.loader import get_template
from django.utils.timezone import now
from pdf2image import convert_from_bytes
from weasyprint import HTML

from common.admincontext import dashboard_context
from fieldcalendar.models import FieldEvent


def convert_pdf_to_img(request, pdf_bytes: bytes):
    imgs = []
    images = convert_from_bytes(pdf_bytes)
    for img in images:
        buffered = BytesIO()
        img.save(buffered, format="PNG")
        buffered.seek(0)
        data_uri = base64.b64encode(buffered.read()).decode('ascii')
        imgs.append(data_uri)
        buffered.close()
        img.close()
    gc.collect()
    return imgs


def render_pdf_as_html(request, pdf_bytes: bytes):
    html = """<!DOCTYPE html><html>
                <style>
                img {
                max-width: 90%;
                max-height: 90%;
            }
            @page {
                size: 8.5in 11in;   /* auto is the initial value */
                /* this affects the margin in the printer settings */
                margin: 10mm 10mm 10mm 10mm;
            }
                </style>
                <body>
                <h3>Plat File Attachment</h3>

    """
    imgs = convert_pdf_to_img(request, pdf_bytes)
    for img in imgs:
        html += '<div><img alt="Plat File" src="data:image/png;charset=utf-8;base64, {}"></div>'.format(img)
    html += '</body></html>'
    return html


def render_img_as_html(input_path):
    html = '<!DOCTYPE html><html><body><img src="..{}" alt="Plat File" height="800" width="480"></body></html>'.format(
        input_path)
    return html


def render_to_pdf(context, **kwargs):
    """ https://docs.djangoproject.com/en/dev/howto/outputting-pdf/ """
    context_data = context
    request = kwargs.pop('request')
    order = context.get('object')
    savenew = False
    pdf_file = None
    if request.GET.get('cached') == "1":
        try:
            fe = FieldEvent.objects.filter(field=order.field,
                                           file__icontains=f"{order.order_number}_report").latest('pk')
            if request.GET.get('download'):
                response = FileResponse(fe.file, as_attachment=True)
                #response['Content-Disposition'] = f"inline; filename={fe.file.name}"
                return response

            return HttpResponse(fe.file, content_type='application/pdf')
        except Exception:
            savenew = True
    savenew = savenew or request.GET.get('save') == "1"
    ## Now we generate the report.


    logger = logging.getLogger('weasyprint')
    logger.addHandler(logging.FileHandler('/tmp/weasyprint.log'))

    context_data = {**context_data, **dashboard_context(request)}
    context_data['pdf'] = True

    template_file = kwargs.pop("template")

    template = get_template(template_file)
    rendered_template = template.render(context_data)

    main_document = HTML(string=rendered_template, base_url=request.build_absolute_uri()).render()

    documents = [main_document]
    if context.get('plat_file'):
        plat_path = context['plat_file'].url
        if plat_path[-3:] == 'pdf':
            plat_html_cache_fe = FieldEvent.objects.get_platmap_image(field=order.field)
            if not plat_html_cache_fe:
                plat_pdf_bytes = context.get("plat_file").read()
                plat_doc_html = render_pdf_as_html(request, plat_pdf_bytes)
                FieldEvent.objects.create_platmap_image(field=order.field, file=plat_doc_html)
            else:
                plat_doc_html = plat_html_cache_fe.file.read().decode()
            plat_doc = HTML(string=plat_doc_html).render()
            documents.append(plat_doc)

    all_pages = [p for doc in documents for p in doc.pages]
    pdf_file = documents[0].copy(all_pages).write_pdf()  # stylesheets=[
    # CSS(settings.STATIC_ROOT + '/assets/plugins/bootstrap/bootstrap.min.css'),
    # CSS(settings.STATIC_ROOT + '/assets/css/work_order.css'),
    # CSS(settings.STATIC_ROOT + '/assets/css/seipkon_print.css'),
    # ])

    # Let's see if we need to save the report
    if savenew:
        f = ContentFile(pdf_file, name=f"{order.order_number}_report.pdf")
        fe = FieldEvent.objects.create_journal_entry(field=order.field, title="Report Generated",
                                                     entry=f"Order {order.order_number}",
                                                     start=now(), end=now(), allDay=False,
                                                     editable=False)
        fe.details['order'] = order.id
        fe.file.save(f"reports/{order.order_number}_report.pdf", f)

    if request.GET.get('download'):
        response = FileResponse(fe.file, as_attachment=True)
        # response['Content-Disposition'] = f"inline; filename={fe.file.name}"
        return response

    else:
        response = HttpResponse(pdf_file, content_type='application/pdf')
        try:
            filename = f"{context.get('object').order_number}.pdf"
        except AttributeError:
            filename = "Report.pdf"
        response['Content-Disposition'] = f"filename={filename}"

        return response

#
# def render_pdf_to_jpg(pdffile):
#     pages = convert_from_path(pdffile, 500)
#
#     for page in pages:
#         page.save('out.jpg', 'JPEG')
#
# def retrieve_image_bytes():
#     resp = api.request_resource(image, bands=bands, clipped=True, width=-1, resourceVariation=8)
#     if resp.status_code != 200:
#         return None
#     img_bytes = api.get_or_set_cache_image(resp, settings.CACHE_TIMEOUT_HIGH)
#     # print(get_image_filename(image))
#     # print(resp.content)
#     # img_bytes = api.get_cache_image_with_filename(get_image_filename(image))
#     if img_bytes:
#         nparr = np.frombuffer(img_bytes, np.uint8)
#         return cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
#     return None
#
#
# """
# 1. get file
# 2.
# """
