from django.test import TestCase

from associates.models import Organization
from associates.models import OrganizationUser
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company
from customnotes.models import CustomField
from fieldcalendar.models import FieldEvent
from orders.models import OrderStatus, Order
from products.models import Product, Discount, Category


# Create your tests here.

class OrderTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()

    def test_add_order_status(self):
        status = OrderStatus.objects.create(name="Submitted")
        self.assertEqual(str(status), "Submitted")

    def test_add_order(self):
        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        cat, created = Category.objects.get_or_create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N", category=cat, created_by=ouser.organization)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        self.assertEqual(str(o.totalUSD), "$90.00")
        self.assertEqual(o.field_name, "Test field")
        self.assertEqual(o.get_products, "FertiSaver-N")

        fe = FieldEvent.objects.last()
        self.assertIn('new', fe.details['title'])
        self.assertEqual(o.get_absolute_url(), fe.details['custom_fields'][0]['value'])

    def test_add_order_no_created_by(self):
        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            lab_processor=ouser.organization,
            order_status=status
        )

        self.assertNotEqual(o.pk, None)
        self.assertIsNone(o.created_by)
        self.assertEqual(str(o), "123ABC@#$")

    def test_add_order_with_no_quantity(self):
        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        cat, created = Category.objects.get_or_create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N", category=cat, created_by=ouser.organization)

        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        self.assertEqual(str(o.totalUSD), "$90.00")
        self.assertEqual(o.field_name, "Test field")
        self.assertEqual(o.get_products, "FertiSaver-N")

        qty = o.get_quantity_for_product(p)
        self.assertEqual(qty, 1)

        price = o.subtotal_for_product(p)
        self.assertEqual(price.amount, 90)

        fe = FieldEvent.objects.last()
        self.assertIn('new', fe.details['title'])
        self.assertEqual(o.get_absolute_url(), fe.details['custom_fields'][0]['value'])

    def test_add_order_with_quantity(self):
        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        cat, created = Category.objects.get_or_create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N", category=cat, created_by=ouser.organization)

        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

        o.products.add(p)

        self.assertEqual(str(o.totalUSD), "$90.00")
        self.assertEqual(o.field_name, "Test field")
        self.assertEqual(o.get_products, "FertiSaver-N")

        cfqty = CustomField.objects.create(content_object=p,
                                           name="Quantity")

        addl_data = {
            str(p.id): {
                str(cfqty.pk): "10"
            }
        }

        o.additional_data = addl_data
        o.save()

        qty = o.get_quantity_for_product(p)
        self.assertEqual(qty, 10)

        price = o.subtotal_for_product(p)
        self.assertEqual(price.amount, 900)

        fe = FieldEvent.objects.last()
        self.assertIn('new', fe.details['title'])
        self.assertEqual(o.get_absolute_url(), fe.details['custom_fields'][0]['value'])

    def test_modify_order(self):
        ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=ouser,
            lab_processor=ouser.organization,
            order_status=status
        )

        ss, _ = OrderStatus.objects.get_or_create(name="modified")
        o.order_status = ss
        self.assertIn('order_status', o.changed_fields)
        o.save()

        fe = FieldEvent.objects.latest('pk')
        self.assertIn('modified', fe.details['title'])
        self.assertIn("Entry", fe.details['custom_fields'][0]['name'])
