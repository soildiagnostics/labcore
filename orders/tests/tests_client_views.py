import json

# from django.contrib.auth.models import User
from dbtemplates.models import Template as DBTemplate
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse

from associates.models import OrganizationUser, Organization, Role
from clients.models import Client, Farm, Field, HallPass
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company, Person, EmailModel
from contact.models import User
from contact.tests.tests_models import ContactTestCase
from orders.models import Order, OrderStatus
from products.models import Product


class OrderByClientsTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()
        self.client = TestClient()
        self.user = User.objects.get(username="testinguser")
        self.client.force_login(self.user)
        self.ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new", allow_third_party_view=True)
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status
        )
        self.o2 = Order.objects.create(
            order_number="asdf",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status,
            parent_order=o
        )
        p = Product.objects.create(name="test product", created_by=self.ouser.organization)
        self.o2.products.add(p)

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        DBTemplate.objects.create(name="products/misc/plat_identification.html")
        DBTemplate.objects.create(name="orders/reports/custom_report_css.html")

    def test_client_can_view_orders(self):
        theclient = self.field.farm.client
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_view_orders_by_different_dealer(self):
        ## Create new dealer and set it as the new client originator
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        newdlr, _ = Organization.objects.create_dealership(company=c)

        theclient = self.field.farm.client
        newouser, c = OrganizationUser.objects.get_or_create(user=u, organization=newdlr)
        theclient.originator = newouser  ## Change the client originator
        theclient.save()
        theclient.refresh_from_db()

        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_view_orders_created_by_different_dealer(self):
        ## Create new dealer and set it as the new client originator
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        newdlr, _ = Organization.objects.create_dealership(company=c)

        theclient = self.field.farm.client
        newouser, c = OrganizationUser.objects.get_or_create(user=u, organization=newdlr)
        self.o2.created_by = newouser
        self.o2.save()
        self.o2.refresh_from_db()

        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_create_order_get(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        Organization.objects.set_primary(theclient.originator.organization)
        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_create_order_post(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        Organization.objects.set_primary(theclient.originator.organization)
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)

        os, _ = OrderStatus.objects.get_or_create(name="new")
        post_data = {
            'order_number': "test order",
            "easy_product": "",
            "order_status": os.pk,
            "additional_data": b""
        }
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        post_data = {
            'order_number': "test order2",
            'additional_data': json.dumps(add_data),
            "easy_product": "",
            "order_status": os.pk
        }
        #
        old_pk = Order.objects.latest("pk").pk

        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        new_order = Order.objects.latest("pk")
        self.assertEqual(new_order.order_number, str(old_pk + 1))
        resp = self.client.get(reverse("order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_create_reorder_get(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        Organization.objects.set_primary(theclient.originator.organization)
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        resp = self.client.get(reverse("order_reorder", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_create_reorder_post(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        Organization.objects.set_primary(theclient.originator.organization)
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        os, _ = OrderStatus.objects.get_or_create(name="new")

        post_data = {
            'order_number': "test order2",
            'additional_data': json.dumps(add_data),
            "parent_order": self.o2.pk,
            "easy_product": "",
            "order_status": os.pk
        }

        old_pk = Order.objects.latest("pk").pk

        resp = self.client.post(reverse("order_reorder", kwargs={'pk': self.o2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        new_order = Order.objects.latest("pk")
        self.assertEqual(new_order.order_number, str(old_pk + 1))
        resp = self.client.get(reverse("order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_update_order_get(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        resp = self.client.get(reverse("update_order", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_update_order_post(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)
        theclient = self.field.farm.client
        clt_person = Person.objects.get(id=theclient.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }

        post_data = {
            'order_number': self.o2.order_number,
            'additional_data': json.dumps(add_data),
            "easy_product": "",
            "order_status": self.o2.order_status.pk
        }

        old_pk = Order.objects.latest("pk").pk

        resp = self.client.post(reverse("update_order", kwargs={'pk': self.o2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        new_order = Order.objects.latest("pk")
        self.assertEqual(new_order.order_number, str(old_pk))
        resp = self.client.get(reverse("order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': new_order.pk}))
        self.assertEqual(resp.status_code, 200)


class OrderByHallpassPermissionsTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.superuser = User.objects.create_superuser('mysuperuser', 'myemail@test.com', 'password')
        self.staffuser = User.objects.create_user('mystaffuser')
        self.staffuser.is_staff = True
        self.staffuser.is_active = True
        self.staffuser.save()

        cprimary = Company.objects.create(name="Primary company")
        b, created = Organization.objects.create_dealership(
            company=cprimary
        )
        b.roles.add(Role.objects.get(role="Primary"))

        p = Person.objects.get(user=self.superuser)
        p.company = cprimary
        p.is_company_contact = True
        p.save()

        p = Person.objects.get(user=self.staffuser)
        p.company = cprimary
        p.save()
        l = b.add_company_members_to_organization()
        self.assertEqual(len(l), 2)

        ## Create two companies

        ## Company 1
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)
        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        e = EmailModel.objects.create(email="test@example.com")
        e.contact = m
        e.save()
        m.save()
        ### Create clients farms fields for each company
        self.orguser1 = OrganizationUser.objects.get(user__username="testinguser")
        self.client1 = Client.objects.create(contact=m,
                                             originator=self.orguser1)
        self.farm1 = Farm.objects.create(name="Farm1", client=self.client1)
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.field1 = Field.objects.create(name="Field1",
                                           farm=self.farm1,
                                           boundary=mp)

        ## For company 2
        c2 = Company.objects.create(name="Another company")
        b, created = Organization.objects.create_dealership(
            company=c2
        )
        u2 = User.objects.create_user(username="seconduser")
        p = Person.objects.get(user=u2)
        p.company = c2
        p.is_company_contact = True
        p.save()
        l = b.add_company_members_to_organization()
        self.assertEqual(len(l), 1)

        ### Create clients farms fields for each company
        self.orguser2 = OrganizationUser.objects.get(user__username="seconduser")
        m2 = Person.objects.create(name="Jessie",
                                   last_name="Bhalerao")
        self.client2 = Client.objects.create(contact=m2,
                                             originator=self.orguser2)
        self.farm2 = Farm.objects.create(name="Farm2", client=self.client2)
        mp = GEOSGeometry(
            'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        self.field2 = Field.objects.create(name="Field2",
                                           farm=self.farm2,
                                           boundary=mp)

        self.client1pages = [
            reverse('client_detail', kwargs={'pk': self.client1.pk}),
            reverse('farm_delete', kwargs={'pk': self.farm1.pk}),
            reverse('field_detail', kwargs={'pk': self.field1.pk}),
            reverse('field_geo_details', kwargs={'pk': self.field1.pk})
        ]

        self.client2pages = [
            reverse('client_detail', kwargs={'pk': self.client2.pk}),
            reverse('farm_delete', kwargs={'pk': self.farm2.pk}),
            reverse('field_detail', kwargs={'pk': self.field2.pk}),
            reverse('field_geo_details', kwargs={'pk': self.field2.pk})
        ]
        # allow client1 to view field2
        ## First we create a user for the contact.
        clt1user = self.client1.contact.get_or_create_user_from_person(username='client1', is_active=True)

        hp = HallPass.objects.create(field=self.field2, user=clt1user, created_by=self.staffuser)

        Product.objects.create(name="test product", created_by=a)

        tc = TestClient()
        tc.force_login(clt1user)

        for view in self.client2pages[0:2]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 403)

        for view in self.client2pages[2:]:
            response = tc.get(view)
            self.assertEqual(response.status_code, 200)

        self.client = tc

    def test_client_can_create_order_get_with_hallpass(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)

        self.assertNotEqual(self.field1.farm.client, self.field2.farm.client)

        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_create_order_post_with_hallpass(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)

        self.assertNotEqual(self.field1.farm.client, self.field2.farm.client)

        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field2.pk}))
        self.assertEqual(resp.status_code, 200)

        os, _ = OrderStatus.objects.get_or_create(name="new")

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        post_data = {
            'order_number': "test order2",
            'additional_data': json.dumps(add_data),
            "easy_product": "",
            "order_status": os.pk
        }

        #
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        o = Order.objects.latest('pk')
        self.assertIsNone(o.created_by)

        resp = self.client.get(reverse("order_detail", kwargs={'pk': o.pk}))
        self.assertEqual(resp.status_code, 200)

        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="<html></html>")

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': o.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_client_can_update_order_post_with_hallpass(self):
        self.assertTrue(settings.CLIENT_CAN_PLACE_ORDERS)

        self.assertNotEqual(self.field1.farm.client, self.field2.farm.client)

        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field2.pk}))
        self.assertEqual(resp.status_code, 200)

        os, _ = OrderStatus.objects.get_or_create(name="new")

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        post_data = {
            'order_number': "test order2",
            'additional_data': json.dumps(add_data),
            "order_status": os.pk
        }
        #
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        o = Order.objects.latest('pk')
        self.assertIsNone(o.created_by)

        resp = self.client.post(reverse("update_order", kwargs={'pk': o.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

