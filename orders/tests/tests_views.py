import json

# from django.contrib.auth.models import User
from unittest import skip

from dbtemplates.models import Template as DBTemplate
from django.conf import settings
from django.core.files.base import ContentFile
from django.test import Client as TestClient, LiveServerTestCase
from django.test import TestCase
from django.urls import reverse

from associates.models import OrganizationUser, Organization, Role
from clients.tests.tests_models import ClientsTestCase
from contact.models import Company, Person
from contact.models import User
from fieldcalendar.event_base import CalendarEvent
from fieldcalendar.models import FieldEvent
from orders.models import Order, OrderStatus
from products.models import Product


class OrderTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()
        self.client = TestClient()
        self.user = User.objects.get(username="testinguser")
        self.client.force_login(self.user)
        self.ouser = OrganizationUser.objects.get(user__username="testinguser")
        status, created = OrderStatus.objects.get_or_create(name="new", allow_third_party_view=True)
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status
        )
        self.o2 = Order.objects.create(
            order_number="asdf",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status,
            parent_order=o
        )
        p = Product.objects.create(name="test product", created_by=self.ouser.organization)
        self.o2.products.add(p)

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")
        DBTemplate.objects.create(name="products/misc/plat_identification.html")
        DBTemplate.objects.create(name="orders/reports/custom_report_css.html")

    @skip
    def test_order_page(self):
        resp = self.client.get(reverse("order_list"))
        self.assertEqual(resp.status_code, 200)

    def test_order_table_json(self):
        default_url = "/orders/data/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=order_number&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=order_status&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=client&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=farm&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=field&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=date_created&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=date_modified&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=parent_order&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=8&columns%5B8%5D%5Bname%5D=products&columns%5B8%5D%5Bsearchable%5D=false&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=5&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1538418970441"
        search_url = "/orders/data/?draw=3&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=order_number&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=order_status&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=client&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=farm&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=field&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=date_created&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=date_modified&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=parent_order&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=8&columns%5B8%5D%5Bname%5D=products&columns%5B8%5D%5Bsearchable%5D=false&columns%5B8%5D%5Borderable%5D=false&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=5&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=sm&search%5Bregex%5D=false&_=1538418595575"

        resp = self.client.get(default_url)
        self.assertEqual(resp.status_code, 200)
        resp = self.client.get(search_url)
        self.assertEqual(resp.status_code, 200)

        with self.assertNumQueries(10):
            self.client.get(search_url)

    def test_order_detail_view(self):
        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_order_detail_pdf(self):
        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}) + "?format=pdf")
        self.assertEqual(resp.status_code, 200)

    def test_workorder_detail_view(self):
        # file = SimpleUploadedFile(name='platfile.pdf',
        #                           content=open('orders/tests/sampleworkorder.pdf', 'rb').read(),
        #                           content_type='application/pdf')
        #
        # self.o2.field.plat_file = file
        # self.o2.field.save()
        with open("orders/templates/orders/work_order_detail.html", "r") as f:
            template = f.read()
#
        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content=template)

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_workorder_platmap_pdf_conversion(self):
        with open("orders/templates/orders/work_order_detail.html", "r") as f:
            template = f.read()
        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content=template)

        with open("orders/tests/sampleworkorder.pdf", "rb") as f:
            self.o2.field.plat_file.save("plat.pdf", f)
            self.o2.field.save()
        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        ## A png cache image is created
        self.assertIsNotNone(FieldEvent.objects.get_platmap_image(self.o2.field))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        ## PNG cache is used

        import shutil
        from django.conf import settings
        folder = settings.MEDIA_ROOT + "/platfiles/"
        shutil.rmtree(folder)
        folder = settings.MEDIA_ROOT + "/files/"
        shutil.rmtree(folder)

    def test_workorder_visible_to_sampler(self):
        c = Company.objects.create(name="SamplerOrg")
        u = User.objects.create_user(username="sampler",
                                     first_name="Danny",
                                     last_name="Sampler",
                                     email="yetanother@example.com",
                                     password="ReadMySecrets")
        u.person.company = c
        u.person.save()
        sorg = Organization.objects.create(
            name="SamplerOrg",
            company=c,
        )
        sr, _ = Role.objects.get_or_create(role="Sampler")
        sorg.roles.add(sr)
        sorg.save()

        sorg.add_person_to_organization(u.person)
        self.assertEqual(sorg.member_count, 1)
        self.o2.sampler = sorg.organization_users.first()
        self.o2.save()

        self.assertEqual(self.o2.sampler.organization, sorg)

        self.client.force_login(u)
        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="""template""")
        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_workorder_not_visible_to_sampler(self):
        c = Company.objects.create(name="SamplerOrg")
        u = User.objects.create_user(username="sampler",
                                     first_name="Danny",
                                     last_name="Sampler",
                                     email="yetanother@example.com",
                                     password="ReadMySecrets")
        u.person.company = c
        u.person.save()
        sorg = Organization.objects.create(
            name="SamplerOrg",
            company=c,
        )
        sr, _ = Role.objects.get_or_create(role="Sampler")
        sorg.roles.add(sr)
        sorg.save()

        sorg.add_person_to_organization(u.person)
        self.assertEqual(sorg.member_count, 1)
        self.o2.sampler = sorg.organization_users.first()
        self.o2.save()
        self.o2.order_status.allow_third_party_view = False
        self.o2.order_status.save()

        self.assertEqual(self.o2.sampler.organization, sorg)

        self.client.force_login(u)
        DBTemplate.objects.create(name="orders/work_order_detail_db.html",
                                  content="""template""")
        resp = self.client.get(reverse("work_order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 403)
        self.assertIn(b"<h4>Permission Denied</h4>", resp.content)

    def test_tableassignment_visible_to_sampler(self):
        c = Company.objects.create(name="SamplerOrg")
        u = User.objects.create_user(username="sampler",
                                     first_name="Danny",
                                     last_name="Sampler",
                                     email="yetanother@example.com",
                                     password="ReadMySecrets")
        u.person.company = c
        u.person.save()
        sorg = Organization.objects.create(
            name="SamplerOrg",
            company=c,
        )
        sr, _ = Role.objects.get_or_create(role="Sampler")
        sorg.roles.add(sr)
        sorg.save()

        sorg.add_person_to_organization(u.person)
        self.assertEqual(sorg.member_count, 1)
        self.o2.sampler = sorg.organization_users.first()
        self.o2.save()

        self.assertEqual(self.o2.sampler.organization, sorg)

        self.client.force_login(u)

        resp = self.client.get(reverse("sampler_list_json"))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['recordsTotal'], 1)

    def test_tableassignment_not_visible_to_sampler(self):
        c = Company.objects.create(name="SamplerOrg")
        u = User.objects.create_user(username="sampler",
                                     first_name="Danny",
                                     last_name="Sampler",
                                     email="yetanother@example.com",
                                     password="ReadMySecrets")
        u.person.company = c
        u.person.save()
        sorg = Organization.objects.create(
            name="SamplerOrg",
            company=c,
        )
        sr, _ = Role.objects.get_or_create(role="Sampler")
        sorg.roles.add(sr)
        sorg.save()

        sorg.add_person_to_organization(u.person)
        self.assertEqual(sorg.member_count, 1)
        self.o2.sampler = sorg.organization_users.first()
        self.o2.save()
        self.o2.order_status.allow_third_party_view = False
        self.o2.order_status.save()

        self.assertEqual(self.o2.sampler.organization, sorg)

        self.client.force_login(u)

        resp = self.client.get(reverse("sampler_list_json"))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['recordsTotal'], 0)

    def test_order_detail_view_wrong_org(self):
        c = Company.objects.create(name="SoilDx")
        u = User.objects.create_user(username="anotheruser",
                                     first_name="newguy",
                                     last_name="newlast",
                                     email="newuser@test.com",
                                     password="nothingsecret")

        u.person.company = c
        u.person.save()
        Organization.objects.create_dealership(company=c)
        self.client.force_login(u)

        resp = self.client.get(reverse("order_detail", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 403)

    def test_create_order_get_view(self):
        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        resp = self.client.get(reverse("new_order", kwargs={'pk': self.field.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_create_order_post_view(self):
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)
        os, _ = OrderStatus.objects.get_or_create(name="new")
        post_data = {
            'order_number': "test order",
            "created_by": self.ouser.pk,
            "lab_processor": lporg.pk,
            "order_status": os.pk
        }
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        post_data = {
            'order_number': "test order2",
            "created_by": self.ouser.pk,
            "lab_processor": lporg.pk,
            'additional_data': json.dumps(add_data),
            "order_status": os.pk
        }
        #
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

    def test_create_order_post_view_nocreated(self):
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)
        os, _ = OrderStatus.objects.get_or_create(name="new")
        post_data = {
            'order_number': "test order",
            "lab_processor": lporg.pk,
            "order_status": os.pk
        }
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        post_data = {
            'order_number': "test order2",
            "lab_processor": lporg.pk,
            'additional_data': json.dumps(add_data),
            "order_status": os.pk
        }
        #
        resp = self.client.post(reverse("new_order", kwargs={'pk': self.field.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

        o = Order.objects.latest('pk')
        self.assertIsNone(o.created_by)

    def test_create_reorder_get(self):
        resp = self.client.get(reverse("order_reorder", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_create_reorder_post(self):
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }
        os, _ = OrderStatus.objects.get_or_create(name="new")

        post_data = {
            'order_number': "test order2",
            "created_by": self.ouser.pk,
            "lab_processor": lporg.pk,
            'additional_data': json.dumps(add_data),
            "parent_order": self.o2.pk,
            "order_status": os.pk
        }

        resp = self.client.post(reverse("order_reorder", kwargs={'pk': self.o2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

    def test_update_order_get(self):
        resp = self.client.get(reverse("update_order", kwargs={'pk': self.o2.pk}))
        self.assertEqual(resp.status_code, 200)

    def test_update_order_post(self):
        lp = Role.objects.get(role="Lab Processor")
        lporg, created = Organization.objects.get_or_create(name=settings.DEFAULT_LAB_PROCESSOR,
                                                            company=Company.objects.create(
                                                                name=settings.DEFAULT_LAB_PROCESSOR))
        lporg.roles.add(lp)

        pid = Product.objects.get(name="test product").pk
        add_data = {
            pid: {}
        }

        post_data = {
            'order_number': self.o2.order_number,
            "created_by": self.o2.created_by.id,
            "lab_processor": lporg.pk,
            'additional_data': json.dumps(add_data),
            "order_status": self.o2.order_status.pk
        }

        resp = self.client.post(reverse("update_order", kwargs={'pk': self.o2.pk}), data=post_data)
        self.assertEqual(resp.status_code, 302)

    def test_sampling_list(self):
        self.o2.sampler = self.o2.created_by
        self.o2.save()
        self.o2.order_status.allow_third_party_view = False
        self.o2.order_status.save()

        url = "/orders/sampling/?draw=3&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=order_number&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=order_status&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=client&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=farm&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=field&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=date_created&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=date_modified&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=5&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=det&search%5Bregex%5D=false&_=1552928321266"
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.json()["data"]), 0)

        url = "/orders/sampling/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=order_number&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=order_status&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=client&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=farm&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=field&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=date_created&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=date_modified&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=5&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1552928321264"
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.json()["data"]), 0)

        os, _ = OrderStatus.objects.get_or_create(name="ready to sample", allow_third_party_view=True)
        os2, _ = OrderStatus.objects.get_or_create(name="sampled", allow_third_party_view=True)
        os.succeeded_by = os2
        os.save()

        self.o2.order_status = os
        self.o2.save()

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()["recordsTotal"], 1)
        self.assertEqual(len(resp.json()["data"]), 1)

    def test_sampling_map_available_to_sampler(self):
        self.o2.sampler = self.o2.created_by
        self.o2.save()
        self.o2.order_status.allow_third_party_view = False
        self.o2.order_status.save()
        url = "/orders/sampling/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=order_number&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=order_status&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=client&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=farm&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=field&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=date_created&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=date_modified&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=5&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1552928321264"
        os, _ = OrderStatus.objects.get_or_create(name="ready to sample", allow_third_party_view=True)
        os2, _ = OrderStatus.objects.get_or_create(name="sampled", allow_third_party_view=True)
        os.succeeded_by = os2
        os.save()
        self.o2.order_status = os
        self.o2.save()
        import django.utils.timezone
        fe = FieldEvent.objects.create(
            field=self.o2.field,
            event_type=CalendarEvent.objects.get(name="Sampling Locations"),
            file=ContentFile("some content"),
            start=django.utils.timezone.now()
        )

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()["recordsTotal"], 1)
        self.assertEqual(len(resp.json()["data"]), 1)
        self.assertTrue(f"/fieldevents/{fe.pk}/download/" in resp.json()['data'][0][0])

