import json
import os

from django.contrib.contenttypes.models import ContentType
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from rest_framework.renderers import JSONRenderer

# from django.contrib.auth.models import User
from associates.models import OrganizationUser, Role
from clients.tests.tests_models import ClientsTestCase
from contact.models import User
from customnotes.models import CustomField
from orders.models import Order, OrderStatus
from orders.serializers import OrderTransmitSerializer
from orders.soildx_helper import get_soildx_rec
from products.models import Product
from svx.models import SVXParticipant


class OrderAPITestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()
        self.client = TestClient()
        self.user = User.objects.get(username="testinguser")
        self.client.force_login(self.user)
        self.ouser = OrganizationUser.objects.get(user__username="testinguser")
        role, created = Role.objects.get_or_create(role="Primary")
        self.ouser.organization.roles.add(role)
        self.ouser.organization.save()

        SVXParticipant.objects.create(organization=self.ouser.organization,
                                      base_url="http://example.com")
        status, created = OrderStatus.objects.get_or_create(name="new")
        o = Order.objects.create(
            order_number="123ABC@#$",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status
        )
        self.o2 = Order.objects.create(
            order_number="asdf",
            field=self.field,
            created_by=self.ouser,
            lab_processor=self.ouser.organization,
            order_status=status,
            parent_order=o,
            additional_data=dict()
        )
        p = Product.objects.create(name="test product", created_by=self.ouser.organization)
        self.o2.products.add(p)

        p = Product.objects.create(name="test product2", created_by=self.ouser.organization)
        self.o2.products.add(p)
        self.p2id = p.id

        ct = ContentType.objects.get_for_model(Product)

        cf = CustomField.objects.create(content_type=ct,
                                        object_id=p.id,
                                        name="additional question",
                                        help_text="Help text")
        self.cfid = cf.id

        cfstring = {
            p.id: {
                cf.id: "Some answer"
            }
        }
        self.o2.additional_data = cfstring
        self.o2.save()

        self.assertNotEqual(o.pk, None)
        self.assertEqual(str(o), "123ABC@#$")

    def test_order_list_api(self):
        url = reverse("api_order_list")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_order_create_api(self):
        url = reverse("api_order_transmit")
        self.assertEqual(Order.objects.count(), 2)
        data = JSONRenderer().render(OrderTransmitSerializer(self.o2).data)
        response = self.client.post(url, data, content_type="application/json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 3)

        ad = json.loads(response.content)['additional_data']

        self.assertEqual(ad[str(self.p2id)][str(self.cfid)], "Some answer")

    def test_order_send_files(self):

        url = reverse("api_order_transmit")
        self.assertEqual(Order.objects.count(), 2)

        file = SimpleUploadedFile(name='platfile.pdf',
                                  content=open('orders/tests/sampleworkorder.pdf', 'rb').read(),
                                  content_type='application/pdf')

        self.o2.field.plat_file = file
        self.o2.field.save()

        data = OrderTransmitSerializer(self.o2).data
        data = JSONRenderer().render(data=data)

        response = self.client.post(url, data, content_type="application/json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 3)

        content = json.loads(response.content)
        ad = content['additional_data']

        self.assertEqual(ad[str(self.p2id)][str(self.cfid)], "Some answer")
        order = Order.objects.get(id=content['id'])
        field = order.field

        self.assertNotEqual(self.o2.field.pk, field.pk) ## new field should be saved
        self.assertNotEqual(self.o2.field.plat_file.url, field.plat_file.url) ##  new plat file


    def test_soildx_connector(self):
        rec = get_soildx_rec('27667')
        self.assertEqual(rec['id'], 53)

        rec = get_soildx_rec('432432')
        self.assertIsNone(rec)

    def tearDown(self):
        try:
            for fname in os.listdir("media/platfiles/"):
                if fname.startswith("platfile_"):
                    os.remove(os.path.join("media/platfiles/", fname))
        except FileNotFoundError:
            pass
