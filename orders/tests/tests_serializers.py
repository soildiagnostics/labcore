import json
import timeit

from django.db.models import Prefetch

from clients.models import Field
from .tests_views import OrderTestCase
from django.test import TestCase
from orders.serializers import OrderSerializer, OrderSummarySerializer, FieldOrderSerializer, \
    field_order_serializer
from ..models import Order


class SerializerTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        OrderTestCase().setUp()

    def test_order_list_serializer(self):
        ord = Order.objects.first()
        ser = OrderSerializer(ord).data
        self.assertEqual(ord.pk, ser.get("id"))
        self.assertEqual(ord.field.pk, ser.get("field").get("id"))

    def test_order_summary_serializer(self):
        ord = Order.objects.first()
        ser = OrderSummarySerializer(ord).data
        self.assertEqual(ord.pk, ser.get("id"))
        self.assertEqual(ord.field.pk, ser.get("field"))

    def test_field_order_summary_serializer(self):
        fields = Field.objects.all().prefetch_related(
            Prefetch("orders",
                     queryset=Order.objects.all().prefetch_related("order_status", "products"),
                     to_attr="recent_orders"))
        ser = FieldOrderSerializer(fields, many=True).data
        self.assertEqual(len(ser), 1)
        self.assertEqual(type(json.loads(ser[0].get("boundary"))), type(dict()))

    def test_core_geojson_serializer(self):
        fields = Field.objects.all().prefetch_related(
            Prefetch("orders",
                     queryset=Order.objects.all().prefetch_related("order_status", "products"),
                     to_attr="recent_orders"))
        ser = json.loads(field_order_serializer(fields))

        self.assertEqual(ser.get("type"), "FeatureCollection")
        self.assertEqual(len(ser['features']), 1)
