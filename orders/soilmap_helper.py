import shutil

from associates.models import OrganizationUser
from clients.models import SubfieldLayer
from formulas.models import Formula
from orders.models import Order
from samples.models import LabMeasurement, Sample
from products.models import TestParameter
from django.contrib import messages
from fieldcalendar.models import FieldEvent
from django.utils.timezone import now
import uuid

headings = [
    ("Layer ID", None, None),
    ("Order #", None, None),
    ("Lab ID", None, None),
    ("Sample #", None, None),
    ("pH water", "pH Water", "units"),
    ("pH Buffer", "pH Buffer", "units"),
    ("LOI %", "LOI", "%"),
    ("OM cc", "OM", "cc"),
    ("ISNT ppm", "Organic N", "ppm"),
    ("NRec #/a", "Nitrogen", "lb/ac"),
    ("P#/a", "P", "lb/ac"),
    ("K#/a", "K", "lb/ac"),
    ("Ca#/a", "Ca", "lb/ac"),
    ("Mg#/a", "Mg", "lb/ac"),
    ("CEC meq/100g", "CEC", "meq/100g"),
    ("%K", "K", "% base saturation"),
    ("%Ca", "Ca", "% base saturation"),
    ("%Mg", "Mg", "% base saturation"),
    ("%H", "H", "% base saturation"),
    ("S ppm", "S", "ppm"),
    ("Zn ppm", "Zn", "ppm"),
    ("Mn ppm", "Mn", "ppm"),
    ("Fe ppm", "Fe", "ppm"),
    ("Cu ppm", "Cu", "ppm"),
    ("B ppm", "B", "ppm"),
    ("Na ppm", "Na", "ppm")
]


def get_tp_or_none(name, unit):
    try:
        return TestParameter.objects.get(name=name, unit__unit=unit)
    except Exception:
        return ''


def get_lm_or_none(qs, sample, par):
    try:
        return str(round(float(qs.filter(parameter=par, sample=sample).first().value), par.significant_digits))
    except:
        return ''


def make_shapefile(order_id, subfield_id, request):
    order = Order.objects.get(id=order_id)
    sf = SubfieldLayer.objects.get(id=subfield_id)
    try:
        measurements = LabMeasurement.objects.filter(sample__order=order, void=False)
        testparams = TestParameter.objects.filter(id__in=measurements.values_list('parameter', flat=True))
        samples = order.samples.all().order_by('order_serial')

        sf.dbf_field_names = {'field_names': list()}
        sf.dbf_field_values = dict()

        sf.dbf_field_values['LabID'] = [s.pk for s in samples]
        if not "LabID" in sf.dbf_field_names['field_names']:
            sf.dbf_field_names['field_names'].append("LabID")

        strmake = lambda s: s.custom_name if s.custom_name != "" else "-"
        sf.dbf_field_values['Name'] = [strmake(s) for s in samples]
        if not "Name" in sf.dbf_field_names['field_names']:
            sf.dbf_field_names['field_names'].append("Name")

        sf.dbf_field_values['Serial'] = [s.order_serial for s in samples]
        if not "Serial" in sf.dbf_field_names['field_names']:
            sf.dbf_field_names['field_names'].append("Serial")

        for tp in testparams:
            parstring = f"{tp.name}_{tp.unit}"
            lms = measurements.filter(parameter=tp).order_by('sample__order_serial').values_list('value', flat=True)
            sf.dbf_field_values[parstring] = [round(float(l), tp.significant_digits) for l in lms]
            if not parstring in sf.dbf_field_names['field_names']:
                sf.dbf_field_names['field_names'].append(parstring)
        sf.pk = None
        sf.name = f"Order {order} Layer {str(uuid.uuid4())[:8]}"
        folder, zipfile = sf.create_shapefile_from_layers()
        fe = FieldEvent.objects.create_journal_entry(sf.field, "GIS Layer Created",
                                                     "Created soil test shapefile",
                                                     now(), now(), False, False)

        with open(f"{folder}/{zipfile}", 'rb') as f:
            fe.file.save(f"shapefiles/{sf.field.id}/{zipfile}", f)
            fe.save()
            sf.source_file = fe.file

        sf.editable = False
        sf.dbf_field_values["order"] = order.pk
        sf.save()
        try:
            # print("cleaning up " + self.tempdir)
            shutil.rmtree(folder)
        except OSError as e:
            print(f"make_shapefile Error {e.filename}-{e.strerror}")

        messages.info(request, f"Succeeded for Order {order.order_number}, parameters: {[p.name for p in testparams]}")
        return sf

    except Exception as e:
        messages.error(request, f"Failed for Order {order.order_number}, with error {e}")


COMMON_PARLIST = ['SampleID',
                  'pH Water_units',
                  'P_lb/ac',
                  'K_lb/ac',
                  'CEC_meq/100g']


def duplicate_layer(request, src : SubfieldLayer, name, parameters=COMMON_PARLIST, additional_params=list()):
    try:
        lyr_to_copy = SubfieldLayer.objects.get(id=src.id)
        src.pk = None
        src.source_file = ""
        src.name = name
        src.dbf_field_names = {'field_names': list()}
        src.dbf_field_values = dict()
        src.editable = True

        parameters.extend(additional_params)
        parameters = set(parameters)
        for par in parameters:
            if par in lyr_to_copy.dbf_field_names.get('field_names'):
                src.dbf_field_names['field_names'].append(par)
                try:
                    src.dbf_field_values[par] = lyr_to_copy.get_values(par)
                except KeyError:
                    messages.info(request, f"{lyr_to_copy} has no values for parameter {par}. View {par} on the map first")

        src.append_log(f"Duplicate of layer {lyr_to_copy.id}", save=True)
        messages.info(request,
                      f"Layer {lyr_to_copy} duplicated as {src.name} and with values for {src.dbf_field_names.get('field_names')}")
        return src
    except Exception as e:
        messages.error(request, f"Duplication failed for {lyr_to_copy}, {repr(e)}")


def apply_formulas_gis(shapefile, formulalist, request):
    try:
        orguser = OrganizationUser.objects.get(user=request.user)
        formulas = Formula.objects.filter(id__in=formulalist)
        formulas = dependency_sort(formulas)
        for formula in formulas:
            try:
                kw = formula.validate_scalars()
                for key in kw.keys():
                    post_key = f"scalar_{formula.id}_{key}"
                    post_val = request.POST.get(post_key, kw[key])
                    kw[key] = post_val
                formula.evaluate(shapefile, orguser, **kw)
                messages.info(request, f"Formula {formula} applied to shapefile {shapefile}")
            except Exception as e:
                messages.error(request, f"Failed for Shapefile  {shapefile}, formula {formula}, with error {e}")
    except Exception as e:
        messages.error(request, f"Failed for Shapefile  {shapefile}, with error {e}")


def dependency_sort(formulas):
    """
    Sort formulas based on a sequence. A formula who's input depends on the output of another formula
    should be applied after the first formula has been applied
    """
    formulas = list(formulas)
    sorted_list = list()
    while len(formulas) > 0:
        candidate = formulas.pop(0)
        candidate_inputs = candidate.inputs.all().values_list('pk', flat=True)
        rest_outputs = [o.output.pk for o in formulas]
        if len(rest_outputs) > 0 and any([i in rest_outputs for i in candidate_inputs]):
            formulas.append(candidate)
        else:
            sorted_list.append(candidate)
    print(sorted_list)
    return sorted_list


def apply_formulas(order, formulalist, request):
    """
    order: Order id
    formulalist: list for formula id's chosen for the order.
    """
    try:
        orguser = OrganizationUser.objects.get(user=request.user)
        ord = Order.objects.get(id=order)
        formulas = Formula.objects.filter(id__in=formulalist)
        formulas = dependency_sort(formulas)
        for formula in formulas:
            formula.evaluate(ord, orguser)
    except Exception as e:
        messages.error(request, f"Failed for Order {ord.order_number}, with error {e}")
