import base64
import datetime

from django.core.files.base import ContentFile
from django.db.models import Prefetch
from django.utils.timezone import now
from rest_framework import generics, status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from associates.models import Organization
from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin
from clients.models import Client, Farm, Field
from clients.serializers import FieldSerializer
from common.models import ParseTimeMixin
from contact.models import Contact
from products.models import Product
from .models import Order, OrderStatus
from .serializers import OrderTransmitSerializer, OrderSerializer, FieldOrderSerializer


class APIOrderList(DealershipRequiredMixin, generics.ListAPIView):
    serializer_class = OrderSerializer
    model = Order

    def get_queryset(self):
        return Order.objects.filter(created_by__organization=self.org).prefetch_related("products",
                                                                                        "created_by__user__person",
                                                                                        "products__test_parameters",
                                                                                        "products__test_parameters__unit",
                                                                                        "products__package__products")


class APIFieldOrderStatusView(generics.ListAPIView, ParseTimeMixin, FilteredQuerySetMixin, DealershipRequiredMixin):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = FieldOrderSerializer
    model = Field
    instance_owner_organization = "farm__client__originator__organization"
    instance_owner_client = "farm__client"

    def get_queryset(self):
        qs = self.get_initial_queryset()
        qs = qs.filter(boundary__isnull=False)
        extent = self.request.query_params.get("extent", None)
        if extent:
            qs = Field.objects.filter_queryset_with(qs, extent)
        order_qs = Order.objects.all()
        cat = self.request.query_params.get("category")
        if cat != "---":
            order_qs = order_qs.filter(products__category=cat)
        try:
            order_qs = order_qs.filter(date_created__gte=self.parse_with_tz(self.request.query_params.get('start')))
        except TypeError:
            pass
        try:
            order_qs = order_qs.filter(date_created__lte=self.parse_with_tz(self.request.query_params.get('end')))
        except TypeError:
            pass

        return qs.prefetch_related(
            Prefetch("orders",
                     queryset=order_qs.prefetch_related("order_status", "products"),
                     to_attr="recent_orders")
        )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['queryset'] = self.get_queryset()
        return context

class APIOrderTransmit(DealershipRequiredMixin, generics.CreateAPIView):
    serializer_class = OrderTransmitSerializer
    model = Order

    def create(self, request, *args, **kwargs):
        order = self.get_serializer(data=request.data)
        order.is_valid(raise_exception=True)
        me = Organization.objects.me
        contact = Contact.objects.create(name=request.data.get('client'))
        client = Client.objects.create(contact=contact,
                                       originator=self.orguser)
        farm = Farm.objects.create(name=request.data.get('farm'), client=client)
        dfield = order.validated_data.get('field')
        dfield['farm'] = farm.pk
        dfield['id'] = None

        fieldserializer = FieldSerializer(data=dfield)

        if fieldserializer.is_valid(raise_exception=True):
            field = fieldserializer.save()

        if request.data['field']['plat_file']:
            content = base64.b64decode(request.data['field']['plat_file'])
            field.plat_file.save(name="platfile.pdf",
                                 content=ContentFile(content))

        products = [int(i) for i in order.initial_data.get('product_list')]
        products = Product.objects.filter(id__in=products)
        orderstatus, created = OrderStatus.objects.get_or_create(name="New SVX Order")
        neworder = Order.objects.create(order_number="temp",
                                        order_status=orderstatus,
                                        field=field,
                                        created_by=self.orguser,
                                        additional_data=order.validated_data['additional_data'],
                                        lab_processor=me
                                        )
        neworder.order_number = neworder.pk
        for p in products:
            neworder.products.add(p)
        neworder.save()
        newdata = OrderSerializer(neworder).data
        headers = self.get_success_headers(newdata)

        return Response(newdata, status=status.HTTP_201_CREATED, headers=headers)


class APIOrderStatusUpdate(DealershipRequiredMixin, APIView):

    def post(self, request, pk, format=None):
        try:
            order = Order.objects.get(pk=pk)
            desired_status = request.GET.get('status')
            os = OrderStatus.objects.get(pk=desired_status)
            order.order_status = os
            order.save()
            return Response({'status': 'success'}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'status': 'fail', 'exception': repr(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# class APIOrderTaskMarkDone(DealershipRequiredMixin, APIView):
#
#     def post(self, request, pk, product, format=None):
#         try:
#             order = Order.objects.get(pk=pk)
#             product = Product.objects.get(pk=product)
#             assert product in order.products, f"This product ({product}) isn't part of Order {order}"
#             order.create_journal_entry(self.field,
#                                        f"Task: {product}, Order {order}",
#                                        entry=f"{request.user} marked Product {product} as Done on Order {order}")
#             return Response({'status': 'success'}, status=status.HTTP_201_CREATED)
#         except Exception as e:
#             return Response({'status': 'fail', 'exception': repr(e)},
#                             status=status.HTTP_500_INTERNAL_SERVER_ERROR)
