import json

import requests

from products.models import TestParameter, MeasurementUnit
from samples.models import LabMeasurement, Sample


def get_soildx_rec(order_number):
    """
    Creates a get request to soildx.com and brings back the FertiSaver-N recommendation
    Parses it and returns the recommendation directory
    """
    url = f"https://soildx.com/fieldrecs/order/{order_number}/rec/"
    result = requests.get(url, auth=('fieldrec', 'fsn2019rec'))
    if result.status_code == 200 and result.content:
        content = json.loads(result.content)
        return content
    return None


def create_lab_measurements(order, results, user):
    """
    Creates lab measurements for each sample
    """
    buac, _ = MeasurementUnit.objects.get_or_create(unit="bu/ac")
    tpa, _ = MeasurementUnit.objects.get_or_create(unit="t/ac")
    yld, _ = TestParameter.objects.get_or_create(name="Yield Estimate",
                                                 unit=buac, calculated=True)
    lime, _ = TestParameter.objects.get_or_create(name="Lime", unit=tpa,
                                                  calculated=True)
    peracre, _ = MeasurementUnit.objects.get_or_create(unit="/ac")
    seed, _ = TestParameter.objects.get_or_create(name="Seeding rate", unit=peracre,
                                                  calculated=True)

    lbac, _ = MeasurementUnit.objects.get_or_create(unit="lb/ac")
    nrec, _ = TestParameter.objects.get_or_create(name="Nitrogen",
                                                  unit=lbac, calculated=True)

    ppm, _ = MeasurementUnit.objects.get_or_create(unit="ppm")
    fsn, _ = TestParameter.objects.get_or_create(name="Organic N",
                                                 unit=ppm)

    text = json.loads(results['recommendation_text'])

    for presc in text['prescription']:

        try:
            sample = Sample.objects.get(order=order, custom_name=presc['subfield'])
        except Sample.DoesNotExist:
            try:
                sample = Sample.objects.get(order=order, order_serial=presc['subfield'])
            except Sample.DoesNotExist:
                raise


        for par, key in [(yld, 'yield_index'),
                         (lime, 'lime'),
                         (seed, 'seeding_rate'),
                         (nrec, 'nitrogen'),
                         (fsn, 'fsn')]:
            try:
                labm = LabMeasurement.objects.get(sample=sample,
                                                  parameter=par)
            except LabMeasurement.DoesNotExist:
                labm = LabMeasurement(sample=sample,
                                      parameter=par)
            except LabMeasurement.MultipleObjectsReturned:
                set = LabMeasurement.objects.filter(sample=sample,
                                                     parameter=par)
                labm = set.latest('pk')
                rest = set.exclude(id=labm.id)
                rest.update(void=True)
            labm.value = presc.get(key, '0')
            labm.save(orguser=user)

