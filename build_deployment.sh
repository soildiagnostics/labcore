#!/bin/bash
## switch to development
## run all tests

pip freeze | grep -v "pkg-resources" | grep -v "GDAL" | grep -v "pygdal" > requirements.txt
python manage.py makemigrations --dry-run
git status
read -p "Confirm that all migrations and new files have been committed. Are you sure? [y/n]" -n 1 -r

## Confirm that all migrations have been added to the git repo

echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    python setup.py sdist
    cmd='git commit -am '
    cmd+='"Built $(grep version setup.py) on $(date)"'
    echo $cmd
    eval $cmd
    git push
    echo "Now go to Bitbucket and do a PR"
fi

read -p "Should we build a new soildx/gdalbase:dj3 docker image as well? [y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker build -f DockerfileBase -t soildx/gdalbase:dj3 .
    # docker push soildx/gdalbase:latest
fi

read -p "Run tests? [y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker-compose build
    docker-compose up
fi

read -p "Should we push the image to DockerHub [y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # docker build -f DockerfileBase -t soildx/gdalbase:latest .
    docker push soildx/gdalbase:dj3
fi




