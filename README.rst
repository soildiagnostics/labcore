SoilDx Labcore Modules
======================

:Authors:
    Kaustubh Bhalerao, bhalerao@soildiagnostics.com

:Version: 1.0.4a of Nov 23 2020

Description
===========

This package contains core modules for SoilDx's soil testing clients. 
These modules include:

- Contacts
- Associate Organizations
- Clients
- FieldCalendar
- Products
- Orders
- Samples
- Inventory
- Meetings
- FertiSaverN
- GIS functionality
- Field Variability Analysis
- Customer logins
- SSURGO data integration
- USGS topography integration
- Basic weather integration via aWhere

Installation
============

Install inside a Django 3.0 project with::
    pip install git+https://kbhalerao@bitbucket.org/soildiagnostics/labcore.git#egg=soildx_labcore
    pip install --upgrade -e git+https://kbhalerao@bitbucket.org/soildiagnostics/labcore.git@development#egg=soildx-labcore --ignore-installed

Changelog
=========

- V1.0.4a of November 23 2020
    - Regressed back to Django 3.1.1 because of admin changelist issue.
- V1.0.4 of November 23 2020
    - Moved to Django 3.1.3
    - Lots of new API endpoints for eventual frontend redevelopment
- V1.0.3 of October 16 2020
    - Added TIFF handling capabilities
    - Added sieve and polygonize functions
- V1.0.2 of September 29 2020
    - Significant updates to GIS system
    - added Redis cache to some views
- V1.0.1 of August 26 2020
    - This is a massive update, where SubfieldLayers were moved to a generic geometry
- V1.0.0 of May 5 2020
    - Added spatial index and big infrastructure update to enable field prescriptions
- V0.995 of April 25 2020
    - Big update - now allows formulae and ability to change shapefile data
- V0.99 of April 07 2020
    - Now with upgraded python dependencies and Django 3.0
- V0.98 of April 05 2020
    - Tested FieldCalendar API to enable photo uploads with mobile apps
    - Moved shapefile processing to a worker process
    - Lots more testing
- V0.97 of March 18 2020
    - Significant improvements to report generation
    - Significant improvements to Contact handling
- V0.96 of March 06 2020
    - Added Colorfield to project
    - Created database models for soil nutrient adequacy tables
    - Proof of concept soil shapefile creator produced
- V0.95 of February 08 2020
    - Added aWhere weather integration
- V0.92 of February 04 2020
    - Lots more bugfixes
    - Added RelatedDropdown filters across analytics pages
    - Hallpass automation is now better tested
- V0.90 of December 21 2019
    - Several bugfixes and improvements. Code has been running in production since September.
    - Better test coverage
    - A more flexible Order system
- V0.80 of August 26 2019
    - Lots of tests added
    - A Hall Pass system is plumbed in that allows sharing fields across clients or dealers
- V0.75 of August 24 2019
    - Completely refactored all the javascript into static files
    - Reworked FieldCalendar app completely,
    - Some enhancements to the GIS processing tools
- V0.72 of August 19 2019
    - Significantly more testing and more improvements to the Field Calendar app.
    - Added a new feature by which users can schedule service tasks right off the dashboard
    - created a Journal mixin so order status changes can be tracked on a calendar
- V0.70 of August 3 2019
    - Codebase has very few new features, but several upgrades and bugfixes
- V0.60 of March 9 2019
    - Another huge update. System now talks to a Lambda to obtain data on 10m elevation information from USGS
- V0.50 of Feb 28 2019
    - Huge update consisting of Soil Survey Data and Soil Test Data integration
    - Regressions are now possible with all parameters
- V0.4 of Jan 27 2019
    - This is a major bump, since we are now running on Docker with an ASGI server to allow async tasks in the future.
    - We rolled back to GDAL 2.2.3 for simplicity of install
    - No longer using the ElasticBeanstalk python framework but the single container Docker framework instead
- V0.34 of Jan 17 2019
    - More testing
    - New AMI with GDAL 2.4.0
    - Hopefully faster deploys
- V0.33 of Jan 10 2019
    - Lots of improvements to forms
    - Client login model set up and tested
    - New AMI build for faster deployments
- V0.32 of December 27 2018
    - Small fixes
    - Labcore is now SSL compliant - deployed to soildiagnostics.com
- V0.31 of December 19 2018
    - Getting close to deployment
    - Will be moving to master branch deployment
- V0.30 of December 14 2018
    - Massive update
    - Associate Org model fixes
    - GIS Field Variability calculator implemented
    - GEOS library and GDAL 2.3.2 setup in AWS
- V0.26b of December 7 2018
    - GIS capabilties streamlined
    - This is a short upgrade, with broken tests to help NP get off the ground
- V0.25 of October 29 2018
    - Lots of modifications to old apps.
    - Fertisaver now integrated with LabCore
    - Lots of tests added
    - Products now have a database template system
    - Some GIS capabilities added
- V0.24 of October 15 2018
    - added 'meetings' app
- V0.23 of October 12 2018
    - added 'inventory' app
- V0.22 of October 8 2018
    - added 'samples' app
    - added significantly more tests
- V0.21 of September 26 2018
    - added 'fieldcalendar' app
    - moved to Django 2.1
- V0.20 of September 14 2018
    - Order app is now in draft. Need to write tests
    - Created new app called 'customnotes' that will be used
      in several different apps for drag and drop capability.
- V0.19 of September 9 2018
    - Working Clients Dashboard Module
    - Order and Field History app are still pending
- V0.18 of August 28 2018
    - Added 'order' and 'fieldhistory' apps
- V0.16 of August 14 2018
    - Added 'clients' app with geospatial goodness
    - lots of changes to admin interface
    - Added WYSIWYG editing to the blog
- V0.12 of July 17 2018
    - Added 'products` app
    - Made small changes to admin interface


Moving to a custom Linux AMI
============================

Labcore-Dev is currently building off soildx/gdalbase:latest as the docker image off dockerhub
