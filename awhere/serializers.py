from rest_framework import serializers
from clients.models import Field
from awhere.models import AWhereSubscription


class FieldSerializerForWeather(serializers.ModelSerializer):
    """
    Ensure that the minimal serializer works only on fields that are located,
    i.e. have a boundary.
    """
    farmId = serializers.SerializerMethodField()

    def get_farmId(self, obj):
        return obj.field.farm.id

    acres = serializers.SerializerMethodField()

    def get_acres(self, obj):
        return obj.field.area

    centerPoint = serializers.SerializerMethodField()

    def get_centerPoint(self, obj):
        lng, lat = obj.field.boundary.centroid.coords
        return {"latitude": lat,
                "longitude": lng}

    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.field.name

    id = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.field.id

    class Meta:
        model = AWhereSubscription
        fields = ('id', 'name', 'farmId', 'acres', 'centerPoint')
