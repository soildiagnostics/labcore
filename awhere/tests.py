import pprint
from unittest import skip

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from clients.tests.tests_models import ClientsTestCase
from customnotes.models import CustomField
from fieldcalendar.event_base import CalendarEvent
from .models import AWhereSubscription
from .serializers import FieldSerializerForWeather

from .awhere_api_helper import AWhereAPI
from fieldcalendar.models import FieldEvent


class aWhereTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ctc = ClientsTestCase()
        ctc.setUp()
        self.field = ctc.createField()

    def test_create_subscription(self):
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNotNone(subscription.pk)
        self.assertFalse(subscription.expired)
        subs = AWhereSubscription.objects.get_current_subscriptions()
        self.assertIn(subscription, subs)

    def test_serializer(self):
        subscription = AWhereSubscription.objects.create_default(self.field)
        serdata = FieldSerializerForWeather(subscription).data

        self.assertEqual(serdata.get('id'), self.field.id)

    @skip
    def test_login(self):
        api_object = AWhereAPI()
        self.assertIsNotNone(api_object.auth_token)

    @skip
    def test_create_field(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

    @skip
    def test_download_weather(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        response = api_object.get_weather_by_id(self.field.id)
        self.assertEqual(response.status_code, 200)
        pprint.pprint(response.content)

    # def test_field_event_mock(self):
    #     # response = b'''
    #     # {"forecasts":[{"date":"2020-02-07","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-07T00:00:00+00:00","endTime":"2020-02-07T23:59:59+00:00","conditionsCode":"E21","conditionsText":"MostlyCloudy,LightRain,LightWind/Calm","temperatures":{"max":0.39066079258918762,"min":-8.2733888626098633,"units":"C"},"precipitation":{"chance":98.333335876464844,"amount":1.3125,"units":"mm"},"sky":{"cloudCover":66.5,"sunshine":33.5},"solar":{"amount":2700.0,"units":"Wh/m^2"},"relativeHumidity":{"average":90.870834668477372,"max":99.800003051757813,"min":86.300003051757812},"wind":{"average":2.6777609244872713,"max":4.3511057908050539,"min":1.5285951074894624,"units":"m/sec","bearing":252.6,"direction":"WSW"},"dewPoint":{"amount":-5.6270223020442307,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.76492512412369251,"max":0.36998045444488525,"min":-1.5735595226287842,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.8190649251143138,"max":1.842226505279541,"min":1.7829883098602295,"units":"C"},{"depth":"0.4-1mbelowground","average":4.452549596627553,"max":4.4617772102355957,"min":4.4417576789855957,"units":"C"},{"depth":"1-2mbelowground","average":8.3370833794275914,"max":8.369999885559082,"min":8.3000001907348633,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":36.595833301544189,"max":39.099998474121094,"min":35.299999237060547},{"depth":"0.1-0.4mbelowground","average":32.912040392557778,"max":33.403705596923828,"min":32.403709411621094},{"depth":"0.4-1mbelowground","average":34.562041759490967,"max":34.703708648681641,"min":34.403709411621094},{"depth":"1-2mbelowground","average":33.518129348754883,"max":33.543128967285156,"min":33.443130493164062}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-07"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-08","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-08T00:00:00+00:00","endTime":"2020-02-08T23:59:59+00:00","conditionsCode":"E51","conditionsText":"MostlyCloudy,TraceAmountofRain,LightWind/Calm","temperatures":{"max":0.054602865129709244,"min":-7.3363218307495117,"units":"C"},"precipitation":{"chance":0.0,"amount":0.1250000037252903,"units":"mm"},"sky":{"cloudCover":72.5,"sunshine":27.5},"solar":{"amount":3774.0,"units":"Wh/m^2"},"relativeHumidity":{"average":91.625000317891434,"max":95.9000015258789,"min":82.0999984741211},"wind":{"average":3.417827118481068,"max":4.4760017275274011,"min":1.2276206329175963,"units":"m/sec","bearing":276.8,"direction":"W"},"dewPoint":{"amount":-5.2648980051836309,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-1.3174568613370259,"max":-1.092714786529541,"min":-1.6662499904632568,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.6981843958298366,"max":1.77531898021698,"min":1.6207226514816284,"units":"C"},{"depth":"0.4-1mbelowground","average":4.4310555458068848,"max":4.4413800239562988,"min":4.42117166519165,"units":"C"},{"depth":"1-2mbelowground","average":8.2583333651224766,"max":8.2966670989990234,"min":8.2200002670288086,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":40.16249942779541,"max":41.800003051757813,"min":39.166664123535156},{"depth":"0.1-0.4mbelowground","average":32.070374250411987,"max":32.3703727722168,"min":31.703706741333008},{"depth":"0.4-1mbelowground","average":34.27870829900106,"max":34.403709411621094,"min":34.103706359863281},{"depth":"1-2mbelowground","average":33.601463476816811,"max":33.643131256103516,"min":33.543128967285156}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-08"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-09","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-09T00:00:00+00:00","endTime":"2020-02-09T23:59:59+00:00","conditionsCode":"F41","conditionsText":"Cloudy,HeavyRain,LightWind/Calm","temperatures":{"max":3.9621548652648926,"min":-4.0178842544555664,"units":"C"},"precipitation":{"chance":100.0,"amount":11.124999795109034,"units":"mm"},"sky":{"cloudCover":87.625,"sunshine":12.375},"solar":{"amount":150.0,"units":"Wh/m^2"},"relativeHumidity":{"average":92.891666412353516,"max":99.800003051757813,"min":85.699996948242188},"wind":{"average":4.37096797172802,"max":6.526596776997506,"min":1.6105773383279243,"units":"m/sec","bearing":167.8,"direction":"SSE"},"dewPoint":{"amount":-1.0416750342588383,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-1.116594245036443,"max":-0.57203125953674316,"min":-1.4124023914337158,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.4749072293440502,"max":1.6073958873748779,"min":1.3629688024520874,"units":"C"},{"depth":"0.4-1mbelowground","average":4.4130208293596906,"max":4.4217772483825684,"min":4.3917579650878906,"units":"C"},{"depth":"1-2mbelowground","average":8.1783308982849121,"max":8.2166662216186523,"min":8.1400003433227539,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":44.183333396911621,"max":47.600002288818359,"min":41.833332061767578},{"depth":"0.1-0.4mbelowground","average":31.807873964309692,"max":33.703708648681641,"min":31.303707122802734},{"depth":"0.4-1mbelowground","average":33.953707218170166,"max":34.103706359863281,"min":33.803707122802734},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-09"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-10","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-10T00:00:00+00:00","endTime":"2020-02-10T23:59:59+00:00","conditionsCode":"F21","conditionsText":"Cloudy,LightRain,LightWind/Calm","temperatures":{"max":3.2020833492279053,"min":-4.5087499618530273,"units":"C"},"precipitation":{"chance":17.5,"amount":0.62499998509883881,"units":"mm"},"sky":{"cloudCover":100.0,"sunshine":0.0},"solar":{"amount":2838.0,"units":"Wh/m^2"},"relativeHumidity":{"average":89.854166666666671,"max":97.833335876464844,"min":81.0},"wind":{"average":4.1314278613255926,"max":6.13354212161457,"min":1.6840709049561127,"units":"m/sec","bearing":340.9,"direction":"NNW"},"dewPoint":{"amount":-2.3244783804585141,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.34064941604932147,"max":-0.28843748569488525,"min":-0.53089195489883423,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.3054541051387787,"max":1.3559049367904663,"min":1.2623633146286011,"units":"C"},{"depth":"0.4-1mbelowground","average":4.37453293800354,"max":4.391777515411377,"min":4.3517580032348633,"units":"C"},{"depth":"1-2mbelowground","average":8.1079167127609253,"max":8.1400003433227539,"min":8.0699996948242187,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":47.075000286102295,"max":47.600002288818359,"min":46.700000762939453},{"depth":"0.1-0.4mbelowground","average":33.849539915720619,"max":33.903705596923828,"min":33.7703742980957},{"depth":"0.4-1mbelowground","average":33.803707122802734,"max":33.803707122802734,"min":33.803707122802734},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-10"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-11","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-11T00:00:00+00:00","endTime":"2020-02-11T23:59:59+00:00","conditionsCode":"F11","conditionsText":"Cloudy,NoRain,LightWind/Calm","temperatures":{"max":3.8785643577575684,"min":-2.4429688453674316,"units":"C"},"precipitation":{"chance":0.0,"amount":0.0,"units":"mm"},"sky":{"cloudCover":88.75,"sunshine":11.25},"solar":{"amount":3342.0,"units":"Wh/m^2"},"relativeHumidity":{"average":78.720834096272782,"max":89.033332824707031,"min":64.4000015258789},"wind":{"average":2.2932784388121328,"max":3.4854062134069119,"min":0.89767646429134074,"units":"m/sec","bearing":325.1,"direction":"NW"},"dewPoint":{"amount":-3.6051682392911633,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.3059741196533044,"max":-0.25333985686302185,"min":-0.34560546278953552,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.2181900888681412,"max":1.2590234279632568,"min":1.1823437213897705,"units":"C"},{"depth":"0.4-1mbelowground","average":4.3296883304913836,"max":4.3517580032348633,"min":4.3117771148681641,"units":"C"},{"depth":"1-2mbelowground","average":8.0329198638598118,"max":8.0666732788085938,"min":7.9999804496765137,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":46.283333142598472,"max":46.633331298828125,"min":45.299999237060547},{"depth":"0.1-0.4mbelowground","average":33.620374202728271,"max":33.803707122802734,"min":33.403705596923828},{"depth":"0.4-1mbelowground","average":33.770374139149986,"max":33.803707122802734,"min":33.703708648681641},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-11"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-12","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-12T00:00:00+00:00","endTime":"2020-02-12T23:59:59+00:00","conditionsCode":"C11","conditionsText":"MostlyClear,NoRain,LightWind/Calm","temperatures":{"max":6.9993653297424316,"min":-3.2575716972351074,"units":"C"},"precipitation":{"chance":0.0,"amount":0.0,"units":"mm"},"sky":{"cloudCover":37.5,"sunshine":62.5},"solar":{"amount":4092.0,"units":"Wh/m^2"},"relativeHumidity":{"average":73.654165903727218,"max":82.699996948242188,"min":55.799999237060547},"wind":{"average":3.536714736968515,"max":6.7445658767644918,"min":1.3622041692706652,"units":"m/sec","bearing":275.7,"direction":"W"},"dewPoint":{"amount":-4.5560211806248558,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.26815022590259713,"max":-0.18822266161441803,"min":-0.31511718034744263,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.1599991967280705,"max":1.1821548938751221,"min":1.1417578458786011,"units":"C"},{"depth":"0.4-1mbelowground","average":4.2838419079780579,"max":4.3084373474121094,"min":4.2617578506469727,"units":"C"},{"depth":"1-2mbelowground","average":7.9591706991195679,"max":7.9966473579406738,"min":7.929999828338623,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":44.191666444142662,"max":45.100002288818359,"min":42.5},{"depth":"0.1-0.4mbelowground","average":33.674540519714355,"max":33.803707122802734,"min":33.403705596923828},{"depth":"0.4-1mbelowground","average":33.703708648681641,"max":33.703708648681641,"min":33.703708648681641},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-12"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-13","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-13T00:00:00+00:00","endTime":"2020-02-13T23:59:59+00:00","conditionsCode":"C11","conditionsText":"MostlyClear,NoRain,LightWind/Calm","temperatures":{"max":2.5224447250366211,"min":-5.0432486534118652,"units":"C"},"precipitation":{"chance":0.0,"amount":0.0,"units":"mm"},"sky":{"cloudCover":34.0,"sunshine":66.0},"solar":{"amount":4014.0,"units":"Wh/m^2"},"relativeHumidity":{"average":78.329167207082108,"max":94.0,"min":61.099998474121094},"wind":{"average":4.4096967683409991,"max":6.1232859570006433,"min":1.70103793862854,"units":"m/sec","bearing":23.8,"direction":"NNE"},"dewPoint":{"amount":-5.3137190594847619,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.43480386957526207,"max":-0.18693359196186066,"min":-2.35302734375,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.1341544787089031,"max":1.1523633003234863,"min":1.0129557847976685,"units":"C"},{"depth":"0.4-1mbelowground","average":4.2301000555356341,"max":4.2584242820739746,"min":4.1717772483825684,"units":"C"},{"depth":"1-2mbelowground","average":7.8883333206176758,"max":7.9266667366027832,"min":7.8499999046325684,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":41.837500095367432,"max":46.799999237060547,"min":40.599998474121094},{"depth":"0.1-0.4mbelowground","average":33.674540996551514,"max":34.003707885742187,"min":32.203708648681641},{"depth":"0.4-1mbelowground","average":33.670375188191734,"max":33.703708648681641,"min":33.403705596923828},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-13"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}},{"date":"2020-02-14","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"44"},"forecast":[{"startTime":"2020-02-14T00:00:00+00:00","endTime":"2020-02-14T23:59:59+00:00","conditionsCode":"C11","conditionsText":"MostlyClear,NoRain,LightWind/Calm","temperatures":{"max":7.0272297859191895,"min":-4.6794891357421875,"units":"C"},"precipitation":{"chance":0.0,"amount":0.0,"units":"mm"},"sky":{"cloudCover":37.5,"sunshine":62.5},"solar":{"amount":4104.0,"units":"Wh/m^2"},"relativeHumidity":{"average":57.120833555857338,"max":77.0,"min":41.0},"wind":{"average":3.9463147272357006,"max":4.61792349806826,"min":3.0247177274352954,"units":"m/sec","bearing":159.2,"direction":"SSE"},"dewPoint":{"amount":-8.6059662137937,"units":"C"},"soilTemperatures":[{"depth":"0-0.1mbelowground","average":-0.4692791725198428,"max":-0.18763671815395355,"min":-0.91363281011581421,"units":"C"},{"depth":"0.1-0.4mbelowground","average":1.1507438272237778,"max":1.1623632907867432,"min":1.1323633193969727,"units":"C"},{"depth":"0.4-1mbelowground","average":4.1851090788841248,"max":4.2084441184997559,"min":4.1617579460144043,"units":"C"},{"depth":"1-2mbelowground","average":7.8179167111714678,"max":7.8499999046325684,"min":7.78000020980835,"units":"C"}],"soilMoisture":[{"depth":"0-0.1mbelowground","average":40.187498887379967,"max":40.399997711181641,"min":39.899997711181641},{"depth":"0.1-0.4mbelowground","average":33.237040519714355,"max":33.670375823974609,"min":32.903705596923828},{"depth":"0.4-1mbelowground","average":33.645373980204262,"max":33.703708648681641,"min":33.603706359863281},{"depth":"1-2mbelowground","average":33.643131256103516,"max":33.643131256103516,"min":33.643131256103516}]}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-14"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/44"}}}],"_links":{"self":{"href":"/v2/weather/fields/44/forecasts/2020-02-07,2020-02-23?limit=8"},"next":{"href":"/v2/weather/fields/44/forecasts/2020-02-07,2020-02-23?limit=8&offset=8"}}}'''
    #     # response = json.loads(response)
    #     awhere_string = b'''{"observations":[{"date":"2020-01-31","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":4.0799999237060547,"min":-0.67000001668930054,"units":"C"},"precipitation":{"amount":0.0,"units":"mm"},"solar":{"amount":1121.9539794921875,"units":"Wh/m^2"},"relativeHumidity":{"max":97.779998779296875,"min":79.959999084472656},"wind":{"morningMax":3.1476032733917236,"dayMax":3.7034344673156738,"average":1.5473642349243164,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-01-31"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-01","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":4.9800000190734863,"min":0.090000003576278687,"units":"C"},"precipitation":{"amount":0.82700002193450928,"units":"mm"},"solar":{"amount":1728.4066162109375,"units":"Wh/m^2"},"relativeHumidity":{"max":95.470001220703125,"min":65.879997253417969},"wind":{"morningMax":4.9372916221618652,"dayMax":6.6519360542297363,"average":3.5148258209228516,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-01"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-02","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":16.950000762939453,"min":2.4900000095367432,"units":"C"},"precipitation":{"amount":0.32300001382827759,"units":"mm"},"solar":{"amount":3762.362060546875,"units":"Wh/m^2"},"relativeHumidity":{"max":86.089996337890625,"min":43.529998779296875},"wind":{"morningMax":6.315274715423584,"dayMax":6.6850004196166992,"average":3.4934594631195068,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-02"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-03","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":19.020000457763672,"min":1.3600000143051147,"units":"C"},"precipitation":{"amount":5.05649995803833,"units":"mm"},"solar":{"amount":3580.806396484375,"units":"Wh/m^2"},"relativeHumidity":{"max":92.319999694824219,"min":43.590000152587891},"wind":{"morningMax":5.5081539154052734,"dayMax":6.1978082656860352,"average":3.1687498092651367,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-03"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-04","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":4.3000001907348633,"min":-0.4699999988079071,"units":"C"},"precipitation":{"amount":10.144000053405762,"units":"mm"},"solar":{"amount":1186.4320068359375,"units":"Wh/m^2"},"relativeHumidity":{"max":92.199996948242187,"min":63.520000457763672},"wind":{"morningMax":6.4876532554626465,"dayMax":7.2218217849731445,"average":4.9611043930053711,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-04"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-05","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":-0.47999998927116394,"min":-2.7899999618530273,"units":"C"},"precipitation":{"amount":4.4679999351501465,"units":"mm"},"solar":{"amount":1232.8006591796875,"units":"Wh/m^2"},"relativeHumidity":{"max":94.989997863769531,"min":61.799999237060547},"wind":{"morningMax":5.7869281768798828,"dayMax":6.3635802268981934,"average":4.3599996566772461,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-05"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}},{"date":"2020-02-06","location":{"latitude":39.333669486136884,"longitude":-91.2954442058566,"fieldId":"65"},"temperatures":{"max":-0.51999998092651367,"min":-6.3400001525878906,"units":"C"},"precipitation":{"amount":0.0,"units":"mm"},"solar":{"amount":1235.08544921875,"units":"Wh/m^2"},"relativeHumidity":{"max":93.4800033569336,"min":67.419998168945313},"wind":{"morningMax":4.5030837059021,"dayMax":5.5489177703857422,"average":3.0377395153045654,"units":"m/sec"},"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-02-06"},"curies":[{"name":"awhere","href":"http://awhere.com/rels/{rel}","templated":true}],"awhere:field":{"href":"/v2/fields/65"}}}],"_links":{"self":{"href":"/v2/weather/fields/65/observations/2020-01-31,2020-02-06"}}}'''
    #     awhere_dict = json.loads(awhere_string)

    @skip
    def test_weather_event_list(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        eventlist = api_object.get_observation(self.field)
        self.assertEqual(len(eventlist), 7)
        pprint.pprint(eventlist)

    @skip
    def test_weather_observation_create(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        eventlist = api_object.get_observation(self.field)
        self.assertEqual(len(eventlist), 7)

        for e in eventlist:
            fe = FieldEvent.objects.create_weather_entry(self.field, e)
            self.assertIsNotNone(fe.pk)
            pprint.pprint(fe.details)

    @skip
    def test_weather_forecast_create(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        eventlist = api_object.get_forecast(self.field)
        self.assertEqual(len(eventlist), 8)

        for e in eventlist:
            fe = FieldEvent.objects.create_weather_entry(self.field, e)
            self.assertIsNotNone(fe.pk)
            pprint.pprint(fe.details)

    @skip
    def test_weather_forecast_event_create(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        api_object.create_forecasts(self.field)
        evts = FieldEvent.objects.filter(field=self.field)
        self.assertEqual(evts.count(), 8)

    @skip
    def test_reuse_custom_fields(self):
        api_object = AWhereAPI()
        subscription = AWhereSubscription.objects.create_default(self.field)
        self.assertIsNone(subscription.url)
        response = api_object.create_field(subscription)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(subscription.url)

        api_object.create_forecasts(self.field)
        evts = FieldEvent.objects.filter(field=self.field)
        self.assertEqual(evts.count(), 8)
        evt = evts.first()

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        cfs = CustomField.objects.filter(content_type=contenttype.pk,
                                         object_id=evt.event_type.id)

        self.assertEqual(cfs.count(), 8)
        api_object.create_forecasts(self.field)
        evts = FieldEvent.objects.filter(field=self.field)
        self.assertEqual(evts.count(), 8)

        cfs = CustomField.objects.filter(content_type=contenttype.pk,
                                         object_id=evt.event_type.id)

        self.assertEqual(cfs.count(), 8)


