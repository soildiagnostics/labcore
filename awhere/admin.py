from django.contrib import admin
from .models import AWhereSubscription
from .awhere_api_helper import AWhereAPI
# Register your models here.

@admin.register(AWhereSubscription)
class AWhereSubscriptionAdmin(admin.ModelAdmin):

    list_display = ('field', 'order', 'start', 'end', 'products')
    autocomplete_fields = ('field', 'order')

    def products(self, obj):
        if obj.order:
            return ",".join([p.name for p in obj.order.products.all()])
        return None
    products.short_description = "Weather Products"

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not obj.url:
            api_helper = AWhereAPI()
            api_helper.create_field(obj)
