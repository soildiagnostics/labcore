from datetime import timedelta

from django.db import models
from clients.models import Field
from django.utils.translation import gettext_lazy as _
# Create your models here.
from django.utils.timezone import now
from fieldcalendar.models import FieldEvent
from orders.models import Order

class AWhereSubscriptionManager(models.Manager):

    def get_current_subscriptions(self):
        current = now()
        return AWhereSubscription.objects.filter(start__lte=current, end__gte=current)

    def create_default(self, field):
        current = now()
        return AWhereSubscription.objects.create(field=field, start=current, end=current+timedelta(days=7))

    def create_annual(self, order):
        current = now()
        return AWhereSubscription.objects.create(field=order.field,
                                                 start=current,
                                                 end=current + timedelta(days=365),
                                                 order=order)

class AWhereSubscription(models.Model):

    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    start = models.DateField(help_text=_("Start date of the subcription"))
    end = models.DateField(help_text=_("End date of the subscription"))
    objects = AWhereSubscriptionManager()
    url = models.CharField(max_length=256, blank=True, null=True, editable=False)
    order = models.ForeignKey(Order, blank=True, null=True, on_delete=models.PROTECT)

    @property
    def expired(self):
        current = now()
        if self.start < current and self.end >= current:
            return False
        return True


