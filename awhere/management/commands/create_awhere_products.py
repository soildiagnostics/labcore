from django.core.management.base import BaseCommand

from fieldcalendar.event_base import CalendarEventKind
from products.models import Product, Category, Package
from associates.models import Organization
from fieldcalendar.models import CalendarEvent


class Command(BaseCommand):

    def handle(self, *args, **options):
        primary = Organization.objects.me
        category, _ = Category.objects.get_or_create(name="Weather")
        obs, _ = Product.objects.get_or_create(name="Observations",
                                               category=category,
                                               description="Weather observations from aWhere, past 7 days",
                                               created_by=primary,
                                               active=False)
        forecast, _ = Product.objects.get_or_create(name="Forecast",
                                                    category=category,
                                                    description="10 day Forecast from aWhere",
                                                    created_by=primary,
                                                    active=False)
        norms, _ = Product.objects.get_or_create(name="Historic Norms",
                                                 category=category,
                                                 description="Daily Historic Norms",
                                                 created_by=primary,
                                                 active=False)

        pkg, _ = Package.objects.get_or_create(category=category,
                                               name="Annual Weather Subscription")

        pkg.package.add(obs)
        pkg.package.add(forecast)
        pkg.package.add(norms)

        try:
            evtkind, _ = CalendarEventKind.objects.get_or_create(name="Weather",
                                                                 color="darkkhaki",
                                                                 system_event=True)
            CalendarEvent.objects.get_or_create(name="Observations",
                                                kind=evtkind)
            CalendarEvent.objects.get_or_create(name="Forecast",
                                                kind=evtkind)
        except Exception:
            pass

