from django.core.management.base import BaseCommand
from awhere.models import AWhereSubscription
from awhere.awhere_api_helper import AWhereAPI
from django.utils.timezone import now
from datetime import timedelta

from fieldcalendar.event_base import CalendarEvent
from fieldcalendar.models import FieldEvent


class Command(BaseCommand):

    def handle(self, *args, **options):

        current = now()
        forecast = CalendarEvent.objects.get(name__icontains="forecast")
        stale = FieldEvent.objects.filter(event_type=forecast, start__lt=current - timedelta(days=1))
        stale.delete()

        api_helper = AWhereAPI()
        subscriptions = AWhereSubscription.objects.get_current_subscriptions()
        for sub in subscriptions:
            prods = sub.order.products.first().package.all().values_list('name', flat=True)
            if "Observations" in prods:
                api_helper.create_observations(sub.field)
            if "Forecast" in prods:
                api_helper.create_forecasts(sub.field)


