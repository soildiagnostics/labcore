from django.core.management.base import BaseCommand
from products.models import Product
from awhere.awhere_api_helper import AWhereAPI
from awhere.models import AWhereSubscription
from orders.models import Order

class Command(BaseCommand):

    def handle(self, *args, **options):

        weather_products = Product.objects.filter(category__name="Weather")
        orders = Order.objects.filter(products__in=weather_products)
        orders = orders.filter(awheresubscription__isnull=True)
        for order in orders:
            s = AWhereSubscription.objects.create_annual(order)
            apih = AWhereAPI()
            apih.create_field(s)
            print(s)






