import base64
import json
from datetime import datetime

import pytz
from django.conf import settings
import requests as rq

from fieldcalendar.models import FieldEvent
from .serializers import FieldSerializerForWeather


def t_to_f(t_in_c):
    return round(t_in_c * 9 / 5 + 32, 1)

def mm_to_in(p_in_mm):
    return round(p_in_mm/25.4, 2)

def vel_to_mph(vel):
    return round(vel * 2.23694, 1)

class AWhereAPI(object):
    def __init__(self):
        """
        Initializes the AWhereAPI class, which is used to perform HTTP requests
        to the aWhere V2 API.
        Docs:
            http://developer.awhere.com/api/reference
        """
        self._fields_url = 'https://api.awhere.com/v2/fields'
        self._weather_url = 'https://api.awhere.com/v2/weather/fields'
        self.api_key = settings.AWHERE_KEY
        self.api_secret = settings.AWHERE_SECRET
        self.base_64_encoded_secret_key = self.encode_secret_and_key(
            self.api_key, self.api_secret)
        self.auth_token = self.get_oauth_token(self.base_64_encoded_secret_key)
        self.url_dict = {
            "forecast": self._weather_url + '/{0}/forecasts?blockSize=24',
            "observations": self._weather_url + '/{0}/observations'
        }

    def create_field(self, subscription):
        """
        Performs a HTTP POST request to create and add a Field to your aWhere App.AWhereAPI
        Docs:
            http://developer.awhere.com/api/reference/fields/create-field
        """

        fieldBody = FieldSerializerForWeather(subscription).data

        # Setup the HTTP request headers
        auth_headers = {
            "Authorization": "Bearer %s" % self.auth_token,
            "Content-Type": 'application/json'
        }

        # Perform the POST request to create your Field
        response = rq.post(self._fields_url,
                           headers=auth_headers,
                           json=fieldBody)
        # A successful request will return a 201 status code


        if response.status_code == 201:
            subscription.url = json.loads(response.content).get("_links").get("self").get("href")
            subscription.save()

        return response


    def delete_field_by_id(self, field_id):
        """
        Performs a HTTP DELETE request to delete a Field from your aWhere App.
        Docs: http://developer.awhere.com/api/reference/fields/delete-field
        Args:
            field_id: The field to be deleted
        """
        # Setup the HTTP request headers
        auth_headers = {
            "Authorization": "Bearer %s" % self.auth_token,
            "Content-Type": 'application/json'
        }
        # Perform the POST request to Delete your Field
        response = rq.delete(self._fields_url + '/{0}'.format(field_id),
                             headers=auth_headers)
        return response

    def encode_secret_and_key(self, key, secret):
        """
        Docs:
            http://developer.awhere.com/api/authentication
        Returns:
            Returns the base64-encoded {key}:{secret} combination, seperated by a colon.
        """
        # Base64 Encode the Secret and Key
        key_secret = '%s:%s' % (key, secret)
        # print('\nKey and Secret before Base64 Encoding: %s' % key_secret)

        encoded_key_secret = base64.b64encode(
            bytes(key_secret, 'utf-8')).decode('ascii')

        # print('Key and Secret after Base64 Encoding: %s' % encoded_key_secret)
        return encoded_key_secret

    def get_fields(self):
        """
        Performs a HTTP GET request to obtain all Fields you've created on your aWhere App.

        Docs:
            http://developer.awhere.com/api/reference/fields/get-fields
        """
        # Setup the HTTP request headers
        auth_headers = {
            "Authorization": "Bearer %s" % self.auth_token,
        }

        # Perform the HTTP request to obtain a list of all Fields
        fields_response = rq.get(self._fields_url,
                                 headers=auth_headers)

        responseJSON = fields_response.json()

        return responseJSON

    def get_weather_by_id(self, field_id, rtype="observations"):
        """
        Performs a HTTP GET request to obtain Forecast, Historical Norms and Forecasts

        Docs:
            1. Forecast: http://developer.awhere.com/api/forecast-weather-api
            2. Historical Norms: http://developer.awhere.com/api/reference/weather/norms
            3. Observations: http://developer.awhere.com/api/reference/weather/observations
        """
        # Setup the HTTP request headers
        auth_headers = {
            "Authorization": "Bearer %s" % self.auth_token,
        }

        url = self.url_dict.get(rtype)

        # Perform the HTTP request to obtain the Forecast for the Field
        response = rq.get(url.format(field_id),
                          headers=auth_headers)

        return response


    def get_oauth_token(self, encoded_key_secret):
        """
        Demonstrates how to make a HTTP POST request to obtain an OAuth Token

        Docs:
            http://developer.awhere.com/api/authentication

        Returns:
            The access token provided by the aWhere API
        """
        auth_url = 'https://api.awhere.com/oauth/token'

        auth_headers = {
            "Authorization": "Basic %s" % encoded_key_secret,
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        body = "grant_type=client_credentials"

        response = rq.post(auth_url,
                           headers=auth_headers,
                           data=body)

        # .json method is a requests lib method that decodes the response
        return response.json()['access_token']

    def create_observations(self, field):
        eventlist = self.get_observation(field)

        for e in eventlist:
            FieldEvent.objects.create_weather_entry(field, e)

    def get_observation(self, field):

        response = self.get_weather_by_id(field_id=field.id)

        awhere_dict = response.json()

        obslist = list()

        for obs in awhere_dict['observations']:
            start = pytz.utc.localize(datetime.strptime(obs['date'], '%Y-%m-%d'))
            tmax = t_to_f(obs['temperatures']['max'])
            tmin = t_to_f(obs['temperatures']['min'])
            rhmax = round(obs['relativeHumidity']['max'], 1)
            rhmin = round(obs['relativeHumidity']['min'], 1)
            precip = mm_to_in(obs['precipitation']['amount'])

            parameters = {
                "Temp (F)": f"Max {tmax}F Min {tmin}F",
                "Precip (in)": f"{precip}",
                "RH (%)": f"Max {rhmax}% Min {rhmin}%",
                f"Solar ({obs['solar']['units']})": f"{round(obs['solar']['amount'])}"
            }

            details = {
                "start": start,
                "title": f"T: {tmax}H, {tmin}L, Precip: {precip} in",
                "parameters": parameters,
                "event_type": "observation"
            }

            obslist.append(details)


        return obslist

    def create_forecasts(self, field):
        eventlist = self.get_forecast(field)

        for e in eventlist:
            FieldEvent.objects.create_weather_entry(field, e)

    def get_forecast(self, field):

        response = self.get_weather_by_id(field_id=field.id, rtype="forecast")
        awhere_dict = response.json()

        fcast_list = list()
        for item in awhere_dict['forecasts']:
            start = pytz.utc.localize(datetime.strptime(item['date'], '%Y-%m-%d'))
            fc = item['forecast'][0]
            tmax = round(t_to_f(fc['temperatures']['max']), 1)
            tmin = round(t_to_f(fc['temperatures']['min']), 1)
            rhmax = round(fc['relativeHumidity']['max'], 1)
            rhmin = round(fc['relativeHumidity']['min'], 1)
            precip_pc = round(fc['precipitation']['chance'])

            precip_amt = mm_to_in(fc['precipitation']['amount'])
            wind_avg = vel_to_mph(fc['wind']['average'])
            wind_max = vel_to_mph(fc['wind']['max'])
            wind_min = vel_to_mph(fc['wind']['min'])
            wind_dir = fc['wind']['direction']
            dew = t_to_f(fc['dewPoint']['amount']) # deg C
            soiltemp = list()
            for soilt in fc['soilTemperatures']:
                depth = soilt['depth'].replace("0-0.1 m below ground", 'at 0-4"')\
                    .replace("0.1-0.4 m below ground", 'at 4-16"')\
                    .replace("0.4-1 m below ground", 'at 16-40"')\
                    .replace("1-2 m below ground", 'at 40-80"')
                avg = soilt['average']
                soiltemp.append(f"{t_to_f(avg)}F {depth}")
            soiltemp = ", ".join(soiltemp)

            soilmoisture = list()
            for soilm in fc['soilMoisture']:
                depth = soilm['depth'].replace("0-0.1 m below ground", 'at 0-4"') \
                    .replace("0.1-0.4 m below ground", 'at 4-16"') \
                    .replace("0.4-1 m below ground", 'at 16-40"') \
                    .replace("1-2 m below ground", 'at 40-80"')
                avg = soilm['average']
                soilmoisture.append(f"{round(avg)}% {depth}")
            soilmoisture = ", ".join(soilmoisture)

            parameters = {
                "Temp (F)": f"Max {tmax}F Min {tmin}F",
                "Precip (in)": f"{precip_amt} in, {precip_pc}% chance",
                "RH (%)": f"Max {rhmax}% Min {rhmin}%",
                f"Solar ({fc['solar']['units']})": f"{round(fc['solar']['amount'])}",
                "Wind (mph)": f"{wind_dir} {wind_avg} mph ({wind_min} to {wind_max})",
                "Dew Point (F)": f"{dew} F",
                "Soil Temp (F)": f"{soiltemp}",
                "Soil Moisture (%)": f"{soilmoisture}"
            }

            details = {
                "start": start,
                "title": f"T: {tmax}H, {tmin}L, Precip: {precip_amt} in, {precip_pc}% chance",
                "parameters": parameters,
                "event_type": "forecast"
            }

            fcast_list.append(details)

        return fcast_list



