# Generated by Django 3.0.5 on 2020-09-09 16:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customnotes', '0002_auto_20180919_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='customfield',
            name='field_type',
            field=models.CharField(choices=[('T', 'text'), ('R', 'radio'), ('C', 'checkbox')], default='T', help_text='Make sure you set up appropriate options if you are choosing Radio or Checkbox', max_length=1),
        ),
        migrations.AddField(
            model_name='customfield',
            name='required',
            field=models.BooleanField(default=True, help_text='Is this field required?'),
        ),
        migrations.CreateModel(
            name='CustomFieldOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('customfield', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='options', to='customnotes.CustomField')),
            ],
        ),
    ]
