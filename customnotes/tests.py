from django.test import TestCase

from associates.models import Organization, OrganizationUser
from contact.models import Company, Person, User
from contact.tests.tests_models import ContactTestCase
from products.models import Product, Category
from .forms import customField_formfield_factory
from .models import CustomField, CustomFieldOption
from .serializers import CustomFieldSerializer
from django.test import SimpleTestCase


# Create your tests here.

class CustomNotesTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.orguser = OrganizationUser.objects.get(user=u)
        Category.objects.create(name="Soil Test")
        Product.objects.create(name="New Product", created_by=a)

    def test_create_customfield(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p)

        self.assertEqual(str(c), "Custom field")

    def test_create_form(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p)

        field = customField_formfield_factory(c)
        self.assertEqual(field.label, "Custom field")

    def test_serializer(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p, field_type="R")
        CustomFieldOption.objects.create(customfield=c, name="Option1")
        CustomFieldOption.objects.create(customfield=c, name="Option2")
        ser = CustomFieldSerializer(c).data
        self.assertEqual(len(ser['options']), 2)

class CustomNotesTemplateTagTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.orguser = OrganizationUser.objects.get(user=u)
        Category.objects.create(name="Soil Test")
        Product.objects.create(name="New Product", created_by=a)

    def test_rendered_text(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p)
        from django.template import Context, Template

        context = Context({'cf': c})
        template_to_render = Template(
            '{% load customfields_tags %}'
            '{% cfrender cf classes="form-control" %}'
        )
        rendered_template = template_to_render.render(context)
        self.assertIn('text', rendered_template)
        self.assertIn('form-control', rendered_template)

    def test_rendered_radio(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p, field_type="R")
        CustomFieldOption.objects.create(customfield=c, name="Option1")
        CustomFieldOption.objects.create(customfield=c, name="Option2")
        from django.template import Context, Template

        context = Context({'cf': c})
        template_to_render = Template(
            '{% load customfields_tags %}'
            '{% cfoptionrender cf classes="form-control" groupclass="form-group" %}'
        )
        rendered_template = template_to_render.render(context)
        self.assertIn('radio', rendered_template)
        self.assertIn('form-group', rendered_template)

    def test_rendered_checkbox(self):
        p = Product.objects.get(name="New Product")
        c = CustomField.objects.create(name="Custom field", content_object=p, field_type="C")
        CustomFieldOption.objects.create(customfield=c, name="Option1")
        CustomFieldOption.objects.create(customfield=c, name="Option2")
        from django.template import Context, Template

        context = Context({'cf': c})
        template_to_render = Template(
            '{% load customfields_tags %}'
            '{% cfoptionrender cf classes="form-control" groupclass="form-group" %}'
        )
        rendered_template = template_to_render.render(context)
        self.assertIn('checkbox', rendered_template)
        self.assertIn('form-group', rendered_template)

