from rest_framework import serializers

from .models import CustomField, CustomFieldOption

class CustomFieldOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomFieldOption
        fields = ("name",)

class CustomFieldSerializer(serializers.ModelSerializer):
    options = CustomFieldOptionSerializer(many=True, read_only=True)

    class Meta:
        model = CustomField
        fields = ("id", "name", 'field_type', "help_text", 'options')

    def create(self, validated_data):
        try:
            cf = CustomField.objects.get(**validated_data)
            self.initial_data['content_object'].custom_fields.add(cf)
        except CustomField.DoesNotExist:
            cf = CustomField(
                content_object=self.initial_data['content_object'],
                **validated_data
            )
        return cf
