from django import forms


def customField_formfield_factory(customfield, classes=None):
    """
    :param customfield:
    :return CharField:
    """
    if customfield.field_type == "T":
        return forms.CharField(required=False,
                               label=customfield.name,
                               help_text=customfield.help_text,
                               widget=forms.TextInput(attrs={'class': classes}))
    elif customfield.field_type == "R":
        choices = ((x.name, x.name) for x in customfield.options.all())
        return forms.ChoiceField(required=False,
                                 label=customfield.name,
                                 help_text=customfield.help_text,
                                 choices=choices,
                                 widget=forms.Select(attrs={'class': classes}))
    elif customfield.field_type == "C":
        choices = ((x.name, x.name) for x in customfield.options.all())
        return forms.ChoiceField(required=False,
                                 label=customfield.name,
                                 help_text=customfield.help_text,
                                 choices=choices,
                                 widget=forms.CheckboxSelectMultiple(attrs={'class': classes}))
