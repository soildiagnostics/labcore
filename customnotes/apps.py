from django.apps import AppConfig


class CustomnotesConfig(AppConfig):
    name = 'customnotes'
