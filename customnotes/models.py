from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext as _

# Create your models here.
FIELD_TYPE = (("T", "text"),
              ("R", "radio"),
              ("C", "checkbox"))


class CustomField(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    name = models.CharField(max_length=100,
                            help_text=_("This is the name of the field you want to attach to the model"))
    field_type = models.CharField(max_length=1, choices=FIELD_TYPE, default="T",
                                  help_text=_("Make sure you set up appropriate options if you are choosing Radio or Checkbox"))
    required = models.BooleanField(help_text=_("Is this field required?"), default=True)

    help_text = models.TextField(blank=True, null=True,
                                 help_text=_(
                                     "This is the help text that you want to display in the form that uses this field"))

    def __str__(self):
        return self.name


class CustomFieldOption(models.Model):
    customfield = models.ForeignKey(CustomField, on_delete=models.CASCADE, related_name='options')
    name = models.CharField(max_length=100)
