from django.contrib import admin
from django.contrib.admin import TabularInline
from django.contrib.contenttypes.admin import GenericTabularInline
from django import forms
from django.db import models

from .models import CustomField, CustomFieldOption


class CustomFieldInline(GenericTabularInline):
    model = CustomField
    extra = 1
    show_change_link = True
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 1})}
    }

class CustomFieldOptionInline(TabularInline):
    model = CustomFieldOption
    extra = 1

@admin.register(CustomField)
class CustomFieldAdmin(admin.ModelAdmin):
    inlines = [CustomFieldOptionInline]