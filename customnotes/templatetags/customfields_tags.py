from django import template

from customnotes.models import CustomField
from inventory.models import Item

register = template.Library()


def updated_context(cf, **kwargs):
    context = {"cf": cf}
    context.update(**kwargs)
    instance = kwargs.get('instance')
    if instance:
        context['value'] = instance.get_value_for_cf(cf)
    else:
        context['value'] = ''
    return context

@register.inclusion_tag('customnotes/customfield_textwidget.html')
def cfrender(cf, *args, **kwargs):
    assert cf.field_type == "T", "Not a text field"
    return updated_context(cf, **kwargs)

@register.inclusion_tag('customnotes/customfield_optionwidget.html')
def cfoptionrender(cf, *args, **kwargs):
    assert cf.field_type != "T", "Not an options field"
    return updated_context(cf, **kwargs)
