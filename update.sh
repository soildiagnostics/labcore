#!/usr/bin/env bash
source ../bin/activate
eb ssh $1 --command "sudo docker ps -q" > out.txt
DOCKERID=`head -n 1 out.txt`
eb ssh $1 --command "sudo docker exec $DOCKERID env"

