#!/bin/bash
#python3 manage.py makemigrations --dry-run
python3 manage.py migrate
python3 manage.py collectstatic --no-input
#uwsgi --http-auto-chunked --http-keepalive
redis-server &
python3 manage.py runworker default &
#daphne -b 0.0.0.0 -p 8000 labcore.asgi:application
python3 manage.py test -v 3
rm media/contactimages/anonymous*
