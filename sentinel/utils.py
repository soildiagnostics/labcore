from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date, timedelta
from django.conf import settings

BASE_URL = 'https://scihub.copernicus.eu/dhus'
api = SentinelAPI(settings.SENTINEL_USER, settings.SENTINEL_PASSWORD, BASE_URL)


def download_image(field):
    products = api.query(field.boundary.convex_hull.wkt,
                         date=('NOW-2DAY', 'NOW'),
                         platformname='Sentinel-2',
                         cloudcoverpercentage=(0, 30))
