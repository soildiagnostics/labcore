import base64

from django.core.files.base import ContentFile
from django.urls import reverse
from rest_framework import serializers

from fieldcalendar.serializers import BaseEventSerializer
from inventory.models import ItemTemplate, Asset, Consumable, ItemEvent, Item


class ItemTemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ItemTemplate
        fields = "__all__"

class AssetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Asset
        fields = "__all__"

class ItemEventSerializer(BaseEventSerializer):
    extendedProps = serializers.SerializerMethodField()

    def get_extendedProps(self, obj):
        sent = {k: v for k, v in obj.details.items() if k in ['item_id',
                                                              'event_type',
                                                              'file',
                                                              'geotags',
                                                              'custom_fields']}
        if obj.file:
            sent["file"] = reverse("item_file_download", kwargs={'pk' : obj.id})
        else:
            sent["file"] = None
        if obj.event_type and obj.event_type.icon:
            sent['icon'] = obj.event_type.icon.url
        sent['item_name'] = obj.item.name
        return sent

    class Meta:
        model = ItemEvent
        fields = ['id', 'allDay', 'start', 'end', 'title', 'event_type',
                  'editable', 'color', 'extendedProps', 'textColor']

    def validate(self, data):
        data = super().validate(data)
        data['item'] = Item.objects.get(pk=int(self.initial_data['extendedProps']['item_id']))
        data['details'] = self.initial_data['extendedProps']
        data['details']['title'] = self.initial_data['title']

        return data

    def create(self, validated_data):
        if "photo" in self.initial_data["extendedProps"]:
            photo = self.initial_data['extendedProps'].pop("photo")

            photo_bytes = base64.decodebytes(photo.encode("utf-8"))
            name = self.initial_data["title"]
            # validated_data['file'] = File(photo_bytes, name)
            validated_data["details"] = ItemEvent.objects.create_photo_memo_customfields(validated_data["details"])
            fe = super().create(validated_data)
            fe.file.save(name, ContentFile(photo_bytes))
            return fe

        return super().create(validated_data)

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)