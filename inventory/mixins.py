import json
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from customnotes.models import CustomFieldOption, CustomField
from inventory.models import default_cf, ItemTemplate


class CustomFieldFormCleanMixin():
    """
    This field provides a method to take a form object and create a value stub for the JSON field
    """

    def get_value_for_field(self, cfpk):
        """
        cfpk: ID of the custom field
        form: a django Form, from which data will be extracted
        returns: {cfpk: 'value_from_form'}
        """
        cf = CustomField.objects.get(id=cfpk)
        val = self.data.get(f"custom_field_{cfpk}")
        if cf.field_type == "C":
            try:
                options = self.data.getlist(f"custom_field_{cfpk}")
                val = CustomFieldOption.objects.filter(id__in=options).values_list('name', flat=True)
            except AttributeError:
                ## This error is raised during testing. When a dictionary is passed to a form, instead of a QueryDict,
                ## the getlist function is not available, and a simple get returns thr right response.
                if val:
                    val = CustomFieldOption.objects.filter(id__in=val).values_list('name', flat=True)
                else:
                    val = list()
            val = [v for v in val]

        elif cf.field_type == "R":
            if val:
                try:
                    val = CustomFieldOption.objects.get(id=val).name
                except TypeError:
                    val = CustomFieldOption.objects.get(id=val[0]).name
        if cf.required and not val:
            raise ValidationError(_('Required field: %(name)s'),
                                  code="required",
                                  params={'name': cf.name})
        cfdata = {
            "id": cfpk,
            "name": cf.name,
            "value": val
        }
        return cfdata


    def clean(self):
        super().clean()
        self.cleaned_data['template'] = ItemTemplate.objects.get(id=self.data['template'])
        template = self.cleaned_data['template']
        custom_fields = template.custom_fields.all().values_list('pk', flat=True)
        addl_details = default_cf()
        for cf in custom_fields:
            addl_details['custom_fields'].append(self.get_value_for_field(cf))
        self.cleaned_data['additional_details'] = addl_details



