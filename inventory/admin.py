from django import forms
from django.contrib import admin
from django.db import models

from associates.models import Role
from customnotes.admin import CustomFieldInline
from .models import ItemTemplate, Asset, Consumable, ItemEvent


# Register your models here.


@admin.register(ItemTemplate)
class ItemTemplateAdmin(admin.ModelAdmin):
    filter_horizontal = ('suppliers',)
    search_fields = ('description', 'brand')
    list_display = ('description', 'brand',)
    autocomplete_fields = ('owner',)
    inlines = [CustomFieldInline]

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "suppliers":
            supplier_role, created = Role.objects.get_or_create(role="Supplier")
            kwargs["queryset"] = supplier_role.organization_set.all()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class LogInlineAdmin(admin.TabularInline):
    model = ItemEvent
    extra = 0

    # formfield_overrides = {
    #     models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    # }

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Asset)
class AssetAdmin(admin.ModelAdmin):
    autocomplete_fields = ('contact', 'template')
    filter_horizontal = ('group',)
    list_display = ('name', 'serial_number', 'template', 'contact', 'location', 'address',)
    list_editable = ('template', 'location', 'address', 'contact', 'serial_number')
    search_fields = ('name', 'template__description', 'contact__name', 'serial_number')
    list_filter = ('template',)

    inlines = [LogInlineAdmin]
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }


@admin.register(Consumable)
class ConsumableAdmin(admin.ModelAdmin):
    autocomplete_fields = ('contact', 'template')
    list_display = ('name', 'quantity', 'template', 'location', 'address', 'contact',)
    list_editable = ('template', 'location', 'address', 'contact', 'quantity')
    list_filter = ('template',)

    inlines = [LogInlineAdmin]
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }
