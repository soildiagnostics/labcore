# Generated by Django 3.0.5 on 2020-04-28 01:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0010_auto_20200427_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='quantity',
            field=models.IntegerField(null=True, verbose_name='Quantity'),
        ),
        migrations.AlterField(
            model_name='item',
            name='serial_number',
            field=models.CharField(max_length=100, null=True, verbose_name='Serial'),
        ),
    ]
