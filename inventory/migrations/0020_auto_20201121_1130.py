# Generated by Django 3.1.3 on 2020-11-21 17:30

from django.db import migrations, models
import inventory.models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0019_auto_20200909_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='additional_details',
            field=models.JSONField(default=inventory.models.default_cf, help_text='Do not modify directly'),
        ),
        migrations.AlterField(
            model_name='itemevent',
            name='details',
            field=models.JSONField(default=dict),
        ),
    ]
