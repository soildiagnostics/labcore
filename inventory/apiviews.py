from django.utils.timezone import now
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView

from associates.role_privileges import FilteredQuerySetMixin, DealershipRequiredMixin
from common.models import ParseTimeMixin
from inventory.models import ItemEvent, Item
from inventory.serializers import ItemEventSerializer


class APIItemEventsJSONBase(ParseTimeMixin, DealershipRequiredMixin):
    model = ItemEvent
    # client_has_permission = True
    # instance_owner_client = 'field__farm__client'
    # instance_owner_organization = 'field__farm__client__originator__organization'
    serializer_class = ItemEventSerializer
    related_model = Item

    def get_initial_queryset(self):
        qs = ItemEvent.objects.all()

        try:
            qs = qs.filter(start__gte=self.parse_with_tz(self.request.query_params.get('start')))
        except TypeError:
            pass
        try:
            qs = qs.filter(start__lte=self.parse_with_tz(self.request.query_params.get('end')))
        except TypeError:
            pass
        return qs

    def get_queryset(self):
        qs = self.get_initial_queryset()
        pk = self.kwargs.get('pk')
        item = self.related_model.objects.get(pk=pk)
        return qs.filter(item=item)


class APIAllItemsEventsJSON(APIItemEventsJSONBase, ListAPIView):
    def get_queryset(self):
        return self.get_initial_queryset()


class APIItemEventsJSON(APIItemEventsJSONBase, ListCreateAPIView):
    pass


class APIItemEventUpdateJSON(APIItemEventsJSON, RetrieveUpdateDestroyAPIView):
    lookup_field = 'pk'
    lookup_url_kwarg = 'event'

    def get_queryset(self):
        return self.get_initial_queryset().filter(editable=True)
