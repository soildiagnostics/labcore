import json
from datetime import timedelta

from address.models import Address
from django.core.exceptions import ValidationError
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now

from associates.models import Organization, OrganizationUser
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Company
from contact.tests.tests_models import ContactTestCase
from customnotes.models import CustomField, CustomFieldOption
from fieldcalendar.models import FieldEvent, CalendarEventKind, CalendarEvent
from inventory.forms import ItemTemplateForm, AssetForm, ConsumableForm
from inventory.models import ItemTemplate, Asset, Consumable, default_cf


class InventoryViewTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self) -> None:
        ContactTestCase.setUp(self)
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
        company = Company.objects.get(name__startswith="SkyData")
        person = user.person
        self.org, _ = Organization.objects.create_dealership(company)
        self.org.add_person_to_organization(person)
        self.client = TestClient()
        self.client.force_login(user)

        data = {
            "description": "Some description",
            "brand": "Brand",
            "notes": "some notes",
            "model": "",
            "part_number": "123"
        }

        self.template = ItemTemplate.objects.create(**data)
        CustomField.objects.create(name="Custom text", content_object=self.template, field_type="T")
        c = CustomField.objects.create(name="Custom radio", content_object=self.template, field_type="R")
        CustomFieldOption.objects.create(customfield=c, name="radio1")
        CustomFieldOption.objects.create(customfield=c, name="radio2")
        c = CustomField.objects.create(name="Custom checkbox", content_object=self.template, field_type="C")
        CustomFieldOption.objects.create(customfield=c, name="check1")
        CustomFieldOption.objects.create(customfield=c, name="check2")

    def check_200(self, url):
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_list_view(self):
        self.check_200(reverse("inventory_list"))

    def test_consumable_json(self):
        url = "/inventory/consumables/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=name&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=quantity&columns%5B1%5D%5Bsearchable%5D=false&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=template&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=location&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=contact&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=notes&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1588078649869"
        self.check_200(url)

    def test_asset_json(self):
        url = "/inventory/assets/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=name&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=template&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=location&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=contact&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1588078649867"
        self.check_200(url)

    def test_template_json(self):
        url = "/inventory/templates/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=description&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=brand&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=model&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=part_number&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=notes&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1588078649868"
        self.check_200(url)

    def test_template_create(self):
        url = reverse("item_template_create")
        self.check_200(url)

    def test_template_create_update_form(self):
        data = {
            "description": "Some description",
            "brand": "Brand",
            "notes": "some notes",
            "model": "",
            "part_number": "123",
            "ouser": 1
        }

        form = ItemTemplateForm(data)
        self.assertTrue(form.is_valid())

    def test_template_create_view(self):
        data = {
            "description": "Some description",
            "brand": "Brand",
            "notes": "some notes",
            "model": "",
            "part_number": "123",
            "ouser": 1
        }

        form = ItemTemplateForm(data)
        self.assertTrue(form.is_valid())
        url = reverse("item_template_create")
        resp = self.client.post(url, data=data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(ItemTemplate.objects.count(), 2)

    def test_template_update_view(self):
        data = {
            "description": "Some description",
            "brand": "Brand",
            "notes": "some notes",
            "model": "",
            "part_number": "123",
            "ouser": 1
        }

        form = ItemTemplateForm(data)
        self.assertTrue(form.is_valid())
        item = form.save()
        self.assertEqual(ItemTemplate.objects.count(), 2)
        url = reverse("item_template_update", kwargs={"pk": item.id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_asset_create_update_form(self):
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": self.template.id,
            "contact": OrganizationUser.objects.first(),
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": default_cf()
        }

        form = AssetForm(data)
        form.is_valid()
        self.assertFalse(form.is_valid())

        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": self.template.id,
            "contact": OrganizationUser.objects.first(),
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": default_cf(),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }
        form = AssetForm(data)
        form.is_valid()
        print(form.errors)
        self.assertTrue(form.is_valid())

        instance = form.save()
        self.assertEqual(len(instance.additional_details['custom_fields']), 3)
        self.assertEqual(len(instance.get_value_for_cf(cfc)), 2)

        self.assertEqual(str(instance.address), "14 Oak Ct")

    def test_form_get_cf_value_mixin(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": self.template.id,
            "contact": OrganizationUser.objects.first(),
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": default_cf(),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }
        form = AssetForm(data)
        form.is_valid()
        self.assertTrue(form.is_valid())

        asset = form.save()
        self.assertEqual(len(asset.additional_details['custom_fields']), 3)
        self.assertEqual(len(asset.get_value_for_cf(cfc)), 2)

        self.assertNotEqual(asset.additional_details, default_cf())
        self.assertEqual(asset.get_value_for_cf(cft), "Text")
        self.assertEqual(asset.get_value_for_cf(cfr), cfr.options.all().first().name)
        self.assertEqual(asset.get_value_for_cf(cfc), [o.name for o in cfc.options.all()])


    def test_cf_form_validation_errors(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": self.template.id,
            "contact": OrganizationUser.objects.first(),
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": default_cf(),
            f"custom_field_{cft.pk}": "",
            f"custom_field_{cfr.pk}": [],
            f"custom_field_{cfc.pk}": "",
        }
        form = AssetForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn("Required", form.errors['__all__'][0])



    def test_asset_create_view(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": ItemTemplate.objects.first().id,
            "contact": OrganizationUser.objects.first().id,
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": json.dumps(default_cf()),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }

        url = f"{reverse('asset_create')}?item_template_id={ItemTemplate.objects.first().id}"
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        resp = self.client.post(url, data=data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Asset.objects.count(), 1)
        asset = Asset.objects.first()
        self.assertEqual(asset.name, "Some name")
        self.assertNotEqual(asset.additional_details, default_cf())
        self.assertEqual(asset.get_value_for_cf(cft), "Text")
        self.assertEqual(asset.get_value_for_cf(cfr), cfr.options.all().first().name)
        self.assertEqual(asset.get_value_for_cf(cfc), [o.name for o in cfc.options.all()])


    def test_asset_update_view(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "123",
            "notes": "some notes",
            "template": ItemTemplate.objects.first().id,
            "contact": OrganizationUser.objects.first().id,
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": json.dumps(default_cf()),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }

        form = AssetForm(data)
        self.assertTrue(form.is_valid())
        asset = form.save()

        url = reverse("asset_update", kwargs={"pk": asset.pk})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        data['name'] = "some other name"
        resp = self.client.post(url, data=data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Asset.objects.count(), 1)
        self.assertEqual(Asset.objects.first().name, "some other name")

    def test_consumable_create_update_form(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        self.template.is_asset = False
        self.template.save()
        data = {
            "name": "Some name",
            "serial_number": "",
            "notes": "some notes",
            "template": self.template.id,
            "contact": OrganizationUser.objects.first().id,
            "is_asset": True,
            "quantity": 0,
            "address": "14 Oak Ct",
            "additional_details": json.dumps(default_cf()),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }

        form = ConsumableForm(data)
        self.assertTrue(form.is_valid())

    def test_consumable_create_view(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "",
            "notes": "some notes",
            "template": ItemTemplate.objects.first().id,
            "contact": OrganizationUser.objects.first().id,
            "is_asset": False,
            "quantity": 10,
            "address": "14 Oak Ct",
            "additional_details": json.dumps(default_cf()),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }

        form = ConsumableForm(data)
        self.assertTrue(form.is_valid())
        url = reverse("consumable_create")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        resp = self.client.post(url, data=data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Consumable.objects.count(), 1)
        self.assertEqual(Consumable.objects.first().name, "Some name")


    def test_consumable_update_view(self):
        cft = CustomField.objects.get(name="Custom text")
        cfr = CustomField.objects.get(name="Custom radio")
        cfc = CustomField.objects.get(name="Custom checkbox")
        data = {
            "name": "Some name",
            "serial_number": "",
            "notes": "some notes",
            "template": ItemTemplate.objects.first().id,
            "contact": OrganizationUser.objects.first().id,
            "is_asset": False,
            "quantity": 10,
            "address": "14 Oak Ct",
            "additional_details": json.dumps(default_cf()),
            f"custom_field_{cft.pk}": "Text",
            f"custom_field_{cfr.pk}": [cfr.options.all().first().pk],
            f"custom_field_{cfc.pk}": [o.pk for o in cfc.options.all()],
        }

        form = ConsumableForm(data)
        self.assertTrue(form.is_valid())
        consumable = form.save()

        url = reverse("consumable_update", kwargs={"pk": consumable.pk})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        data['name'] = "some other name"
        resp = self.client.post(url, data=data)
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(Consumable.objects.count(), 1)
        self.assertEqual(Consumable.objects.first().name, "some other name")


