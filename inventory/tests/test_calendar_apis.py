import json
from datetime import timedelta

from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now
from unittest import skip

from clients.tests.tests_models import ClientsTestCase
from contact.models import User
from inventory.models import ItemEvent, Asset, ItemTemplate
from fieldcalendar.models import CalendarEvent, CalendarEventKind
from inventory.tests.tests_models import ItemCreateTests


class ItemCalendarViewTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        kind, _ = CalendarEventKind.objects.get_or_create(name="Service")
        c, _ = CalendarEvent.objects.get_or_create(kind=kind, name="Scheduled maintenance")

        ItemCreateTests.setUp(self)
        self.itemtemplate = ItemTemplate.objects.create(
            description="Item template description",
            brand="Item brand",
            model="Model 123",
            part_number="",
            notes="Very expensive",
            owner=self.org
        )

        self.asset = Asset.objects.create(
            name="Asset name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            serial_number="123"
        )

        self.client = TestClient()
        user = User.objects.get(username="test_admin")
        self.client.force_login(user)

    def test_get_events(self):
        ItemEvent.objects.create_system_event(item=self.asset, title="System Event", start=now(), end=now())

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)

        self.assertTrue('start' not in resp[0]['extendedProps'].keys())

        ### Get no events

        time = {
            'start': now() - timedelta(days=2),
            'end': now() - timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(len(resp), 0)

    def test_post_new_event(self):
        time = now()

        details = {
            "id": "",
            "color": "green",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": True,
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})
        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['item_id'], self.asset.pk)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)
        self.assertEqual(len(resp), 2)

    @skip
    def test_post_scheduled_task(self):
        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": True,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "task": "This is a task"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})
        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['item_id'], self.asset.pk)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'This is a task')
        self.assertEqual(resp[0]['title'], "Corn")
        self.assertTrue(resp[0]['editable'])
        self.assertEqual(len(resp), 1)

    def test_post_completed_task(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "task": "Completed: This is a task"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        self.client.force_login(user)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})
        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['item_id'], self.asset.pk)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'This is a task')
        self.assertEqual(resp[0]['title'], "Completed: Corn")
        self.assertFalse(resp[0]['editable'])
        self.assertEqual(len(resp), 1)

    @skip
    def test_post_photo(self):
        time = now()
        name = "IMG_3601.JPG"
        import base64
        with open(f"./fieldcalendar/tests/{name}", "rb") as photo:
            photo_dat = base64.b64encode(photo.read()).decode("utf-8")

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": name,
            "editable": True,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "photo": photo_dat,
                "photo_memo": "This is a custom memo about the photo"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})
        # data = json.dumps(details)
        response = self.client.post(url, data=details, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['item_id'], self.asset.pk)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'This is a custom memo about the photo')
        self.assertEqual(resp[0]['title'], name)
        self.assertEqual(len(resp), 1)
        fe = ItemEvent.objects.get(id=resp[0].get("id"))

        self.assertAlmostEqual(fe.details.get("geotags").get("bearing"), 329.952, 2)
        self.assertAlmostEqual(resp[0]['extendedProps'].get("geotags").get("bearing"), 329.952, 2)

        self.assertEqual(ItemEvent.objects.filter(details__geotags__isnull=False).count(), 1)

    @skip
    def test_post_completed_task(self):
        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "task": "This is a task"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})
        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['item_id'], self.asset.pk)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['editable'], False)
        self.assertEqual(len(resp), 1)

    def test_update_existing_event(self):
        time = now()
        details = {
            "id": "",
            "color": "green",
            "title": "Corn",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": False,
            "editable": True,
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        details['start'] = (now() + timedelta(hours=2)).isoformat()
        details['end'] = (now() + timedelta(hours=3)).isoformat()
        details['extendedProps']['custom_fields'] = ['123']
        data = json.dumps(details)
        url = reverse('api_item_event_update',
                      kwargs={'pk': self.asset.pk, 'event': ItemEvent.objects.latest("pk").event_id})
        response = self.client.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        from common.models import ParseTimeMixin
        tp = ParseTimeMixin()
        self.assertEqual(tp.parse_with_tz(json.loads(response.content)['end']),
                         tp.parse_with_tz(details['end']))

        self.assertEqual(json.loads(response.content)['extendedProps']['custom_fields'], ['123'])

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['item_id'], self.asset.pk)
        self.assertEqual(len(resp), 2)


    def test_delete_event(self):
        time = now()

        details = {
            "id": "",
            "color": "green",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": True,
            "editable": True,
            "title": "Corn",
            "className": [],
            "extendedProps": {
                "item_id": self.asset.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        data = json.dumps(details)
        response = self.client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_item_event_update', kwargs={'pk': self.asset.pk,
                                                       'event': ItemEvent.objects.latest("pk").event_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

        url = reverse('api_item_events_json', kwargs={'pk': self.asset.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(len(resp), 1)
