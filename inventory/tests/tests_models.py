from django.db import IntegrityError
from django.test import TestCase

from associates.models import Organization
from contact.models import User, Company
from contact.tests.tests_models import ContactTestCase

from inventory.models import ItemTemplate, Asset, Consumable, Item


class ItemCreateTests(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

        company = Company.objects.get(name__startswith="SkyData")
        person = user.person
        self.org, _ = Organization.objects.create_dealership(company)
        self.org.add_person_to_organization(person)

    def test_create_item_template(self):
        itemtemplate = ItemTemplate.objects.create(
            description="Item template description",
            brand="Item brand",
            model="Model 123",
            part_number="",
            notes="Very expensive",
            owner=self.org
        )

        self.assertEqual(str(itemtemplate), "Item template description")
        self.assertEqual(itemtemplate.is_asset, True)


class AssetTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ItemCreateTests.setUp(self)
        self.itemtemplate = ItemTemplate.objects.create(
            description="Item template description",
            brand="Item brand",
            model="Model 123",
            part_number="",
            notes="Very expensive",
            owner=self.org
        )

        self.assertEqual(str(self.itemtemplate), "Item template description")

    def test_create_asset(self):
        asset = Asset.objects.create(
            name="Asset name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes"
        )

        self.assertEqual(asset.name, "Asset name")

        assetevent = asset.itemevent_set.first()
        assetevent.details["title"] = "New Asset"

        asset.name = "Name Changed"
        asset.save()

        assetevent = asset.itemevent_set.latest('pk')
        assetevent.details["title"] = "Update"


    def test_no_duplicate_serials(self):
        asset = Asset.objects.create(
            name="Asset name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            serial_number="123"
        )

        with self.assertRaises(IntegrityError):
            Asset.objects.create(
                name="Asset name",
                template=self.itemtemplate,
                contact=self.org.organization_users.first(),
                notes="Some notes",
                serial_number="123"
            )

    def test_one_each(self):
        asset = Asset.objects.create(
            name="Asset name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            serial_number="123"
        )
        self.itemtemplate.pk = None
        self.itemtemplate.is_asset = False
        self.itemtemplate.save()
        consumabletemplate = ItemTemplate.objects.latest('pk')
        self.assertFalse(consumabletemplate.is_asset)
        consumable = Consumable.objects.create(
            name="Consumable name",
            template=consumabletemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            quantity=100
        )
        self.assertEqual(Item.objects.count(), 2)
        self.assertEqual(Asset.objects.all().count(), 1)
        self.assertEqual(Consumable.objects.all().count(), 1)

class ConsumableTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ItemCreateTests.setUp(self)
        self.itemtemplate = ItemTemplate.objects.create(
            description="Item template description",
            brand="Item brand",
            model="Model 123",
            part_number="",
            notes="Very expensive",
            owner=self.org
        )

        self.assertEqual(str(self.itemtemplate), "Item template description")

    def test_create_consumable(self):

        self.itemtemplate.is_asset = False
        self.itemtemplate.save()

        consumable = Consumable.objects.create(
            name="Consumable name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            quantity=100
        )

        self.assertEqual(consumable.name, "Consumable name")
        self.assertEqual(consumable.quantity, 100)

        consumableevent = consumable.itemevent_set.first()
        consumableevent.details["title"] = "New Consumable"

        consumable.name = "Name Changed"
        consumable.save()

        consumableevent = consumable.itemevent_set.latest('pk')
        consumableevent.details["title"] = "Update"

    def test_change_quantities(self):
        self.itemtemplate.is_asset = False
        self.itemtemplate.save()

        consumable = Consumable.objects.create(
            name="Consumable name",
            template=self.itemtemplate,
            contact=self.org.organization_users.first(),
            notes="Some notes",
            quantity=100
        )

        self.assertEqual(consumable.name, "Consumable name")
        self.assertEqual(consumable.quantity, 100)

        consumable.add_quantity(5)
        consumableevent = consumable.itemevent_set.latest('pk')
        consumableevent.details["title"] = "Update"
        self.assertEqual(consumableevent.details.get('custom_fields')[0]['value'],
                         'Updated: quantity changed from 100 to 105')

        consumable.deduct_quantity(10)
        consumableevent = consumable.itemevent_set.latest('pk')
        consumableevent.details["title"] = "Update"
        self.assertEqual(consumableevent.details.get('custom_fields')[0]['value'],
                         'Updated: quantity changed from 105 to 95')
