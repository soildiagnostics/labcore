from django.urls import path
from common.apihelper import apipath

from .apiviews import (APIItemEventsJSON, APIItemEventUpdateJSON, APIAllItemsEventsJSON)
from .views import InventoryList, AssetListJSON, ConsumableListJSON, TemplateListJSON, \
    ItemTemplateCreate, ItemTemplateUpdate, AssetCreate, AssetUpdate, ConsumableCreate, ConsumableUpdate, \
    ItemFileDropzone, ItemFileDownload

urlpatterns = [
    path('', InventoryList.as_view(), name="inventory_list"),
    path("assets/", AssetListJSON.as_view(), name="asset_json_list"),
    path("assets/new/", AssetCreate.as_view(), name="asset_create"),
    path("assets/<int:pk>/update/", AssetUpdate.as_view(), name="asset_update"),
    path("consumables/new/", ConsumableCreate.as_view(), name="consumable_create"),
    path("consumables/<int:pk>/update/", ConsumableUpdate.as_view(), name="consumable_update"),
    path("consumables/", ConsumableListJSON.as_view(), name="consumable_json_list"),
    path("templates/", TemplateListJSON.as_view(), name="template_json_list"),
    path("templates/new/", ItemTemplateCreate.as_view(), name="item_template_create"),
    path("templates/<int:pk>/update/", ItemTemplateUpdate.as_view(), name="item_template_update"),
    path("items/<int:pk>/fileupload/", ItemFileDropzone.as_view(), name="item_file_upload"),
    path("items/<int:pk>/filedownload/", ItemFileDownload.as_view(), name="item_file_download")
    # path('eventlist/<int:pk>/', FieldEventListJSON.as_view(), name="event_list_json"),
    # path('eventlist/', FieldEventListJSON.as_view(), name="event_list_json_all"),
    # path('<int:pk>/download/', FieldEventFileDownload.as_view(), name="fieldevent_file"),
]

## API urls

urlpatterns += [
    apipath('item/events/', APIAllItemsEventsJSON.as_view(), name="api_all_item_events_json"),
    apipath('item/<int:pk>/events/', APIItemEventsJSON.as_view(), name="api_item_events_json"),
    apipath('item/<int:pk>/events/update_event/<event>/', APIItemEventUpdateJSON.as_view(),
            name="api_item_event_update"),
]
