import json

from address.models import AddressField
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import UniqueConstraint, Q
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext as _
from tagging.registry import register

from associates.models import Organization, OrganizationUser

# Create your models here.
from common.models import ParseTimeMixin, ModelDiffMixin
from customnotes.models import CustomField
from customnotes.serializers import CustomFieldSerializer
from fieldcalendar.event_base import BaseEvent, CalendarEventKind, CalendarEvent
from products.models import MeasurementUnit


class ItemTemplate(models.Model):
    description = models.CharField(verbose_name=_('Description'), max_length=64)
    brand = models.CharField(verbose_name=_('Brand'), max_length=32, null=True, blank=True)
    model = models.CharField(verbose_name=_('Model'), max_length=32, null=True, blank=True)
    part_number = models.CharField(verbose_name=_('Part number'), max_length=32, null=True, blank=True)
    notes = models.TextField(verbose_name=_('Notes'), null=True, blank=True)
    suppliers = models.ManyToManyField(Organization, verbose_name=_('Suppliers'),
                                       related_name="suppliers", blank=True)
    owner = models.ForeignKey(Organization, help_text=_("Owner organization"),
                              on_delete=models.CASCADE, related_name="itemtemplate",
                              blank=True, null=True)
    is_asset = models.BooleanField(verbose_name=_('Is Asset'), default=True)
    custom_fields = GenericRelation(CustomField, related_query_name='item_template')

    class Meta:
        ordering = ['description']

    def __str__(self):
        return self.description

    def get_absolute_url(self):
        return reverse("item_template_update", kwargs={"pk": self.id})


def default_cf():
    return {'custom_fields': list()}


class Item(ModelDiffMixin, models.Model):
    capture_diff_for_fields = "__all__"

    name = models.CharField(max_length=50, verbose_name=_("Name"))
    template = models.ForeignKey(ItemTemplate, on_delete=models.PROTECT)
    location = models.CharField(max_length=100, blank=True, null=True,
                                verbose_name=_("Location"),
                                help_text=_("e.g. Shelf 214"))
    address = AddressField(on_delete=models.CASCADE, blank=True, null=True)
    contact = models.ForeignKey(OrganizationUser, on_delete=models.PROTECT, blank=True, null=True)
    notes = models.TextField(verbose_name=_('Notes'), null=True, blank=True)
    serial_number = models.CharField(max_length=100, verbose_name=_('Serial'), null=True, default="")
    group = models.ManyToManyField('self', blank=True,
                                   help_text=_("Combine multiple assets into a Group"))
    quantity = models.IntegerField(verbose_name=_("Quantity"), null=True, default=1)
    units = models.ForeignKey(MeasurementUnit, on_delete=models.PROTECT, blank=True, null=True, related_name="myunit")
    is_asset = models.BooleanField()
    additional_details = models.JSONField(default=default_cf,
                                   help_text=_("Do not modify directly"))

    class Meta:
        constraints = [
            UniqueConstraint(fields=["serial_number", "template"],
                             condition=Q(is_asset=True),
                             name="serial_number_unique")
        ]
        unique_together = ('serial_number', 'template')

    def __str__(self):
        return "{} {}".format(self.name, self.serial_number)

    def save(self, *args, **kwargs):
        kind = "Asset" if self.is_asset else "Consumable"
        if not self.pk:
            obj = super().save(**kwargs)
            ItemEvent.objects.log_entry(item=self, title=f"New {kind}: {repr(self)}",
                                        entry="{} recorded: {}".format(kind, self.__str__()))
            return obj
        elif self.pk and self.has_changed:
            ItemEvent.objects.log_entry(item=self, title=f"Updated {repr(self)}",
                                        entry="Updated: {}".format(self.diff_text))
            obj = super().save(**kwargs)
            return obj
        else:
            return super().save(**kwargs)

    @property
    def owner(self):
        return self.template.owner

    def get_value_for_cf(self, cf):
        assert cf in self.template.custom_fields.all(), "Custom field not in template"
        try:
            for item in self.additional_details.get('custom_fields'):
                if item.get('id') == cf.id:
                    return item['value']
            return None
        except TypeError:
            return None


class AssetManager(models.Manager):

    def create(self, *args, **kwargs):
        kwargs.update({"is_asset": True})
        assert kwargs.get('template').is_asset, "Not an Asset Template"
        return super(AssetManager, self).create(*args, **kwargs)

    def get(self, *args, **kwargs):
        kwargs.update({"is_asset": True})
        return super(AssetManager, self).get(*args, **kwargs)

    def all(self):
        return super(AssetManager, self).filter(is_asset=True)


class Asset(Item):
    capture_diff_for_fields = "__all__"
    objects = AssetManager()

    class Meta:
        proxy = True

    def __str__(self):
        return "{} {}".format(self.name, self.serial_number)

    @property
    def owner(self):
        return self.template.owner

    def get_absolute_url(self):
        return reverse("asset_update", kwargs={"pk": self.id})


register(Asset)


class ConsumableManager(models.Manager):

    def create(self, *args, **kwargs):
        kwargs.update({"is_asset": False})
        assert not kwargs.get('template').is_asset, "Not a Consumable Template"
        return super(ConsumableManager, self).create(*args, **kwargs)

    def get(self, *args, **kwargs):
        kwargs.update({"is_asset": False})
        return super(ConsumableManager, self).get(*args, **kwargs)

    def all(self, *args, **kwargs):
        return super(ConsumableManager, self).filter(is_asset=False)


class Consumable(Item):
    capture_diff_for_fields = "__all__"
    objects = ConsumableManager()

    class Meta:
        proxy = True

    def add_quantity(self, delta):
        """
        delta can be plus or minus; will create the right event record.
        """
        self.quantity += delta
        self.save()

    def deduct_quantity(self, delta):
        self.add_quantity(-delta)

    def _str__(self):
        return "{}, {} {}".format(self.name, self.quantity, self.units.unit if self.units else "units")

    def get_absolute_url(self):
        return reverse("consumable_update", kwargs={"pk": self.id})


register(Consumable)


class ItemEventManager(ParseTimeMixin, models.Manager):

    def get_events_in_window(self, field, start=None, end=None):
        events = self.model.objects.filter(field=field)
        if start:
            events = events.filter(start__gte=self.parse_with_tz(start))
        if end:
            events = events.filter(end__lte=self.parse_with_tz(end))
        return events

    def create_system_event(self, item, title, start, end, allDay=True, editable=False):

        details = {
            "id": "",
            "end": end.isoformat(),
            "color": CalendarEventKind.objects.get(name="System Event").color,
            "start": start.isoformat(),
            "title": title,
            "allDay": allDay,
            "item_id": item.pk,
            "className": [],
            'editable': editable,
            "custom_fields": []}

        fe = self.model.objects.create(
            item=item,
            start=start,
            end=end,
            editable=editable,
            allDay=allDay,
            event_id="To be replaced",
            details=details
        )

        return fe

    def log_entry(self, item, title, entry):
        start = now()
        end = start
        fe = self.create_system_event(item, title, start, end, allDay=False)
        event_kind = CalendarEventKind.objects.get(name="System Event")
        event, _ = CalendarEvent.objects.get_or_create(name="Log Entry", kind=event_kind)

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                            object_id=event.id,
                                                            name="Entry",
                                                            help_text="Details for the Log Entry")

        cf_sr = CustomFieldSerializer(custom_field).data
        cf_sr['value'] = entry

        fe.event_type = event
        fe.details['custom_fields'].append(cf_sr)
        fe.save()
        return fe


class ItemEvent(BaseEvent):
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    objects = ItemEventManager()
