# Create your views here.
from django.contrib.contenttypes.forms import generic_inlineformset_factory
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import TemplateView, CreateView, UpdateView, DetailView, DeleteView
from django_datatables_view.base_datatable_view import BaseDatatableView

from associates.role_privileges import FilteredQuerySetMixin, DealershipRequiredMixin
from common.searchquery import SearchQueryAssemblyMixin
from customnotes.models import CustomField
from fieldcalendar.event_base import CalendarEventKind
from fieldcalendar.views import FieldEventFileDownload
from inventory.models import Asset, ItemTemplate, Consumable, Item, ItemEvent
from inventory.forms import ItemTemplateForm, AssetForm, ConsumableForm


class InventoryList(TemplateView):
    template_name = "inventory/inventory_list.html"


class AssetListJSON(DealershipRequiredMixin,
                    SearchQueryAssemblyMixin, BaseDatatableView):
    model = Asset
    columns = ['name',
               'template',
               'location',
               'contact',
               'notes']
    order_columns = ['name',
                     'template',
                     'location',
                     'contact',
                     'notes']
    max_display_length = 500
    # instance_owner_organization = "originator__organization"
    # instance_owner_client = 'client'
    # client_has_permission = False
    search_fields = [
        'name',
        'template__description',
        'template__brand',
        'template__model',
        'template__part_number',
        'template__notes'
    ]

    def get_initial_queryset(self):
        return Asset.objects.all()

    def render_column(self, row, column):

        if column == 'template':
            link = f"""<a href="{reverse('item_template_update',
                                         kwargs={"pk": row.template.id})}" 
                        title="View Template Details">{row.template.description}</a>"""
            return link
        else:
            return super().render_column(row, column)


class TemplateListJSON(DealershipRequiredMixin,
                       SearchQueryAssemblyMixin, BaseDatatableView):
    model = ItemTemplate
    columns = ['description',
               'brand',
               'model',
               'part_number',
               'notes']
    order_columns = ['description',
                     'brand',
                     'model',
                     'part_number',
                     'notes']
    max_display_length = 500
    # instance_owner_organization = "originator__organization"
    # instance_owner_client = 'client'
    # client_has_permission = False
    search_fields = [
        'description',
        'brand',
        'model',
        'part_number',
        'notes'
    ]

    def render_column(self, row, column):
        if column == 'description':
            if row.is_asset:
                link = f"""<a href="{reverse('asset_create')}?item_template_id={row.id}" 
                            title="Create a new asset of type {row.description}"
                            class="pull-right"><i class="fa fa-tractor"></i></a>"""
            else:
                link = f"""<a href="{reverse('consumable_create')}?item_template_id={row.id}" 
                            title="Create a new consumable of type {row.description}"
                            class="pull-right"><i class="fa fa-flask"></i></a>"""
            return f"{super().render_column(row, column)} {link}"
        else:
            return super().render_column(row, column)


class ConsumableListJSON(DealershipRequiredMixin,
                         SearchQueryAssemblyMixin, BaseDatatableView):
    model = Consumable
    columns = ['name',
               'quantity',
               'template',
               'location',
               'contact',
               'notes']
    order_columns = ['name',
                     'quantity',
                     'template',
                     'location',
                     'contact',
                     'notes'
                     ]
    max_display_length = 500
    search_fields = [
        'name',
        'template__description',
        'template__brand',
        'template__model',
        'template__part_number',
        'template__notes'
    ]

    def render_column(self, row, column):
        if column == 'template':
            link = f"""<a href="{reverse('item_template_update',
                                         kwargs={"pk": row.template.id})}" 
                        title="View Template Details">{row.template.description}</a>"""
            return link
        elif column == "quantity":
            return f"{row.quantity} {row.units}"
        else:
            return super().render_column(row, column)


class ItemTemplateCreate(DealershipRequiredMixin, CreateView):
    model = ItemTemplate
    form_class = ItemTemplateForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["form_heading"] = "Add a New Item Template"
        context['breadcrumb_tag'] = "Templates"

        return context

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['ouser'] = self.orguser
        return kw


class ItemTemplateUpdate(DealershipRequiredMixin, UpdateView):
    model = ItemTemplate
    form_class = ItemTemplateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_heading"] = "View and Edit Item Template"
        context['breadcrumb_tag'] = "Templates"
        context['related_records_heading'] = "Items using this Template"
        return context

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['ouser'] = self.orguser
        return kw


class ItemCreate(DealershipRequiredMixin, CreateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_heading"] = f"Add a New {self.model.__name__}"
        context['breadcrumb_tag'] = f"{self.model.__name__}s"
        item_template_id = self.request.GET.get('item_template_id', self.object.template.id if self.object else None)
        if item_template_id:
            item_template = ItemTemplate.objects.get(pk=item_template_id)
            custom_fields = CustomField.objects.filter(item_template=item_template)
            context['custom_fields_dict'] = custom_fields
        return context

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['ouser'] = self.orguser
        kw['template_id'] = self.request.GET.get('item_template_id')
        return kw


class ItemUpdate(ItemCreate, UpdateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_heading"] = f"Update {self.model.__name__}"
        context['events'] = CalendarEventKind.objects.user_editable(self.org, app="ItemEvent")
        context['files'] = self.get_object().itemevent_set.exclude(file="").order_by("-modified")
        return context

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['ouser'] = self.orguser
        kw['template_id'] = self.object.template.id
        return kw


class AssetCreate(ItemCreate):
    model = Asset
    form_class = AssetForm


class AssetUpdate(ItemUpdate):
    model = Asset
    form_class = AssetForm


class ConsumableCreate(ItemCreate):
    model = Consumable
    form_class = ConsumableForm

class ConsumableUpdate(ItemUpdate):
    model = Consumable
    form_class = ConsumableForm


class ItemFileDropzone(DealershipRequiredMixin, DetailView):

    def post(self, request, pk):
        try:
            item = Item.objects.get(pk=pk)
            file = self.request.FILES['file']

            fe = ItemEvent.objects.log_entry(item=item,
                                             title="Dropped File",
                                             entry=f"File {file.name} added")

            fe.file.save(f"inventory/{item.id}/{file.name}", file)
            fe.save()

            return JsonResponse({'status': 'success'})
        except Exception as e:
            return JsonResponse({"error": repr(e)})


class ItemFileDownload(FieldEventFileDownload):
    model = ItemEvent
