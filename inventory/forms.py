from address.forms import AddressField
from crispy_forms.bootstrap import FormActions
from django.forms import ModelForm, CharField, TextInput

from associates.models import Organization, OrganizationUser
from inventory.models import ItemTemplate, Asset, Consumable

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, ButtonHolder, Submit, Field, Fieldset
from .mixins import CustomFieldFormCleanMixin


class ItemTemplateForm(ModelForm):
    class Meta:
        model = ItemTemplate
        exclude = ["suppliers"]

    def __init__(self, *args, **kwargs):
        try:
            ouser = kwargs.pop("ouser")
        except KeyError:
            ouser = None
        super(ItemTemplateForm, self).__init__(*args, **kwargs)
        self.fields['owner'].queryset = Organization.objects.filter(is_active=True)
        if ouser:
            self.fields['owner'].initial = ouser.organization

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div(
                    "description",
                    "notes",
                    "owner",
                    "is_asset",
                    css_class="col-md-6"
                ),
                Div(
                    "brand",
                    "model",
                    "part_number",
                    css_class="col-md-6"
                ),
                css_class="col-md-12"
            ),
            Div(
                ButtonHolder(
                    Submit('submit', 'Save', css_class='button')
                ),
                css_class="col-md-12"
            )
        )


class ItemForm(CustomFieldFormCleanMixin, ModelForm):
    address = AddressField()

    def __init__(self, *args, **kwargs):
        ouser = kwargs.pop("ouser", None)
        template_id = kwargs.pop('template_id', None)
        super().__init__(*args, **kwargs)
        try:
            self.fields['contact'].queryset = self.fields['contact'].queryset.filter(organization=ouser.organization)
            ## Reduce queryset down to members of the logged-in's organization
        except Exception as e:
            pass
        self.helper = FormHelper()
        self.helper.form_tag = False
        if template_id:
            self.fields['template'].initial = ItemTemplate.objects.get(id=template_id)
        if ouser:
            self.fields['contact'].initial = ouser


class AssetForm(ItemForm):
    class Meta:
        model = Asset
        exclude = ("quantity", "units")

    def __init__(self, *args, **kwargs):
        super(AssetForm, self).__init__(*args, **kwargs)
        self.fields["is_asset"].initial = True
        self.helper.layout = Layout(
            Field("is_asset", type="hidden"),
            Div(
                Div(
                    "name",
                    "serial_number",
                    Field("template", readonly=True),
                    "notes",
                    css_class="col-md-6"
                ),
                Div(
                    "location",
                    "address",
                    "contact",
                    "group",
                    css_class="col-md-6"
                ),
                css_class="col-md-12"
            ),
            Field("additional_details", type="hidden"),
        )


class ConsumableForm(ItemForm):
    class Meta:
        model = Consumable
        exclude = ("serial_number", "group")

    def __init__(self, *args, **kwargs):
        super(ConsumableForm, self).__init__(*args, **kwargs)
        self.fields["is_asset"].initial = False
        self.helper.layout = Layout(
            Div(
                Field("is_asset", type="hidden"),
                Field("additional_details", type="hidden"),
                Div(
                    "name",
                    Div(
                        Div("quantity", css_class="col-md-6"),
                        Div("units", css_class="col-md-6"),
                        css_class="row"
                    ),
                    Field("template", readonly=True),
                    "notes",
                    css_class="col-md-6"
                ),
                Div(
                    "location",
                    "address",
                    "contact",
                    css_class="col-md-6"
                ),
                css_class="col-md-12"
            ),
        )
