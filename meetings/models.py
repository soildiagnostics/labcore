import datetime

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from associates.models import OrganizationUser

PRIORITIES = (
    ('H', 'High'),
    ('M', 'Medium'),
    ('L', 'Low'),
    ('X', 'NA or None'))


class Meeting(models.Model):
    name = models.CharField(max_length=200,
                            help_text=_("This field is searchable"))
    date = models.DateTimeField()
    duration = models.IntegerField(default=30, help_text=_("Minutes"))
    venue = models.CharField(max_length=200, default="Phone conference")
    telecon = models.CharField(max_length=200, blank=True)

    invitees = models.ManyToManyField(OrganizationUser, related_name="invitees")
    attendees = models.ManyToManyField(OrganizationUser, related_name="attendees", blank=True)

    def __str__(self):
        return "{}, {}".format(self.name, timezone.localtime(self.date).strftime("%a %m-%d-%Y, %I:%M %p"))

    def get_absolute_url(self):
        return reverse('meeting_details', args=[self.pk])

    @property
    def end_time(self):
        delta = datetime.timedelta(minutes=self.duration)
        return self.date + delta

    @property
    def when(self):
        return "{} to {}".format(timezone.localtime(self.date).strftime("%a %m-%d-%Y, %I:%M %p"),
                                 timezone.localtime(self.end_time).strftime("%I:%M %p"))

    def actions_for_user(self, ouser):
        return ActionItem.objects.filter(agenda_item__meeting=self, responsible__in=[ouser])

    def add_agenda(self, agenda_item):
        agenda_item.meeting = self
        agenda_item.serial = self.agenda_item.count() + 1
        agenda_item.item = "Follow up: {}".format(agenda_item.item)
        agenda_item.save()

    class Meta:
        ordering = ('-date',)

AGENDA_STATE = (
    ('S', 'Submitted'),
    ('A', 'Assigned to Meeting'),
    ('R', 'Resolved'),
    ('T', 'Tabled'),
    ('X', 'Will not be discussed'),
    ('C', 'Closed'),
)


class AgendaItem(models.Model):
    meeting = models.ForeignKey(
        Meeting, null=True, on_delete=models.CASCADE, related_name="agenda_item")
    serial = models.IntegerField(blank=True, null=True,
                                 help_text=_("Order in which you want the agenda items to appear"))
    item = models.CharField(max_length=200)
    priority = models.CharField(max_length=1, choices=PRIORITIES, default='M')
    created = models.DateTimeField(auto_now_add=True)
    proposed_by = models.ForeignKey(OrganizationUser, null=True, on_delete=models.SET_NULL)
    state = models.CharField(max_length=1, default='S', choices=AGENDA_STATE)

    def __str__(self):
        return self.item

    def save(self, *args, **kwargs):
        """ 
        Automatically set the meeting state to Assigned if
        there is a meeting assignment when submitted.  
        """
        if self.meeting and self.state == 'S':
            self.state = 'A'
        super().save(*args, **kwargs)

    class Meta:
        ordering = ('meeting', 'serial', 'created')
        unique_together = ('meeting', 'serial')

    


class ActionItem(models.Model):
    agenda_item = models.ForeignKey(
        AgendaItem, on_delete=models.CASCADE, related_name="action_item")
    responsible = models.ManyToManyField(OrganizationUser, related_name="responsible")
    action = models.TextField()
    due = models.DateTimeField(blank=True)
    priority = models.CharField(max_length=1, choices=PRIORITIES)
    follow_up = models.ForeignKey(Meeting, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}, {}".format(self.agenda_item, self.action)

    def save(self, *args, **kwargs):
        new_item = AgendaItem.objects.create(
            item="Follow up: {}; Report on action: {}".format(self.agenda_item.item, self.action)
        )
        if self.follow_up:
            self.follow_up.add_agenda(new_item)

        super().save(*args, **kwargs)


class Discussion(models.Model):
    user = models.ForeignKey(OrganizationUser, on_delete=models.SET_NULL, null=True)
    agenda_item = models.ForeignKey(AgendaItem, on_delete=models.CASCADE, related_name="discussion")
    created = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    resolution = models.BooleanField(default=False, help_text=_(
        "Check if this is a motion for resolution"))

    def save(self, *args, **kwargs):
        """ 
        Automatically set the agenda item state to Resolved if
        this discussion item is marked as a resolution.  
        """
        if self.resolution:
            self.agenda_item.state = 'R'
            self.agenda_item.save()
        super().save(*args, **kwargs)    
