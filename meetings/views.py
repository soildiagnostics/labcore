from django.views.generic import ListView, DetailView
from meetings.models import Meeting
from django.views.generic import ListView, DetailView

from meetings.models import Meeting


# Create your views here.
class MeetingList(ListView):
    model = Meeting

class MeetingDetail(DetailView):
    model = Meeting

