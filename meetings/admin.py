from django import forms
from django.contrib import admin

# Register your models here.
from .models import *


class ActionItemInline(admin.TabularInline):
    model = ActionItem
    filter_vertical = ('responsible',)
    extra = 0

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name=="responsible":
            org = OrganizationUser.objects.get(user=request.user).organization
            kwargs['queryset'] = OrganizationUser.objects.filter(organization=org)
        return super().formfield_for_manytomany(db_field, request, **kwargs)


class DiscussionInline(admin.TabularInline):
    model = Discussion
    autocomplete_fields = ('user',)

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    def get_changeform_initial_data(self, request):
        get_data = super(DiscussionInline,
                         self).get_changeform_initial_data(request)
        ouser = OrganizationUser.objects.get(user=request.user)
        get_data['user'] = ouser.pk
        return get_data

# admin.site.register(Discussion)

class AgendaItemInline(admin.TabularInline):
    model = AgendaItem
    fields = ('item', 'priority', 'proposed_by')
    show_change_link = True
    autocomplete_fields = ('proposed_by',)



@admin.register(AgendaItem)
class AgendaItemAdmin(admin.ModelAdmin):
    model = AgendaItem
    date_hierarchy = 'created'
    inlines = [
        DiscussionInline,
        ActionItemInline
    ]
    list_display = ('meeting', 'serial', 'state', 'item', 'proposed_by')
    list_editable = ('serial', 'state')
    list_filter = ('meeting', 'state',)
    autocomplete_fields = ('meeting',)
    search_fields = ('meeting', 'proposed_by')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name=="proposed_by":
            org = OrganizationUser.objects.get(user=request.user).organization
            kwargs['queryset'] = OrganizationUser.objects.filter(organization=org)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)




@admin.register(Meeting)
class MeetingAdmin(admin.ModelAdmin):
    model = Meeting
    inlines = [
        AgendaItemInline,
    ]
    #autocomplete_fields = ['attendees', 'invitees']
    filter_horizontal = ['attendees', 'invitees']

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    list_display = ('name', 'when', 'venue', 'my_responsibilities')
    search_fields = ('name',)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name=="invitees" or db_field.name=="attendees":
            org = OrganizationUser.objects.get(user=request.user).organization
            kwargs['queryset'] = OrganizationUser.objects.filter(organization=org)
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_queryset(self, request):
        self.request = request
        return super().get_queryset(request)

    def my_responsibilities(self, obj):
        ouser = OrganizationUser.objects.get(user=self.request.user)
        return ";".join([str(a) for a in obj.actions_for_user(ouser)])