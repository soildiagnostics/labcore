# Generated by Django 2.1.1 on 2018-10-15 00:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meetings', '0010_auto_20181014_1322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='agendaitem',
            options={'ordering': ('serial',)},
        ),
        migrations.AddField(
            model_name='agendaitem',
            name='serial',
            field=models.IntegerField(blank=True, help_text='Order in which you want the agenda items to appear', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='agendaitem',
            unique_together={('meeting', 'serial')},
        ),
    ]
