# Generated by Django 2.0.5 on 2018-05-21 21:36

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meetings', '0004_auto_20180521_1907'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discussion',
            name='agenda_item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to='meetings.AgendaItem'),
        ),
    ]
