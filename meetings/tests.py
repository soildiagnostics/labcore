import datetime

from django.db import IntegrityError
from django.test import Client
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from associates.models import OrganizationUser
from clients.tests.tests_models import ClientsTestCase
from .models import Meeting, AgendaItem, ActionItem, Discussion


# Create your tests here.

class MeetingsTestCase(TestCase):

    fixtures = ['roles']
    def setUp(self):

        ctc = ClientsTestCase()
        ctc.setUp()
        self.orguser = OrganizationUser.objects.get(user__username="testinguser")


    def test_create_meeting(self):

        t = timezone.now()
        m = Meeting.objects.create(
            name="test meeting",
            date=t,
            duration=60,
            venue="Home"
        )

        m.invitees.add(self.orguser)
        m.attendees.add(self.orguser)

        self.assertEqual("test meeting", m.name)
        self.assertIn(m.name, str(m))
        self.assertEqual(m.when, "{} to {}".format(timezone.localtime(t).strftime("%a %m-%d-%Y, %I:%M %p"),
                                                   timezone.localtime(m.end_time).strftime("%I:%M %p")))

    def test_add_agenda_item(self):

        m = Meeting.objects.create(
            name="test meeting",
            date=timezone.now(),
            duration=60,
            venue="Home"
        )

        a0 = AgendaItem.objects.create(
            serial=1,
            item="Item 1",
            priority="H",
            proposed_by=self.orguser
        )
        self.assertEqual(a0.state, "S")

        a1 = AgendaItem.objects.create(
            meeting=m,
            serial=1,
            item="Item 1",
            priority="H",
            proposed_by=self.orguser
        )

        self.assertEqual(a1.state, "A")
        self.assertEqual(str(a1), "Item 1")

        a2 = AgendaItem.objects.create(
            meeting=m,
            serial=2,
            item="Item 2",
            priority="H",
            proposed_by=self.orguser
        )

        self.assertEqual(a2.state, "A")

        self.assertEqual(m.agenda_item.first(), a1)

        with self.assertRaises(IntegrityError):
            a3 = AgendaItem.objects.create(
                meeting=m,
                serial=1,
                item="Item 3",
                priority="H",
                proposed_by=self.orguser
            )

    def test_add_action_item(self):
        m = Meeting.objects.create(
            name="test meeting",
            date=timezone.now(),
            duration=60,
            venue="Home"
        )

        a1 = AgendaItem.objects.create(
            meeting=m,
            serial=1,
            item="Item 1",
            priority="H",
            proposed_by=self.orguser
        )

        m2 = Meeting.objects.create(
            name="test meeting2",
            date=timezone.now(),
            duration=60,
            venue="Home"
        )

        action = ActionItem.objects.create(
            agenda_item=a1,
            action="Action taken",
            priority="H",
            due=timezone.now() + datetime.timedelta(days=4),
        )

        self.assertEqual(str(action), "Item 1, Action taken")

        faction = ActionItem.objects.create(
            agenda_item=a1,
            action="Action taken",
            priority="H",
            due=timezone.now() + datetime.timedelta(days=4),
            follow_up=m2
        )

        self.assertEqual(m2.agenda_item.count(), 1)

    def test_add_discussion(self):

        m = Meeting.objects.create(
            name="test meeting",
            date=timezone.now(),
            duration=60,
            venue="Home"
        )

        a1 = AgendaItem.objects.create(
            meeting=m,
            serial=1,
            item="Item 1",
            priority="H",
            proposed_by=self.orguser
        )

        d = Discussion.objects.create(
            user=self.orguser,
            agenda_item=a1,
            text="Some discussion",
            resolution=False
        )
        self.assertNotEqual(a1.state, 'R')


        d = Discussion.objects.create(
            user=self.orguser,
            agenda_item=a1,
            text="Some resolution",
            resolution=True
        )

        self.assertEqual(a1.state, 'R')


class MeetingsViews(TestCase):

    fixtures = ['roles']

    def setUp(self):

        ctc = ClientsTestCase()
        ctc.setUp()
        self.orguser = OrganizationUser.objects.get(user__username="testinguser")

        m = Meeting.objects.create(
            name="test meeting",
            date=timezone.now(),
            duration=60,
            venue="Home"
        )

        a1 = AgendaItem.objects.create(
            meeting=m,
            serial=1,
            item="Item 1",
            priority="H",
            proposed_by=self.orguser
        )

        d = Discussion.objects.create(
            user=self.orguser,
            agenda_item=a1,
            text="Some discussion",
            resolution=False
        )

        d = Discussion.objects.create(
            user=self.orguser,
            agenda_item=a1,
            text="Some resolution",
            resolution=True
        )

    def test_admin_views(self):

        c = Client()
        self.orguser.user.is_superuser = True
        self.orguser.user.is_staff = True
        self.orguser.user.save()

        c.force_login(self.orguser.user)

        pages = [
            reverse("admin:meetings_meeting_changelist"),
            reverse("admin:meetings_meeting_add"),
            reverse("admin:meetings_agendaitem_changelist"),
            #reverse("admin:meetings_agendaitem_add"), # TODO look into Media Conflict Warning here.
            "/meetings/",
            Meeting.objects.get(name="test meeting").get_absolute_url()
        ]

        for page in pages:
            response = c.get(page)
            self.assertEqual(response.status_code, 200)


