from django.urls import path

from meetings.views import MeetingList, MeetingDetail

urlpatterns = [
    path('', MeetingList.as_view()),
    path('<int:pk>/', MeetingDetail.as_view(), name="meeting_details"),
]