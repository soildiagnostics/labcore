# Serializers

from django.contrib.auth.models import User
from organizations.models import *
from rest_framework import serializers

from .models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class OrganizationUserSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)

    class Meta:
        model = OrganizationUser
        fields = ['user', 'organization']


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ['name']


class JarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jar
        fields = '__all__'



class BarCodeReaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = BarCodeReader
        fields = '__all__'


class WeighScaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeighScale
        fields = '__all__'


class FertiSaverNFillingStationSerializer(serializers.ModelSerializer):
    class Meta:
        model = FertiSaverNFillingStation
        exclude = [
            'notes',
            'template',
            'address',
            'contact',
            'group'
        ]


class FertiSaverNScanningStationSerializer(serializers.ModelSerializer):
    class Meta:
        model = FertiSaverNScanningStation
        exclude = ['key',
                   'template',
                   'notes',
                   'address',
                   'contact',
                   'group']


class FilledSamplePostSerialzer(serializers.Serializer):
    jar = serializers.CharField(required=True, max_length=100)
    filling_station_id = serializers.CharField(required=True, max_length=100)
    filling_weight = serializers.CharField(required=True, max_length=100)
    sample_id = serializers.CharField(required=True, max_length=100)


class FilledSampleSerializer(serializers.ModelSerializer):
    jar = JarSerializer()
    filled_by = OrganizationUserSerializer()
    filled_at = OrganizationSerializer()

    class Meta:
        model = FilledSample
        fields = '__all__'

class FertiSaverNScanSerialzerPost(serializers.Serializer):
    jar = serializers.CharField(required=True, max_length=100)
    reading_station_id = serializers.CharField(required=True, max_length=100)
    raw_reading = serializers.CharField(required=True, max_length=2048)

class FertiSaverNScanSerializer(serializers.ModelSerializer):
    jar = JarSerializer(many=False)
    read_by = OrganizationUserSerializer(many=False)
    read_at = OrganizationSerializer(many=False)
    # filled_sample = serializers.SerializerMethodField()
    filled_sample = FilledSampleSerializer(source='get_filled_sample',
                                           many=False, required=False)
    fsn_value = serializers.SerializerMethodField()
    quality = serializers.SerializerMethodField()

    # fsn_value = FilledSampleSerializer(source='get_filled_sample',
    #                                   many=False, required=False)

    def get_filled_sample(self, obj):
        return obj.get_filled_sample()

    def get_fsn_value(self, obj):
        return obj.get_fsn_value()

    def get_quality(self, obj):
        return obj.get_read_quality()

    class Meta:
        model = FertiSaverNScan
        exclude = ['raw_reading', 'raw_hsv']


class CalibrationSampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalibrationSample
        fields = '__all__'


class ScannerCalibrationSerializer(serializers.ModelSerializer):
    dark_read = FertiSaverNScanSerializer(many=False)
    bright_read = FertiSaverNScanSerializer(many=False)
    standard1 = CalibrationSampleSerializer(many=False)
    standard2 = CalibrationSampleSerializer(many=False)
    standard3 = CalibrationSampleSerializer(many=False)

    class Meta:
        model = ScannerCalibration
        fields = '__all__'


class CalibratedReadSerializer(serializers.ModelSerializer):
    filled_sample = FilledSampleSerializer(many=False)
    calibration = ScannerCalibrationSerializer(many=False)
    sample_scan = FertiSaverNScanSerializer(many=False)

    class Meta:
        model = CalibratedRead
        fields = '__all__'


class AssayInventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = AssayInventory
        fields = '__all__'
