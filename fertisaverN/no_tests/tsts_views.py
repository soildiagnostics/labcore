import json

from django.test import Client
from django.test import TestCase
from django.urls import reverse

from associates.models import Organization, OrganizationUser
from contact.models import Company, Person
from fertisaverN.serializers import *
from fertisaverN.tests.tests_models import FertiSaverNModelTests


# @skip('Fertisaver tests need work')
class FertiSaverNViewsTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        FertiSaverNModelTests().setUp()
        self.client = Client()
        admin_user = User.objects.get(username="testinguser")
        admin_user.is_active
        admin_user.is_staff
        admin_user.is_superuser
        admin_user.save()
        self.client.force_login(admin_user)

    def failing_test_admin_view(self):
        password = 'mypassword'
        my_admin = User.objects.create_superuser('myadminuser', 'myemail@test.com', password)
        c = Client()
        # You'll need to log him in before you can send requests through the client
        c.login(username=my_admin.username, password=password)
        admin_urls = [reverse("admin:app_list", kwargs={'app_label': 'fertisaverN'}),
                      reverse("admin:fertisaverN_fertisavernscan_changelist"),
                      reverse("admin:fertisaverN_calibratedread_changelist")]
        for url in admin_urls:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    def test_filler_view(self):
        response = self.client.get(reverse("filling"))
        self.assertNotEqual(response.context['form'], None)

    def test_filler_info_json(self):
        response = self.client.get(reverse("filler_info", kwargs={"serial_number": "F01001"}))
        self.assertEqual(response.json()["serial_number"], "F01001")
        response = self.client.get(reverse("filler_info", kwargs={"serial_number": "F0101"}))
        self.assertEqual(response.json(), {'detail': 'Not found.'})

    def test_filled_sample_list_json(self):

        urls = [
            "/fertisaverN/filling/sampledata/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=sample_id&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=fill_date&columns%5B1%5D%5Bsearchable%5D=false&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=filled_by&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=filling_weight&columns%5B3%5D%5Bsearchable%5D=false&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=1&order%5B0%5D%5Bdir%5D=fill_date&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1539901005111",
            "/fertisaverN/filling/sampledata/?draw=2&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=sample_id&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=fill_date&columns%5B1%5D%5Bsearchable%5D=false&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=filled_by&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=filling_weight&columns%5B3%5D%5Bsearchable%5D=false&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=1&order%5B0%5D%5Bdir%5D=fill_date&start=0&length=10&search%5Bvalue%5D=d&search%5Bregex%5D=false&_=1539916288201"
        ]
        for url in urls:
            response = self.client.get(url)
            self.assertTrue(response.status_code, 200)


    def test_sample_fill(self):

        data = {"sample_id": "testSampleId",
                "filling_weight": "1.5",
                "jar": "testjar",
                "filling_station_id": "F01001"}

        response = self.client.post(reverse("fill_sample"), data)
        self.assertEqual(response.json()['msg'], "Done")

        data = {"sample_id": "testSampleId",
                "jar": "testjar",
                "filling_station_id": "F01001"}

        response = self.client.post(reverse("fill_sample"), data)
        self.assertEqual(response.json(), {'filling_weight': ['This field is required.']})

        CalibrationBatch.objects.create(
            sample_type='S0',
            sample_name='zero',
            ISNT_value='0'
        )

        data = {"sample_id": "zero",
                "filling_weight": "1.5",
                "jar": "testjar2",
                "filling_station_id": "F01001"}

        response = self.client.post(reverse("fill_sample"), data)
        self.assertEqual(response.json()['calibration'], True)

    def test_reader_view(self):
        response = self.client.get(reverse("reading"))
        self.assertNotEqual(response.context['form'], None)


    def test_scanned_sample_list_json(self):

        urls = [
            "/fertisaverN/reading/sampledata/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=sample_id&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=read_date&columns%5B1%5D%5Bsearchable%5D=false&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=fsn_number&columns%5B2%5D%5Bsearchable%5D=false&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1539916652947",
            "/fertisaverN/reading/sampledata/?draw=2&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=sample_id&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=read_date&columns%5B1%5D%5Bsearchable%5D=false&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=fsn_number&columns%5B2%5D%5Bsearchable%5D=false&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=d&search%5Bregex%5D=false&_=1539916652948"
        ]
        for url in urls:
            response = self.client.get(url)
            self.assertTrue(response.status_code, 200)

    def test_scanner_info_json(self):
        response = self.client.get(reverse("reader_info", kwargs={"serial_number": "R01001"}))
        self.assertEqual(response.json()["serial_number"], "R01001")
        response = self.client.get(reverse("reader_info", kwargs={"serial_number": "F0101"}))
        self.assertEqual(response.json(), {'detail': 'Not found.'})

    def test_scanned_sample_only(self):
        jar_key = Jar.objects.create(serial_number="test_jar").serial_number
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()
        cal = reader.last_calibrated()
        cal.create_model()

        raw_read = "3031323334353637383930313233343534C4D1706AB8FB48AD50BB2312FC5726C9E60B36927A4A8D13D6014814C32B4C4A8FEC4CD4BF10049ACD440488CF55A3"

        data = {"raw_reading": raw_read,
                "jar": jar_key,
                "reading_station_id": "R01001"
                }
        response = self.client.post(reverse("scan_sample"), data)
        self.assertEqual(response.json()['status'], 'success')
        self.assertIn('Filled sample not found', response.json()['message'], )

    def test_filled_scanned_sample(self):

        data = {"sample_id": "testSampleId",
                "filling_weight": "1.5",
                "jar": "testjar",
                "filling_station_id": "F01001"}

        response = self.client.post(reverse("fill_sample"), data)
        self.assertEqual(response.json()['msg'], "Done")

        jar_key = Jar.objects.get(serial_number="testjar").serial_number
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()
        cal = reader.last_calibrated()
        cal.create_model()

        raw_read = "3031323334353637383930313233343534C4D1706AB8FB48AD50BB2312FC5726C9E60B36927A4A8D13D6014814C32B4C4A8FEC4CD4BF10049ACD440488CF55A3"

        data = {"raw_reading": raw_read,
                "jar": jar_key,
                "reading_station_id": "R01001"
                }

        response = self.client.post(reverse("scan_sample"), data)
        resp = response.json()
        old_id = resp['scan']['id']
        self.assertEqual(resp['scan']['fsn_value'], 338)
        scan = FertiSaverNScan.objects.get(id=old_id)
        serdat = FertiSaverNScanSerializer(scan)
        fs = serdat.get_filled_sample(scan)
        self.assertEqual(fs.jar.serial_number, 'testjar')

        # Rescan test here.
        response = self.client.post(reverse("scan_sample"), data)
        resp = response.json()
        self.assertEqual(resp['scan']['fsn_value'], 338)
        self.assertNotEqual(resp['scan']['id'], old_id)

        # Test jar not found.
        data = {"raw_reading": raw_read,
                "jar": "bogus jar",
                "reading_station_id": "R01001"
                }
        response = self.client.post(reverse("scan_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(
            resp['message'], 'Jar not found, scan rejected')

        # GET request.
        data = {"raw_reading": raw_read,
                "jar": "bogus jar",
                "reading_station_id": "R01001"
                }
        response = self.client.get(reverse("scan_sample"), data)
        self.assertEqual(
            response.json()['detail'], 'Method "GET" not allowed.')

        # invalid form
        data = {"raw_reading": raw_read,
                "reading_station_id": "R01001"
                }
        response = self.client.post(reverse("scan_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(
            resp['message'], 'invalid form')

    def test_data_view(self):

        response = self.client.get(reverse("data"))
        self.assertEqual(response.status_code, 200)

    def test_calibrated_read_list_json(self):

        urls = [
            "/fertisaverN/data/calibratedreads/?draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&start_time=&end_time=&_=1540150556240",
            "/fertisaverN/data/calibratedreads/?draw=3&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=j&search%5Bregex%5D=false&start_time=2018-10-21T05%3A00%3A00.000Z&end_time=2018-10-24T04%3A59%3A59.000Z&_=1540150556242"
        ]

        for url in urls:
            response = self.client.get(url)
            self.assertTrue(response.status_code, 200)

    def test_calibrate_view(self):

        response = self.client.get(reverse("calibrate"))
        self.assertEqual(response.status_code, 200)

    def test_get_last_calibration(self):

        response = self.client.get(reverse("last_calibration", kwargs={
            "serial_number": "R01001"}))
        resp = json.loads(response.content)
        self.assertEqual(resp, [[0, 7.893], [129, 6.282], [224, 4.0267], [289, 3.8083]])
        response = self.client.get(reverse("last_calibration", kwargs={
            "serial_number": "R00001"}))
        resp = json.loads(response.content)
        self.assertEqual(resp, {'error': "No data"})

    def test_delete_calibration(self):

        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()
        cal = ScannerCalibration.objects.all()[0]

        # Try deleting as non-owner of calibration
        newuser = User.objects.create(username="newuser",
                                      first_name="Testing",
                                      last_name="User",
                                      email="user@test.com",
                                      password="nothingsecret",
                                      is_active=True)
        newuser.save()
        Person.objects.create(user=newuser)

        c = Company.objects.create(name="SkyD")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)

        p = newuser.person
        p.company = c
        p.is_company_contact = True
        p.save()
        a.add_company_members_to_organization()

        newouser = OrganizationUser.objects.get(user=newuser)
        self.assertEqual(newouser.organization, a)
        self.client.force_login(newuser)

        response = self.client.post(reverse("delete_calibration", kwargs={"pk": cal.pk}))
        self.assertEqual(response.status_code, 403)


        # Now try deleting as owner of calibration
        self.client.force_login(User.objects.get(username="testinguser"))
        response = self.client.post(reverse("delete_calibration", kwargs={"pk": cal.pk}))
        self.assertEqual(response.status_code, 302)

        response = self.client.post(reverse("delete_calibration", kwargs={"pk": cal.pk+1}))
        self.assertEqual(response.status_code, 404)

    def test_calibration_detail_view(self):

        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()
        cal = ScannerCalibration.objects.last()

        url = reverse("details_calibration", kwargs={'pk':cal.pk})

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


    def test_calibration_sample_fill(self):

        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()
        data = {"sample_id": "default low",
                "filling_weight": "1.5",
                "jar": "testjar",
                "filling_station_id": "F01001"}
        response = self.client.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)



    def test_calibration_dragon(self):

        # Fill samples
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        # reader.generate_first_calibration()
        cal = reader.last_calibrated()
        cal.create_model()

        c = self.client

        data = {"sample_id": "default zero",
                "filling_weight": "2",
                "jar": "testjar0",
                "filling_station_id": "F01001"}

        response = c.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)

        data = {"sample_id": "default high",
                "filling_weight": "2",
                "jar": "testjarH",
                "filling_station_id": "F01001"}

        response = c.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)

        data = {"sample_id": "default low",
                "filling_weight": "2",
                "jar": "testjarL",
                "filling_station_id": "F01001"}

        response = c.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)

        data = {"sample_id": "default med",
                "filling_weight": "2",
                "jar": "testjarM",
                "filling_station_id": "F01001"}

        response = c.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)

        scheck = SensorCheckJar.objects.get(serial_number="SensorCheck")
        self.assertNotEqual(scheck.pk, None)

        data = {"raw_reading": "30313233343536373839303132333435F7E0340703FB8E67DBBB186D3FD0D84E42D981A64917BAEF2ADEDD1F315676D0943A810FB94D8D4E4D206AC181B43549",
                "jar": "BogusSensorCheckJar",
                "reading_station_id": "R01001",
                "reading_type": "dark"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['msg'], "Not a Sensor Check Jar")

        data = {"raw_reading": "30313233343536373839303132333435F7E0340703FB8E67DBBB186D3FD0D84E42D981A64917BAEF2ADEDD1F315676D0943A810FB94D8D4E4D206AC181B43549",
                "jar": scheck.serial_number,
                "reading_station_id": "R01001",
                "reading_type": "dark"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['reading_type'], "dark")
        dark_pk = resp['scan']['id']

        data = {"raw_reading": "3031323334353637383930313233343587F967B6F775866C419F0420448EA81702493536E0D17539654B3D7D3E2BB3338507E70A97DC13C374C3BB96409083F9",
                "jar": scheck.serial_number,
                "reading_station_id": "R01001",
                "reading_type": "bright"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['reading_type'], "bright")
        bright_pk = resp['scan']['id']

        data = {"raw_reading": "303132333435363738393031323334359A20BE27FDECF490774F9509E760B1E9D03DB79C39CCD09F05EB5FF380C044B8557E5AAB9ADDA6D8B0F7FCEBC2AEE81F",
                "jar": Jar.objects.get(serial_number="testjar0").serial_number,
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['sample'], "S0")
        standards_pk = []
        standards_pk.append(resp['scan']['id'])

        data = {"raw_reading": "3031323334353637383930313233343560D56E5106E9211293FF1ADF4A703B7BAD8C85F2FA10546E8DA41DE59F5AE4D486244750C8CBFE0573DA3EB9749464BA",
                "jar": Jar.objects.get(serial_number="testjarL").serial_number,
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['sample'], "S1")
        standards_pk.append(resp['scan']['id'])

        data = {"raw_reading": "3031323334353637383930313233343534C4D1706AB8FB48AD50BB2312FC5726C9E60B36927A4A8D13D6014814C32B4C4A8FEC4CD4BF10049ACD440488CF55A3",
                "jar": Jar.objects.get(serial_number="testjarM").serial_number,
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['sample'], "S2")
        standards_pk.append(resp['scan']['id'])

        data = {"raw_reading": "303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
                "jar": Jar.objects.get(serial_number="testjarH").serial_number,
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['sample'], "S3")
        standards_pk.append(resp['scan']['id'])

        data = {"dark_pk": dark_pk,
                "bright_pk": bright_pk,
                "standards[]": standards_pk,
                "reading_station_id": "R01001",
                "approve": True,
                }

        response = c.post(reverse("set_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration']['model'],
                         '508.7601,-63.2913')

        response = c.get(reverse("details_calibration", kwargs={
            "pk": resp['calibration']['id']}))
        self.assertEqual(response.status_code, 200)

        # cal = ScannerCalibration.objects.get(pk=resp['calibration']['id'])
        # cal.summary = None
        # cal.save()
        #
        # response = c.get(reverse("details_calibration", kwargs={
        #     "pk": resp['calibration']['id']}))
        # self.assertEqual(response.status_code, 200)
        # cal = ScannerCalibration.objects.get(pk=resp['calibration']['id'])
        # self.assertNotEqual(cal.summary, None)

        # Bad jar
        data = {"raw_reading": "303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
                "jar": "testjar_bogus",
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['msg'], 'Not a Calibration Sample Jar')

        # Bad form
        data = {"raw_reading": "303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = c.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['form_invalid'], 'true')

        # # Not a POST
        #
        # # Bad form
        # data = {"raw_reading": "303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
        #         "jar": Jar.objects.get(serial_number="testjarH").serial_number,
        #         "reading_station_id": "R01001",
        #         "reading_type": "standards"
        #         }
        #
        # response = c.get(reverse("sensor_calibration"), data)
        # print(response)
        # resp = json.loads(response.content)
        # self.assertEqual(resp['error'], 'Not a correctly formatted request')

        # Force delete calibrations.
    def test_force_delete_calibrations(self):
        # This test deletes existing calibrations, but
        # still proves that the system will generate
        # a calibration to prevent failure.

        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        reader.generate_first_calibration()

        data = {"sample_id": "default zero",
                "filling_weight": "2",
                "jar": "testjar0",
                "filling_station_id": "F01001"}

        response = self.client.post(reverse("fill_sample"), data)
        resp = json.loads(response.content)
        self.assertEqual(resp['calibration'], True)

        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")

        cals = ScannerCalibration.objects.filter(
            dark_read__reading_station=reader)
        for cal in cals:
            cal.delete()
        cals = ScannerCalibration.objects.filter(
            dark_read__reading_station=reader)
        self.assertEqual(len(cals), 0)

        data = {"raw_reading": "303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
                "jar": "testjar0",
                "reading_station_id": "R01001",
                "reading_type": "standards"
                }

        response = self.client.post(reverse("sensor_calibration"), data)
        resp = json.loads(response.content)
        self.assertIn('calibration sample found', resp['msg'])

        #
        #     def test_calibrated_reads_list(self):
        #
        #         c = self.set_permission("view_fsn_data", "user", "buckeyes")
        #         resp = c.get("/fertisavern/data/calibratedreads?draw=1&columns%5B0%5D%5Bdata%5D=filled_sample.sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=filled_sample.fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_sample.filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filled_sample.filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=sample_scan.read_date&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=sample_scan.read_by.user.username&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=fsn_value&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=calibration.date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&start_time=&end_time=&_=1508369516353")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/calibratedreads?draw=1&columns%5B0%5D%5Bdata%5D=filled_sample.sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=filled_sample.fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_sample.filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filled_sample.filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=sample_scan.read_date&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=sample_scan.read_by.user.username&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=fsn_value&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=calibration.date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&start_time=&end_time=&_=1508369516353")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/calibratedreads?draw=4&columns%5B0%5D%5Bdata%5D=filled_sample.sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=filled_sample.fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_sample.filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filled_sample.filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=sample_scan.read_date&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=sample_scan.read_by.user.username&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=fsn_value&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=calibration.date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&start_time=2017-10-04T05%3A00%3A00.000Z&end_time=2017-10-11T05%3A00%3A00.000Z&_=1508348382638")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/calibratedreads?draw=4&columns%5B0%5D%5Bdata%5D=filled_sample.sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=123&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=filled_sample.fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_sample.filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filled_sample.filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=sample_scan.read_date&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=sample_scan.read_by.user.username&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=fsn_value&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=calibration.date&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&start_time=2017-10-04T05%3A00%3A00.000Z&end_time=2017-10-11T05%3A00%3A00.000Z&_=1508348382638")
        #         self.assertEqual(resp.status_code, 200)
        #
        #     def test_filled_samples_list(self):
        #         c = self.set_permission("view_fsn_data", "user", "buckeyes")
        #         resp = c.get("/fertisavern/data/filledsamples?draw=1&columns%5B0%5D%5Bdata%5D=sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1508370718722")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/filledsamples?draw=1&columns%5B0%5D%5Bdata%5D=sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1508370718722")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/filledsamples?draw=1&columns%5B0%5D%5Bdata%5D=sample_id&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=434&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=fill_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=filled_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=filling_weight&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1508370718722")
        #         self.assertEqual(resp.status_code, 200)
        #
        #     def test_scanned_samples_list(self):
        #         c = self.set_permission("view_fsn_data", "user", "buckeyes")
        #         resp = c.get("/fertisavern/data/scannedsamples?draw=1&columns%5B0%5D%5Bdata%5D=jar.serial_number&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=read_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=read_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1508370864292")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/scannedsamples?draw=1&columns%5B0%5D%5Bdata%5D=jar.serial_number&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=read_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=read_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1508370864292")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/scannedsamples?draw=1&columns%5B0%5D%5Bdata%5D=jar.serial_number&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=read_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=read_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=4321&search%5Bregex%5D=false&_=1508370864292")
        #         self.assertEqual(resp.status_code, 200)
        #
        #         resp = c.get("/fertisavern/data/scannedsamples?draw=1&columns%5B0%5D%5Bdata%5D=jar.serial_number&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=read_date&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=read_by.user.username&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=123&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=10&search%5Bvalue%5D=4321&search%5Bregex%5D=false&_=1508370864292")
        #         self.assertEqual(resp.status_code, 200)
        #

