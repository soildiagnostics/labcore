from django.contrib.auth.models import User
from django.test import TestCase

from clients.tests.tests_models import ClientsTestCase
from fertisaverN.models import *


# Create your tests here.

# @skip('FertiSaverN tests are broken')
class FertiSaverNModelTests(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase().setUp()
        admin_user = User.objects.get(username="testinguser")
        ouser = OrganizationUser.objects.get(user=admin_user)
        admin_user.is_staff = True
        admin_user.save()
        user = User.objects.create_user(username='user', password='buckeyes')
        user.save()

        self.soilcenter = ouser.organization
        self.create_db_assets()

    def create_db_assets(self):
        Computer.objects.create(
            template=create_computer_template(owner=self.soilcenter),
            name="Chromebox",
            serial_number="123ABC"
        )

        Monitor.objects.create(
            template=create_monitor_template(owner=self.soilcenter),
            name="HP fancy",
            serial_number="123ABC"
        )

        WeighScale.objects.create(
            template=create_weigh_scale_template(owner=self.soilcenter),
            serial_number="123ABC"
        )

        BarCodeReader.objects.create(
            template=create_barcode_reader_template(owner=self.soilcenter),
            serial_number="bar121"
        )

        FertiSaverNFillingStation.objects.create(
            template=create_fertisaverN_filling_station_template(owner=self.soilcenter),
            name="FSN Filling Station",
            serial_number="F01001"
        )

        FertiSaverNScanningStation.objects.create(
            template=create_fertisaverN_scanning_station_template(owner=self.soilcenter),
            name="FSN Reading Station",
            serial_number="R01001",
            key="0123456789ABCDEF",
            last_used=timezone.now()
        )

        Jar.objects.create(
            owner=self.soilcenter,
            name="Jar",
            serial_number="test_jar"
        )

    def test_create_assets(self):
        self.assertNotEqual(Computer.objects.last().pk, None)
        self.assertNotEqual(Monitor.objects.last().pk, None)
        self.assertNotEqual(WeighScale.objects.last().pk, None)
        self.assertNotEqual(FertiSaverNFillingStation.objects.last().pk, None)
        self.assertNotEqual(FertiSaverNScanningStation.objects.last().pk, None)

    def test_device_string(self):
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        self.assertEqual(str(reader), "FSN Reading Station R01001")

    def test_create_jar(self):
        jar = Jar.objects.create(
            owner=self.soilcenter,
            name="Jar",
            serial_number="test_jar_uuid")
        self.assertEqual(str(jar), "Jar test_jar_uuid")

    def test_get_or_create_jar(self):
        jar, created = Jar.objects.get_or_create(
            owner=self.soilcenter,
            name="Jar",
            serial_number="test_jar_uuid")
        self.assertEqual(str(jar), "Jar test_jar_uuid")
        self.assertTrue(created)

        jar, created = Jar.objects.get_or_create(
            owner=self.soilcenter,
            name="Jar",
            serial_number="test_jar_uuid")
        self.assertEqual(str(jar), "Jar test_jar_uuid")
        self.assertFalse(created)

    def test_create_sensor_check_jar(self):
        jar = SensorCheckJar.objects.create(
            template=create_sensorcheck_jar_template(owner=self.soilcenter),
            name="CheckJar",
            serial_number="test_jar_uuid")
        self.assertEqual(str(jar), "CheckJar test_jar_uuid")

    def test_create_scanned_sample(self):
        jar = Jar.objects.create(serial_number="test_jar_uuid", owner=self.soilcenter)
        ouser = OrganizationUser.objects.get(user__username="testinguser")

        scan = FertiSaverNScan.objects.create(
            jar=jar,
            reading_station=FertiSaverNScanningStation.objects.get(
                serial_number="R01001"),
            read_at=ouser.organization,
            read_by=ouser,
            raw_reading="30313233343536373839303132333435733B25F9DD6C2AE6D192AC50C72E1AB19E7C914B9C55892D6A88B7CAE937E6ABBBA5642845AAF18F35058838935FF515"
        )
        self.assertNotEqual(scan.id, None)

    def test_get_fsn_value(self):
        jar = Jar.objects.create(serial_number="test_jar_uuid", owner=self.soilcenter)
        ouser = OrganizationUser.objects.get(user__username="testinguser")

        sample = FilledSample.objects.create(
            sample_id="test sample id",
            filling_weight=2.05,
            jar=jar,
            filled_at=self.soilcenter,
            filled_by=OrganizationUser.objects.get(user__username="testinguser"),
            filling_station=FertiSaverNFillingStation.objects.get(
                serial_number="F01001")
        )

        self.assertNotEqual(sample.pk, None)

        scan = FertiSaverNScan.objects.create(
            jar=jar,
            reading_station=FertiSaverNScanningStation.objects.get(
                serial_number="R01001"),
            read_at=ouser.organization,
            read_by=ouser,
            raw_reading="30313233343536373839303132333435733B25F9DD6C2AE6D192AC50C72E1AB19E7C914B9C55892D6A88B7CAE937E6ABBBA5642845AAF18F35058838935FF515"
        )
        self.assertNotEqual(str(scan), None)
        fsn = scan.get_fsn_value()
        self.assertEqual(fsn, 496)
        rq = scan.get_read_quality()
        self.assertEqual(rq, 3)
        self.assertEqual(scan.get_saturation(), '0.0000')

    def test_create_filled_sample(self):
        jar = Jar.objects.create(serial_number="test_jar_uuid", owner=self.soilcenter)
        jar.save()

        sample = FilledSample.objects.create(
            sample_id="test sample id",
            filling_weight=2.05,
            jar=jar,
            filled_at=self.soilcenter,
            filled_by=OrganizationUser.objects.get(user__username="testinguser"),
            filling_station=FertiSaverNFillingStation.objects.get(
                serial_number="F01001")
        )

        self.assertNotEqual(sample.pk, None)
        self.assertEqual(str(sample), "test sample id")

    def test_default_calibration(self):
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        cal = reader.last_calibrated()
        self.assertNotEqual(cal.id, None)
        cal = reader.last_calibrated()
        self.assertNotEqual(cal.summary, None)
        self.assertNotEqual(reader.get_last_calibration_data(), None)

    def test_last_calibrated_before(self):
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        cal = reader.last_calibrated_before(timezone.now())
        self.assertNotEqual(cal.id, None)
        cal2 = reader.last_calibrated_before(timezone.now())
        self.assertEqual(cal, cal2)

    def test_cal_renormalize(self):
        reader = FertiSaverNScanningStation.objects.get(serial_number="R01001")
        cal = reader.last_calibrated_before(timezone.now())
        cal.renormalize()
        cal2 = reader.last_calibrated_before(timezone.now())
        self.assertEqual(cal, cal2)

        self.assertNotEqual(str(cal), None)

    def test_increment_assay(self):
        assay = AssayInventory.objects.create(quantity=500, owner=self.soilcenter)
        assay.save()
        assay.add_kit(500)
        assay = AssayInventory.objects.get(template__owner=self.soilcenter)
        self.assertEqual(str(assay), "1000")

        self.assertEqual(assay.time_to_order(), "unknown days")

    def test_create_calibration_batch(self):
        cal = CalibrationBatch.objects.create(
            sample_type='S0',
            sample_name='zero',
            ISNT_value='0'
        )
        self.assertEqual(str(cal), "zero (S0)")

    def test_create_calibration_sample(self):
        jar = Jar.objects.create(serial_number="test_jar_uuid", owner=self.soilcenter)
        jar.save()

        sample = FilledSample.objects.create(
            sample_id="test sample id",
            filling_weight=2.05,
            jar=jar,
            filled_at=self.soilcenter,
            filled_by=OrganizationUser.objects.get(user__username="testinguser"),
            filling_station=FertiSaverNFillingStation.objects.get(
                serial_number="F01001")
        )

        cal = CalibrationBatch.objects.create(
            sample_type='S0',
            sample_name='zero',
            ISNT_value='0'
        )

        cs = CalibrationSample.objects.create(
            filled_sample=sample,
            calibration_batch=cal
        )

        self.assertEqual(str(cs), "test sample id")

    def test_create_calibrated_read(self):
        jar = Jar.objects.create(serial_number="test_jar_uuid", owner=self.soilcenter)
        jar.save()

        sample = FilledSample.objects.create(
            sample_id="test sample id",
            filling_weight=2.05,
            jar=jar,
            filled_at=self.soilcenter,
            filled_by=OrganizationUser.objects.get(user__username="testinguser"),
            filling_station=FertiSaverNFillingStation.objects.get(
                serial_number="F01001")
        )
        scan = FertiSaverNScan.objects.create(
            jar=jar,
            reading_station=FertiSaverNScanningStation.objects.get(
                serial_number="R01001"),
            read_at=OrganizationUser.objects.get(user__username="testinguser").organization,
            read_by=OrganizationUser.objects.get(user__username="testinguser"),
            raw_reading="30313233343536373839303132333435733B25F9DD6C2AE6D192AC50C72E1AB19E7C914B9C55892D6A88B7CAE937E6ABBBA5642845AAF18F35058838935FF515"
        )

        cr = CalibratedRead.objects.create(
            filled_sample=sample,
            calibration=scan.reading_station.last_calibrated(),
            sample_scan=scan
        )

        self.assertEqual(str(cr), "test sample id")
