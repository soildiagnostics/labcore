import ast
import datetime
import warnings
from datetime import timedelta

import pandas as pd
import pytz
import statsmodels.formula.api as sm
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

from associates.models import OrganizationUser, Organization
from inventory.models import Asset, ItemTemplate, Consumable, Item
from products.models import TestParameter
from samples.models import Sample, LabMeasurement
# this is for the decryption string.
from .crypt import get_hsv


def create_jar_item_template(owner):
    jartemplate, created = ItemTemplate.objects.get_or_create(
        description="SoilDx FertiSaver-N Incubation Jar",
        brand="Soil Diagnostics, Inc",
        model="Jar",
        part_number="Incubation Jar",
        owner=owner
    )

    return jartemplate


def create_sensorcheck_jar_template(owner):
    jartemplate, created = ItemTemplate.objects.get_or_create(
        description="SoilDx FertiSaver-N SensorCheck Jar",
        brand="Soil Diagnostics, Inc",
        model="SensorCheck Jar",
        part_number="SensorCheck Jar",
        owner=owner
    )

    return jartemplate


def create_device_template(**kwargs):
    device_template, created = ItemTemplate.objects.get_or_create(**kwargs)
    return device_template


def create_barcode_reader_template(owner):
    return create_device_template(description="Barcode Reader",
                                  owner=owner)


def create_weigh_scale_template(owner):
    t = create_device_template(description="Weigh Scale",
                               owner=owner)
    t.brand = "OHaus"
    t.model = "SPX223"
    t.save()
    return t


def create_computer_template(owner):
    return create_device_template(description="Chromebox computer",
                                  owner=owner)


def create_monitor_template(owner):
    return create_device_template(description="Computer monitor",
                                  owner=owner)


def create_fertisaverN_filling_station_template(owner):
    return create_device_template(description="FertisaverN filling station unit",
                                  owner=owner)


def create_fertisaverN_scanning_station_template(owner):
    return create_device_template(description="FertiSaverN scanning station unit",
                                  owner=owner)


class JarManager(models.Manager):

    def get_or_create(self, owner=None, *args, **kwargs):
        try:
            return super().get(template__owner=owner, *args, **kwargs), False
        except Jar.DoesNotExist:
            return self.create(owner=owner, *args, **kwargs), True

    def create(self, owner=None, *args, **kwargs):
        if 'template' in kwargs.keys():
            pass
        else:
            jtemplate = create_jar_item_template(owner)
            kwargs['template'] = jtemplate

        return super().create(*args, **kwargs)


class Jar(Item):
    objects = JarManager()

class SensorCheckJarManager(models.Manager):

    def get_or_create(self, owner=None, *args, **kwargs):
        try:
            return super().get(template__owner=owner, *args, **kwargs), False
        except SensorCheckJar.DoesNotExist:
            return self.create(owner=owner, *args, **kwargs), True

    def create(self, owner=None, *args, **kwargs):
        if 'template' in kwargs.keys():
            pass
        else:
            jtemplate = create_sensorcheck_jar_template(owner)
            kwargs['template'] = jtemplate

        return super().create(*args, **kwargs)

class SensorCheckJar(Jar):
    objects = SensorCheckJarManager()


class BarCodeReader(Item):
    pass


class WeighScale(Item):
    pass


class Computer(Item):
    pass


class Monitor(Item):
    pass


class FSDevice(Item):
    last_calibration = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True

    @property
    def owner(self):
        return self.template.owner


class FertiSaverNFillingStation(FSDevice):
    """ Just a different table for the filling station
        In the future this will have its own calibration information.
    """
    pass


class FertiSaverNScanningStation(FSDevice):
    last_used = models.DateTimeField(blank=True, null=True)
    key = models.CharField(max_length=32)

    def get_last_calibration_data(self):
        c = self.last_calibrated()
        return c.get_cal_vector()

    def last_calibrated(self):
        try:
            c = ScannerCalibration.objects.filter(
                dark_read__reading_station=self, active=True).latest('date')
        except ScannerCalibration.DoesNotExist:
            return self.generate_first_calibration()
        return c

    def last_calibrated_before(self, readdate):
        try:
            c = ScannerCalibration.objects.filter(
                dark_read__reading_station=self).filter(date__lte=readdate).latest('date')
            # print("found cal " + c)
        except ScannerCalibration.DoesNotExist:
            # print("last_cal_exc\n" + e)
            return self.generate_first_calibration()
        return c

    def generate_first_calibration(self):
        # create calibration batches
        b0, c = CalibrationBatch.objects.get_or_create(
            sample_type="S0",
            ISNT_value=0,
            sample_name="default zero")
        b0.save()
        b1, c = CalibrationBatch.objects.get_or_create(
            sample_type="S1",
            ISNT_value=129,
            sample_name="default low")
        b1.save()
        b2, c = CalibrationBatch.objects.get_or_create(
            sample_type="S2",
            ISNT_value=224,
            sample_name="default med")
        b2.save()
        b3, c = CalibrationBatch.objects.get_or_create(
            sample_type="S3",
            ISNT_value=289,
            sample_name="default high")
        b3.save()

        # Jars
        j0, created = Jar.objects.get_or_create(serial_number="fake jar0", owner=self.owner)
        j1, created = Jar.objects.get_or_create(serial_number="fake jar1", owner=self.owner)
        j2, created = Jar.objects.get_or_create(serial_number="fake jar2", owner=self.owner)
        j3, created = Jar.objects.get_or_create(serial_number="fake jar3", owner=self.owner)

        # filled samples
        f0, c = FilledSample.objects.get_or_create(
            sample_id="default zero",
            filling_weight=2.0,
            jar=j0,
            filled_at=self.owner,
            filled_by=self.owner.organization_users.first(),
            filling_station=FertiSaverNFillingStation.objects.filter(template__owner=self.owner).last())

        f1, c = FilledSample.objects.get_or_create(
            sample_id="default low",
            filling_weight=2.0,
            jar=j1,
            filled_at=self.owner,
            filled_by=self.owner.organization_users.first(),
            filling_station=FertiSaverNFillingStation.objects.filter(template__owner=self.owner).last())

        f2, c = FilledSample.objects.get_or_create(
            sample_id="default med",
            filling_weight=2.0,
            jar=j2,
            filled_at=self.owner,
            filled_by=self.owner.organization_users.first(),
            filling_station=FertiSaverNFillingStation.objects.filter(template__owner=self.owner).last())

        f3, c = FilledSample.objects.get_or_create(
            sample_id="default high",
            filling_weight=2.0,
            jar=j3,
            filled_at=self.owner,
            filled_by=self.owner.organization_users.first(),
            filling_station=FertiSaverNFillingStation.objects.filter(template__owner=self.owner).last())

        # Calibration samples
        cs0, c = CalibrationSample.objects.get_or_create(
            filled_sample=f0,
            calibration_batch=b0)
        cs1, c = CalibrationSample.objects.get_or_create(
            filled_sample=f1,
            calibration_batch=b1)
        cs2, c = CalibrationSample.objects.get_or_create(
            filled_sample=f2,
            calibration_batch=b2)
        cs3, c = CalibrationSample.objects.get_or_create(
            filled_sample=f3,
            calibration_batch=b3)

        # sample scans
        scheck, c = SensorCheckJar.objects.get_or_create(
            serial_number="SensorCheck",
            owner=self.owner)

        dark, c = FertiSaverNScan.objects.get_or_create(
            jar=scheck,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="30313233343536373839303132333435F7E0340703FB8E67DBBB186D3FD0D84E42D981A64917BAEF2ADEDD1F315676D0943A810FB94D8D4E4D206AC181B43549",
            chromic_index=0.0
        )

        bright, c = FertiSaverNScan.objects.get_or_create(
            jar=scheck,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="3031323334353637383930313233343587F967B6F775866C419F0420448EA81702493536E0D17539654B3D7D3E2BB3338507E70A97DC13C374C3BB96409083F9",
            chromic_index=1.0
        )

        s0, c = FertiSaverNScan.objects.get_or_create(
            jar=j0,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="303132333435363738393031323334359A20BE27FDECF490774F9509E760B1E9D03DB79C39CCD09F05EB5FF380C044B8557E5AAB9ADDA6D8B0F7FCEBC2AEE81F",
            chromic_index=7.8930
        )

        s1, c = FertiSaverNScan.objects.get_or_create(
            jar=j1,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="3031323334353637383930313233343560D56E5106E9211293FF1ADF4A703B7BAD8C85F2FA10546E8DA41DE59F5AE4D486244750C8CBFE0573DA3EB9749464BA",
            chromic_index=6.2820
        )

        s2, c = FertiSaverNScan.objects.get_or_create(
            jar=j2,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="3031323334353637383930313233343534C4D1706AB8FB48AD50BB2312FC5726C9E60B36927A4A8D13D6014814C32B4C4A8FEC4CD4BF10049ACD440488CF55A3",
            chromic_index=4.0267
        )

        s3, c = FertiSaverNScan.objects.get_or_create(
            jar=j3,
            reading_station=self,
            read_at=self.owner,
            read_by=self.owner.organization_users.first(),
            raw_reading="303132333435363738393031323334355C33466DE05997B6D463214B92203343171D0B956B130E628D7BBE8B8BB9FCC05D95143316488EFC6CDD1876C68D962C",
            chromic_index=3.8083
        )

        # print("new cal " + self.name)
        cal = ScannerCalibration(
            dark_read=dark,
            bright_read=bright,
            standard0=cs0,
            standard1=cs1,
            standard2=cs2,
            standard3=cs3,
        )
        cal.create_model()
        cal.save()
        d = datetime.datetime(2000, 1, 1, 0, 0, 0)
        d = pytz.timezone("America/Chicago").localize(d)
        cal.date = d
        cal.save()

        ## Bootstrap the scan so it has the raw hsv.
        for i in [s0, s1, s2, s3]:
            i.set_ci()
            i.save()

        return cal


class FertiSaverNScan(models.Model):
    jar = models.ForeignKey(Jar, on_delete=models.CASCADE, related_name="scan")
    reading_station = models.ForeignKey(
        FertiSaverNScanningStation, on_delete=models.CASCADE)
    read_date = models.DateTimeField(auto_now_add=True)
    read_at = models.ForeignKey(Organization, on_delete=models.CASCADE)
    read_by = models.ForeignKey(OrganizationUser, on_delete=models.CASCADE)
    raw_reading = models.CharField(max_length=200)
    chromic_index = models.DecimalField(null=True, max_digits=8, decimal_places=4)
    raw_hsv = models.CharField(max_length=200, null=True)

    def set_ci(self, calibration=None):
        hsv = self.hsv(calibration)
        try:
            self.chromic_index = hsv['Saturation'] / hsv['Value'] * 100
        except ZeroDivisionError:
            self.chromic_index = 0

        return self.chromic_index

    def hsv(self, calibration=None):
        # Set up the cipher object
        key = self.reading_station.key
        iv = '0123456789012345'
        if not calibration:
            calibration = self.reading_station.last_calibrated()
        hsv = get_hsv(key, iv, self.raw_reading, calibration)
        self.raw_hsv = str(hsv)
        return hsv
        # Decrypt and store

    def get_fsn_value(self):
        try:
            # This is available when there is a calibrated scan
            return self.calibrated_scan_value.fsn_value
        except Exception:
            # When there is no calibrated scan record,
            # such as when it is a calibration sample
            calibration = self.reading_station.last_calibrated_before(self.read_date)

            try:
                filled_sample = self.get_filled_sample()
                self.set_ci(calibration)
                fsn = calibration.get_fsn(self.chromic_index,
                                          filled_sample.filling_weight)
            except Exception:
                fsn = 0

            return fsn

    def _get_color_component(self, color):
        try:
            d = ast.literal_eval(self.raw_hsv)
            return "{0:.4f}".format(d[color])
        except Exception:
            return -1

    def get_hue(self):
        return self._get_color_component('Hue')

    def get_saturation(self):
        return self._get_color_component('Saturation')

    def get_value(self):
        return self._get_color_component('Value')

    def get_filled_sample(self):
        try:
            filled_sample = FilledSample.objects. \
                filter(jar=self.jar,
                       fill_date__lte=self.read_date,
                       fill_date__gt=self.read_date - timezone
                       .timedelta(days=2)).latest('fill_date')
            return filled_sample
        except FilledSample.DoesNotExist:
            return None

    def get_read_quality(self):
        h = float(self.get_hue())
        v = float(self.get_value())

        hq = min(abs(h + 1 - 0.98), 1 - (abs(h + 1 - 0.98))) * 500
        vq = abs(v - 0.88) * 100
        q = hq + vq
        quality = 0
        if q < 10:
            quality = 1
        if q < 5:
            quality = 2
        if q < 3:
            quality = 3
        if h == 0.0 and (v == 1.0 or v == 0):
            quality = 3
        return quality

    def __str__(self):
        return "Scan %s" % self.read_date


class FilledSample(models.Model):
    sample_id = models.CharField(max_length=30)
    filling_weight = models.FloatField()
    jar = models.ForeignKey(Jar, on_delete=models.CASCADE, related_name="filled_sample")

    filled_at = models.ForeignKey(Organization, on_delete=models.CASCADE)
    fill_date = models.DateTimeField(auto_now_add=True)
    filled_by = models.ForeignKey(OrganizationUser, on_delete=models.CASCADE)
    filling_station = models.ForeignKey(
        FertiSaverNFillingStation, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.sample_id


@receiver(pre_save, sender=FilledSample)
def decrement_assay_inventory(sender, instance, **kwargs):
    # print("triggered presave")
    # do your inventory logic here
    if instance.pk is None:
        # print("new instance")
        inventory, created = AssayInventory.objects.get_or_create(owner=instance.filled_at)
        inventory.decrement()


class CalibrationBatch(models.Model):
    SAMPLE_CHOICES = (
        ('S0', 'Standard 0'),
        ('S1', 'Standard 1'),
        ('S2', 'Standard 2'),
        ('S3', 'Standard 3'))

    sample_type = models.CharField(max_length=2, choices=SAMPLE_CHOICES)
    ISNT_value = models.IntegerField(help_text="ISNT value in ppm (integer)")
    sample_name = models.CharField(max_length=50)

    def __str__(self):
        return "%s (%s)" % (self.sample_name, self.sample_type)


class CalibrationSample(models.Model):
    filled_sample = models.OneToOneField(
        FilledSample, related_name='calibration_sample', on_delete=models.CASCADE)
    calibration_batch = models.ForeignKey(
        CalibrationBatch, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.filled_sample.sample_id


class ScannerCalibration(models.Model):
    # Note - all readings are one-to-one.
    date = models.DateTimeField(auto_now_add=True)
    dark_read = models.OneToOneField(
        FertiSaverNScan, related_name='dark_read', on_delete=models.CASCADE)
    bright_read = models.OneToOneField(
        FertiSaverNScan, related_name='bright_read', on_delete=models.CASCADE)
    standard0 = models.OneToOneField(
        CalibrationSample, related_name='standard0', on_delete=models.CASCADE, null=True)
    standard1 = models.OneToOneField(
        CalibrationSample, related_name='standard1', on_delete=models.CASCADE)
    standard2 = models.OneToOneField(
        CalibrationSample, related_name='standard2', on_delete=models.CASCADE)
    standard3 = models.OneToOneField(
        CalibrationSample, related_name='standard3', on_delete=models.CASCADE)
    model = models.CharField(max_length=100, blank=True)
    summary = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def get_cal_points(self):
        # try:
        #     read_0 = FertiSaverNScan.objects.filter(
        #         jar=self.standard0.filled_sample.jar).latest('read_date')
        # except Exception:
        #     ## backfill a missing zero standard.
        #     b0, c = CalibrationBatch.objects.get_or_create(
        #         sample_type="S0",
        #         ISNT_value=0,
        #         sample_name="default zero")
        #     b0.save()
        #     owner = self.dark_read.reading_station.owner
        #     j0 = Jar(uuid="fake jar", owner=owner)
        #     j0.save()
        #     f0 = FilledSample(
        #         sample_id="default zero",
        #         filling_weight=2.0,
        #         jar=j0,
        #         filled_at=owner,
        #         filled_by=owner.owner.organization_user,
        #         filling_station=FertiSaverNFillingStation.objects.filter(owner=owner)[
        #             0]
        #     )
        #     f0.save()
        #     s0 = FertiSaverNScan(
        #         jar=j0,
        #         reading_station=self.dark_read.reading_station,
        #         read_at=owner,
        #         read_by=owner.owner.organization_user,
        #         raw_reading="303132333435363738393031323334359A20BE27FDECF490774F9509E760B1E9D03DB79C39CCD09F05EB5FF380C044B8557E5AAB9ADDA6D8B0F7FCEBC2AEE81F",
        #         chromic_index=7.893
        #     )
        #     s0.save()
        #     cs0 = CalibrationSample(
        #         filled_sample=f0,
        #         calibration_batch=b0)
        #     cs0.save()
        #     self.standard0 = cs0
        #     self.save()
        #     read_0 = FertiSaverNScan.objects.filter(
        #         jar=self.standard0.filled_sample.jar).latest('read_date')

        read_0 = FertiSaverNScan.objects.filter(
            jar=self.standard0.filled_sample.jar).latest('read_date')
        read_1 = FertiSaverNScan.objects.filter(
            jar=self.standard1.filled_sample.jar).latest('read_date')
        read_2 = FertiSaverNScan.objects.filter(
            jar=self.standard2.filled_sample.jar).latest('read_date')
        read_3 = FertiSaverNScan.objects.filter(
            jar=self.standard3.filled_sample.jar).latest('read_date')

        dat = {'isnt': [self.standard0.calibration_batch.ISNT_value,
                        self.standard1.calibration_batch.ISNT_value,
                        self.standard2.calibration_batch.ISNT_value,
                        self.standard3.calibration_batch.ISNT_value],
               'chromic_index': [float(read_0.chromic_index),
                                 float(read_1.chromic_index),
                                 float(read_2.chromic_index),
                                 float(read_3.chromic_index)]}

        return dat

    def get_cal_vector(self):
        d = self.get_cal_points()
        ret = []
        for i in range(0, 4):
            pair = [d['isnt'][i], d['chromic_index'][i]]
            ret.append(pair)
        return ret

    def renormalize(self):
        """ Use the dark and bright reads to renormalize its own readings"""
        read_0 = FertiSaverNScan.objects.filter(
            jar=self.standard0.filled_sample.jar).latest('read_date')
        read_0.set_ci(calibration=self)
        read_0.save()

        read_1 = FertiSaverNScan.objects.filter(
            jar=self.standard1.filled_sample.jar).latest('read_date')
        read_1.set_ci(calibration=self)
        read_1.save()

        read_2 = FertiSaverNScan.objects.filter(
            jar=self.standard2.filled_sample.jar).latest('read_date')
        read_2.set_ci(calibration=self)
        read_2.save()

        read_3 = FertiSaverNScan.objects.filter(
            jar=self.standard3.filled_sample.jar).latest('read_date')
        read_3.set_ci(calibration=self)
        read_3.save()

    def create_model(self):

        dataframe = pd.DataFrame(self.get_cal_points())

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            ## Suppressing a warning since sample size < 8
            reg = sm.ols(formula='isnt ~ chromic_index',
                         data=dataframe)
            regression = reg.fit()
            self.summary = regression.summary()
        self.model = '{0:.4f},{1:.4f}'.format(regression.params['Intercept'],
                                    regression.params['chromic_index'])
        self.save()
        return self.model

    def get_fsn(self, chromic_index, weight=2.0):
        coefs = self.model.split(',')
        fsn = float(coefs[0]) + float(coefs[1]) * chromic_index
        fsn = fsn / weight * 2.0
        return int(fsn)

    def __str__(self):
        return "Calibration %s" % self.date


class CalibratedRead(models.Model):
    filled_sample = models.OneToOneField(
        FilledSample, related_name='calibration_filled_sample', on_delete=models.CASCADE)
    calibration = models.ForeignKey(
        ScannerCalibration, on_delete=models.CASCADE)
    sample_scan = models.OneToOneField(
        FertiSaverNScan, related_name='calibrated_scan_value', on_delete=models.CASCADE)
    fsn_value = models.IntegerField(
        help_text="FSN value in ppm (integer)", null=True)

    def get_fsn_value(self):
        chromic_index = self.sample_scan.chromic_index
        weight = self.filled_sample.filling_weight
        try:
            fsn = self.calibration.get_fsn(chromic_index, weight)
        except Exception:
            fsn = chromic_index
        self.fsn_value = fsn
        return fsn

    def save(self, *args, **kwargs):
        self.get_fsn_value()
        try:
            sample = Sample.objects.get(id=self.filled_sample.sample_id)
            param, _ = TestParameter.objects.get_or_create(name="Organic N")
            LabMeasurement.objects.create(
                sample=sample,
                parameter=param,
                value=self.fsn_value
            )
        except Sample.DoesNotExist:
            pass
        except ValueError:
            pass

        super(CalibratedRead, self).save(*args, **kwargs)

    def __str__(self):
        return "%s" % self.filled_sample.__str__()


def create_assay_template(owner):
    template, created = ItemTemplate.objects.get_or_create(
        description="FertisaverN Assay Reagent",
        brand="SoilDx",
        model="4% Boric Acid reagent",
        owner=owner
    )
    return template


class AssayInventoryManager(models.Manager):
    def get_or_create(self, owner=None, *args, **kwargs):
        try:
            return (super().get(template__owner=owner, *args, **kwargs), False)
        except AssayInventory.DoesNotExist:
            return (self.create(owner=owner, *args, **kwargs), True)

    def create(self, owner=None, *args, **kwargs):
        if not 'template' in kwargs.keys():
            template = create_assay_template(owner)
            kwargs['template'] = template

        if not 'quantity' in kwargs.keys():
            kwargs['quantity'] = 0

        return super().create(*args, **kwargs)


class AssayInventory(Item):
    objects = AssayInventoryManager()

    def time_to_order(self):
        # inventory divided by average number of fills of last 7 days
        num_fills_avg = FilledSample.objects. \
                            filter(filled_at=self.owner,
                                   fill_date__gte=timezone.now() - timedelta(days=7)). \
                            count() / 7
        try:
            time_in_days = "about " + \
                           str(int(self.quantity / num_fills_avg)) + " days"
        except ZeroDivisionError:
            time_in_days = "unknown days"

        return time_in_days

    def add_kit(self, size):
        self.quantity = self.quantity + size
        self.save()

    def decrement(self):
        self.quantity -= 1
        self.save()

    def __str__(self):
        return "%s" % self.quantity


class BilledSamplesSummary(CalibratedRead):
    class Meta:
        proxy = True
        verbose_name = "Billed Sample"
        verbose_name_plural = "Billed Samples"
