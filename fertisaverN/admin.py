from django.contrib import admin
from django.db.models import Count

from .actions import export_as_csv_action
from .models import *


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('serial_number', 'owner')
    list_filter = ('template__owner',)
    search_fields = ['name', 'hardware', 'serial_number']
    autocomplete_fields = ('contact', 'template')


admin.site.register(Jar, DeviceAdmin)
admin.site.register(SensorCheckJar, DeviceAdmin)
admin.site.register(BarCodeReader, DeviceAdmin)
admin.site.register(WeighScale, DeviceAdmin)
admin.site.register(Computer, DeviceAdmin)
admin.site.register(Monitor, DeviceAdmin)


@admin.register(FertiSaverNFillingStation)
class FertiSaverNFillingStationAdmin(admin.ModelAdmin):
    list_display = ('name', 'serial_number', 'last_calibration',)
    list_filter = ('last_calibration',)
    search_fields = ['name']
    autocomplete_fields = ('template', 'contact',)


@admin.register(FertiSaverNScanningStation)
class FertiSaverNScanningStationAdmin(admin.ModelAdmin):
    list_display = ('name', 'serial_number', 'last_calibration', 'last_used')
    list_filter = ('last_used',)
    search_fields = ['name']
    date_hierarchy = 'last_used'
    autocomplete_fields = ('template', 'contact')


@admin.register(FertiSaverNScan)
class FertiSaverNScanAdmin(admin.ModelAdmin):
    list_display = ('read_date', 'sample_id', 'read_at', 'read_by',
                    'reading_station', 'chromic_index', 'quality', 'hue', 'saturation', 'value')
    list_filter = ('read_date', 'read_at__name', 'reading_station__name',)
    search_fields = ['reading_station__name', 'sample_id']
    date_hierarchy = 'read_date'
    raw_id_fields = ('jar', 'reading_station', 'read_at', 'read_by')

    def quality(self, obj):
        return obj.get_read_quality()

    quality.admin_order_field = 'quality'
    quality.short_description = 'Quality'

    def hue(self, obj):
        return obj.get_hue()

    hue.admin_order_field = 'hue'
    hue.short_description = 'Hue'

    def saturation(self, obj):
        return obj.get_saturation()

    saturation.admin_order_field = 'saturation'
    saturation.short_description = 'Saturation'

    def value(self, obj):
        return obj.get_value()

    value.admin_order_field = 'value'
    value.short_description = 'Value'

    def sample_id(self, obj):
        return obj.get_filled_sample()

    sample_id.admin_order_field = 'sample_id'
    sample_id.short_description = 'Sample ID'


class FilledSampleAdmin(admin.ModelAdmin):
    list_display = ('sample_id', 'filling_weight', 'filled_at',
                    'fill_date', 'filled_by', 'filling_station',)
    list_filter = ('filled_by__user__username',)
    show_full_result_count = ('sample_id')
    search_fields = ['filling_station__name']
    date_hierarchy = 'fill_date'
    raw_id_fields = ('jar', 'filled_at', 'filled_by', 'filling_station')


admin.site.register(FilledSample, FilledSampleAdmin)


class ScannerCalibrationAdmin(admin.ModelAdmin):
    list_display = ('date', 'active', 'get_owner', 'get_scanner',
                    'standard0', 'standard1',
                    'standard2', 'standard3', 'model')
    list_filter = ('date', 'dark_read__reading_station__template__owner',
                   'dark_read__reading_station')
    show_full_result_count = ('serial')
    search_fields = ['serial', 'date']
    date_hierarchy = 'date'
    raw_id_fields = ('dark_read', 'bright_read', 'standard0',
                     'standard1', 'standard2', 'standard3')

    def get_owner(self, obj):
        return obj.dark_read.reading_station.owner

    def get_scanner(self, obj):
        return obj.dark_read.reading_station


admin.site.register(ScannerCalibration, ScannerCalibrationAdmin)


class CalibratedReadAdmin(admin.ModelAdmin):
    list_display = ('filled_sample', 'calibration', 'sample_scan', 'fsn_value', 'quality',
                    'sample_ci', 'sample_weight', 'hue', 'saturation', 'value')
    list_filter = ('filled_sample__filled_at', 'filled_sample__filled_by',)
    show_full_result_count = ('filled_sample')
    search_fields = ['filled_sample__sample_id', ]
    date_hierarchy = 'filled_sample__fill_date'
    actions = [export_as_csv_action("CSV Export",
                                    fields=['filled_sample',
                                            'calibration',
                                            'sample_scan',
                                            'fsn_value',
                                            'scample_scan__chromic_index',
                                            'filled_sample__filling_weight',
                                            'hue',
                                            'saturation',
                                            'value'])]
    raw_id_fields = ('filled_sample', 'calibration', 'sample_scan')

    def sample_ci(self, obj):
        return obj.sample_scan.chromic_index

    sample_ci.admin_order_field = 'sample_ci'
    sample_ci.short_description = 'Chromic Index'

    def sample_weight(self, obj):
        return obj.filled_sample.filling_weight

    sample_weight.admin_order_field = 'sample_weight'
    sample_weight_short_description = 'Sample Weight'

    def hue(self, obj):
        return obj.sample_scan.get_hue()

    hue.admin_order_field = 'hue'
    hue.short_description = 'Hue'

    def saturation(self, obj):
        return obj.sample_scan.get_saturation()

    saturation.admin_order_field = 'saturation'
    saturation.short_description = 'Saturation'

    def value(self, obj):
        return obj.sample_scan.get_value()

    value.admin_order_field = 'value'
    value.short_description = 'Value'

    def quality(self, obj):
        return obj.sample_scan.get_read_quality()

    quality.admin_order_field = 'quality'
    quality.short_description = 'Quality'


admin.site.register(CalibratedRead, CalibratedReadAdmin)


class CalibrationBatchAdmin(admin.ModelAdmin):
    list_display = ('sample_name', 'sample_type', 'ISNT_value')
    list_filter = ('sample_type',)
    search_fields = ['sample_name', 'sample_type']


admin.site.register(CalibrationBatch, CalibrationBatchAdmin)


class CalibrationSampleAdmin(admin.ModelAdmin):
    list_display = ('filled_sample', 'calibration_batch',)
    list_filter = ('filled_sample',)
    search_fields = ['filled_sample', 'calibration_batch']
    date_hierarchy = 'filled_sample__fill_date'
    raw_id_fields = ('filled_sample', 'calibration_batch')


admin.site.register(CalibrationSample, CalibrationSampleAdmin)


class AssayInventoryAdmin(admin.ModelAdmin):
    list_display = ('owner', 'quantity', "time_to_order")
    autocomplete_fields = ('template', 'contact')


admin.site.register(AssayInventory, AssayInventoryAdmin)


@admin.register(BilledSamplesSummary)
class BilledSampleAdmin(admin.ModelAdmin):
    change_list_template = "admin/billed_sample_summary.html"
    date_hierarchy = 'filled_sample__fill_date'
    list_filter = ('filled_sample__filled_at',)

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': Count('filled_sample__sample_id', distinct=True)
        }
        response.context_data['summary'] = list(
            qs
                .values('filled_sample__filled_at__name')
                .annotate(**metrics)
                .order_by('-total')
        )

        return response
