from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import *


class FillingForm(forms.ModelForm):
    jar = forms.CharField(initial='Jar ID')

    class Meta:
        model = FilledSample
        fields = ['sample_id', 'filling_weight']
        labels = {'sample_id': _("Sample ID"),
                  'filling_weight': _("Filling weight (mg)")}


class ScanningForm(forms.Form):
    jar = forms.CharField(initial='Jar ID')
