$(document).ready(function() {
    $("#button_scan").click(function() {
	$("#imgscan").html("Scanning (fake!) nematode slide");
        $.get('/scan', function(data, status) {
	    if(data.stderr) {alert(data.stderr);}
	    else {
		console.log(data.stdout);
		$("#imgscan").html("Analyzing (fake!) image");
		$.get('/analyze', function(data, status) {
	    	  if(data.stderr) {alert(data.stderr);}
	    	  else {
		    console.log(data.stdout);
		    $("#imgscan").html("Num Eggs: " + data.stdout); 
	    	  }
		});
	    }
	});
    });
})