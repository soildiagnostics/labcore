$('#post-form').on('submit', function(event){
    event.preventDefault();
    beep();
    create_post();
});

function reset_form() {
    $('#id_sample_id').val(''); // remove the value from the input
    $('#id_filling_weight').val('');
    $('#id_jar').val('');
    $('#id_jar').prop('readonly', false);
}

$('#reset_form').on('click', function(event){
    event.preventDefault();
    beep();
    reset_form();
});

$('#post-form input[name=optradio]').on('change', function() {
    alert('Mode now set to ' + $('input[name=optradio]:checked', '#post-form').val());
    Cookies.set('filling-form', $('input[name=optradio]:checked', '#post-form').val());
});

// AJAX for posting
function create_post() {
    Cookies.set('lastSample', $('#id_sample_id').val());
    row_add = function(json) {
        var sample_string = json.obj.sample_id;
        if (json.calibration) {
            sample_string += '<span class="label label-success pull-right">Calibration</span>'
        }
        var t = $('#fillingstation').DataTable();
        t.row.add( [
            sample_string,
            date_string(json.obj.fill_date),
            json.obj.filling_weight
        ] ).draw( false );
        t.order([1, 'desc']).draw();
    }
    $.ajax({
        url : "/fertisavern/fill/", // the endpoint
        type : "POST", // http method
        data : { sample_id : $('#id_sample_id').val(),
            filling_weight: $('#id_filling_weight').val(),
            jar: $('#id_jar').val(),
            filling_station_id: $('#filling_station_id').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            reset_form();
            if(json.obj) {
                $("#results").removeClass("alert alert-danger");
                $("#results").addClass("alert alert-success");
                $("#results").html("Sample added <a href='#' class='close'>&times;</a>");
                if(json.created) {
                    $("#results").html("Sample added and New jar added <a href='#' class='close'>&times;</a>");
                }
                if(Cookies.get('filling-form') == 'autoincrement') {
                    new_id = parseInt(json.obj.sample_id) + 1;
                    $('#id_sample_id').val(new_id);
                }
                row_add(json);
            }
            else if(json.form_invalid) {
                $("#results").removeClass("alert alert-success");
                $("#results").addClass("alert alert-danger");
                $('#results').html("Form invalid! <a href='#' class='close'>&times;</a>");
            } else {
                $("#results").removeClass("alert alert-success");
                $("#results").addClass("alert alert-danger");
                $('#results').html("Undetermined error");
            }
            //console.log(json)
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};


// Code to detect barcode scan - automatically populate sample_id field if true
$(document).ready(function() {
    // Initialize Example 2
    // May 26, 2016, 4:16 p.m.
    $("#id_sample_id").addClass('input-lg');
    filling_form_state = Cookies.get('filling-form');
    $("input[name=optradio][value=" + filling_form_state + "]").prop('checked', true);
    if(filling_form_state == 'autoincrement') {
        new_id = parseInt(Cookies.get('lastSample')) + 1;
        $('#id_sample_id').val(new_id);
    }
    $.fn.dataTable.moment('ddd, MMM D YYYY, HH:mm:ss a');
    $('#fillingstation').DataTable({
        order: [[ 1, "fill_date" ]],
        columnDefs: [
            {
                name: "sample_id",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "fill_date",
                orderable: true,
                searchable: false,
                targets: [1]
            },
            {
                name: "filled_by",
                orderable: true,
                searchable: true,
                targets: [2]
            },
            {
                name: "filling_weight",
                orderable: false,
                searchable: false,
                targets: [3]
            }
        ],
        'paging': true,
        "processing": true,
        "serverSide": true,
        "ajax": "/fertisaverN/filling/sampledata/",

    });

    var pressed = false;
    var chars = [];
    $(window).keypress(function(e) {
        //console.log(e.which);
        if(Cookies.get('filling-form') == 'barcode') {
            console.log('barcodes?');
            if (e.which >= 48 && e.which <= 57) {
                chars.push(String.fromCharCode(e.which));
                console.log(chars);
            }
            if (pressed == false) {
                setTimeout(function(){
                    if (chars.length >= 4) {
                        var barcode = chars.join("");
                        console.log("Barcode Scanned: " + barcode);
                        // assign value to some input (or do whatever you want)
                        $("#id_sample_id").val(barcode);
                    }
                    chars = [];
                    pressed = false;
                },500);
            }
            pressed = true;
        }
    });
});

$("#barcode").keypress(function(e){
    if ( e.which === 13 ) {
        console.log("Prevent form submit.");
        e.preventDefault();
    }
});

$(document).keypress(function(e) {
    if(e.which == 13) {
        beep();
        create_post();
    }
});
