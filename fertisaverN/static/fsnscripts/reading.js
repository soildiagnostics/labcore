$('#post-form').on('submit', function(event){
    event.preventDefault();
    beep();
    create_post();
});

function reset_form() {
    $('#id_jar').val('');
    $('#id_jar').prop('readonly', false); 
    $('#raw_reading').val('');
}

$('#reset_form').on('click', function(event){
    event.preventDefault();
    beep();
    reset_form();
});

$('#raw_reading').on('change', function(event){
    //console.log($('#raw_reading').val()); 
    beep(); 
    create_post(); 
});

function quality(v) {
    //console.log(v);ß
    var label_type = [  "label-danger", 
                        "label-danger", 
                        "label-warning", 
                        "label-warning", 
                        "label-success"][v]

    return ('<span class="label ' + label_type + ' pull-right">' + v.toString() + '</span>');
};
// AJAX for posting
function create_post() {
    $.ajax({
        url : "/fertisavern/scan/", // the endpoint
        type : "POST", // http method
        
        data : { raw_reading: $('#raw_reading').val(),
                 jar: $('#id_jar').val(),
                 reading_station_id: $('#reading_station_id').val() 
                 }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            reset_form(); 
            if(json.scan) {
                $("#results").removeClass("alert alert-success"); 
                $("#results").html("");
                var t = $('#readingstation').DataTable();
                var sample_name = json.scan.filled_sample.sample_id ? json.scan.filled_sample.sample_id : "None";
                var fsn_string = json.scan.fsn_value + quality(json.scan.quality);
                t.row.add( [
                    sample_name,
                    date_string(json.scan.read_date),
                    fsn_string
                ] ).draw( true );
            }
            if(json.form_invalid) {
                $("#results").addClass("alert alert-danger"); 
                $('#results').html("Form invalid! <a href='#' class='close'>&times;</a>");
            } else {
                $("#results").removeClass("alert alert-danger"); 
                $('#results').html("");
            }
            console.log(json);
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};

$(document).ready(function() {
    $.fn.dataTable.moment('ddd, MMM D YYYY, HH:mm:ss a');
        $('#readingstation').DataTable({
        //order: [[ 1, "read_date" ]],
        columnDefs: [
            {
                name: "sample_id",
                orderable: false,
                searchable: true,
                targets: [0]
            },
            {
                name: "read_date",
                orderable: false,
                searchable: false,
                targets: [1]
            },
            {
                name: "fsn_number",
                orderable: false,
                searchable: false,
                targets: [2]
            }
        ],
        'paging': true,
        "processing": true,
        "serverSide": true,
        "ajax": "/fertisaverN/reading/sampledata/",

    });
});