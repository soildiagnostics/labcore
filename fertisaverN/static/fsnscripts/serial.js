// got this stuff from Johnny Five
// load this file from the server.
/* global ChromeApiProxy */
'use strict';

var extensionId = 'pcngagajhlocaamcdoableokimllkacl'; // this is the WebStore one
    if (document.domain === 'localhost') {
        var extensionId = 'bpjhcenniagegmcahpkmgiconbcnbfld';
        // development server connector
    }

(function(exports) {
    // just attaches the global ChromeApiProxy object to the screen
    // didn't change anything here...
  function ChromeApiProxy(extensionId) {
    this._events = new Map();
    this._port = chrome.runtime.connect(extensionId);
  }

  ChromeApiProxy.prototype = {
    _events: null,
    _port: null,

    call: function(api) {
      // Convert to an Array.
      //console.log("args: ");
      //console.log(arguments);
      var params = Array.prototype.slice.call(arguments);
      var port = this._port;
      var callId = this._uuid();
      return new Promise(function(resolve) {
        //console.log("length " + params.length);
        if (params.length > 1) {
          params.shift();
        } else {
          params = null;
        }
        //console.log("api: " + api);
        //console.log("params: " + params);
        port.postMessage({
          callId: callId, call: api, params: params
        });
        port.onMessage.addListener(function(message) {
          message.callId === callId && resolve(message.data);
        });
      });
    },

    listenSerialPort: function(callback) {
        callback && this._port.onMessage.addListener(function(message) {
        //console.log(message);
        message.type === 'serial' && callback(message.info);
      });
    },

    _uuid: function() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        .replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });
    }
  };

  exports.ChromeApiProxy = ChromeApiProxy;
}(window));

// Globals

var proxy = new ChromeApiProxy(extensionId);
var incoming_string = "";
var listening = false;
var connection_id;

//console.log(extensionId);
chrome.runtime.sendMessage(extensionId, { launch: true }, {}, function(response) {
    console.log(response);
    //location.reload();
    });

function get_serial_devices() {
    //console.log('fired gsd');
    proxy.call('chrome.serial.getDevices')
        .then(function(devices) {
             $("#install-button").hide();
             for(var i = 0; i < devices.length; i++) {
                var dev = devices[i];
                var el = $('<option>');
                el.val(dev.path).text(dev.path).appendTo($("#ports_list"));
                if (dev.path.match('/\/dev\/tty|\/dev\/cu.usb|COM/') || devices.length == 1) {
                    // check possible paths here for windows.
                    el.prop('selected', true);
                    }
                }
            return connect_selected();
            });
}

function log(msg) {
    // helper function
  var buffer = document.querySelector('#serial_message');
  buffer.innerHTML += msg + '<br />';
}

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function connect_selected() {
    //disconnect_current();
    proxy.call('chrome.serial.getConnections')
      .then(function(info) {
        //console.log(info);

        for(var conn in info) {
            //console.log(info[conn]);
            disconnect_current(info[conn].connectionId);
        }
        //console.log('Connecting to:');
        var path = $("#ports_list").val();
        //console.log(path);
        var options = {
            bitrate: 9600,
            bufferSize: 4096
        }
        proxy.call('chrome.serial.connect', path, options)
            .then(function(info) {
            //console.log("buttons");
            console.log(info);
            try {
                connection_id = info.connectionId;
                $("#button_active").text("Connected");
                $("#button_active").addClass("btn-warning");
                $("#button_active").removeClass("btn-danger");
                $("#button_active").removeClass("disabled");
                $("#serial_message").addClass("alert alert-warning");
                $('#serial_message').html("No devices available <a href='#' class='close' data-dismiss='alert'>&times;</a>");
            } catch (e) {
                if(e) {
                    $("#button_active").text("Not Connected");
                    $("#button_active").addClass("btn-danger");
                    $("#button_active").addClass("disabled");
                    $("#button_active").removeClass("btn-warning");
                }
            }
            //$("#connected_status").text(connectionId);

            if(!listening) {
                proxy.listenSerialPort(function(info) {
                    text_dispatch(info.data);
                    //console.log("dispatch");
                    });
                listening = true;
                //console.log("setting listener");
                }
            })
        .then(function() {
            setTimeout(get_device_serial, 4000);
            });
    });


}

function text_dispatch(incoming_text) {
    // check this for actual working....
    var msg = hex2a(incoming_text);

    incoming_string += msg;
    // extract the first part of the string
    console.log(msg);
    var msg_line_re = /^(.*)[\r\n]+/; // matches everything until a new line.

    while (msg_line_re.test(incoming_string)) {
        var my_string = incoming_string.match(msg_line_re)[0];
        //console.log(my_string);
        var jar_id_re = /^Jar:(.+)/; // matches jar id string
        var weight_re = /^\s*(\d+\.\d+)\s+g\s.+/; // matches float g
        var hex_re = /Val:([0-9A-F]+)/; // this is a hex string
        var serial_re = /^Ser:(.+)/; // matches serial number

        if(jar_id_re.test(my_string)) {
            //console.log("jar");
            var jar_id = my_string.match(jar_id_re);
            if(!$("#id_jar").prop("readonly")) {
                $("#id_jar").val(jar_id[1]);
                $("#id_jar").prop("readonly", true);
            }

        }
        else if(weight_re.test(my_string)) {
            //console.log("wt");
            var wt = my_string.match(weight_re);
            $("#id_filling_weight").val(wt[1]);
        }
        else if(serial_re.test(my_string)) {
            //console.log("serial");
            var serial = my_string.match(serial_re);
            $("#filling_station_id").val(serial[1]);
            $("#reading_station_id").val(serial[1]);
            $("#serial_status").text(serial[1]);
            get_device_info();

        } else if(hex_re.test(my_string)) {
            //console.log("hex");
            var hex = my_string.match(hex_re);
            $("#raw_reading").val(hex[1]);
            $("#raw_reading").change();
        }
        incoming_string = incoming_string.slice(my_string.length);

        if (typeof(incoming_string) == "undefined") {
            incoming_string = "";
            }
    }
}

function get_device_serial() {
//     var msg = "SERNO\n";
//     var p = $("#ports_list");
//     var connId = parseInt($("#connected_status").text());
//     console.log("Sending message: " + msg + "at id: " + connId);
//     proxy.call('chrome.serial.send', connId, msg);
    send_device_message("SERNO\n");
}

function send_device_message(msg) {
    var p = $("#ports_list");

    //console.log("Sending message: " + msg + "at id: " + connId);
    proxy.call('chrome.serial.send', connection_id, msg);
}

function get_device_info() {
    console.log("trying device info");
    var url = window.location.pathname + "/info";
    $.get(  url,
            {'serial' : $("#serial_status").text()},
            function(data) {
                console.log(data);
                if (data.id) {
                    $("#button_active").text("Ready");
                    $("#device_name").text(data.name);
                    $("#device_type").text(data.hardware);
                    $("#button_active").addClass("btn-success");
                    $("#button_active").removeClass("btn-warning");
                    $('#serial_message').html("");
                    $("#serial_message").removeClass("alert alert-warning");
                    }
                else {
                    $("#button_active").text("Device not registered");
                    $("#button_active").removeClass("btn-warning");
                    $("#button_active").removeClass("btn-success");
                    $("#button_active").addClass("btn-danger");
                    $("#serial_message").addClass("alert alert-danger");
                    $('#serial_message').html("This device is not correct - try the other port! <a href='#' class='close' data-dismiss='alert'>&times;</a>");
                }
            }
        );
}

$(function() {
    $("#ports_list").change(connect_selected);
    $("#info_button").click(function() {
        //console.log("getting serial number");
        //send_device_message("SERNO\n");
        connect_selected();
    });
    //$("#button_active").click(get_serial_devices);
    //console.log("doc loaded");
    setTimeout(get_serial_devices, 1000);
});

function disconnect_current(connId) {
    //console.log ("Disconnecting :" + connId);
    proxy.call('chrome.serial.disconnect', connId)
        .then(function() {
            console.log('Disconnected ' + connId)});
}


$(window).on('unload', function() {
    proxy.call('chrome.serial.getConnections')
      .then(function(info) {
        //console.log(info);

        for(conn in info) {
            disconnect_current(conn.connectionId);
        }
    });
});
