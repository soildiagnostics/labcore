var current_read = "none";
var dark_pk = 0;
var bright_pk = 0;
var standards_pk = [0,0,0,0];

$(function() {
    $("#sensor_check").click(function(event) {
        fire_dark_read(event);
    });

    $("#dark_threshold").click(function(event) {
        if(confirm("Are you sure you want to overwrite factory settings for this sensor?")) {
            send_device_message("DARK_THRESHOLD\n");
        }
    });

    $("#bright_threshold").click(function(event) {
        if(confirm("Are you sure you want to overwrite factory settings for this sensor?")) {
            send_device_message("BRIGHT_THRESHOLD\n");
        }
    });

    $("#standard_check").click(function(event) {
        event.preventDefault();
        console.log('standard check');
        standard_check();
    });
    $("#reset_form").click(function(event) {
        event.preventDefault();
        reset_form();
    });

    // var data = [[0, 0], [0.25, 0.3], [0.5, 0.45], [1, 1]];
    // make_plot(data);

});

function make_plot(data) {
    var d2 = lineFit(data);

    var plot = $.plot("#placeholder",
        [
            {
                data: data,
                points: { show: true },
                lines: { show: true }
            },
            {
                data: d2,
                lines:{ show: true }
            }
        ],
        {
            grid: {
                hoverable: true,
                clickable: true
            },
            trendline: {
                show:true,
                lineWidth:2,
                fill:true,
                fillColor:false,
                steps:true
            },
            axisLabels: {
                show: true
            },
            xaxes: [{
                axisLabel: 'Organic N (ppm)',
            }],
            yaxes: [{
                position: 'left',
                axisLabel: 'Color index',
            }]
        }
    );
}

function reset_form() {
    current_read = "none";
    dark_pk = 0;
    bright_pk = 0;
    standards_pk = [0,0,0,0];
    $("#dark_read_raw").val("");
    $("#bright_read_raw").val("");
    $("#dark_read").val("");
    $("#bright_read").val("");
    $("#raw_reading").val("");
    $("#id_jar").val("");
    $("#id_jar").prop("readonly", false);
}

function fire_dark_read(event) {
    console.log('fired dark_read');
    $("#dark_read_raw").val("");
    $("#bright_read_raw").val("");
    event.preventDefault();
    send_device_message("DARKREAD\n");
}

function get_last_calibration() {
    var s = $("#serial_status").text();
    $.ajax({
        url : "/fertisavern/calibration/" + s + "/", // the endpoint
        type : "GET", // http method
        data : {}, // data sent with the post request
        // handle a successful response
        success : function(json) {
            make_plot(json);
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            show_hard_error(xhr, errmsg, err);
        }
    });
}

$("#button_active").change(function(event) {
    if($("#button_active").text() == "Ready") {
        get_last_calibration();
    }
});

$("#raw_reading").change(function(event) {
    // the first time this reading changes, it will be the dark read coming in
    console.log("current: " + current_read);
    if(current_read === "none") {
        // this is the first reading on this page. Ignore the reading that came in
        current_read = "dark";
        fire_dark_read(event);
    } else if(current_read === 'dark') {
        $("#dark_read_raw").val($("#raw_reading").val());
        sensor_check($("#dark_read_raw"), $("#dark_read"), function() {
            current_read = 'bright';
            send_device_message("READ\n");
        });

    } else if(current_read === 'bright') {
        $("#bright_read_raw").val($("#raw_reading").val());
        sensor_check($("#bright_read_raw"), $("#bright_read"), function() {
            $("#id_jar").prop("readonly", false);
            current_read = "standards";
        });
    } else if(current_read === 'standards') {
        sensor_check($("#raw_reading"), $("#sample_read"), function() {
            $("#id_jar").prop("readonly", false);
            current_read = "standards";
        });
    }
});

function show_invalid() {
    remove_results();
    $("#results")
        .addClass("alert alert-danger")
        .html("Form invalid! <a href='#' class='close'>&times;</a>");
}

function show_msg(msg) {
    remove_results();
    $("#results")
        .addClass("alert alert-info")
        .html(msg);
}

function show_success(msg) {
    remove_results();
    $("#results")
        .addClass("alert alert-success")
        .html(msg);
}

function show_hard_error(xhr, errmsg, err) {
    remove_results();
    $("#results")
        .addClass("alert alert-danger")
        .html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
            " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
    console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
}

function remove_results() {
    $('#results').html("").removeClass();
}
// AJAX for posting
function sensor_check(input_field, output_field, callback) {

    $.ajax({
        url : "/fertisavern/calibration/sensor", // the endpoint
        type : "POST", // http method

        data : { raw_reading: input_field.val(),
            jar: $('#id_jar').val(),
            reading_station_id: $('#reading_station_id').val(),
            reading_type: current_read
        }, // data sent with the post request
        // handle a successful response
        success : function(json) {
            if(json.scan) {
                if(current_read === 'dark') {
                    console.log("dark");
                    console.log(json);
                    show_success("Dark energy discovered!");
                    dark_pk = json.scan.id;
                } else if (current_read === 'bright') {
                    show_success("Bright point established!");
                    console.log("bright");
                    console.log(json);
                    bright_pk = json.scan.id
                } else if (current_read === 'standards') {
                    var s = json.sample[1] // extract the number
                    output_field = $("#standard_"+s);
                    show_success("Standard " + s + " detected");
                    console.log(json);
                    standards_pk[s] = json.scan.id;
                }
                output_field.val(json.scan.fsn_value);
            }
            else if(json.form_invalid) {
                show_invalid();
                reset_form();
            } else {
                show_msg(json.msg);
                reset_form();
            }
            callback();
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            show_hard_error(xhr, errmsg, err);
        }
    });
};

function check_calibration_data() {
    var calibration_good = true;
    var msg = ""
//     if(!(dark_pk && parseFloat($("#dark_read").val()) < 1)) {
//         msg += "Sensor seeing too much ambient light. Close the lid properly before triggering a sensor health check</br>";
//         calibration_good = false;
//    } 
    if(!bright_pk) {
        msg += "Bright reading not available - try running the sensor health check again</br>";
        calibration_good = false;
    }
    standards_pk.forEach(function(pk, index) {
        if(!pk) {
            msg += "Standard " + index + " did not register properly - try rescanning</br>";
            calibration_good = false;
        }
    });
    if(calibration_good) {
        $("#calibration_check").addClass("fa-check-circle").removeClass("fa-times-circle")
        $("#calibration_results").html("Everything checks out - sensor is calibrated! </br> You're set for the day!");
        show_msg("Check the approval box and submit");
        $(".submitWizard").removeClass('disabled').attr("data-toggle", "modal");
    } else {
        $("#calibration_check").removeClass("fa-check-circle").addClass("fa-times-circle");
        $("#calibration_results").html("We are not done yet - look carefully at the messages below");
        show_msg(msg);
        $(".submitWizard").addClass('disabled').removeAttr('data-toggle');
    }
}

$(function(){

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('a[data-toggle="tab"]').removeClass('btn-primary');
        $('a[data-toggle="tab"]').addClass('btn-default');
        $(this).removeClass('btn-default');
        $(this).addClass('btn-primary');
        check_calibration_data();
    })

    $('.next').click(function(){
        var nextId = $(this).parents('.tab-pane').next().attr("id");
        $('[href=#'+nextId+']').tab('show');
        if(nextId == "step3") {
            check_calibration_data();
        }
    })

    $('.prev').click(function(){
        var prevId = $(this).parents('.tab-pane').prev().attr("id");
        $('[href=#'+prevId+']').tab('show');
    })

    $('.submitWizard').click(function(){

        var approve = $(".approveCheck").is(':checked');
        if(approve) {
            console.log('assembling data');
            var data = {
                dark_pk : dark_pk,
                bright_pk : bright_pk,
                standards : standards_pk,
                reading_station_id: $('#reading_station_id').val(),
                approve : approve,
            }

            //Example code for post form
            $.ajax({
                type: "POST",
                url: "calibration/approve",
                data: data,
                success: function(json) {
                    show_success("Device calibrated\n" + json.model );
                    console.log(json);
                    get_last_calibration();
                }
            });
        } else {
            // Show notification
            show_msg("Please approve before submitting");
        }
    })
});

// calc slope and intercept
// then use resulting y = mx + b to create trendline
lineFit = function(points){
    sI = slopeAndIntercept(points);
    if (sI){
        // we have slope/intercept, get points on fit line
        var N = points.length;
        var rV = [];
        rV.push([points[0][0], sI.slope * points[0][0] + sI.intercept]);
        rV.push([points[N-1][0], sI.slope * points[N-1][0] + sI.intercept]);
        return rV;
    }
    return [];
}

// simple linear regression
slopeAndIntercept = function(points){
    var rV = {},
        N = points.length,
        sumX = 0,
        sumY = 0,
        sumXx = 0,
        sumYy = 0,
        sumXy = 0;

    // can't fit with 0 or 1 point
    if (N < 2){
        return rV;
    }

    for (var i = 0; i < N; i++){
        var x = points[i][0],
            y = points[i][1];
        sumX += x;
        sumY += y;
        sumXx += (x*x);
        sumYy += (y*y);
        sumXy += (x*y);
    }

    // calc slope and intercept
    rV['slope'] = ((N * sumXy) - (sumX * sumY)) / (N * sumXx - (sumX*sumX));
    rV['intercept'] = (sumY - rV['slope'] * sumX) / N;
    rV['rSquared'] = Math.abs((rV['slope'] * (sumXy - (sumX * sumY) / N)) / (sumYy - ((sumY * sumY) / N)));

    return rV;
}