$(document).ready(function() {
    //$.fn.dataTable.moment( 'MMMM D, YYYY, H:mm a');
    $('#datepicker').daterangepicker({
        opens: 'left'
    }, function(start, end, label) {
        $("#start").val(start);
        $("#end").val(end);
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        dtable.ajax.reload();
    });


    var dtable = $('#calibrated_readings').DataTable({
        'scrollX' : true,
        "processing": true,
        "serverSide": true,
        columnDefs: [
            {
                name: "sample_id",
                orderable: true,
                searchable: true,
                targets: [0]
            },
            {
                name: "fsn_value",
                orderable: false,
                searchable: false,
                targets: [1]
            },
            {
                name: "fill_date",
                orderable: true,
                searchable: false,
                targets: [2]
            },
            {
                name: "filled_by",
                orderable: true,
                searchable: true,
                targets: [3]
            },
            {
                name: "filling_weight",
                orderable: false,
                searchable: false,
                targets: [4]
            },
            {
                name: "read_date",
                orderable: true,
                searchable: false,
                targets: [5]
            },
            {
                name: "read_by",
                orderable: true,
                searchable: true,
                targets: [6]
            },
            {
                name: "calibration_date",
                orderable: false,
                searchable: false,
                targets: [7]
            },
        ],
        "ajax" : {
            url: "calibratedreads/",
            type: "GET",
            data : function ( d ) {
                if($('#start').val()) {
                    var st = new Date($('#start').val());
                    d.start_time = st.toISOString();
                } else {
                    d.start_time = ''
                }
                if($('#end').val()) {
                    var et = new Date($('#end').val());
                    d.end_time = et.toISOString();
                } else {
                    d.end_time = ''
                }
            }
        },
        // "columns": [
        //     { "data": "filled_sample.sample_id" },
        //     { "data": "fsn_value" },
        //     { "data": "filled_sample.fill_date" },
        //     { "data": "filled_sample.filled_by.user.username"},
        //     { "data": "filled_sample.filling_weight" },
        //     { "data": "sample_scan.read_date" },
        //     { "data": "sample_scan.read_by.user.username"},
        //     { "data": "calibration.date" },
        // ],
        'columnDefs': [
            {
                targets: [1],
                render: function ( data, type, full, meta ) {
                    var label_type = ["label-danger",
                        "label-warning",
                        "label-warning",
                        "label-success"][full.sample_scan.quality]
                    return data + '<span class="label ' + label_type + ' pull-right">' + full.sample_scan.quality + '</span>';
                }
            }
        ]
    });

});