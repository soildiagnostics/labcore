from fertisaverN.crypt import output_dictionary
from fertisaverN.models import *

scans = FertiSaverNScan.objects.all()


def rgb(scan):
    key = scan.reading_station.key
    iv = '0123456789012345'
    rgb = output_dictionary(key, iv, scan.raw_reading)
    hsv = scan.hsv()
    rgb.update(hsv)
    return rgb


rgb_dict = [rgb(s) for s in scans]

keys = ['Red', 'Green', 'Blue', 'Clear', 'Lux', 'Hue', 'Saturation', 'Value']

with open('scan_output.csv', 'w') as f:
    f.write(','.join(keys) + '\n')
    for d in rgb_dict:
        vals = [str(d[key]) for key in keys]
        f.write(','.join(vals) + '\n')
