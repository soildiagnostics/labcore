import json

from django.db.models import Q
from django.shortcuts import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import DeleteView
from django_datatables_view.base_datatable_view import BaseDatatableView
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response as APIResponse

from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin, FilteredObjectMixin
from .forms import FillingForm, ScanningForm
from .serializers import *


# Create your views here.


class FillingPageView(DealershipRequiredMixin, TemplateView):
    template_name = "fertisaverN/filling.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['device'] = {'type': 'FertiSaver-N Filling Station',
                             'name': 'Device name',
                             'instructions': 'Scan the jar, place on scale, \
                             zero the scale, add a scoop of soil, and press \
                             the print button'}
        context['form'] = FillingForm()
        context['formtype'] = 'Fill Samples'
        context['fillers'] = FertiSaverNFillingStation.objects.filter(template__owner=self.org)
        return context

class FillerDetail(DealershipRequiredMixin, generics.RetrieveAPIView):
    serializer_class = FertiSaverNFillingStationSerializer
    lookup_field = "serial_number"
    def get_queryset(self):
        return FertiSaverNFillingStation.objects.filter(template__owner=self.org)

class FilledSamplesListJSON(FilteredQuerySetMixin, DealershipRequiredMixin, BaseDatatableView):
    model = FilledSample
    columns = ['sample_id',
               'fill_date',
               'filled_by',
               'filling_weight']

    order_columns = ['fill_date', 'filled_by']
    max_display_length = 500
    instance_owner_organization = "filling_station__template__owner"

    def filter_queryset(self, qs):
        search = self.request.GET.get('search[value]', None)
        if search:
            q_params = Q(filled_by__user__first_name__icontains=search)| \
                       Q(filled_by__user__last_name__icontains=search) | \
                       Q(sample_id__iexact=search)
            qs = qs.filter(q_params)

        return qs


class CreateFilledSample(DealershipRequiredMixin, generics.CreateAPIView):

    def post(self, request, format=None):
        serializer = FilledSamplePostSerialzer(data=request.data)
        if serializer.is_valid():
            jar_key, created = Jar.objects.get_or_create(
                serial_number=serializer.validated_data['jar'], owner=self.org)
            filled_sample = FilledSample.objects.create(
                jar=jar_key,
                sample_id=serializer.validated_data['sample_id'],
                filling_weight=serializer.validated_data['filling_weight'],
                filled_at=self.org,
                filled_by=self.orguser,
                filling_station=FertiSaverNFillingStation.objects.
                    get(serial_number=serializer.validated_data['filling_station_id']))

            try:
                calibration_batch = CalibrationBatch.objects.get(
                    sample_name=filled_sample.sample_id)
                calibration = True
                calibration_sample = CalibrationSample.objects.create(
                    filled_sample=filled_sample,
                    calibration_batch=calibration_batch)
            except CalibrationBatch.DoesNotExist:
                calibration = False

            response_data = {
                "msg": "Done",
                "obj": serializer.data,
                "created": created,
                "calibration": calibration
            }

            return APIResponse(response_data, status=status.HTTP_201_CREATED)
        return APIResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReadingPageView(DealershipRequiredMixin, TemplateView):
    template_name = "fertisaverN/reading.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['device'] = {'type': 'FertiSaver-N Reading Station',
                             'name': 'Device name',
                             'instructions': 'Place the jar in the center, \
                             open the lid and close the scanner lid. \
                             Hold until reading is complete'}
        context['form'] = ScanningForm()
        context['formtype'] = 'Read Samples'
        context['scanners'] = FertiSaverNScanningStation.objects.filter(template__owner=self.org)
        return context

class ReadSamplesListJSON(FilteredQuerySetMixin, DealershipRequiredMixin, BaseDatatableView):
    model = FertiSaverNScan
    columns = ['sample_id',
               'read_date']

    order_columns = ['read_date']
    max_display_length = 500
    instance_owner_organization = "reading_station__template__owner"

    def filter_queryset(self, qs):
        search = self.request.GET.get('search[value]', None)
        if search:
            q_params = Q(read_by__user__first_name__icontains=search)| \
                       Q(read_by__user__last_name__icontains=search) | \
                       Q(calibrated_scan_value__filled_sample__sample_id__iexact=search)
            qs = qs.filter(q_params)

        return qs


class ReaderDetail(DealershipRequiredMixin, generics.RetrieveAPIView):
    serializer_class = FertiSaverNScanningStationSerializer
    lookup_field = "serial_number"
    def get_queryset(self):
        return FertiSaverNScanningStation.objects.filter(template__owner=self.org)


class CreateFSNScanView(DealershipRequiredMixin, generics.CreateAPIView):

    def post(self, request, format=None):
        serializer = FertiSaverNScanSerialzerPost(data=request.data)
        if serializer.is_valid():
            response_data = dict()
            try:
                jar_key = Jar.objects.get(serial_number=serializer.validated_data['jar'],
                                          template__owner=self.org)
            except Jar.DoesNotExist:
                response_data = {
                    'status': 'fail',
                    'message': 'Jar not found, scan rejected'}
                return APIResponse(response_data, status=status.HTTP_400_BAD_REQUEST)

            reading_station = FertiSaverNScanningStation.objects.get(
                serial_number=serializer.validated_data['reading_station_id'])
            scanned_sample = FertiSaverNScan.objects.create(
                jar=jar_key,
                reading_station=reading_station,
                read_at=self.org,
                read_by=self.orguser,
                raw_reading=serializer.validated_data['raw_reading']
            )

            scanned_sample.set_ci()
            scanned_sample.save()  # saving the scan.
            reading_station.last_used = timezone.now()
            reading_station.save()
            response_data['status'] = "success"
            response_data['message'] = "Scan created"
            response_data['scan'] = FertiSaverNScanSerializer(scanned_sample).data
            try:
                filled_sample = FilledSample.objects.filter(jar=jar_key,
                                                            fill_date__lte=timezone.now(),
                                                            fill_date__gt=timezone.now() - timezone
                                                            .timedelta(days=2)).latest('fill_date')
            except FilledSample.DoesNotExist:
                # Store the scan anyway...
                response_data['message'] += ". Filled sample not found"
                return APIResponse(response_data, status=status.HTTP_201_CREATED)

            if hasattr(filled_sample, 'calibration_filled_sample'):
                filled_sample.calibration_filled_sample.sample_scan.delete()
                response_data['message'] += ". Calibrated Read updated"
                filled_sample.calibration_filled_sample.sample_scan = scanned_sample
                filled_sample.calibration_filled_sample.save()
            else:
                CalibratedRead.objects.create(filled_sample=filled_sample,
                                              sample_scan=scanned_sample,
                                              calibration=reading_station.last_calibrated())
                response_data['message'] += ". Calibrated Read created"

            return APIResponse(response_data, status=status.HTTP_201_CREATED)
        else:
            response_data = {
                'status': 'fail',
                'message': 'invalid form'}
            return APIResponse(response_data, status=status.HTTP_400_BAD_REQUEST)


class FSNDataPage(DealershipRequiredMixin, TemplateView):
    template_name = "fertisaverN/data.html"


class FSNDataView(FilteredQuerySetMixin, DealershipRequiredMixin, BaseDatatableView):
    model = CalibratedRead
    instance_owner_organization = 'sample_scan__reading_station__template__owner'

    columns = [
        'filled_sample__sample_id',
        'fsn_value',
        'filled_sample__fill_date',
        'filled_sample__filled_by__user__username',
        'filled_sample__filling_weight',
        'sample_scan__read_date',
        'sample_scan__read_by__user__username',
        'sample_scan__calibrated_scan_value__calibration__date']

    order_columns = [
        'filled_sample__sample_id',
        'filled_sample__fill_date',
        'sample_scan__read_date'
    ]

    max_display_length = 500

    def filter_queryset(self, qs):
        start_time = self.request.GET.get('start_time')
        end_time = self.request.GET.get('end_time')
        if start_time:
            t = timezone.datetime.strptime(start_time, "%Y-%m-%dT%H:%M:%S.%fZ")
            t = timezone.make_aware(t)
            # print(t)
            qs = qs.filter(filled_sample__fill_date__gte=t)
        if end_time:
            t = timezone.datetime.strptime(end_time, "%Y-%m-%dT%H:%M:%S.%fZ")
            t = timezone.make_aware(t)
            # print(t)
            qs = qs.filter(sample_scan__read_date__lte=t)

        search = self.request.GET.get('search[value]', None)
        if search:
            q_params = Q(sample_scan__read_by__user__first_name__icontains=search)| \
                       Q(sample_scan__read_by__user__last_name__icontains=search) | \
                       Q(filled_sample__sample_id__iexact=search)
            qs = qs.filter(q_params)

        return qs


#
#
#
#
#
#
#
# def create_scanned_sample_helper(post, jar_key, ouser):
#     # form = ScanningForm(post)
#     jar_key = jar_key
#     reading_station = FertiSaverNScanningStation.objects.get(
#         serial=post['reading_station_id'])
#     scanned_sample = FertiSaverNScan(
#         jar=jar_key,
#         reading_station=reading_station,
#         read_at=ouser.organization,
#         read_by=ouser,
#         raw_reading=post['raw_reading']
#     )
#
#     scanned_sample.set_ci()
#     scanned_sample.save()  # saving the scan.
#     reading_station.last_used = timezone.now()
#     reading_station.save()
#     return scanned_sample


class ScannerCalibrationView(DealershipRequiredMixin, View):

    def get(self, request, serial_number, *args, **kwargs):
        try:
            obj = FertiSaverNScanningStation.objects.filter(template__owner=self.org).get(serial_number=serial_number)
            c = obj.get_last_calibration_data()
            return HttpResponse(
                json.dumps(c),
                content_type="application/json"
            )
        except Exception:
            return HttpResponse(
                json.dumps({'error': "No data"}),
                content_type="application/json"
            )

class CalibrationPageView(DealershipRequiredMixin, TemplateView):
    template_name = "fertisaverN/calibration.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['last_readings'] = ScannerCalibration.objects.filter(
            dark_read__read_at=self.org).order_by('-dark_read__read_date')[:10]
        context['devices'] = FertiSaverNScanningStation.objects.filter(
            template__owner=self.org)
        context['device'] = {'type': 'FertiSaver-N Scanning Station (Calibration mode)',
                             'name': 'Device name',
                             'instructions': 'Follow the instructions on the form below'}
        context['formtype'] = 'Set up Calibration'
        return context


class CalibrationProcessJSON(DealershipRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        response_data = {'msg': "",
                         "reading_type": request.POST.get('reading_type')}

        form = ScanningForm(request.POST)
        if form.is_valid():
            if request.POST.get('reading_type') in ('dark', 'bright'):
                # This is a bright/dark reading
                try:
                    jar_key = SensorCheckJar.objects.get(
                        serial_number=form.cleaned_data['jar'])
                    # Check that the jar is a sensor check jar.
                except SensorCheckJar.DoesNotExist:
                    response_data["msg"] = "Not a Sensor Check Jar"
                    return HttpResponse(
                        json.dumps(response_data),
                        content_type="application/json"
                    )
            else:  # reading_type == standards
                try:
                    jar_key = Jar.objects.get(serial_number=form.cleaned_data["jar"],
                                              template__owner=self.org)
                    calibration_sample = CalibrationSample.objects.filter(
                        filled_sample__jar=jar_key,
                        filled_sample__fill_date__lte=timezone.now(),
                        filled_sample__fill_date__gt=timezone.now() - timezone.timedelta(days=2)) \
                        .latest('filled_sample__fill_date')
                    response_data["msg"] += ", calibration sample found"
                    response_data["sample"] = calibration_sample.calibration_batch.sample_type
                except Jar.DoesNotExist:
                    response_data["msg"] = "Not a Calibration Sample Jar"
                    return HttpResponse(
                        json.dumps(response_data),
                        content_type="application/json"
                    )
                    # Create a scanned sample record
            reading_station = FertiSaverNScanningStation.objects.get(serial_number=request.POST.get('reading_station_id'))

            scanned_sample = FertiSaverNScan.objects.create(
                jar=jar_key,
                reading_station=reading_station,
                read_at=self.org,
                read_by=self.orguser,
                raw_reading=request.POST.get('raw_reading')
            )

            scanned_sample.set_ci()
            scanned_sample.save()  # saving the scan.
            reading_station.last_used = timezone.now()
            reading_station.save()
            response_data['scan'] = FertiSaverNScanSerializer(
                scanned_sample).data

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps({"form_invalid": "true"}),
                content_type="application/json"
            )


class DeleteCalibrationView(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    model = ScannerCalibration
    instance_owner_organization = "dark_read__reading_station__template__owner"

    def get_success_url(self):
        return reverse_lazy("calibrate")

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.active = False
        self.object.save()
        return HttpResponseRedirect(success_url)


class CalibrationDetail(FilteredObjectMixin, DealershipRequiredMixin, DetailView):
    model = ScannerCalibration
    instance_owner_organization = "dark_read__reading_station__template__owner"
    template_name = "fertisaverN/calibration_details.html"

#
# @permission_required('fertisaverN.calibrate_fsn_reader')
# def sensor_calibration(request):
#     response_data = {'msg': "",
#                      "reading_type": request.POST.get('reading_type')}
#     if request.method == 'POST':
#         form = ScanningForm(request.POST)
#         if form.is_valid():
#             ouser = OrganizationUser.objects.get(user=request.user)
#
#             if(request.POST.get('reading_type') in ('dark', 'bright')):
#                 # This is a bright/dark reading
#                 try:
#                     jar_key = SensorCheckJar.objects.get(
#                         uuid=form.cleaned_data['jar'])
#                     # Check that the jar is a sensor check jar.
#                 except SensorCheckJar.DoesNotExist:
#                     response_data["msg"] = "Not a Sensor Check Jar"
#                     return HttpResponse(
#                         json.dumps(response_data),
#                         content_type="application/json"
#                     )
#             else:  # reading_type == standards
#                 try:
#                     jar_key = Jar.objects.get(uuid=form.cleaned_data[
#                         "jar"], owner=ouser.organization)
#                     calibration_sample = CalibrationSample.objects.filter(
#                         filled_sample__jar=jar_key,
#                         filled_sample__fill_date__lte=timezone.now(),
#                         filled_sample__fill_date__gt=timezone.now() - timezone.timedelta(days=2)) \
#                         .latest('filled_sample__fill_date')
#                     response_data["msg"] += ", calibration sample found"
#                     response_data["sample"] = calibration_sample.calibration_batch.sample_type
#                 except Jar.DoesNotExist:
#                     response_data["msg"] = "Not a Calibration Sample Jar"
#                     return HttpResponse(
#                         json.dumps(response_data),
#                         content_type="application/json"
#                     )
#
#             # Create a scanned sample record
#
#             scanned_sample = create_scanned_sample_helper(
#                 request.POST, jar_key, ouser)
#             response_data['scan'] = FertiSaverNScanSerializer(
#                 scanned_sample).data
#
#             return HttpResponse(
#                 json.dumps(response_data),
#                 content_type="application/json"
#             )
#         else:
#             return HttpResponse(
#                 json.dumps({"form_invalid": "true"}),
#                 content_type="application/json"
#             )
#     else:
#         return HttpResponse(
#             json.dumps({"error": "Not a correctly formatted request"}),
#             content_type="application/json"
#         )


class SetCalibrationView(DealershipRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        response_data = {}
        standard0_jar = FertiSaverNScan.objects.get(
            id=request.POST.getlist('standards[]')[0]).jar
        standard1_jar = FertiSaverNScan.objects.get(
            id=request.POST.getlist('standards[]')[1]).jar
        standard2_jar = FertiSaverNScan.objects.get(
            id=request.POST.getlist('standards[]')[2]).jar
        standard3_jar = FertiSaverNScan.objects.get(
            id=request.POST.getlist('standards[]')[3]).jar

        calibration = ScannerCalibration(
            dark_read=FertiSaverNScan.objects.get(id=request.POST['dark_pk']),
            bright_read=FertiSaverNScan.objects.get(
                id=request.POST['bright_pk']),
            standard0=CalibrationSample.objects.filter(
                filled_sample__jar=standard0_jar)
                .latest('filled_sample__fill_date'),
            standard1=CalibrationSample.objects.filter(
                filled_sample__jar=standard1_jar)
                .latest('filled_sample__fill_date'),
            standard2=CalibrationSample.objects.filter(
                filled_sample__jar=standard2_jar)
                .latest('filled_sample__fill_date'),
            standard3=CalibrationSample.objects.filter(
                filled_sample__jar=standard3_jar)
                .latest('filled_sample__fill_date')
        )
        calibration.save()
        calibration.renormalize() ## check this!!
        calibration.create_model()

        response_data['calibration'] = ScannerCalibrationSerializer(
            calibration).data
        FertiSaverNScan.objects.get(
            id=request.POST['dark_pk']) \
            .reading_station.last_calibration = timezone.now()
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )


#
# @permission_required('fertisaverN.view_fsn_data')
# def filled_samples_list(request):
#
#     ouser = OrganizationUser.objects.get(user=request.user)
#     column_list = ['sample_id',
#                    'fill_date',
#                    'filled_by__user__username',
#                    'filling_weight']
#
#     order_by = column_list[int(request.GET.get('order[0][column]'))]
#     order_dir = request.GET.get('order[0][dir]')
#     if order_dir == 'desc':
#         order_by = '-' + order_by
#
#     start = int(request.GET.get('start'))
#     length = int(request.GET.get('length'))
#     # search = request.GET.get('search[value]')
#     draw = int(request.GET.get('draw'))
#
#     # print(order_by, order_dir, start, length, search)
#
#     dat = FilledSample.objects.filter(filled_at=ouser.organization)
#     recordsTotal = len(dat)
#
#     for i in range(0, len(column_list)):
#         # filter by search terms
#         key = "columns[%d][search][value]" % i
#         val = request.GET.get(key)
#         if val:
#             # search requested
#             kwargs = {
#                 '{0}__{1}'.format(column_list[i], 'icontains'): val
#             }
#             dat = dat.filter(**kwargs)
#
#     dat_slice = dat.order_by(order_by)[start:start + length]
#     data = FilledSampleSerializer(dat_slice, many=True).data
#
#     recordsFiltered = len(dat)
#
#     return HttpResponse(
#         json.dumps({'draw': draw,
#                     'data': data,
#                     'recordsTotal': recordsTotal,
#                     'recordsFiltered': recordsFiltered}),
#         content_type="application/json"
#     )
#
#
# @permission_required('fertisaverN.view_fsn_data')
# def scanned_samples_list(request):
#
#     ouser = OrganizationUser.objects.get(user=request.user)
#     column_list = ['jar__uuid',
#                    'read_date',
#                    'read_by__user__username',
#                    'chromic_index']
#
#     order_by = column_list[int(request.GET.get('order[0][column]'))]
#     order_dir = request.GET.get('order[0][dir]')
#     if order_dir == 'desc':
#         order_by = '-' + order_by
#
#     start = int(request.GET.get('start'))
#     length = int(request.GET.get('length'))
#     search = request.GET.get('search[value]')
#     draw = int(request.GET.get('draw'))
#
#     ##print(order_by, order_dir, start, length, search)
#
#     dat = FertiSaverNScan.objects.filter(
#         read_at=ouser.organization, calibrated_scan_value=None)
#     recordsTotal = len(dat)
#
#     for i in range(0, len(column_list)):
#         # filter by search terms
#         key = "columns[%d][search][value]" % i
#         val = request.GET.get(key)
#         if val:
#             # search requested
#             kwargs = {
#                 '{0}__{1}'.format(column_list[i], 'icontains'): val
#             }
#             dat = dat.filter(**kwargs)
#
#     dat_slice = dat.order_by(order_by)[start:start + length]
#     data = FertiSaverNScanSerializer(dat_slice, many=True).data
#
#     recordsFiltered = len(dat)
#
#     return HttpResponse(
#         json.dumps({'draw': draw,
#                     'data': data,
#                     'recordsTotal': recordsTotal,
#                     'recordsFiltered': recordsFiltered}),
#         content_type="application/json"
#     )
#
#
#











