import colorsys
# from Crypto import Random
import re

from Crypto.Cipher import AES

# Hard coded initialization vector
iv_default = '0123456789012345'


def _decrypt(key, iv, incoming):
    """
    This is a hex string with or without spaces
    """
    cipher = AES.new(key, AES.MODE_CBC, iv)
    hb = bytes.fromhex(incoming)
    # print(hb)
    output = cipher.decrypt(hb)
    return output


def _byte_to_utf8(byte):
    """
    Convert a byte string to utf-8 for those that are possible
    """
    out = ""
    out = chr(byte)
    return out


def _str_to_dict(utf8output):
    # print(utf8output)
    m = re.search("R:(\d+),G:(\d+),B:(\d+),C:(\d+),L:(\d+)END", utf8output)
    out = {'Red': float(m.group(1)),
           'Green': float(m.group(2)),
           'Blue': float(m.group(3)),
           'Clear': float(m.group(4)),
           'Lux': float(m.group(5))}

    return out


def output_dictionary(key, iv, incoming):
    clear = _decrypt(key, iv_default, incoming)
    # print(clear)
    clear = ''.join([_byte_to_utf8(c) for c in clear])
    rgb = _str_to_dict(clear)
    return rgb


def raw_to_dict(key, iv, encrypted_string):
    clear = _decrypt(key, iv_default, encrypted_string)
    clear = ''.join([_byte_to_utf8(c) for c in clear])
    rgb = _str_to_dict(clear)
    return rgb

def clip(value, min=0, max=1):
    ret = value;
    if value < 0:
        ret = 0
    if value > 1:
        ret = 1
    return ret 

def cnormalize(rgb, rgbdark, rgbbright):
    rgbout={}
    for c in ['Red', 'Green', 'Blue']:
        rgbout[c] = clip((rgb[c] - rgbdark[c]) / (rgbbright[c] - rgbdark[c]))
    return rgbout

def get_hsv(key, iv_default, incoming, calibration):
    """
    This converts the encrypted incoming string to HSV values
    This should be the only function you need.
    """

    rgb = raw_to_dict(key, iv_default, incoming)

    try:
        rgbdark = raw_to_dict(key, iv_default, calibration.dark_read.raw_reading)
        rgbbright = raw_to_dict(key, iv_default, calibration.bright_read.raw_reading)

        rgb = cnormalize(rgb, rgbdark, rgbbright)
    except Exception: 
        print("Failed")

    h, s, v = colorsys.rgb_to_hsv(rgb['Red'], rgb['Green'], rgb['Blue'])

    out = {'Hue': h,
           'Saturation': s,
           'Value': v}

    return out


if __name__ == '__main__':

    key = '0123456789ABCDEF'
    incoming = "3031323334353637383930313233343526C7C11B79CA1A59E8B8820841A07BE8621CC906B491C54F3EAD80FD2B6C79D270A1C4819CB8465595C5330E19AFA558"

    print(raw_to_dict(key, iv_default, incoming))
