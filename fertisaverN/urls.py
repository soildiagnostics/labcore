from django.urls import path

from fertisaverN import views

urlpatterns = [
    path('filling/', views.FillingPageView.as_view(), name="filling"),
    path('filling/info/<serial_number>/', views.FillerDetail.as_view(), name="filler_info"),
    path('filling/sampledata/', views.FilledSamplesListJSON.as_view()),
    path('fill/', views.CreateFilledSample.as_view(), name="fill_sample"),
    path('reading/', views.ReadingPageView.as_view(), name="reading"),
    path('reading/sampledata/', views.ReadSamplesListJSON.as_view()),
    path('reading/info/<serial_number>/', views.ReaderDetail.as_view(), name="reader_info"),
    path('scan/', views.CreateFSNScanView.as_view(), name="scan_sample"),
    path('data/', views.FSNDataPage.as_view(), name="data"),
    path('data/calibratedreads/', views.FSNDataView.as_view(), name="calibrated_reads_list"),
    # url(r'data/filledsamples', fertisaverN.views.filled_samples_list),
    # url(r'data/scannedsamples', fertisaverN.views.scanned_samples_list,
    #     name="scanned_samples_list"),
    path('calibration/', views.CalibrationPageView.as_view(), name="calibrate"),
    # path('calibration/info/<serial_number>/', views.ScannerCalibrationView.as_view(),
    # name="calibration_info"),
    path('calibration/sensor/', views.CalibrationProcessJSON.as_view(),
         name="sensor_calibration"),
    path('calibration/approve/', views.SetCalibrationView.as_view(),
         name="set_calibration"),
    path('calibration/<pk>/delete/', views.DeleteCalibrationView.as_view(), name="delete_calibration"),
    path('calibration/<pk>/details/', views.CalibrationDetail.as_view(),
         name="details_calibration"),
    path('calibration/<serial_number>/',
         views.ScannerCalibrationView.as_view(), name="last_calibration"),
]
