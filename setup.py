import os

from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='soildx-labcore',
    version='1.0.4a',
    packages=find_packages(),
    include_package_data=True,
    license='Proprietary',  # for now
    description='A Soil Diagnostics, Inc. app for core soil management technologies.',
    long_description=README,
    url='https://www.soildiagnostics.com/',
    author='Kaustubh D. Bhalerao',
    author_email='bhalerao@soildiagnostics.com',
    install_requires=[
        'django-address', 
        'djangorestframework',
        'django-tagging',
        'django-money',
        'pandas',
        'scipy',
        'numpy',
        'pycrypto',
        'plotly',
        'PIL'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 3.1.1',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Proprietary',  # for now
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)