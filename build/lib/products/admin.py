from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from products.models import Category, Price, Product, Package, Discount, Unit


# Register your models here.


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at',)
    list_filter = ('name',)
    search_fields = ('name',)
    #autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "name": ['name'],
    }
    prepopulated_fields = {'slug': ('name',), }


class UnitInLine(admin.TabularInline):
    model = Unit 

admin.site.register(Unit)

class PriceInline(admin.TabularInline):
    model = Price
    inlines = [
        UnitInLine,
    ]


class DiscountInline(GenericTabularInline):
    model = Discount


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):

    list_display = ('name', 'code', 'created_at', 'updated_at',)
    list_filter = ('name',)
    search_fields = ('name',)
    inlines = [
        PriceInline,
        DiscountInline,
    ]
    #autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "name": ['name'],
    }
    prepopulated_fields = {'slug': ('name',), }


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):

    list_display = ('name', 'code', 'created_at', 'updated_at',)
    list_filter = ('name',)
    search_fields = ('name', 'products__name')
    inlines = [
        PriceInline,
        DiscountInline,
    ]
    #autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "name": ['name'],
    }
    prepopulated_fields = {'slug': ('name',), }

#admin.site.register(Discount)
