from contact.tests.tests import ContactTestCase
from django.db import IntegrityError
from django.test import TestCase
from djmoney.money import Money

from associates.models import AssociatedOrganization
from contact.models import Company
from products.models import Category, Price, Product, Package, Discount


# Create your tests here.


class CategoryProductTest(TestCase):
    def setUp(self):
        Category.objects.create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N")
        Price.objects.create(amount=Money(49.99, 'USD'))

    def test_slugify(self):
        c = Category.objects.get(name="Soil Test")
        self.assertEqual(str(c), "Soil Test")
        self.assertEqual(c.slug, "soil-test")

        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(str(p), "FertiSaver-N")
        self.assertEqual(p.slug, "fertisaver-n")

    def test_set_price(self):

        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(len(p.price.all()), 0)
        self.assertEqual(p.get_price_for_currency('INR'), None)
        p.set_price(1, 'USD')

        self.assertEqual(len(p.price.all()), 1)
        price = p.price.all()[0]
        self.assertEqual(str(price), "USD 1.0000 per unknown")
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(1, 'USD'))

    def test_update_price(self):

        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(1, 'USD')
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(1, 'USD'))

        p.set_price(2, 'USD')
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(2, 'USD'))

    def test_product_tags(self):
        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(p.tags.count(), 0)
        p.tags = ["nitrogen"]
        self.assertEqual(p.tags.count(), 1)

    def test_package(self):

        pack = Package.objects.create(name="FSN")
        p = Product.objects.get(name="FertiSaver-N")
        p2 = Product.objects.create(name="LOI")
        p3 = Product.objects.create(name="pH")

        pack.products.add(p)
        pack.products.add(p2)
        pack.products.add(p3)
        pack.save()

        self.assertEqual(pack.name, "FSN")
        self.assertEqual(len(pack.products.all()), 3)

    def test_price_uniqueness(self):

        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(1, 'USD')

        with self.assertRaises(IntegrityError):
            Price.objects.create(amount=Money(2, 'USD'), product=p)


class DiscountTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        Category.objects.create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N")

    def test_create_discount(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        d = Discount.objects.create(dealer=a,
                                    discount=11.2,
                                    content_object=p)

        self.assertNotEqual(d.id, None)

    def test_create_unique_discount(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        Discount.objects.create(dealer=a,
                                discount=11.2,
                                content_object=p)
        with self.assertRaises(IntegrityError):
            Discount.objects.create(dealer=a,
                                    discount=12.2,
                                    content_object=p)

    def test_discounted_price(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)
        v = d.get_discounted_price_for_currency('INR')
        self.assertEqual(v, None)

        v = d.get_discounted_price_for_currency('USD')
        self.assertEqual(v, Money(90, 'USD'))

