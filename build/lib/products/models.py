from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext as _
from djmoney.models.fields import MoneyField
from djmoney.money import Money
from tagging.registry import register

from associates.models import AssociatedOrganization


# uses django-tagging, which was installed with Zinnia any way.
# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=150)
    slug = models.SlugField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name', )
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.slug == "":
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)  # Call the "real" save() method.


class Unit(models.Model):
    symbol = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    class Meta:
        ordering = ('symbol', )
        verbose_name = _('Price Unit')
        verbose_name_plural = _('Price Units')

    def __str__(self):
        return self.symbol


class Price(models.Model):
    amount = MoneyField(max_digits=19, decimal_places=4,
                        default_currency="USD")
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True,
                             help_text=_("Price per unit of sale"))
    product = models.ForeignKey(
        'Product', related_name="price", null=True,
        on_delete=models.CASCADE)

    class Meta:
        unique_together = ('amount_currency', 'product', 'unit')

    def __str__(self):
        unit = "Unknown unit"
        if self.unit:
            unit = str(self.unit)
        return "{} {} {}".format(self.amount.currency, self.amount.amount, unit)

    def save(self, *args, **kwargs):
        if self.unit == None:
            self.unit, created = Unit.objects.get_or_create(symbol="per unknown")
        super().save(*args, **kwargs)  # Call the "real" save() method.


class Product(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(
        max_length=20, blank=True,
        help_text=_("Billing / accounting codes"))
    slug = models.SlugField(blank=True,
                            help_text="Will be prepopulated in the admin page")
    category = models.ForeignKey(Category, null=True, on_delete=models.PROTECT)
    description = models.TextField(blank=True, help_text=_(
        "Short description of the product. Please create a product page when possible"))
    product_page = models.URLField(
        blank=True, help_text=_("Product page on the blog"))
    active = models.BooleanField(default=True, help_text=_(
        "Do not delete products - just deactivate them when necessary"))
    stock = models.PositiveIntegerField(default=0,
                                        help_text=_("Inventory - NA for Service category items"))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    protocol = models.URLField(
        blank=True, help_text=_("Protocol description on the blog"))
    report_template = models.FileField(
        upload_to="product_templates/", blank=True)
    image = models.ImageField(upload_to="product_images/", blank=True)

    class Meta:
        ordering = ('name', )
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name

    def get_price_for_currency(self, currency):
        try:
            return self.price.get(amount_currency=currency)
        except ObjectDoesNotExist:
            return None

    def set_price(self, amount, currency):
        try:
            p = self.price.get(amount_currency=currency)
            p.amount = amount
            p.save()
            self.save()
        except ObjectDoesNotExist:
            p = Price(amount=Money(amount, currency))
            p.save()
            self.price.add(p)
            self.save()

    def save(self, *args, **kwargs):
        if self.slug == "":
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)  # Call the "real" save() method.


register(Product)


class Package(Product):

    products = models.ManyToManyField(Product, related_name="products")

    class Meta:
        ordering = ('name', )
        verbose_name = _('Package')
        verbose_name_plural = _('Packages')


class Discount(models.Model):
    code = models.CharField(max_length=20, default="DISCOUNT")
    dealer = models.ForeignKey(
        AssociatedOrganization, on_delete=models.CASCADE)
    discount = models.DecimalField(
        max_digits=5, decimal_places=2, help_text=_("Percent discount"))
    # generic item to apply the discount to
    limit = models.Q(app_label='products', model='Product') | models.Q(
        app_label='products', model='Package')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = ('dealer', 'content_type', 'object_id')

    def get_discounted_price_for_currency(self, currency):
        p = self.content_object.get_price_for_currency(currency)
        if p:
            return p.amount * (100 - self.discount) / 100
        else:
            return None
