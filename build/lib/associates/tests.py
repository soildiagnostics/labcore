from contact.tests.tests import ContactTestCase
from django.contrib.auth.models import User
from django.test import TestCase

from associates.models import Role, AssociatedOrganization
from contact.models import Company, Person, EmailModel


# Create your tests here.


class RoleTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        pass

    def test_role_dealer(self):

        r = Role.objects.get(role="Dealer")
        self.assertEqual(r.pk, 2)
        self.assertEqual(r.__str__, "Dealer (Dealer organization, that has access to all clients of this organization)")


class AssociatedOrganizationTestCase(TestCase):

    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)

    def test_get_or_create_associated_organization(self):

        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.get_or_create(company=c)
        self.assertEqual(a.__str__, "SkyData Technologies LLC")

    def test_create_dealership(self):

        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.create_dealership(company=c)
        self.assertEqual(created, True)

    def test_existing_company(self):

        c = Company.objects.get(name__startswith="Sky")
        a1, created1 = AssociatedOrganization.objects.get_or_create(company=c)
        a2, created2 = AssociatedOrganization.objects.get_or_create(company=c)
        self.assertEqual(a1, a2)

    def test_add_members_to_organization(self):

        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.get_or_create(company=c)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c 
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)

        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        e = EmailModel.objects.create(email="test@example.com")
        e.contact = m
        e.save()
        m.company=c
        m.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 2)

        m = Person.objects.create(name="Jessie",
                                  last_name="Bhalerao")
        m.company=c
        m.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 3)

    def test_set_organization_owner(self):

        c = Company.objects.get(name__startswith="Sky")
        a, created = AssociatedOrganization.objects.get_or_create(company=c)
        u = User.objects.get(username="testinguser")
        # p = Person.objects.get_or_create_person_from_user(u)
        # p.save()
        p = Person.objects.get(user=u)
        owner = a.set_organization_owner(u)

        self.assertEqual(owner.organization_user.user.username, "testinguser")





