from django.core.exceptions import *
from django.db import models
from django.utils.translation import gettext as _
from organizations.models import Organization, OrganizationUser, OrganizationOwner

from contact.models import Company, Person


# Create your models here.
class Role(models.Model):
    role = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    def __str__(self):
        return "{} ({})".format(self.role, self.description)


class AssociatedOrganizationManager(models.Manager):

    def get_or_create(self, *args, **kwargs):
        c = kwargs['company']
        try:
            o = AssociatedOrganization.objects.get(company=c)
            return (o, False)
        except ObjectDoesNotExist:
            o = AssociatedOrganization(company=c)
            o.save()
            return (o, True)

    def create_dealership(self, company):
        ''' Creates a dealership organization from a company record '''
        a, created = self.get_or_create(company=company)
        r = Role.objects.get(role="Dealer")
        a.roles.add(r)
        a.save()
        return (a, created)


class AssociatedOrganization(Organization):
    """ Subclass of :model: `Organization` that links to :model: `Company` """

    company = models.ForeignKey(Company, on_delete=models.PROTECT)
    roles = models.ManyToManyField(Role)
    objects = AssociatedOrganizationManager()

    def __str__(self):
        return self.company.name

    def save(self, *args, **kwargs):
        if not self.pk:
            # if this object is not yet createdß
            self.name = self.company.name
        super().save(*args, **kwargs)


    def add_company_members_to_organization(self):
        ''' looks for :model: `contactfield.Person`s 
            and adds them as :model: `organization.OrganizationUser`s
            to the :model: `people.AssociatedOrganization`. 
            If the person is listed as the company contact, they are 
            set to be the organization admin.

            This method assumes that if a person is not an existing 
            :model: `django.contrib.auth.models.User`
            it will create a User from for that person. 
        '''
        persons = Person.objects.filter(company=self.company)
        added_list = []
        for p in persons:
            if not p.user:
                username = Person.get_unique_user_name(None, p.name, p.last_name, p.company)
                p.get_or_create_user_from_person(username=username, is_active=True)

            res = OrganizationUser.objects.get_or_create(
                user=p.user, organization=self)
            added_list.append(res)
            ou, created = res
            if p.is_company_contact:
                ou.is_admin = True
                ou.save()
        return added_list

    def set_organization_owner(self, person):
        ''' a :model: `contactfields.Person` may be set to be an owner of the organization
            Will check to ensure that the person is registered as a user of that organization
        '''
        try:
            ouser = OrganizationUser.objects.get(user=person.user)
        except ObjectDoesNotExist:
            ''' create the org user'''
            ouser, created = OrganizationUser.objects.get_or_create(user=person.user,
                                                                    organization=self)
        orgowner = OrganizationOwner(
            organization_user=ouser, organization=self)
        orgowner.save()
        return orgowner

    class Meta:
        verbose_name = _("Associated Organization")
        verbose_name_plural = _("Associated Organizations")


