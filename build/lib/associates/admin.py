from django.contrib import admin

# Register your models here.
from .models import Role, AssociatedOrganization

admin.site.register(Role)


@admin.register(AssociatedOrganization)
class AssociatedOrganizationAdmin(admin.ModelAdmin):

    list_display = ('name','get_roles')
    autocomplete_search_fields = {
        "name": ['name'],
    }
    prepopulated_fields = {'slug': ('name',), }

    def get_roles(self, obj):
        return ', '.join([o.role for o in obj.roles.all()])
    get_roles.short_description = 'Organization Roles'

