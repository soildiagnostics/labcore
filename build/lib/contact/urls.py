from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.ContactList.as_view(), name="contact_list"),
    path('persons/', views.PersonList.as_view(), name="persons_list"),
    path('person/<int:pk>/',
        views.PersonDetail.as_view(), name="person_detail"),
    path('companies/', views.CompanyList.as_view(), name="companies_list"),
    path('company/<int:pk/',
        views.CompanyDetail.as_view(), name="company_detail"),

]

urlpatterns = format_suffix_patterns(urlpatterns)
