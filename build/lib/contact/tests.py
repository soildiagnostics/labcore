# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from contact.models import PhoneModel, Contact, Person, Company, AddressModel, EmailModel


# Create your tests here.
class PhoneTestCase(TestCase):
    def setUp(self):
        PhoneModel.objects.create(phone_number="+12177216032")

    def test_phone(self):
        phone = PhoneModel.objects.get(phone_number="+12177216032")
        self.assertEqual(phone.phone_type, "M")
        self.assertEqual(phone.__str__, "+12177216032 (M)")


class EmailTestCase(TestCase):
    def setUp(self):
        EmailModel.objects.create(email="test@example.com")

    def test_email(self):
        email = EmailModel.objects.get(email='test@example.com')
        self.assertEqual(email.email_type, "O")
        self.assertEqual(email.__str__, "test@example.com (O)")


class ContactTestCase(TestCase):
    def setUp(self):
        PhoneModel.objects.create(phone_number="+12177216032")
        c = Company.objects.create(name="SkyData Technologies LLC")
        address1 = AddressModel(
            address="60 Hazelwood Drive, Champaign, IL 61820", contact=c)
        u = User.objects.create(username="testinguser",
                                first_name="Testing",
                                last_name="User",
                                email="user@test.com",
                                password="nothingsecret",
                                is_active=True)

    def test_create_company(self):

        c = Contact.objects.get(name__startswith="Sky")
        self.assertTrue(c.is_company)
        self.assertEqual(c.__str__, "SkyData Technologies LLC")

    def test_create_person(self):
        p = PhoneModel.objects.get(phone_number="+12177216032")
        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        address2 = AddressModel(
            address="14 Oak Ct, Champaign, IL 61822, USA", contact=m)
        address2.save()

        p.contact = m
        p.save()
        self.assertEqual(p.contact, m)
        self.assertEqual(len(m.phone.all()), 1)

        a2 = m.address.all()[0]
        self.assertEqual(a2.__str__, "14 Oak Ct, Champaign, IL 61822, USA")

        c = Company.objects.get(name__startswith="Sky")
        m.company = c
        m.save()
        ml = Person.objects.filter(company=c)
        self.assertEqual(len(ml), 1)

    def test_create_person_from_user(self):

        u = User.objects.get(username="testinguser")
        # p = Person.objects.get_or_create_person_from_user(u)
        p = Person.objects.get(user=u)
        e = EmailModel.objects.get(email=u.email)

        self.assertEqual(p.email.all()[0], e)
        self.assertEqual(p.__str__, "Testing User")

    def test_create_user_from_person(self):

        p = Person.objects.create(
            name='Sachin',
            last_name = 'Bhalerao'
        )
        e = EmailModel.objects.create(
            contact = p,
            email = 'bhalersa2026@u4sd.org'
        )

        self.assertEqual(p.user, None)
        u = p.get_or_create_user_from_person('sachinb', False)

        self.assertEqual(u.username, 'sachinb')
        self.assertEqual(u.last_name, 'Bhalerao')
        self.assertEqual(u.email, 'bhalersa2026@u4sd.org')
        self.assertEqual(u.is_active, False)
        self.assertNotEqual(u.password, '')
        p.refresh_from_db()
        self.assertEqual(p.user, u)

        p = Person.objects.create(name = 'Sam Bhalerao')
        u = p.get_or_create_user_from_person('samb')

        self.assertEqual(u.last_name, '')
        self.assertEqual(u.username, 'samb')
        self.assertEqual(u.is_active, True)


    def test_contactfields_views(self):
        view_list = ['contact_list',
                     'persons_list',
                     'companies_list']
        c = Client()

        for v in view_list:
            response = c.get(reverse(v))
            self.assertEqual(response.status_code, 200)

    def test_create_unique_usernames_function(self):

        un = Person.get_unique_user_name(None, 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao')

        u = User.objects.create(username = 'jessiebhalerao', first_name='Jessie', last_name = 'Bhalerao')
        un = Person.get_unique_user_name('jessiebhalerao', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao001')

        u.username = 'jessiebhalerao001'
        u.save()
        un = Person.get_unique_user_name('jessiebhalerao001', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao002')

        u.username = 'jessiebhalerao1'
        u.save()
        un = Person.get_unique_user_name('jessiebhalerao1', 'Jessie', 'Bhalerao')
        self.assertEqual(un, 'jessiebhalerao2')


# test for when a new user is created and the person has the same name already