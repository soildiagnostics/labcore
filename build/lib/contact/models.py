# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from address.models import AddressField
from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext as _

PHONE_TYPES = (
    ('M', 'Mobile'),
    ('O', 'Office'),
    ('H', 'Home'),
    ('X', 'Other'),
)

ADDRESS_TYPES = (
    ('O', 'Office'),
    ('H', 'Home'),
    ('X', 'Other'),
)


class PhoneModel(models.Model):
    """
    Abstraction on the Phone number. 
    Related to a :model: `contactfields.Contact`
    """
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message=_("Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))
    phone_number = models.CharField(
        validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    phone_type = models.CharField(
        max_length=1, choices=PHONE_TYPES, default='M',
        help_text="Phone may be mobile, office, home or other.")
    extension = models.CharField(max_length=17, blank=True,
                                 help_text=_('Extension'))
    primary = models.BooleanField(default=False, help_text=_("Primary phone"))
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="phone")

    def __str__(self):
        return "{} ({})".format(self.phone_number, self.phone_type)

    class Meta:
        verbose_name = _("Phone")
        verbose_name_plural = _("Phones")


class EmailModel(models.Model):
    """
    Wraps Emailfield with additional information
    """
    email = models.EmailField()
    email_type = models.CharField(
        max_length=1, choices=ADDRESS_TYPES, default='O')
    primary = models.BooleanField(default=False)
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="email")

    def __str__(self):
        return "{} ({})".format(self.email, self.email_type)

    class Meta:
        verbose_name = _("Email")
        verbose_name_plural = _("Emails")


class AddressModel(models.Model):
    """
    Wraps :model: `address.Address` with useful information
    """
    address = AddressField(on_delete=models.CASCADE)
    address_type = models.CharField(
        max_length=1, choices=ADDRESS_TYPES, default='O')
    primary = models.BooleanField(default=False)
    contact = models.ForeignKey(
        "Contact", null=True, on_delete=models.CASCADE, related_name="address")

    def __str__(self):
        return self.address.formatted

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")

    def save(self, *args, **kwargs):
        if self.address.formatted == "":
            self.address.formatted = self.address.raw
        self.address.save()
        super(AddressModel, self).save(*args, **kwargs)


class Contact(models.Model):
    """ 
    Generic Contact model. Can be marked as Person or Company
    """
    name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, default='')
    last_name = models.CharField(max_length=100, blank=True)
    company = models.ForeignKey(
        'Company', null=True, blank=True, on_delete=models.CASCADE)
    is_company = models.BooleanField(default=False)
    is_company_contact = models.BooleanField(default=False,
                                             help_text=_("This person is the primary contact for the company"))
    user = models.OneToOneField(
        User, on_delete=models.PROTECT, null=True, blank=True, related_name='person')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    notes = models.TextField(null=True, blank=True, verbose_name=(u'Notes'))
    image = models.ImageField(upload_to='contactimages/', blank=True)
    url = models.URLField(blank=True)

    class Meta:
        verbose_name = _("Contact")
        verbose_name_plural = _("Contacts")

    def __str__(self):
        if self.is_company:
            return "{}".format(self.name)
        else:
            return " ".join(filter(None, [self.name, self.middle_name, self.last_name]))

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains")


class PersonManager(models.Manager):

    def get_queryset(self):
        return super(PersonManager, self).get_queryset().filter(
            is_company=False)

    # def get_or_create_person_from_user(self, user):
    #     """
    #     Helper function to create a :model: `contactfields.Person`
    #     from a :model: `auth.User` object
    #     """
    #     p, created = Person.objects.get_or_create(
    #         name=user.first_name,
    #         #middle_name='',
    #         last_name=user.last_name,
    #         user=user)
    #     e = EmailModel.objects.get_or_create(
    #         email=user.email,
    #         contact=p)
    #     return (p)


class Person(Contact):
    """
    Proxy model on :model: `contactfields.Contact` 
    """
    objects = PersonManager()

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        proxy = True

    @staticmethod
    def get_unique_user_name(username, first_name, last_name, company=None):
        '''
        Get a unique user_name when creating new users
        :param username:    given username string
        :param first_name:  first_name string
        :param last_name:   last_name string
        :param company:     company name string
        :return:  a unique username validated from the User set
        '''
        if not username:
            username = '{}{}'.format(first_name, last_name).lower()
        if User.objects.filter(username=username).exists():
            if username[-1].isdigit():
                if username[-3:].isdigit():
                    # add 1 to the previous user with similar to this name
                    return '{}{}'.format(username[0:-3], '000{}'.format(int(username[-3:]) + 1)[-3:])
                else:
                    # add 1 to the previous user with similar name
                    return '{}{}'.format(username[0:-1], '{}'.format(int(username[-1]) + 1))
            else:
                return '{}{}'.format(username, '001')
        else:
            return username

    def get_or_create_user_from_person(self, username, is_active=True):
        """
        Helper function to create a :model: `auth.User`
        from a :model: `contactfields.Person` object
        : param username : string username for the auth.User
        : param is_active : boolean whether the User should be set active
        """

        # find an email if available
        email = ''
        if self.email.exists():
            email = self.email.first().email

        u, created = User.objects.get_or_create(
            username=self.get_unique_user_name(username if username else None, self.name, self.last_name, self.company),
            is_active=is_active,
            first_name=self.name,
            last_name=self.last_name
        )

        if created:
            self.user = u
            self.save()
            # be sure an unusable password is set for a new user
            u.set_unusable_password()
            u.email = email
            u.save()

        return (u)


@receiver(post_save, sender=User)
def create_or_update_user_person(sender, instance, created, **kwargs):
    '''
    Creates or updates a Person whenever a User is saved
    :param sender:      sending model, User in this case
    :param instance:    User instance
    :param created:     whether the User instance was just created
    :param kwargs:      keyword arguments
    :return:            None
    '''
    if created:
        # the person can already exist, so must be find it or create
        try:
            p, new_person = Person.objects.get_or_create(
                name=instance.first_name,
                last_name=instance.last_name,
            )
        except MultipleObjectsReturned:

            p = Person.objects.filter(name=instance.first_name,
                                      last_name=instance.last_name,
                                      email__email__contains=instance.email
                                      ).first()
            if p:
                # set the new user to the existing person with this first and last name and email
                if not p.user:
                    p.user = instance
                    p.save()
                    return
            return

        if new_person:
            p.user = instance
            p.save()
            if instance.email:
                EmailModel.objects.create(
                    contact=p,
                    email=instance.email,
                    primary=True,
                )


class CompanyManager(models.Manager):

    def create(self, **kwargs):
        kwargs.update({'is_company': True})
        return super(CompanyManager, self).create(**kwargs)

    def get_queryset(self):
        return super(CompanyManager, self).get_queryset().filter(
            is_company=True)


class Company(Contact):
    """
    Proxy model on :model: `contactfields.Contact` 
    """
    objects = CompanyManager()

    def save(self, *args, **kwargs):
        self.is_company = True
        super(Company, self).save(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _("Company")
        verbose_name_plural = _("Companies")
