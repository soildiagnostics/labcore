# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .models import PhoneModel, EmailModel, AddressModel, \
    Person, Company, Contact


# Register your models here.
# admin.site.register(Contact)
# admin.site.register(PhoneModel)
# admin.site.register(AddressModel)
# admin.site.register(EmailModel)


class PhoneInline(admin.TabularInline):
    model = PhoneModel


class AddressInline(admin.TabularInline):
    model = AddressModel


class EmailInline(admin.TabularInline):
    model = EmailModel


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    exclude = (
        'is_company',
    )
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]

    list_display = ('get_name', 'company',)
    list_filter = ('company',)
    search_fields = ('last_name', 'name', 'company__name',)
    #autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "company": ['company__name'],
    }

    def get_name(self, instance):
        return '{} {}'.format(instance.name, instance.last_name)
    get_name.short_description =  'Name'

    # suppress add user because the default pop-up is too minimal
    def get_form(self, request, obj=None, **kwargs):
        form = super(PersonAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['user'].widget.can_add_related = False
        return form




@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    exclude = (
        'middle_name',
        'last_name',
        'company',
        'is_company_contact',
        'is_company',
        'user',
    )
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]
    search_fields = ('name',)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    inlines = [
        PhoneInline,
        AddressInline,
        EmailInline
    ]

    list_display = ('__str__', 'company', 'is_company',)
    list_filter = ('company',)
    search_fields = ('last_name', 'name', 'company__name',)
    #autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "company": ['company__name'],
    }

class PersonInline(admin.StackedInline):

    model = Person
    can_delete = False
    verbose_name_plural = 'Person'
    fk_name = 'user'
    inline_classes = ("grp-collapse grp-open",
                      )
    fields = (('name', 'middle_name', 'last_name'),
              ('company', 'is_company_contact'),
              ('image',),
              ('url',),
              ('notes', ),
              )


class CustomUserAdmin(UserAdmin):
    inlines = (PersonInline, )
    list_display = ('get_person', 'username', 'email', 'first_name', 'last_name', 'is_active', 'last_login') #, 'get_location')
    list_select_related = ('person', )

    fieldsets = (
        ("Person", {"classes": ("placeholder person-group",
                                "grp-collapse grp-open",
                                ),
                    "fields": ()
                    }),
        ("User", {
            "classes": ("grp-collapse grp-open",
                        #'collapse', 'extrapretty',
                        ),
            "fields": (#('first_name', 'last_name'),
                       ('username'),
                       ("email"),
                       )
        }),
        ('Permissions', {
            'classes': (),
            'fields': (('is_active','is_staff', ),
                       ('password'),
                       ('groups',),
                       ('user_permissions')
                       ),
        }),
    )

    add_fieldsets = (
            ("User", {
                "classes": ("grp-collapse grp-open",
                            #'collapse', 'extrapretty',
                            ),
                "fields": (('first_name', 'last_name'),
                           ("email"),
                           ('username'),
                           ('is_active','is_staff', ),
                           )
            }),
            ('Permissions', {
                'classes': (),
                'fields': ('password1', 'password2', 'groups', 'user_permissions'),
            }),
        )

    def get_person(self, instance):
        if instance.person:
            return '{} {}'.format(instance.person.name, instance.person.last_name)
        else:
            return instance.username
    get_person.short_description = 'Person'
    get_person.admin_order_field = 'person__name'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['email'] = forms.EmailField(label=_("E-mail"), max_length=75)

CustomUserAdmin.add_form = UserCreationFormExtended

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
