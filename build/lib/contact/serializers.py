from rest_framework import serializers

from .models import Person, Company, PhoneModel, EmailModel, AddressModel, Contact


class PhoneSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PhoneModel
        fields = "__all__"


class AddressFieldSerialzer(serializers.Serializer):
    address = serializers.CharField(required=True, max_length=150)


class AddressModelSerializer(serializers.ModelSerializer):
    address = AddressFieldSerialzer()

    class Meta:
        model = AddressModel
        fields = "__all__"


class EmailModelSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmailModel
        fields = "__all__"


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = "__all__"


class CompanySerializer(serializers.ModelSerializer):
    phone = serializers.StringRelatedField()
    address = serializers.StringRelatedField()
    email = serializers.StringRelatedField()

    class Meta:
        model = Company
        fields = ('name', 'address', 'phone', 'email')


class PersonSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, max_length=100)
    middle_name = serializers.CharField(required=False, max_length=100)
    last_name = serializers.CharField(required=False, max_length=100)
    phone = serializers.StringRelatedField()
    address = serializers.StringRelatedField()
    company = serializers.StringRelatedField()
    email = serializers.StringRelatedField()

    class Meta:
        model = Person
        fields = "__all__"
