from io import StringIO

from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase, Client

from labcore.wsgi import application


class TestAdminPanel(TestCase):
    def setUp(self):
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def test_spider_admin(self):
        client = Client()
        client.login(username=self.username, password=self.password)
        admin_pages = [
            "/admin/",
            # put all the admin pages for your models in here.
            "/admin/auth/",
            "/admin/auth/group/",
            "/admin/auth/group/add/",
            "/admin/auth/user/",
            "/admin/auth/user/add/",
            "/admin/password_change/"
        ]
        for page in admin_pages:
            resp = client.get(page)
            self.assertEqual(resp.status_code, 200)

class TestManagementCommands(TestCase):
    def test_createsu(self):
        out = StringIO()
        call_command('createsu', stdout=out)
        self.assertEqual('', out.getvalue())

class TestWSGIapplication(TestCase):
    def test_wsgi_application(self):
        self.assertNotEqual(application, None)
