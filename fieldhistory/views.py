# from .models import FieldHistory
# # Create your views here.
#
# from django.views.generic.list import ListView
# from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.urls import reverse_lazy
# from django.contrib.auth.mixins import PermissionRequiredMixin
# from associates.role_privileges import DealershipRequiredMixin
#
# class FieldHistoryList(DealershipRequiredMixin, PermissionRequiredMixin, ListView):
#     template_name = "fieldhistory/fieldhistory_list.html"
#     model = FieldHistory
#     permission_required = "orders.add_order"
#
#     def get_context_data(self, **kwargs):
#         context = super(FieldHistoryList, self).get_context_data(**kwargs)
#         context['org'] = self.org
#         return context
#
#     def get_queryset(self):
#         return FieldHistory.objects.filter(field__farm__client__originator__organization=self.org)