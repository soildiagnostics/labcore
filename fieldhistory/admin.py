# from django.contrib import admin
# from .models import Tillage, ChemicalInput, FertilizerApplication, \
#     HerbicideApplication, InsecticideApplication, PesticideApplication, \
#     Seed, SeedRateUnit, YieldUnit, CoverCrop, PreviousCrop, \
#     CropPlan, FieldHistory, ChemicalInputType, ManureApplication
#
#
# # Register your models here.
#
# class TillageInline(admin.TabularInline):
#     model = Tillage
#     extra = 1
#
#
# class ChemicalInputInline(admin.TabularInline):
#     model = ChemicalInput
#     extra = 0
#
#
# class FertilizerApplicationInline(admin.TabularInline):
#     model = FertilizerApplication
#     extra = 0
#
#
# class HerbicideApplicationInline(admin.TabularInline):
#     model = HerbicideApplication
#     extra = 0
#
#
# class InsecticideApplicationInline(admin.TabularInline):
#     model = InsecticideApplication
#     extra = 0
#
#
# class PesticideApplicationInline(admin.TabularInline):
#     model = PesticideApplication
#     extra = 0
#
# class ManureApplicationInline(admin.TabularInline):
#     model = ManureApplication
#     extra = 0
#
# @admin.register(Seed)
# class SeedAdmin(admin.ModelAdmin):
#     model = Seed
#
# class YieldUnitInline(admin.TabularInline):
#     model = YieldUnit
#     extra = 0
#
#
# class PreviousCropInline(admin.TabularInline):
#     model = PreviousCrop
#     extra = 0
#     exclude = ["is_cover_crop"]
#
# class CoverCropInline(admin.TabularInline):
#     model = CoverCrop
#     extra = 0
#     exclude = ["is_cover_crop"]
#
#
# class CropPlanInline(admin.TabularInline):
#     model = CropPlan
#     extra = 0
#     exclude = ["is_cover_crop"]
#
# admin.site.register(SeedRateUnit)
# admin.site.register(ChemicalInput)
# admin.site.register(ChemicalInputType)
# admin.site.register(Tillage)
#
# @admin.register(FieldHistory)
# class FieldHistoryAdmin(admin.ModelAdmin):
#     model = FieldHistory
#     inlines = [
#         FertilizerApplicationInline,
#         ManureApplicationInline,
#         HerbicideApplicationInline,
#         InsecticideApplicationInline,
#         PesticideApplicationInline,
#         PreviousCropInline,
#         CoverCropInline,
#     ]
