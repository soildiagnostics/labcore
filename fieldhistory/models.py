# from django.db import models
# from clients.models import Field
# from django.utils.translation import gettext as _
# from django.contrib.contenttypes.fields import GenericForeignKey
# from django.contrib.contenttypes.models import ContentType
# from django.db.models.signals import post_save
#
# # Create your models here.
#
#
# class FieldHistory(models.Model):
#     field = models.OneToOneField(Field, on_delete = models.CASCADE)
#     updated = models.DateTimeField(auto_now = True)
#
#     class Meta:
#         verbose_name = _("Field History Record")
#         verbose_name_plural = _("Field History Records")
#
# class FieldEvent(models.Model):
#     history = models.ForeignKey(FieldHistory, on_delete=models.CASCADE, related_name="fieldevent")
#     date = models.DateField(auto_now_add=True)
#     notes = models.TextField(blank=True)
#
#     def save(self, *args, **kwargs):
#         self.history.save() # update timestamp
#         super(FieldEvent, self).save(*args, **kwargs)
#
#     class Meta:
#         abstract = True
#         ordering = ['-date']
#
# class Tillage(models.Model):
#     name = models.CharField(max_length=100)
#
#     def __str__(self):
#         return self.name
#
# class ChemicalInputType(models.Model):
#     input_type = models.CharField(max_length=50)
#
#     def __str__(self):
#         return self.input_type
#
#     class Meta:
#         ordering = ['input_type']
#
# DEFAULT_FERTILIZER = 1
# class ChemicalInput(models.Model):
#     name = models.CharField(max_length=200, help_text=_(
#         "Synthetic or natural chemical application"))
#     unit = models.CharField(max_length=100, help_text=_("e.g. lb/ac"))
#     notes = models.TextField(blank=True)
#     input_type = models.ForeignKey(ChemicalInputType,
#                                    on_delete=models.CASCADE, default=DEFAULT_FERTILIZER)
#
#     def __str__(self):
#         return "{} in {}".format(self.name, self.unit)
#
#     class Meta:
#         verbose_name = _("Biological or Chemical Input")
#         verbose_name_plural = _("Biological or Chemical Inputs")
#
#
# class InputApplication(FieldEvent):
#     chemical_input = models.ForeignKey(ChemicalInput, on_delete=models.PROTECT)
#     amount = models.DecimalField(decimal_places=2, max_digits=6)
#
#     def __str__(self):
#         return "{} {} of {}".format(self.amount, self.chemical_input.unit, self.chemical_input)
#
#
# class FertilizerApplication(InputApplication):
#     nitrogen = models.DecimalField(decimal_places=2, max_digits=6, null=True,
#                                    blank=True, help_text=_("Equivalent units of N"))
#     phosphorus = models.DecimalField(decimal_places=2, max_digits=6, null=True,
#                                      blank=True, help_text=_("Equivalent units of P2O5"))
#     potassium = models.DecimalField(decimal_places=2, max_digits=6, null=True,
#                                     blank=True, help_text=_("Equivalent units of K2O"))
#
#
# class HerbicideApplication(InputApplication):
#     pass
#
#
# class FungicideApplication(InputApplication):
#     pass
#
#
# class PesticideApplication(InputApplication):
#     pass
#
#
# class InsecticideApplication(InputApplication):
#     pass
#
# class ManureApplication(InputApplication):
#     pass
#
#
# class SeedRateUnit(models.Model):
#     unit = models.CharField(max_length=50, help_text=_("e.g. lb/ac"))
#
#     def __str__(self):
#         return self.unit
#
#
# class Seed(models.Model):
#     seed = models.CharField(max_length=100)
#     rate = models.DecimalField(decimal_places=2, max_digits=6, help_text=_("Seeding rate"))
#     unit = models.ForeignKey(SeedRateUnit, on_delete=models.PROTECT)
#     notes = models.TextField(blank=True)
#
#     def __str__(self):
#         return "{} at {}".format(self.seed, self.unit)
#
#     class Meta:
#         ordering = ['seed']
#
# class YieldUnit(models.Model):
#     unit = models.CharField(max_length=50, help_text=_("e.g. bu/ac"))
#
#     def __str__(self):
#         return self.unit
#
#     class Meta:
#         ordering = ['unit']
#
#
# class Crop(FieldEvent):
#     history = models.ForeignKey(FieldHistory, on_delete=models.CASCADE, related_name="cropevent")
#     mix = models.ManyToManyField(Seed)
#     planting_date = models.DateField()
#     termination_date = models.DateField()
#     crop_yield = models.DecimalField(
#         decimal_places=2, max_digits=6, blank=True)
#     yield_units = models.ForeignKey(
#         YieldUnit, blank=True, null=True, on_delete=models.PROTECT)
#     tillage = models.ForeignKey(
#         Tillage, on_delete=models.PROTECT, blank=True, null=True)
#     is_cover_crop = models.BooleanField(default=False)
#     is_crop_plan = models.BooleanField(default=False)
#
#     def __str__(self):
#         return str(self.year)
#
#
# class CoverCropManager(models.Manager):
#
#     def create(self, **kwargs):
#         kwargs.update({'is_cover_crop': True})
#         return super(CoverCropManager, self).create(**kwargs)
#
#     def get_queryset(self):
#         return super(CoverCropManager, self).get_queryset().filter(
#             is_cover_crop=True, is_crop_plan=False)
#
#
# class CoverCrop(Crop):
#     """ Type of cover crops """
#     manager = CoverCropManager()
#
#     class Meta:
#         proxy = True
#
#
# class MainCropManager(models.Manager):
#
#     def get_queryset(self):
#         return super(MainCropManager, self).get_queryset().filter(
#             is_cover_crop=False, is_crop_plan=False)
#
#
# class PreviousCrop(Crop):
#     manager = MainCropManager()
#
#     class Meta:
#         proxy = True
#
#
# class CropPlanManager(models.Manager):
#
#     def get_queryset(self):
#         return super(CropPlanManager, self).get_queryset().filter(
#             is_cover_crop=False, is_crop_plan=True)
#
#
# class CropPlan(Crop):
#     manager = CropPlanManager()
#
#     class Meta:
#         proxy = True
#
#
# class Event(models.Model):
#     content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
#     object_id = models.PositiveIntegerField()
#     content_object = GenericForeignKey('content_type', 'object_id')
#     created = models.DateTimeField(null=True)
#     class Meta:
#         ordering = ['-created']
#     def __str__(self):
#         return "{0} - {1}".format(self.content_object.history.field,
#                                   self.created.date())
#
# def create_timeline_event(sender, instance, created, **kwargs):
#     content_type = ContentType.objects.get_for_model(instance)
#     print("post save fired")
#     print(content_type)
#     event, created = Event.objects.get_or_create(content_type=content_type,
#                                                  object_id=instance.id)
#     event.created = instance.date
#     event.save()
#
# post_save.connect(create_timeline_event, sender=InputApplication)
# post_save.connect(create_timeline_event, sender=Crop)
# post_save.connect(create_timeline_event, sender=FertilizerApplication)
# post_save.connect(create_timeline_event, sender=HerbicideApplication)
# post_save.connect(create_timeline_event, sender=InsecticideApplication)
# post_save.connect(create_timeline_event, sender=FungicideApplication)
# post_save.connect(create_timeline_event, sender=PesticideApplication)
# post_save.connect(create_timeline_event, sender=ManureApplication)
# post_save.connect(create_timeline_event, sender=CoverCrop)
# post_save.connect(create_timeline_event, sender=PreviousCrop)
# post_save.connect(create_timeline_event, sender=CropPlan)