from django.contrib import admin

from .models import *


# Register your models here.

@admin.register(SVXParticipant)
class SVXParticipantAdmin(admin.ModelAdmin):
    autocomplete_fields = ('organization',)
    list_display = ("name", "base_url", "id", )

    def name(self, obj):
        return str(obj.organization)
    name.short_description = "Name"

admin.site.register(SVXTransactionStatus)

class TransactionHistoryInline(admin.TabularInline):
    model=SVXTrackingHistory
    readonly_fields = ('participant', 'log')
    extra = 0

@admin.register(SVXTransaction)
class SVXTransactionAdmin(admin.ModelAdmin):
    inlines = [TransactionHistoryInline,]
    readonly_fields = ('initiator', 'vendor', 'internal_reference',
                       'vendor_reference')
    list_display = ('initiator', 'vendor', 'internal_reference',
                       'vendor_reference', 'status')