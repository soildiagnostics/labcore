import json

import requests
from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from associates.role_privileges import DealershipRequiredMixin
from .models import SVXTransaction, SVXParticipant
from .serializers import SVXTransactionSerializer, SVXParticipantSerializer


# Create your views here.

class APISVXSyncLocalDatabaseView(DealershipRequiredMixin, APIView):
    """
    Protocol to get all users known to the system from the SoilDx API Central
    and to sync the local database with it.
    """
    def get(self, request, format=None):

        central_url = settings.SOILDX_API_CENTRAL + reverse('api_participants_list') + "?format=json"
        response = requests.get(central_url)
        if response.status_code==200:
            data = json.loads(response.content)
            SVXParticipant.objects.sync_from_json(data)
            return HttpResponseRedirect(reverse('api_participants_list'))
        else:
            return Response({'status': response.status_code})





class APISVXTransaction(DealershipRequiredMixin, generics.ListCreateAPIView):
    serializer_class = SVXTransactionSerializer

    def get_queryset(self):
        return SVXTransaction.objects.my_transactions(self.org)

class APISVXParticipants(generics.ListAPIView):
    """
    This view is completely public and does not require authentication. It will typically be called only
    on the central hub.
    """
    authentication_classes = []
    permission_classes = []
    serializer_class = SVXParticipantSerializer
    queryset = SVXParticipant.objects.filter(active=True)


class APISVXParticipantProducts(generics.ListAPIView):
    """
    For a given SVX Participant ID, this redirects to the current product list
    of the participant
    """
    def get(self, request, *args, **kwargs):
        try:
            participant = SVXParticipant.objects.get(id=request.query_params['id'])
            urlbase = participant.base_url
            return HttpResponseRedirect(urlbase + reverse("api_products"))
        except SVXParticipant.DoesNotExist:
            return Response({'status': "fail",
                             "message": "SVX Participant with id {} does not exist".format(request.query_params['id'])
                             })

