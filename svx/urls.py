from common.apihelper import apipath
from .views import APISVXTransaction, APISVXParticipants, APISVXParticipantProducts

urlpatterns = [
    apipath("list/", APISVXTransaction.as_view(), name="api_transaction_list"),
    apipath("participants/", APISVXParticipants.as_view(), name="api_svx_participants_list"),
    # apipath("sync/", APISVXSyncLocalDatabaseView.as_view(), name="api_network_list"),
    apipath("products/", APISVXParticipantProducts.as_view(), name="api_participant_product_list"),
]