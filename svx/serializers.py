from rest_framework import serializers

from associates.serializers import OrganizationSerializer
from .models import SVXTrackingHistory, SVXParticipant, SVXTransaction, SVXTransactionStatus, \
    SVXProduct


class SVXParticipantSerializer(serializers.ModelSerializer):
    organization=OrganizationSerializer(read_only=True)
    class Meta:
        model = SVXParticipant
        fields = ("id", "organization", "active", "base_url")

class SVXTransactionHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SVXTrackingHistory
        fields = "__all__"

class SVXTransactionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = SVXTransactionStatus
        fields = "__all__"

class SVXTransactionSerializer(serializers.ModelSerializer):
    initiator = SVXParticipantSerializer(many=False)
    vendor = SVXParticipantSerializer(many=False)
    status = SVXTransactionStatus()
    history = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = SVXTransaction
        fields = "__all__"

class SVXProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = SVXProduct
        fields = "__all__"