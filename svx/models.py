import uuid

from django.db import models
from django.utils.text import gettext_lazy as _

from associates.models import Organization, Role
from associates.role_privileges import NestedAttributeMixin
from common.models import ModelDiffMixin
from contact.models import Company, AddressModel, PhoneModel, EmailModel
from orders.models import Order
from products.models import Product
from samples.models import TimeRecord


# Create your models here.
class SVXParticipantManager(NestedAttributeMixin, models.Manager):

    @property
    def me(self):
        return SVXParticipant.objects.get(organization=Organization.objects.me)


    def sync_from_json(self, datalist):
        #SVXParticipant.objects.all().update(active=False)

        for data in datalist:

            participant, pcreated = SVXParticipant.objects.get_or_create(id=data['id'])
            participant.base_url = data.get('base_url')
            participant.name=data.get('name')
            participant.active=True

            pcompany, ccreated = Company.objects.get_or_create(name=data.get('name'))


            def _update(self, model, key, rmodelfield, wmodelfield):
                try:
                    new_info = data.get('contact').get('company').get(key)
                    known_instances = [self.multi_getattr(a, rmodelfield) for a in model.objects.filter(contact=pcompany)]
                    modified = list()
                    for obj in new_info:
                        value = obj.get(key)
                        if not value:
                            ## quirks!
                            value = obj.get('phone_number')
                        modified.append(value)
                        if value in known_instances:
                            kw = { rmodelfield : value,
                                   'contact' : pcompany }
                            knowns = model.objects.filter(**kw)
                            kwupdate = { f"{key}_type" : obj.get(f"{key}_type"),
                                         'primary' : obj.get('primary')}
                            knowns.update(**kwupdate)
                        else:
                            kwcreate = {
                                wmodelfield : obj.get(key),
                                f"{key}_type" : obj.get(f"{key}_type"),
                                "primary": obj.get("primary"),
                                "contact": pcompany
                            }
                            model.objects.create(**kwcreate)
                    difflist = [ki for ki in known_instances if ki not in modified]
                    kwdel = { 'contact' : pcompany,
                              f"{rmodelfield}__in" : difflist}
                    model.objects.filter(**kwdel).delete()
                except Exception:
                    raise

            _update(self, AddressModel, 'address', 'address__formatted', 'address__formatted')
            _update(self, EmailModel, 'email', 'email', 'email')
            _update(self, PhoneModel, 'phone', 'phone_number', 'phone_number')


            org, ocreated = Organization.objects.get_or_create(company=pcompany)

            for role in data.get('contact').get('roles'):
                r, rcreated = Role.objects.get_or_create(role=role.get('role'))
                r.description = role.get('description')
                r.save()

            participant.contact = org
            participant.save()
            return participant



class SVXParticipant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # name = models.CharField(max_length=200)
    organization = models.OneToOneField(Organization, on_delete=models.PROTECT, related_name="svxparticipant",
                                help_text=_("Associate this provider with an Organization in your database"))
    base_url = models.URLField()
    active = models.BooleanField(default=True)
    objects = SVXParticipantManager()

    def __str__(self):
        return str(self.organization)


class SVXTransactionStatus(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class SVXTransactionManager(models.Manager):

    def my_transactions(self, org):
        return SVXTransaction.objects.filter(
            models.Q(initiator__contact=org) | models.Q(vendor__contact=org)
        )


class SVXTransaction(ModelDiffMixin, TimeRecord):
    initiator = models.OneToOneField(SVXParticipant, on_delete=models.PROTECT, related_name="initiator")
    vendor = models.OneToOneField(SVXParticipant, on_delete=models.PROTECT, related_name="vendor")
    internal_reference = models.OneToOneField(Order, on_delete=models.PROTECT, related_name="svx_internal_reference")
    vendor_reference = models.OneToOneField(Order, on_delete=models.PROTECT, related_name="svx_vendor_reference")
    status = models.ForeignKey(SVXTransactionStatus, on_delete=models.PROTECT)
    objects = SVXTransactionManager()

    def save(self, *args, **kwargs):
        try:
            svxparticipant = kwargs.pop('svxparticipant')
        except KeyError:
            raise
        if self.has_changed:
            SVXTrackingHistory.objects.create(transaction=self,
                                              svxparticipant=svxparticipant,
                                              log="Sample updated: {}".format(self.diff_text))
            obj = super().save(**kwargs)
            return obj
        else:
            obj = super().save(**kwargs)
            SVXTrackingHistory.objects.create(sample=self,
                                          user=svxparticipant,
                                          log="Transaction registered")
            return obj


class SVXTrackingHistory(TimeRecord):
    transaction = models.ForeignKey(SVXTransaction, on_delete=models.CASCADE, related_name="history")
    participant = models.ForeignKey(SVXParticipant, on_delete=models.PROTECT)
    log = models.TextField()

    def __str__(self):
        return self.created.strftime("%m-%d-%Y, %I:%m %p")


class SVXProduct(TimeRecord):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    vendor = models.ForeignKey(SVXParticipant, on_delete=models.PROTECT)
    vendor_product_id = models.IntegerField(editable=False)