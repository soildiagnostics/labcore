from django.test.testcases import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from associates.models import Organization
from svx.models import SVXParticipant
from svx.tests.tests_models import SVXParticipantModelTestCase


class SVXParticipantAPITests(TestCase):

    fixtures = ['roles']
    def setUp(self):
        SVXParticipantModelTestCase.setUp(self)
        self.hub = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXHub"))
        self.vendor = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXVendor"))
        self.dealer = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXDealer"))

    def test_api_list_participants(self):

        url = reverse('api_svx_participants_list')
        client = APIClient()

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        import json
        data = json.loads(response.content)

        self.assertEqual(len(data), 3)


