from django.test.testcases import TestCase

from associates.models import Organization, Role
from contact.models import Company
from svx.models import SVXParticipant


class SVXParticipantModelTestCase(TestCase):

    fixtures = ['roles']
    def setUp(self):
        hub = Company.objects.create(name='SVXHub')
        vendor = Company.objects.create(name="SVXVendor")
        dealer = Company.objects.create(name="SVXDealer")

        huborg = Organization.objects.create(company=hub)
        huborg.roles.add(Role.objects.get(role='Primary'))

        Role.objects.create(role="Vendor")
        vendororg = Organization.objects.create(company=vendor)
        vendororg.roles.add(Role.objects.get(role="Vendor"))

        dealerorg = Organization.objects.create(company=dealer)
        dealerorg.roles.add(Role.objects.get(role="Dealer"))

    def test_create_SVXParticpants(self):
        hub = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXHub"),
                                            base_url="https://hub.skydatarcns.com")
        vendor = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXVendor"),
                                               base_url="https://vendor.skydatarcns.com")
        dealer = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXDealer"),
                                               base_url="https://dealer.skydatarcns.com")

        self.assertNotEqual(hub.id, None)
        self.assertNotEqual(hub.id, vendor.id)
        self.assertNotEqual(hub.id, dealer.id)
        self.assertNotEqual(vendor.id, dealer.id)

    def test_organization_me(self):

        me = Organization.objects.me
        self.assertEqual(me.id, Organization.objects.get(name="SVXHub").id)

    def test_svx_participant_me(self):
        hub = SVXParticipant.objects.create(organization=Organization.objects.get(name="SVXHub"),
                                            base_url="https://hub.skydatarcns.com")

        me = SVXParticipant.objects.me
        self.assertEqual(me.id, hub.id)


