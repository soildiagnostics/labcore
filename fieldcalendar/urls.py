from django.urls import path

from common.apihelper import apipath
from .apiviews import (APIFieldEventsJSON, APIFieldEventUpdateJSON, APIClientFieldEventsJSON,
                       APICalendarEventKinds)
from .views import FieldEventDelete, FieldEventListJSON, FieldEventFileDownload

urlpatterns = [
    path('<int:pk>/delete', FieldEventDelete.as_view(), name="fieldevent_delete"),
    path('eventlist/<int:pk>/', FieldEventListJSON.as_view(), name="event_list_json"),
    path('eventlist/', FieldEventListJSON.as_view(), name="event_list_json_all"),
    path('<int:pk>/download/', FieldEventFileDownload.as_view(), name="fieldevent_file"),
]

## API urls

urlpatterns += [
    apipath('field/<int:pk>/events/', APIFieldEventsJSON.as_view(), name="api_field_events_json"),
    apipath('field/<int:pk>/events/update_event/<event>/', APIFieldEventUpdateJSON.as_view(),
            name="api_field_event_update"),
    apipath('client/<int:pk>/events', APIClientFieldEventsJSON.as_view(), name="api_client_field_events"),
    apipath("categories/", APICalendarEventKinds.as_view(), name="api_calendar_event_kinds"),
]
