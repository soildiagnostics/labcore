from django import forms
from .models import CalendarEvent

class CalendarEventForm(forms.ModelForm):

    class Meta:
        model = CalendarEvent
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.initial.get('created_by'):
            self.initial['created_by'] = self.created_by