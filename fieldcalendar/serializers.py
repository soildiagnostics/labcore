from django.core.files.base import ContentFile
from django.urls import reverse
from rest_framework import serializers

from clients.models import Field
from common.models import ParseTimeMixin
from customnotes.serializers import CustomFieldSerializer
from fieldcalendar.models import FieldEvent
from fieldcalendar.event_base import CalendarEvent, CalendarEventKind
import base64
from django.core.files import File

class CalendaEventSerializer(serializers.ModelSerializer):
    custom_fields = CustomFieldSerializer(many=True)
    class Meta:
        model = CalendarEvent
        fields = "__all__"

class CalendarEventKindSerializer(serializers.ModelSerializer):
    event = CalendaEventSerializer(many=True)
    class Meta:
        model = CalendarEventKind
        fields = "__all__"


class BaseEventSerializer(ParseTimeMixin, serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()
    textColor = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.pk

    def get_title(self, obj):
        return obj.details.get('title')

    def get_color(self, obj):
        return obj.color

    def get_textColor(self, obj):
        try:
            return obj.event_type.kind.textcolor
        except AttributeError:
            return "white"


class FieldEventSerializer(BaseEventSerializer):
    extendedProps = serializers.SerializerMethodField()
    # file = serializers.SerializerMethodField()
    #
    # def get_file(self, obj):
    #     if obj.file:
    #         return reverse("fieldevent_file", kwargs={'pk' : obj.id})

    def get_extendedProps(self, obj):
        sent = {k: v for k, v in obj.details.items() if k in ['field_id',
                                                              'field_name',
                                                              'event_type',
                                                              'file',
                                                              'file_type',
                                                              'geotags',
                                                              'geolocation',
                                                              'custom_fields']}
        if obj.file:
            sent["file"] = reverse("fieldevent_file", kwargs={'pk' : obj.id})
        else:
            sent["file"] = None
        if obj.event_type and obj.event_type.icon:
            sent['icon'] = obj.event_type.icon.url
        return sent

    class Meta:
        model = FieldEvent
        fields = ['id', 'allDay', 'start', 'end', 'title', 'event_type',
                  'editable', 'color', 'extendedProps', 'textColor']

    def validate(self, data):
        data = super().validate(data)
        data['field'] = Field.objects.get(pk=int(self.initial_data['extendedProps']['field_id']))
        data['details'] = self.initial_data['extendedProps']
        data['details']['title'] = self.initial_data['title']

        return data

    def create(self, validated_data):
        if 'task' in self.initial_data['extendedProps'].keys():
            validated_data['details'] = FieldEvent.objects.create_task_customfield(validated_data['details'])
        elif "photo" in self.initial_data["extendedProps"].keys():
            photo = self.initial_data['extendedProps'].pop("photo")

            photo_bytes = base64.decodebytes(photo.encode("utf-8"))
            name = self.initial_data["title"]
            # validated_data['file'] = File(photo_bytes, name)
            validated_data["details"] = FieldEvent.objects.create_photo_memo_customfields(validated_data["details"])
            fe = super().create(validated_data)
            fe.file.save(name, ContentFile(photo_bytes))
            return fe
        elif "geolocation" in self.initial_data['extendedProps'].keys():
            validated_data["details"] = FieldEvent.objects.create_geo_memo_customfields(validated_data["details"])
        return super().create(validated_data)


    def update(self, instance, validated_data):
        return super().update(instance, validated_data)
