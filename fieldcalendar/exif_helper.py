from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS


class EXIFMixin(object):

    def _get_exif(self, filename):
        image = Image.open(filename)
        image.verify()
        return image._getexif()

    def _get_labeled_exif(self, filename, gps=False):
        exif = self._get_exif(filename)
        labeled = dict()
        for (key, val) in exif.items():
            label = TAGS.get(key)
            if label == "GPSInfo":
                geovals = dict()
                for gkey, gval in val.items():
                    geovals[GPSTAGS.get(gkey)] = gval
                val = geovals
            if gps and label != "GPSInfo":
                ## store only GPS tags
                continue
            labeled[label] = val

        return labeled

    def exif(self, filename, gps=False):
        return self._get_labeled_exif(filename, gps=gps)

    def get_float_from_frac(self, frac):
        return frac[0] / frac[1]

    def get_decimal_from_dms(self, dms, ref):
        degrees = self.get_float_from_frac(dms[0])
        minutes = self.get_float_from_frac(dms[1]) / 60.0
        seconds = self.get_float_from_frac(dms[2]) / 3600.0

        if ref in ['S', 'W']:
            degrees = -degrees
            minutes = -minutes
            seconds = -seconds

        return round(degrees + minutes + seconds, 5)

    def get_coordinates(self, geotags):
        """
        https://exiftool.org/TagNames/GPS.html
        :param geotags:
        :return:
        """
        point = dict()
        point['latitude'] = self.get_decimal_from_dms(geotags['GPSLatitude'], geotags['GPSLatitudeRef'])
        point['longitude'] = self.get_decimal_from_dms(geotags['GPSLongitude'], geotags['GPSLongitudeRef'])
        point['altitude'] = self.get_float_from_frac(geotags["GPSAltitude"])
        point['altitude_ref'] = "ASL" if geotags["GPSAltitudeRef"] == b'\x00' else "BSL"
        point['bearing'] = self.get_float_from_frac(geotags["GPSImgDirection"])
        point['error'] = self.get_float_from_frac(geotags["GPSHPositioningError"]) # meters

        return point

