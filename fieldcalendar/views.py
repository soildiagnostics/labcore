import boto3
from botocore.client import Config
from botocore.exceptions import ClientError
from django.conf import settings
from django.shortcuts import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import RedirectView, DetailView
from django.views.generic.edit import DeleteView
from django_datatables_view.base_datatable_view import BaseDatatableView

from associates.role_privileges import FilteredObjectMixin, DealershipRequiredMixin, FilteredQuerySetMixin
from common.searchquery import SearchQueryAssemblyMixin
from .models import FieldEvent


# Create your views here.

class FieldEventDelete(FilteredObjectMixin, DealershipRequiredMixin, DeleteView):
    model = FieldEvent
    instance_owner_organization = "field__farm__client__originator__organization"
    client_has_permission = False

    def get_success_url(self):
        vr = FieldEvent.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('field_detail', kwargs={'pk': vr.field.pk})


class FieldEventListJSON(DealershipRequiredMixin, FilteredQuerySetMixin,
                         SearchQueryAssemblyMixin, BaseDatatableView):
    model = FieldEvent
    columns = ['created',
               'field',
               'category',
               'event',
               'start',
               'details']

    order_columns = ['created',
                     'field',
                     'category',
                     'event',
                     'start']

    max_display_length = 500
    client_has_permission = True
    instance_owner_client = 'field__farm__client'
    instance_owner_organization = 'field__farm__client__originator__organization'

    search_fields = [
        'event_type__name',
        'event_type__kind__name',
        'field__name',
        'details__custom_fields'
    ]

    def get_initial_queryset(self):
        qs = super(FieldEventListJSON, self).get_initial_queryset()
        client = self.kwargs.get('pk')
        if not client:
            return qs
        return qs.filter(field__farm__client__id=self.kwargs.get('pk'))

    def render_column(self, row, column):
        if column == 'category':
            try:
                name = row.event_type.kind.name
                color =  f"""<span><i class="fa fa-lg fa-square pull-right" style="color:{row.event_type.kind.color}"></i>"""
            except AttributeError:
                name = row.event_id
                color = ""
            return f"""{name} {color}"""

        elif column == 'event':
            try:
                return row.event_type.name
            except AttributeError:
                if row.event_id == "System Event":
                    return row.details.get('title')
                return None
        elif column == 'details':
            output = list()
            try:
                for item in row.details.get("custom_fields"):
                    output.append(f"{item['name']}: {item['value']}")
                return "<br>".join(output)
            except TypeError:
                return None
        elif column == 'field':
            link = "<a href='{0}'>{1} {2}</a>".format(
                reverse("field_detail", kwargs={'pk': row.field.pk}),
                row.field.name,
                "({} Acres)".format(row.field.area) if row.field.area else ""
            )
            return link
        else:
            return super(FieldEventListJSON, self).render_column(row, column)




class FieldEventFileDownload(DealershipRequiredMixin, RedirectView, DetailView):
    model = FieldEvent
    client_has_permission = True
    instance_owner_client = 'field__farm__client'
    hallpass_field = 'field'

    def create_presigned_url(self, bucket_name, object_name, expiration=120):
        """Generate a presigned URL to share an S3 object

        :param bucket_name: string
        :param object_name: string
        :param expiration: Time in seconds for the presigned URL to remain valid
        :return: Presigned URL as string. If error, returns None.
        """

        # Generate a presigned URL for the S3 object
        s3_client = boto3.client('s3', settings.AWS_REGION,
                                 aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                                 config=Config(signature_version="s3v4"))
        try:
            return s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': object_name},
                                                    ExpiresIn=expiration)
        except ClientError as e:
            print(e)
            return None

    def get(self, request, *args, **kwargs):
        event = self.get_object()
        if hasattr(settings, "AWS_REGION"):
            object_name = f'{settings.MEDIAFILES_LOCATION}/{event.file.name}'
            url = self.create_presigned_url(settings.AWS_STORAGE_BUCKET_NAME, object_name)
        else:
            url = event.file.url
        response = HttpResponseRedirect(url, content_type='application/octet-stream')
        response['Content-Disposition'] = f'attachment; filename={event.file.name}'
        return response