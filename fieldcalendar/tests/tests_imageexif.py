from django.test import TestCase

from fieldcalendar.exif_helper import EXIFMixin
import pprint

class TestIMGExif(TestCase):

    def setUp(self):
        pass

    def test_exif_extraction(self):
        exifobj = EXIFMixin()
        ex = exifobj.exif("./fieldcalendar/tests/IMG_3601.JPG")
        # pprint.pprint(ex)
        self.assertEqual(ex.get("DateTime"), '2020:03:28 10:40:41')

    def test_exif_coords(self):
        exifobj = EXIFMixin()
        ex = exifobj.exif("./fieldcalendar/tests/IMG_3601.JPG")
        coords = exifobj.get_coordinates(ex["GPSInfo"])
        self.assertAlmostEqual(coords.get("altitude"), 215.44, 2)
        #pprint.pprint(coords)
