import datetime
import json

from django.test import TestCase
from django.utils.timezone import now

from clients.tests.tests_models import ClientsTestCase
from fieldcalendar.mixins import FieldJournalMixin
from fieldcalendar.models import CalendarEvent, CalendarEventKind, FieldEvent


# Create your tests here.
class FieldEventCalendarModelTests(TestCase):

    fixtures = ['calendar_events','roles']

    def setUp(self):
        pass

    def test_create_event_kind(self):

        c, created = CalendarEventKind.objects.get_or_create(name="Planting")
        self.assertFalse(created)
        self.assertEqual(str(c), "Planting")

    def test_create_calendarevent(self):

        kind = CalendarEventKind.objects.get(name="Planting")
        c, created = CalendarEvent.objects.get_or_create(kind = kind, name="Corn")
        self.assertFalse(created)
        self.assertEqual(str(c), "Corn (Planting)")

    def test_create_protected_event(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        details = {
            "id": "",
            "color": "green",
            "start": now().isoformat(),
            "end": now().isoformat(),
            "title": "Corn",
            "allDay": True,
            "editable": False,
            "field_id": f.pk,
            "className": [],
            "custom_fields": []
        }

        fe = FieldEvent.objects.create_protected_event(field=f, details=details)
        self.assertEqual(fe.pk, FieldEvent.objects.last().pk)
        self.assertEqual(fe.pk, int(FieldEvent.objects.last().event_id))
        self.assertEqual(fe.pk, int(FieldEvent.objects.last().details['id']))


    def test_field_event(self):

        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)
        with self.assertRaises(AssertionError):
            FieldEvent.objects.create(field=f, event_id="test")

    def test_create_journal_entry(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)
        e = FieldEvent.objects.create_journal_entry(f, "Test", "Captain's Log", now(), now(), allDay=True, editable=False)
        self.assertEqual(e.details['custom_fields'][0]['value'], "Captain's Log")

        ## Journal entry should not be editable
        self.assertEqual(e.details['editable'], False)

    def test_mixin(self):

        class NewObj(FieldJournalMixin):
            pass

        instance = NewObj()
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        e = instance.create_journal_entry(f, "Test", "Captain's Log")
        self.assertEqual(e.details['custom_fields'][0]['value'], "Captain's Log")

        ## Journal entry should not be editable
        self.assertEqual(e.details['editable'], False)

    def test_create_file_upload_event(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)
        e = FieldEvent.objects.create_file_upload_event(field=f, title="Title", start=now(), end=now())

        self.assertIsNotNone(e.pk)
        self.assertIsNotNone(e.details['color'])



