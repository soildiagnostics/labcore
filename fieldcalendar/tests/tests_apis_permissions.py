import json
import os
from datetime import timedelta

from django.contrib.gis.geos import Polygon, MultiPolygon, GeometryCollection
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now

from clients.models import Client, HallPass, SubfieldLayer
from clients.tests.tests_view_permissions import FieldViewPermissionsTests
from contact.models import User
from fieldcalendar.models import FieldEvent, CalendarEvent


class FieldAPIPermissionsTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        FieldViewPermissionsTests.setUp(self)

        ## Create subfield layers on field 1:
        layer1 = SubfieldLayer.objects.create(name="layer1", field=self.field1)

        p1 = Polygon(((0, 0), (0, 1), (1, 1), (0, 0)))
        p2 = Polygon(((1, 1), (1, 2), (2, 2), (1, 1)))
        mp = MultiPolygon(p1, p2)
        layer1.geoms = GeometryCollection(mp)
        layer1.save()

        self.assertEqual(str(layer1), "Field1 (layer1)")
        SubfieldLayer.objects.create(name="layer2", field=self.field1,
                                     geoms = GeometryCollection(mp))
        self.assertEqual(len(self.field1.layer.all()), 2)

        ## Create FieldEvents on Field1

        self.event1 = FieldEvent.objects.create_journal_entry(self.field1, "Test1", "Captain's Log", now(), now(),
                                                              allDay=True,
                                                              editable=False)
        self.event2 = FieldEvent.objects.create_journal_entry(self.field2, "Test2", "Captain's Log", now(), now(),
                                                              allDay=True,
                                                              editable=False)
        self.assertEqual(FieldEvent.objects.filter(field=self.field1).count(), 1)
        self.assertEqual(FieldEvent.objects.filter(field=self.field2).count(), 1)

    def test_not_authenticated_failure(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()

        response = tc.get(url)
        self.assertEqual(response.status_code, 302)

        url = reverse('api_client_field_events', kwargs={'pk': self.client1.pk})
        url = url + '?' + urlencode(time)

        response = tc.get(url)
        self.assertEqual(response.status_code, 302)

    def test_staffuser_can_see_calendar(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.staffuser)


        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_dealeruser_can_see_calendar(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_clientuser_can_see_calendar(self):
        user = self.client1.contact.get_or_create_user_from_person(username='client1', is_active=True)
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

        url = reverse('api_client_field_events', kwargs={'pk': self.client1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_wrong_dealeruser_cannot_see_calendar(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser2.user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_different_orguser_can_see_calendar(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser2.user)

        hp = HallPass.objects.create(field=self.field1, user=self.orguser2.user, created_by=self.staffuser)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

        hp.expiry = now() - timedelta(seconds=1)
        hp.save()

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_staff_can_see_client_calendar(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_client_field_events', kwargs={'pk': self.client1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.staffuser)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_orguser_can_see_only_one_clients_calendar(self):
        ## We are going to set client2 as org1's client.
        self.client2.originator = self.orguser1
        self.client2.save()

        self.assertEqual(Client.objects.filter(originator__organization=self.orguser1.organization).count(), 2)
        self.assertEqual(Client.objects.filter(originator__organization=self.orguser2.organization).count(), 0)

        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

        url = reverse('api_field_events_json', kwargs={'pk': self.field2.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser2.user)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_hallpass_doesnt_work_on_client_calendar_api(self):
        time = {
            'start': now() - timedelta(days=2),
            'end': now() + timedelta(days=1)
        }

        url = reverse('api_client_field_events', kwargs={'pk': self.client1.pk})
        url = url + '?' + urlencode(time)

        tc = TestClient()
        tc.force_login(self.orguser2.user)

        hp = HallPass.objects.create(field=self.field1, user=self.orguser2.user, created_by=self.staffuser)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_update_event(self):
        time = now()

        details = {
            "id": "",
            "color": "green",
            "title": "Corn",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": False,
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": self.field1.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        client = TestClient()
        client.force_login(self.orguser1.user)

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        details['start'] = (now() + timedelta(hours=2)).isoformat()
        details['end'] = (now() + timedelta(hours=3)).isoformat()
        details['extendedProps']['custom_fields'] = ['123']
        data = json.dumps(details)
        url = reverse('api_field_event_update', kwargs={'pk': self.field1.pk, 'event': FieldEvent.objects.latest('pk').event_id})
        response = client.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        from common.models import ParseTimeMixin
        tp = ParseTimeMixin()
        self.assertEqual(tp.parse_with_tz(json.loads(response.content)['end']),
                         tp.parse_with_tz(details['end']))

        self.assertEqual(json.loads(response.content)['extendedProps']['custom_fields'], ['123'])

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], self.field1.pk)
        self.assertEqual(len(resp), 2)

    def test_update_event_with_hallpass(self):

        ## Create new event with originator
        time = now()

        details = {
            "id": "",
            "color": "green",
            "title": "Corn",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": False,
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": self.field1.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        data = json.dumps(details)
        response = tc.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)  ## New event created


        tc.force_login(self.orguser2.user) ## Try logging in as different user and updating.

        details['start'] = (now() + timedelta(hours=2)).isoformat()
        details['end'] = (now() + timedelta(hours=3)).isoformat()
        details['extendedProps']['custom_fields'] = ['123']
        data = json.dumps(details)
        url = reverse('api_field_event_update',
                      kwargs={'pk': self.field1.pk, 'event': FieldEvent.objects.latest('pk').event_id})
        response = tc.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 404)  ## Should fail!

        # Now we get a hall pass

        hp = HallPass.objects.create(field=self.field1, user=self.orguser2.user, created_by=self.staffuser)

        response = tc.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 200)  ## should work now.

        from common.models import ParseTimeMixin
        tp = ParseTimeMixin()
        self.assertEqual(tp.parse_with_tz(json.loads(response.content)['end']),
                         tp.parse_with_tz(details['end']))

        self.assertEqual(json.loads(response.content)['extendedProps']['custom_fields'], ['123'])

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], self.field1.pk)
        self.assertEqual(len(resp), 2)

    def test_staff_can_update_event(self):
        time = now()

        details = {
            "id": "",
            "color": "green",
            "title": "Corn",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": False,
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": self.field1.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        tc = TestClient()
        tc.force_login(self.orguser1.user)

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        data = json.dumps(details)
        response = tc.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.client.force_login(self.staffuser)
        details['start'] = (now() + timedelta(hours=2)).isoformat()
        details['end'] = (now() + timedelta(hours=3)).isoformat()
        details['extendedProps']['custom_fields'] = ['123']
        data = json.dumps(details)
        url = reverse('api_field_event_update',
                      kwargs={'pk': self.field1.pk, 'event': FieldEvent.objects.latest('pk').event_id})
        response = tc.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        from common.models import ParseTimeMixin
        tp = ParseTimeMixin()
        self.assertEqual(tp.parse_with_tz(json.loads(response.content)['end']),
                         tp.parse_with_tz(details['end']))

        self.assertEqual(json.loads(response.content)['extendedProps']['custom_fields'], ['123'])

        url = reverse('api_field_events_json', kwargs={'pk': self.field1.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], self.field1.pk)
        self.assertEqual(len(resp), 2)


    def test_create_hallpass_for_another_client(self):

        # allow client1 to view field2
        ## First we create a user for the contact.
        user = self.client1.contact.get_or_create_user_from_person(username='client1', is_active=True)

        hp = HallPass.objects.create(field=self.field2, user=user, created_by=self.staffuser)

        tc = TestClient()
        tc.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': self.field2.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = tc.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], self.field2.pk)
        self.assertEqual(len(resp), 1)

    def test_hallpass_works_for_rasters(self):
        raster_file = 'test_elev_raster.png'
        raster_path = os.path.join('clients/tests/', raster_file)
        url = reverse('field_geo_raster_post', kwargs={'pk': self.field1.pk,
                                                       'file': raster_file,
                                                       'parameter': '13_DEM',
                                                       'layer': 'soildx'})
        user = User.objects.get(username="testinguser")
        self.client.force_login(user)
        with open(raster_path, 'rb') as fp:
            resp = self.client.post(url,
                                    {"file": fp},
                                    )
        self.assertEqual(resp.status_code, 201)

        url = reverse("field_geo_raster", kwargs={'pk': self.field1.pk,
                                                  "subfield": SubfieldLayer.objects.latest('pk').id,
                                                  'file': 'soildx',
                                                  'parameter': '13_DEM'})
        tc = TestClient()
        tc.force_login(self.orguser1.user)
        response = tc.get(url)
        self.assertEqual(response.status_code, 200)

        tc.force_login(self.orguser2.user)
        response = tc.get(url)
        self.assertEqual(response.status_code, 403)

        user = self.client2.contact.get_or_create_user_from_person(username='client2', is_active=True)

        tc.force_login(user)
        response = tc.get(url)
        self.assertEqual(response.status_code, 403)

        hp = HallPass.objects.create(field=self.field1, user=user, created_by=self.staffuser)
        response = tc.get(url)
        self.assertEqual(response.status_code, 200)


