import json
from datetime import timedelta

from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.timezone import now

from clients.tests.tests_models import ClientsTestCase
from contact.models import User
from fieldcalendar.models import FieldEvent, CalendarEventKind, CalendarEvent


class FieldCalendarViewTests(TestCase):
    fixtures = ['calendar_events', 'roles']

    def setUp(self):
        kind, _ = CalendarEventKind.objects.get_or_create(name="Planting")
        c, _ = CalendarEvent.objects.get_or_create(kind=kind, name="Corn")

    def test_get_events(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        details = {
            "_id": "_fc8",
            "color": "green",
            "start": now().isoformat(),
            "end": now().isoformat(),
            "title": "Corn",
            "allDay": True,
            "editable": False,
            "field_id": f.pk,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        FieldEvent.objects.create_system_event(field=f, title="System Event", start=now(), end=now())

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)

        self.assertTrue('start' not in resp[0]['extendedProps'].keys())

        ### Get no events

        time = {
            'start': now() - timedelta(days=2),
            'end': now() - timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(len(resp), 0)

    def test_get_client_events(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        details = {
            "_id": "_fc8",
            "color": "green",
            "start": now().isoformat(),
            "end": now().isoformat(),
            "title": "Corn",
            "allDay": True,
            "editable": False,
            "field_id": f.pk,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        FieldEvent.objects.create_system_event(field=f, title="System Event", start=now(), end=now())

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)

        self.assertTrue('start' not in resp[0]['extendedProps'].keys())

        ### Get no events

        time = {
            'start': now() - timedelta(days=2),
            'end': now() - timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(len(resp), 0)

    def test_post_new_event(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "color": "green",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": True,
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['field_id'], f.pk)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)
        self.assertEqual(len(resp), 1)

    def test_post_scheduled_task(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "task": "This is a task"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['field_id'], f.pk)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'This is a task')
        self.assertEqual(resp[0]['title'], "Corn")
        self.assertTrue(resp[0]['editable'])
        self.assertEqual(len(resp), 1)

    def test_post_completed_task(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "task": "Completed: This is a task"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['field_id'], f.pk)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'Completed: This is a task')
        self.assertEqual(resp[0]['title'], "Corn")
        self.assertFalse(resp[0]['editable'])
        self.assertEqual(len(resp), 1)

    def test_post_completed_task2(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": 'Corn',
            "editable": False,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "task": "This is a task"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['extendedProps']['field_id'], f.pk)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['editable'], False)
        self.assertEqual(len(resp), 1)

    def test_update_existing_event(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "color": "green",
            "title": "Corn",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": False,
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        details['start'] = (now() + timedelta(hours=2)).isoformat()
        details['end'] = (now() + timedelta(hours=3)).isoformat()
        details['extendedProps']['custom_fields'] = ['123']
        data = json.dumps(details)
        url = reverse('api_field_event_update', kwargs={'pk': f.pk, 'event': FieldEvent.objects.last().event_id})
        response = client.put(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        from common.models import ParseTimeMixin
        tp = ParseTimeMixin()
        self.assertEqual(tp.parse_with_tz(json.loads(response.content)['end']),
                         tp.parse_with_tz(details['end']))

        self.assertEqual(json.loads(response.content)['extendedProps']['custom_fields'], ['123'])

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)
        self.assertEqual(len(resp), 1)

    def test_delete_event(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "color": "green",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "allDay": True,
            "editable": True,
            "title": "Corn",
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "event_type": f"event_{CalendarEvent.objects.last().pk}"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        data = json.dumps(details)
        response = client.post(url, data=data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_field_event_update', kwargs={'pk': f.pk, 'event': FieldEvent.objects.last().event_id})
        response = client.delete(url)
        self.assertEqual(response.status_code, 204)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(len(resp), 0)

    def test_post_photo(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()
        name = "IMG_3601.JPG"
        import base64
        with open(f"./fieldcalendar/tests/{name}", "rb") as photo:
            photo_dat = base64.b64encode(photo.read()).decode("utf-8")

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": name,
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "photo": photo_dat,
                "photo_memo": "This is a custom memo about the photo"
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        # data = json.dumps(details)
        response = client.post(url, data=details, content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.content)['extendedProps']['field_id'], f.pk)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})

        time = {
            'start': now() - timedelta(days=1),
            'end': now() + timedelta(days=1)
        }

        url = url + '?' + urlencode(time)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)
        self.assertEqual(resp[0]['extendedProps']['field_id'], f.pk)
        self.assertEqual(resp[0]['extendedProps']['custom_fields'][0]['value'], 'This is a custom memo about the photo')
        self.assertEqual(resp[0]['title'], name)
        self.assertEqual(len(resp), 1)
        fe = FieldEvent.objects.get(id=resp[0].get("id"))

        self.assertAlmostEqual(fe.details.get("geotags").get("bearing"), 329.952, 2)
        self.assertAlmostEqual(resp[0]['extendedProps'].get("geotags").get("bearing"), 329.952, 2)

        self.assertEqual(FieldEvent.objects.filter(details__geotags__isnull=False).count(), 1)

    def test_post_geonote(self):
        tc = ClientsTestCase()
        ClientsTestCase.setUp(tc)
        f = ClientsTestCase.createField(tc)

        time = now()

        details = {
            "id": "",
            "start": time.isoformat(),
            "end": time.isoformat(),
            "title": "Scouting Note",
            "editable": True,
            "className": [],
            "extendedProps": {
                "field_id": f.pk,
                "custom_fields": [],
                "geolocation": {"latitude": 40.72256900,
                                "longitude": -88.5061365},
                "memo": 'This is a custom memo about this location'
            }
        }

        client = TestClient()
        user = User.objects.get(username="testinguser")
        client.force_login(user)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        response = client.post(url, data=details, content_type="application/json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)[0]
        self.assertEqual(resp['extendedProps']['field_id'], f.pk)
        self.assertEqual(resp['extendedProps']['custom_fields'][0]['value'],
                         'This is a custom memo about this location')
        self.assertEqual(resp['title'], "Scouting Note")
        fe = FieldEvent.objects.get(id=resp.get("id"))

        self.assertAlmostEqual(fe.details.get("geolocation").get("latitude"), 40.722, 2)

        self.assertEqual(FieldEvent.objects.filter(details__geolocation__isnull=False).count(), 1)
        ## Let's try updating it.
        resp['extendedProps']['custom_fields'][0]['value'] = "This is an updated memo"
        resp['extendedProps']['geolocation']["latitude"] = 40.73

        url = reverse('api_field_event_update', kwargs={'pk': f.pk, 'event': fe.event_id})
        response = client.put(url, data=resp, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        url = reverse('api_field_events_json', kwargs={'pk': f.pk})
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        resp = json.loads(response.content)[0]
        self.assertEqual(resp['extendedProps']['field_id'], f.pk)
        self.assertEqual(resp['extendedProps']['custom_fields'][0]['value'],
                         'This is an updated memo')
        self.assertEqual(resp['title'], "Scouting Note")
        fe = FieldEvent.objects.get(id=resp.get("id"))

        self.assertAlmostEqual(fe.details.get("geolocation").get("latitude"), 40.73, 2)
        self.assertEqual(FieldEvent.objects.filter(details__geolocation__isnull=False).count(), 1)
