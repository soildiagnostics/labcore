from django.utils.timezone import now
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView

from associates.role_privileges import DealershipRequiredMixin, FilteredQuerySetMixin
from clients.models import Field, Client
from common.models import ParseTimeMixin
from fieldcalendar.models import FieldEvent
from fieldcalendar.serializers import FieldEventSerializer, CalendarEventKindSerializer, CalendaEventSerializer
from fieldcalendar.event_base import CalendarEvent, CalendarEventKind



class APICalendarEventKinds(ListAPIView, DealershipRequiredMixin):
    model = CalendarEventKind
    client_has_permission = True
    serializer_class = CalendarEventKindSerializer

    def get_queryset(self):
        appname = self.request.query_params.get("app", "FieldEvent")
        return CalendarEventKind.objects.user_editable(self.org, app=appname)



class APIFieldEventsJSONBase(ParseTimeMixin, FilteredQuerySetMixin, DealershipRequiredMixin):
    model = FieldEvent
    client_has_permission = True
    instance_owner_client = 'field__farm__client'
    instance_owner_organization = 'field__farm__client__originator__organization'
    serializer_class = FieldEventSerializer
    related_model = Field

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        pk = self.kwargs.get('pk')
        field = self.related_model.objects.get(pk=pk)
        qs = qs.filter(field=field)
        if qs.count() == 0:
            if field.hallpass.filter(user=self.request.user,
                                     expiry__gte=now()).exists():
                qs = self.model.objects.filter(field=field)
        try:
            qs = qs.filter(start__gte=self.parse_with_tz(self.request.query_params.get('start')))
        except TypeError:
            pass
        try:
            qs = qs.filter(start__lte=self.parse_with_tz(self.request.query_params.get('end')))
        except TypeError:
            pass
        return qs

    def get_queryset(self):
        return self.get_initial_queryset()

class APIFieldEventsJSON(APIFieldEventsJSONBase, ListCreateAPIView):
    pass

class APIFieldEventUpdateJSON(APIFieldEventsJSONBase, RetrieveUpdateDestroyAPIView):
    lookup_field = 'pk'
    lookup_url_kwarg = 'event'

    def get_queryset(self):
        return self.get_initial_queryset().filter(editable=True)



class APIClientFieldEventsJSON(ParseTimeMixin, FilteredQuerySetMixin, DealershipRequiredMixin, ListAPIView):
    model = FieldEvent
    client_has_permission = True
    instance_owner_client = 'field__farm__client'
    instance_owner_organization = 'field__farm__client__originator__organization'
    serializer_class = FieldEventSerializer

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        pk = self.kwargs.get('pk')
        try:
            client = Client.objects.get(pk=pk)
            start = self.parse_with_tz(self.request.query_params.get('start'))
            end = self.parse_with_tz(self.request.query_params.get('end'))
            return qs.filter(field__farm__client=client, start__gte=start, start__lte=end)
        except Client.DoesNotExist:
            """This happens when the referrer is the GIS page, and when the client is inactive """
            return FieldEvent.objects.none()

    def get_queryset(self):
        return self.get_initial_queryset()




