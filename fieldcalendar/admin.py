from django.contrib import admin
from django.utils.safestring import mark_safe

from associates.models import OrganizationUser
from customnotes.admin import CustomFieldInline
from .models import CalendarEventKind, CalendarEvent, FieldEvent
from .forms import CalendarEventForm


# Register your models here.
@admin.register(CalendarEventKind)
class CalendarEventKindAdmin(admin.ModelAdmin):
    list_display = ('name', 'color', 'system_event', 'app')


@admin.register(FieldEvent)
class FieldEventAdmin(admin.ModelAdmin):
    raw_id_fields = ['field']
    list_display = ['field', 'event_id', 'event_type', 'start', 'title', 'get_custom_fields']
    list_filter = ["event_type", "event_type__kind"]
    search_fields = ["field__name", "field__farm__name",
                     "field__farm__client__contact__name",
                     "field__farm__client__contact__last_name",
                     "details"]
    def get_custom_fields(self, obj):
        try:
            fields = list()
            for dictionary in obj.details.get('custom_fields'):
                fields.append(f"{dictionary.get('name')}: {dictionary.get('value')}")
            return mark_safe("<br>".join(fields))
        except TypeError:
            return None
    get_custom_fields.short_description = "Custom fields"


@admin.register(CalendarEvent)
class CalendarEventAdmin(admin.ModelAdmin):
    list_display = ['name', 'kind', 'kind__app', 'created_by']
    list_filter = ['kind', 'kind__app']
    inlines = [
        CustomFieldInline,
    ]

    form = CalendarEventForm

    def get_form(self, request, obj=None, **kwargs):
        org = OrganizationUser.objects.get(user=request.user).organization
        form = super().get_form(request, obj, **kwargs)
        form.created_by=org
        return form

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        org = OrganizationUser.objects.get(user=request.user).organization
        return qs.filter(created_by=org)

    def kind__app(self, obj):
        return obj.kind.app