from django.utils.timezone import now

from .models import FieldEvent


class FieldJournalMixin(object):
    """
    A small mixin to add a method to any model that permits the
    creation of a Field Event associated with the field.
    """

    def create_journal_entry(self, field, title, entry, start=None, end=None):
        '''
        Create an entry using a FieldEventManager method
        :return: the created fe
        '''
        if not start:
            start = now()
        if not end:
            end = now()

        return FieldEvent.objects.create_journal_entry(field, title, entry, start, end, allDay=False, editable=False)
