# Generated by Django 2.1.5 on 2020-02-25 18:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fieldcalendar', '0019_auto_20200216_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendareventkind',
            name='app',
            field=models.CharField(default='FieldEvent', max_length=20),
        ),
    ]
