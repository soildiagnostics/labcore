# Generated by Django 2.0.7 on 2018-09-20 20:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fieldcalendar', '0005_auto_20180920_0841'),
    ]

    operations = [
        migrations.AddField(
            model_name='fieldevent',
            name='event_id',
            field=models.CharField(default='0', max_length=10),
            preserve_default=False,
        ),
    ]
