from django.contrib.contenttypes.fields import GenericRelation
#from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models import Q
from django.utils.functional import cached_property

from associates.models import Organization
from common.models import ParseTimeMixin, prefetch_id
from customnotes.models import CustomField
from django.utils.translation import gettext_lazy as _


class CalendarEventKindManager(models.Manager):

    def user_editable(self, org, app="FieldEvent"):
        qs = CalendarEventKind.objects.exclude(system_event=True).filter(app=app)
        cat_dict = dict()
        for cat in qs:
            event_list = CalendarEvent.objects.filter(Q(created_by=org) | Q(created_by__isnull=True), kind=cat)
            if event_list.count() > 0:
                cat_dict[cat] = event_list
        return cat_dict


class CalendarEventKind(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=20, blank=True)
    textcolor = models.CharField(max_length=20, default="white")
    objects = CalendarEventKindManager()
    system_event = models.BooleanField(default=False)
    app = models.CharField(max_length=20, default="FieldEvent",
                           help_text=_("Use the name of the Event model associated with these Calendar Event Kinds"))


    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class CalendarEvent(models.Model):
    kind = models.ForeignKey(CalendarEventKind, on_delete=models.CASCADE, related_name="event")
    name = models.CharField(max_length=200)
    icon = models.ImageField(blank=True, null=True)
    description = models.TextField(blank=True)
    custom_fields = GenericRelation(CustomField)
    created_by = models.ForeignKey(Organization, blank=True, null=True, on_delete=models.PROTECT)

    def __str__(self):
        return "{} ({})".format(self.name, self.kind)

    class Meta:
        ordering = ["name"]


class BaseEvent(ParseTimeMixin, models.Model):
    event_id = models.CharField(max_length=100)  # if the calendar creates this, store it here.
    event_type = models.ForeignKey(CalendarEvent, blank=True, null=True,
                                   on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    details = models.JSONField(default=dict)
    start = models.DateTimeField(blank=True, null=True)
    allDay = models.BooleanField(blank=True, default=False)
    end = models.DateTimeField(blank=True, null=True)
    editable = models.BooleanField(default=True)
    file = models.FileField(upload_to='files/', null=True, blank=True)

    class Meta:
        abstract = True
        ordering = ('-start',)

    def delete(self, *args, **kwargs):
        if self.file:
            self.file.delete()
        super().delete()

    @cached_property
    def title(self):
        return self.details.get("title")

    @cached_property
    def color(self):
        try:
            return self.event_type.kind.color
        except AttributeError:
            return None

    def save(self, *args, **kwargs):

        if not self.pk:
            self.pk = prefetch_id(instance=self)
            self.details['id'] = self.pk
            self.event_id = self.pk

        assert self.start is not None, "Start time needs to be provided."

        try:
            evt = self.details['event_type'][6:]
            self.event_type = CalendarEvent.objects.get(pk=int(evt))
        except KeyError:
            pass
        except CalendarEvent.DoesNotExist:
            pass

        return super(BaseEvent, self).save(*args, **kwargs)

    def get_custom_field_by_name(self, name):
        """
        Looks for name in custom fields
        :param name: String
        :return: None
        """
        for cf in self.details['custom_fields']:
            if cf['name'] == name:
                return cf
        return None

    def set_custom_field_value(self, name, value):
        """
        Finds custom field and sets value.
        Fails silently if the customfield is not found.
        :param name: String
        :param value: String
        :return: None
        """
        cf = self.get_custom_field_by_name(name)
        if cf:
            cf['value'] = value
            self.save()
        return None

