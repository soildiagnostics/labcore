from django.contrib.contenttypes.models import ContentType
from django.core.files import File
from django.core.files.base import ContentFile
from django.db import models
from django.utils.timezone import now

from clients.models import Field
from common.consumers import send_ws_message
from common.models import ParseTimeMixin
from customnotes.models import CustomField
from customnotes.serializers import CustomFieldSerializer
# Create your models here.
from fieldcalendar.event_base import CalendarEventKind, CalendarEvent, BaseEvent
from fieldcalendar.exif_helper import EXIFMixin
import re


class FieldEventManager(ParseTimeMixin, models.Manager):

    def get_events_in_window(self, field, start=None, end=None):
        events = FieldEvent.objects.filter(field=field)
        if start:
            events = events.filter(start__gte=self.parse_with_tz(start))
        if end:
            events = events.filter(end__lte=self.parse_with_tz(end))
        return events

    def create_protected_event(self, **kwargs):
        fe = FieldEvent.objects.create(
            field=kwargs['field'],
            details=kwargs['details'],
            editable=False,
            start=now()
        )
        return fe

    def create_system_event(self, field, title, start, end, allDay=True, editable=False):

        details = {
            "id": "",
            "end": end.isoformat(),
            "color": CalendarEventKind.objects.get(name="System Event").color,
            "start": start.isoformat(),
            "title": title,
            "allDay": allDay,
            "field_id": field.pk,
            "className": [],
            'editable': editable,
            "custom_fields": []}

        fe = FieldEvent.objects.create(
            field=field,
            start=start,
            end=end,
            editable=editable,
            allDay=allDay,
            event_id="To be replaced",
            details=details
        )

        return fe

    def filter_journal_entries(self, field):
        event_kind = CalendarEventKind.objects.get(name="System Event")
        event, _ = CalendarEvent.objects.get_or_create(name="Journal Entry", kind=event_kind)

        return FieldEvent.objects.filter(field=field, event_type=event)

    def create_journal_entry(self, field, title, entry, start, end, allDay=False, editable=False):
        fe = self.create_system_event(field, title, start, end, allDay, editable)
        event_kind = CalendarEventKind.objects.get(name="System Event")
        event, _ = CalendarEvent.objects.get_or_create(name="Journal Entry", kind=event_kind)

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                            object_id=event.id,
                                                            name="Entry",
                                                            help_text="Entry details for the Journal Entry")

        cf_sr = CustomFieldSerializer(custom_field).data
        cf_sr['value'] = entry

        fe.event_type = event
        fe.details['custom_fields'].append(cf_sr)
        fe.save()
        return fe

    def create_weather_entry(self, field, entry_details):

        evtkind, _ = CalendarEventKind.objects.get_or_create(name="Weather",
                                                             color="darkkhaki",
                                                             system_event=True)
        weather_event_type, _ = CalendarEvent.objects.get_or_create(name=entry_details['event_type'],
                                                                    kind=evtkind)

        start = entry_details.pop("start")
        details = {
            "id": "",
            "end": None,
            "color": evtkind.color,
            "start": start.isoformat(),
            "title": entry_details['title'],
            "allDay": True,
            "field_id": field.pk,
            "className": [],
            'editable': False,
            'event_type': f"event_{weather_event_type.pk}",
            "custom_fields": []}

        try:
            fe = FieldEvent.objects.get(
                field=field,
                start=start,
                end=None,
                editable=False,
                allDay=True,
                event_type=weather_event_type,
            )

            fe.details = details
        except FieldEvent.DoesNotExist:
            fe = FieldEvent.objects.create(
                field=field,
                start=start,
                end=None,
                editable=False,
                allDay=True,
                event_id="To be replaced",
                event_type=weather_event_type,
                details=details
            )

        contenttype = ContentType.objects.get_for_model(CalendarEvent)

        for key, value in entry_details['parameters'].items():
            custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                                object_id=weather_event_type.id,
                                                                name=key)
            cf_sr = CustomFieldSerializer(custom_field).data
            cf_sr['value'] = value
            fe.details['custom_fields'].append(cf_sr)

        fe.save()
        return fe

    def create_task_customfield(self, details):
        try:
            event_kind = CalendarEventKind.objects.get(name="Service Task")
        except CalendarEventKind.DoesNotExist:
            event_kind = CalendarEventKind.objects.create(name="Service Task",
                                                          color="darkslateblue",
                                                          system_event=False)

        event, _ = CalendarEvent.objects.get_or_create(name="To Do item", kind=event_kind)

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                            object_id=event.id,
                                                            name="Task",
                                                            help_text="Specify the task")

        cf_sr = CustomFieldSerializer(custom_field).data
        cf_sr['value'] = details.pop('task')

        details['event_type'] = f"event_{event.pk}"
        details['custom_fields'].append(cf_sr)
        return details

    def create_file_upload_event(self, field, title, start, end):

        fe = self.create_system_event(field, title, start, end, allDay=False, editable=False)
        fe.event_type, _ = CalendarEvent.objects.get_or_create(name="File Upload",
                                                               kind=CalendarEventKind.objects.get(name="System Event"))

        fe.save()
        return fe

    def create_shapefile_upload_event(self, layer, zipdir, zipfile):

        fe = FieldEvent.objects.create_file_upload_event(layer.field, layer.name, now(), now())
        file = open(f"{zipdir}/{zipfile}", "rb")
        upload_to = f"shapefiles/{layer.field.id}/{zipfile}"
        fe.file.save(upload_to, File(file))
        layer.source_file = fe.file.name
        layer.save()
        return fe

    def create_raster_upload_event(self, layer, rasterfile):
        fe = FieldEvent.objects.create_file_upload_event(layer.field, rasterfile.file.name, now(), now())
        return fe

    def create_photo_memo_customfields(self, details):
        try:
            event_kind = CalendarEventKind.objects.get(name="Field Scouting")
        except CalendarEventKind.DoesNotExist:
            event_kind = CalendarEventKind.objects.create(name="Field Scouting",
                                                          color="darkred",
                                                          system_event=False)

        event, _ = CalendarEvent.objects.get_or_create(name="Photo Upload", kind=event_kind)

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                            object_id=event.id,
                                                            name="Memo",
                                                            help_text="Details about this image")

        cf_sr = CustomFieldSerializer(custom_field).data
        cf_sr['value'] = details.pop('photo_memo')

        details['event_type'] = f"event_{event.pk}"
        details['custom_fields'].append(cf_sr)
        return details

    def create_geo_memo_customfields(self, details):
        try:
            event_kind = CalendarEventKind.objects.get(name="Field Scouting")
        except CalendarEventKind.DoesNotExist:
            event_kind = CalendarEventKind.objects.create(name="Field Scouting",
                                                          color="darkred",
                                                          system_event=False)

        event, _ = CalendarEvent.objects.get_or_create(name="Geolocated Memo", kind=event_kind)

        contenttype = ContentType.objects.get_for_model(CalendarEvent)
        custom_field, _ = CustomField.objects.get_or_create(content_type_id=contenttype.pk,
                                                            object_id=event.id,
                                                            name="Memo",
                                                            help_text="Details about this location")

        cf_sr = CustomFieldSerializer(custom_field).data
        cf_sr['value'] = details.pop('memo')

        details['event_type'] = f"event_{event.pk}"
        details['custom_fields'].append(cf_sr)
        return details

    def get_platmap_image(self, field):
        fe = self.filter(field=field, event_type__name="Platmap conversion")
        if fe.exists():
            return fe.latest("pk")
        return None

    def create_platmap_image(self, field, file):
        time = now()
        fe = self.create_system_event(field, "Platmap pdf conversion", time, time)
        event_kind = CalendarEventKind.objects.get(name="System Event")
        event, _ = CalendarEvent.objects.get_or_create(name="Platmap conversion", kind=event_kind)
        fe.event_type = event
        fe.file.save(f"{field.pk}/platfile.html", ContentFile(file.encode("utf-8")), save=True)
        return fe


class FieldEvent(EXIFMixin, BaseEvent):
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    objects = FieldEventManager()

    def __str__(self):
        return "{}, {}".format(self.field.name, self.modified)

    def save(self, *args, **kwargs):
        self.details['field_name'] = f"{self.field.name} ({self.field.farm.name})"
        # try:
        #     send_ws_message(self.field.farm.client.originator.username, f"Field event {self.id} created")
        # except Exception as e:
        #     print(e)
        if self.event_type:
            if self.event_type.name == "Photo Upload":
                self.process_event_details()
            elif self.title == "Dropped File" and self.file.name and re.search(r'jpg|png|jpeg', self.file.name):
                self.process_event_details()
        return super(FieldEvent, self).save(*args, **kwargs)

    def process_event_details(self):
        self.details['file_type'] = "image"
        try:
            ex = self.exif(self.file)
            coords = self.get_coordinates(ex["GPSInfo"])
            self.details["geotags"] = coords
        except (AttributeError, KeyError):
            pass
