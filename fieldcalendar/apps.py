from django.apps import AppConfig


class FieldcalendarConfig(AppConfig):
    name = 'fieldcalendar'
