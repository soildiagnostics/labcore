from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView
from django_datatables_view.base_datatable_view import BaseDatatableView

from associates.models import OrganizationUser
from associates.role_privileges import FilteredQuerySetMixin, DealershipRequiredMixin
from bugs.forms import BugCommentForm, BugForm
from bugs.models import Bug, BugComment, BugCategory
from common.searchquery import SearchQueryAssemblyMixin


# Create your views here.
class BugListView(DealershipRequiredMixin, CreateView):
    model = Bug
    template_name = 'bugs/buglist.html'
    success_url = reverse_lazy('bug_list')
    form_class = BugForm
    client_has_permission = True

    def form_valid(self, form):
        if form.cleaned_data['category'] not in ["", None]:
            print(form.cleaned_data['category'])
            cat = BugCategory.objects.get(category=form.cleaned_data['category'])
            assignee = OrganizationUser.objects.get(user=cat.assignee)
        else:
            cat = None
            assignee = OrganizationUser.objects.get(user__username=settings.BUG_REVIEWER)
        bug = Bug(state='0',
                  bug_name=form.cleaned_data['bug_name'],
                  bug_description=form.cleaned_data['bug_description'],
                  category=cat,
                  image=self.request.FILES.get('image'),
                  provided_by=self.request.user,
                  assigned_to=assignee)
        bug.save()
        messages.info(self.request,
                      "Your bug report has been filed and the system developer has been alerted")

        return HttpResponseRedirect(self.success_url)


class BugDetail(DealershipRequiredMixin, CreateView):
    model = BugComment
    template_name = 'bugs/bugdetail.html'
    success_url = reverse_lazy('bug_list')
    form_class = BugCommentForm
    client_has_permission = True

    def form_valid(self, form):
        bugcomment = BugComment(bug=Bug.objects.get(id=self.kwargs['pk']),
                                comment=form.cleaned_data['comment'],
                                image=self.request.FILES.get('image'),
                                provided_by=self.request.user)
        bugcomment.save()
        messages.info(self.request,
                      "Your comments have been sent to the developer")
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bug = Bug.objects.get(id=self.kwargs['pk'])
        context['bug'] = bug
        context['comments'] = bug.bugcomment_set.all()
        return context


class BugListJSON(FilteredQuerySetMixin, DealershipRequiredMixin,
                  SearchQueryAssemblyMixin, BaseDatatableView):
    model = Bug
    columns = ['pk',
               'category',
               'bug_name',
               'modified',
               'state',
               'bug_description']
    order_columns = ['pk',
                     'category',
                     'bug_name',
                     'modified',
                     'state',
                     '']
    max_display_length = 500
    instance_owner_organization = "submitted_by__organization"
    client_has_permission = True
    search_fields = ['pk',
                     'category__category',
                     'bug_name',
                     'state',
                     'bug_description']

    def get_initial_queryset(self):
        qs = Bug.objects.exclude(state__iexact='5')
        return qs

    def render_column(self, row, column):

        if column == "pk":
            link = "<a href='{}'>{}</a>".format(
                reverse("bug_detail", kwargs={'pk': row.pk}),
                row.pk
            )
            return link
        if column == 'bug_name':
            link = "<a href='{}'>{}</a>" \
                   "<span class='pull-right'>{} <i class='fa fa-comment'></i>".format(
                reverse("bug_detail", kwargs={'pk': row.pk}),
                row.bug_name, row.bugcomment_set.count()
            )
            return link
        elif column == 'state':
            spans = ['<span class ="label label-warning "> Submitted </span>',
                     '<span class ="label label-danger "> Confirmed </span>',
                     '<span class ="label label-info "> Assigned </span>',
                     '<span class ="label label-success "> Resolved </span>',
                     '<span class ="label label-success "> Fixed </span>',
                     '<span class ="label label-success "> Closed! </span>',
                     '<span class ="label label-success "> Feature Request </span>',
                     "<span class ='label label-success '> Won't Fix </span>"
                     ]
            return spans[int(row.state)]
        else:
            return super(BugListJSON, self).render_column(row, column)
