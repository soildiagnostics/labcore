from django.contrib import admin

from .models import *


# Register your models here.
@admin.register(BugCategory)
class BugCategoryAdmin(admin.ModelAdmin):
    autocomplete_fields = ('assignee',)

class BugCommentAdmin(admin.ModelAdmin):
    list_display = ('submitted', 'provided_by', 'bug', 'comment')

class BugCommentInLine(admin.TabularInline):
    model = BugComment
    autocomplete_fields = ('provided_by',)
    extra = 1
    exclude = ('submitted_by',)

class BugAdmin(admin.ModelAdmin):
    list_display = ('bug_name', 'category', 'state', 'modified', 'bug_description')
    date_heirarchy = 'modified'
    list_filter = ('state',)
    raw_id_fields = ('provided_by',)
    autocomplete_fields = ('assigned_to', )
    actions = ['close_bugs']
    list_editable = ['state']
    filter_horizontal = ('related_bugs',)
    exclude = ('submitted_by', )

    inlines = [
        BugCommentInLine,
    ]

    def close_bugs(self, request, queryset):
        queryset.update(state='5')
    close_bugs.short_description = "Close selected bugs"

admin.site.register(Bug, BugAdmin)
#admin.site.register(BugComment, BugCommentAdmin)