from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from contact.models import User

from associates.models import OrganizationUser


# Create your models here.

class BugCategory(models.Model):
    category = models.CharField(max_length=32, default="Software issue")
    assignee = models.ForeignKey(User, blank=True, null=True, on_delete=models.PROTECT)
    def __str__(self):
        return self.category

class Bug(models.Model):

    STATE_CHOICE = (
        ('0', 'Submitted'),
        ('1', 'Confirmed'),
        ('2', 'Assigned'),
        ('3', 'Resolved'),
        ('4', 'Fixed'),
        ('5', 'Closed'),
        ('6', 'Feature Request'),
        ('7', "Won't Fix!")
    )

    submitted = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    state = models.CharField(max_length=1, choices=STATE_CHOICE)
    category = models.ForeignKey(BugCategory, blank=True, null=True, on_delete=models.PROTECT,
                                 help_text="Help us assign it to the right person quickly")

    bug_name = models.CharField(max_length=200,
                                help_text="A short name to identify it in the table below")
    bug_description = models.TextField(help_text="Please provide details")
    image = models.FileField(upload_to="bug_images/", blank=True, null=True, help_text=_("Attach a screen shot"))
    related_bugs = models.ManyToManyField("Bug", blank=True)

    submitted_by = models.ForeignKey(
        OrganizationUser, related_name='submitted_by', on_delete=models.CASCADE, blank=True, null=True)
    assigned_to = models.ForeignKey(
        OrganizationUser, related_name='assigned_to', on_delete=models.CASCADE)
    provided_by = models.ForeignKey(User,
                                    blank=True, null=True,
                                    related_name="bugs", on_delete=models.PROTECT)

    def __str__(self):
        return "(%s) %s" % (self.state, self.bug_name)

    class Meta:
        ordering = ('-modified',)


class BugComment(models.Model):

    bug = models.ForeignKey(Bug, on_delete=models.CASCADE)
    comment = models.TextField()
    image = models.FileField(upload_to="bug_images/", blank=True, null=True, help_text=_("Attach a screen shot"))
    submitted = models.DateTimeField(auto_now_add=True)
    submitted_by = models.ForeignKey(OrganizationUser, on_delete=models.CASCADE, blank=True, null=True)
    provided_by = models.ForeignKey(User,
                                    blank=True, null=True,
                                    related_name="bug_comments", on_delete=models.PROTECT)

    def __str__(self):
        return self.comment[:60]

    class Meta:
        ordering = ('-submitted',)


@receiver(post_save, sender=Bug)
def send_bug_email(sender, instance, *args, **kwargs):
    try:
        protocol = settings.SITE_PROTOCOL
    except AttributeError:
        protocol = 'http'

    msg_plain = render_to_string('bugs/bug_mail.txt', {'bug': instance,
                                                       'site': Site.objects.get_current(),
                                                       'protocol': protocol })
    send_mail(
        subject='LabCore Bug: ' + str(instance.id),
        message=msg_plain,
        from_email="bugs@soildiagnostics.com",
        recipient_list=[instance.assigned_to.user.email,
                        instance.provided_by.email],
    )


@receiver(post_save, sender=BugComment)
def send_bug_comment_email(sender, instance, *args, **kwargs):
    try:
        protocol = settings.SITE_PROTOCOL
    except AttributeError:
        protocol = 'http'

    email_list = [instance.bug.assigned_to.user.email,
                  instance.bug.provided_by.email]
    all_comments = BugComment.objects.filter(bug=instance.bug)
    comment_emails = [c.provided_by.email for c in all_comments]
    email_list.extend(comment_emails)
    msg_plain = render_to_string('bugs/bug_mail.txt', {'bug': instance.bug,
                                                       'comments': all_comments,
                                                       'site': Site.objects.get_current(),
                                                       'protocol': protocol})
    send_mail(
        subject='New Comment on LabCore Bug: ' + str(instance.bug.id),
        message=msg_plain,
        from_email="bugs@soildiagnostics.com",
        recipient_list=list(set(email_list)),
    )
