from crispy_forms.layout import Layout, Div
from django import forms

from clients.forms import CommonHelper
from .models import *


class BugForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BugForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_id = 'id-newBugForm'
        self.helper.form_class = "uniForm"
        self.helper.field_template = "bootstrap3/field.html"
        cat, _ = BugCategory.objects.get_or_create(category="Software issue")
        self.initial['category'] = cat
        self.helper.layout = Layout(
            Div('bug_name', 'bug_description', 'category', 'image', css_class="form-group")
        )

    class Meta:
        model = Bug
        fields = ['bug_name', 'bug_description', 'category', 'image']


class BugCommentForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BugCommentForm, self).__init__(*args, **kwargs)
        self.helper = CommonHelper(self)
        self.helper.form_id = 'id-newBugCommentForm'
        self.helper.form_class = "uniForm"
        self.helper.field_template = "bootstrap3/field.html"
        self.helper.layout = Layout(
            Div('comment', 'image',  css_class="form-group")
        )
    class Meta:
        model = BugComment
        fields = ['comment', 'image']
