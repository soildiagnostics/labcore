from django.urls import path

from bugs import views

urlpatterns = [
    path('', views.BugListView.as_view(), name="bug_list"),
    path('<int:pk>/', views.BugDetail.as_view(), name="bug_detail"),
    path('list/', views.BugListJSON.as_view(), name="bug_list_json")
]