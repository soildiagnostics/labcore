from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse

from associates.models import Organization
from bugs.models import *
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Person


# Create your tests here.


class BugTests(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        org = Organization.objects.get(company__name__istartswith="Sky")
        u = User.objects.create_user(username=settings.BUG_REVIEWER, password="buckeyes")
        OrganizationUser.objects.create(organization=org, user=u)
        self.tester = User.objects.get(username="testinguser")
        self.client = Client()
        self.client.force_login(self.tester)

    def test_review_user(self):
        ouser = OrganizationUser.objects.get(user__username=settings.BUG_REVIEWER)
        self.assertNotEqual(ouser.id, None)

    def test_view_list_not_logged_in(self):
        response = Client().get(reverse("bug_list"))
        self.assertEqual(response.status_code, 302)

    def test_view_list_regular_user(self):
        response = self.client.get(reverse("bug_list"))
        self.assertEqual(response.status_code, 200)

    def test_create_bug(self):

        dat = {"bug_name": "Test bug",
               "bug_description": "Test bug description"}
        response = self.client.post(reverse("bug_list"), dat)
        self.assertEqual(response.status_code, 302)
        bug = Bug.objects.get(bug_name="Test bug")
        response = self.client.get(reverse("bug_detail", kwargs={"pk": bug.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(bug.__str__(), "({}) {}".format(bug.state, bug.bug_name))

    def test_create_bug_by_client(self):
        clt = ClientsTestCase.createClient(self)
        clt_person = Person.objects.get(id=clt.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)
        dat = {"bug_name": "Test bug",
               "bug_description": "Test bug description"}
        response = self.client.post(reverse("bug_list"), dat)
        self.assertEqual(response.status_code, 302)
        bug = Bug.objects.get(bug_name="Test bug")
        response = self.client.get(reverse("bug_detail", kwargs={"pk": bug.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(bug.__str__(), "({}) {}".format(bug.state, bug.bug_name))

    def test_create_invalid_bug(self):
        dat = {"bug_name": "Test bug"}
        response = self.client.post(reverse("bug_list"), dat)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["form_show_errors"], True)

    def test_bug_comment_post(self):
        image = SimpleUploadedFile("file.png", b"file_content", content_type="image/png")
        dat = {"bug_name": "Test bug",
               "bug_description": "Test bug description",
               "image": image}
        response = self.client.post(reverse("bug_list"), dat)
        self.assertEqual(response.status_code, 302)
        bug = Bug.objects.get(bug_name="Test bug")
        self.assertIsNotNone(bug.image)


        response = self.client.get(reverse("bug_detail", kwargs={"pk": bug.id}))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("bug_detail", kwargs={"pk": bug.id}),
                          data={"comment": "test comment"})
        self.assertEqual(response.status_code, 302)
        comments = BugComment.objects.filter(bug=bug)
        self.assertEqual(len(comments), 1)
        self.assertNotEqual(comments[0].__str__(), None)

    def test_bug_comment_post_by_client(self):
        clt = ClientsTestCase.createClient(self)
        clt_person = Person.objects.get(id=clt.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)

        image = SimpleUploadedFile("file.png", b"file_content", content_type="image/png")
        dat = {"bug_name": "Test bug",
               "bug_description": "Test bug description",
               "image": image}
        response = self.client.post(reverse("bug_list"), dat)
        self.assertEqual(response.status_code, 302)
        bug = Bug.objects.get(bug_name="Test bug")
        self.assertIsNotNone(bug.image)


        response = self.client.get(reverse("bug_detail", kwargs={"pk": bug.id}))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("bug_detail", kwargs={"pk": bug.id}),
                          data={"comment": "test comment"})
        self.assertEqual(response.status_code, 302)
        comments = BugComment.objects.filter(bug=bug)
        self.assertEqual(len(comments), 1)
        self.assertNotEqual(comments[0].__str__(), None)

