from django.conf import settings
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin, AccessMixin
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.conf import settings
from associates.models import OrganizationUser, Role, Organization
from common.models import RefreshFromDbInvalidatesCachedPropertiesMixin


def debugPrint(txt):
    if settings.DEBUG:
        print(txt)


class DealershipRequiredMixin(UserPassesTestMixin):
    '''
    Allows every class to have access to the org and orguser properties,
    once the user is logged in
    '''

    is_client = False

    def dispatch(self, request, *args, **kwargs):
        try:
            api_call = 'api' in self.request.path and 'Token' in self.request.headers.get('Authorization')
        except TypeError:
            api_call = False
        if not request.user.is_authenticated and not api_call:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    @property
    def orguser(self) -> OrganizationUser:
        """Org user is given preference. If a dealer is also a client, this
        should still say is_client is false.. need to write a test for this"""
        try:
            return OrganizationUser.objects.select_related("organization", "user").get(
                user=self.request.user,
                organization__roles__role__in=[
                    "Dealer",
                    "Sampler"])
        except OrganizationUser.DoesNotExist:
            ## Just return the user - this is probably a client.
            self.is_client = hasattr(self.request.user.person, 'client') or hasattr(self.request.user.person.company,
                                                                                    'client')
            debugPrint(f"{self.request}::{self.request.user} is client: {self.is_client}")
            return self.request.user
        except OrganizationUser.MultipleObjectsReturned:
            debugPrint(f"Multiple users: {OrganizationUser.objects.filter(user=self.request.user)}, returning first")
            return OrganizationUser.objects.select_related("organization", "user").filter(
                user=self.request.user,
                organization__roles__role__in=[
                    "Dealer",
                    "Sampler"]).first()

    @property
    def org(self) -> Organization:
        try:
            return self.orguser.organization
        except AttributeError:
            if hasattr(self.request.user.person, 'client'):
                org = self.request.user.person.client.originator.organization
            elif hasattr(self.request.user.person.company, 'client'):
                org = self.request.user.person.company.client.originator.organization
            debugPrint(f"org: {org}")
            return org

    def test_func(self) -> bool:
        if self.request.user.is_anonymous:
            if ('api' in self.request.path and 'Token' in self.request.headers.get('Authorization')):
                """
                Allow the DRF framework to authenticate the API user. 
                """
                return True
        try:
            result = self.orguser.organization.is_dealer or \
                     self.orguser.organization.is_sampler or \
                     self.orguser.organization.is_primary
        except AttributeError:
            debugPrint("AttributeError in test_func")
            perm = getattr(self, 'client_has_permission', False)
            debugPrint(f"client: {self.is_client}, perm: {perm}")
            result = self.is_client and perm
        debugPrint(f"test_func: {result}")
        return result


class FilteredQuerySetMixin():
    '''
    overrides the get_object method in a generic view;
    use the class attribute `my_owner' to determine
    which object / queryset needs to be tested for ownership. Requires to be
    inherited after the DealershipMixin above
    '''

    def get_initial_queryset(self):
        kwargs = dict()
        if self.request.user.is_staff and hasattr(super(self.__class__), 'get_initial_queryset'):
            return super().get_initial_queryset()
        if (self.request.user.is_staff and self.org.is_primary) or (self.request.user.is_superuser):
            return self.model.objects.all()
        if self.org.is_dealer:
            kwargs = {
                "{}".format(self.instance_owner_organization): self.org,
            }
        if self.is_client:
            ## In this case self.org.is_dealer is true, since client's org is the
            ## dealer's org. So we overwrite the permissions.
            if hasattr(self.request.user.person, 'client'):
                client = self.request.user.person.client
            else:
                client = self.request.user.person.company.client
            try:
                if self.model == type(client):
                    kwargs = {
                        "id": client.id
                    }
                else:
                    kwargs = {
                        "{}".format(self.instance_owner_client): client,
                    }
            except Exception as e:
                raise PermissionDenied(f"Exception in Role Privileges, Block FilteredQueryMixin: {repr(e)}")

        qlist = [Q(**{k: v}) for k, v in kwargs.items()]

        query = Q()
        for q in qlist:
            query |= q

        return self.model.objects.filter(query)


class NestedAttributeMixin():

    def multi_getattr(self, obj, attr, **kw):
        attributes = attr.split("__")
        for i in attributes:
            if i != '':
                try:
                    obj = getattr(obj, i)
                except AttributeError as e:
                    debugPrint(f"AttributeError in Role Privileges, block NestedAttributeMixin: {repr(e)}")
                    raise
        return obj


class FilteredObjectMixin(NestedAttributeMixin):

    # make sure all inherited classes set this property
    @staticmethod
    def instance_owner_organization():
        raise AttributeError

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if self.request.user.is_staff:
            # Staff user always gets the object
            return obj

        if hasattr(self, 'hallpass_field'):
            # The hall pass overrides everything else
            related_field = self.multi_getattr(obj, self.hallpass_field)
            if hasattr(related_field, 'hallpass') and related_field.hallpass.filter(user=self.request.user,
                                                                                    expiry__gte=now()).exists():
                ## This should cover views of objects based on models related to Field.
                return obj
        try:
            if self.is_client:
                # Check if the client permissions are good.
                if hasattr(self.request.user.person, 'client'):
                    client = self.request.user.person.client
                else:
                    client = self.request.user.person.company.client

                order_client = self.multi_getattr(obj, self.instance_owner_client)
                draftorder_client = None
                if hasattr(obj, 'draftorder'):
                    if hasattr(obj.draftorder.user.person, 'client'):
                        draftorder_client = obj.draftorder.user.person.client
                    elif hasattr(obj.draftorder.user.person.company, 'client'):
                        draftorder_client = obj.draftorder.user.person.company.client
                assert client in [order_client, draftorder_client], \
                    f"""AssertFail, Object {obj}, requesting client is {client} which is not in
                    {[order_client, draftorder_client]}"""
                debugPrint("instance owner client is correct")
                return obj
            ## This is not a client - may be a dealer or sampler then?
            assert (self.multi_getattr(obj, self.instance_owner_organization) == self.org), \
                f"{self.instance_owner_organization} != {self.org} for obj: {obj}"
            debugPrint("instance owner organization is correct")
            # Also check that the client can access when he's a client of the right organization
            return obj
        except AssertionError as e:
            # Carve out for the sampler for orders.
            debugPrint(repr(e))
            try:
                ## Maybe this is an order and its sampler org is correct
                if hasattr(obj, 'sampler'):
                    assert obj.sampler.organization == self.org, "Sampler organization != object organization"
                    return obj
                raise PermissionDenied(
                    f"Exception in Role Privileges, Block FilteredObjectMixin: Not a client or sampler")
            except (AssertionError, AttributeError) as e:
                debugPrint(f"Exception in Role Privileges, Block FilteredObjectMixin: {repr(e)}")
                raise PermissionDenied(f"Exception in Role Privileges, Block FilteredObjectMixin: {repr(e)}")
        except AttributeError as e:
            ## this occurs if an org user is trying to change their profile
            if str(e) == "Person has no client.":
                try:
                    assert self.org.is_dealer and \
                           (self.request.user == obj.user or obj.company.client.originator.organization == self.org), \
                        "Assertion error when Client record is missing from Person"
                    return obj
                except Exception as e:
                    raise PermissionDenied(f"AttributeError in Role Privileges, Block FilteredObjectMixin: {repr(e)}")
