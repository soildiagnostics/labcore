import organizations as org_module
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from organizations.admin import BaseOrganizationUserAdmin

from clients.models import Client
from .forms import OrganizationUserForm, AssociatedOrganizationForm, OrgUserFormSet
from .models import Role, Organization, OrganizationUser

admin.site.unregister(org_module.models.Organization)
admin.site.unregister(org_module.models.OrganizationUser)
admin.site.unregister(org_module.models.OrganizationOwner)

admin.site.register(Role)


class ClientInline(admin.TabularInline):
    model = Client
    extra = 0
    show_change_link = True
    view_on_site = True
    list_display = ('name', 'company',)

    fields = ('name', 'company', 'is_company', 'is_user',)
    readonly_fields = ('name', 'company', 'user', 'is_company', 'is_user',)


class OrgUserInline(admin.StackedInline):
    model = OrganizationUser
    extra = 0
    show_change_link = True
    view_on_site = False
    form = OrganizationUserForm
    # add_form = OrganizationUserAddForm
    formset = OrgUserFormSet
    ordering = ('-user__is_active', 'user__username')
    # inlines = [ClientInline]
    #
    fields = (('username'), ('first_name', 'last_name'), 'email', ('is_admin', 'is_primary'),
              ('password1', 'password2'), ('last_login'),)
    readonly_fields = ('get_full_name_and_user', 'get_clients_link', 'full_name', 'last_login',)

    def response_change(self, request, obj=None, **kwargs):
        if not '_continue' in request.POST:
            return HttpResponseRedirect(reverse("admin:associates_organization_change", args=(obj.organization.id,)))
        else:
            return super(OrgUserInline, self).response_change(request, obj, **kwargs)


@admin.register(Organization)
class AssociatedOrganizationAdmin(admin.ModelAdmin):
    search_fields = ("company__name",)
    list_filter = ('roles', 'is_active')
    list_display = ('company', 'is_active', 'get_roles', 'member_count', 'client_count')
    # autocomplete_search_fields = {
    #     "name": ['name'],
    # }
    autocomplete_fields = ('company',)
    # prepopulated_fields = {'slug': ('name',), }
    inlines = [
        OrgUserInline,
    ]
    list_editable = ('is_active',)
    filter_horizontal = ('roles',)
    fields = ('company', 'slug', 'roles', 'is_active',)
    actions = ['add_dealership',
               'remove_dealership',
               'move_client_to_primary_and_deactivate']

    form = AssociatedOrganizationForm

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("organization_users__clients", "roles").select_related("company")

    def get_roles(self, obj):
        return ', '.join([o.role for o in obj.roles.all()])

    get_roles.short_description = 'Organization Roles'

    def get_form(self, request, *args, **kwargs):
        form = super(AssociatedOrganizationAdmin, self).get_form(request, *args, **kwargs)
        if request.user.is_authenticated:
            form.current_user = request.user
        else:
            form.current_user = None
        form.admin_site = self.admin_site
        return form

    def save_related(self, request, form, formsets, change):

        super(AssociatedOrganizationAdmin, self).save_related(request, form, formsets, change)
        try:
            org = formsets[0].cleaned_data[0]['organization']
            if not change:
                for inlineform in formsets:
                    if inlineform.cleaned_data[0]['is_primary'] and not org.get_owner:
                        org.set_organization_owner(inlineform.cleaned_data[0]['username'])
            if change:
                for inlineform in formsets[0].cleaned_data:
                    if inlineform['is_primary']:
                        org.set_organization_owner(inlineform['username'])
        except Exception:
            pass

    def add_role_action_helper(self, queryset, role):
        for o in queryset:
            o.roles.add(role)

    def remove_role_action_helper(self, queryset, role):
        for o in queryset:
            o.roles.remove(role)

    def add_dealership(self, request, queryset):
        self.add_role_action_helper(queryset, Role.objects.get(role="Dealer"))

    add_dealership.short_description = "Add Dealership Role"

    def remove_dealership(self, request, queryset):
        self.remove_role_action_helper(queryset, Role.objects.get(role="Dealer"))

    remove_dealership.short_description = "Remove Dealership Role"

    def move_client_to_primary_and_deactivate(self, request, queryset):
        me = OrganizationUser.objects.get(user=request.user)
        for ao in queryset:
            try:
                assert not ao.is_primary, "Can't move your clients to yourself!"
                for member in ao.organization_users.all():
                    member.clients.update(originator=me)
                messages.info(request, f"Transferred clients of {ao} successfully")
                ao.is_active = False
                ao.save()
            except Exception as e:
                print(e)
                messages.error(request, f"FAILED transfer for {ao} with error: {e}")

    move_client_to_primary_and_deactivate.short_description = "Move clients to logged in user and deactivate AO"


@admin.register(OrganizationUser)
class AssociatedOrganizationUserAdmin(BaseOrganizationUserAdmin):
    form = OrganizationUserForm
    list_display = ('get_person', 'organization')
    search_fields = ('user__username', 'user__first_name', 'user__last_name')
    list_filter = ('organization',)
    autocomplete_fields = ('organization',)

    def get_person(self, instance):
        return '{} {}'.format(instance.user.first_name, instance.user.last_name)

    get_person.short_description = 'Name'
    get_person.admin_order_field = 'user__last_name'

    def response_change(self, request, obj):
        if not '_continue' in request.POST:
            return HttpResponseRedirect(reverse("admin:associates_organization_change", args=(obj.organization.id,)))
        else:
            return super(AssociatedOrganizationUserAdmin, self).response_change(request, obj)
