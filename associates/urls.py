from django.urls import path

from common.apihelper import apipath
from .apiviews import APIOrganizationList, APINewOrganization, APIEditOrganization, APINewOrganizationUser, \
    APIEditOrganizationUser
from .views import TeamMembers, ChangePermissions

urlpatterns = [
    path('', TeamMembers.as_view(), name="team_members"),
    path('change/<str:par>/<int:pk>/', ChangePermissions.as_view(), name="change_permissions")
]

## API urls

urlpatterns += [
    apipath('list/', APIOrganizationList.as_view(), name="api_associate_organizations"),
    apipath('create/', APINewOrganization.as_view(), name="api_new_associate_organization"),
    apipath('edit/<int:pk>/', APIEditOrganization.as_view(), name="api_edit_associate_organization"),
    apipath('user/create/', APINewOrganizationUser.as_view(), name="api_new_associate_organization_user"),
    apipath('user/edit/<int:pk>/', APIEditOrganizationUser.as_view(), name="api_edit_associate_organization_user"),
]