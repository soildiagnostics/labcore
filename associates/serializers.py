from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from contact.models import User
from contact.serializers import CompanySerializer, UserCreateSerializer, UserUpdateSerializer, CompanyCreateSerializer
from .models import Organization, Role, OrganizationUser


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ('role', 'description')


class OrganizationSerializer(serializers.ModelSerializer):
    roles = RoleSerializer(many=True, read_only=True)
    company = CompanySerializer(read_only=True)
    class Meta:
        model = Organization
        fields = ('roles', 'name', 'created', 'modified', 'is_active', 'company')


class OrganizationCreateSerializer(serializers.ModelSerializer):
    roles = serializers.ListField(child=serializers.CharField())
    company = CompanyCreateSerializer()

    class Meta:
        model = Organization
        fields = ('roles', 'company')

    def create(self, validated_data):
        roles = validated_data.pop('roles')
        serializer = CompanyCreateSerializer(data=validated_data['company'])
        if serializer.is_valid():
            new_company = serializer.save()
            new_org = Organization.objects.create(company=new_company)
            if roles:
                for role in roles:
                    new_org.roles.add(Role.objects.get(role=role))
            new_org.refresh_from_db()
            return new_org

        else:
            raise ValidationError(detail=serializer.errors)


class OrganizationUpdateSerializer(serializers.ModelSerializer):
    add_roles = serializers.ListField(child=serializers.CharField(), required=False)
    remove_roles = serializers.ListField(child=serializers.CharField(), required=False)
    company = CompanyCreateSerializer(required=False)

    class Meta:
        model = Organization
        fields = ('add_roles', 'remove_roles', 'company')

    def update(self, instance, validated_data):
        org = instance

        existing_roles = [obj.role for obj in org.roles.all()]
        add_roles = validated_data.get('add_roles')
        if add_roles:
            for role in add_roles:
                if role not in existing_roles:
                    org.roles.add(Role.objects.get(role=role))

        org.refresh_from_db()

        existing_roles = [obj.role for obj in org.roles.all()]
        remove_roles = validated_data.get('remove_roles')
        if remove_roles:
            for role in remove_roles:
                if role in existing_roles:
                    org.roles.remove(Role.objects.get(role=role))

        if validated_data.get('company'):
            company = org.company
            serializer = CompanyCreateSerializer(instance=company, data=self.get_initial().get('company'))
            if serializer.is_valid():
                serializer.save()

            else:
                raise ValidationError(detail=serializer.errors)

        org.refresh_from_db()
        return org


class OrganizationUserCreateSerializer(serializers.ModelSerializer):
    is_admin = serializers.BooleanField(required=True)
    is_owner = serializers.BooleanField(required=False)
    user = UserCreateSerializer(many=False)

    class Meta:
        model = OrganizationUser
        fields = ('is_admin', 'is_owner', 'user', 'organization')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        org = Organization.objects.get(pk=validated_data['organization'].pk)
        org_user = OrganizationUser.objects.create(is_admin=validated_data['is_admin'], user=user,
                                                   organization=org)
        user.person.company = org_user.organization.company
        user.person.is_company_contact = True
        user.person.save()
        if validated_data.get('is_owner'):
            org_owner = org.get_owner
            if org_owner:
                deleted, _ = org_owner.delete()
                assert deleted == 1, "Organization Owner not deleted"
            org.set_organization_owner(org_user.user)
        org_user.refresh_from_db()
        return org_user


class OrganizationUserUpdateSerializer(serializers.ModelSerializer):
    is_admin = serializers.BooleanField(required=False)
    is_owner = serializers.BooleanField(required=False)
    user = UserUpdateSerializer(many=False, required=False)
    organization = serializers.PrimaryKeyRelatedField(queryset=Organization.objects.all(), required=False)

    def validate_is_owner(self, value):
        if not value:
            raise ValidationError("This optional field accepts only True value.")
        return value

    class Meta:
        model = OrganizationUser
        fields = ('is_admin', 'is_owner', 'user', 'organization')

    def update(self, instance, validated_data):
        org_user = instance
        user = org_user.user
        if validated_data.get('user'):
            user_data = validated_data.pop('user')

            if user_data.get('password'):
                password = user_data.pop('password')
                user.set_password(password)

            User.objects.filter(id=user.id).update(**user_data)
            u = User.objects.get(id=user.id)
            u.update_person()
            org_user.refresh_from_db()

        if validated_data.get('is_owner'):
            validated_data.pop('is_owner')
            org = org_user.organization
            org_owner = org.get_owner
            if org_owner:
                deleted, _ = org_owner.delete()
                assert deleted == 1, "Organization Owner not deleted"
            org.set_organization_owner(org_user.user)
        OrganizationUser.objects.filter(id=org_user.id).update(**validated_data)
        org_user.refresh_from_db()
        return org_user
