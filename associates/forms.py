from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.translation import gettext as _

from associates.models import OrganizationUser, Organization
from contact.models import User


def add_related_field_wrapper(form, col_name):
    rel_model = form.Meta.model
    remote_field = rel_model._meta.get_field(col_name)
    rel = remote_field.remote_field
    if not form.fields.get(col_name):
        form.fields.update({col_name: forms.ModelChoiceField(queryset=remote_field.model.objects.all())})
    form.fields[col_name].widget = RelatedFieldWidgetWrapper(form.fields[col_name].widget,
                                                             rel,
                                                             admin.site,
                                                             can_add_related=True,
                                                             can_change_related=True)


class OrganizationUserForm(forms.ModelForm):
    """
    Form class for editing OrganizationUsers *and* the linked user model.
    """
    username = forms.CharField(max_length=50, required=True)
    first_name = forms.CharField(max_length=100, required=True)
    last_name = forms.CharField(max_length=100)
    email = forms.EmailField(required=True)
    password1 = forms.CharField(label='Password',
                                widget=forms.PasswordInput,
                                required=False,
                                help_text='Input is masked.')
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput, required=False)
    is_primary = forms.BooleanField(required=False, help_text='Primary contact person for this organization.',
                                    )
    get_clients_link = forms.CharField(required=False)

    class Meta:
        exclude = ('user',)
        model = OrganizationUser

    def __init__(self, *args, **kwargs):
        # self.parent_object = parent_object
        super(OrganizationUserForm, self).__init__(*args, **kwargs)
        # add_related_field_wrapper(self, 'organization')
        if self.instance.pk is not None:
            self.fields['first_name'].initial = self.instance.user.first_name
            self.fields['last_name'].initial = self.instance.user.last_name
            self.fields['email'].initial = self.instance.user.email
            self.fields['username'].initial = self.instance.user.username
            self.fields['is_primary'].initial = self.instance.is_owner
            self.fields['is_admin'].initial = self.instance.is_admin

            # make the User (not org user) change link available
            self.fields['username'].widget.attrs['readonly'] = True
            self.fields['username'].disabled = True
            change_link = "/admin/contact/user/{}/".format(self.instance.user.id)
            html = mark_safe(
                'Only the user should edit username after creation.  Go to the User page or <a href="{}">click here<a/> to make changes.'.format(
                    change_link))
            self.fields['username'].help_text = html

            self.fields['password1'].widget = forms.HiddenInput()
            self.fields['password2'].widget = forms.HiddenInput()

    def save(self, *args, **kwargs):
        """
        This method saves changes to the linked user model.
        """
        if self.instance.pk is None:
            try:
                User.objects.get(username=self.cleaned_data['username'])
            except User.DoesNotExist:
                u = User.objects.create_user(
                    username=self.cleaned_data['username'],
                    email=self.cleaned_data['email'],
                )
                if self.cleaned_data['password1']:
                    u.set_password(self.cleaned_data['password1'])
                else:
                    u.set_unusable_password()
                u.first_name = self.cleaned_data['first_name']
                u.last_name = self.cleaned_data['last_name']
                try:
                    u.save()
                except Exception as e:
                    raise ValidationError(_(f'There was problem with the user save: {repr(e)}'))

                # save the new person details
                u.update_person(company=self.cleaned_data['organization'].company)

            self.cleaned_data['user'] = u
            self.instance.user = u

        else:
            user = User.objects.get(associates_organizationuser=self.instance)
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.email = self.cleaned_data['email']
            if self.cleaned_data['password1']:
                user.set_password(self.cleaned_data['password1'])
            user.save()
            user.update_person()

            # handle change owner
            if self.cleaned_data['is_primary'] and not self.instance.organization.get_owner:
                self.instance.organization.set_organization_owner(self.instance.user)
            if self.cleaned_data['is_primary'] and self.instance.organization.get_owner != self.instance:
                self.instance.organization.change_owner(self.instance)
        return super(OrganizationUserForm, self).save(*args, **kwargs)


# class OrganizationUserAddForm(OrganizationUserForm):
#
#     def __init__(self, *args, **kwargs):
#         # self.parent_object = parent_object
#         super(OrganizationUserForm, self).__init__(*args, **kwargs)
#         # add_related_field_wrapper(self, 'organization')
#         if self.instance.pk is not None:
#             change_link = "/admin/contact/user/{}/".format(self.instance.user.id)
#             html = mark_safe(
#                 'Go to the User page or <a href="{}">click here<a/> to change the password'.format(change_link))
#             self.fields['password1'].help_text = html


class OrgUserFormSet(BaseInlineFormSet):
    '''
    Validate formset data here
    '''

    def clean(self):
        super(OrgUserFormSet, self).clean()

        has_owner = False
        has_admin = False
        for form in self.forms:
            if not hasattr(form, 'cleaned_data'):
                continue
            data = form.cleaned_data
            is_owner = data.get('is_primary', False)
            if is_owner and not has_owner:
                has_owner = True
            elif is_owner and has_owner:
                raise ValidationError(_('There can only be one Primary contact.  Please select only one.'))

            is_admin = data.get('is_admin', False)
            if is_admin and not has_admin:
                has_admin = True

            if form.cleaned_data.get('id') is None:
                if form.cleaned_data['username'] == '':
                    raise ValidationError(_('A username is required.'))
                if User.objects.filter(username=form.cleaned_data['username']).exists():
                    raise ValidationError(_('This username is already used, please select another.'))
                if form.cleaned_data['password1'] != form.cleaned_data['password2']:
                    raise ValidationError(_('The passwords do not match.'))

        if not has_owner:
            raise ValidationError(_('There must be a PRIMARY contact for the organization.  Please select one.'))
        if not has_admin:
            raise ValidationError(_('There must be an ADMIN for the organization.  Please select one or more.'))


class AssociatedOrganizationForm(forms.ModelForm):
    slug = forms.SlugField(required=False)

    class Meta:
        fields = '__all__'
        model = Organization

    def save(self, *args, **kwargs):
        if not self.instance.slug:
            self.cleaned_data['slug'] = slugify(self.instance.name)
        return super(AssociatedOrganizationForm, self).save(*args, **kwargs)


class OrgUserInvitationForm(forms.Form):
    email = forms.EmailField(label="Email")
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-inline'
    helper.field_template = 'bootstrap3/layout/inline_field.html'
    helper.layout = Layout(
        'email',
        Submit('submit', 'Invite', css_class='btn-primary')
    )


class AssocUserRegistrationForm(forms.ModelForm):
    """
    Form class for completing a user's registration and activating the
    User.
    The class operates on a user model which is assumed to have the required
    fields of a BaseUserModel
    """

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    # helper.field_template = 'bootstrap3/layout/inline_field.html'
    helper.layout = Layout(
        'username',
        'password',
        'password_confirm',
        'first_name',
        'last_name',
        'email',
        Submit('submit', 'Register', css_class='btn-primary')
    )

    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-10'
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    password = forms.CharField(max_length=30, widget=forms.PasswordInput)
    password_confirm = forms.CharField(max_length=30,
                                       widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(AssocUserRegistrationForm, self).__init__(*args, **kwargs)
        self.initial['username'] = ''

    def clean(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")
        if password != password_confirm or not password:
            raise forms.ValidationError(_("Your password entries must match"))
        return super(AssocUserRegistrationForm, self).clean()

    class Meta:
        model = get_user_model()
        exclude = ('is_staff', 'is_superuser', 'is_active', 'last_login',
                   'date_joined', 'groups', 'user_permissions')
