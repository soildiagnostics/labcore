from dbtemplates.models import Template
from django.contrib.sites.models import Site
from django.core import mail
from django.http import Http404
from django.test import TestCase
from django.test.client import RequestFactory, Client
from django.utils.translation import gettext as _
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from organizations.backends.tokens import RegistrationTokenGenerator
from organizations.compat import reverse

from associates.backends import CustomInvitations
from associates.forms import OrganizationUserForm
from associates.models import Organization, OrganizationUser
from associates.role_privileges import FilteredObjectMixin, FilteredQuerySetMixin
from contact.models import Company, User
from contact.tests.tests_models import ContactTestCase


class MemberListTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
        Site.objects.create(domain="localhost", name="localhost")

        company = Company.objects.get(name__startswith="SkyData")
        person = user.person
        self.org, _ = Organization.objects.create_dealership(company)
        self.org.add_person_to_organization(person)
        Template.objects.create(name="assocorganizations/email/invitation_body_db.html",
                                content="")

    def test_member_list(self):

        client = Client()
        client.login(username=self.username, password=self.password)

        response = client.get(reverse("team_members"))
        self.assertEqual(response.status_code, 200)

    def test_member_list_post_non_admin(self):
        post_data = {
            'email': 'bhalerao.1@gmail.com'
        }

        client = Client()
        client.login(username=self.username, password=self.password)

        response = client.post(reverse("team_members"), data=post_data)
        self.assertEqual(response.status_code, 302)

    def test_user_already_exists(self):
        ouser = OrganizationUser.objects.get(user__username=self.username)
        ouser.user.email = 'test@gmail.com'
        ouser.user.save()

        post_data = {
            'email': 'test@gmail.com'
        }

        client = Client()
        client.login(username=self.username, password=self.password)
        ouser.is_admin = True
        ouser.save()
        response = client.post(reverse("team_members"), data=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "already exists")

    def test_member_list_post_admin(self):
        new_email = 'mymail@gmail.com'
        post_data = {
            'email': new_email
        }

        client = Client()
        client.login(username=self.username, password=self.password)
        ouser = OrganizationUser.objects.get(user__username=self.username)
        ouser.is_admin = True
        ouser.save()
        self.assertEqual(OrganizationUser.objects.count(), 1)
        response = client.post(reverse("team_members"), data=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "{} invited to register!".format(new_email))
        self.assertEqual(OrganizationUser.objects.count(), 2)

        new_user = User.objects.get(email=new_email)
        self.assertEqual(self.org.company, new_user.person.company)
        self.assertIn(new_user.person.name, new_user.username)

    def test_member_list_post_admin_form_invalid(self):

        post_data = {
            'username': 'bhalerao.1@gmail.com'
        }

        client = Client()
        client.login(username=self.username, password=self.password)
        ouser = OrganizationUser.objects.get(user__username=self.username)
        ouser.is_admin = True
        ouser.save()
        response = client.post(reverse("team_members"), data=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, b"Invalid form")

    def test_organization_user(self):
        ouser = OrganizationUser.objects.get(user=self.user)
        self.assertEqual(ouser.user.username, "test_admin")

    def test_admincontext_processor(self):

        client = Client()
        client.force_login(self.user)
        response = client.get("/dashboard/")
        self.assertEqual(response.status_code, 200)

    def test_admin_views(self):

        c = Client()
        c.force_login(self.user)
        ouser = OrganizationUser.objects.get(user=self.user)
        admin_urls = [
            "/admin/associates/organizationuser/",
            '/admin/associates/organization/',
            '/admin/associates/organization/add/',
            "/admin/associates/organizationuser/{}/change/".format(ouser.pk),
            "/admin/associates/organization/{}/change/".format(ouser.organization.pk)
        ]
        for url in admin_urls:
            resp = c.get(url)
            self.assertEqual(resp.status_code, 200)

    def test_organization_user_form(self):
        ouser = OrganizationUser.objects.get(user=self.user)

        form_data = {
            'organization': ouser.organization.pk,
            'first_name': "Kaustubh",
            'last_name': "Bhalerao",
            "email": 'nothing@burger.com',
            "password1": "bigsecret",
            "password2": "bigsecret",
            "user": ouser.user.pk
        }
        form = OrganizationUserForm(data=form_data, instance=ouser)

        self.assertTrue(form.is_valid())
        form.save()

        ouser.refresh_from_db()
        self.assertTrue(ouser.user.first_name == "Kaustubh")
        self.assertTrue(ouser.user.email == "nothing@burger.com")
        self.assertTrue(ouser.user.check_password("bigsecret"))

        form_data = {
            'organization': ouser.organization.pk,
            'first_name': "KaustubhTheMan",
            'last_name': "Bhalerao",
            "email": 'nothing@burger.com',
            "user": ouser.user.pk
        }
        form = OrganizationUserForm(data=form_data, instance=ouser)
        self.assertTrue(form.is_valid())
        form.save()
        ouser.refresh_from_db()
        self.assertTrue(ouser.user.first_name == "KaustubhTheMan")
        self.assertTrue(ouser.user.check_password("bigsecret"))

    def test_change_member_permission(self):
        c = Client()
        c.force_login(self.user)
        url = [
            reverse('change_permissions', kwargs={'par': 'active', 'pk': self.user.pk}),
            reverse('change_permissions', kwargs={'par': 'admin', 'pk': self.user.pk}),
            reverse('change_permissions', kwargs={'par': 'active', 'pk': self.user.pk}),
            reverse('change_permissions', kwargs={'par': 'admin', 'pk': self.user.pk}),
        ]
        import json
        for u in url:
            resp = c.post(u)
            self.assertEqual(json.loads(resp.content)['pk'], self.user.pk)

        url = [
            reverse('change_permissions', kwargs={'par': 'active', 'pk': self.user.pk + 1}),
            reverse('change_permissions', kwargs={'par': 'admin', 'pk': self.user.pk + 1}),
            reverse('change_permissions', kwargs={'par': 'active', 'pk': self.user.pk + 1}),
            reverse('change_permissions', kwargs={'par': 'admin', 'pk': self.user.pk + 1}),
        ]
        for u in url:
            with self.assertRaises(User.DoesNotExist):
                c.post(u)


class MixinTests(TestCase):
    class DummyListView(FilteredQuerySetMixin, ListView):
        model = OrganizationUser

    class DummyUpdateView(FilteredObjectMixin, UpdateView):
        model = OrganizationUser
        instance_owner_organization = "random"

    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
        Site.objects.create(domain="localhost", name="localhost")

        company = Company.objects.first()
        person = user.person
        self.org, _ = Organization.objects.create_dealership(company)
        self.org.add_person_to_organization(person)

    def test_filtered_query_set_mixin(self):
        view = self.DummyListView()
        self.assertRaises(AttributeError, view.get_initial_queryset)

    def test_filtered_object_mixin(self):
        view = self.DummyUpdateView(kwargs={'pk': OrganizationUser.objects.first().pk})
        self.assertRaises(AttributeError, view.get_object)


class InvitationTests(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.username = "test_admin"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user
        Site.objects.create(domain="localhost", name="localhost")

        company = Company.objects.first()
        person = user.person
        self.org, _ = Organization.objects.create_dealership(company)
        self.org.add_person_to_organization(person)

        mail.outbox = []
        self.factory = RequestFactory()
        self.tokenizer = RegistrationTokenGenerator()
        self.user = User.objects.get(username="test_admin")
        self.pending_user = User.objects.create_user(username="theresa",
                                                     email="t@example.com", password="test")
        self.pending_user.is_active = False
        self.pending_user.save()

        self.org.add_person_to_organization(self.pending_user.person)
        self.pending_user.person.company = self.org.company
        self.pending_user.person.name = self.pending_user.username
        self.pending_user.person.save()

    def test_activate_user(self):
        request = self.factory.request()
        request.user = self.pending_user
        with self.assertRaises(Http404):
            CustomInvitations().activate_view(request, self.user.id,
                                              self.tokenizer.make_token(self.user))
        with self.assertRaises(Http404):
            CustomInvitations().activate_view(request,
                                              self.pending_user.id,
                                              self.tokenizer.make_token(
                                                  self.user))
        self.assertEqual(200, CustomInvitations().activate_view(request,
                                                                self.pending_user.id,
                                                                self.tokenizer.make_token(
                                                                    self.pending_user)).status_code)

    def test_activate_user_post(self):
        form_data = {
            'username': 'test',
            'password': 'password',
            'password_confirm': 'password-diff',
            'first_name': "T",
            'last_name': 'M',
            'email_address': 't@example.com'
        }
        token = self.tokenizer.make_token(self.pending_user)
        url = reverse("invitations_register", kwargs={'user_id': self.pending_user.id,
                                                      'token': token})

        response = Client().post(url, data=form_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, _("Your password entries must match"))

        form_data = {
            'username': 'test',
            'password': 'password',
            'password_confirm': 'password',
            'first_name': "T",
            'last_name': 'M',
            'email_address': 't@example.com'
        }
        token = self.tokenizer.make_token(self.pending_user)
        url = reverse("invitations_register", kwargs={'user_id': self.pending_user.id,
                                                      'token': token})

        response = Client().post(url, data=form_data)

        self.assertEqual(response.status_code, 302)
