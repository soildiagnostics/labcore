import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from django.contrib.auth.models import User

from associates.models import Organization
from contact.models import Person
from contact.tests.tests_models import ContactTestCase
from django.test.client import Client


class CustomWebDriver(WebDriver):
    """Our own WebDriver with some helpers added"""

    def find_css(self, css_selector):
        """Shortcut to find elements by CSS. Returns either a list or singleton"""
        elems = self.find_elements_by_css_selector(css_selector)
        found = len(elems)
        if found == 1:
            return elems[0]
        elif not elems:
            raise NoSuchElementException(css_selector)
        return elems

    def wait_for_css(self, css_selector, timeout=3):
        """ Shortcut for WebDriverWait"""
        #return WebDriverWait(self, timeout).until(lambda driver: driver.find_css(css_selector))
        WebDriverWait(self, timeout).until(EC.invisibility_of_element_located((By.CSS_SELECTOR, "div.loader-section")))
        return WebDriverWait(self, timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, css_selector)))
    
    def fill_input(self, css_selector, keys):
        elem = self.wait_for_css(css_selector)
        elem.send_keys(keys)

        

class BrowserTests(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.wd = CustomWebDriver()
        cls.wd.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.wd.quit()
        super().tearDownClass()

    def _login(self, user, password):
        self.wd.get('%s%s' % (self.live_server_url, "/auth/login/?next=/dashboard/"))
        self.wd.wait_for_css(".btn")
        username_input = self.wd.find_element_by_name("username")
        username_input.send_keys(user)
        password_input = self.wd.find_element_by_name("password")
        password_input.send_keys(password)
        self.wd.wait_for_css(".btn").click()
        self.wd.wait_for_css("#profile_button")

class LoginTests(BrowserTests):

    fixtures = ['roles']
    def setUp(self):
        ContactTestCase.setUp(self)
        self.u = User.objects.get(username="testinguser")
        self.u.set_password("buckeyes")
        self.u.is_superuser = True
        self.u.is_staff = True
        self.u.is_active = True
        self.u.save()

    def _logout(self, selector):
        self.wd.wait_for_css("#user-tools > a:nth-child(5)")
        self.wd.find_css(selector).click()
        self.wd.wait_for_css("#dashboard")

    def _login(self, url, button):
        self.wd.get('%s%s' % (self.live_server_url, url))
        self.wd.wait_for_css(button)
        username_input = self.wd.find_element_by_name("username")
        username_input.send_keys(self.u.username)
        password_input = self.wd.find_element_by_name("password")
        password_input.send_keys('buckeyes')
        self.wd.find_css(button).click()

    def test_create_associate_organization(self):
        self._login("/admin/", '#login-form > div.submit-row > input[type=submit]')
        self.wd.get('%s%s' % (self.live_server_url, "/admin/associates/organization/add/"))
        self.wd.find_css("#add_id_company > img").click()
        main_window = self.wd.window_handles[0]
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_id("id_name").send_keys("Test Company")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        ## Add a new AO
        self.wd.find_css("#organization_users-group > fieldset > div.add-row > a").click()
        self.wd.wait_for_css("#id_organization_users-0-username")
        self.wd.find_css("#id_organization_users-0-username").send_keys("newaouser")
        self.wd.find_css("#id_organization_users-0-first_name").send_keys("New")
        self.wd.find_css("#id_organization_users-0-last_name").send_keys("User")
        self.wd.find_css("#id_organization_users-0-last_name").send_keys("User")
        self.wd.find_css("#id_organization_users-0-email").send_keys("test@testco.com")
        self.wd.find_css("#id_organization_users-0-is_admin").click()
        self.wd.find_css("#id_organization_users-0-is_primary").click()
        self.wd.find_css("#id_roles_from > option[title='Dealer']").click()
        self.wd.find_css("#id_roles_add_link").click()
        self.wd.find_css("#id_organization_users-0-password1").send_keys("buckeyes")
        self.wd.find_css("#id_organization_users-0-password2").send_keys("buckeyes")
        # self.wd.find_css("#organization_form > div > div.submit-row > input.default").click()

        ## Add a second, non-primary AO
        self.wd.find_css("#organization_users-group > fieldset > div.add-row > a").click()
        self.wd.wait_for_css("#id_organization_users-0-username")
        self.wd.find_css("#id_organization_users-1-username").send_keys("secondaouser")
        self.wd.find_css("#id_organization_users-1-first_name").send_keys("Second")
        self.wd.find_css("#id_organization_users-1-last_name").send_keys("User")
        self.wd.find_css("#id_organization_users-1-last_name").send_keys("User")
        self.wd.find_css("#id_organization_users-1-email").send_keys("test2@testco.com")
        self.wd.find_css("#id_organization_users-1-password1").send_keys("buckeyes")
        self.wd.find_css("#id_organization_users-1-password2").send_keys("buckeyes")
        self.wd.find_css("#organization_form > div > div.submit-row > input.default").click()

        ao = Organization.objects.latest("pk")
        self.assertTrue(ao.is_dealer)
        self.assertEqual(ao.company.name, "Test Company")
        p = Person.objects.get(user__username="newaouser")
        self.assertEqual(ao.company.get_company_contact(), p)

        self.assertEqual(len(ao.company.company_members()), 2)
        p = Person.objects.get(user__username="secondaouser")
        self.assertIn(p.user, ao.users.all())

        tc = Client()
        tc.force_login(p.user)
        resp = tc.get("/dashboard/")
        self.assertEqual(resp.status_code, 200)


        self.wd.get('%s%s' % (self.live_server_url, "/auth/login/?next=/dashboard/"))
        self.wd.wait_for_css(".btn")
        username_input = self.wd.find_element_by_name("username")
        username_input.send_keys(p.user.username)
        password_input = self.wd.find_element_by_name("password")
        password_input.send_keys('buckeyes')
        self.wd.find_css(".btn").click()
        self.wd.wait_for_css("a.btn")






