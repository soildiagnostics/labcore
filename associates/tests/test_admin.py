from unittest import skip

from django.contrib.admin.sites import AdminSite
from django.forms import inlineformset_factory
from django.test import TestCase
from django.test.client import Client
from organizations.forms import OrganizationUserAddForm

from associates.admin import OrgUserFormSet, OrgUserInline
from associates.forms import AssociatedOrganizationForm, OrganizationUserForm
from associates.models import Role, Organization, OrganizationUser
from contact.models import User, Person, Company
from contact.tests.tests_models import ContactTestCase


class AssociatesAdminTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.u = User.objects.get(username="testinguser")
        self.u.is_superuser = True
        self.u.is_staff = True
        self.u.is_active = True
        self.u.save()
        self.client = Client()
        self.client.force_login(self.u)

        self.site = AdminSite()

    def test_admin_views(self):
        """Staff user can view Associated Organization pages"""
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(company=c)

        p = Person.objects.get(user=self.u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)

        org = Organization.objects.get(company__name__startswith="Sky")
        pages = [
            '/admin/associates/organization/',
            '/admin/associates/organization/add/',
            f'/admin/associates/organization/{org.pk}/change/',
        ]

        for page in pages:
            response = self.client.get(page)
            self.assertEqual(response.status_code, 200)

    def test_assoc_org_form(self):
        data = {
            'name': "Form Organization",
            "company": Company.objects.create(name="Form Org Company").pk,
            "is_active": True,
            "slug": "Org-slug",
            "roles": [Role.objects.get(role="Dealer").pk],
            "users": [User.objects.get(username="testinguser").pk]
        }

        form = AssociatedOrganizationForm(data)
        self.assertTrue(form.is_valid())

    # def test_add_assoc_org_form(self):
    #     company = Company.objects.create(name="Form Org Company")
    #     ao = Organization.objects.create(company=company)
    #     data = {
    #         'name': "Form Organization",
    #         "company": company.pk,
    #         "is_active": True,
    #         "slug": "Org-slug",
    #         "roles": [Role.objects.get(role="Dealer").pk],
    #         "users": [User.objects.get(username="testinguser").pk],
    #         'organization_users-TOTAL_FORMS': '1',
    #         'organization_users-INITIAL_FORMS': '0',
    #         'organization_users-MIN_NUM_FORMS': '0',
    #         'organization_users-MAX_NUM_FORMS': '1000',
    #         'organization_users-0-username': 'testcouser',
    #         'organization_users-0-first_name': 'TestCo',
    #         'organization_users-0-last_name': 'User',
    #         'organization_users-0-email': 'testco@testco.com',
    #         'organization_users-0-is_admin': True,
    #         'organization_users-0-is_primary': True,
    #         'organization_users-0-password1': 'buckeyes',
    #         'organization_users-0-password2': 'buckeyes',
    #         'organization_users-0-id': "",
    #         'organization_users-0-organization': ao.pk,
    #         '_save': 'Save'
    #     }
    #     form = inlineformset_factory(Organization,
    #                                  OrganizationUser,
    #                                  formset=OrgUserFormSet,
    #                                  fields=('user','username',))(data)
    #     self.assertTrue(form.is_valid())

    def test_add_assoc_orguser_form(self):
        company = Company.objects.create(name="Form Org Company")
        ao = Organization.objects.create(company=company)
        data = {
            'username': 'testcouser',
            'first_name': 'TestCo',
            'last_name': 'User',
            'email': 'testco@testco.com',
            'is_admin': True,
            'is_primary': True,
            'password1': 'buckeyes',
            'password2': 'buckeyes',
            'id': '',
            'organization': ao.pk,
            '_save': 'Save'
        }

        form = OrganizationUserForm(data)
        self.assertTrue(form.is_valid())

        # form = OrganizationUserAddForm(data, organization=ao)
        # form.is_valid()
        # print(form)
        # self.assertTrue(form.is_valid())
