from django.core.exceptions import ValidationError
from django.test import Client
from django.test import TestCase

from associates.models import Role, Organization
from contact.models import Company, Person, EmailModel, User
from contact.tests.tests_models import ContactTestCase


# Create your tests here.


class RoleTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)

    def test_role_dealer(self):
        # r = Role.objects.get(role="Dealer")
        # self.assertEqual(r.pk, 2)
        # self.assertEqual(str(r), "Dealer (Dealer organization, that has access to all clients of this organization)")

        c = Company.objects.get(name__startswith="Sky")
        org, created = Organization.objects.get_or_create(company=c)
        u = User.objects.get(username="testinguser")
        p = u.person
        p.save()
        org.add_person_to_organization(p)

        r, created = Role.objects.get_or_create(role="Primary")

        self.assertFalse(org.has_role(r))
        org.roles.add(r)
        self.assertTrue(org.has_role(r))

        ou = org.organization_users.first()
        self.assertTrue(ou.organization.has_role(r))

        self.assertTrue(u.associates_organizationuser.first().organization.has_role(r))


class AssociatedOrganizationTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)

    def test_get_or_create_associated_organization(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)
        self.assertEqual(str(a), "SkyData Technologies LLC")
        self.assertEqual(a.get_owner, None)

    def test_create_dealership(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(company=c)
        self.assertEqual(created, True)
        self.assertEqual(a.is_dealer, True)

    def test_create_lab_processor(self):
        c = Company.objects.get(name__startswith="Sky")
        r, created = Role.objects.get_or_create(role="Lab Processor")
        a, created = Organization.objects.get_or_create(company=c)
        self.assertEqual(created, True)
        a.roles.add(r)
        l = Organization.objects.get_lab_processor_organizations()
        self.assertIn(a, l)
        self.assertTrue(a.is_lab_processor)

    def test_existing_company(self):
        c = Company.objects.get(name__startswith="Sky")
        a1, created1 = Organization.objects.get_or_create(company=c)
        a2, created2 = Organization.objects.get_or_create(company=c)
        self.assertEqual(a1, a2)

    def test_add_members_to_organization(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)
        Organization.objects.create_dealership(company=c)
        u = User.objects.get(username="testinguser")
        a.set_organization_owner(user=u)
        self.assertEqual(a.get_owner.organization_user.user, u)
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 1)
        client = Client()
        client.force_login(u)
        resp = client.get("/dashboard/")
        self.assertEqual(resp.status_code, 200)

        m = Person.objects.create(name="Kaustubh",
                                  last_name="Bhalerao")
        e = EmailModel.objects.create(email="test@example.com")
        e.contact = m
        e.save()
        m.company = c
        m.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 2)

        m = Person.objects.create(name="Jessie",
                                  last_name="Bhalerao")
        m.company = c
        m.save()
        l = a.add_company_members_to_organization()
        self.assertEqual(len(l), 3)

        # try running again, make sure get_or_create present
        self.assertEqual(len(a.add_company_members_to_organization()), 3)

    def test_add_person_without_user_to_organization(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)
        p, created = Person.objects.get_or_create(name="Sachin", last_name="Bhalerao")
        self.assertTrue(created)
        self.assertEqual(p.user, None)

        a.add_person_to_organization(p)
        l = a.organization_users.count()

        self.assertEqual(l, 1)
        p.refresh_from_db()
        self.assertIsNotNone(p.user)

    def test_set_organization_owner(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)
        u = User.objects.get(username="testinguser")
        # p = Person.objects.get_or_create_person_from_user(u)
        # p.save()
        p = Person.objects.get(user=u)
        owner = a.set_organization_owner(u)

        self.assertEqual(owner.organization_user.user.username, "testinguser")
        self.assertEqual(owner.organization_user.username, "testinguser")

    def test_allow_only_one_primary(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)

        r = Role.objects.get(role="Primary")
        a.roles.add(r)

        self.assertEqual(a.is_primary, True)
        self.assertEqual(r.organization_set.count(), 1)

        c2 = Company.objects.create(name="Usurper Inc")
        b, created = Organization.objects.get_or_create(company=c2)

        with self.assertRaises(ValidationError):
            b.roles.add(r)

    def test_allow_only_one_primary_other_side(self):
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.get_or_create(company=c)

        r = Role.objects.get(role="Primary")
        a.roles.add(r)

        self.assertEqual(a.is_primary, True)
        self.assertEqual(r.organization_set.count(), 1)

        c2 = Company.objects.create(name="Usurper Inc")
        b, created = Organization.objects.get_or_create(company=c2)

        with self.assertRaises(ValidationError):
            r.organization_set.add(b)
