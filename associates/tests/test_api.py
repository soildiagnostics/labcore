import json

from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from rest_framework.test import APITestCase

from associates.models import Organization, OrganizationUser, OrganizationOwner
from associates.tests.test_views import MemberListTestCase
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Person
from contact.tests.tests_models import ContactTestCase


class AOListAPITestCase(TestCase):

    fixtures = ['roles']

    def setUp(self):
        MemberListTestCase.setUp(self)

    def test_list_associate_orgs(self):

        client = Client()
        client.force_login(self.user)

        response = client.get(reverse('api_associate_organizations'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)
        self.assertEqual(json.loads(response.content)[0]['name'], "SkyData Technologies LLC")


class AOAPITestCase(APITestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.is_superuser = True

    def test_create_associated_organization(self):
        data = {
            "company": {
                "name": "EC Infosolutions Pvt Ltd",
                "address": [{
                    "address": "Baner",
                    "address_type": "O",
                    "primary": True
                }],
                "phone": [{
                    "phone_number": "+1234567890",
                    "phone_type": "M",
                    "primary": True
                }],
                "email": [{
                    "email": "ec@gmail.com",
                    "email_type": "O",
                    "primary": True
                }]
            },
            "roles": [
                "Dealer",
                "Lab Processor"
            ]
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        self.assertEqual(content['name'], "EC Infosolutions Pvt Ltd")
        self.assertEqual(content['roles'][0]['role'], "Dealer")
        self.assertEqual(content['roles'][1]['role'], "Lab Processor")
        self.assertEqual(content['is_active'], True)
        self.assertEqual(content['company']['name'], "EC Infosolutions Pvt Ltd")
        self.assertEqual(content['company']['address'][0]['address'], "Baner")
        self.assertEqual(content['company']['phone'][0]['phone_number'], "+1234567890")
        self.assertEqual(content['company']['email'][0]['email'], "ec@gmail.com")

        # This user is not superuser nor an organization user with staff previlages, hence response status code
        # should be 403 Forbidden
        user = User.objects.create_user(username="nonuser", password="anotherpassword")
        self.assertEqual(user.is_superuser, False)
        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_update_associated_organization(self):
        data = {
            "company": {
                "name": "EC Infosolutions",
                "address": [{
                    "address": "Baner",
                    "address_type": "O",
                    "primary": True
                }],
                "phone": [{
                    "phone_number": "+1234567890",
                    "phone_type": "M",
                    "primary": True
                }],
                "email": [{
                    "email": "ec@gmail.com",
                    "email_type": "O",
                    "primary": True
                }]
            },
            "roles": [
                "Dealer",
                "Lab Processor"
            ]
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        org = Organization.objects.latest('pk')
        self.assertEqual(org.name, "EC Infosolutions")
        url_update = reverse("api_edit_associate_organization", kwargs={'pk': org.pk})

        company_data = dict()
        company_data['company'] = content['company']
        company_data['company']['name'] = "EC Infosolutions Updated"
        company_data['company']['address'][0]['address'] = "Baner Updated"
        company_data['company']['phone'][0]['phone_number'] = "+1111111111"
        company_data['company']['email'][0]['email'] = "ec_updated@gmail.com"

        response = self.client.put(url_update, data=company_data, format='json')
        self.assertEqual(response.status_code, 200)
        content = response.json()

        self.assertEqual(content['name'], "EC Infosolutions Updated")
        self.assertEqual(content['roles'][0]['role'], "Dealer")
        self.assertEqual(content['roles'][1]['role'], "Lab Processor")
        self.assertEqual(content['is_active'], True)
        self.assertEqual(content['company']['name'], "EC Infosolutions Updated")
        self.assertEqual(content['company']['address'][0]['address'], "Baner Updated")
        self.assertEqual(content['company']['phone'][0]['phone_number'], "+1111111111")
        self.assertEqual(content['company']['email'][0]['email'], "ec_updated@gmail.com")

        # This user is not superuser nor an organization user with staff previlages, hence response status code
        # should be 403 Forbidden
        user = User.objects.create_user(username="nonuser2", password="anotherpassword")
        self.assertEqual(user.is_superuser, False)
        self.client.force_authenticate(user=user)
        response = self.client.put(url_update, data=company_data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_add_role_via_update(self):
        data = {
            "company": {
                "name": "Testing Company"
            },
            "roles": []
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        org = Organization.objects.latest('pk')
        self.assertEqual(org.name, "Testing Company")
        self.assertEqual(len(content['roles']), 0)
        url_update = reverse("api_edit_associate_organization", kwargs={'pk': org.pk})

        updated_data = {
            "add_roles": [
                "Dealer"
            ]
        }

        response = self.client.put(url_update, data=updated_data, format='json')
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(len(content['roles']), 1)
        self.assertEqual(content['roles'][0]['role'], "Dealer")

    def test_remove_role_via_update(self):
        data = {
            "company": {
                "name": "Testing Company Second"
            },
            "roles": [
                "Dealer",
                "Lab Processor"
            ]
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        org = Organization.objects.latest('pk')
        self.assertEqual(org.name, "Testing Company Second")
        self.assertEqual(len(content['roles']), 2)
        url_update = reverse("api_edit_associate_organization", kwargs={'pk': org.pk})

        updated_data = {
            "remove_roles": [
                "Lab Processor"
            ]
        }

        response = self.client.put(url_update, data=updated_data, format='json')
        self.assertEqual(response.status_code, 200)
        content = response.json()
        self.assertEqual(len(content['roles']), 1)
        self.assertEqual(content['roles'][0]['role'], "Dealer")

    def tearDown(self):
        self.user.is_superuser = False


class AOUserCreateAPITestCase(APITestCase):
    fixtures = ['roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.is_superuser = True
        self.org = Organization.objects.get(company__name__startswith="Sky")

    def test_create_associated_organization_user(self):
        data = {
            "is_admin": True,
            "is_owner": True,
            "user": {
                "username": "OrgUser",
                "password": "password",
                "first_name": "Org",
                "last_name": "User",
                "email": "org@user.com",
                "is_staff": True
            },
            "organization": self.org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_admin, True)
        self.assertEqual(org_user.is_owner, True)
        self.assertEqual(org_user.user.username, "OrgUser")
        self.assertEqual(org_user.user.first_name, "Org")
        self.assertEqual(org_user.user.last_name, "User")
        self.assertEqual(org_user.user.email, "org@user.com")
        self.assertEqual(org_user.user.is_staff, True)
        self.assertEqual(org_user.user.is_superuser, False)
        self.assertEqual(org_user.organization.pk, self.org.pk)

        person = Person.objects.latest('pk')
        self.assertEqual(person.user.pk, org_user.user.pk)
        self.assertEqual(person.company.pk, org_user.organization.company.pk)
        self.assertEqual(person.is_company_contact, True)
        self.assertEqual(str(person), "Org User")

    def test_update_associated_organization_user(self):
        data = {
            "is_admin": True,
            "is_owner": False,
            "user": {
                "username": "UpdateUser",
                "password": "password",
                "first_name": "Update",
                "last_name": "User New",
                "email": "update@user.com",
                "is_staff": True
            },
            "organization": self.org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_owner, False)

        updated_data = {
            "is_admin": False,
            "is_owner": True,
            "user": {
                "username": "UpdateUserNew",
                "first_name": "UpdateAgain",
                "last_name": "User",
                "email": "updateagain@user.com",
                "is_staff": False
            },
            "organization": self.org.pk
        }

        url = reverse("api_edit_associate_organization_user", kwargs={'pk': org_user.pk})
        response = self.client.put(url, data=updated_data, format='json')
        self.assertEqual(response.status_code, 200)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_admin, False)
        self.assertEqual(org_user.is_owner, True)
        self.assertEqual(org_user.user.username, "UpdateUserNew")
        self.assertEqual(org_user.user.first_name, "UpdateAgain")
        self.assertEqual(org_user.user.last_name, "User")
        self.assertEqual(org_user.user.email, "updateagain@user.com")
        self.assertEqual(org_user.user.is_staff, False)
        self.assertEqual(org_user.user.is_superuser, False)
        self.assertEqual(org_user.organization.pk, self.org.pk)

        person = Person.objects.latest('pk')
        self.assertEqual(person.user.pk, org_user.user.pk)
        self.assertEqual(person.company.pk, org_user.organization.company.pk)
        self.assertEqual(person.is_company_contact, True)
        self.assertEqual(str(person), "UpdateAgain User")

    def test_unique_organization_owner_while_creating_associated_organization_users(self):
        data = {
            "company": {
                "name": "Organization Owner Test",
            },
            "roles": [
                "Dealer",
                "Lab Processor"
            ]
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        org = Organization.objects.latest('pk')
        org_owner = org.get_owner
        self.assertEqual(org_owner, None)

        org_owner_data = {
            "is_admin": True,
            "is_owner": True,
            "user": {
                "username": "OrgOwner",
                "password": "password",
                "first_name": "Update",
                "last_name": "User New",
                "email": "update@user.com",
                "is_staff": True
            },
            "organization": org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=org_owner_data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_owner, True)
        org.refresh_from_db()
        org_owner = org.get_owner
        self.assertEqual(org_owner.organization_user.pk, org_user.pk)

        owner_db = OrganizationOwner.objects.filter(organization=org)
        self.assertEqual(len(owner_db), 1)

        org_owner_data = {
            "is_admin": True,
            "is_owner": True,
            "user": {
                "username": "OrgOwnerAgain",
                "password": "password",
                "first_name": "Update",
                "last_name": "User New Again",
                "email": "update@user.com",
                "is_staff": True
            },
            "organization": org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=org_owner_data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_owner, True)
        org.refresh_from_db()
        org_owner = org.get_owner
        self.assertEqual(org_owner.organization_user.pk, org_user.pk)

        owner_db = OrganizationOwner.objects.filter(organization=org)
        self.assertEqual(len(owner_db), 1)

    def test_unique_organization_owner_while_updating_associated_organization_users(self):
        data = {
            "company": {
                "name": "Organization Owner Update Test",
            },
            "roles": [
                "Dealer",
                "Lab Processor"
            ]
        }

        url = reverse("api_new_associate_organization")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 201)
        content = response.json()

        org = Organization.objects.latest('pk')
        org_owner = org.get_owner
        self.assertEqual(org_owner, None)

        org_owner_data = {
            "is_admin": True,
            "is_owner": True,
            "user": {
                "username": "OwnerOrg",
                "password": "password",
                "first_name": "Update",
                "last_name": "User New",
                "email": "update@user.com",
                "is_staff": True
            },
            "organization": org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=org_owner_data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user.is_owner, True)
        org.refresh_from_db()
        org_owner = org.get_owner
        self.assertEqual(org_owner.organization_user.pk, org_user.pk)

        owner_db = OrganizationOwner.objects.filter(organization=org)
        self.assertEqual(len(owner_db), 1)

        org_owner_2_data = {
            "is_admin": True,
            "is_owner": False,
            "user": {
                "username": "OwnerOrgAgain",
                "password": "password",
                "first_name": "Update",
                "last_name": "User New Again",
                "email": "update@user.com",
                "is_staff": True
            },
            "organization": org.pk
        }

        url = reverse("api_new_associate_organization_user")
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data=org_owner_2_data, format='json')
        self.assertEqual(response.status_code, 201)

        org_user_2 = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user_2.is_owner, False)
        org.refresh_from_db()
        org_owner = org.get_owner
        self.assertEqual(org_owner.organization_user.pk, org_user.pk)

        owner_db = OrganizationOwner.objects.filter(organization=org)
        self.assertEqual(len(owner_db), 1)

        org_owner_2_data_update = {
            "is_owner": True,
        }

        url = reverse("api_edit_associate_organization_user", kwargs={'pk': org_user_2.pk})
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data=org_owner_2_data_update, format='json')
        self.assertEqual(response.status_code, 200)

        org_user_2 = OrganizationUser.objects.latest('pk')
        self.assertEqual(org_user_2.is_owner, True)
        org.refresh_from_db()
        org_owner = org.get_owner
        self.assertEqual(org_owner.organization_user.pk, org_user_2.pk)

        owner_db = OrganizationOwner.objects.filter(organization=org)
        self.assertEqual(len(owner_db), 1)

        org_owner_2_data_update = {
            "is_owner": False,
        }

        url = reverse("api_edit_associate_organization_user", kwargs={'pk': org_user_2.pk})
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data=org_owner_2_data_update, format='json')
        self.assertEqual(response.status_code, 400)
        content = response.json()
        self.assertIn("is_owner", content.keys())

    def tearDown(self):
        self.user.is_superuser = False
