from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from contact.serializers import CompanyCreateSerializer
from .models import Organization, OrganizationUser, Role
from .permissions import IsSuperUserOrIsOrgStaffUser
from .serializers import OrganizationSerializer, OrganizationUserCreateSerializer, OrganizationUserUpdateSerializer, \
    OrganizationCreateSerializer, OrganizationUpdateSerializer


class APIOrganizationList(generics.ListAPIView):
    """
    Send list of dealers
    """
    def get_queryset(self):
        queryset = Organization.objects.filter(roles__role="Dealer")
        return queryset
    serializer_class = OrganizationSerializer


class APINewOrganization(generics.CreateAPIView):
    """
    Single form API to create a new Associated Organization - returns organization data.
    """
    permission_classes = (IsAuthenticated, IsSuperUserOrIsOrgStaffUser, )
    serializer_class = OrganizationCreateSerializer

    def post(self, request, *args, **kwargs):
        # updated_data = request.data.copy()
        roles = request.data.get('roles')

        serializer = OrganizationCreateSerializer(data=request.data)

        if serializer.is_valid():
            roles = serializer.validated_data.get('roles')

            if roles:
                if "Primary" in roles and Organization.objects.get_primary():
                    return Response({'error': 'Organization with Primary role already exists, you can have max one Primary Organization'},
                                    status=status.HTTP_400_BAD_REQUEST)

                for role in roles:
                    try:
                        r = Role.objects.get(role=role)
                    except ObjectDoesNotExist:
                        return Response({'error': f'Role with name {role} does not exist'},
                                        status=status.HTTP_400_BAD_REQUEST)

            new_org = serializer.save()
            org = OrganizationSerializer(new_org)
            return Response(org.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIEditOrganization(generics.RetrieveUpdateAPIView):
    """
    Edit Associated Organization's underlying company details
    """
    permission_classes = (IsAuthenticated, IsSuperUserOrIsOrgStaffUser, )
    serializer_class = OrganizationUpdateSerializer
    queryset = Organization.objects.all()

    def update(self, request, *args, **kwargs):
        organization = self.get_object()

        serializer = OrganizationUpdateSerializer(instance=organization, data=request.data)
        if serializer.is_valid():
            add_roles = serializer.validated_data.get('add_roles')
            remove_roles = serializer.validated_data.get('remove_roles')

            if add_roles:
                if "Primary" in add_roles and Organization.objects.get_primary():
                    return Response({'error': 'Organization with Primary role already exists, you can have max one Primary Organization'},
                                    status=status.HTTP_400_BAD_REQUEST)

                for role in add_roles:
                    try:
                        r = Role.objects.get(role=role)
                    except ObjectDoesNotExist:
                        return Response({'error': f'Role with name {role} does not exist'},
                                        status=status.HTTP_400_BAD_REQUEST)

            if remove_roles:
                for role in remove_roles:
                    try:
                        r = Role.objects.get(role=role)
                    except ObjectDoesNotExist:
                        return Response({'error': f'Role with name {role} does not exist'},
                                        status=status.HTTP_400_BAD_REQUEST)

            updated_org = serializer.save()
            org = OrganizationSerializer(updated_org)
            return Response(org.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APINewOrganizationUser(generics.CreateAPIView):
    """
    Single form API to create a new Associated Organization Users - returns Organization User data.
    """
    permission_classes = (IsAuthenticated, IsSuperUserOrIsOrgStaffUser,)
    serializer_class = OrganizationUserCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = OrganizationUserCreateSerializer(data=request.data)
        if serializer.is_valid():
            org_user = serializer.save()
            user_data = OrganizationUserCreateSerializer(org_user)
            response_data = user_data.data
            response_data['user'].pop('password')
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APIEditOrganizationUser(generics.RetrieveUpdateAPIView):
    """
        Edit Associated Organization User
    """
    permission_classes = (IsAuthenticated, IsSuperUserOrIsOrgStaffUser,)
    serializer_class = OrganizationUserUpdateSerializer
    queryset = OrganizationUser.objects.all()

    def update(self, request, *args, **kwargs):
        org_user = self.get_object()
        updated_request = request.data.copy()
        if updated_request.get('user'):
            if org_user.user.username == updated_request['user'].get('username'):
                updated_request['user'].pop('username')

        serializer = OrganizationUserUpdateSerializer(instance=org_user, data=updated_request)
        if serializer.is_valid():
            org_user = serializer.save()
            user_data = OrganizationUserUpdateSerializer(org_user)
            response_data = user_data.data
            response_data['user'].pop('password')
            return Response(user_data.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
