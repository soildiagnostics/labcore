from django.contrib.auth import authenticate, login
from django.http import Http404
from django.shortcuts import render, redirect
from django.urls import reverse, path
from django.utils.translation import ugettext as _
from organizations.backends.defaults import InvitationBackend
# from organizations.backends.tokens import RegistrationTokenGenerator
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from associates.forms import AssocUserRegistrationForm
from associates.models import Organization, OrganizationUser
from contact.models import User


class CustomInvitations(InvitationBackend):
    registration_form_template = 'assocorganizations/register_form.html'
    activation_success_template = 'assocorganizations/register_success.html'
    invitation_body = 'assocorganizations/email/invitation_body_db.html'
    form_class = AssocUserRegistrationForm

    def __init__(self, org_model=None, namespace=None):
        super().__init__(org_model)
        self.user_model = User
        self.org_model = Organization

    def get_urls(self):
        return [
            path('<int:user_id>-<str:token>/',
                view=self.activate_view, name="invitations_register"),
        ]

    def activate_view(self, request, user_id, token):
        """
        View function that activates the given User by setting `is_active` to
        true if the provided information is verified.
        """
        context = dict()
        try:
            user = self.user_model.objects.get(id=user_id, is_active=False)
        except self.user_model.DoesNotExist:
            raise Http404(_("Your URL may have expired."))
        if not PasswordResetTokenGenerator().check_token(user, token):
            raise Http404(_("Your URL may have expired."))
        form = self.get_form(data=request.POST or None, files=request.FILES or None, instance=user)
        context['form'] = form
        context['ouser'] = OrganizationUser.objects.get(user=user)
        context['org'] = Organization.objects.get(company=user.person.company)
        context['orgimage'] = context['org'].company.image.url if context['org'].company.image else None
        if form.is_valid():
            form.instance.is_active = True
            user = form.save()
            user.set_password(form.cleaned_data['password'])
            user.save()
            user.person.name = user.first_name
            user.person.last_name = user.last_name
            user.person.save()
            self.activate_organizations(user)
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
            login(request, user)
            return redirect(self.get_success_url())
        else:
            #print("Errors {}".format(form.errors))
            return render(request, self.registration_form_template, context)

    def get_success_url(self):
        return reverse("team_members")
