from django.core.management.color import no_style
from django.db import connection
from django.db import migrations

from associates.models import Organization, OrganizationUser, OrganizationOwner


def reset_sequences_in_app(apps, schema_editor):
    sequence_sql = connection.ops.sequence_reset_sql(no_style(), [Organization, OrganizationUser, OrganizationOwner])
    with connection.cursor() as cursor:
        for sql in sequence_sql:
            cursor.execute(sql)


class Migration(migrations.Migration):

    dependencies = [
        ('associates', '0006_move_organizations_part2'),
    ]

    operations = [
        migrations.RunPython(reset_sequences_in_app, reverse_code=migrations.RunPython.noop),
    ]
