# Generated by Django 2.0.7 on 2018-08-30 15:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('associates', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='role',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
