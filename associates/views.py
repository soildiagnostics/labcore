from django.conf import settings
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView, View
from organizations.backends import invitation_backend

from associates.forms import OrgUserInvitationForm
from associates.models import OrganizationUser
from associates.role_privileges import DealershipRequiredMixin
from contact.models import User


class TeamMembers(DealershipRequiredMixin, ListView):
    template_name = 'associates/member_list.html'
    model = OrganizationUser

    def get_queryset(self, **kwargs):
        return OrganizationUser.objects.filter(organization=self.org).order_by('user__username')

    def get_context_data(self, **kwargs):
        context = super(TeamMembers, self).get_context_data(**kwargs)
        context['org'] = self.org
        context['invitation_form'] = OrgUserInvitationForm()
        return context

    def post(self, request, *args, **kwargs):
        if self.orguser.is_admin:

            form = OrgUserInvitationForm(request.POST)

            context = self.get_context_data(object_list=self.get_queryset())

            if form.is_valid():
                invited_email = form.cleaned_data['email']
                try:
                    ouser = OrganizationUser.objects.get(organization=self.org,
                                                         user__email=invited_email,
                                                         user__is_active=True
                                                         )
                    context['form_msg'] = "User {} already exists".format(invited_email)
                    return render(request, self.template_name, context)

                except OrganizationUser.DoesNotExist:
                    user = invitation_backend().invite_by_email(invited_email,
                                                                sender=request.user,
                                                                domain=settings.DOMAIN,
                                                                organization=self.org)
                    context['form_msg'] = "{} invited to register! \
                            Once they have accepted, their details will \
                            appear on this page.".format(invited_email)

                    person = user.person
                    self.org.add_person_to_organization(person)
                    person.company = self.org.company
                    person.name = user.username[:30] ## limited characters because user.first_name is varchar(30)
                    person.save()
                    return render(request, self.template_name, context)

            else:
                context['form_msg'] = "Invalid form"
                return render(request, self.template_name, context)

        else:
            return HttpResponseRedirect(reverse("team_members"))


class ChangePermissions(DealershipRequiredMixin, View):

    def post(self, request, *args, **kwargs):

        try:
            user = User.objects.get(pk=kwargs['pk'])
        except Exception:
            raise

        if kwargs['par'] == "active":
            user.is_active = not user.is_active
            user.save()
        if kwargs['par'] == "admin":
            ouser = OrganizationUser.objects.get(user=user)
            ouser.is_admin = not ouser.is_admin
            ouser.save()

        return JsonResponse(kwargs)
