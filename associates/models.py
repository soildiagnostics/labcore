from django.core.exceptions import *
from django.db import models
from django.db.models import ProtectedError
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from organizations.models import AbstractOrganization, AbstractOrganizationUser, AbstractOrganizationOwner

from common.models import RefreshFromDbInvalidatesCachedPropertiesMixin
from contact.models import Company, Person


class Role(models.Model):
    role = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return "{}".format(self.role)


class OrganizationManager(models.Manager):

    # select the related company on every queryset
    def get_queryset(self, *args, **skwargs):
        return super(OrganizationManager, self).get_queryset().select_related('company')

    def create_dealership(self, company):
        ''' Creates a dealership organization from a company record '''
        a, created = self.get_or_create(company=company)
        r, _ = Role.objects.get_or_create(role="Dealer")
        a.roles.add(r)
        a.save()
        a.invalidate_cached_properties()
        assert a.is_dealer, "Failed to set org to dealership"
        return (a, created)

    def get_lab_processor_organizations(self):
        return Organization.objects.filter(roles__role="Lab Processor")

    def get_sampler_organizations(self):
        return Organization.objects.filter(roles__role="Sampler")

    def get_primary(self):
        return Organization.objects.get(roles__role="Primary")

    def set_primary(self, org):
        try:
            p = self.get_primary()
            raise ValidationError(f"There is already another primary: {p}")
        except ObjectDoesNotExist:
            org.roles.add(Role.objects.get(role="Primary"))
            org.invalidate_cached_properties()
        return org

    @property
    def me(self):
        return self.get_primary()


class Organization(RefreshFromDbInvalidatesCachedPropertiesMixin, AbstractOrganization):
    """ Concrete class inheriting from Abstract :model: `Organization` that links to :model: `Company` """

    company = models.ForeignKey(Company, on_delete=models.PROTECT)
    roles = models.ManyToManyField(Role)
    objects = OrganizationManager()

    def __str__(self):
        return self.company.name

    def save(self, *args, **kwargs):

        if not self.pk:
            # if this object is not yet created
            self.name = self.company.name

        super().save(*args, **kwargs)
        if self.is_dealer:
            self.add_company_members_to_organization()

    def add_person_to_organization(self, person):
        user = person.user
        if not user:
            username = Person.get_unique_user_name(None, person.name, person.last_name, person.company)
            user = person.get_or_create_user_from_person(username=username, is_active=True)
            # print('User from get_or_create method: ', user)

        res = OrganizationUser.objects.get_or_create(
            user=user, organization=self)
        return (res)

    def add_company_members_to_organization(self):
        ''' looks for :model: `contactfield.Person`s 
            and adds them as :model: `organization.OrganizationUser`s
            to the :model: `people.AssociatedOrganization`. 
            If the person is listed as the company contact, they are 
            set to be the organization admin.

            This method assumes that if a person is not an existing 
            :model: `django.contrib.auth.models.User`
            it will create a User from for that person. 
        '''
        persons = Person.objects.filter(company=self.company)
        added_list = []
        for p in persons:
            if not p.user:
                username = Person.get_unique_user_name(None, p.name, p.last_name, p.company)
                p.get_or_create_user_from_person(username=username, is_active=True)

            res = self.add_person_to_organization(p)
            added_list.append(res)
            ou, created = res
            if p.is_company_contact and created:
                ou.is_admin = True
                ou.save()
        return added_list

    def set_organization_owner(self, user):
        ''' a :model: `contactfields.Person` may be set to be an owner of the organization
            Will check to ensure that the person is registered as a user of that organization
        '''
        try:
            ouser = OrganizationUser.objects.get(user__username=user,
                                                 organization=self)
        except ObjectDoesNotExist:
            ''' create the org user'''
            ouser, created = OrganizationUser.objects.get_or_create(user=user,
                                                                    organization=self)

        for c in self.company.contact_set.all():
            c.is_company_contact = True if c.user == ouser.user else False
            c.save()

        try:
            self.owner.organization_user = ouser
            self.owner.save()
        except Exception:
            orgowner = OrganizationOwner.objects.create(organization_user=ouser,
                                                        organization=self)
            orgowner.save()
        return self.owner

    @cached_property
    def is_lab_processor(self):
        return self.has_role("Lab Processor")

    @cached_property
    def is_dealer(self):
        return self.has_role("Dealer")

    @cached_property
    def is_supplier(self):
        return self.has_role("Supplier")

    @cached_property
    def is_sampler(self):
        return self.has_role("Sampler")

    @cached_property
    def is_primary(self):
        return self.has_role("Primary")

    def has_role(self, role):
        if isinstance(role, str):
            # return role in list(self.roles.values_list('role', flat=True).all())
            return self.roles.filter(role=role).exists()
        return role in self.roles.all()

    @property
    def client_count(self):
        members = self.organization_users.all()
        counts = [m.clients.count() for m in members]
        return sum(counts)

    @property
    def member_count(self):
        return self.organization_users.count()

    @cached_property
    def get_owner(self):
        try:
            owner = self.owner
            return owner
        except:
            return None

    class Meta:
        verbose_name = _("Associated Organization")
        verbose_name_plural = _("Associated Organizations")


@receiver(m2m_changed, sender=Organization.roles.through)
def prevent_second_primary_organization(sender, instance, action, reverse, model, pk_set, **kwargs):
    if action != 'pre_add':
        return

    role = Role.objects.get(role="Primary")
    primaries = role.organization_set.all()
    if primaries.count() >= 1:
        if (not reverse and role.pk in pk_set) or (reverse and instance.pk == role.pk):
            raise ValidationError(_("Too many Primary Organizations - you can have a maximum of one"))


class OrganizationOwner(RefreshFromDbInvalidatesCachedPropertiesMixin, AbstractOrganizationOwner):

    @cached_property
    def get_full_name_and_user(self):
        return self.organization_user.get_full_name_and_user

    @cached_property
    def full_name(self):
        return self.organization_user.full_name

    @cached_property
    def email(self):
        return self.organization_user.email

    @cached_property
    def username(self):
        return self.organization_user.username

    @cached_property
    def last_login(self):
        return self.organization_user.last_login

    @cached_property
    def is_admin(self):
        return self.organization_user.is_admin


class OrganizationUser(RefreshFromDbInvalidatesCachedPropertiesMixin, AbstractOrganizationUser):
    class Meta:
        verbose_name = _("Associated Organization User")
        verbose_name_plural = _("Associated Organization Users")

    def delete(self, using=None):
        ## Set the company of the person to none and set the user to be inactive
        self.user.person.company = None
        self.user.person.save()
        self.user.is_active = False
        self.user.save()
        super().delete()
        # try:
        #     super().delete()
        #     self.user.person.delete()
        #     self.user.delete()
        # except ProtectedError:
        #     raise

    @cached_property
    def first_name(self):
        return self.user.first_name

    @cached_property
    def last_name(self):
        return self.user.last_name

    @cached_property
    def full_name(self):
        return self.user.get_full_name()

    @cached_property
    def email(self):
        return self.user.email

    @cached_property
    def username(self):
        return self.user.username

    @cached_property
    def is_owner(self):
        try:
            oo = self.organizationowner
            return True
        except:
            return False

    def last_login(self):
        return self.user.last_login.strftime("%Y-%m-%d %H:%M:%S")

    # def get_absolute_url(self):
    #     return reverse("associates_associatedorganizationuser_change", kwargs={'pk': self.pk})

    @cached_property
    def get_full_name_and_user(self):
        return '{} (user: {})'.format(self.full_name, self.username)

    def get_clients_link(self):
        pass
    #     if self.pk:  # if object has already been saved and has a primary key, show link to it
    #         print('in url create')
    #         url  = None
    #         try:
    #             url = reverse('admin:clients_client_changelist', kwargs={'organization': self.organization})
    #         except Exception as e:
    #             print('\n\n\n', e, '\n\n')
    #         return format_html("""<a href="{url}">{text}</a>""".format(
    #             url=url,
    #             text=_("Edit clients separately"),
    #         ))
    #     return _("(save and continue editing to create a clients link)")
    # get_clients_link.short_description = _("Clients link")
    # get_clients_link.allow_tags = True
