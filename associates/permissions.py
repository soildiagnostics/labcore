from rest_framework.permissions import BasePermission

from associates.models import OrganizationUser


class IsSuperUserOrIsOrgStaffUser(BasePermission):
    """
    Allows access only to authenticated super user or Organization user with staff status.
    """

    def has_permission(self, request, view):
        return bool(request.user.is_authenticated and
                    (request.user.is_superuser or
                    (request.user.is_staff and OrganizationUser.objects.filter(user=request.user).exists())))
