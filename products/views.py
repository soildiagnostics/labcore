import json

import channels
import requests
from asgiref.sync import async_to_sync
from django.conf import settings
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import TemplateView, View
from django.views.generic.list import ListView
from django.db.models import Q

# from django.contrib.auth.mixins import PermissionRequiredMixin
from associates.role_privileges import DealershipRequiredMixin
from .models import Category


# Create your views here.
class CategoryList(DealershipRequiredMixin, ListView):
    template_name = 'products/category_list.html'
    model = Category
    client_has_permission = getattr(settings, 'CLIENT_CAN_VIEW_PRODUCTS', False)

    def dispatch(self, *args, **kwargs):
        self.client_has_permission = getattr(settings, 'CLIENT_CAN_VIEW_PRODUCTS', False)
        return super(CategoryList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CategoryList, self).get_context_data(**kwargs)
        context['org'] = self.org
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.prefetch_related('product_set', 'product_set__prices', 'product_set__created_by')
        if self.request.user.is_staff:
            return qs.distinct()
        qs = qs.filter(Q(product__created_by=self.org)|Q(product__created_by__isnull=True), product__active=True)
        return qs.distinct()

class SVXProductsList(DealershipRequiredMixin, TemplateView):
    template_name = 'products/svx_list.html'

    def get_product_list_url(self, vendorbaseurl):
        if vendorbaseurl[-1] == "/":
            vendorbaseurl = vendorbaseurl[:-1]

        vendorurl = vendorbaseurl + reverse("api_products") + "?format=json"
        return vendorurl

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        vendor_url = settings.SOILDX_API_CENTRAL + reverse("api_svx_participants_list") + "?format=json"
        vendor_list = json.loads(requests.get(vendor_url).content)
        vendor_baseurls = [self.get_product_list_url(v['base_url']) for v in vendor_list]
        context['network'] = json.dumps(vendor_baseurls)
        context['vendors'] = vendor_list
        return context


class SVXProductsListJSON(View):

    def make_response(self, request):
        response = dict()
        draw = request.GET.get('draw')
        response['draw'] = int(draw) if draw else 1
        response['recordsTotal'] = 0
        response['recordsFiltered'] = 0
        response['data'] = dict()
        return response

    async def fetch_coroutine(session, url):
        with async_timeout.timeout(10):
            async with session.get(url) as response:
                return await response.text()



    async def main_download_loop(self, loop, urls):
        async with aiohttp.ClientSession(loop=loop) as session:
            for url in urls:
                await self.fetch_coroutine(session, url)


    def get(self, request, *args, **kwargs):

        vendor_url = settings.SOILDX_API_CENTRAL + reverse("api_participants_list") + "?format=json"
        vendor_list = json.loads(requests.get(vendor_url).content)
        vendor_baseurls = [v['base_url'] for v in vendor_list]
        vendor_urls = [self.get_product_list_url(v) for v in vendor_baseurls]
        print(vendor_urls)

        channel_layer = channels.layers.get_channel_layer()
        print(channel_layer)
        async_to_sync(channel_layer.send)("fetch",
                                          {
                                              "type": "fetch_urls",
                                              "urls": vendor_urls
                                          })

        return JsonResponse(self.make_response(request))
