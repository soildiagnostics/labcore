from associates.models import OrganizationUser
from products.models import Product, Package
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple


class ProductAdminForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.initial.get('created_by'):
            self.initial['created_by'] = self.created_by


class PackageAdminForm(forms.ModelForm):
    field_order = ('category', 'name', 'description', 'package', 'active')

    class Meta:
        model = Package
        exclude = ('image', 'slug', 'is_package', 'test_parameters',)


    def clean(self, commit=True):
        cleaned_data = self.cleaned_data
        package = cleaned_data.get('package')
        p = [str(d) for d in package if d.is_package]
        if p:
            raise forms.ValidationError(
                "Cannot put a package inside another package at this time. "
                "Offending package(s): {}".format(",".join(package))
            )
        if len(package) < 2:
            raise forms.ValidationError('Two or more products must be selected to form a Package.')
        return cleaned_data
