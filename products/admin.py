from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from django.db import models
from djmoney.forms.fields import MoneyField
from moneyed import CURRENCIES, DEFAULT_CURRENCY
from zinnia.models import Category as ArticleCategory
from zinnia.models import Entry

from associates.models import OrganizationUser
from customnotes.admin import CustomFieldInline
from products.forms import ProductAdminForm, PackageAdminForm
from products.models import Category, Price, Product, Discount, SaleUnit, \
    MeasurementUnit, TestParameter, EasyOrder, Package, \
    TestParameterRenderer, ParameterBreakpoint, BreakpointAbundance
from django_admin_listfilter_dropdown.filters import RelatedOnlyDropdownFilter


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'created', 'updated',)
    list_filter = ('name',)
    search_fields = ('name',)
    # autocomplete_fields = ('company',)
    autocomplete_search_fields = {
        "name": ['name'],
    }
    prepopulated_fields = {'slug': ('name',), }


class SaleUnitInLine(admin.TabularInline):
    model = SaleUnit
    extra = 0


admin.site.register(SaleUnit)

# Get the allowed currencies for the project from settings
DEFAULT_CURRENCIES = ('USD', 'EUR')
ALLOWED_CURRENCIES = getattr(settings, 'ALLOWED_CURRENCIES', DEFAULT_CURRENCIES)
DEFAULT_CURRENCY = getattr(settings, 'DEFAULT_CURRENCY', DEFAULT_CURRENCY)

CURRENCY_CHOICES = [(c.code, c.name) for i, c in CURRENCIES.items()
                    if c.code in ALLOWED_CURRENCIES]


class AmountField(MoneyField):
    def __init__(self, **kwargs):
        super().__init__(
            currency_choices=CURRENCY_CHOICES,
            default_currency=DEFAULT_CURRENCY,
            **kwargs
        )


class PriceInLineForm(forms.ModelForm):
    amount = AmountField()

    class Meta:
        model = Price
        exclude = ()


class PriceInline(GenericTabularInline):
    model = Price
    form = PriceInLineForm
    list_select_related = ('saleunit',)
    extra = 0

    inlines = [
        SaleUnitInLine,
    ]


class DiscountInline(GenericTabularInline):
    model = Discount
    list_select_related = ('dealer',)
    autocomplete_fields = ('dealer',)
    extra = 0


@admin.register(TestParameter)
class TestParameterAdmin(admin.ModelAdmin):
    model = TestParameter
    search_fields = ['name']
    list_display = ['name', 'calculated', 'index', 'unit', 'significant_digits']
    list_editable = ['index', 'unit', 'significant_digits']


admin.site.register(MeasurementUnit)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    form = ProductAdminForm
    save_as = True
    list_display = ('name', 'category', 'code', 'created', 'updated', 'created_by', 'active')
    list_filter = ('active', 'category',)
    search_fields = ('name',)
    list_select_related = ('category',
                           )
    autocomplete_fields = ('category',)
    filter_horizontal = ('test_parameters',)
    exclude = ('slug', 'is_package', 'package',)

    actions = ['create_articles_for_product']
    readonly_fields = ('svx_uuid',)

    inlines = [
        CustomFieldInline,
        PriceInline,
        DiscountInline,
    ]
    # autocomplete_search_fields = {
    #     "name": ['name'],
    # }
    # prepopulated_fields = {'slug': ('name',), }

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    fieldsets = (
        (None, {'fields': (
            'name', 'category', 'description', ('active', 'publish'), 'test_parameters', ('created_by', 'svx_uuid'))}),
        ('Other Product Settings',
         {'fields': ('report_template', 'report_order', 'color', 'image', 'code', 'product_page', 'protocol',),
          'classes': ('collapse',)}),
        # ('Information', {'fields': ('created_by', 'svx_uuid')})
    )

    def get_form(self, request, obj=None, **kwargs):
        org = OrganizationUser.objects.get(user=request.user).organization
        form = super().get_form(request, obj, **kwargs)
        form.created_by = org
        return form

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        org = OrganizationUser.objects.get(user=request.user).organization
        if not request.user.is_superuser:
            qs = Product.objects.get_available_products(org)
        return qs.filter(is_package=False)

    ### Admin actions on products

    def create_articles_for_product(self, request, queryset):
        parentcat, created = ArticleCategory.objects.get_or_create(title="Product Information",
                                                                   slug="product-information")
        infocat, created = ArticleCategory.objects.get_or_create(title="Description", parent=parentcat,
                                                                 description="Information about products",
                                                                 slug="product-description")
        protcat, created = ArticleCategory.objects.get_or_create(title="Protocols", parent=parentcat,
                                                                 description="Product fulfilment protocols",
                                                                 slug="product-protocols")

        for prod in queryset:
            e, created = Entry.objects.get_or_create(title=prod.name, slug=prod.slug)
            e.categories.add(infocat)
            e, created = Entry.objects.get_or_create(title="Protocol: {}".format(prod.name),
                                                     slug="protocol-{}".format(prod.slug))
            e.categories.add(protcat)

        self.message_user(request, "{} articles and protocol entries created for product(s)".format(len(queryset)))

    create_articles_for_product.short_description = "Create Article Entries for Products"


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    form = PackageAdminForm
    save_as = True
    list_display = ('name', 'category', 'code', 'created', 'updated', 'active')
    list_filter = ('active', 'category',)
    search_fields = ('name', 'package__name',)
    list_select_related = ('category',
                           )
    autocomplete_fields = ('category',)
    filter_horizontal = ('package',)
    exclude = ('slug', 'is_package', 'test_parameters',)

    actions = [ProductAdmin.create_articles_for_product]
    readonly_fields = ('created_by', 'svx_uuid')

    inlines = [
        CustomFieldInline,
        PriceInline,
        DiscountInline,
    ]

    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})}
    }

    fieldsets = (
        (None, {'fields': (
            'name', 'category', 'description', ('active', 'publish'), 'package', ('created_by', 'svx_uuid'))}),
        (
            'Other Package Settings',
            {'fields': ('report_template', 'report_order', 'color', 'image', 'code', 'product_page', 'protocol',),
             'classes': ('collapse',)}),
        # ('Information', {'fields': ('created_by', 'svx_uuid')})
    )

    def get_queryset(self, request):
        qs = Package.objects.all()
        org = OrganizationUser.objects.get(user=request.user).organization
        if not request.user.is_superuser:
            qs = Package.objects.get_available_products(org)
        return qs

    def get_form(self, request, obj=None, **kwargs):
        """
        We have to hack this get_form function to limit the Product choices
        in the Package creator to those available to the organization
        see: https://docs.djangoproject.com/en/3.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.get_form
        :param request:
        :param obj:
        :param kwargs:
        :return:
        """
        org = OrganizationUser.objects.get(user=request.user).organization
        form_class = super().get_form(request, obj, **kwargs)

        class NewForm(form_class):
            def __init__(self, *args, **kwargs):
                super(PackageAdminForm, self).__init__(*args, **kwargs)
                self.fields['package'].queryset = Product.objects.get_available_products(org)

        return NewForm


@admin.register(EasyOrder)
class EasyOrderAdmin(admin.ModelAdmin):
    list_display = ('product', 'icon')


@admin.register(BreakpointAbundance)
class BreakPointAbundanceAdmin(admin.ModelAdmin):
    list_display = ('name', 'color')
    list_editable = ('color',)


class ParameterBreakPointInlineAdmin(admin.TabularInline):
    model = ParameterBreakpoint
    extra = 0


@admin.register(TestParameterRenderer)
class TestParamterRendererAdmin(admin.ModelAdmin):
    inlines = [ParameterBreakPointInlineAdmin, ]


@admin.register(ParameterBreakpoint)
class ParameterBreakpointAdmin(admin.ModelAdmin):
    list_display = ('renderer', 'description', 'lower_limit', 'upper_limit')
    list_editable = ('lower_limit', 'upper_limit')
    list_filter = (('renderer__parameter', RelatedOnlyDropdownFilter),)
