# Generated by Django 2.0.7 on 2018-09-04 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_auto_20180904_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='package',
            field=models.ManyToManyField(blank=True, related_name='products', to='products.Product'),
        ),
    ]
