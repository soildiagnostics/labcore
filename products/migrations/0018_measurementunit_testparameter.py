# Generated by Django 2.1.1 on 2018-09-27 14:54

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_switch_org_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='MeasurementUnit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='TestParameter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('product', models.ManyToManyField(blank=True, to='products.Product')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='products.MeasurementUnit')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
    ]
