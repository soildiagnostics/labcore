# Generated by Django 2.1.3 on 2018-12-20 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_auto_20181105_0818'),
    ]

    operations = [
        migrations.AddField(
            model_name='testparameter',
            name='calculated',
            field=models.BooleanField(default=False, help_text='Is this parameter calculated from other parameters?'),
        ),
    ]
