import uuid

from dbtemplates.models import Template as DBTemplate
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.db.models.signals import m2m_changed
# uses django-tagging, which was installed with Zinnia any way.
# Create your models here.
from django.template.loader import get_template
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext as _
from djmoney.models.fields import MoneyField
from djmoney.money import Money
from tagging import registry

from associates.models import Organization
from colorfield.fields import ColorField
from customnotes.models import CustomField
import re


class Category(models.Model):
    name = models.CharField(max_length=150)
    slug = models.SlugField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    description = models.TextField(
        blank=True, help_text=_("Short description used on front page and other locations"))
    featured = models.BooleanField(default=True,
                                   help_text=_(
                                       "If True, this description will be displayed on the front page(s)"))
    document = models.FileField(upload_to="category/",
                                help_text="Attach a file here",
                                blank=True, null=True)
    article = models.URLField(blank=True, null=True)
    color = ColorField(default="#FF0000")

    class Meta:
        ordering = ('name',)
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.slug == "":
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def all_products_and_packages(self):
        return Product.objects.filter(category=self)


class SaleUnit(models.Model):
    '''
    Model for the Unit of Sale, e.g. per acre, per sample
    '''
    abbreviation = models.CharField(max_length=30,
                                    help_text=_(
                                        "Short version of the unit of sale, e.g. /ac for per acre"),
                                    )
    description = models.TextField(blank=True,
                                   help_text=_("Long version of the unit of sale, e.g. per acre"))

    class Meta:
        ordering = ('abbreviation',)
        verbose_name = _('Sale Unit')
        verbose_name_plural = _('Sale Units')

    def __str__(self):
        return self.abbreviation


class Price(models.Model):
    amount = MoneyField(max_digits=19, decimal_places=2,
                        default_currency="USD")
    saleunit = models.ForeignKey(SaleUnit, on_delete=models.CASCADE, null=True,
                                 help_text=_("Price per unit of sale"))
    # product = models.ForeignKey(
    #     'Product', related_name="price", null=True,
    #     on_delete=models.CASCADE)
    # generic item to apply the price to
    limit = models.Q(app_label='products', model='Product') | models.Q(
        app_label='products', model='Package')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = ('amount_currency', 'content_type',
                           'object_id', 'saleunit')

    def __str__(self):
        return "{} {} {}".format(self.amount.currency, self.amount.amount, self.saleunit)

    def save(self, *args, **kwargs):
        if self.saleunit == None:
            self.saleunit, created = SaleUnit.objects.get_or_create(
                abbreviation="per unknown")
        super().save(*args, **kwargs)  # Call the "real" save() method.


class BaseProduct(models.Model):
    category = models.ForeignKey(Category, null=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=100)
    code = models.CharField(
        max_length=20, blank=True,
        help_text=_("Billing / accounting codes"))
    slug = models.SlugField(blank=True,
                            help_text="Will be prepopulated in the admin page")
    description = models.TextField(blank=True, help_text=_(
        "Short description of the product. Please create a product page when possible"))
    product_page = models.URLField(
        blank=True, help_text=_("Product page on the blog"))
    active = models.BooleanField(default=True, help_text=_(
        "Do not delete products - just deactivate them when necessary"))
    stock = models.PositiveIntegerField(default=0,
                                        help_text=_("Inventory - NA for Service category items"))
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    protocol = models.URLField(
        blank=True, help_text=_("Protocol description on the blog"))

    report_template = models.ForeignKey(DBTemplate, blank=True, null=True, on_delete=models.PROTECT)
    image = models.ImageField(upload_to="product_images/", blank=True)
    color = ColorField(default="#FF0000")
    custom_fields = GenericRelation(CustomField)
    test_parameters = models.ManyToManyField("TestParameter", blank=True)

    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name

    def get_price_for_currency(self, currency):
        try:
            return self.prices.get(amount_currency=currency)
        except Exception:
            return None

    def set_price(self, amount, currency):
        try:
            p = self.prices.get(amount_currency=currency)
            p.amount = amount
            p.save()
            self.save()
        except ObjectDoesNotExist:
            p = Price(amount=Money(amount, currency),
                      item=self,
                      )
            p.save()
            self.prices.add(p)
            self.save()

    def save(self, *args, **kwargs):
        if self.slug == "":
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    # @property
    # def custom_fields(self):
    #     return self.custom_fields.all()


class ProductManager(models.Manager):

    # def get_queryset(self):
    #     return super(ProductManager, self).get_queryset().filter(
    #         is_package=False)

    def get_available_products(self, org):
        qs = self.get_queryset()
        if not org.is_primary:
            qs = qs.filter(Q(created_by=org) | Q(created_by__isnull=True))
        return qs.prefetch_related('custom_fields',
                                   "prices").select_related("category")


class Product(BaseProduct):
    package = models.ManyToManyField("Product", blank=True, related_name="products",
                                     verbose_name='products',
                                     help_text='Select the Products in this Package.')
    prices = GenericRelation(Price, related_query_name='products')
    created_by = models.ForeignKey(Organization, on_delete=models.PROTECT, null=True, blank=True,
                                   help_text=_(
                                       "Set to nobody if you want this product to be available to all dealers."))
    svx_uuid = models.UUIDField(default=uuid.uuid4, unique=True, null=True)
    publish = models.BooleanField(default=False, help_text=_(
        "Would you like this product to be available to the SoilDx Vendor Exchange?"
    ))
    is_package = models.BooleanField(default=False, null=True)
    report_order = models.PositiveSmallIntegerField(default=1)

    objects = ProductManager()

    def available_to(self, org):
        if self.created_by == org or self.created_by == None or org.is_primary:
            return True
        return False

    @property
    def is_vendored(self):
        return not self.created_by == Organization.objects.me

    @cached_property
    def discounts(self):
        ctype = ContentType.objects.get_for_model(self.__class__)
        discounts = Discount.objects.filter(content_type__pk=ctype.id, object_id=self.id)
        return discounts

    def discounted_price(self, price, org):
        try:
            discount = self.discounts.get(dealer=org)
            price.amount = discount.get_discounted_price_for_currency(price.amount.currency)
            return price
        except Exception:
            return price

    def all_discounted_prices(self, org):
        prices = self.prices.all()
        prices = [self.discounted_price(p, org) for p in prices]
        return prices

    def discounted_price_single(self, org):
        try:
            price = self.prices.first()
            return self.discounted_price(price, org)
        except Exception:
            return None

    # @property
    # def is_package(self):
    #     return self.package.count() != 0

    @property
    def has_test_parameters(self):
        return self.test_parameters.count() != 0

    def get_test_parameters(self):
        return self.test_parameters.all()

    @property
    def get_report_template(self):
        if self.report_template:
            return self.report_template

        # if self.is_package:
        #     return {'name': get_template("products/reports/generic_package.html")}

        if self.has_test_parameters:
            return {'name': get_template("products/reports/generic_table_example.html")}

        return {'name': get_template("products/reports/default_missing.html")}


#
# # On change of a Product package, handling changes using signal
# def product_package_items_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
#     " set test_parameters and is_package values on the Product model "
#     if action == 'post_add':
#         instance.is_package = True
#         instance.save()
#     elif action == 'post_remove' or 'post_clear':
#         if instance.package.count()==0:
#             instance.is_package = False
#             instance.save()
#     # disable test_parameter on change signal to prevent recursion
#     instance._skip_further_signal = True
# m2m_changed.connect(product_package_items_changed, sender=Product.package.through)

# On change of a Product package, handling changes using signal
def product_package_items_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
    " set test_parameters and is_package values on the Product model "
    if action == 'post_add':
        instance.is_package = True
        instance.save()
        test_parameters = TestParameter.objects.filter(product__in=list(pk_set))
        instance.test_parameters.set(test_parameters)
    elif action == 'post_remove' or 'post_clear':
        if instance.package.count() == 0:
            instance.is_package = False
            instance.save()
            instance.test_parameters.clear()
        else:
            test_parameters = TestParameter.objects.filter(product__in=instance.package.all())
            instance.test_parameters.set(test_parameters)
    # disable test_parameter on change signal to prevent recursion
    instance._skip_further_signal = True


m2m_changed.connect(product_package_items_changed, sender=Product.package.through)


# On change of a Product test_parameters, handling changes to any parent packages using signal
def product_test_parameter_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
    " Update test_parameters on packages when child products are changed "
    if getattr(instance, '_skip_further_signal', False):
        return None
    # check if this product is part of any packages, and if so, reset these package test_parameters
    if instance.products.count() > 0:
        for pack in instance.products.all():
            if action in ('post_add', 'post_remove', 'post_clear'):
                test_parameters = TestParameter.objects.filter(product__in=pack.package.all())
                pack.test_parameters.set(test_parameters)
    instance._skip_further_signal = False


m2m_changed.connect(product_test_parameter_changed, sender=Product.test_parameters.through)


# class Package(BaseProduct):
#
#     products = models.ManyToManyField(Product, related_name="products")
#     prices = GenericRelation(Price, related_query_name='packages')
#
#     class Meta:
#         ordering = ('name', )
#         verbose_name = _('Package')
#         verbose_name_plural = _('Packages')

class PackageManager(ProductManager):

    def get_queryset(self):
        return super(PackageManager, self).get_queryset().filter(
            is_package=True).prefetch_related('package', 'package__custom_fields',
                                              "prices").select_related("category")


class Package(Product):
    objects = PackageManager()

    class Meta:
        proxy = True
        ordering = ('name',)
        verbose_name = _('Product Package')
        verbose_name_plural = _('Product Packages')


class Discount(models.Model):
    code = models.CharField(max_length=20, default="DISCOUNT")
    # limit this to those with Role = Dealer
    dealer = models.ForeignKey(
        Organization, on_delete=models.CASCADE,
        limit_choices_to={'roles__role': 'Dealer'},
    )
    discount = models.DecimalField(
        max_digits=5, decimal_places=2, help_text=_("Percent discount"))
    # generic item to apply the discount to
    limit = models.Q(app_label='products', model='Product') | models.Q(
        app_label='products', model='Package')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = ('dealer', 'content_type', 'object_id')

    def get_discounted_price_for_currency(self, currency):
        p = self.content_object.get_price_for_currency(currency)
        if p:
            return p.amount * (100 - self.discount) / 100
        else:
            return None


# registering these models for tagging
registry.register(Product)


# registry.register(Package)


class MeasurementUnit(models.Model):
    unit = models.CharField(max_length=20)

    def __str__(self):
        if self.unit == "% base saturation":
            return "% sat."
        return self.unit


class TestParameterManager(models.Manager):

    def get_parameter_by_string(self, parstring):
        regex = re.compile("(?P<parameter>.+)_(?P<unit>.+)")
        try:
            pardict = regex.match(parstring).groupdict()
            return TestParameter.objects.get(name=pardict.get("parameter"), unit__unit=pardict.get("unit"))
        except Exception:
            try:
                return TestParameter.objects.filter(name=parstring).first()
            except Exception:
                return None


class TestParameter(models.Model):
    name = models.CharField(max_length=50)
    unit = models.ForeignKey(MeasurementUnit, on_delete=models.PROTECT)
    significant_digits = models.IntegerField(default=1)
    index = models.IntegerField(default=999,
                                help_text=_("Order in which you want parameters to appear in a report"))
    calculated = models.BooleanField(default=False, help_text=_("Is this parameter calculated from other parameters?"))
    measurement_protocol = models.CharField(max_length=50, blank=True, null=True)
    objects = TestParameterManager()

    def __str__(self):
        return "{} {}".format(self.name, self.unit)

    class Meta:
        ordering = ['index', 'name']
        indexes = [
            models.Index(fields=['index', 'name']),
        ]


class EasyOrderManager(models.Manager):

    def get_easy_links(self, base_url):
        easy_products = EasyOrder.objects.filter(product__active=True)
        easy_link = "<a class='pull-right' href='{0}?product={1}'><i title='{3}' class='fa {2}' ></i>&nbsp</a>"
        links = [easy_link.format(base_url, eo.product_id, eo.icon, eo.product.name) for eo in easy_products]
        links.append(
            "<a class='pull-right' href='{0}'><i title='New Order' class='fa fa-chart-line'></i>&nbsp</a>".format(
                base_url))
        return " ".join(links)


class EasyOrder(models.Model):
    product = models.OneToOneField(Product, on_delete=models.PROTECT)
    icon = models.CharField(max_length=32, help_text=_(
        "This is a font-awesome icon class that will be used to render the button, e.g. 'fa-chart'"))
    objects = EasyOrderManager()


## Test Parameter Renderer for soil parameters
DEFAULT_YG_COLOR = """nv 0 0 0 0
    5.5% 255 255 229 255 
    16.66% 247 252 185 255
    27.77% 217 240 163 255
    38.88% 173 221 142 255
    50% 120 198 121 255
    61.11% 65 171 93 255
    72.22% 35 132 67 255
    83.33% 0 104 55 255
    94.44% 0 69 41 255"""


class TestParameterRendererManager(models.Manager):

    def get_colorfile_for_string(self, parstring):

        try:
            par = TestParameter.objects.get_parameter_by_string(parstring)
            return self.get_colorfile(par)
        except Exception as e:
            return DEFAULT_YG_COLOR

    def get_colorfile(self, parameter):
        tpr = parameter.renderer
        breaks = tpr.breakpoints.all().order_by('upper_limit')
        # check if all upper limits are non-none.
        nonebreaks = breaks.filter(upper_limit__isnull=True)
        if nonebreaks.count() > 0:
            ## Let's just return the default YG color scheme by percentiles
            return DEFAULT_YG_COLOR

        colorscheme = ["nv 0 0 0 0"]
        for idx, bk in enumerate(breaks):
            if idx == 0:
                val = bk.upper_limit / 2
            else:
                val = (bk.upper_limit + breaks[idx - 1].upper_limit) / 2
            clr = bk.description.color.lstrip("#")
            r, g, b = tuple(int(clr[i:i + 2], 16) for i in (0, 2, 4))
            colorscheme.append(f"{val} {r} {g} {b} 255")
        return "\n".join(colorscheme)


class TestParameterRenderer(models.Model):
    parameter = models.OneToOneField(TestParameter, on_delete=models.PROTECT, related_name="renderer")
    objects = TestParameterRendererManager()

    def get_color(self, value):
        gtes = self.breakpoints.filter(upper_limit__gt=value).order_by('upper_limit')

        return gtes.first()

    @property
    def legend(self):
        breaks = self.breakpoints.all().order_by('upper_limit').values_list('description__color',
                                                                            'description__name',
                                                                            'upper_limit')
        ## check for completeness
        vals = [x for a, b, x in breaks]
        if all(vals):
            return list(breaks)
        return None

    def __str__(self):
        return f"Renderer for {self.parameter}"


class BreakpointAbundance(models.Model):
    name = models.CharField(max_length=20)
    color = ColorField(default="#FF0000")

    def __str__(self):
        return self.name


class ParameterBreakpoint(models.Model):
    renderer = models.ForeignKey(TestParameterRenderer, on_delete=models.PROTECT, related_name='breakpoints')
    description = models.ForeignKey(BreakpointAbundance, on_delete=models.PROTECT)
    lower_limit = models.FloatField(blank=True, null=True, help_text=_("Inclusive, i.e. >= used for comparison"))
    upper_limit = models.FloatField(blank=True, null=True, help_text=_("Exclusive, i.e. < used for comparison"))

    def __str__(self):
        return f"{self.renderer}, {self.description}"
