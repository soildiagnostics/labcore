from django import template

register = template.Library()

@register.filter
def discount(price, org):
    return price.item.discounted_price(price, org)

@register.filter
def quantity(product, order):
    return order.get_quantity_for_product(product)

@register.filter
def subtotal(product, order):
    return order.subtotal_for_product(product)

@register.filter
def order_total(order):
    return order.total()

@register.filter()
def available_to(product, org):
    return product.available_to(org)