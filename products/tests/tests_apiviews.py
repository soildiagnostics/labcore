import json

from django.contrib.auth.models import Group
from django.test.testcases import TestCase
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from contact.models import User
from products.tests.tests_serializers import ProductSerializerTest


class ProductAPITestCase(TestCase):

    fixtures = ['roles']

    def setUp(self):
        ProductSerializerTest.setUp(self)
        g = Group.objects.create(name="ProductAPI")
        user = User.objects.get(username="testinguser")
        g.user_set.add(user)
        self.assertTrue(user.groups.filter(name="ProductAPI").exists())

        user.set_password('testpass')
        user.save()
        token, _ = Token.objects.get_or_create(user=user)

        self.client = APIClient()
        self.client.login(username='testinguser', password='testpass')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        
        self.product_dict = {
            "id": 57,
            "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
            "vendor_product_id": "aa8702b7-6db7-465f-a86d-822cbd5401a3",
            "created_by": "GMS Lab",
            "name": "Fertility Testing",
            "active": True,
            "publish": True,
            "prices": [
                {
                    "amount": "5.00",
                    "saleunit": {
                        "id": 3,
                        "abbreviation": "each",
                        "description": "each"
                    },
                    "string": "USD 5.00 each",
                    "currency": "USD"
                }
            ],
            "description": "Testing by the acre",
            "product_page": "",
            "stock": 0,
            "category": {
                "id": 1,
                "name": "Fertility",
                "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
            },
            "test_parameters": [
                {
                    "name": "B",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "H",
                    "unit": "%",
                    "calculated": False
                },
                {
                    "name": "Mn",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "Na",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "P",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "Zn",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "CEC",
                    "unit": "meq/100g",
                    "calculated": True
                },
                {
                    "name": "Cu",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "Fe",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "K",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "Mg",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "S",
                    "unit": "ppm",
                    "calculated": False
                }
            ],
            "package": [],
            "custom_fields": [
                {
                    "id": 8,
                    "name": "Rogo-sampler?",
                    "help_text": "Would you like this field sampled by Rogo?"
                },
                {
                    "id": 7,
                    "name": "Harvested?",
                    "help_text": "Has the field been harvested and is it ready to sample?"
                }
            ],
            "is_package": False
        }
        self.package_dict = {
            "id": 14,
            "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
            "vendor_product_id": "a836c558-e6ec-48b7-bc54-3f8ab39f5276",
            "created_by": "GMS Lab",
            "name": "Basic 1.5 grid",
            "active": True,
            "publish": True,
            "prices": [
                {
                    "amount": "2.00",
                    "saleunit": {
                        "id": 1,
                        "abbreviation": "/ac",
                        "description": "per acre"
                    },
                    "string": "USD 2.0000 /ac",
                    "currency": "USD"
                }
            ],
            "description": "Basic - 1.5 grid",
            "product_page": "",
            "stock": 0,
            "category": {
                "id": 1,
                "name": "Fertility",
                "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
            },
            "test_parameters": [],
            "package": [
                {
                    "id": 48,
                    "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
                    "vendor_product_id": "867ae9ff-8574-4ee6-a9a1-9edbfab6b819",
                    "created_by": "GMS Lab",
                    "name": "Boundary maps",
                    "active": True,
                    "publish": True,
                    "prices": [
                        {
                            "amount": "1.00",
                            "saleunit": {
                                "id": 1,
                                "abbreviation": "/ac",
                                "description": "per acre"
                            },
                            "string": "USD 1.00 /ac",
                            "currency": "USD"
                        }
                    ],
                    "description": "",
                    "product_page": "",
                    "stock": 0,
                    "category": {
                        "id": 1,
                        "name": "Fertility",
                        "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
                    },
                    "test_parameters": [],
                    "custom_fields": []
                }
            ],
            "custom_fields": [
                {
                    "id": 1,
                    "name": "Field Ready?",
                    "help_text": "yes or no"
                }
            ],
            "is_package": True
        }



    def test_publish_product_to_svx(self):

        url = reverse('api_publish_product')
        response = self.client.post(url, self.product_dict, format="json")
        self.assertEqual(response.status_code, 201)

    def test_publish_package_to_svx(self):

        url = reverse('api_publish_product')
        response = self.client.post(url, self.package_dict, format="json")
        self.assertEqual(response.status_code, 201)

    def test_list_products(self):
        url = reverse('api_publish_product')
        self.client.post(url, self.product_dict, format="json")
        self.client.post(url, self.package_dict, format="json")

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 3)

    def test_retrieve_product(self):
        url = reverse('api_publish_product')
        response = self.client.post(url, self.product_dict, format="json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['prices'][0]["string"], "USD 5.00 each")

        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "aa8702b7-6db7-465f-a86d-822cbd5401a3"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_delete_product(self):
        url = reverse('api_publish_product')
        response = self.client.post(url, self.product_dict, format="json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "aa8702b7-6db7-465f-a86d-822cbd5401a3"})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)

    def test_retrieve_package(self):
        url = reverse('api_publish_product')
        response = self.client.post(url, self.package_dict, format="json")
        self.assertEqual(response.status_code, 201)

        self.assertEqual(json.loads(response.content)['prices'][0]["string"], "USD 2.00 /ac")

        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "a836c558-e6ec-48b7-bc54-3f8ab39f5276"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_delete_package(self):
        url = reverse('api_publish_product')
        response = self.client.post(url, self.package_dict, format="json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "a836c558-e6ec-48b7-bc54-3f8ab39f5276"})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)

        response = self.client.get(reverse('api_publish_product'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)
        ## Only package is deleted, not products inside it.

    def test_retrieve_package_login_required(self):
        url = reverse('api_publish_product')
        response = self.client.post(url, self.package_dict, format="json")
        self.assertEqual(response.status_code, 201)

        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "a836c558-e6ec-48b7-bc54-3f8ab39f5276"})

        self.client.logout()
        url = reverse('api_retreive_delete_product', kwargs={'svx_uuid': "a836c558-e6ec-48b7-bc54-3f8ab39f5276"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 401)

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 401)

        response = self.client.get(reverse('api_publish_product'))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(len(json.loads(response.content)), 1)
        ## Only package is deleted, not products inside it.




