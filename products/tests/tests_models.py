from django.db import IntegrityError
from django.template import Template, Context
from django.test import Client
from django.test import TestCase
# from django.contrib.auth.models import User
from django.urls import reverse
from djmoney.money import Money

from associates.models import Organization
from associates.models import OrganizationUser
from contact.models import Person, Company
from contact.models import User
from contact.tests.tests_models import ContactTestCase
from products.models import (Category, Price, Product, Discount, TestParameter, MeasurementUnit,
                             Package)


# Create your tests here.


class CategoryProductTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.orguser = OrganizationUser.objects.get(user=u)
        Category.objects.create(name="Soil Test")
        Product.objects.create(name="FertiSaver-N", created_by=self.orguser.organization)
        # Price.objects.create(amount=Money(49.99, 'USD'),
        #                      item=p)

    def test_slugify(self):
        c = Category.objects.get(name="Soil Test")
        self.assertEqual(str(c), "Soil Test")
        self.assertEqual(c.slug, "soil-test")

        c.slug = "soiltest"
        c.save()

        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(str(p), "FertiSaver-N")
        self.assertEqual(p.slug, "fertisaver-n")

    def test_set_price(self):
        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(len(p.prices.all()), 0)
        self.assertEqual(p.get_price_for_currency('INR'), None)
        p.set_price(1, 'USD')

        self.assertEqual(len(p.prices.all()), 1)
        price = p.prices.all()[0]
        self.assertEqual(str(price), "USD 1.00 per unknown")
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(1, 'USD'))

    def test_update_price(self):
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(1, 'USD')
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(1, 'USD'))

        p.set_price(2, 'USD')
        self.assertEqual(p.get_price_for_currency('USD').amount,
                         Money(2, 'USD'))

    def test_product_tags(self):
        p = Product.objects.get(name="FertiSaver-N")
        self.assertEqual(p.tags.count(), 0)
        p.tags = ["nitrogen"]
        self.assertEqual(p.tags.count(), 1)

    def test_package(self):
        pack = Package.objects.create(name="FSN", created_by=self.orguser.organization)
        p = Product.objects.get(name="FertiSaver-N")
        p2 = Product.objects.create(name="LOI", created_by=self.orguser.organization)
        p3 = Product.objects.create(name="pH", created_by=self.orguser.organization)

        tp = TestParameter.objects.create(
            name='pH',
            unit=MeasurementUnit.objects.create(unit='no unit'),
            significant_digits=1,
            index=1,
            calculated=False
        )
        p3.test_parameters.add(tp)
        pack.package.add(p, p2, p3)

        self.assertEqual(pack.name, "FSN")
        self.assertEqual(pack.package.all().count(), 3)
        self.assertEqual(pack.is_package, True)
        self.assertEqual(pack.test_parameters.all().count(), 1)

        pack.package.remove(p, p2, p3)
        self.assertEqual(pack.package.all().count(), 0)
        self.assertEqual(pack.is_package, False)
        self.assertEqual(pack.test_parameters.all().count(), 0)

        # test that removing test_parameters is saved correctly to the parent product/package record
        pack.package.add(p, p2, p3)
        self.assertEqual(pack.test_parameters.all().count(), 1)
        self.assertEqual(pack.is_package, True)
        p3.test_parameters.clear()
        self.assertEqual(p3.test_parameters.count(), 0)
        self.assertEqual(pack.test_parameters.all().count(), 0)
        p3.test_parameters.add(tp)
        self.assertEqual(pack.test_parameters.all().count(), 1)
        pack.package.clear()
        self.assertEqual(pack.test_parameters.all().count(), 0)
        self.assertEqual(pack.is_package, False)

    def test_price_uniqueness(self):
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(1, 'USD')

        with self.assertRaises(IntegrityError):
            Price.objects.create(amount=Money(2, 'USD'), item=p)

    def test_create_article_entries(self):
        c = Client()
        u = User.objects.create_superuser('myuser', 'myemail@test.com', 'password')
        self.orguser.organization.add_user(u)
        c.login(username="myuser", password="password")
        change_url = reverse('admin:products_product_changelist')
        products = Product.objects.all()
        data = {'action': 'create_articles_for_product',
                '_selected_action': [p.pk for p in products],
                }
        response = c.post(change_url, data)
        self.assertTrue(response.status_code, 302)
        self.assertTrue(response.url, change_url)


class DiscountTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        CategoryProductTest.setUp(self)
        Category.objects.create(name="Soil Test")
        Company.objects.create(name="Sky2")

    def test_create_discount(self):
        c = Company.objects.get(name="Sky2")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        d = Discount.objects.create(dealer=a,
                                    discount=11.2,
                                    content_object=p)

        self.assertNotEqual(d.id, None)

    def test_create_unique_discount(self):
        c = Company.objects.get(name__startswith="Sky2")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        Discount.objects.create(dealer=a,
                                discount=11.2,
                                content_object=p)
        with self.assertRaises(IntegrityError):
            Discount.objects.create(dealer=a,
                                    discount=12.2,
                                    content_object=p)

    def test_discounted_price(self):
        c = Company.objects.get(name__startswith="Sky2")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)
        v = d.get_discounted_price_for_currency('INR')
        self.assertEqual(v, None)

        v = d.get_discounted_price_for_currency('USD')
        self.assertEqual(v, Money(90, 'USD'))

        self.assertEqual(p.discounts.count(), 1)

        price = p.prices.first()

        self.assertEqual(p.discounted_price(price, a).amount, Money(90, "USD"))

    def test_base_price(self):
        c = Company.objects.get(name__startswith="Sky2")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)
        price = p.prices.first()
        c2 = Company.objects.create(name="soils")
        a2, created = Organization.objects.create_dealership(
            company=c2)
        self.assertEqual(p.discounted_price(price, a2).amount, Money(100, "USD"))

    def test_add_parameter(self):
        p = Product.objects.get(name="FertiSaver-N")
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=1)

        self.assertEqual(str(u), "ppm")
        self.assertEqual(str(par), "Organic N ppm")

        p.test_parameters.add(par)
        self.assertEqual(len(p.get_test_parameters()), 1)


class TemplateFilterTestCase(TestCase):
    TEMPLATE = Template(
        """
        {% load product_extras %} 
        {{ cat }}
        {{ org }} 
        {% for prod in cat.product_set.all %}
        <li>
        {% if prod.active %}
            {{ prod.name }}
            {% for price in prod.prices.all %}
            <span class="pull-right">{{ price|discount:org }}</span>
            {% endfor %}
            {% endif %}
        </li>
        {% endfor %}
        """
    )
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        Category.objects.create(name="Soil Test")
        cat = Category.objects.get(name="Soil Test")
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        Product.objects.create(name="FertiSaver-N", category=cat, created_by=a)
        self.assertEqual(created, True)
        p = Product.objects.get(name="FertiSaver-N")
        p.set_price(100, 'USD')
        d = Discount.objects.create(dealer=a,
                                    discount=10,
                                    content_object=p)

    def test_discount_filter(self):
        context = {}
        context['cat'] = Category.objects.get(name="Soil Test")
        aorg = Organization.objects.get(company__name__startswith="Sky")
        context['org'] = aorg

        rendered = self.TEMPLATE.render(Context(context))
        self.assertIn("USD 90.0000 per unknown", rendered)


class ProductsViewsTestCase(TestCase):
    fixtures = ['roles']

    def setUp(self):
        tc = TemplateFilterTestCase()
        tc.setUp()

    def test_product_list_view(self):
        c = Client()
        u = User.objects.get(username="testinguser")
        company = Company.objects.get(name__startswith="Sky")
        u.person.company = company
        u.person.save()
        Organization.objects.create_dealership(company)

        c.force_login(u)

        resp = c.get(reverse("category_list"))
        self.assertEqual(resp.status_code, 200)
