from django.test import TestCase

from products.models import Product, MeasurementUnit, TestParameter, ParameterBreakpoint, TestParameterRenderer
from products.tests.tests_models import TemplateFilterTestCase
from django.core import management


class RendererModelTestCase(TestCase):
    fixtures = ['roles', 'abundances']

    def setUp(self):
        tc = TemplateFilterTestCase()
        tc.setUp()
        p = Product.objects.get(name="FertiSaver-N")
        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=1)
        management.call_command('create_soiltest_renderers')

    def test_set_limits(self):
        parameter = TestParameter.objects.get(name="Organic N")
        ample = ParameterBreakpoint.objects.filter(description__name="Ample").first()
        ample.lower_limit = 5.0
        ample.save()
        self.assertEqual(ample.renderer.parameter, parameter)

    def test_color_accuracy(self):
        parameter = TestParameter.objects.get(name="Organic N")
        self.assertIsNone(parameter.renderer.get_color(0))

        breakpoints = parameter.renderer.breakpoints.all().order_by('pk')
        breaks = [
            (None, 50),
            (None, 100),
            (None, 200),
            (None, 300),
            (None, 10e6)
        ]

        breakszip = zip(list(breakpoints), breaks)

        for (breakpoint, (lower, upper)) in breakszip:
            breakpoint.lower_limit = lower
            breakpoint.upper_limit = upper
            breakpoint.save()

        self.assertEqual(parameter.renderer.get_color(0).description.color, '#C0392B')
        self.assertEqual(parameter.renderer.get_color(10).description.color, '#C0392B')
        self.assertEqual(parameter.renderer.get_color(50).description.color, '#FFD700')
        self.assertEqual(parameter.renderer.get_color(70).description.color, '#FFD700')
        self.assertEqual(parameter.renderer.get_color(100).description.color, '#2E8B57')
        self.assertEqual(parameter.renderer.get_color(110).description.color, '#2E8B57')
        self.assertEqual(parameter.renderer.get_color(200).description.color, '#4169E1')
        self.assertEqual(parameter.renderer.get_color(300).description.color, '#9370DB')
        self.assertEqual(parameter.renderer.get_color(3000).description.color, '#9370DB')

    def test_color_scheme(self):
        parameter = TestParameter.objects.get(name="Organic N")
        self.assertIsNone(parameter.renderer.get_color(0))

        breakpoints = parameter.renderer.breakpoints.all().order_by('pk')
        breaks = [
            (None, None),
            (None, None),
            (None, None),
            (None, None),
            (None, None)
        ]

        breakszip = zip(list(breakpoints), breaks)

        for (breakpoint, (lower, upper)) in breakszip:
            breakpoint.lower_limit = lower
            breakpoint.upper_limit = upper
            breakpoint.save()

        clr = TestParameterRenderer.objects.get_colorfile(parameter)
        self.assertEqual(clr, """nv 0 0 0 0
    5.5% 255 255 229 255 
    16.66% 247 252 185 255
    27.77% 217 240 163 255
    38.88% 173 221 142 255
    50% 120 198 121 255
    61.11% 65 171 93 255
    72.22% 35 132 67 255
    83.33% 0 104 55 255
    94.44% 0 69 41 255""")

        breaks = [
            (None, 50),
            (None, 100),
            (None, 200),
            (None, 300),
            (None, 10e6)
        ]

        breakszip = zip(list(breakpoints), breaks)

        for (breakpoint, (lower, upper)) in breakszip:
            breakpoint.lower_limit = lower
            breakpoint.upper_limit = upper
            breakpoint.save()

        clr = TestParameterRenderer.objects.get_colorfile(parameter)
        self.assertEqual(clr, """nv 0 0 0 0
25.0 192 57 43 255
75.0 255 215 0 255
150.0 46 139 87 255
250.0 65 105 225 255
5000150.0 147 112 219 255""")

    def test_color_legend(self):
        parameter = TestParameter.objects.get(name="Organic N")
        self.assertIsNone(parameter.renderer.get_color(0))

        breakpoints = parameter.renderer.breakpoints.all().order_by('pk')
        breaks = [
            (None, 50),
            (None, 100),
            (None, 200),
            (None, 300),
            (None, 10e6)
        ]

        breakszip = zip(list(breakpoints), breaks)

        for (breakpoint, (lower, upper)) in breakszip:
            breakpoint.lower_limit = lower
            breakpoint.upper_limit = upper
            breakpoint.save()

        self.assertEqual(parameter.renderer.legend,
                         [('#C0392B', 'Deficient', 50.0),
                          ('#FFD700', 'Tolerable', 100.0),
                          ('#2E8B57', 'Optimal', 200.0),
                          ('#4169E1', 'Ample', 300.0),
                          ('#9370DB', 'Excessive', 10000000.0)])
