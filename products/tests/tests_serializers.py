from django.test import TestCase

from associates.models import Organization, Role
# from django.contrib.auth.models import User
from associates.models import OrganizationUser
from contact.models import Person, Company
from contact.models import User
from contact.tests.tests_models import ContactTestCase
from customnotes.models import CustomField
from products.serializers import *


# Create your tests here.


class ProductSerializerTest(TestCase):
    fixtures = ['roles']
    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        a.roles.add(Role.objects.get(role="Primary"))
        self.dlr = a
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.orguser = OrganizationUser.objects.get(user=u)
        Category.objects.create(name="Soil Test", description='description')
        prod = Product.objects.create(name="FertiSaver-N", created_by=self.orguser.organization)
        Price.objects.create(amount=Money(49.99, 'USD'),
                             item=prod)

        u, created = MeasurementUnit.objects.get_or_create(unit="ppm")
        par, created = TestParameter.objects.get_or_create(name="Organic N", unit=u, index=1)

        prod.test_parameters.add(par)
        CustomField.objects.create(name="Custom field",
                                   help_text="Custom help",
                                   content_object=prod)

        newcompany = Company.objects.create(name="GMS Lab")
        neworg = Organization.objects.create(name="GMS Lab", company=newcompany)
        SVXParticipant.objects.create(
            id="778dcce8-7b2f-4074-ae86-fb47d874b4d3",
            organization=neworg
        )

    def test_category_serializer(self):
        cat = Category.objects.first()
        ser = CategorySerializer(instance=cat).data
        self.assertEqual(ser['name'], "Soil Test")
        self.assertEqual(ser['description'], "description")

    def test_measurement_unit_serializer(self):
        m = MeasurementUnit.objects.first()
        ser = MeasurementUnitSerializer(instance=m).data
        self.assertEqual(m.unit, ser['unit'])

    def test_testparameter_serializer(self):
        tp = TestParameter.objects.first()
        ser = TestParameterSerializer(instance=tp).data
        self.assertEqual(ser['name'], tp.name)
        self.assertEqual(ser['unit'], tp.unit.unit)

    def test_testparameter_serializer_create(self):
        validated_dict = {
            'name': 'LOI',
            'unit': '%'
        }

        ser = TestParameterSerializer(data=validated_dict)
        self.assertEqual(ser.is_valid(), True)
        ser.save()

        tp = TestParameter.objects.get(name="LOI")
        self.assertNotEqual(tp.pk, None)

    def test_priceserializer(self):
        prod = Product.objects.first()
        price = prod.prices.all()
        ser = PriceSerializer(instance=price, many=True).data
        self.assertEqual(ser[0]['string'], 'USD 49.99 per unknown')

    def test_productserializer(self):
        prod = Product.objects.first()
        ser = ProductSerializer(instance=prod).data
        self.assertEqual(ser['prices'][0]['string'], 'USD 49.99 per unknown')
        ser = ProductSerializer(instance=prod, context={'dealer': self.dlr}).data
        self.assertEqual(ser['prices'][0]['string'], 'USD 49.99 per unknown')

    def test_productserializer_create(self):
        data_dict = {
            "id": 57,
            "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
            "vendor_product_id": "aa8702b7-6db7-465f-a86d-822cbd5401a3",
            "created_by": "GMS Lab",
            "name": "Fertility Testing",
            "publish": True,
            "active": True,
            "prices": [
                {
                    "amount": "5.00",
                    "saleunit": {
                        "id": 3,
                        "abbreviation": "each",
                        "description": "each"
                    },
                    "string": "USD 5.00 each",
                    "currency": "USD"
                }
            ],
            "description": "Testing by the acre",
            "product_page": "",
            "stock": 0,
            "category": {
                "id": 1,
                "name": "Fertility",
                "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
            },
            "test_parameters": [
                {
                    "name": "B",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "H",
                    "unit": "%",
                    "calculated": False
                },
                {
                    "name": "Mn",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "Na",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "P",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "Zn",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "CEC",
                    "unit": "meq/100g",
                    "calculated": True
                },
                {
                    "name": "Cu",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "Fe",
                    "unit": "ppm",
                    "calculated": False
                },
                {
                    "name": "K",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "Mg",
                    "unit": "lb/ac",
                    "calculated": False
                },
                {
                    "name": "S",
                    "unit": "ppm",
                    "calculated": False
                }
            ],
            "package": [],
            "custom_fields": [
                {
                    "id": 8,
                    "name": "Rogo-sampler?",
                    "help_text": "Would you like this field sampled by Rogo?"
                },
                {
                    "id": 7,
                    "name": "Harvested?",
                    "help_text": "Has the field been harvested and is it ready to sample?"
                }
            ],
            "is_package": False
        }

        ser = ProductSerializer(data=data_dict)
        ser.is_valid()
        self.assertTrue(ser.is_valid())
        prod = ser.save()
        self.assertEqual(prod.name, "Fertility Testing")
        org = SVXParticipant.objects.get(id="778dcce8-7b2f-4074-ae86-fb47d874b4d3")
        self.assertEqual(prod.created_by, org.organization)
        self.assertTrue(prod.is_vendored)
        self.assertEqual(prod.test_parameters.count(), 12)

    def test_packageserializer_create(self):
        data_dict = {
            "id": 14,
            "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
            "vendor_product_id": "a836c558-e6ec-48b7-bc54-3f8ab39f5276",
            "created_by": "GMS Lab",
            "name": "Basic 1.5 grid",
            "publish": True,
            "active": True,
            "prices": [
                {
                    "amount": "2.00",
                    "saleunit": {
                        "id": 1,
                        "abbreviation": "/ac",
                        "description": "per acre"
                    },
                    "string": "USD 2.0000 /ac",
                    "currency": "USD"
                }
            ],
            "description": "Basic - 1.5 grid",
            "product_page": "",
            "stock": 0,
            "category": {
                "id": 1,
                "name": "Fertility",
                "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
            },
            "test_parameters": [],
            "package": [
                {
                    "id": 48,
                    "vendor_svx_id": "778dcce8-7b2f-4074-ae86-fb47d874b4d3",
                    "vendor_product_id": "867ae9ff-8574-4ee6-a9a1-9edbfab6b819",
                    "created_by": "GMS Lab",
                    "name": "Boundary maps",
                    "publish": True,
                    "active": True,
                    "prices": [
                        {
                            "amount": "1.00",
                            "saleunit": {
                                "id": 1,
                                "abbreviation": "/ac",
                                "description": "per acre"
                            },
                            "string": "USD 1.00 /ac",
                            "currency": "USD"
                        }
                    ],
                    "description": "",
                    "product_page": "",
                    "stock": 0,
                    "category": {
                        "id": 1,
                        "name": "Fertility",
                        "description": "To grow a bumper crop good fertility is vital. At GMS Lab we put our emphasis on providing you the most accurate results possible."
                    },
                    "test_parameters": [],
                    "custom_fields": []
                }
            ],
            "custom_fields": [
                {
                    "id": 1,
                    "name": "Field Ready?",
                    "help_text": "yes or no"
                }
            ],
            "is_package": True
        }

        ser = NestedProductSerializer(data=data_dict)
        ser.is_valid()
        self.assertTrue(ser.is_valid())
        prod = ser.save()
        self.assertEqual(prod.name, "Basic 1.5 grid")
        org = SVXParticipant.objects.get(id="778dcce8-7b2f-4074-ae86-fb47d874b4d3")
        self.assertEqual(prod.created_by, org.organization)

        maps = Product.objects.get(svx_uuid="867ae9ff-8574-4ee6-a9a1-9edbfab6b819")
        self.assertEqual(maps.name, "Boundary maps")
