from django.db import IntegrityError
from django.template import Template, Context
from django.test import Client
from django.test import TestCase, override_settings
# from django.contrib.auth.models import User
from django.urls import reverse
from djmoney.money import Money

from associates.models import Organization
from associates.models import OrganizationUser
from clients.tests.tests_models import ClientsTestCase
from contact.models import Person, Company
from contact.models import User
from contact.tests.tests_models import ContactTestCase
from products.models import (Category, Price, Product, Discount, TestParameter, MeasurementUnit)
from django.conf import settings


class CategoryProductViewsTest(TestCase):
    fixtures = ['roles']

    def setUp(self):
        ContactTestCase.setUp(self)
        c = Company.objects.get(name__startswith="Sky")
        a, created = Organization.objects.create_dealership(
            company=c)
        self.assertEqual(created, True)
        u = User.objects.get(username="testinguser")
        p = Person.objects.get(user=u)
        p.company = c
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()
        self.orguser = OrganizationUser.objects.get(user=u)
        cat = Category.objects.create(name="Soil Test 12345")
        Product.objects.create(name="FertiSaver-N", created_by=self.orguser.organization, category=cat)
        self.client = Client()
        # Price.objects.create(amount=Money(49.99, 'USD'),
        #                      item=p)

    def test_dealer_can_view_products_page(self):
        self.client.force_login(User.objects.get(username="testinguser"))
        url = reverse('category_list')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertIn(b"Soil Test 12345", resp.content)

    def test_different_dealer_cannot_view_product_page(self):
        c2 = Company.objects.create(name="Dealer2")
        a, created = Organization.objects.create_dealership(
            company=c2)
        u2 = User.objects.create_user("user2")
        p = Person.objects.get(user=u2)
        p.company = c2
        p.is_company_contact = True
        p.save()
        l = a.add_company_members_to_organization()

        self.client.force_login(u2)
        url = reverse('category_list')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertNotIn(b"Soil Test 12345", resp.content)

    def test_client_can_view_products_page(self):
        self.assertTrue(settings.CLIENT_CAN_VIEW_PRODUCTS)
        clt = ClientsTestCase.createClient(self)
        clt.originator = self.orguser
        clt.save()
        clt_person = Person.objects.get(id=clt.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)

        url = reverse('category_list')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertIn(b"Soil Test 12345", resp.content)

    @override_settings(CLIENT_CAN_VIEW_PRODUCTS=False)
    def test_client_cannot_view_products_page(self):
        clt = ClientsTestCase.createClient(self)
        clt_person = Person.objects.get(id=clt.contact.id)
        clt_user = clt_person.get_or_create_user_from_person()
        self.client.force_login(clt_user)

        url = reverse('category_list')

        self.assertFalse(settings.CLIENT_CAN_VIEW_PRODUCTS)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 403)
