from clients.tests.tests_models import ClientsTestCase
from products.models import Product, Package
from contact.models import User
from associates.tests.tests_browser import BrowserTests

from selenium.webdriver.support.ui import Select


class ProductTests(BrowserTests):
    fixtures = ['roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.set_password("buckeyes")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.is_active = True
        self.user.save()

    def test_products_save_a_copy(self):
        self._login(self.user.username, "buckeyes")
        self.wd.get('%s%s' % (self.live_server_url, "/admin/products/product/add/"))
        self.wd.find_element_by_name("name").send_keys("Product 1")

        # Add a new category
        self.wd.find_css("#add_id_category > img").click()
        main_window = self.wd.window_handles[0]
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("name").send_keys("Category 1")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add a new Test Parameter
        self.wd.find_css("#add_id_test_parameters > img").click()
        # main_window = self.wd.window_handles[0]
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("name").send_keys("Organic N")

        # Add a new Unit
        self.wd.find_css("#add_id_unit > img").click()
        self.wd.switch_to.window(self.wd.window_handles[2])
        self.wd.find_element_by_name("unit").send_keys("ppm")
        self.wd.find_css("input[type='submit']").click()

        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add another new Test Parameter
        self.wd.find_css("#add_id_test_parameters > img").click()
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("name").send_keys("pH")

        # Add another new Unit
        self.wd.find_css("#add_id_unit > img").click()
        self.wd.switch_to.window(self.wd.window_handles[2])
        self.wd.find_element_by_name("unit").send_keys("unit")
        self.wd.find_css("input[type='submit']").click()

        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Custom Fields
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").send_keys("Custom Field 1")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").send_keys(
            "Help Text 1")
        self.wd.find_element_by_link_text("Add another Custom field").click()
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").send_keys(
            "Custom Field 2")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").send_keys(
            "Help Text 2")

        # Add Prices
        self.wd.find_element_by_link_text("Add another Price").click()
        self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").send_keys("5")
        self.wd.find_css("#add_id_products-price-content_type-object_id-0-saleunit > img").click()
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("abbreviation").send_keys("/ac")
        self.wd.find_element_by_name("description").send_keys("per acre")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Discount
        self.wd.find_element_by_link_text("Add another Discount").click()
        self.wd.find_element_by_xpath("//tr[@id='products-discount-content_type-object_id-0']//span").click()
        self.wd.find_element_by_xpath(
            "//ul[@id='select2-id_products-discount-content_type-object_id-0-dealer-results']//li[text()='SkyData Technologies LLC']").click()
        self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").send_keys("10")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()

        products = Product.objects.all()
        self.assertEqual(len(products), 1)

        # Creating a copy of product
        self.wd.find_element_by_link_text("Product 1").click()
        self.wd.find_element_by_xpath("//input[@name='_saveasnew']").click()
        self.assertIn("You may edit it again below.", self.wd.page_source)


        # Checking there are two products in the Product Model
        products = Product.objects.all()
        self.assertEqual(len(products), 2)

        # Checking that the Category, Test Parameters, Custom Fields, Prices, and Discounts are same as above
        self.assertEqual(self.wd.find_element_by_name("name").get_attribute("value"), "Product 1")
        self.wd.find_element_by_name("name").clear()
        self.wd.find_element_by_name("name").send_keys("Product 1 Updated")
        self.assertEqual(self.wd.find_element_by_id("select2-id_category-container").get_attribute("title"),
                         "Category 1")

        self.assertEqual(
            self.wd.find_element_by_xpath("//select[@id='id_test_parameters_to']//option[1]").get_attribute("title"),
            "Organic N ppm")
        self.assertEqual(
            self.wd.find_element_by_xpath("//select[@id='id_test_parameters_to']//option[2]").get_attribute("title"),
            "pH unit")

        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").get_attribute("value"),
            "Custom Field 1")
        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").get_attribute("value"),
            "Help Text 1")

        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").get_attribute(
                "value"),
            "Custom Field 2")
        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").get_attribute(
                "value"),
            "Help Text 2")

        self.assertEqual(
            self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").get_attribute("value"),
            "5.00")
        select = Select(self.wd.find_element_by_name("products-price-content_type-object_id-0-saleunit"))
        self.assertEqual(select.first_selected_option.text, "/ac")

        self.assertEqual(
            self.wd.find_element_by_id("select2-id_products-discount-content_type-object_id-0-dealer-container").get_attribute("title"),
                         "SkyData Technologies LLC")
        self.assertEqual(
            self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").get_attribute("value"),
            "10.00")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()
        self.assertIn("was changed successfully.", self.wd.page_source)

    def tearDown(self):
        self.user = User.objects.get(username="testinguser")
        self.user.is_superuser = False
        self.user.is_staff = False
        self.user.is_active = False
        self.user.save()


class PackageTests(BrowserTests):
    fixtures = ['roles']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.user.set_password("buckeyes")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.is_active = True
        self.user.save()

    def test_product_packages_save_a_copy(self):
        # Add a new Product
        self._login(self.user.username, "buckeyes")
        self.wd.get('%s%s' % (self.live_server_url, "/admin/products/product/add/"))
        self.wd.find_element_by_name("name").send_keys("Product 2")

        # Add a new category
        self.wd.find_css("#add_id_category > img").click()
        main_window = self.wd.window_handles[0]
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("name").send_keys("Category 2")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Custom Fields
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").send_keys(
            "Custom Field 3")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").send_keys(
            "Help Text 3")
        self.wd.find_element_by_link_text("Add another Custom field").click()
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").send_keys(
            "Custom Field 4")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").send_keys(
            "Help Text 4")

        # Add Prices
        self.wd.find_element_by_link_text("Add another Price").click()
        self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").send_keys("7")
        self.wd.find_css("#add_id_products-price-content_type-object_id-0-saleunit > img").click()
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("abbreviation").send_keys("/ea")
        self.wd.find_element_by_name("description").send_keys("per each")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Discount
        self.wd.find_element_by_link_text("Add another Discount").click()
        self.wd.find_element_by_xpath("//tr[@id='products-discount-content_type-object_id-0']//span").click()
        self.wd.find_element_by_xpath(
            "//ul[@id='select2-id_products-discount-content_type-object_id-0-dealer-results']//li[text()='SkyData Technologies LLC']").click()
        self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").send_keys("10")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()

        # Add a new Product
        self.wd.get('%s%s' % (self.live_server_url, "/admin/products/product/add/"))
        self.wd.find_element_by_name("name").send_keys("Product 3")

        # Add a new category
        self.wd.find_css("#add_id_category > img").click()
        main_window = self.wd.window_handles[0]
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("name").send_keys("Category 3")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Custom Fields
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").send_keys(
            "Custom Field 5")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").send_keys(
            "Help Text 5")
        self.wd.find_element_by_link_text("Add another Custom field").click()
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").send_keys(
            "Custom Field 6")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").send_keys(
            "Help Text 6")

        # Add Prices
        self.wd.find_element_by_link_text("Add another Price").click()
        self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").send_keys("7")
        self.wd.find_css("#add_id_products-price-content_type-object_id-0-saleunit > img").click()
        self.wd.switch_to.window(self.wd.window_handles[1])
        self.wd.find_element_by_name("abbreviation").send_keys("/ea")
        self.wd.find_element_by_name("description").send_keys("per each")
        self.wd.find_css("input[type='submit']").click()
        self.wd.switch_to.window(main_window)

        # Add Discount
        self.wd.find_element_by_link_text("Add another Discount").click()
        self.wd.find_element_by_xpath("//tr[@id='products-discount-content_type-object_id-0']//span").click()
        self.wd.find_element_by_xpath(
            "//ul[@id='select2-id_products-discount-content_type-object_id-0-dealer-results']//li[text()='SkyData Technologies LLC']").click()
        self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").send_keys("10")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()

        # Creating a Product Package
        self.wd.get('%s%s' % (self.live_server_url, "/admin/products/package/add/"))
        self.wd.find_element_by_name("name").send_keys("Package 1")

        # Select a Category
        self.wd.find_element_by_xpath("//span[@aria-labelledby='select2-id_category-container']").click()
        self.wd.find_element_by_xpath("//ul[@id='select2-id_category-results']//li[text()='Category 2']").click()

        # Select Products
        self.wd.find_element_by_xpath("//select[@id='id_package_from']//option[@title='Product 2']").click()
        self.wd.find_element_by_link_text("Choose").click()
        self.wd.find_element_by_xpath("//select[@id='id_package_from']//option[@title='Product 3']").click()
        self.wd.find_element_by_link_text("Choose").click()

        # Add Custom Fields
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").send_keys(
            "Custom Field 5")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").send_keys(
            "Help Text 5")
        self.wd.find_element_by_link_text("Add another Custom field").click()
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").send_keys(
            "Custom Field 6")
        self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").send_keys(
            "Help Text 6")

        # Add Prices
        self.wd.find_element_by_link_text("Add another Price").click()
        self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").send_keys("10")
        # self.wd.find_element_by_name("products-price-content_type-object_id-0-saleunit").click()
        select = Select(self.wd.find_element_by_name("products-price-content_type-object_id-0-saleunit"))
        select.select_by_visible_text("/ea")

        # Add Discount
        self.wd.find_element_by_link_text("Add another Discount").click()
        self.wd.find_element_by_xpath("//tr[@id='products-discount-content_type-object_id-0']//span").click()
        self.wd.find_element_by_xpath(
            "//ul[@id='select2-id_products-discount-content_type-object_id-0-dealer-results']//li[text()='SkyData Technologies LLC']").click()
        self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").send_keys("10")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()

        packages = Package.objects.all()
        self.assertEqual(len(packages), 1)

        # Creating a copy of Package
        self.wd.find_element_by_link_text("Package 1").click()
        self.wd.find_element_by_xpath("//input[@name='_saveasnew']").click()
        self.assertIn("You may edit it again below.", self.wd.page_source)

        # Checking there are two products in the Product Model
        packages = Package.objects.all()
        self.assertEqual(len(packages), 2)

        # Checking that the Category, Products, Custom Fields, Prices, and Discounts are same as above
        self.assertEqual(self.wd.find_element_by_name("name").get_attribute("value"), "Package 1")
        self.wd.find_element_by_name("name").clear()
        self.wd.find_element_by_name("name").send_keys("Package 1 Updated")
        self.assertEqual(self.wd.find_element_by_id("select2-id_category-container").get_attribute("title"),
                         "Category 2")

        self.assertEqual(
            self.wd.find_element_by_xpath("//select[@id='id_package_to']//option[1]").get_attribute("title"),
            "Product 2")
        self.assertEqual(
            self.wd.find_element_by_xpath("//select[@id='id_package_to']//option[2]").get_attribute("title"),
            "Product 3")

        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-name").get_attribute(
                "value"),
            "Custom Field 5")
        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-0-help_text").get_attribute(
                "value"),
            "Help Text 5")

        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-name").get_attribute(
                "value"),
            "Custom Field 6")
        self.assertEqual(
            self.wd.find_element_by_name("customnotes-customfield-content_type-object_id-1-help_text").get_attribute(
                "value"),
            "Help Text 6")

        self.assertEqual(
            self.wd.find_element_by_name("products-price-content_type-object_id-0-amount_0").get_attribute("value"),
            "10.00")
        select = Select(self.wd.find_element_by_name("products-price-content_type-object_id-0-saleunit"))
        self.assertEqual(select.first_selected_option.text, "/ea")

        self.assertEqual(
            self.wd.find_element_by_id(
                "select2-id_products-discount-content_type-object_id-0-dealer-container").get_attribute("title"),
            "SkyData Technologies LLC")
        self.assertEqual(
            self.wd.find_element_by_name("products-discount-content_type-object_id-0-discount").get_attribute("value"),
            "10.00")
        self.wd.find_element_by_xpath("//input[@name='_save']").click()
        self.assertIn("was changed successfully.", self.wd.page_source)

    def tearDown(self):
        self.user = User.objects.get(username="testinguser")
        self.user.is_superuser = False
        self.user.is_staff = False
        self.user.is_active = False
        self.user.save()
