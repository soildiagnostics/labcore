from django.db.models import Q
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from associates.models import OrganizationUser
from common.apihelper import api_group_permission_classfactory
from .models import Product, Category
from .serializers import CategorySerializer, NestedProductSerializer

ProductAPIGroupPermission = api_group_permission_classfactory("ProductAPIGroupPermission", "ProductAPI")


class APIProductList(generics.ListAPIView):
    """
    Send list of products
    """
    def get_queryset(self):
        queryset = Product.objects.filter(active=True, publish=True)
        searchterm = self.request.query_params.get("search", None)
        if searchterm:
            q_params = Q(category__name__icontains=searchterm)|\
                Q(category__description__icontains=searchterm)|\
                Q(name__icontains=searchterm)|\
                Q(description__icontains=searchterm)|\
                Q(test_parameters__name__iexact=searchterm)|\
                Q(svx_uuid__iexact=searchterm)
            queryset = queryset.filter(q_params).distinct('name')

        return queryset
    serializer_class = NestedProductSerializer

    def get_serializer_context(self):
        orguser = OrganizationUser.objects.get(user=self.request.user)
        context = super().get_serializer_context()
        context.update({'dealer':orguser.organization})
        return context

class APICategoryList(generics.ListAPIView):
    """
    Send list of categories
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class APICreateProduct(generics.ListCreateAPIView):

    serializer_class = NestedProductSerializer
    permission_classes = (IsAuthenticated, ProductAPIGroupPermission)

    def get_queryset(self):
        return Product.objects.filter(active=True, publish=True)

class APIRetrieveDeleteProduct(generics.RetrieveDestroyAPIView):
    serializer_class = NestedProductSerializer
    permission_classes = (IsAuthenticated, ProductAPIGroupPermission)
    lookup_url_kwarg = 'svx_uuid'
    lookup_field = 'svx_uuid'

    def get_queryset(self):
        return Product.objects.filter(active=True, publish=True)
