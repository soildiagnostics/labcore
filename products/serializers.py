from django.contrib.contenttypes.models import ContentType
from djmoney.money import Money
from rest_framework import serializers

from customnotes.serializers import CustomFieldSerializer
from svx.models import SVXParticipant
from .models import Category, Product, Price, SaleUnit, TestParameter, MeasurementUnit


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'description')

class CategoryNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')

class MeasurementUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeasurementUnit
        fields = "__all__"

class TestParameterSerializer(serializers.ModelSerializer):
    unit = serializers.SerializerMethodField()
    def get_unit(self, obj):
        return str(obj.unit)
    class Meta:
        model = TestParameter
        fields = ('name', 'unit', 'calculated')

    def create(self, vd):
        unit, _ = MeasurementUnit.objects.get_or_create(unit=self.initial_data['unit'])
        try:
            tp = TestParameter.objects.get(**vd)
        except TestParameter.DoesNotExist:
            tp = TestParameter(**vd)
        tp.unit = unit
        tp.save()
        return tp


class SaleUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = SaleUnit
        fields = "__all__"


class PriceSerializer(serializers.ModelSerializer):
    saleunit = SaleUnitSerializer(many=False)
    string = serializers.SerializerMethodField()
    def get_string(self, obj):
        return str(obj)
    currency = serializers.SerializerMethodField()
    def get_currency(self, obj):
        return repr(obj.amount.currency)
    class Meta:
        model = Price
        fields = ("amount", "saleunit", "string", "currency")

    def create(self, validated_data):
        su = SaleUnitSerializer(data=validated_data['saleunit'])
        if su.is_valid():
            su = su.save()

            price = Price.objects.create(
                amount = Money(self.initial_data['amount'], self.initial_data['currency']),
                saleunit=su,
                content_type=ContentType.objects.get_for_model(self.initial_data['content_object']),
                object_id=self.initial_data['object_id']
            )

            return price
        return None


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False)
    test_parameters = TestParameterSerializer(many=True, read_only=True)
    custom_fields = serializers.SerializerMethodField()
    prices = serializers.SerializerMethodField()
    vendor_product_id = serializers.SerializerMethodField()
    vendor_svx_id = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    def get_created_by(self, obj):
        return str(obj.created_by)
    def get_vendor_svx_id(self, obj):
        if obj.created_by:
            try:
                svx_participant = SVXParticipant.objects.get(organization=obj.created_by)
                return svx_participant.id
            except SVXParticipant.DoesNotExist:
                return None
        return SVXParticipant.objects.me.id
    def get_vendor_product_id(self, obj):
        return obj.svx_uuid
    def get_prices(self, obj):
        if 'dealer' in self.context.keys():
            return PriceSerializer(obj.all_discounted_prices(self.context["dealer"]), many=True).data
        else:
            return PriceSerializer(obj.prices.all(), many=True).data
    def get_custom_fields(self, obj):
        fields = obj.custom_fields.all()
        return CustomFieldSerializer(fields, many=True).data

    class Meta:
        model = Product
        fields = ('id', "vendor_svx_id", "vendor_product_id", "created_by", 'name',
                  "prices", "description", "product_page", "stock", "category", "test_parameters",
                  "custom_fields", "active", "publish")

    def create(self, vd):
        ## vd is validated_data
        ## Check if product already exists
        try:
            prod = Product.objects.get(svx_uuid=self.initial_data['vendor_product_id'])
        except Product.DoesNotExist:
            participant = SVXParticipant.objects.get(id=self.initial_data['vendor_svx_id'])
            cat, created = Category.objects.get_or_create(
                name=vd['category']['name'],
                description=vd['category']['description'],
            )
            prod = Product(category=cat,
                           svx_uuid=self.initial_data['vendor_product_id'],
                           created_by=participant.organization)

        prod.active = vd['active']
        prod.publish = vd['publish']
        prod.name=vd['name']
        prod.description=vd['description']
        prod.product_page=vd['product_page']
        prod.save()

        tplist = self.initial_data['test_parameters']
        for tp in tplist:
            tps = TestParameterSerializer(data=tp)
            if tps.is_valid():
                tpc = tps.save()
                prod.test_parameters.add(tpc)

        cflist = self.initial_data['custom_fields']
        for cf in cflist:
            cf['content_object'] = prod
            cf['object_id'] = prod.id
            cfs = CustomFieldSerializer(data=cf)
            if cfs.is_valid():
                cfc = cfs.save()

        pricelist = self.initial_data['prices']
        for price in pricelist:
            price['content_object'] = prod
            price['object_id'] = prod.id
            priceser = PriceSerializer(data=price)
            if priceser.is_valid():
                priceobj = priceser.save()

        return prod


# class PackageSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Product
#         fields = ("name", "svx_uuid")

class NestedProductSerializer(ProductSerializer):
    package = ProductSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', "vendor_svx_id", "vendor_product_id", "created_by", 'name',
                  "prices", "description", "product_page", "stock", "category",
                  "test_parameters", "package", "custom_fields", "is_package", "active", "publish")

    def create(self, vd):
        package_ser = ProductSerializer(data=self.initial_data)
        if package_ser.is_valid():
            package = package_ser.save()
            package.active = vd['active']
            package.publish = vd['publish']
            for p in self.initial_data['package']:
                ser = ProductSerializer(data=p)
                if ser.is_valid():
                    ser.save()
            return package
        return None


class ProductSummarySerializer(serializers.ModelSerializer):
    category_color = serializers.SerializerMethodField()
    image_set = serializers.SerializerMethodField()
    def get_category_color(self, obj):
        try:
            return obj.category.color
        except AttributeError:
            return None

    def get_image_set(self, obj):
        img_set = []
        if obj.is_package:
            img_set = obj.package.exclude(image="").values_list("image", flat=True)
            img_set = [f"/media/{i}" for i in img_set]
        return img_set

    class Meta:
        model = Product
        fields = ("name", "color", "image", "image_set", "category_color", "is_package")


