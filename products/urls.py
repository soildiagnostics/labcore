from django.urls import path

from common.apihelper import apipath
from . import views
from .apiviews import APIProductList, APICategoryList, APICreateProduct, APIRetrieveDeleteProduct

urlpatterns = [
    path('', views.CategoryList.as_view(), name="category_list"),
    path('svx/', views.SVXProductsList.as_view(), name="svx_products"),
    path('data/', views.SVXProductsListJSON.as_view(), name="svx_list_json")
    ]

urlpatterns += [
    apipath('list/', APIProductList.as_view(), name="api_products"),
    apipath('categories/', APICategoryList.as_view(), name="api_categories"),
    apipath('publish/', APICreateProduct.as_view(), name="api_publish_product"),
    apipath('product/<str:svx_uuid>/', APIRetrieveDeleteProduct.as_view(), name="api_retreive_delete_product")
]