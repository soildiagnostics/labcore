from django.core.management.base import BaseCommand
from products.models import TestParameterRenderer, BreakpointAbundance, ParameterBreakpoint, TestParameter

class Command(BaseCommand):

    def handle(self, *args, **options):

        abundances = BreakpointAbundance.objects.all()
        for parameter in TestParameter.objects.all():
            tp, created = TestParameterRenderer.objects.get_or_create(parameter=parameter)
            for a in abundances:
                ParameterBreakpoint.objects.get_or_create(renderer=tp,
                                                          description=a)
