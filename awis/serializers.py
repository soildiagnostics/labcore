from rest_framework import serializers

from .models import AWISFieldSubscription, AWISFieldEventData


class FieldSerializerForWeather(serializers.ModelSerializer):
    """
    Ensure that the minimal serializer works only on fields that are located,
    i.e. have a boundary.
    """
    coords = serializers.SerializerMethodField()
    def get_coords(self, obj):
        lng, lat = obj.field.boundary.centroid.coords
        return (lng, lat)
    class Meta:
        model = AWISFieldSubscription
        fields = ('field', 'coords')

class AWISFieldEventSerializer(serializers.Serializer):
    _id = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    allDay = serializers.SerializerMethodField()
    start = serializers.SerializerMethodField()
    end = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()
    className = serializers.SerializerMethodField()
    editable = serializers.SerializerMethodField()
    color = serializers.SerializerMethodField()
    textColor = serializers.SerializerMethodField()
    def get_editable(self, obj):
        return False
    def get_textColor(self, obj):
        return 'white'
    def get_color(self, obj):
        return "#003707" ## AWIS green.
    def get_className(self, obj):
        return 'weather'
    def get_url(self, obj):
        return None
    def get_end(self, obj):
        return None
    def get_start(self, obj):
        return obj.date.isoformat()
    def get_allDay(self, obj):
        return True
    def get_title(self, obj):
        return f"Hi: {obj.max_air_temperature} F, " \
            f"Lo: {obj.min_air_temperature}, " \
            f"Precip: {obj.precipitation}"
    def get__id(self, obj):
        return f"weather_{obj.pk}"
    class Meta:
        models = AWISFieldEventData
        fields = "__all__"




