from django.contrib import admin

from .models import AWISFieldSubscription, AWISFieldEventData


# Register your models here.

@admin.register(AWISFieldSubscription)
class AWISSubscriptionAdmin(admin.ModelAdmin):
    model = AWISFieldSubscription
    list_display = ('field', 'start_date', 'end_date')
    raw_id_fields = ('field', )

@admin.register(AWISFieldEventData)
class AWISEventAdmin(admin.ModelAdmin):
    model = AWISFieldEventData
    list_display = ('field', 'date')
    raw_id_fields = ('field', )