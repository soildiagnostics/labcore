from common.apihelper import apipath
from .views import APIAWISSubscribers, APIAWISFieldEvents

urlpatterns = [
    apipath('list/', APIAWISSubscribers.as_view(), name="api_awis_fields_list"),
    apipath('post/', APIAWISFieldEvents.as_view(), name="api_awis_field_events")
]