import datetime
import json

from django.contrib.auth.models import Group
from django.test import Client as TestClient
from django.test import TestCase
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from associates.models import Organization
# Create your tests here.
from awis.models import AWISFieldSubscription
from clients.tests.tests_models import ClientsTestCase
from contact.models import User, Person, Company
from .views import APIAWISFieldEvents


class AWISAPITestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        ClientsTestCase.setUp(self)
        self.user = User.objects.get(username="testinguser")
        self.client = TestClient()
        self.client.force_login(self.user)
        self.field = ClientsTestCase.createField(ClientsTestCase())

        # add different org and orguser
        self.alt_org_user = User.objects.create(first_name='Amos', last_name='Colleague',
                                                username='amosc',
                                                email='colleague@test.com')
        person = Person.objects.get(user=self.alt_org_user)
        c = Company.objects.create(name="Alternate Org")
        self.alt_org, created = Organization.objects.create_dealership(
            company=c)
        self.alt_org.add_person_to_organization(person)
        g = Group.objects.create(name="AWIS")
        g.user_set.add(self.alt_org_user)

    def test_add_subscription(self):
        new_sub = AWISFieldSubscription.objects.create(
            field=self.field,
            start_date=datetime.datetime.today(),
            end_date=datetime.datetime.today() + datetime.timedelta(days=365)
        )

        self.assertNotEqual(new_sub.pk, None)

    def test_user_in_group(self):
        self.assertTrue(self.alt_org_user.groups.filter(name="AWIS").exists())

    def test_get_token(self):
        self.alt_org_user.set_password('testpass')
        self.alt_org_user.save()

        token_url = '/api-token-auth/'
        login_data = {
            'username': 'amosc',
            'password': 'testpass'
        }

        res = self.client.post(token_url, data=login_data)
        self.assertEqual(res.status_code, 200)


    def test_get_field_list(self):
        AWISFieldSubscription.objects.create(
            field=self.field,
            start_date=datetime.datetime.today(),
            end_date=datetime.datetime.today() + datetime.timedelta(days=365)
        )

        self.alt_org_user.set_password('testpass')
        self.alt_org_user.save()
        token = Token.objects.create(user=self.alt_org_user)

        aclient = APIClient()
        aclient.login(username='amosc', password='testpass')
        aclient.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        res = aclient.get(reverse("api_awis_fields_list"))

        field_id = json.loads(res.content)[0]['field']


        self.assertEqual(res.status_code, 200)
        self.assertEqual(field_id, self.field.id)

    def test_post_csv(self):
        with open("awis/CSVexample.csv", 'r') as f:
            contents = f.read()
        self.alt_org_user.set_password('testpass')
        self.alt_org_user.save()
        token = Token.objects.create(user=self.alt_org_user)

        aclient = APIClient()
        aclient.login(username='amosc', password='testpass')
        aclient.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        res = aclient.post(reverse("api_awis_field_events"), data={
            'data': contents
        })
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content)['message'], "processed")

    def test_make_event(self):
        contents = f"{self.field.pk},35.05,-90.5,03/13/2019,73.9,46,59.95,0,73.9,46,60,0.21,0.15\n"
        view = APIAWISFieldEvents()
        field, res = view.make_event(contents)
        self.assertEqual(self.field.pk, field)
        self.assertTrue(res)
