import datetime

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from clients.models import Field
from common.apihelper import api_group_permission_classfactory
from .models import AWISFieldSubscription, AWISFieldEventData
from .serializers import FieldSerializerForWeather

# Create your views here.

AWISGroupPermission = api_group_permission_classfactory("AWISGroupPermission", "AWIS")


class APIAWISSubscribers(generics.ListAPIView):
    permission_classes = (IsAuthenticated, AWISGroupPermission)
    queryset = AWISFieldSubscription.objects.all()
    serializer_class = FieldSerializerForWeather


class APIAWISFieldEvents(APIView):
    permission_classes = (IsAuthenticated, AWISGroupPermission)

    def get(self, request, format=None):

        inst = {'end': 'YYYY-MM-DDTHH:MM:SS|YYYY-MM-DD',
                'start': 'YYYY-MM-DDTHH:MM:SS|YYYY-MM-DD',
                'title': 'Event (e.g. Rain: 0.4 in)',
                'allDay': False,
                'field_id': 12345678
                }

        return Response([inst])

    def make_event(self, line):
        line = line.strip().split(",")
        try:
            field = Field.objects.get(id=int(line[0]))
        except Field.DoesNotExist:
            return (line[0], 0)
        except ValueError:
            return (line[0], 0)
        AWISFieldEventData.objects.create(
            field=field,
            date=datetime.datetime.strptime(line[3], "%m/%d/%Y").date(),
            max_air_temperature=line[4],
            min_air_temperature=line[5],
            avg_air_temperature=line[6],
            precipitation=line[7],
            max_soil_temperature=line[8],
            min_soil_temperature=line[9],
            avg_soil_temperature=line[10],
            evap=line[11],
            pet=line[12],
        )
        return (field.id, 1)

    def post(self, request, format=None):
        data = request.data.get('data').strip().split("\n")
        event = [self.make_event(line) for line in data]

        return Response({'message': 'processed',
                         'result': event})
