from django.core.exceptions import ValidationError
from django.db import models

from clients.models import Field
from common.models import ParseTimeMixin


class AWISFieldSubscription(models.Model):
    field = models.OneToOneField(Field, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()

    def clean(self, *args, **kwargs):
        if not self.field.is_located:
            raise ValidationError
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.field)


class AWISFieldEventManager(ParseTimeMixin, models.Manager):

    def get_events_in_window(self, field, start=None, end=None):
        events = AWISFieldEventData.objects.filter(field=field)
        events = events.filter(date__gte=self.parse_with_tz(start))
        events = events.filter(date__lte=self.parse_with_tz(end))
        return events


class AWISFieldEventData(models.Model):
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    date = models.DateField(help_text=("Data for 24h total ending 12z on this date"))
    max_air_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    min_air_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    avg_air_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    precipitation = models.FloatField(blank=True, help_text="Inches")
    max_soil_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    min_soil_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    avg_soil_temperature = models.FloatField(blank=True, help_text="Fahrenheit")
    evap = models.FloatField(blank=True, help_text="Evapotranspiration in inches")
    pet = models.FloatField(blank=True, help_text="Potential evapotranspiration in inches")
    objects = AWISFieldEventManager()
