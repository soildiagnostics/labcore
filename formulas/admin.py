# Register your models here.
from django.contrib import admin
from .models import Formula, FormulaVariable

@admin.register(Formula)
class FormulaAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_inputs', 'output', 'code', 'description')
    filter_horizontal = ('inputs', 'scalar_defaults')
    # list_editable = ('input_parameters', 'output_parameter', 'code', 'description')

@admin.register(FormulaVariable)
class FormulaVariableAdmin(admin.ModelAdmin):
    pass