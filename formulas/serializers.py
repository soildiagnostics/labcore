from formulas.models import Formula, FormulaVariable
from rest_framework import serializers

class FormulaVariableSerializer(serializers.ModelSerializer):

    class Meta:
        model = FormulaVariable

class FormulaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Formula
        fields = ('id', 'name', 'description')