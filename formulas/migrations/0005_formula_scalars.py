# Generated by Django 3.0.5 on 2020-08-22 18:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formulas', '0004_auto_20200425_0844'),
    ]

    operations = [
        migrations.AddField(
            model_name='formula',
            name='scalars',
            field=models.CharField(default='', help_text='Comma separated list of numerical constants passed as extra arguments to a formula', max_length=100),
        ),
    ]
