# Generated by Django 3.0.5 on 2020-08-23 02:05

from django.db import migrations, models
import django.db.models.deletion
import formulas.models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0046_auto_20200423_0828'),
        ('formulas', '0007_auto_20200822_1452'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formula',
            name='input_parameters',
        ),
        migrations.RemoveField(
            model_name='formula',
            name='output_parameter',
        ),
        migrations.RemoveField(
            model_name='formula',
            name='scalar_defaults',
        ),
        migrations.CreateModel(
            name='FormulaVariable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('default_value', models.FloatField(blank=True, null=True)),
                ('scalar', models.BooleanField(default=False)),
                ('test_parameter', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='products.TestParameter')),
            ],
        ),
        migrations.AddField(
            model_name='formula',
            name='inputs',
            field=models.ManyToManyField(related_name='formula_input', to='formulas.FormulaVariable'),
        ),
        migrations.AddField(
            model_name='formula',
            name='output',
            field=models.ForeignKey(default=formulas.models.default_output, on_delete=django.db.models.deletion.PROTECT, related_name='formula_output', to='formulas.FormulaVariable'),
        ),
        migrations.AddField(
            model_name='formula',
            name='scalar_defaults',
            field=models.ManyToManyField(related_name='formula_scalars', to='formulas.FormulaVariable'),
        ),
    ]
