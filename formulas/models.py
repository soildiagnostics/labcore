from math import *

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.functional import cached_property

from clients.models import SubfieldLayer
from django.apps import apps
from orders.models import Order
from products.models import TestParameter, MeasurementUnit
from samples.models import Sample, LabMeasurement
from django.db.models import Max, Count
from django.conf import settings

safe_list = ['acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh', 'degrees',
             'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 'hypot', 'ldexp', 'log',
             'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh']

prohibited_list = [
    "__import__",
    "__builtins__",
    "__name__",
    "__subclass__",
    "__main__",
    "__repr__",
    "__dir__",
    '__class__',
    '__delattr__',
    '__dict__',
    '__dir__',
    '__doc__',
    '__eq__',
    '__format__',
    '__ge__',
    '__getattribute__',
    '__gt__',
    '__hash__',
    '__init__',
    '__init_subclass__',
    '__le__',
    '__lt__',
    '__module__',
    '__ne__',
    '__new__',
    '__reduce__',
    '__reduce_ex__',
    '__repr__',
    '__setattr__',
    '__sizeof__',
    '__str__',
    '__subclasshook__',
    '__weakref__'
]


# Create your models here.

def validate_code(value):
    check = value
    for prohibited in prohibited_list:
        check = check.replace(prohibited, "")
    if not value == check:
        raise ValidationError("Can't use dunders in code")


class FormulaVariable(models.Model):
    test_parameter = models.OneToOneField(TestParameter, blank=True, null=True, on_delete=models.PROTECT)
    default_value = models.FloatField(blank=True, null=True)
    scalar = models.BooleanField(default=False)
    caption = models.CharField(max_length=100, blank=True, default="")

    def __str__(self):
        return f"{self.test_parameter.name} {self.test_parameter.unit}{' (scalar)' if self.scalar else ''}"

    @cached_property
    def shp_string(self):
        return f"{self.test_parameter.name}{f'_{self.test_parameter.unit.unit}' if self.test_parameter.unit.unit != 'null' else ''}"

    def clean(self):
        if self.scalar and self.default_value is None:
            raise ValidationError("Scalars must set a default value")

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)


def default_output():
    if not settings.TESTING:
        """When running tests, if this default is not bypassed, it will throw an error on the FormulaVariable object"""
        u, _ = MeasurementUnit.objects.get_or_create(unit="null")
        tp, _ = TestParameter.objects.get_or_create(name="null", unit=u)
        try:
            fm = apps.get_model("formulas", "FormulaVariable")
            fv, _ = fm.objects.get_or_create(test_parameter=tp)
            return fv.id
        except LookupError:
            pass


class Formula(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    inputs = models.ManyToManyField(FormulaVariable, related_name="formula_input")
    output = models.ForeignKey(FormulaVariable, related_name="formula_output",
                               default=default_output,
                               on_delete=models.PROTECT)
    code = models.TextField(validators=[validate_code])
    scalar_defaults = models.ManyToManyField(FormulaVariable, related_name="formula_scalars", blank=True)

    def get_inputs(self):
        return self.inputs.all()

    def __str__(self):
        return self.name

    def validate_scalars(self, **kwargs):
        """Make sure all the values are numeric - cast them into floats"""
        scalars = dict()
        for scl in self.scalar_defaults.all():
            scalars[scl.shp_string] = float(kwargs.get(scl.test_parameter.name, scl.default_value))
        return scalars

    def augment_formula(self, **kwargs):
        codestring = self.code
        for k, v in kwargs.items():
            codestring = codestring.replace(k, str(v))
        return codestring

    def _evaluate_sample(self, object, orguser, x, y0, **kw):
        xlms = [LabMeasurement.objects.filter(sample=object, parameter=i, void=False).last().value for i in x]
        safe_dict = dict([(k, globals().get(k, None)) for k in safe_list])
        safe_dict.update({'x': xlms})
        safe_dict.update(kw)
        try:
            out = LabMeasurement(sample=object,
                                 parameter=y0,
                                 value=eval(self.augment_formula(**kw), safe_dict))
            out.save(orguser=orguser)
            return out
        except SyntaxError:
            raise ValueError

    def _evaluate_order(self, object, orguser, x, y0, **kw):
        res = LabMeasurement.objects.filter(sample__order=object,
                                            parameter__in=x,
                                            void=False) \
            .values('parameter').annotate(par=Count("parameter"), latest=Max('pk')) \
            .values("latest", "sample", "parameter", "value", "par").order_by()
        xlms = dict()
        for q in res:
            sample = q['sample']
            if sample not in xlms.keys():
                xlms[sample] = dict()
            xlms[sample][q['parameter']] = float(q['value'])

        p_id = [r.id for r in [i.test_parameter for i in self.inputs.all()]]
        out_list = list()
        for key, val in xlms.items():
            xs = [val[id] for id in p_id]

            safe_dict = dict([(k, globals().get(k, None)) for k in safe_list])
            safe_dict.update({'x': xs})
            safe_dict.update(kw)
            try:
                value = eval(self.augment_formula(**kw), safe_dict)
                try:
                    out = LabMeasurement.objects.get(sample_id=key, parameter=y0)
                except LabMeasurement.DoesNotExist:
                    out = LabMeasurement(sample_id=key, parameter=y0)
                except LabMeasurement.MultipleObjectsReturned:
                    out_set = LabMeasurement.objects.filter(sample_id=key, parameter=y0)
                    out = out_set.latest("pk")
                    out_set.exclude(id__in=[out.id]).delete()
                out.value = value
                out.save(orguser=orguser)
                out_list.append(out)
            except SyntaxError:
                raise ValueError
        return out_list

    def _evaluate_subfield(self, object, orguser, x, y0, **kw):
        x_ = [xi.formulavariable.shp_string in object.dbf_field_names['field_names'] for xi in x]
        assert all(x_), f"One of these parameters {x} not in subfieldlayer {object}"
        xis = [xi.formulavariable.shp_string for xi in x]
        xgdf = object.make_geodataframe(xis)
        safe_dict = dict([(k, globals().get(k, None)) for k in safe_list])
        safe_dict.update({'row': 'row', 'x': xis})
        try:
            ## The self.code object must be able to run on a row
            codestring = self.augment_formula(**kw)

            fun = eval(f"lambda x: {codestring}", safe_dict)
            xgdf[y0.name] = xgdf[xis].apply(fun, axis=1)
            if y0.name not in object.dbf_field_names['field_names']:
                object.dbf_field_names['field_names'].append(y0.name)
            object.dbf_field_values[y0.name] = xgdf[y0.name].to_list()
            object.append_log(f"User {orguser} applied formula {self.name} with parameters {repr(kw)}",
                              save=True)
            return xgdf[y0.name].to_list()
        except SyntaxError:
            raise ValueError

    def evaluate(self, object, orguser, **kwargs):
        kw = self.validate_scalars(**kwargs)
        x = [i.test_parameter for i in self.inputs.all()]
        y0 = self.output.test_parameter
        if isinstance(object, Sample):
            return self._evaluate_sample(object, orguser, x, y0, **kw)
        elif isinstance(object, Order):
            """We can do some optimization"""
            return self._evaluate_order(object, orguser, x, y0, **kw)
        elif isinstance(object, SubfieldLayer):
            return self._evaluate_subfield(object, orguser, x, y0, **kw)
        else:
            print(type(object), type(Sample))
            raise NotImplementedError

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
