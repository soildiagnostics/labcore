import json

from django.contrib.gis.geos import GeometryCollection
from django.core.exceptions import ValidationError
from django.test import TestCase

from formulas.models import Formula, FormulaVariable
from formulas.serializers import FormulaSerializer
from orders.soilmap_helper import dependency_sort
from products.models import TestParameter, MeasurementUnit
from samples.tests.tests_calculations import CalculationsTestCase, Sample, LabMeasurement


# Create your tests here.

class FormulaTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        CalculationsTestCase.setUp(self)
        self.nullunit, _ = MeasurementUnit.objects.get_or_create(unit="null")
        pcbs = MeasurementUnit.objects.create(unit="% base saturation")
        input_par = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        self.input = FormulaVariable.objects.create(test_parameter=input_par)
        output_par = TestParameter.objects.create(name="K", unit=pcbs)
        self.output = FormulaVariable.objects.create(test_parameter=output_par)

        self.formula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=self.output,
            code="sqrt(x[0]/2)"
        )
        self.formula.inputs.add(self.input)

        l = LabMeasurement(sample=Sample.objects.last(),
                           parameter=input_par,
                           value=400)
        l.save(orguser=Sample.objects.last().order.created_by)

    def test_formula_variable(self):
        self.input.scalar = True
        with self.assertRaises(ValidationError):
            self.input.save()


    def test_calculate(self):
        s = Sample.objects.last()
        out = self.formula.evaluate(s, orguser=s.order.created_by)
        import math
        self.assertEqual(out.value, math.sqrt(200))

    def test_bad_code(self):

        newformula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=self.output,
            code="import os"
        )
        newformula.inputs.add(self.input)

        s = Sample.objects.last()
        with self.assertRaises(ValueError):
            newformula.evaluate(s, orguser=s.order.created_by)

        with self.assertRaises(ValidationError):
            newformula = Formula.objects.create(
                name="Test Formula",
                description="Some description here",
                output=self.output,
                code="dir('__builtins__')"
            )
            newformula.inputs.add(self.input)

        newformula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=self.output,
            code="x['Soil__pH__1')"
        )
        newformula.inputs.add(self.input)

        with self.assertRaises(ValidationError):
            newformula = Formula.objects.create(
                name="Test Formula",
                description="Some description here",
                output=self.output,
                code="""
(lambda fc=(
    lambda n: [
        c for c in
            ().__class__.__bases__[0].__subclasses__()
            if c.__name__ == n
        ][0]
    ):
    fc("function")(
        fc("code")(
            0,0,0,0,"KABOOM",(),(),(),"","",0,""
        ),{}
    )()
)()
"""
            )
            newformula.inputs.add(self.input)


    def test_subfieldlayer_calculations(self):
        from orders.models import Order
        order = Order.objects.get(order_number="123ABC@#$")
        from django.contrib.gis.geos import GEOSGeometry

        from clients.models import SubfieldLayer
        sf = SubfieldLayer.objects.create(
            field=order.field,
            geoms=GeometryCollection(GEOSGeometry(
                'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)),((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')),
            dbf_field_names={'field_names': ["K"]},
            dbf_field_values={'K': [200]}
        )

        knone = TestParameter.objects.create(name="K", unit=self.nullunit)
        krec = TestParameter.objects.create(name="K_rec", unit=self.nullunit)


        formula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=FormulaVariable.objects.create(test_parameter=krec),
            code="max(x['K']/2, 100)"
        )
        formula.inputs.add(FormulaVariable.objects.create(test_parameter=knone))

        result = formula.evaluate(sf, orguser=order.created_by)
        self.assertEqual(result, [100])

    def test_k_removal_cec_formula(self):
        code = """round(row['K2ORemoval'] + (((((280 - row['K_lbac']) / 15) * 100 / 5.8) * 0.6) if row['CEC'] <= 5 else (((((300 - row['K_lbac']) / 15) * 100 / 5.8) * 0.6) if row['CEC'] <= 10 else (((((350 - row['K_lbac']) / 15) * 100 / 5.8) * 0.6) if row['CEC'] <= 20 else ((((280 - row['K_lbac']) / 15) * 100 / 5.8) * 0.6)))) if row['K_lbac'] <= 400 else 0)"""
        krecfun = eval(f"lambda row: {code}")

        row = {'K2ORemoval': 43.2, 'CEC': 5, 'K_lbac': 350}
        self.assertEqual(krecfun(row), -5)

        row = {'K2ORemoval': 60, 'CEC': 10, 'K_lbac': 300}
        self.assertEqual(krecfun(row), 60)

    def test_k_buildup_cec_formula(self):
        code = """((260 - x['K_lb/ac'] * 4 / 4) if x['CEC_meq/100g'] < 12 else ((300-x['K_lb/ac'] * 4/ 4) if (x['CEC_meq/100g']>=12 and x['K_lb/ac']<300) else 0))"""

        krecfun = eval(f"lambda x: {code}")

        row = {'K_lb/ac': 225, 'CEC_meq/100g': 15, 'K_lbac': 350}
        self.assertEqual(krecfun(row), 75)

    def test_pH_cec_formula(self):
        code = """(13-2*x['pH Water units'] if (x['pH Water units'] < 6.5 and x['CEC_meq/100g'] <= 8) else (22.82-3.636*x['pH Water units'] if (x['pH Water units'] < 6.5 and x['CEC_meq/100g'] <= 24) else (43.86-7.413*x['pH Water units'] if (x['pH Water units']<6.5 and x['CEC_meq/100g']>24) else 0)))"""

        krecfun = eval(f"lambda x: {code}")

        row = {'pH Water units': 6, 'CEC_meq/100g': 20, 'K_lbac': 350}
        self.assertAlmostEqual(krecfun(row), 1, 2)


    def test_scalar(self):
        from orders.models import Order
        order = Order.objects.get(order_number="123ABC@#$")
        from django.contrib.gis.geos import GEOSGeometry

        from clients.models import SubfieldLayer
        mp = GEOSGeometry(
                'SRID=4326;MULTIPOLYGON (((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)),((-91.29771700000001 39.330082, -91.29771700000001 39.337218, -91.293125 39.337218, -91.293211 39.330115, -91.29771700000001 39.330082)))')
        sf = SubfieldLayer.objects.create(
            field=order.field,
            geoms=GeometryCollection(list(mp)),
            dbf_field_names={'field_names': ["K"]},
            dbf_field_values={'K': [200, 300]}
        )

        knone = TestParameter.objects.create(name="K", unit=self.nullunit)
        krec = TestParameter.objects.create(name="K_rec", unit=self.nullunit)
        offset = TestParameter.objects.create(name="offset", unit=self.nullunit)
        offform = FormulaVariable.objects.create(test_parameter=offset, scalar=True, default_value=50)

        formula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=FormulaVariable.objects.create(test_parameter=krec),
            code="max(x['K']/2, 100)",
        )
        formula.inputs.add(FormulaVariable.objects.create(test_parameter=knone))

        result = formula.evaluate(sf, orguser=order.created_by)
        self.assertEqual(result, [100, 150])

        formula.code = "x['K']/2 - offset"
        formula.save()
        formula.scalar_defaults.add(offform)

        result = formula.evaluate(sf, orguser=order.created_by, offset=100)
        self.assertEqual(result, [0, 50])


    def test_dependency_sort(self):
        """
        F1: A --> B
        F2: B --> C
        F3: A,C --> D
        Order should be F1, F2, F3
        """


        varA = TestParameter.objects.create(name="A", unit=self.nullunit)
        varB = TestParameter.objects.create(name="B", unit=self.nullunit)
        varC = TestParameter.objects.create(name="C", unit=self.nullunit)
        varD = TestParameter.objects.create(name="D", unit=self.nullunit)

        forA = FormulaVariable.objects.create(test_parameter=varA)
        forB = FormulaVariable.objects.create(test_parameter=varB)
        forC = FormulaVariable.objects.create(test_parameter=varC)
        forD = FormulaVariable.objects.create(test_parameter=varD)

        f1 = Formula.objects.create(
            name="Test Formula A",
            description="Some description here",
            output=forB,
            code="x['K']/2",
        )
        f1.inputs.add(forA)

        f2 = Formula.objects.create(
            name="Test Formula B",
            description="Some description here",
            output=forC,
            code="x['K']/2"
        )
        f2.inputs.add(forB)

        f3 = Formula.objects.create(
            name="Test Formula D",
            description="Some description here",
            output=forD,
            code="x['K']/2"
        )
        f3.inputs.add(forA)
        f3.inputs.add(forC)

        sorted = dependency_sort(Formula.objects.all().order_by("-pk"))
        sorted = [i.pk for i in sorted]

        self.assertTrue(sorted.index(f2.pk) > sorted.index(f1.pk))
        self.assertTrue(sorted.index(f3.pk) > sorted.index(f2.pk))

class FormulaSerializerTestCase(TestCase):
    fixtures = ['roles', 'calendar_events']

    def setUp(self):
        CalculationsTestCase.setUp(self)
        self.nullunit, _ = MeasurementUnit.objects.get_or_create(unit="null")
        pcbs = MeasurementUnit.objects.create(unit="% base saturation")
        input_par = TestParameter.objects.get(name="K", unit__unit="lb/ac")
        self.input = FormulaVariable.objects.create(test_parameter=input_par)
        output_par = TestParameter.objects.create(name="K", unit=pcbs)
        self.output = FormulaVariable.objects.create(test_parameter=output_par)

        self.formula = Formula.objects.create(
            name="Test Formula",
            description="Some description here",
            output=self.output,
            code="sqrt(x[0]/2)"
        )
        self.formula.inputs.add(self.input)

        l = LabMeasurement(sample=Sample.objects.last(),
                           parameter=input_par,
                           value=400)
        l.save(orguser=Sample.objects.last().order.created_by)

    def test_serializer(self):

        ser = FormulaSerializer(self.formula).data
        self.assertEqual(ser['id'], self.formula.id)
        self.assertIsNone(ser.get('code'))